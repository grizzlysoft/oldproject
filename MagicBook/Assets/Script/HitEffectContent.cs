﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitEffectContent : MonoBehaviour 
{
    SpriteRenderer sp_Img;
    SpriteAnimation spriteAnim;

    public void Init()
    {
        sp_Img = this.GetComponent<SpriteRenderer>();
        Hide();
    }

    public void Show(Vector3 _pos, SpriteAnimation _spriteAnim)
    {
        this.transform.position = _pos;
        spriteAnim = _spriteAnim;

        this.gameObject.SetActive(true);
        StartCoroutine(spriteAnim.PlaySpriteAnimation_EndMaxImage(sp_Img , Hide));
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }
}
