﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleAnim : MonoBehaviour 
{
    [SerializeField] UITexture tx_Main;
    [SerializeField] Texture[] ar_Texture;
    [SerializeField] GameObject go_Touch;
    public UILabel lb_Loading;
    public UILabel lb_Ver;

    private void Awake()
    {
        go_Touch.SetActive(false);

        tx_Main.mainTexture = ar_Texture[0];
        StartCoroutine(StartAnim());
    }

    private void Start()
    {
        lb_Ver.text = string.Format("Ver_{0}", Application.version);   
    }

    IEnumerator StartAnim()
    {
        while (true)
        {
            tx_Main.mainTexture = ar_Texture[0];
            yield return new WaitForSeconds(0.5f);
            tx_Main.mainTexture = ar_Texture[1];
            yield return new WaitForSeconds(0.5f);
        }
    }

    public void SetReadyIcon()
    {
        go_Touch.SetActive(true);
    }
    
    public void OnClick_BG()
    {
        if (go_Touch.activeSelf == true)
        {
            Sound_Manager.Get.PlayBGM("Bgm_1");
            StopAllCoroutines();
            Destroy(this.gameObject);
        }
    }
}
