﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialDungeonContent : MonoBehaviour 
{
    Popup_SpecialDungeon parent;

    [SerializeField] LocalizeText Label_Title;
    [SerializeField] LocalizeText Label_Desc;
    [SerializeField] UITexture Texture_BG;
    [SerializeField] UILabel Label_NeedTicket;

    Table_Manager.SpecialDungeonData data;

    public void Init(Popup_SpecialDungeon _parent,Table_Manager.SpecialDungeonData _data)
    {
        parent = _parent;
        data = _data;
        //타이틀 , 설명 셋팅
        Label_Title.ChangeText(data.NameIndex);
        Label_Desc.ChangeText(data.DescIndex);
        //배경
        Texture_BG.mainTexture = Resources.Load(string.Format("Texture/{0}", data.TextureName)) as Texture;
        //필요 쿠폰량
        Label_NeedTicket.text = string.Format("x{0}", data.Cost);
    }

    public void OnClick_Enter()
    {
        if (LocalData_Manager.Get.UseTicket(data.Cost) == true)
        {
            parent.Hide();

            //인게임 모드 변경 
            Ingame_Manager.Get.ChangeIngameMode(data.Type);
        }
    }

}
