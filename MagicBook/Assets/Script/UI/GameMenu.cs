﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CognitioConsulting.Numerics;

public class GameMenu : MonoBehaviour
{
    [HideInInspector]
    public bool IsOpen = false;
    public TweenPosition Tweenpos;
    //이동 관련
    ////////////////////////////////////////
    //메뉴
    public UILabel[] MenuLable;
    public UIScrollView[] ScrollView;
    public UIGrid[] ScrollGrid;

    //추가 재화 표시<
    [SerializeField] UILabel lb_RemainGoods;
    [SerializeField] UISprite sp_Goods;
    //강화 회수 버튼
    [SerializeField] UILabel lb_LevelUp;
    [SerializeField] GameObject go_LevelUp;

    List<ScrollItem> UpGradeScrollItems = new List<ScrollItem>();
    List<ScrollItem> ArtifactScrollItems = new List<ScrollItem>();
    List<ScrollItem> AchievementScrollItems = new List<ScrollItem>();
    List<ScrollItem> ShopScrollItems = new List<ScrollItem>();
    List<ScrollItem> BonusScrollItems = new List<ScrollItem>();

    [HideInInspector]
    public MenuOpenState State = MenuOpenState.Upgrade;

    /// <summary>
    /// 스크롤 오브젝트 생성 후 엑티브 false한다
    /// </summary>
    public void MenuInit()
    {
        //upgrade scroll item create
        for (int i = 0; i < (int)UpgradeType.Max; ++i)
        {
            ScrollItem item = (Instantiate(Resources.Load("Prefab/Upgrade_ScrollItem")) as GameObject).GetComponent<ScrollItem>();
            item.transform.parent = ScrollGrid[(int)MenuOpenState.Upgrade].transform;
            item.transform.localScale = Vector3.one;
            item.transform.localPosition = Vector3.zero;
            item.SetData((UpgradeType)i);
            UpGradeScrollItems.Add(item);
        }
        ScrollGrid[(int)MenuOpenState.Upgrade].Reposition();


        //Artifact scroll item create 
        for (int i = 0; i < (int)ArtifactType.Max; ++i)
        {
            ScrollItem item = (Instantiate(Resources.Load("Prefab/Artifact_ScrollItem")) as GameObject).GetComponent<ScrollItem>();
            item.transform.parent = ScrollGrid[(int)MenuOpenState.Artifact].transform;
            item.transform.localScale = Vector3.one;
            item.transform.localPosition = Vector3.zero;
            item.SetData((ArtifactType)i);
            ArtifactScrollItems.Add(item);
        }
        ScrollGrid[(int)MenuOpenState.Artifact].Reposition();


        //achievement scroll item create
        for (int i = 0; i < (int)AchievementType.Max; ++i)
        {
            ScrollItem item = (Instantiate(Resources.Load("Prefab/Achievement_ScrollItem")) as GameObject).GetComponent<ScrollItem>();
            item.transform.parent = ScrollGrid[(int)MenuOpenState.Achievement].transform;
            item.transform.localScale = Vector3.one;
            item.transform.localPosition = Vector3.zero;
            item.SetData((AchievementType)i);
            AchievementScrollItems.Add(item);
        }
        ScrollGrid[(int)MenuOpenState.Achievement].Reposition();

        //shop scroll item create
        for(int i = 0; i < Table_Manager.Get.li_ShopData.Count; ++i)
        {
            ScrollItem item = (Instantiate(Resources.Load("Prefab/Shop_ScrollItem")) as GameObject).GetComponent<ScrollItem>();
            item.transform.parent = ScrollGrid[(int)MenuOpenState.Shop].transform;
            item.transform.localScale = Vector3.one;
            item.transform.localPosition = Vector3.zero;
            item.SetData(Table_Manager.Get.li_ShopData[i]);
            ShopScrollItems.Add(item);
        }
        ScrollGrid[(int)MenuOpenState.Shop].Reposition();

        //shop scroll item create
        for (int i = 0; i < (int)BonusType.Max; ++i)
        {
            ScrollItem item = (Instantiate(Resources.Load("Prefab/Bonus_ScrollItem")) as GameObject).GetComponent<ScrollItem>();
            item.transform.parent = ScrollGrid[(int)MenuOpenState.Bonus].transform;
            item.transform.localScale = Vector3.one;
            item.transform.localPosition = Vector3.zero;
            item.SetData((BonusType)i);
            BonusScrollItems.Add(item);
        }
        ScrollGrid[(int)MenuOpenState.Bonus].Reposition();

        MenuChanage();

        //레벨업 횟수 갱신
        SetLevelUpLabel();

        gameObject.SetActive(false);
    }

    public void MenuOpen()
    {
        gameObject.SetActive(true);
        Tweenpos.onFinished.Clear();
        Tweenpos.onFinished.Add(new EventDelegate(TweenOpenEnd));
        Tweenpos.PlayForward();
        RefreshRuby();
    }

    public void CloseMenu()
    {
        Tweenpos.onFinished.Clear();
        Tweenpos.onFinished.Add(new EventDelegate(TweenCloseEnd));
        Tweenpos.PlayReverse();
    }

    void MenuChanage()
    {
        for (int i = 0; i < ScrollView.Length; ++i)
        {
            if (i != (int)State)
            {
                MenuLable[i].color = Color.white;
                ScrollView[i].gameObject.SetActive(false);
            }
            else
            {
                Sound_Manager.Get.PlayEffect("Effect_select");

                MenuLable[i].color = UtilityTools.HexToColor("fbb33aff");
                ScrollView[i].gameObject.SetActive(true);

                //강화탭에서만 열린다
                go_LevelUp.SetActive(State == MenuOpenState.Upgrade);
            }
        }
        
        //루비 갱신
        RefreshRuby();
    }

    public void OnClickedUpgradeTab()
    {
        State = MenuOpenState.Upgrade;
        MenuChanage();
    }

    public void OnClickedArtifactTab()
    {
        State = MenuOpenState.Artifact;
        MenuChanage();
    }

    public void OnClickedAchievementTab()
    {
        State = MenuOpenState.Achievement;
        MenuChanage();
        ReFreshAchievementItems();
    }

    public void OnClickedShopTab()
    {
        State = MenuOpenState.Shop;
        MenuChanage();
    }

    public void OnClickedBonusTab()
    {
        State = MenuOpenState.Bonus;
        MenuChanage();
    }

    public void OnClickedLevelup()
    {
        LocalData_Manager.Get.ChangeLevelUpType();
        SetLevelUpLabel();
        RefreshUpGradeItems();
    }

    void SetLevelUpLabel()
    {
        switch (LocalData_Manager.Get.Levelup_Type)
        {
            case LevelupType.One:
                lb_LevelUp.text = "Level Up x 1";
                break;
            case LevelupType.Max:
                lb_LevelUp.text = "[FBB33AFF]MAX[-]";
                break;
        }
    }
    
    void TweenOpenEnd()
    {
        IsOpen = true;
    }

    void TweenCloseEnd()
    {
        IsOpen = false;
        gameObject.SetActive(false);
    }
    
    public void RefreshUpGradeItems()
    {
        for(int i = 0; i < UpGradeScrollItems.Count; ++i)
            UpGradeScrollItems[i].Refresh();
    }

    public void ReFreshArtifactItems()
    {
        for (int i = 0; i < ArtifactScrollItems.Count; ++i)
            ArtifactScrollItems[i].Refresh();
    }

    public void ReFreshAchievementItems()
    {
        for (int i = 0; i < AchievementScrollItems.Count; ++i)
            AchievementScrollItems[i].Refresh();
    }

    public void ReFreshBonusItems()
    {
        for (int i = 0; i < BonusScrollItems.Count; ++i)
            BonusScrollItems[i].Refresh();
    }

    public void MenuDirectOpen(MenuOpenState type_)
    {
        State = type_;
        MenuChanage();
    }

    public void RefreshRuby()
    {
        if (State == MenuOpenState.Artifact)
        {
            sp_Goods.spriteName = "magicpowder";
            lb_RemainGoods.text = UserData_Manager.Get.MagicDust.ToString();
        }
        else
        {
            sp_Goods.spriteName = "ruby";
            lb_RemainGoods.text = UserData_Manager.Get.Cash_Value.ToString();
        }
    }

}
