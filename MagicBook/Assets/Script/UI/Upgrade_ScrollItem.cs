﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CognitioConsulting.Numerics;

public class Upgrade_ScrollItem : ScrollItem
{
    [SerializeField] LocalizeText Label_Name;
    [SerializeField] UILabel Label_Level;
    [SerializeField] UILabel Label_Cost;
    [SerializeField] LocalizeText Label_Text;
    [SerializeField] LocalizeText Label_Unlock;

    [SerializeField] UISprite sp_BG;
    [SerializeField] UISprite sp_CostBG;
    [SerializeField] GameObject go_Max;

    Table_Manager.UpgradeData data;
    UpgradeType Type = UpgradeType.Attack;

    int UpgradeCount;
    BigDecimal UpgradeCost;

    public override void SetData(object data_)
    {
        Type = (UpgradeType)data_;
        data = Table_Manager.Get.li_UpgradeData[(int)Type];
        Label_Name.ChangeText(data.Name);

        SetLabel();

        base.SetData(data_);
    }

    public override void Refresh()
    {
        SetLabel();
        base.Refresh();
    }


    float CurrentTime = 0.0f;
    private void Update()
    {
        CurrentTime += Time.deltaTime;
        if(CurrentTime >= 1.0f)
        {
            CurrentTime = 0;
            SetLabel();
        }
    }


    void SetLabel()
    {
        //업그레이드 비용 구하기
        //현재 레벨 업그레이드 비용
        UpgradeCount = 1;
        UpgradeCost = Table_Manager.Get.GetUpgradeCost(Type, UserData_Manager.Get.Level_Upgrade[(int)Type]);

        if (UserData_Manager.Get.Level_Upgrade[(int)Type] != 0)
        {
            //맥스 타입이고 , 돈이 현재 업그레이드 비용보다 크면 몇개나 더 업그레이드 가능한지 계산해본다.
            if (LocalData_Manager.Get.Levelup_Type == LevelupType.Max && UpgradeCost < UserData_Manager.Get.MagicStone)
            {
                BigDecimal NextNeed = 0;

                //내가 소모 할 수 있는 최대 파편 갯수
                while (true)
                {
                    //현재 업그레이드 횟수가 최대치에 도달하면 Break
                    if (UserData_Manager.Get.Level_Upgrade[(int)Type] + UpgradeCount >= data.MaxLevel)
                        break;

                    //현재 코스트+1 을 구함
                    NextNeed = Table_Manager.Get.GetUpgradeCost(Type, UserData_Manager.Get.Level_Upgrade[(int)Type] + UpgradeCount);

                    //현재 토탈 + 다음 값 이 현재 마법석 보다 작다 -> 더해도 됨!
                    if (UpgradeCost + NextNeed < UserData_Manager.Get.MagicStone)
                    {
                        UpgradeCost += NextNeed;
                        UpgradeCount++;
                    }
                    //같거나 크면 끝내야됨...
                    else
                    {
                        break;
                    }
                }
            }
        }
            
        //최대 레벨 체크를 버튼에서도 해서 없어도 되지만..레이블 셋팅값을 0으로 바꾸기 위해서 넣어둠
        if (UserData_Manager.Get.Level_Upgrade[(int)Type] >= data.MaxLevel)
        {
            UpgradeCount = 0;
            UpgradeCost = 0;
        }

        //현재 레벨
        Label_Level.text = string.Format("Lv.[Fbb33a]{0}[-]", UserData_Manager.Get.Level_Upgrade[(int)Type]);

        //go_Max 최대 레벨 도달시Max 오브젝트 활성화
        go_Max.SetActive(UserData_Manager.Get.Level_Upgrade[(int)Type] >= data.MaxLevel);

        //각 레이블 셋팅
        Label_Cost.text = NumericalFormatter.Format(UpgradeCost);

        //Label_Text ,sp_BG 설정 아직 오픈 안한 컨텐츠
        if (UserData_Manager.Get.Level_Upgrade[(int)Type] == 0)
        {
            sp_BG.color = UtilityTools.HexToColor("BAABDBB2");

            switch (data.UnlockType)
            {
                case UpgradeUnlock.None:
                    Label_Text.ChangeText(223);
                    break;
                default:
                    Label_Text.ChangeText(223 + (int)data.UnlockType, data.UnlockValue);
                    break;
            }

            Label_Unlock.ChangeText(11);
        }
        //오픈 한 컨텐츠
        else
        {
            sp_BG.color = UtilityTools.HexToColor("6D5B97B2");

            if (Type == UpgradeType.Attack)
                Label_Text.ChangeText(data.Desc, NumericalFormatter.Format(UserData_Manager.Get.NowAttack));
            else
                Label_Text.ChangeText(data.Desc, UserData_Manager.Get.NowValue[(int)Type]);
            
            Label_Unlock.ChangeText(55, UpgradeCount);
        }

        //Label_Cost 의 알파값 적용 하기
        if (UserData_Manager.Get.UpgradeConditionCheck(Type) == false || UpgradeCost > UserData_Manager.Get.MagicStone)
            Label_Cost.alpha = 0.5f;
        else
            Label_Cost.alpha = 1.0f;

    }
    
    public void OnClickedUpgradeBtn()
    {
        UserData_Manager.Get.LevelupUpgrade(Type, UpgradeCount, UpgradeCost);
    }
}
