﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CognitioConsulting.Numerics;

public class EnemyInfoUI : MonoBehaviour
{
    [SerializeField] GameObject go_HP;
    [SerializeField] UISprite sp_HP_Slide;
    [SerializeField] LocalizeText lt_Name;

    public void SetUI(string _Name , float _posY, bool _isHP = true)
    {
        lt_Name.ChangeText(_Name);
        go_HP.SetActive(_isHP);
        this.transform.localPosition = new Vector3(this.transform.localPosition.x, _posY, this.transform.localPosition.z);

    }
    
    public void SetHP(BigDecimal _value)
    {
        sp_HP_Slide.fillAmount = float.Parse(_value.ToString());
    }
}
