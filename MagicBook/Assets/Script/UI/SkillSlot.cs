﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillSlot : MonoBehaviour 
{
    bool isOpen = false;
    public UISprite sp_Icon;
    public UILabel lb_Timer;
    public UILabel lb_ManaCost;

    UpgradeType type;
    float coolTime = 0.0f;

    public void Init(UpgradeType _type)
    {
        type = _type;

        lb_ManaCost.text = Ingame_Manager.Get.skill.GetManaCost(_type).ToString();

        ResetData();
        Refresh();
    }


    private void Update()
    {
        if (coolTime > 0)
        {
            coolTime -= Time.deltaTime;
            lb_Timer.text = coolTime.ToString("F1");

            if (coolTime <= 0)
            {
                ResetData();
            }
        }
    }

    public void Refresh()
    {        
        //스킬이 열려있는지 체크 후 갱신
        if (UserData_Manager.Get.Level_Upgrade[(int)type] > 0)
        {
            isOpen = true;
            sp_Icon.spriteName = string.Format("icon_{0}", type.ToString());
        }
        else
        {
            isOpen = false;
            sp_Icon.spriteName = string.Format("icon_{0}_Lock", type.ToString());
        }
    }

    public bool CheckCoolTIme()
    {
        if (coolTime != 0 || isOpen == false)
        {
            Debug.Log("스킬 사용 실패-쿨타임이거나 아직 열지 않은 스킬");
            return false;
        }

        return true;
    }

    bool CheckMana()
    {
        int mana = 0;
        switch (type)
        {
            case UpgradeType.Meteor:
            case UpgradeType.AquaLaser:
            case UpgradeType.LeafStrom:
                mana = ConstValue.ManaCost_AttackSpell;
                break;
            case UpgradeType.SummonFire:
            case UpgradeType.SummonWater:
            case UpgradeType.SummonEarth:
                mana = ConstValue.ManaCost_SummonSpell;
                break;

        }

        //마나소모 체크
        if (LocalData_Manager.Get.Mana < mana)
        {
            //Debug.LogFormat("마나가 부족합니다. 현재마나:{0} 필요마나:{1}", LocalData_Manager.Get.Mana, mana);
            UI_Manager.Get.op_NoticeText.ShowText(258);
            return false;
        }

        
        //마나 소모
        LocalData_Manager.Get.AddMana(-mana);

        return true;
    }
    public void UseSKill()
    {
        if (CheckCoolTIme() == false)
            return;

        if (CheckMana() == false)
            return;

        CastSpell();
    }

    public void CastSpell()
    {
        switch (type)
        {
            case UpgradeType.Meteor:
                coolTime = Ingame_Manager.Get.skill.CastAttackSpell(MagicElement.Fire);
                break;
            case UpgradeType.SummonFire:
                coolTime = Ingame_Manager.Get.skill.SummonSpirit(MagicElement.Fire);
                break;
            case UpgradeType.AquaLaser:
                coolTime = Ingame_Manager.Get.skill.CastAttackSpell(MagicElement.Water);
                break;
            case UpgradeType.SummonWater:
                coolTime = Ingame_Manager.Get.skill.SummonSpirit(MagicElement.Water);
                break;
            case UpgradeType.LeafStrom:
                coolTime = Ingame_Manager.Get.skill.CastAttackSpell(MagicElement.Earth);
                break;
            case UpgradeType.SummonEarth:
                coolTime = Ingame_Manager.Get.skill.SummonSpirit(MagicElement.Earth);
                break;
        }
        
        sp_Icon.alpha = 0.5f;

    }

    void ResetData()
    {
        coolTime = 0;
        lb_Timer.text = "";
        sp_Icon.alpha = 1;
    }
}
