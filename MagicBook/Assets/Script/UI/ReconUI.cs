﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReconUI : MonoBehaviour 
{
    [SerializeField] UISprite sp_Filled;
    public UISprite sp_GetButton;
    [SerializeField] UISprite sp_Step1;
    [SerializeField] UISprite sp_Step2;

    public void SetFilledSprite(float _value)
    {
        sp_Filled.fillAmount = _value;

        //비활성화
        if (_value < 0.333f)
        {
            //기본 이미지로 변경
            sp_Step1.spriteName = "gage_point_l";
            sp_Step2.spriteName = "gage_point_l";
            sp_GetButton.spriteName = "btn01_l";
        }
        //1차보상
        else if (_value < 0.666f)
        {
            sp_Step1.spriteName = "gage_point";
            sp_Step2.spriteName = "gage_point_l";
            sp_GetButton.spriteName = "btn01";
        }
        //2차보상 
        else if (_value < 1)
        {
            sp_Step1.spriteName = "gage_point";
            sp_Step2.spriteName = "gage_point";
            sp_GetButton.spriteName = "btn01";
        }
        //3차보상 
        else
        {
            sp_Step1.spriteName = "gage_point";
            sp_Step2.spriteName = "gage_point";
            sp_GetButton.spriteName = "btn02";
        }

    }

    public void OnClick_ReconReward()
    {
        Ingame_Manager.Get.GetReconReward();
    }
}
