﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillBar : MonoBehaviour
{
    public SkillSlot[] skillSlot = new SkillSlot[6];

    public void Init()
    {
        for (int i=0;i< 6; i++)
        {
            skillSlot[i].Init(UpgradeType.Meteor + i);
        }
    }

    public void Refresh()
    {
        for (int i = 0; i < 6; i++)
        {
            skillSlot[i].Refresh();
        }
    }
}
