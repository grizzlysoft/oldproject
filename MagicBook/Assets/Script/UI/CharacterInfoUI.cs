﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInfoUI : MonoBehaviour 
{
    [SerializeField] UISprite sp_Mana_Slide;
    [SerializeField] UILabel lb_Mana;

    public void SetManaBar()
    {
        float _value = (float)LocalData_Manager.Get.Mana / UserData_Manager.Get.NowValue[(int)UpgradeType.ManaMax];
        sp_Mana_Slide.fillAmount = _value;
        lb_Mana.text = string.Format("{0}/{1}", LocalData_Manager.Get.Mana,UserData_Manager.Get.NowValue[(int)UpgradeType.ManaMax]);
    }

}
