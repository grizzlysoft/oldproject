﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CognitioConsulting.Numerics;

public class Bonus_ScrollItem : ScrollItem
{
    public LocalizeText Label_Name;
    public LocalizeText Label_Text;
    public UILabel Label_Cost;
    public UILabel Label_Time;
    public UILabel Label_Watch;
    public UISprite Sprite_Icon;

    Table_Manager.BonusData data;
    BonusType Type;

    public override void SetData(object data_)
    {
        Type = (BonusType)data_;
        data = Table_Manager.Get.li_BonusData[(int)Type];

        Sprite_Icon.spriteName = string.Format("Perk_{0}",(int)Type);

        //특전 이름
        Label_Name.ChangeText(data.Name);

        SetLabel();

        base.SetData(data_);
    }

    public override void Refresh()
    {
        SetLabel();

        base.Refresh();
    }

    public void SetLabel()
    {
        //s = NumericalFormatter.Format(MathParser.Calculate(Table_Manager.Get.EtcRewardData.V2_Modify, UserData_Manager.Get.Stage));
        switch (Type)
        {
            case BonusType.GetMagicStone:
                //2000*(1.03^level)*1000
                Label_Text.ChangeText(data.TextID, NumericalFormatter.Format( FormulaDefine.PerkMagicStone() )  );
                break;
            case BonusType.GetMagicStone_Big:
                //2000*(1.03^level)*100000
                Label_Text.ChangeText(data.TextID, NumericalFormatter.Format( FormulaDefine.PerkBigMagicStone() )  );
                break;
            default:
                Label_Text.ChangeText(data.TextID);
                break;
        }

        //남은 시간 체크
        if (LocalData_Manager.Get.GetPerkTime(Type) > 0)
        {
            Label_Time.text = LocalData_Manager.Get.GetPerkTimeString(Type);
            Label_Time.gameObject.SetActive(true);

            Label_Cost.gameObject.SetActive(false);
            Label_Watch.gameObject.SetActive(false);
        }
        else
        {
            Label_Time.gameObject.SetActive(false);

            //남은 시간이 없으면 종류에 따라 바꿔줌
            //비용 설정(없으면 0)-> 광고 , 버튼 상태는 아래서 따로 관리
            if (data.Cost == 0)
            {
                Label_Cost.gameObject.SetActive(false);
                Label_Watch.gameObject.SetActive(true);
            }
            else
            {
                Label_Cost.text = data.Cost.ToString();

                Label_Cost.gameObject.SetActive(true);
                Label_Watch.gameObject.SetActive(false);
            }
        }
    }
    
    public void OnClickedUse()
    {
        UserData_Manager.Get.UseBonus(Type);
    }
}
