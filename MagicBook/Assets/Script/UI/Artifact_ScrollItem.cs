﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CognitioConsulting.Numerics;

public class Artifact_ScrollItem : ScrollItem
{
    [SerializeField] LocalizeText Label_Name;
    [SerializeField] LocalizeText Label_Text;
    [SerializeField] LocalizeText Label_Unlock;
    [SerializeField] UILabel Label_Level;
    [SerializeField] UILabel Label_Cost;
    [SerializeField] UISprite Sprite_Icon;
    [SerializeField] UISprite sp_BG;
    [SerializeField] GameObject go_Max;

    Table_Manager.ArtifactData data;
    ArtifactType Type;

    int NextCost = 0;

    public override void SetData(object data_)
    {
        Type = (ArtifactType)data_;
        data = Table_Manager.Get.li_ArtifactData[(int)Type];

        Sprite_Icon.spriteName = string.Format("item{0}",(int)Type);

        //이름
        Label_Name.ChangeText(data.Name);

        SetLabel();

        base.SetData(data_);
    }

    public override void Refresh()
    {
        SetLabel();

        base.Refresh();
    }

    public void SetLabel()
    {
        Label_Level.text = string.Format("Lv.[Fbb33a]{0}[-]", UserData_Manager.Get.Level_Artifact[(int)Type]);

        //Label_Text ,sp_BG 설정 아직 오픈 안한 컨텐츠
        if (UserData_Manager.Get.Level_Artifact[(int)Type] == 0)
        {
            sp_BG.color = UtilityTools.HexToColor("BAABDBB2");
            NextCost = data.UnlockCost;
            Label_Text.ChangeText(223);
            Label_Unlock.ChangeText(10);
        }
        //오픈 한 컨텐츠
        else
        {
            sp_BG.color = UtilityTools.HexToColor("6D5B97B2");
            Label_Text.ChangeText(data.Desc, UserData_Manager.Get.NowValue_Artifact[(int)Type].ToString("F2"));
            NextCost = data.UpgradeCost * UserData_Manager.Get.Level_Artifact[(int)Type];
            Label_Unlock.ChangeText(1);
        }
        Label_Cost.text = NextCost.ToString();

        //go_Max 최대 레벨 도달시Max 오브젝트 활성화
        go_Max.SetActive(UserData_Manager.Get.Level_Artifact[(int)Type] >= data.MaxLevel);

        //Label_Cost 의 알파값 적용 하기
        if (UserData_Manager.Get.Level_Artifact[(int)Type] >= data.MaxLevel || NextCost > UserData_Manager.Get.MagicDust)
            Label_Cost.alpha = 0.5f;
        else
            Label_Cost.alpha = 1.0f;

    }

    public void OnClick_Buy()
    {
        UserData_Manager.Get.LevelupArtifact(Type, 1, NextCost);
    }
}
