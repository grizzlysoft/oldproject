﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop_ScrollItem : ScrollItem
{
    [SerializeField] LocalizeText Label_Name;
    [SerializeField] LocalizeText Label_BuyButton;
    [SerializeField] UISprite sp_Icon;
    [SerializeField] LocalizeText lb_Price;

    Table_Manager.ShopData Data;

    public override void SetData(object data_)
    {
        Data = data_ as Table_Manager.ShopData;

        Label_Name.ChangeText(Data.Name);

        //구매 여부에 따른 단어 변경
        if (Data.PayType == PayMentType.Ads)
            Label_BuyButton.ChangeText(14);
        else
            Label_BuyButton.ChangeText(10);

        //이미지
        sp_Icon.spriteName = string.Format("shop{0}",Data.ID + 1);

        //가격
        lb_Price.ChangeText(Data.Cost);

        base.SetData(data_);
    }

    public override void Refresh()
    {
        base.Refresh();
    }

    public void OnClickedBuy()
    {
        Sound_Manager.Get.PlayEffect("Effect_click");
        PaymentManager.Get.BuyPrice(Data);
    }
}
