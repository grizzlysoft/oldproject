﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Achievement_ScrollItem : ScrollItem
{
    public LocalizeText Label_Name;
    public UILabel Label_RewardRuby;
    public UILabel Label_ProgressText;
    public UIProgressBar ProgressBar;
    public GameObject go_PossibleReward;
    public GameObject go_Complete;

    AchievementType Type;

    public override void SetData(object data_)
    {
        Type = (AchievementType)data_;

        SetLabel();

        base.SetData(data_);
    }

    public override void Refresh()
    {
        SetLabel();

        base.Refresh();
    }

    void SetLabel()
    {
        try
        {
            Table_Manager.AchievementData data = Table_Manager.Get.li_AchievementData.Find(r => r.ID == UserData_Manager.Get.NowAchievementID[(int)Type]);

            //레이블 로컬라이징 변화
            Label_Name.ChangeText(data.Name);

            //리워드 데이터 찾기 
            Table_Manager.RewardData findReward = Table_Manager.Get.li_RewardData.Find(r => r.ID == data.RewardID);
            if (findReward == null)
            {
                Debug.LogError("찾는 리워드 없음");
                Label_RewardRuby.text = "";
            }

            //보상 개수 입력
            Label_RewardRuby.text = findReward.Count.ToString();

            //마지막 업적일 경우~
            if (data.NextID == 0 && UserData_Manager.Get.AchievementClear[data.ID] == true)
            {
                go_Complete.SetActive(true);
                Label_RewardRuby.alpha = 0.5f;
                go_PossibleReward.SetActive(false);
            }
            //업적 미완료
            else if (UserData_Manager.Get.AchievementValue[(int)Type] < data.Clear_Value)
            {
                go_Complete.SetActive(false);
                Label_RewardRuby.alpha = 0.5f;
                go_PossibleReward.SetActive(false);
            }
            //업적 완료-보상 수락 가능 상태
            else
            {
                go_Complete.SetActive(false);
                Label_RewardRuby.alpha = 1.0f;
                go_PossibleReward.SetActive(true);
            }

            Label_ProgressText.text = UserData_Manager.Get.AchievementValue[(int)Type] + "/" + data.Clear_Value;

            ProgressBar.value = (float)UserData_Manager.Get.AchievementValue[(int)Type] / data.Clear_Value;
        }
        catch (NullReferenceException ex)
        {
            Debug.LogError("여기 널이 있다! : " + ex);
        }
        
    }

    public void OnClickedComplete()
    {
        UserData_Manager.Get.ClearAchievment(Type);
    }
}
