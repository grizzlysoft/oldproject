﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Special : MonoBehaviour 
{
    [SerializeField] SpriteRenderer sp_IMG;
    [SerializeField] Animator anim;

    public virtual void ChangeSprite(SpecialDungeonType _type)
    {
        sp_IMG.sprite = Resources.Load(string.Format("Enemy/Puppet_{0}", _type.ToString()), typeof(Sprite)) as Sprite;
    }

    public void Hit()
    {
        anim.SetTrigger("Hit");
    }

    public void Die()
    {
        anim.SetTrigger("Die");
    }

    public void Show()
    {
        this.gameObject.SetActive(true);
    }

    //애니메이션에서 제어
    public void Hide()
    {
        this.gameObject.SetActive(false);
    }
}
