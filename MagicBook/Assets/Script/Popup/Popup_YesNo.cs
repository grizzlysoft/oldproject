﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_YesNo : Popup 
{
    [SerializeField] LocalizeText lb_Text;

    Del_Void del_Yes;
    Del_Void del_No;

    public void Show(string _Desc , Del_Void _del_Yes , Del_Void _del_No = null)
    {
        lb_Text.ChangeText(_Desc);

        del_Yes = _del_Yes;
        del_No = _del_No;

        base.Show();
    }

    public override void PressBackButton()
    {
        base.PressBackButton();
        del_No?.Invoke();
    }

    public void OnClick_Yes()
    {
        OnClick_Hide();
        del_Yes?.Invoke();
    }
    public void OnClick_No()
    {
        OnClick_Hide();
        del_No?.Invoke();
    }
}
