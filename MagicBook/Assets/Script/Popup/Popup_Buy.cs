﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Buy : Popup 
{
    [SerializeField] LocalizeText lb_Text;
    [SerializeField] UISprite sp_Icon;
    [SerializeField] UILabel lb_Cost;

    Del_Void del;

    public void Show(string _Desc, string _Icon , string _Cost , Del_Void _del)
    {
        lb_Text.ChangeText(_Desc);
        sp_Icon.spriteName = _Icon;
        lb_Cost.text = _Cost;

        del = _del;

        base.Show();
    }

    public void OnClick_Yes()
    {
        OnClick_Hide();

        del?.Invoke();
    }

}
