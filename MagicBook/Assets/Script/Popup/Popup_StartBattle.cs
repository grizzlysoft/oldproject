﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_StartBattle : Popup 
{
    [SerializeField] Animation anim;
    [SerializeField] GameObject Ready;
    [SerializeField] GameObject Start;

    bool isEndAnim = false;

    public override void Show()
    {
        Ready.SetActive(false);
        Start.SetActive(false);
        isEndAnim = false;

        isBackButtonClose = false;
        base.Show();
    }

    public IEnumerator AnimStart()
    {
        anim.Play("StartBattle@Start");
        while (true)
        {
            if (isEndAnim == true)
                break;

            yield return null;
        }

        yield return null;
    }

    public void SetEndAnim()
    {
        isEndAnim = true;
    }
}
