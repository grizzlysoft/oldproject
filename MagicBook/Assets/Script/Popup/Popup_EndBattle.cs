﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CognitioConsulting.Numerics;

public class Popup_EndBattle : Popup 
{
    [SerializeField] UILabel lb_Cost;
    [SerializeField] UISprite sp_Icon;
    [SerializeField] LocalizeText lb_Desc;

    RewardType rewardType;
    BigDecimal rewardValue;

    //전투 성공 팝업
    public void Show(SpecialDungeonType _type , RewardType _rewardType , BigDecimal _rewardValue)
    {
        isBackButtonClose = false;

        rewardType = _rewardType;
        rewardValue = _rewardValue;

        switch (_type)
        {
            case SpecialDungeonType.BloodMoon:
                lb_Desc.ChangeText(281);
                break;
            case SpecialDungeonType.FullMoon:
                lb_Desc.ChangeText(320);
                break;
        }

        switch (rewardType)
        {
            case RewardType.Ruby:
                sp_Icon.spriteName = "ruby";
                break;
            case RewardType.MagicStone:
                sp_Icon.spriteName = "mscrap";
                break;
            case RewardType.MagicDust:
                sp_Icon.spriteName = "magicpowder";
                break;
        }

        string reward = NumericalFormatter.Format(rewardValue);
        lb_Cost.text = string.Format("{0} + [5567CA]{1}[-]", reward, reward);
        
        base.Show();
    }

    public void OnClick_AD()
    {
        OnClick_Hide();
        Ads_Manager.Get.ShowAd_UnityAds(ShowAdType.DoubleReward, -1);
    }

    public void OnClick_Confirm()
    {
        OnClick_Hide();
        GetReward(1);
    }

    public void GetReward(int _multiply)
    {
        UserData_Manager.Get.AddReward(rewardType, rewardValue * _multiply);
        //스테이지 ++
        UserData_Manager.Get.StepUpStage_FullMoon();
        Ingame_Manager.Get.ChangeNormalStage();
    }
}
