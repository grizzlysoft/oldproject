﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Option : Popup
{
    [SerializeField] GameObject go_Option;
    [SerializeField] GameObject go_MakePeople;
    [SerializeField] LocalizeText lt_Sound;
    [SerializeField] LocalizeText lt_Push;
    [SerializeField] LocalizeText lt_Language;
    [SerializeField] UIInput ip_Coupon;
    
    string st_CouponCode = "";

    //옵션 버튼 클릭 이벤트들
    public override void Show()
    {
        if (Sound_Manager.Get.BGM_Volume == 1.0f)
            lt_Sound.ChangeText(80);//사운드 On
        else
            lt_Sound.ChangeText(81);//사운드 Off

        if (LocalData_Manager.Get.isPushAlarm == 1)
            lt_Push.ChangeText(82);//푸시 On
        else
            lt_Push.ChangeText(83);//푸시 Off


        if(LocalData_Manager.Get.SelectLanguage == Language.Kor)
            lt_Language.ChangeText(86, LM.GetText(89));//언어 한국어
        else if (LocalData_Manager.Get.SelectLanguage == Language.Jp)
            lt_Language.ChangeText(86, LM.GetText(90));//언어 일본어

        st_CouponCode = "";
        ip_Coupon.value = st_CouponCode;

        go_Option.SetActive(true);
        go_MakePeople.SetActive(false);

        base.Show();
    }

    public void ShowOptionMenu()
    {
        Sound_Manager.Get.PlayEffect("Effect_click");

        go_Option.SetActive(true);
        go_MakePeople.SetActive(false);
    }

    public void OnClick_Sound()
    {

        if (Sound_Manager.Get.BGM_Volume == 1.0f)
        {
            Sound_Manager.Get.PlayEffect("Effect_click");

            Sound_Manager.Get.SetVolume_BGM(0);
            Sound_Manager.Get.SetVolume_Effect(0);
            lt_Sound.ChangeText(81);//사운드 Off
        }
        else
        {
            Sound_Manager.Get.SetVolume_BGM(1);
            Sound_Manager.Get.SetVolume_Effect(1);
            lt_Sound.ChangeText(80);//사운드 On

            Sound_Manager.Get.PlayEffect("Effect_click");
        }
    }

    public void OnClick_Push()
    {
        Sound_Manager.Get.PlayEffect("Effect_click");

        if (LocalData_Manager.Get.isPushAlarm == 1)
        {
            LocalData_Manager.Get.SetPushAlarm(0);
            lt_Push.ChangeText(83);//푸시 Off
        }
        else
        {
            LocalData_Manager.Get.SetPushAlarm(1);
            lt_Push.ChangeText(82);//푸시 On
        }
    }

    public void OnClick_Language()
    {
        Sound_Manager.Get.PlayEffect("Effect_click");

        if (LocalData_Manager.Get.SelectLanguage == Language.Kor)
        {
            LocalData_Manager.Get.ChanageLanguage(Language.Jp);
            lt_Language.ChangeText(86, LM.GetText(90));//언어 일본어
        }
        else if (LocalData_Manager.Get.SelectLanguage == Language.Jp)
        {
            LocalData_Manager.Get.ChanageLanguage(Language.Kor);
            lt_Language.ChangeText(86, LM.GetText(89));//언어 한국어
        }
    }

    public void OnClick_Coupon()
    {
        Sound_Manager.Get.PlayEffect("Effect_click");
        st_CouponCode = ip_Coupon.value;
        Debug.Log("쿠폰 코드 : " + st_CouponCode);
        CouponManager.Get.UseHungryAppCoupon(st_CouponCode);
    }

    public void OnClick_Creator()
    {
        Sound_Manager.Get.PlayEffect("Effect_click");

        go_Option.SetActive(false);
        go_MakePeople.SetActive(true);
    }

    public void OnClick_Review()
    {
        Sound_Manager.Get.PlayEffect("Effect_click");
        Application.OpenURL("market://details?id=com.GrizzlySoft.MagicBook");

    }

    public void OnClick_Blog()
    {
        Sound_Manager.Get.PlayEffect("Effect_click");
        Application.OpenURL("https://blog.naver.com/grizzlysoft");
    }
}
