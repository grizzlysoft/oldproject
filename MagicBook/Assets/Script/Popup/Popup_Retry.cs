﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CognitioConsulting.Numerics;

public class Popup_Retry : Popup 
{
    [SerializeField] LocalizeText lb_Desc;
    [SerializeField] GameObject btn_AD;
    [SerializeField] GameObject btn_Confirm;

    //전투 성공 팝업
    public void Show(int _battleCount , BigDecimal _Hp)
    {
        isBackButtonClose = false;

        string HP = NumericalFormatter.Format(_Hp);
        if (_battleCount == 0)
        {
            lb_Desc.ChangeText(321, HP);
            btn_Confirm.transform.localPosition = new Vector3(-105, btn_Confirm.transform.localPosition.y, 0);
            btn_AD.SetActive(true);
        }
        else
        {
            lb_Desc.ChangeText(322, HP);
            btn_Confirm.transform.localPosition = new Vector3(0, btn_Confirm.transform.localPosition.y, 0);
            btn_AD.SetActive(false);
        }

        base.Show();
    }
    
    public void OnClick_AD()
    {
        OnClick_Hide();
        //Do Something
        Ads_Manager.Get.ShowAd_UnityAds(ShowAdType.Retry, -1);
    }

    public void OnClick_Confirm()
    {
        OnClick_Hide();
        Ingame_Manager.Get.ChangeNormalStage();
    }
}
