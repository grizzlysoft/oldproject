﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Check : Popup 
{
    [SerializeField] LocalizeText lb_Text;
    Del_Void del;

    public void Show(string _Desc, Del_Void _del = null , bool _isBackButton = true)
    {
        isBackButtonClose = _isBackButton;
        lb_Text.ChangeText(_Desc);
        del = _del;
        base.Show();
    }

    public void OnClick_Check()
    {
        OnClick_Hide();

        //if(del != null) del(); 이거 압축버전
        del?.Invoke();

    }
}
