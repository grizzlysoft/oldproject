﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_SpecialDungeon : Popup 
{
    [SerializeField] UILabel lb_Ticket;
    [SerializeField] UIPanel subPanel;
    [SerializeField] UIScrollView scrollview;
    [SerializeField] UIGrid grid;
    [SerializeField] GameObject go_Content;

    List<SpecialDungeonContent> li_Content = new List<SpecialDungeonContent>();

    public override void Init()
    {
        GameObject go;
        SpecialDungeonContent tempScp;

        for (int i = 0; i < Table_Manager.Get.li_SpecialDungeonData.Count; i++)
        {
            go = Instantiate(go_Content, grid.transform) as GameObject;
            tempScp = go.GetComponent<SpecialDungeonContent>();
            tempScp.Init(this, Table_Manager.Get.li_SpecialDungeonData[i]);
            li_Content.Add(tempScp);
        }
        
        base.Init();
    }

    public override void Show()
    {
        LocalData_Manager.Get.RefreshTicket();
        Refresh();
        subPanel.depth = Popup_Manager.Get.current_depth + 1;

        base.Show();

        grid.Reposition();
        scrollview.ResetPosition();
    }

    public override void Refresh()
    {
        lb_Ticket.text = string.Format("{0}/{1}", LocalData_Manager.Get.Ticket, ConstValue.Max_Ticket);
    }

}
