﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_FadeInOut : Popup 
{
    [SerializeField] UISprite sp_Main;

    public override void Show()
    {
        isBackButtonClose = false;
        base.Show();
    }

    //페이드 인 
    public IEnumerator FadeIn()
    {
        sp_Main.alpha = 0;

        while (true)
        {
            sp_Main.alpha += 0.02f;
            if (sp_Main.alpha >= 1)
            {
                sp_Main.alpha = 1;
                break;
            }
            yield return new WaitForSeconds(0.01f);
        }

        yield return null;
    }
    //페이드 아웃
    public IEnumerator FadeOut()
    {
        sp_Main.alpha = 1;

        while (true)
        {
            sp_Main.alpha -= 0.02f;
            if (sp_Main.alpha <= 0)
            {
                sp_Main.alpha = 0;
                break;
            }
            yield return new WaitForSeconds(0.01f);
        }

        yield return null;
    }
}
