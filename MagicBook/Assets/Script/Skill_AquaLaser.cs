﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_AquaLaser : MonoBehaviour
{
    private void Awake()
    {
        Sound_Manager.Get.PlayEffect("Effect_freeze");
    }

    public void Damaged()
    {
        //Sound_Manager.Get.PlayEffect("Effect_normalhit");
        Ingame_Manager.Get.enemy.HitEnemy(Ingame_Manager.Get.Calc_MagicSpellDamage(MagicElement.Water));
    }

    public void DestroyThis()
    {
        Destroy(this.gameObject);
    }
}
