﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AD_Fairy : MonoBehaviour
{
    public float Move_X;
    public float Move_Y;

    public float Speed_X;
    public float Speed_Y;

    ShowAdType Type;

    float LifeTime = 20;

    private void Start()
    {
        this.transform.localPosition = new Vector3(-Move_X, 0, 0);
    }

    private void Update()
    {
        if(LifeTime > 0)
        {
            LifeTime -= Time.deltaTime;
            if(LifeTime <= 0)
            {
                Destroy(this.gameObject);
                return;
            }

            // -> 으로 갈때
            if (Speed_X > 0 && this.transform.localPosition.x >= Move_X)
            {
                Speed_X *= -1;
                this.transform.localScale = new Vector3(-1, 1, 1);
            }
            // <- 으로 갈때
            else if (Speed_X <= 0 && this.transform.localPosition.x <= -Move_X)
            {
                Speed_X *= -1;
                this.transform.localScale = new Vector3(1, 1, 1);
            }

            // ↓으로 갈때
            if (Speed_Y > 0 && this.transform.localPosition.y >= Move_Y)
                Speed_Y *= -1;
            // ↑으로 갈때
            else if (Speed_Y <= 0 && this.transform.localPosition.y <= -Move_Y)
                Speed_Y *= -1;

            this.transform.Translate(Speed_X * Time.deltaTime, Speed_Y * Time.deltaTime, 0);
        }
    }

    void CalcReward()
    {
        //어떤 보상을 주는 요정인지 판단 - 랜덤
        //획득 가능한 보상 리스트 작성 
        List<ShowAdType> li_Reward = new List<ShowAdType>();
        li_Reward.Add(ShowAdType.Ruby);  //0. 루비
        li_Reward.Add(ShowAdType.MagicDust);  //마법 가루
        li_Reward.Add(ShowAdType.Bonus_GetMagicStone);  //1. 특전 - 파편 소 획득

        if (LocalData_Manager.Get.GetPerkTime(BonusType.Focus) == 0)
            li_Reward.Add(ShowAdType.Bonus_GetFocus);  //2. 특전 - 집중 모드

        if (LocalData_Manager.Get.GetPerkTime(BonusType.DamagePlus) == 0)
            li_Reward.Add(ShowAdType.Bonus_GetDamage);  //3. 특전 - 피해량 증가 소

        if (UserData_Manager.Get.Level_Upgrade[(int)UpgradeType.Meteor] > 0 && UI_Manager.Get.Skillbar.skillSlot[0].CheckCoolTIme() == true)
            li_Reward.Add(ShowAdType.Skill_Meteor);

        if (UserData_Manager.Get.Level_Upgrade[(int)UpgradeType.AquaLaser] > 0 && UI_Manager.Get.Skillbar.skillSlot[2].CheckCoolTIme() == true)
            li_Reward.Add(ShowAdType.Skill_AquaLaser);

        if (UserData_Manager.Get.Level_Upgrade[(int)UpgradeType.LeafStrom] > 0 && UI_Manager.Get.Skillbar.skillSlot[4].CheckCoolTIme() == true)
            li_Reward.Add(ShowAdType.Skill_LeafStrom);

        if (UserData_Manager.Get.Level_Upgrade[(int)UpgradeType.SummonFire] > 0 && UI_Manager.Get.Skillbar.skillSlot[1].CheckCoolTIme() == true)
            li_Reward.Add(ShowAdType.Skill_SummonFire);

        if (UserData_Manager.Get.Level_Upgrade[(int)UpgradeType.SummonWater] > 0 && UI_Manager.Get.Skillbar.skillSlot[3].CheckCoolTIme() == true)
            li_Reward.Add(ShowAdType.Skill_SummonWater);

        if (UserData_Manager.Get.Level_Upgrade[(int)UpgradeType.SummonEarth] > 0 && UI_Manager.Get.Skillbar.skillSlot[5].CheckCoolTIme() == true)
            li_Reward.Add(ShowAdType.Skill_SummonEarth);


        //랜덤 보상 얻기
        Type = li_Reward[Random.Range(0, li_Reward.Count)];
    }

    string GetRewardText()
    {
        switch (Type)
        {
            case ShowAdType.Ruby:
                return string.Format(LM.GetText(266), LM.GetText(58));
            case ShowAdType.MagicDust:
                return string.Format("{1} " + LM.GetText(266), LM.GetText(316) , ConstValue.MagicDust_Price);
            case ShowAdType.Bonus_GetMagicStone:
                return LM.GetText(53, NumericalFormatter.Format(FormulaDefine.PerkMagicStone()));
            case ShowAdType.Bonus_GetFocus:
                return string.Format(LM.GetText(265), LM.GetText(47));
            case ShowAdType.Bonus_GetDamage:
                return string.Format(LM.GetText(265), LM.GetText(48));
            case ShowAdType.Skill_Meteor:
                return string.Format(LM.GetText(265), LM.GetText(34));
            case ShowAdType.Skill_SummonFire:
                return string.Format(LM.GetText(265), LM.GetText(35));
            case ShowAdType.Skill_AquaLaser:
                return string.Format(LM.GetText(265), LM.GetText(36));
            case ShowAdType.Skill_SummonWater:
                return string.Format(LM.GetText(265), LM.GetText(37));
            case ShowAdType.Skill_LeafStrom:
                return string.Format(LM.GetText(265), LM.GetText(38));
            case ShowAdType.Skill_SummonEarth:
                return string.Format(LM.GetText(265), LM.GetText(39));
            default:
                break;
        }

        return "BUG";
    }

    string GetRewardIcon()
    {
        switch (Type)
        {
            case ShowAdType.Ruby:
                return "ruby";
            case ShowAdType.MagicDust:
                return "magicpowder";
            case ShowAdType.Bonus_GetMagicStone:
                return "Perk_0";
            case ShowAdType.Bonus_GetFocus:
                return "Perk_1";
            case ShowAdType.Bonus_GetDamage:
                return "Perk_2";
            case ShowAdType.Skill_Meteor:
                return "icon_Meteor";
            case ShowAdType.Skill_SummonFire:
                return "icon_SummonFire";
            case ShowAdType.Skill_AquaLaser:
                return "icon_AquaLaser";
            case ShowAdType.Skill_SummonWater:
                return "icon_SummonWater";
            case ShowAdType.Skill_LeafStrom:
                return "icon_LeafStrom";
            case ShowAdType.Skill_SummonEarth:
                return "icon_SummonEarth";
            default:
                break;
        }

        return "BUG";
    }

    public void OnClick_Fairy()
    {
        CalcReward();
        Popup_Manager.Get.Buy.Show(LM.GetText(263), GetRewardIcon(), GetRewardText(), AD_Result);

        //페어리 파괴
        //페어리 부수면 아래 ADResult 가 작동하긴 하니?... 하더라
        Destroy(this.gameObject);
    }

    public void AD_Result()
    {
        switch (Type)
        {
            case ShowAdType.Ruby:
                Ads_Manager.Get.ShowAd_UnityAds(ShowAdType.Ruby, 0);
                break;
            default:
                Ads_Manager.Get.ShowAd_UnityAds(Type, -1);
                break;
        }
    }
}
