﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_Meteor : MonoBehaviour 
{
    [SerializeField] List<MeteorFragment> li_Fragment = new List<MeteorFragment>();
    public float Speed = 30;

    private void Awake()
    {
        for (int i = 0; i < li_Fragment.Count; i++)
        {
            li_Fragment[i].SetParent(this);
        }
        Sound_Manager.Get.PlayEffect("Effect_meteor");
    }

    public void CheckFragment()
    {
        for(int i=0; i<li_Fragment.Count; i++)
        {
            if (li_Fragment[i].gameObject.activeSelf == true)
                return;
        }

        Destroy(this.gameObject);
    }

}
