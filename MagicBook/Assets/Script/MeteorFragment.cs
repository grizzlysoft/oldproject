﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorFragment : MonoBehaviour 
{
    Animator anim;
    Skill_Meteor parent;

    public void SetParent(Skill_Meteor _par)
    {
        anim = this.GetComponent<Animator>();
        parent = _par;
    }

    private void Update()
    {
        //6.4== 메테오 사라질 위치
        if (6.4f > this.transform.position.y)
        {
            anim.SetTrigger("Ground");
        }
        else
        {
            this.transform.Translate(Time.deltaTime * parent.Speed, 0, 0);
        }
    }

    public void Damaged()
    {
        Sound_Manager.Get.PlayEffect("Effect_normalhit");

        //적 피격시 해야할것
        Ingame_Manager.Get.enemy.HitEnemy(Ingame_Manager.Get.Calc_MagicSpellDamage(MagicElement.Fire));
    }

    public void EndAnim()
    {
        this.gameObject.SetActive(false);
        parent.CheckFragment();
    }
}
