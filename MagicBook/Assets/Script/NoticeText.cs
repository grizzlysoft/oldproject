﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoticeText : MonoBehaviour 
{
    [SerializeField] LocalizeText Label_Text;
    [SerializeField] TweenPosition tw_Pos;
    [SerializeField] TweenAlpha tw_Alpha;


    public void Show(int _Index)
    {
        tw_Pos.ResetToBeginning();
        tw_Alpha.ResetToBeginning();
        tw_Pos.enabled = true;
        tw_Alpha.enabled = true;

        Label_Text.ChangeText(_Index);

        this.gameObject.SetActive(true);
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }
}
