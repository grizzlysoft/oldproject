﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTextObjectPool : MonoBehaviour 
{
    [SerializeField] GameObject go_Content;
    List<DamageTextContent> li_Content = new List<DamageTextContent>();

    public void Init()
    {
        GameObject go;
        DamageTextContent tempScp;

        for (int i = 0; i < 20; i++)
        {
            go = Instantiate(go_Content, transform) as GameObject;
            tempScp = go.GetComponent<DamageTextContent>();
            tempScp.Init();
            li_Content.Add(tempScp);
        }

    }


    public void ShowDamageText(DamageData _Dmg)
    {
        for (int i = 0; i < li_Content.Count; i++)
        {
            if (li_Content[i].gameObject.activeSelf == false)
            {
                li_Content[i].Show(_Dmg);
                break;
            }
        }
    }

}
