﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTextContent : MonoBehaviour 
{
    Animation anim;
    [SerializeField] UILabel lb_Damage;

    public void Init()
    {
        anim = this.GetComponent<Animation>();
        Hide();
    }

    public void Show(DamageData _data)
    {
        this.transform.localPosition = Vector3.zero;
        lb_Damage.alpha = 1.0f;
        lb_Damage.text = NumericalFormatter.Format(_data.Damage);
        if(_data.isCritical == true)
        {
            lb_Damage.fontSize = 40;
            lb_Damage.color = UtilityTools.HexToColor("FFAA00FF");
        }
        else
        {
            lb_Damage.fontSize = 25;
            lb_Damage.color = Color.white;
        }

        int rnd = Random.Range(1, 9);

        this.gameObject.SetActive(true);
        anim.Play(string.Format("DamageText@{0}", rnd));
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }

}
