﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CognitioConsulting.Numerics;

public class MoveReward : MonoBehaviour
{
    [SerializeField] Animation anim;
    [SerializeField] UILabel lb_Get;
    BigDecimal addValue;

    public void Init(string _animName ,BigDecimal _Value)
    {
        addValue = _Value;
        lb_Get.text = NumericalFormatter.Format(addValue);
        anim.Play(_animName);

        this.gameObject.SetActive(true);
    }

    public void AddValue()
    {
        //보상 주기
        UserData_Manager.Get.AddMagicStone(addValue);
        this.gameObject.SetActive(false);
        Destroy(this);
    }
}
