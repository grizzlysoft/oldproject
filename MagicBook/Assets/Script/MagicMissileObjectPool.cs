﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicMissileObjectPool : MonoBehaviour
{
    [SerializeField] GameObject go_Content;
    List<MagicMissileContent> li_Content = new List<MagicMissileContent>();

    //속성 별 미사일 애니메이션 셋트
    public List<SpriteAnimation> li_Fire = new List<SpriteAnimation>();
    public List<SpriteAnimation> li_Water = new List<SpriteAnimation>();
    public List<SpriteAnimation> li_Earth = new List<SpriteAnimation>();

    public float Speed = 1.0f;
    public float AngleMinMax = 20;
    public float AngleMinMax_Summon = 10;

    public void Init()
    {
        GameObject go;
        MagicMissileContent tempScp;

        for (int i = 0; i < 20; i++)
        {
            go = Instantiate(go_Content, transform) as GameObject;
            tempScp = go.GetComponent<MagicMissileContent>();
            tempScp.Init();
            li_Content.Add(tempScp);
        }
        
    }

    SpriteAnimation FindAnimation(MagicElement _type)
    {
        SpriteAnimation Find = null;
        //해당 속성 강화 이펙트 레벨->10단위로 짜르셈
        int Level = 0;

        switch (_type)
        {
            case MagicElement.Fire:
                Level = UserData_Manager.Get.Level_Upgrade[(int)UpgradeType.Fire] / 10;
                if (Level >= li_Fire.Count)
                    Level = li_Fire.Count - 1;
                Find = li_Fire[Level];
                break;
            case MagicElement.Water:
                Level = UserData_Manager.Get.Level_Upgrade[(int)UpgradeType.Water] / 10;
                if (Level >= li_Water.Count)
                    Level = li_Water.Count - 1;
                Find = li_Water[Level];
                break;
            case MagicElement.Earth:
                Level = UserData_Manager.Get.Level_Upgrade[(int)UpgradeType.Earth] / 10;
                if (Level >= li_Earth.Count)
                    Level = li_Earth.Count - 1;
                Find = li_Earth[Level];
                break;
            default:
                break;
        }

        //만약 개별적으로 수정 하고 싶다면 나중에 이걸 주석하고 인스펙터에서 Speed 값을 바꿔준다.
        Find.Speed = 0.1f;

        return Find;
    }

    float tempAngle;
    public void ShowMagicMissile(MagicElement _ElementType , AttackType _AttackType , Vector3 _position)
    {
        for (int i = 0; i < li_Content.Count; i++)
        {
            if (li_Content[i].gameObject.activeSelf == false)
            {
                if (_AttackType == AttackType.Summmon)
                    tempAngle = AngleMinMax_Summon;
                else
                    tempAngle = AngleMinMax;

                li_Content[i].Show(FindAnimation(_ElementType), 
                    Ingame_Manager.Get.Calc_MagicMissileDamage(_ElementType, _AttackType),
                    _position, Speed, tempAngle);

                break;
            }
        }
    }

}
