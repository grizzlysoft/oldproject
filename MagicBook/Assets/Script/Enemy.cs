﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CognitioConsulting.Numerics;

public class Enemy : MonoBehaviour
{
    public Animator anim;
    public SpriteRenderer sp_IMG;
    public GameObject go_BossEffect;

    public virtual void Init(bool _isBoss, MagicElement _type)
    {
        go_BossEffect.SetActive(_isBoss);
        ChangeSprite(_type);
    }

    public virtual void ChangeSprite(MagicElement _type)
    {
        sp_IMG.sprite = Resources.Load(
            string.Format("Enemy/LV{0}_Puppet_{1}",
            UserData_Manager.Get.Level_Upgrade[(int)UpgradeType.UnLockTraining], 
            _type.ToString()), typeof(Sprite)) as Sprite;
    }
    
    public void Hit()
    {
        anim.SetTrigger("Hit");
    }

    public void Die()
    {
        anim.SetTrigger("Die");
    }

    //애니메이션에서 제어
    public void Hide()
    {
        Destroy(gameObject);
    }

    
}
