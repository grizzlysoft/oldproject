﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_LeafStrom : MonoBehaviour
{
    private void Awake()
    {
        Sound_Manager.Get.PlayEffect("Effect_wind");
    }

    public void Damaged()
    {
        Sound_Manager.Get.PlayEffect("Effect_normalhit");
        Ingame_Manager.Get.enemy.HitEnemy(Ingame_Manager.Get.Calc_MagicSpellDamage(MagicElement.Earth));
    }

    public void DestroyThis()
    {
        Destroy(this.gameObject);
    }
}
