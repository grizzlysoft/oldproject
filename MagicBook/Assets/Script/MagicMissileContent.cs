﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CognitioConsulting.Numerics;

public class MagicMissileContent : MonoBehaviour
{
    float speed;
    float angle;
    float distance;
    float startPoint;

    //스프라이트 애니메이션
    SpriteAnimation spriteAnim;

    SpriteRenderer sp_Img;
    DamageData dmgData;

    public void Init()
    {
        sp_Img = this.GetComponent<SpriteRenderer>();
        Hide();
    }

    public void Show( SpriteAnimation _spriteAnim, DamageData _Dmg, Vector3 _position, float _speed, float _angle)
    {
        this.gameObject.transform.position = _position;
        angle = Random.Range(-_angle, _angle);
        this.gameObject.transform.localRotation = Quaternion.Euler(0, 0, angle);

        speed = _speed;
        startPoint = _position.x;
        distance = Ingame_Manager.Get.enemy.transform.position.x - startPoint;

        //마법 이미지 변경
        spriteAnim = _spriteAnim;

        //미사일 데미지 넣어줌
        dmgData = _Dmg;

        this.gameObject.SetActive(true);

        //애니메이션 시작
        StartCoroutine(spriteAnim.PlaySpriteAnimation(sp_Img));
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }

    private void Update()
    {
        //적 보다 멀리가면 사라짐 - 피격
        if (Ingame_Manager.Get.enemy.transform.position.x < this.transform.position.x)
        {
            //적 피격시 해야할것
            Ingame_Manager.Get.enemy.HitEnemy(dmgData);
            //데미지 이펙트- 평타만 나옴
            Ingame_Manager.Get.op_HitEffect.ShowHitEffect(this.transform.position, dmgData);
            Hide();
        }
        else
        {
            //이동
            this.transform.Translate(speed * Time.deltaTime, 0, 0);
            //각도 조정-절반이상 왔을때~
            if (this.transform.position.x > startPoint + (distance * 0.5f))
            {
                Vector3 deltaPos = Ingame_Manager.Get.enemy.transform.position - transform.position;
                float angle = Mathf.Atan2(deltaPos.y, deltaPos.x) * Mathf.Rad2Deg;// 삼각함수로 각도를 구함.

                if (0 != angle)    // 물리연산과 렌더링연산의 차이를 위해서 체크
                {
                    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, angle), speed * 0.5f * Time.deltaTime);
                }
            }
        }
    }

    /*
     // 적 - 현재위치 = 방향
            Vector3 deltaPos = Ingame_Manager.Get.enemy.transform.position - transform.position;
            float angle = Mathf.Atan2(deltaPos.y, deltaPos.x) * Mathf.Rad2Deg;// 삼각함수로 각도를 구함.

            if (0 != angle)    // 물리연산과 렌더링연산의 차이를 위해서 체크
            {
                transform.rotation = Quaternion.Euler(0, 0, angle);
                //transform.Rotate(new Vector3(0, 0, angle * 0.1f));
            }

            if (0 != angle)

        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, angle), 10.0f * Time.deltaTime);
        }
     */


}
