﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitEffectObjectPool : MonoBehaviour 
{
    [SerializeField] GameObject go_Content;
    List<HitEffectContent> li_Content = new List<HitEffectContent>();

    public List<SpriteAnimation> li_Animation = new List<SpriteAnimation>();

    public void Init()
    {
        GameObject go;
        HitEffectContent tempScp;

        for (int i = 0; i < 15; i++)
        {
            go = Instantiate(go_Content, transform) as GameObject;
            tempScp = go.GetComponent<HitEffectContent>();
            tempScp.Init();
            li_Content.Add(tempScp);
        }

    }


    public void ShowHitEffect(Vector3 _pos, DamageData _Dmg)
    {
        for (int i = 0; i < li_Content.Count; i++)
        {
            if (li_Content[i].gameObject.activeSelf == false)
            {
                if(_Dmg.isCritical == true)
                {
                    Sound_Manager.Get.PlayEffect("Effect_criticalhit");
                    li_Content[i].Show(_pos, li_Animation[1]);
                }
                else
                {
                    Sound_Manager.Get.PlayEffect("Effect_normalhit");
                    li_Content[i].Show(_pos, li_Animation[0]);
                }

                break;
            }
        }
    }

}
