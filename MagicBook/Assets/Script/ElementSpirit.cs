﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementSpirit : MonoBehaviour
{
    Animator anim;
    [SerializeField] Transform tr_AttackStartPoint;
    public MagicElement ElementType { get; private set; }
    float currentTIme = 0.0f;

    public void Init(MagicElement _ElementType)
    {
        anim = this.GetComponent<Animator>();
        this.transform.localPosition = Vector3.zero;

        ElementType = _ElementType;
        this.gameObject.name = string.Format("{0}_Spirit", ElementType.ToString());

        Hide();
    }

    public void Show()
    {
        this.gameObject.SetActive(true);
        anim.SetTrigger(ElementType.ToString());

        currentTIme = 0;
        switch (ElementType)
        {
            case MagicElement.Fire:
                currentTIme = UserData_Manager.Get.NowValue[(int)UpgradeType.SummonFire];
                break;
            case MagicElement.Water:
                currentTIme = UserData_Manager.Get.NowValue[(int)UpgradeType.SummonWater];
                break;
            case MagicElement.Earth:
                currentTIme = UserData_Manager.Get.NowValue[(int)UpgradeType.SummonEarth];
                break;
        }
    }

    private void Update()
    {
        if(currentTIme > 0)
        {
            currentTIme -= Time.deltaTime;

            if(currentTIme <= 0)
            {
                Hide();
            }
        }
    }

    public void ShootMagic()
    {
        Ingame_Manager.Get.op_MagicMissile.ShowMagicMissile(ElementType, AttackType.Summmon, tr_AttackStartPoint.position);
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }
}
