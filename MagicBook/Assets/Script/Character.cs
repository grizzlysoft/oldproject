﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] Anima2D.SpriteMeshInstance sm_Face;
    [SerializeField] List<Anima2D.SpriteMesh> li_Face = new List<Anima2D.SpriteMesh>();
    [SerializeField] Animator anim;
    [SerializeField] Transform tr_AttackStartPoint;
    public float ClickDelayTime;
    
    float clickPos;

    public void Attack(bool _isAttack)
    {
        if (LocalData_Manager.Get.GetPerkTime(BonusType.PowerPress) != 0)
        {
            //누르고 있는 동안 공격 하기
            if (_isAttack == true)
                InvokeRepeating("ShootBaseMagic", 0, 0.1f);
            else
                CancelInvoke("ShootBaseMagic");
        }
        else
        {
            if (_isAttack == true)
            {
                ShootBaseMagic();
            }
        }
    }

    float CurrentAttackEndTime = 0;
    IEnumerator CheckCurrentAttackAnimation()
    {
        CurrentAttackEndTime = ClickDelayTime;
        anim.SetBool("isAttack", true);

        while (true)
        {
            CurrentAttackEndTime -= 0.01f;
            if (CurrentAttackEndTime <= 0)
            {
                CurrentAttackEndTime = 0;
                anim.SetBool("isAttack", false);
                break;
            }
            yield return new WaitForSeconds(0.01f);
        }
    }

    void ShootBaseMagic()
    {
        //실행중에 꺼지면 인보크를 끔
        if (LocalData_Manager.Get.GetPerkTime(BonusType.PowerPress) <= 0)
        {
            CancelInvoke("ShootBaseMagic");
        }

        UserData_Manager.Get.AddAchievmentValue(AchievementType.Tap, 1);

        if (CurrentAttackEndTime == 0)
        {
            StartCoroutine(CheckCurrentAttackAnimation());
        }

        CurrentAttackEndTime = ClickDelayTime;

#if UNITY_EDITOR
        clickPos = Input.mousePosition.x;
#else
        clickPos = Input.GetTouch(0).position.x;
#endif

        //일반 게임만 속성 마스터 영향을 받는다
        if(Ingame_Manager.Get.IngameType == SpecialDungeonType.Normal)
        {
            if (LocalData_Manager.Get.GetPerkTime(BonusType.ElementalMaster) > 0)
                Shoot_WeekPoint();
            else
                Shoot_Normal();
        }
        else
        {
            Shoot_Normal();
        }
    }

    void Shoot_Normal()
    {
        //속성 구분
        if (clickPos < Screen.width / 3)
            Ingame_Manager.Get.op_MagicMissile.ShowMagicMissile(MagicElement.Fire, AttackType.Normal, tr_AttackStartPoint.position);
        else if (clickPos < (Screen.width / 3) * 2)
            Ingame_Manager.Get.op_MagicMissile.ShowMagicMissile(MagicElement.Water, AttackType.Normal, tr_AttackStartPoint.position);
        else
            Ingame_Manager.Get.op_MagicMissile.ShowMagicMissile(MagicElement.Earth, AttackType.Normal, tr_AttackStartPoint.position);
    }

    void Shoot_WeekPoint()
    {
        switch (Ingame_Manager.Get.enemy.ElementType)
        {
            case MagicElement.Fire:
                Ingame_Manager.Get.op_MagicMissile.ShowMagicMissile(MagicElement.Water, AttackType.Normal, tr_AttackStartPoint.position);
                break;
            case MagicElement.Water:
                Ingame_Manager.Get.op_MagicMissile.ShowMagicMissile(MagicElement.Earth, AttackType.Normal, tr_AttackStartPoint.position);
                break;
            case MagicElement.Earth:
                Ingame_Manager.Get.op_MagicMissile.ShowMagicMissile(MagicElement.Fire, AttackType.Normal, tr_AttackStartPoint.position);
                break;
        }
    }

    public void ShootBaseMagic_Auto()
    {
        //Debug.Log("자동 공격 발싸");

        if (CurrentAttackEndTime == 0)
        {
            StartCoroutine(CheckCurrentAttackAnimation());
        }

        CurrentAttackEndTime = ClickDelayTime;

        Ingame_Manager.Get.op_MagicMissile.ShowMagicMissile((MagicElement)Random.Range(0, 3), AttackType.AutoCasting, tr_AttackStartPoint.position);
    }

    public void CastSpell()
    {
        anim.SetTrigger("UseSkill");
    }

    public void ChangeFace(int _faceid)
    {
        sm_Face.spriteMesh = li_Face[_faceid];
    }
}
