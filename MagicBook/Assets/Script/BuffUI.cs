﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffUI : MonoBehaviour 
{
    [SerializeField] GameObject[] go_Buff;
    [SerializeField] UIGrid gr;
    [SerializeField] GameObject bg;

    public void SetBuffIcon()
    {
        bg.SetActive(false);

        for (int i=0; i<go_Buff.Length; i++)
        {
            if (LocalData_Manager.Get.GetPerkTime((BonusType)i) > 0)
            {
                ShowBuff((BonusType)i);
                bg.SetActive(true);
            }
            else
                HideBuff((BonusType)i);
        }
    }

    public void ShowBuff(BonusType _type)
    {
        if (go_Buff[(int)_type] == null)
            return;

        go_Buff[(int)_type].SetActive(true);
        gr.Reposition();
    }

    public void HideBuff(BonusType _type)
    {
        if (go_Buff[(int)_type] == null)
            return;

        go_Buff[(int)_type].SetActive(false);
        gr.Reposition();
    }
}
