﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoticeTextPool : MonoBehaviour 
{
    [SerializeField] GameObject go_Content;
    List<NoticeText> li_Content = new List<NoticeText>();

    public void Init()
    {
        GameObject go;
        NoticeText tempScp;

        for (int i = 0; i < 20; i++)
        {
            go = Instantiate(go_Content, transform) as GameObject;
            tempScp = go.GetComponent<NoticeText>();
            tempScp.Hide();
            li_Content.Add(tempScp);
        }

    }

    public void ShowText(int _Index)
    {
        for (int i = 0; i < li_Content.Count; i++)
        {
            if (li_Content[i].gameObject.activeSelf == false)
            {
                li_Content[i].Show(_Index);
                break;
            }
        }
    }

}
