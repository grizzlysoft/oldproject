﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalizeText : MonoBehaviour 
{
    public int localizeNum = -1;
    UILabel lb_text;
    object[] li_param = null;

    private void Awake()
    {
        Init();
    }

    void Init()
    {
        lb_text = GetComponent<UILabel>();
        LM.AddText(this);
        ChangeText();
    }

    private void OnEnable()
    {
        ChangeLanguage();
    }

    private void OnDestroy()
    {
        LM.RemoveText(this);
    }

    public void ChangeLanguage()
    {
        if (localizeNum == -1)
            return;

        if(lb_text == null)
            Init();

        lb_text.trueTypeFont = UI_Manager.Get.li_Font[(int)LocalData_Manager.Get.SelectLanguage];

        //-2는 유저가 직접 기입한 텍스트 사용
        if (localizeNum == -2)
            return;

        if (li_param == null)
            lb_text.text = LM.GetText(localizeNum);
        else
            lb_text.text = LM.GetText(localizeNum, li_param);
    }

    /// <summary> 인스펙터에 설정해둔 번호로 텍스트를 설정합니다. 기본값 -1일 경우 실행 ㄴㄴ </summary>
    public void ChangeText()
    {
        if (localizeNum == -1 || localizeNum == -2)
            return;
        
        if (lb_text == null)
            Init();


        li_param = null;
        lb_text.text = LM.GetText(localizeNum);
       // Debug.LogFormat("{0} 오브젝트 이름 변경: {1} ", this.gameObject.name, lb_text.text);
    }


    //아래 함수들은 인스펙터에 -1로 미사용으로 기입 해도 나중에 인덱스 값을 넣음으로써 사용이 가능하도록 만듬
    
    /// <summary> 기존 인덱스 값을 새로운 값으로 바꾼 뒤 텍스트를 설정합니다</summary>
    public void ChangeText(int _index)
    {
        if (lb_text == null)
            Init();

        li_param = null;    
        localizeNum = _index;
        lb_text.text = LM.GetText(localizeNum);
    }

    /// <summary> 인덱스로 가져온 문자열 값이 {0}, {1} 같은 형식일때 매개변수로 채워 넣을 값을 적습니다.</summary>
    public void ChangeText(int _index, params object[] _list)
    {
        if (lb_text == null)
            Init();

        li_param = _list;
        localizeNum = _index;
        lb_text.text = LM.GetText(localizeNum, li_param);
    }

    /// <summary> 유저가 직접 값을 넣어줌 </summary>
    public void ChangeText(string _st)
    {
        if (lb_text == null)
            Init();

        li_param = null;
        localizeNum = -2;
        lb_text.text = _st;
    }
}
