﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using CognitioConsulting.Numerics;

public enum MathParser_Parameters
{
    A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z
}
public static class MathParser
{
    private static Dictionary<MathParser_Parameters, BigDecimal> _Parameters = new Dictionary<MathParser_Parameters, BigDecimal>();
    private static List<String> OperationOrder = new List<string>();

    public static Dictionary<MathParser_Parameters, BigDecimal> Parameters
    {
        get { return _Parameters; }
        set { _Parameters = value; }
    }

    public static void MathParserInit()
    {
        OperationOrder.Add("/");
        OperationOrder.Add("*");
        OperationOrder.Add("-");
        OperationOrder.Add("+");
        OperationOrder.Add("^");
    }

    /// <summary>
    /// 수식 parser
    /// </summary>
    /// <param name="Formula">수식</param>
    /// <param name="value_">수식에 있는 변수(ex : level)</param>
    /// <returns></returns>
    public static BigDecimal Calculate(string Formula, BigDecimal value_)
    {
        try
        {
            Formula = Formula.Replace("level", value_.ToString());
            string[] arr = Formula.Split("/+-*()^".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (KeyValuePair<MathParser_Parameters, BigDecimal> de in _Parameters)
            {
                foreach (string s in arr)
                {
                    if (s != de.Key.ToString() && s.EndsWith(de.Key.ToString()))
                    {
                        Formula = Formula.Replace(s, (BigDecimal.Parse(s.Replace(de.Key.ToString(), "")) * de.Value).ToString());
                    }
                }
                Formula = Formula.Replace(de.Key.ToString(), de.Value.ToString());
            }
            while (Formula.LastIndexOf("(") > -1)
            {
                int lastOpenPhrantesisIndex = Formula.LastIndexOf("(");
                int firstClosePhrantesisIndexAfterLastOpened = Formula.IndexOf(")", lastOpenPhrantesisIndex);
                BigDecimal result = ProcessOperation(Formula.Substring(lastOpenPhrantesisIndex + 1, firstClosePhrantesisIndexAfterLastOpened - lastOpenPhrantesisIndex - 1));
                bool AppendAsterix = false;
                if (lastOpenPhrantesisIndex > 0)
                {
                    if (Formula.Substring(lastOpenPhrantesisIndex - 1, 1) != "(" && !OperationOrder.Contains(Formula.Substring(lastOpenPhrantesisIndex - 1, 1)))
                    {
                        AppendAsterix = true;
                    }
                }

                Formula = Formula.Substring(0, lastOpenPhrantesisIndex) + (AppendAsterix ? "*" : "") + result.ToString() + Formula.Substring(firstClosePhrantesisIndexAfterLastOpened + 1);

            }
            return ProcessOperation(Formula);
        }
        catch (Exception ex)
        {
            throw new Exception("Error Occured While Calculating. Check Syntax", ex);
        }
    }

    private static BigDecimal ProcessOperation(string operation)
    {
        ArrayList arr = new ArrayList();
        string s = "";
        for (int i = 0; i < operation.Length; i++)
        {
            string currentCharacter = operation.Substring(i, 1);
            if (OperationOrder.IndexOf(currentCharacter) > -1)
            {
                if (s != "")
                {
                    arr.Add(s);
                }
                arr.Add(currentCharacter);
                s = "";
            }
            else
            {
                s += currentCharacter;
            }
        }
        arr.Add(s);
        s = "";
        foreach (string op in OperationOrder)
        {
            //UnityEngine.Debug.Log(op);
            while (arr.IndexOf(op) > -1)
            {
                int operatorIndex = arr.IndexOf(op);
                BigDecimal digitBeforeOperator = BigDecimal.Parse(arr[operatorIndex - 1].ToString());
                BigDecimal digitAfterOperator = 0;
                if (arr[operatorIndex + 1].ToString() == "-")
                {
                    arr.RemoveAt(operatorIndex + 1);
                    digitAfterOperator = BigDecimal.Parse(arr[operatorIndex + 1].ToString()) * -1;
                }
                else
                {
                    digitAfterOperator = BigDecimal.Parse(arr[operatorIndex + 1].ToString());
                }
                arr[operatorIndex] = CalculateByOperator(digitBeforeOperator, digitAfterOperator, op);
                arr.RemoveAt(operatorIndex - 1);
                arr.RemoveAt(operatorIndex);
            }
        }
        //UnityEngine.Debug.Log(arr[0]);
        return BigDecimal.Parse(arr[0].ToString());
    }
    private static BigDecimal CalculateByOperator(BigDecimal number1, BigDecimal number2, string op)
    {
        if (op == "/")
        {
            return number1 / number2;
        }
        else if (op == "*")
        {
            return number1 * number2;
        }
        else if (op == "-")
        {
            return number1 - number2;
        }
        else if (op == "+")
        {
            return number1 + number2;
        }
        else if (op == "^")
        {
            return BigDecimal.Pow(number1, System.Numerics.BigInteger.Parse(number2.ToString()));
        }
        else
        {
            return 0;
        }
    }
}
