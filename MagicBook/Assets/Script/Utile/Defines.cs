﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CognitioConsulting.Numerics;

//싱글톤 소스
public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    protected static T instance = null;
    public static T Get
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType(typeof(T)) as T;

                if (instance == null)
                {
                    Debug.Log("Nothing" + instance.ToString());
                    return null;
                }
            }
            return instance;
        }
    }
}


//개인 스프라이트 애니메이션
[System.Serializable]
public class SpriteAnimation
{
    public float Speed;
    public List<Sprite> li_Sprite = new List<Sprite>();

    public IEnumerator PlaySpriteAnimation(SpriteRenderer _spriteRenderer)
    {
        int animIndex = 0;
        while (true)
        {
            _spriteRenderer.sprite = li_Sprite[animIndex];
            yield return new WaitForSeconds(Speed);

            animIndex++;
            if (animIndex >= li_Sprite.Count)
                animIndex = 0;
        }
    }

    public IEnumerator PlaySpriteAnimation_EndMaxImage(SpriteRenderer _spriteRenderer, Del_Void _del)
    {
        int animIndex = 0;
        while (true)
        {
            _spriteRenderer.sprite = li_Sprite[animIndex];
            yield return new WaitForSeconds(Speed);

            animIndex++;
            if (animIndex >= li_Sprite.Count)
            {
                break;
            }
        }

        _del();
    }
}

public class ScrollItem : MonoBehaviour
{
    public virtual void SetData(object data_) { }
    public virtual void Refresh() { }
    public virtual void SetText() { }
}


public class DamageData
{
    public BigDecimal Damage;
    public MagicElement Type;
    public bool isCritical;
}


public static class UtilityTools
{
    /// 소수점을 제거하는 거
    static string RemoveFloat(string value)
    {
        int findIndex = 0;
        for (int i = 0; i < value.Length; i++)
        {
            if (value[i] == '.')
            {
                findIndex = i;
                break;
            }
        }

        if (findIndex != 0)
        {
            return value.Substring(0, findIndex);
        }

        return value;
    }

    public static Color HexToColor(string hex)
    {
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        byte a = byte.Parse(hex.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);

        return new Color32(r, g, b, a);
    }
}

public static class FormulaDefine
{
    public static BigDecimal PerkMagicStone()
    {
        return Table_Manager.Get.li_StageRewardData[UserData_Manager.Get.Stage].Perk_Small;
    }

    public static BigDecimal PerkBigMagicStone()
    {
        return Table_Manager.Get.li_StageRewardData[UserData_Manager.Get.Stage].Perk_Big;
    }

    public static BigDecimal EnemyHP(bool _isBoss)
    {
        if (_isBoss)
            return Table_Manager.Get.li_StageRewardData[UserData_Manager.Get.Stage].HP * 2;
        else
            return Table_Manager.Get.li_StageRewardData[UserData_Manager.Get.Stage].HP;
    }

    public static BigDecimal StageClearFregment(bool _isBoss)
    {
        //허수아비 종류에 따라 보상이 다름
        int Grade = UserData_Manager.Get.Level_Upgrade[(int)UpgradeType.UnLockTraining] - 1;
        BigDecimal Value;
        if (Grade == 0)
            Value = Table_Manager.Get.li_StageRewardData[UserData_Manager.Get.Stage].ClearReward_1;
        else if (Grade == 1)
            Value = Table_Manager.Get.li_StageRewardData[UserData_Manager.Get.Stage].ClearReward_2;
        else
            Value = Table_Manager.Get.li_StageRewardData[UserData_Manager.Get.Stage].ClearReward_3;

        if (_isBoss)//3배
            return Value * 3;
        else
            return Value;
    }

    public static BigDecimal Recon_RewardFregment(int _type)
    {
        int Multiply = 1;

        if (_type == 0)
            Multiply = 100;
        else if (_type == 1)
            Multiply = 250;
        else
            Multiply = 600;

        BigDecimal result = 2000 * BigDecimal.Pow(1.03f, UserData_Manager.Get.Stage) * Multiply;
        return result * UserData_Manager.Get.GetArtifactValue(ArtifactType.Recon_Locket, 1.0f);
    }

    public static BigDecimal ReconnectReward()
    {
        BigDecimal result = 2000 * BigDecimal.Pow(1.03f, UserData_Manager.Get.Stage);
        return result * UserData_Manager.Get.GetArtifactValue(ArtifactType.Talisman_of_Sloth, 1.0f);
    }
}

public static class ConstValue
{
    public const int Hour24_To_Min = 1440;

    public const int Perk_AD_Delay = 30;
    public const int PerkTime_Cash = 43200;
    public const int PerkTime_AD = 3600;

    public const int CoolTime_AttackSpell = 30;
    public const int ManaCost_AttackSpell = 250;
    public const int ManaCost_SummonSpell = 100;

    public const int Max_Ticket = 5;
    public const int Max_TicketPrice = 50;

    public const int BloodMoonDamageMultiply = 100;

    public const int MagicDust_Price = 10;

    public const float AutoAttackBaseTime = 10.0f;

}

//이넘 등록
public enum MagicElement
{
    Fire,
    Water,
    Earth,
    Max,
}

public enum AttackType
{
    Normal,
    AutoCasting,
    Summmon,
}

public enum UpgradeUnlock
{
    None,
    Stage,
    FireLv,
    WarterLv,
    EarthLv,
}

public enum UpgradeType
{
    Attack,
    Critical,
    ManaMax,
    ManaCharge,
    AutoCasting,
    Recon,
    UnLockTraining,
    Fire,
    Water,
    Earth,
    Meteor,
    SummonFire,
    AquaLaser,
    SummonWater,
    LeafStrom,
    SummonEarth,
    Max,
}

public enum PayMentType
{
    Ads,
    Cash,
}

public enum AchievementType
{
    AttackLevel,
    CreticalLevel,
    ManaMaxLevel,
    AutoCastingLevel,
    SerchUnitLevel,
    LevelupFire,
    LevelupWater,
    LevelupEarth,
    StageLevel,
    ShowAD,
    UseSkill,
    UseBonus,
    Tap,
    UnLockTraining,
    Max,
}

public enum MenuOpenState
{
    Upgrade,
    Artifact,
    Achievement,
    Bonus,
    Shop,
}

public enum BonusType
{
    //광고
    GetMagicStone,
    Focus,
    DamagePlus,
    //비광고
    PowerPress,
    SuperFocus,
    ElementalMaster,
    DamagePlus_Big,
    GetMagicStone_Big,
    ManaFullCharge,
    Max,
}


public enum ArtifactType
{
    Senior_Alumni_Staff,
    Bracelet_of_a_Newborn,
    Absolute_Ring,
    Necklace_of_Fire,
    Necklace_of_Water,
    Necklace_of_Earth,
    Parrot_Brooch,
    Recon_Locket,
    Talisman_of_Sloth,
    Meteorite_Hairpin,
    Ice_Hairpin,
    Leaf_Hairpin,
    Salamander_GuardStone,
    Undine_GuardStone,
    Gnome_GuardStone,
    Max
    /*
    수석 졸업생의 지팡이
    생환자의 팔찌
    절대 반지
    불의 목걸이
    물의 목걸이
    땅의 목걸이
    앵무새 브로치
    수색대의 로켓
    나태한자의 부적
    운석 헤어핀
    얼음 헤어핀
    나뭇잎 헤어핀
    샐러맨더의 수호석
    운디네의 수호석
    노움의 수호석
    */
}

public enum Language
{
    Kor,
    Jp,
    Max,
}

public enum ShowAdType
{
    Bonus_GetMagicStone,
    Bonus_GetFocus,
    Bonus_GetDamage,
    Ruby,
    MagicDust,
    Skill_Meteor,
    Skill_SummonFire,
    Skill_AquaLaser,
    Skill_SummonWater,
    Skill_LeafStrom,
    Skill_SummonEarth,

    DoubleReward,
    Retry,

    Max,
}

public enum RewardType
{
    Ruby,
    MagicStone,
    MagicDust
}

public enum LevelupType
{
    One,
    Max,
}

public enum LogType
{
    TryBuyRuby,
    GetBuyRuby,
    CheckBuyRuby,
    UseRuby,
    GetRuby,
}

public enum SpecialDungeonType
{
    BloodMoon = 0,
    FullMoon = 1,
    Normal = -1,
}

public enum FBDB_UpLoadState
{
    UpLoad_UserData,
    UpLoad_Ruby,
    None,
}

public enum FBDB_LoadState
{
    Check_UserData,
    Load_Ruby,
    Check_Version,
    Load_UserData,
    None,
}
