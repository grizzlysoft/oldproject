﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using UnityEngine.SocialPlatforms;
using Google;
using System.Text;
using System;

//xml 관련
using System.Xml;
using System.Xml.Serialization;
using System.IO;

[Serializable]
public class FireDBData
{
    public string MagicStone;
    public int Ruby;
    public int Stage;
    public int[] UpgradeLevel;
    public int[] NowAchievementid;
    public int[] AchievmenetValue;
    public Dictionary<int, bool> AchievementClear = new Dictionary<int, bool>();
}

[Serializable]
public class Server_UserData
{
    public string Gpgs_Nick;
    public FireDBData Data = new FireDBData();
}

//저장 데이터 구조체
[XmlInclude(typeof(CloudSaveData))]
[Serializable]
public class CloudSaveData
{
    public string MagicStone;
    public int MagicDust;
    public int Ruby;
    public int Stage;
    public int Stage_FullMoon;
    public int[] UpgradeLevel;
    public int[] ArtifactLevel;
    public int[] NowAchievementid;
    public int[] AchievmenetValue;
    public int AchievementCnt;
    public int[] AchievementId;
    public bool[] AchievementClear;
    
}

public class Server_Manager : Singleton<Server_Manager>
{
    public string UserServerID { get; private set; } = string.Empty;

    public void Init()
    {
#if !UNITY_EDITOR
         PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
        .EnableSavedGames()
        .Build();
        //GPGS 시작.
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
        GPGS_Login();
#endif
    }

    public void GPGS_Login()
    {
        if (Social.localUser.authenticated == true)
        {
            Debug.Log("로그인했다");
        }
        else
        {
            Social.localUser.Authenticate((bool success) =>
            {
                if (success)
                {
                    Debug.Log("GPGS 로그인 성공");
                }
                else
                {
                    // 로그인 실패
                    Debug.Log("Login failed for some reason");
                }
            });
        }
    }

    //gpgs 관련
    //인증여부 확인
    public bool CheckLogin()
    {
        return Social.localUser.authenticated;
    }

    /// <summary> 리더보드 UI show </summary>
    public void ShowLeaderBoard()
    {
        if (!CheckLogin()) //로그인되지 않았으면
        {
            //로그인루틴을 진행하던지 합니다.

            return;
        }
        Social.Active.ShowLeaderboardUI();
    }

    /// <summary> 업적 UI show </summary>
    public void ShowAchievement()
    {
        if (!CheckLogin()) //로그인되지 않았으면
        {
            //로그인루틴을 진행하던지 합니다.

            return;
        }
        Social.ShowAchievementsUI();
    }


    /// <summary> 리더보드 점수 업로드 </summary>
    public void ReportScore(string _id, int _value)
    {
#if !UNITY_EDITOR
        Social.ReportScore(_value, _id, (bool success) =>
        {
            if (success) Debug.Log("ReportScore Success");

            else Debug.Log("ReportScore Fail");
        });
#endif
    }

    public void UnlockAchievement(string _id)
    {
        PlayGamesPlatform.Instance.UnlockAchievement(_id, (bool success) =>
        {
            if (success) Debug.Log("UnlockAchievement Success");

            else Debug.Log("UnlockAchievement Fail");
        });
    }

    void ExitGame()
    {
        Application.Quit();
    }

    void GoPlayStore()
    {
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.GrizzlySoft.MagicBook");
    }

    void GameData_to_CloudSaveData()
    {
        Popup_Manager.Get.Loading.Show();
        SaveData.MagicStone = UserData_Manager.Get.MagicStone.ToString();
        SaveData.MagicDust = UserData_Manager.Get.MagicDust;
        SaveData.Stage = UserData_Manager.Get.Stage;
        SaveData.Stage_FullMoon = UserData_Manager.Get.Stage_FullMoon;
        SaveData.Ruby = UserData_Manager.Get.Cash_Value;
        SaveData.UpgradeLevel = UserData_Manager.Get.Level_Upgrade;
        SaveData.ArtifactLevel = UserData_Manager.Get.Level_Artifact;
        SaveData.NowAchievementid = UserData_Manager.Get.NowAchievementID;
        SaveData.AchievmenetValue = UserData_Manager.Get.AchievementValue;

        SaveData.AchievementCnt = UserData_Manager.Get.AchievementClear.Count;
        int[] achievementid = new int[SaveData.AchievementCnt];
        bool[] achievementclear = new bool[SaveData.AchievementCnt];

        int index = 0;
        foreach(KeyValuePair<int, bool> o in UserData_Manager.Get.AchievementClear)
        {
            achievementid[index] = o.Key;
            achievementclear[index] = o.Value;
            index++;
        }
        index = 0;
        SaveData.AchievementId = achievementid;
        SaveData.AchievementClear = achievementclear;

    }
    void CloudSaveData_to_GameData()
    {
        Debug.Log("LoadData");
        Debug.Log("MagicStone = " + SaveData.MagicStone);
        Debug.Log("ruby = " + SaveData.Ruby);
        Debug.Log("Stage = " + SaveData.Stage);

        for (int i = 0; i < SaveData.UpgradeLevel.Length; ++i)
            Debug.Log("upgrade level = " + SaveData.UpgradeLevel[i]);

        for (int i = 0; i < SaveData.NowAchievementid.Length; ++i)
            Debug.Log("now achievement id = " + SaveData.NowAchievementid[i]);

        for (int i = 0; i < SaveData.AchievmenetValue.Length; ++i)
            Debug.Log("achiemenet value = " + SaveData.AchievmenetValue[i]);

        for (int i = 0; i < SaveData.AchievementClear.Length; ++i)
        {
            Debug.Log("achievment id = " + SaveData.AchievementId[i]);
            Debug.Log("achievment clear = " + SaveData.AchievementClear[i]);
        }

        UserData_Manager.Get.SetServerUserData(SaveData);
        Popup_Manager.Get.Check.Show(LM.GetText(282));
        Popup_Manager.Get.Loading.Hide();
        LocalData_Manager.Get.SetLoadTime();
    }

    //변수 선언
    public CloudSaveData SaveData = new CloudSaveData();
    string fileName = "MagicBook_saveData";

    //오브젝트를 바이트로 던지기?
    public byte[] SaveDataToBytes()
    {
        //게임데이터 -> 세이브 데이터 이동
        GameData_to_CloudSaveData();
        //세이브 데이터 -> string -> byte[]로 변환
        return System.Text.ASCIIEncoding.Default.GetBytes(objectToString(SaveData));
    }
    /// <summary> 오브젝트를 Xml 스트링으로 변환 </summary>
    string objectToString(object _class)
    {
        XmlSerializer _xs = new XmlSerializer(typeof(CloudSaveData));
        using (StringWriter _textWriter = new StringWriter())
        {
            _xs.Serialize(_textWriter, _class);
            return _textWriter.ToString();
        }
    }

    //바이트 배열을 클래스로 넣기
    public void LoadDataFromBytes(byte[] bytes)
    {
        SaveData = stringToObject(System.Text.ASCIIEncoding.Default.GetString(bytes));
        CloudSaveData_to_GameData();
    }
    /// <summary> Xml 스트링을  오브젝트로 변환 </summary>
    public CloudSaveData stringToObject(string _xmlString)
    {
        XmlSerializer _xs = new XmlSerializer(typeof(CloudSaveData));
        object _object = _xs.Deserialize(new StringReader(_xmlString));
        return (CloudSaveData)_object;
    }

    public void SaveToCloud()
    {
#if UNITY_EDITOR
        GameData_to_CloudSaveData();
#else
        Debug.Log("클라우드 세이브 시작");
        if (!CheckLogin()) //로그인되지 않았으면
        {
            //로그인루틴을 진행하던지 합니다.
            return;
        }

        //파일이름에 적당히 사용하실 파일이름을 지정해줍니다.
        OpenSavedGame(fileName, true);
#endif
    }

    void OpenSavedGame(string filename, bool bSave)
    {
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        //Invoke("ErrorNotConnent", 20f);
        if (bSave)
        {
            savedGameClient.OpenWithAutomaticConflictResolution(filename, DataSource.ReadCacheOrNetwork,
                ConflictResolutionStrategy.UseLongestPlaytime, OnSavedGameOpenedToSave); //저장루틴진행

            //세이브중 로딩 인디케이트 설정
        }
        else
        {
            savedGameClient.OpenWithAutomaticConflictResolution(filename, DataSource.ReadCacheOrNetwork,
                ConflictResolutionStrategy.UseLongestPlaytime, OnSavedGameOpenedToRead); //로딩루틴 진행

            //로딩 인디케이트 설정
        }
    }

    void OnSavedGameOpenedToSave(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            // handle reading or writing of saved game.
            //파일이 준비되었습니다. 실제 게임 저장을 수행합니다.
            //저장할데이터바이트배열에 저장하실 데이터의 바이트 배열을 지정합니다.
            SaveGame(game, SaveDataToBytes(), DateTime.Now.TimeOfDay);
        }
        else
        {
            //PopupManager.Get.AllClosePopup();
            //CancelInvoke("ErrorNotConnent");
            ////파일열기에 실패 했습니다. 오류메시지를 출력하든지 합니다.
        }

    }

    void SaveGame(ISavedGameMetadata game, byte[] savedData, TimeSpan totalPlaytime)
    {
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        SavedGameMetadataUpdate.Builder builder = new SavedGameMetadataUpdate.Builder();

        builder = builder
            .WithUpdatedPlayedTime(totalPlaytime)
            .WithUpdatedDescription("Saved game at " + DateTime.Now);

        /*
        if (savedImage != null)
        {
            // This assumes that savedImage is an instance of Texture2D
            // and that you have already called a function equivalent to
            // getScreenshot() to set savedImage
            // NOTE: see sample definition of getScreenshot() method below
            byte[] pngData = savedImage.EncodeToPNG();
            builder = builder.WithUpdatedPngCoverImage(pngData);
        }*/

        SavedGameMetadataUpdate updatedMetadata = builder.Build();
        savedGameClient.CommitUpdate(game, updatedMetadata, savedData, OnSavedGameWritten);
    }


    void OnSavedGameWritten(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        Popup_Manager.Get.Loading.Hide();
        if (status == SavedGameRequestStatus.Success)
        {
            //데이터 저장이 완료되었습니다.
            Popup_Manager.Get.Check.Show(LM.GetText(283));
        }
        else
        {
            //데이터 저장에 실패 했습니다.
            Popup_Manager.Get.Check.Show(LM.GetText(284));
        }
    }

    //----------------------------------------------------------------------------------------------------------------
    //클라우드로 부터 파일읽기
    public void LoadFromCloud()
    {
        Debug.Log("클라우드 로드 시작");
        
        Popup_Manager.Get.Loading.Show();
        if (!CheckLogin())
        {
            //로그인되지 않았으니 로그인 루틴을 진행하던지 합니다.
            return;
        }

        //내가 사용할 파일이름을 지정해줍니다. 그냥 컴퓨터상의 파일과 똑같다 생각하시면됩니다.
        OpenSavedGame(fileName, false);
    }

    void OnSavedGameOpenedToRead(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            // handle reading or writing of saved game.
            LoadGameData(game);
        }
        else
        {
            //PopupManager.Get.AllClosePopup();
            //CancelInvoke("ErrorNotConnent");
            ////파일열기에 실패 한경우, 오류메시지를 출력하던지 합니다.
            Popup_Manager.Get.Loading.Hide();
            Popup_Manager.Get.Check.Show(LM.GetText(285));
        }
    }


    //데이터 읽기를 시도합니다.
    void LoadGameData(ISavedGameMetadata game)
    {
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        savedGameClient.ReadBinaryData(game, OnSavedGameDataRead);
    }

    void OnSavedGameDataRead(SavedGameRequestStatus status, byte[] data)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            // handle processing the byte array data
            //데이터 읽기에 성공했습니다.
            //data 배열을 복구해서 적절하게 사용하시면됩니다.
            LoadDataFromBytes(data);
        }
        else
        {
            Popup_Manager.Get.Loading.Hide();
            //읽기에 실패 했습니다. 오류메시지를 출력하던지 합니다.
            Popup_Manager.Get.Check.Show(LM.GetText(285));
        }
    }

}

    
