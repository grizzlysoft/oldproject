﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaymentManager : Singleton<PaymentManager>
{
    public void BuyPrice(Table_Manager.ShopData data_)
    {
        switch(data_.PayType)
        {
            case PayMentType.Cash:
                Purchasing.Get.Buy(data_.StoreID);
                break;
            case PayMentType.Ads:
                Ads_Manager.Get.ShowAd_UnityAds(ShowAdType.Ruby, data_.RewardID);
                break;
        }

    }

    /// <summary>
    /// 상품지급
    /// </summary>
    /// <param name="id_">리워드 시트 id</param>
    public void PayPrice(int id_)
    {
        Table_Manager.RewardData data = Table_Manager.Get.li_RewardData.Find(r => r.ID == id_);

        switch(data.Type)
        {
            case RewardType.Ruby:
                UserData_Manager.Get.AddCash_Value(data.Count);
                break;
            case RewardType.MagicStone:
                UserData_Manager.Get.AddMagicStone(data.Count);
                break;
        }
    }

    /// <summary>
    /// 상품지급
    /// </summary>
    /// <param name="id_">스토어 id</param>
    public void PayPrice(string id_)
    {
        Table_Manager.ShopData s = Table_Manager.Get.li_ShopData.Find(r => r.StoreID == id_);
        PayPrice(s.RewardID);
        Popup_Manager.Get.Check.Show(LM.GetText(252));
    }

}
