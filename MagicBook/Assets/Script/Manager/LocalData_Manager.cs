﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LocalData_Manager : Singleton<LocalData_Manager>
{
    /// 로컬 데이터 로드
    public void LoadLocalData()
    {
        Mana = PlayerPrefs.GetInt("Mana", 0);
        isPushAlarm = PlayerPrefs.GetInt("isPushAlarm", 1);
        SelectLanguage = (Language)PlayerPrefs.GetInt("SelectLanguage", 0);

        UI_Manager.Get.Buff.SetBuffIcon();
        for (int i = 0; i < (int)BonusType.Max; i++)
        {
            SetPerkTime((BonusType)i, CalcPerkTime(i));
        }

        Levelup_Type = (LevelupType)PlayerPrefs.GetInt("Levelup_Type", 0);
        Ticket = PlayerPrefs.GetInt("Remain_Ticket", ConstValue.Max_Ticket);

        FullMoon_RemainHP = PlayerPrefs.GetString("FullMoon_RemainHP", "0");

        LoadPlayerPrefabLoadTime();
    }

    /// <summary> 현재마나 </summary>
    public int Mana { get; private set; } = 0;
    public void AddMana(int _value)
    {
        Mana += _value;

        //마나 최대치 넘으면 최대치로 유지
        if (Mana >= UserData_Manager.Get.NowValue[(int)UpgradeType.ManaMax])
        {
            Mana = UserData_Manager.Get.NowValue[(int)UpgradeType.ManaMax];
        }

        UI_Manager.Get.CharacterInfo.SetManaBar();
        PlayerPrefs.SetInt("Mana", Mana);
    }

    /// <summary> 푸쉬 알림 On/Off </summary>
    public int isPushAlarm { get; private set; } = 1;
    public void SetPushAlarm(int _value)
    {
        isPushAlarm = _value;
        PlayerPrefs.SetInt("isPushAlarm", isPushAlarm);
    }

    /// <summary> 선택 언어 </summary>
    public Language SelectLanguage { get; private set; } = Language.Kor;
    public void ChanageLanguage(Language lan_)
    {
        SelectLanguage = lan_;

        PlayerPrefs.SetInt("SelectLanguage", (int)SelectLanguage);

        //모든 고정 텍스트 변경
        LM.SetAllText();
    }



    #region 특전 시간

    /// <summary> 각 특전 남은 시간 </summary>
    [SerializeField] int[] ar_PerkTime = new int[(int)BonusType.Max];

    public int GetPerkTime(BonusType _type)
    {
        return ar_PerkTime[(int)_type];
    }

    public string GetPerkTimeString(BonusType _type)
    {
        int num = ar_PerkTime[(int)_type];

        //시, 분, 초 선언
        int hours, minute, second;

        //시간공식
        hours = num / 3600;//시 공식
        minute = num % 3600 / 60;//분을 구하기위해서 입력되고 남은값에서 또 60을 나눈다.
        second = num % 3600 % 60;//마지막 남은 시간에서 분을 뺀 나머지 시간을 초로 계산함

        return string.Format("{0:00}:{1:00}:{2:00}", hours, minute, second);
    }

    //이전과 현재 접속 시간 비교, 초(sec) 리턴
    public int CalcPerkTime(int _index)
    {
        string temp = PlayerPrefs.GetString(string.Format("save_PerkTime_{0}", _index), "0");
        if (temp == "0")
            return 0;

        DateTime SaveTime = new DateTime(long.Parse(temp));
        TimeSpan ts_Gap = SaveTime - DateTime.Now;
        //Debug.Log("Gap : " + ts_Gap.Seconds);
        //이미 지나 버렸으면 0 리턴
        if (ts_Gap.Ticks < 0)
            return 0;

        return (int)ts_Gap.TotalSeconds;
    }

    /// <summary> 타이머 변수에 시간 설정 </summary>
    public void SetPerkTime(BonusType _type, int _Time)
    {
        //이미 시간 값이 있다면 하지 않는다(덮어쓰기 방지) , 설정 시간이 0초 라도 하지 않는다(초기화 때 값을 넣어주지 않기 위해).
        if (ar_PerkTime[(int)_type] != 0 || _Time == 0)
            return;

        //Debug.Log("특전 시간 저장!");

        //끝나는 시간 저장
        PlayerPrefs.SetString(string.Format("save_PerkTime_{0}", (int)_type), DateTime.Now.AddSeconds(_Time).Ticks.ToString());

        ar_PerkTime[(int)_type] = _Time;
        StartCoroutine(PerkTimer(_type));
    }

    IEnumerator PerkTimer(BonusType _type)
    {
        UI_Manager.Get.Buff.SetBuffIcon();

        while (true)
        {
            if (ar_PerkTime[(int)_type] <= 0)
                break;

            ar_PerkTime[(int)_type] -= 1;
            //UI 갱신 
            UI_Manager.Get.Menu.ReFreshBonusItems();

            yield return new WaitForSeconds(1.0f);
        }

        UI_Manager.Get.Buff.SetBuffIcon();
        ar_PerkTime[(int)_type] = 0;
    }

    #endregion

    #region 재접속 보상 체크

    //재접속 보상 관련
    public void SaveExitTime()
    {
        //Debug.LogFormat("종료 시간 저장! {0}", System.DateTime.Now.ToLongTimeString());
        PlayerPrefs.SetString("ExitGame_Ticks", DateTime.Now.Ticks.ToString());
    }

    //이전과 현재 접속 시간 비교, 분(min) 리턴
    public int CalcReconnectTime()
    {
        string temp = PlayerPrefs.GetString("ExitGame_Ticks", "0");
        if (temp == "0")
            return 0;

        DateTime SaveTime = new DateTime(long.Parse(temp));
        TimeSpan ts_Gap = DateTime.Now - SaveTime;

        return (int)ts_Gap.TotalMinutes;
    }


    #endregion


    /// <summary> 선택 언어 </summary>
    #region 티켓
    public int Ticket { get; private set; } = 0;

    //남은 티켓 확인
    public void RefreshTicket()
    {
        string temp = PlayerPrefs.GetString("TicketResetTime", "");
        //만약 날짜가 바뀌면 티켓 초기화
        if (DateTime.Now.ToShortDateString() != temp)
        {
            Ticket = ConstValue.Max_Ticket;
            PlayerPrefs.SetString("TicketResetTime", DateTime.Now.ToShortDateString());
        }

        PlayerPrefs.SetInt("Remain_Ticket", Ticket);
    }

    public bool UseTicket(int _Use)
    {
        if(Ticket < _Use)
        {
            //티켓 구매로 넘긴다
            Popup_Manager.Get.Buy.Show(LM.GetText(267), "ruby", ConstValue.Max_TicketPrice.ToString(), BuyTicket);
            return false;
        }

        Ticket -= _Use;
        PlayerPrefs.SetInt("Remain_Ticket", Ticket);

        return true;
    }

    void BuyTicket()
    {
        //캐쉬 부족 . 상점 이동?
        if(UserData_Manager.Get.UseCash_Value(ConstValue.Max_TicketPrice) == true)
        {
            Ticket = ConstValue.Max_Ticket;
            PlayerPrefs.SetInt("Remain_Ticket", Ticket);

            //티켓 갱신 
            Popup_Manager.Get.SpecialDungeon.Refresh();
        }
    }
    

    #endregion

    //다중 업그레이드
    public LevelupType Levelup_Type { get; private set; } = LevelupType.Max;
    public void ChangeLevelUpType()
    {
        //나중에 요소가 늘어날 수도 있으니 이렇게 구현함
        if (Levelup_Type == LevelupType.Max)
            Levelup_Type = LevelupType.One;
        else
            Levelup_Type++;

        PlayerPrefs.SetInt("Levelup_Type", (int)Levelup_Type);
    }

    #region 서버 세이브
    /// <summary>
    /// 로드 시도 성공시 시간 이걸 로컬에 저장하고 불러온다
    /// </summary>
    DateTime LoadTime = DateTime.MinValue;

    public void LoadPlayerPrefabLoadTime()
    {
        if (PlayerPrefs.HasKey("ServerLoadTime"))
            LoadTime = new DateTime(long.Parse(PlayerPrefs.GetString("ServerLoadTime")));
    }

    public void SetLoadTime()
    {
        LoadTime = DateTime.Now;
        PlayerPrefs.SetString("ServerLoadTime", LoadTime.Ticks.ToString());
    }

    public bool CheckLoadTime()
    {
        TimeSpan t = DateTime.Now - LoadTime;
        if (t.TotalHours > 8)
            return true;
        else
            return false;
    }

    public string GetLoadTimeDelay()
    {
        TimeSpan t = LoadTime.AddHours(8) - DateTime.Now;

        return string.Format("{0}:{1}:{2}", t.Hours, t.Minutes, t.Seconds);
    }
    #endregion

    //만월 허수아비 체력
    public string FullMoon_RemainHP { get; private set; } = "0";
    public void SetFullMoon_RemainHP(String _Hp)
    {
        FullMoon_RemainHP = _Hp;
        PlayerPrefs.SetString("FullMoon_RemainHP", FullMoon_RemainHP);
    }
}
