﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CognitioConsulting.Numerics;

public class Ingame_Manager : Singleton<Ingame_Manager>
{
    public SpecialDungeonType IngameType = SpecialDungeonType.Normal;

    //UI 측과 통신은 이걸로 일단...
    public Character character;
    public Enemy_Manager enemy;
    public MagicMissileObjectPool op_MagicMissile;
    public HitEffectObjectPool op_HitEffect;
    public Skill_Manager skill;

    [SerializeField] SpriteRenderer BG;
 
    int ReconnectTime = 0;//몇분만에 재접속 했는지~
    int ReconTime = 0; //파편 수색대 탐색 시간

    float timer = 0;//특정 스테이지 타이머
    public int battleCount { get; private set; } = 0;// 이 스테이지를 몇번 도전했는지~

    Table_Manager.RaidRewardData FullMoonData;

    //초기화
    public void Init()
    {
        IngameType = SpecialDungeonType.Normal;

        //모든 인게임 초기화
        op_MagicMissile.Init();
        op_HitEffect.Init();
        skill.Init();

        //마나 리젠 활성화
        InvokeRepeating("RegenerationMana", 0, 10);
        //파편 수색대 활성
        InvokeRepeating("CheckRecon", 1, 1);
    }

    private void Update()
    {
        AutoCasting();
    }

    public void ChangeIngameMode(SpecialDungeonType _type)
    {
        StopAllCoroutines();
        //현재 소환된 일반 몹 제거
        enemy.DeleteCurrentEnemy();

        IngameType = _type;
        switch (IngameType)
        {
            case SpecialDungeonType.BloodMoon:
            case SpecialDungeonType.FullMoon:
                StartCoroutine(StageChange_SpecialBattle());
                break;
            case SpecialDungeonType.Normal:
                StartCoroutine(StageChange_Normal());
                break;
        }
    }

    //스테이지 변경 효과
    IEnumerator StageChange_Normal()
    {
        //페이드 인 연출
        Popup_Manager.Get.FadeInOut.Show();
        yield return StartCoroutine(Popup_Manager.Get.FadeInOut.FadeIn());

        //UI 변경
        UI_Manager.Get.SetUI_IngameMode(IngameType);
        //배경 변경
        BG.sprite = Resources.Load<Sprite>("Texture/Main_BG");
        //몬스터 변경
        enemy.CreateEnemy(IngameType);

        yield return new WaitForSeconds(0.2f);

        //페이드 아웃
        yield return StartCoroutine(Popup_Manager.Get.FadeInOut.FadeOut());
        Popup_Manager.Get.FadeInOut.Hide();
    }
    IEnumerator StageChange_SpecialBattle()
    {
        //연출 전에 미리 적용해서 시작시 안어색 하게~
        timer = 30.00f;
        UI_Manager.Get.Label_Timer.text = timer.ToString("F2");

        //데이터 초기화 
        enemy.BloodMoon_HitDamage = 0;
        battleCount = 0;
        FullMoonData = Table_Manager.Get.li_RaidRewardData[UserData_Manager.Get.Stage_FullMoon];

        //페이드 인 연출
        Popup_Manager.Get.FadeInOut.Show();
        yield return StartCoroutine(Popup_Manager.Get.FadeInOut.FadeIn());
        
        //UI 변경
        UI_Manager.Get.SetUI_IngameMode(IngameType);

        //배경 변경
        BG.sprite = Resources.Load<Sprite>(string.Format("Texture/{0}_BG", IngameType.ToString()));
        //몬스터 변경
        enemy.CreateEnemy(IngameType);

        //기존 소환 정령 소환 해제
        skill.Hide_AllSummon();

        yield return new WaitForSeconds(0.2f);

        //페이드 아웃
        yield return StartCoroutine(Popup_Manager.Get.FadeInOut.FadeOut());
        Popup_Manager.Get.FadeInOut.Hide();

        //게임 시작 연출
        Popup_Manager.Get.StartBattle.Show();
        yield return StartCoroutine(Popup_Manager.Get.StartBattle.AnimStart());
        Popup_Manager.Get.StartBattle.Hide();

        StartBattle();
     
        yield return null;
    }
    IEnumerator Battle_BloodMoon()
    {
        //시간 체크
        while (true)
        {
            timer -= Time.deltaTime;
            UI_Manager.Get.Label_Timer.text = timer.ToString("F2");

            if (timer <= 0)
            {
                timer = 0;
                UI_Manager.Get.Label_Timer.text = timer.ToString("F2");

                //전투 결과 팝업 
                Popup_Manager.Get.EndBattle.Show(IngameType, RewardType.MagicStone, enemy.BloodMoon_HitDamage);
                break;
            }

            yield return null;
        }

        yield return null;
    }
    IEnumerator Battle_FullMoon()
    {
        //시간 체크 + 적 사망 체크
        while (true)
        {
            timer -= Time.deltaTime;
            UI_Manager.Get.Label_Timer.text = timer.ToString("F2");

            if (timer <= 0)
            {
                timer = 0;
                UI_Manager.Get.Label_Timer.text = timer.ToString("F2");

                LocalData_Manager.Get.SetFullMoon_RemainHP(enemy.HP.ToString());
                Popup_Manager.Get.Retry.Show(battleCount, enemy.HP);
                break;
            }

            if (enemy.HP <= 0)
            {
                //전투 결과 팝업 
                Popup_Manager.Get.EndBattle.Show(IngameType, FullMoonData.rewardType, FullMoonData.rewardAmount);
                break;
            }

            yield return null;
        }

        yield return null;
    }

    void StartBattle()
    {
        //각 전투 끝날때 까지 대기
        switch (IngameType)
        {
            case SpecialDungeonType.BloodMoon:
                StartCoroutine(Battle_BloodMoon());
                break;
            case SpecialDungeonType.FullMoon:
                StartCoroutine(Battle_FullMoon());
                break;
        }
    }

    
    public void RetryBattle()
    {
        battleCount++;
        timer = 30.0f;
        StartBattle();
    }

    //일반 스테이지로 돌아오는 코드
    public void ChangeNormalStage()
    {
        ChangeIngameMode(SpecialDungeonType.Normal);
    }

    #region Character
    //마나 재생 - 인보크 repeat
    void RegenerationMana()
    {
        LocalData_Manager.Get.AddMana(UserData_Manager.Get.NowValue[(int)UpgradeType.ManaCharge]);
    }
    //자동 영창 
    float autoCastTime = 0;
    float shotTime = 0;
    void AutoCasting()
    {
        if (UserData_Manager.Get.Level_Upgrade[(int)UpgradeType.AutoCasting] == 0)
            return;

        //시간 체크..
        autoCastTime += Time.deltaTime;

        shotTime = ConstValue.AutoAttackBaseTime - UserData_Manager.Get.GetArtifactValue(ArtifactType.Parrot_Brooch, 0.0f);
        if (autoCastTime >= shotTime)
        {
            autoCastTime = 0;
            character.ShootBaseMagic_Auto();
        }

    }

    //프레스 공격 데미지
    public DamageData Calc_MagicMissileDamage(MagicElement _ElementType, AttackType _AttackType)
    {
        DamageData temp = new DamageData();
        temp.Type = _ElementType;
        temp.isCritical = false;

        //추가 공격력
        BigDecimal BaseDmg = GetBaseDamage();
        BigDecimal ElementDmg = GetElementDamage(BaseDmg, _ElementType);
        BigDecimal CriticalDmg = 0;
        
        //기본 치명타 확률 + 유물 증가값
        int CritChance = 500 + (int)(UserData_Manager.Get.GetArtifactValue(ArtifactType.Bracelet_of_a_Newborn, 0.0f) * 100);

        //일반 전투 데미지 처리
        if(IngameType == SpecialDungeonType.Normal)
        {
            //집중 특전
            if (LocalData_Manager.Get.GetPerkTime(BonusType.Focus) > 0)
                CritChance += 2500;

            if (LocalData_Manager.Get.GetPerkTime(BonusType.SuperFocus) > 0)
                CritChance += 7000;

            //소환수 공격은 무조건 크리
            if (_AttackType == AttackType.Summmon)
                CritChance = 10000;
        }

        //치명타 떳냐? 5퍼센트 고정
        if (Random.Range(0, 10000) < CritChance)
        {
            temp.isCritical = true;
            CriticalDmg = (BaseDmg + ElementDmg) * 
                (GetDamageMultiply(UpgradeType.Critical) * UserData_Manager.Get.GetArtifactValue(ArtifactType.Absolute_Ring, 1.0f));
        }

        //최종 데미지
        temp.Damage = BaseDmg + ElementDmg + CriticalDmg;


        //일반 전투 최종 데미지 보정
        if (IngameType == SpecialDungeonType.Normal)
        {
            //자동영창 추가 데미지
            if (_AttackType == AttackType.AutoCasting)
            {
                temp.Damage *= UserData_Manager.Get.NowValue[(int)UpgradeType.AutoCasting];
            }

            //소환수 유물에 따른 데미지 증폭
            if (_AttackType == AttackType.Summmon)
            {
                switch (_ElementType)
                {
                    case MagicElement.Fire:
                        temp.Damage *= UserData_Manager.Get.GetArtifactValue(ArtifactType.Salamander_GuardStone, 1.0f);
                        break;
                    case MagicElement.Water:
                        temp.Damage *= UserData_Manager.Get.GetArtifactValue(ArtifactType.Undine_GuardStone, 1.0f);
                        break;
                    case MagicElement.Earth:
                        temp.Damage *= UserData_Manager.Get.GetArtifactValue(ArtifactType.Gnome_GuardStone, 1.0f);
                        break;
                }
            }

            //특전 
            //5배 , 2배 적용~
            GetPerkDamagePlusMultiply(ref temp.Damage);
        }

        temp.Damage = temp.Damage.Round();
        return temp;
    }
    //속성별 공격 스킬 데미지
    public DamageData Calc_MagicSpellDamage(MagicElement _ElementType)
    {
        DamageData temp = new DamageData();
        temp.Type = _ElementType;
        temp.isCritical = false;

        BigDecimal BaseDmg = GetBaseDamage();
        BigDecimal ElementDmg = 0;

        //속성 추가 데미지 구하기
        ElementDmg = GetElementDamage(BaseDmg, _ElementType);

        //스킬 추가 데미지 구하기
        switch (_ElementType)
        {
            case MagicElement.Fire:
                temp.Damage = (BaseDmg + ElementDmg) * (UserData_Manager.Get.NowValue[(int)UpgradeType.Meteor] + UserData_Manager.Get.GetArtifactValue(ArtifactType.Meteorite_Hairpin, 0.0f));
                break;
            case MagicElement.Water:
                temp.Damage = (BaseDmg + ElementDmg) * (UserData_Manager.Get.NowValue[(int)UpgradeType.AquaLaser] + UserData_Manager.Get.GetArtifactValue(ArtifactType.Ice_Hairpin, 0.0f));
                break;
            case MagicElement.Earth:
                temp.Damage = (BaseDmg + ElementDmg) * (UserData_Manager.Get.NowValue[(int)UpgradeType.LeafStrom] + UserData_Manager.Get.GetArtifactValue(ArtifactType.Leaf_Hairpin, 0.0f));
                break;
        }
        //다단 히트라서 나눠서 데미지를 적용
        temp.Damage /= 6;

        //특전  데미지
        GetPerkDamagePlusMultiply(ref temp.Damage);

        temp.Damage = temp.Damage.Round();

        return temp;
    }

    void GetPerkDamagePlusMultiply(ref BigDecimal _origin)
    {
        //특전 
        //2배 +3배 적용~
        int damageModified = 0;
        if (LocalData_Manager.Get.GetPerkTime(BonusType.DamagePlus) > 0)
            damageModified += 2;

        if (LocalData_Manager.Get.GetPerkTime(BonusType.DamagePlus_Big) > 0)
            damageModified += 3;


        if(damageModified != 0)
            _origin *= damageModified;
    }
    float GetDamageMultiply(UpgradeType _type)
    {
        return UserData_Manager.Get.NowValue[(int)_type] * 0.01f;
    }
    BigDecimal GetElementDamage(BigDecimal _baseDmg ,MagicElement _type)
    {
        //속성 추가 데미지 구하기
        switch (_type)
        {
            case MagicElement.Fire:
                return _baseDmg * (GetDamageMultiply(UpgradeType.Fire) * UserData_Manager.Get.GetArtifactValue(ArtifactType.Necklace_of_Fire, 1.0f));
            case MagicElement.Water:
                return _baseDmg * (GetDamageMultiply(UpgradeType.Water) * UserData_Manager.Get.GetArtifactValue(ArtifactType.Necklace_of_Water, 1.0f));
            case MagicElement.Earth:
                return _baseDmg * (GetDamageMultiply(UpgradeType.Earth) * UserData_Manager.Get.GetArtifactValue(ArtifactType.Necklace_of_Earth, 1.0f));
        }

        return 0;
    }
    BigDecimal GetBaseDamage()
    {
        return UserData_Manager.Get.NowAttack * UserData_Manager.Get.GetArtifactValue(ArtifactType.Senior_Alumni_Staff, 1.0f);
    }

    #endregion

    #region 재접속 보상 체크
    //어플 일시정지시 재접속 보상 처리
    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            //현재 시간 저장
            LocalData_Manager.Get.SaveExitTime();
        }
        else
        {
            //보상 체크
            CheckReconnectReward();
        }
    }

    //어플 종료시 ~
    private void OnApplicationQuit()
    {
        //현재 시간 저장
        LocalData_Manager.Get.SaveExitTime();
    }

    //받을 보상이 있는지 체크
    void CheckReconnectReward()
    {
        //Debug.LogFormat("게임 재개 시간! {0}", System.DateTime.Now.ToLongTimeString());

        //보상 받을 시간 저장
        ReconnectTime = LocalData_Manager.Get.CalcReconnectTime();
        //Debug.LogFormat("보상 받을 시간 : {0}", ReconnectTime);
        //1분 이상일 경우만 보상을 받는다
        UI_Manager.Get.ReConnectBtnActive(ReconnectTime >= 1);
    }

    public void GetReconnectReward()
    {
        //1분 미만이면 버그임, 버튼이 안보여야됨
        if (ReconnectTime < 1)
            return;

        BigDecimal temp = 0;
        //24시간 이상은 1440분(24시간->분) 보상을 준다
        if (ReconnectTime < ConstValue.Hour24_To_Min)
            temp = FormulaDefine.ReconnectReward() * ReconnectTime;
        else
            temp = FormulaDefine.ReconnectReward() * ConstValue.Hour24_To_Min;

        temp = temp.Round();

        UI_Manager.Get.MakeReconnectRewardEffect(temp);

        //보상 받은뒤엔 데이터 리셋+UI 꺼주기
        UI_Manager.Get.ReConnectBtnActive(false);
        ReconnectTime = 0;
        PlayerPrefs.SetString("ExitGame_Ticks", "0");
    }
    #endregion

    #region 파편 수색대
    int ReconMaxTime = 0;
    public void CheckRecon()
    {
        //1초마다 증가
        ReconTime++;

        ReconMaxTime = UserData_Manager.Get.NowValue[(int)UpgradeType.Recon] * 3;

        if (ReconTime > ReconMaxTime)
            ReconTime = ReconMaxTime;

        float MaxTime = (float)ReconTime / ReconMaxTime;

        //UI갱신
        UI_Manager.Get.Recon.SetFilledSprite(MaxTime);
    }

    //수색보상 버튼을 눌렀을때~
    public void GetReconReward()
    {
        Debug.LogFormat("수색대 시간 : {0} 보상 가능 시간 : {1}", ReconTime, UserData_Manager.Get.NowValue[(int)UpgradeType.Recon]);

        //조건 최소 시간을 만족 못했으므로 보상 안줌
        if (ReconTime < UserData_Manager.Get.NowValue[(int)UpgradeType.Recon])
            return;

        BigDecimal temp;
        //1차 보상 == 2차 도달점 보다 작을때
        if (ReconTime < UserData_Manager.Get.NowValue[(int)UpgradeType.Recon] * 2)
            temp = FormulaDefine.Recon_RewardFregment(0);
        //2차 보상 == 3차 도달점 보다 작을떄
        else if (ReconTime < UserData_Manager.Get.NowValue[(int)UpgradeType.Recon] * 3)
            temp = FormulaDefine.Recon_RewardFregment(1);
        //3차 보상 == 3차 도달점과 동일 하거나 클때
        else
            temp = FormulaDefine.Recon_RewardFregment(2);
        
        temp = temp.Round();

        //이펙트 활성화
        UI_Manager.Get.MakeReconRewardEffect(temp);

        Debug.LogFormat("수색대 보상 금액 : {0}", temp.ToString());

        //초기화
        ReconTime = 0;
        UI_Manager.Get.Recon.SetFilledSprite(0);
    }

    #endregion

}
