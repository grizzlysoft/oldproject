﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using System;

public class Ads_Manager : Singleton<Ads_Manager>
{
    private string
        androidGameId = "1727588";


    [HideInInspector]
    public bool AdMobReward = false;

    ShowAdType Type = ShowAdType.Max;
    int Reward_ID = -1;

    public void Ads_ManagerInit()
    {
        string gameId = null;

#if UNITY_ANDROID
        gameId = androidGameId;
#elif UNITY_IOS
        gameId = iosGameId;
#endif

#if !UNITY_EDITOR
        if (Advertisement.isSupported && !Advertisement.isInitialized)
        {
            Advertisement.Initialize(gameId, false);
        }
#endif
    }

    public void ShowAd_UnityAds(ShowAdType type_, int rewardid_)
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
            Reward_ID = rewardid_;
            Type = type_;

            Popup_Manager.Get.Loading.Show();
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                //Debug.Log("The ad was successfully shown.");

                if(-1 != Reward_ID)
                {
                    //보상지급
                    PaymentManager.Get.PayPrice(Reward_ID);
                }

                switch(Type)
                {
                    case ShowAdType.MagicDust:
                        UserData_Manager.Get.AddMagicDust(ConstValue.MagicDust_Price);
                        break;
                    case ShowAdType.Bonus_GetMagicStone:
                        LocalData_Manager.Get.SetPerkTime(BonusType.GetMagicStone, ConstValue.Perk_AD_Delay);
                        UserData_Manager.Get.AddMagicStone(FormulaDefine.PerkMagicStone());
                        UserData_Manager.Get.AddAchievmentValue(AchievementType.UseBonus, 1);
                        break;
                    case ShowAdType.Bonus_GetDamage: //데미지 증가 특전 처리
                        LocalData_Manager.Get.SetPerkTime(BonusType.DamagePlus, ConstValue.PerkTime_AD);
                        UserData_Manager.Get.AddAchievmentValue(AchievementType.UseBonus, 1);
                        break;
                    case ShowAdType.Bonus_GetFocus: //집중 특전 처리
                        LocalData_Manager.Get.SetPerkTime(BonusType.Focus, ConstValue.PerkTime_AD);
                        UserData_Manager.Get.AddAchievmentValue(AchievementType.UseBonus, 1);
                        break;
                    case ShowAdType.Skill_Meteor:
                        UI_Manager.Get.Skillbar.skillSlot[0].CastSpell();
                        break;
                    case ShowAdType.Skill_SummonFire:
                        UI_Manager.Get.Skillbar.skillSlot[1].CastSpell();
                        break;
                    case ShowAdType.Skill_AquaLaser:
                        UI_Manager.Get.Skillbar.skillSlot[2].CastSpell();
                        break;
                    case ShowAdType.Skill_SummonWater:
                        UI_Manager.Get.Skillbar.skillSlot[3].CastSpell();
                        break;
                    case ShowAdType.Skill_LeafStrom:
                        UI_Manager.Get.Skillbar.skillSlot[4].CastSpell();
                        break;
                    case ShowAdType.Skill_SummonEarth:
                        UI_Manager.Get.Skillbar.skillSlot[5].CastSpell();
                        break;
                    case ShowAdType.DoubleReward:
                        Popup_Manager.Get.EndBattle.GetReward(2);
                        break;
                    case ShowAdType.Retry:
                        Ingame_Manager.Get.RetryBattle();
                        break;
                }

                //광고 보기 업적 카운트 ++
                UserData_Manager.Get.AddAchievmentValue(AchievementType.ShowAD, 1);

                UI_Manager.Get.Menu.ReFreshBonusItems();

                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }

        Type = ShowAdType.Max;
        Reward_ID = -1;

        Popup_Manager.Get.Loading.Hide();
    }

    private void OnDestroy()
    {

    }

}

