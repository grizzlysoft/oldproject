﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CognitioConsulting.Numerics;

public class UI_Manager : Singleton<UI_Manager>
{
    public GameMenu Menu;
    public UILabel MagicStone;
    public GameObject Btn_ReConnectBtn;
    public ReconUI Recon;
    public SkillBar Skillbar;
    public EnemyInfoUI EnmeyInfo;
    public CharacterInfoUI CharacterInfo;
    public DamageTextObjectPool op_DamageText;
    public NoticeTextPool op_NoticeText;
    public BuffUI Buff;

    //블러드문 UI 사용 
    public UILabel Label_Timer;
    public UISprite Sprite_Pause;



    [SerializeField] GameObject DefaultUI;
    [SerializeField] GameObject SpecialUI;
    
    //public UILabel TestLog;
    [SerializeField] UISprite sp_MenuButton;
    [SerializeField] GameObject go_ReconReward;
    [SerializeField] Transform tr_EffectPos;
    [SerializeField] Transform tr_FairyPos;
    [SerializeField] GameObject go_Fairy;
    GameObject go_CurrentFairy;

    public List<Font> li_Font = new List<Font>();

    public void Init()
    {
        Menu.MenuInit();
        UpdateMagicStoneText();
        Skillbar.Init();
        op_DamageText.Init();
        op_NoticeText.Init();

        SetUI_IngameMode(SpecialDungeonType.Normal);

        //광고 보상 요정 인보크
        InvokeRepeating("MakeFairy", 60, 60);
    }


    public void SetUI_IngameMode(SpecialDungeonType _type)
    {
        DefaultUI.SetActive(false);
        SpecialUI.SetActive(false);

        switch (_type)
        {
            case SpecialDungeonType.BloodMoon:
            case SpecialDungeonType.FullMoon:
                if (go_CurrentFairy != null)
                    Destroy(go_CurrentFairy);

                SpecialUI.SetActive(true);
                break;
            case SpecialDungeonType.Normal:
                DefaultUI.SetActive(true);
                break;
            default:
                break;
        }
    }

    public void UpdateMagicStoneText()
    {
        MagicStone.text = NumericalFormatter.Format(UserData_Manager.Get.MagicStone);
    }

    public void OnClickedBtnMenu()
    {
        if (Menu.Tweenpos.isActiveAndEnabled) return;

        Sound_Manager.Get.PlayEffect("Effect_bookflip");

        if (!Menu.IsOpen)
        {
            sp_MenuButton.spriteName = "btn03";
            Menu.MenuOpen();
        }
        else
        {
            sp_MenuButton.spriteName = "btn01";
            Menu.CloseMenu();
        }
    }

    public void ReConnectBtnActive(bool active_)
    {
        Btn_ReConnectBtn.SetActive(active_);
    }

    public void OnClickedBtnSpecialDungeon()
    {
        Sound_Manager.Get.PlayEffect("Effect_click");
        Popup_Manager.Get.SpecialDungeon.Show();
    }

    
    public void OnClickedBtnReConnect()
    {
        //재접속 보상
        Ingame_Manager.Get.GetReconnectReward();
    }

    public void OnClick_Achieve()
    {
        //업적
        Sound_Manager.Get.PlayEffect("Effect_bookflip");
        Server_Manager.Get.ShowAchievement();
    }

    public void OnClick_LeaderBoard()
    {
        //리더보드        
        Sound_Manager.Get.PlayEffect("Effect_bookflip");
        Server_Manager.Get.ShowLeaderBoard();
    }

    public void OnClick_Setting()
    {
        Sound_Manager.Get.PlayEffect("Effect_bookflip");
        //옵션 팝업
        Popup_Manager.Get.Option.Show();
    }

    public void OnClick_CloudSave()
    {
        Sound_Manager.Get.PlayEffect("Effect_bookflip");
        Popup_Manager.Get.YesNo.Show(LM.GetText(255), OnClick_PopupSaveOk);
    }

    public void OnClick_CloudLoad()
    {
        Sound_Manager.Get.PlayEffect("Effect_bookflip");
        if(LocalData_Manager.Get.CheckLoadTime())
        {
            Popup_Manager.Get.YesNo.Show(LM.GetText(254), OnClick_PopupLoadOk);
        }
        else
        {
            Popup_Manager.Get.Check.Show(string.Format(LM.GetText(256), LocalData_Manager.Get.GetLoadTimeDelay()));
        }
    }

    public void MakeReconRewardEffect(BigDecimal _value)
    {
        GameObject go =  Instantiate(go_ReconReward, tr_EffectPos) as GameObject;
        go.transform.position = Recon.sp_GetButton.transform.position;
        MoveReward temp = go.GetComponent<MoveReward>();
        temp.Init("ReconReward@Move", _value);
    }

    public void MakeReconnectRewardEffect(BigDecimal _value)
    {
        GameObject go = Instantiate(go_ReconReward, tr_EffectPos) as GameObject;
        go.transform.position = Btn_ReConnectBtn.transform.position;
        MoveReward temp = go.GetComponent<MoveReward>();
        temp.Init("ReconnectReward@Move", _value);
    }

    void OnClick_PopupSaveOk()
    {
        Server_Manager.Get.SaveToCloud();
    }

    void OnClick_PopupLoadOk()
    {
        Server_Manager.Get.LoadFromCloud();
    }

    void MakeFairy()
    {
        go_CurrentFairy = Instantiate(go_Fairy, tr_FairyPos) as GameObject;
    }
    
    public void OnClick_Pause()
    {
        Sound_Manager.Get.PlayEffect("Effect_click");
        SetTime_Off();
        Popup_Manager.Get.YesNo.Show(LM.GetText(270) , GoNormalGame, SetTime_On );
    }
    
    void GoNormalGame()
    {
        SetTime_On();
        Ingame_Manager.Get.ChangeNormalStage();
    }

    void SetTime_On()
    {
        sp_MenuButton.spriteName = "btn01";
        Time.timeScale = 1;
    }

    void SetTime_Off()
    {
        sp_MenuButton.spriteName = "btn03";
        Time.timeScale = 0;
    }

}

