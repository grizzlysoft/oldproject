﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HappCoupon
{
    public string result;
    public string error_code;
    public string error_msg;
    public string benefit;
    public C_Items[] items;
}

[System.Serializable]
public class C_Items
{
    public string item_code = "Empty";
    public string item_count = "Empty";
}


public class CouponManager : Singleton<CouponManager>
{
    [HideInInspector]
    public int GamsaCouponGet = 0;

    public void CouponManagerInit()
    {
        GamsaCouponGet = PlayerPrefs.GetInt("Gamsa");
    }

    public void UseHungryAppCoupon(string s)
    {
        if (s == "GAMSAYOM")
        {
            if (GamsaCouponGet == 0)
            {
                GamsaCouponGet = 1;
                PlayerPrefs.SetInt("Gamsa", GamsaCouponGet);
            }
            else
            {
            }
        }
        else
        {
#if UNITY_EDITOR
            WWW w = new WWW("http://couponsystems.kr/api/UseCoupon.php?key=2cc8f6463bba0769&coupon=" + s + "&id=" + "khdragon1102@gmail.com7");
            Popup_Manager.Get.Loading.Show();
            StartCoroutine(ConnentHungryAppCoupon(w));
#else
            WWW w = new WWW("http://couponsystems.kr/api/UseCoupon.php?key=2cc8f6463bba0769&coupon=" + s + "&id=" + Social.localUser.id);
            Popup_Manager.Get.Loading.Show();
            StartCoroutine(ConnentHungryAppCoupon(w));
#endif

        }

    }

    IEnumerator ConnentHungryAppCoupon(WWW w)
    {
        yield return w;

        Popup_Manager.Get.Loading.Hide();

        if (w == null)
        {
            Debug.Log("w = null!");
        }
        else if (w != null)
        {
            //Debug.Log("www error = " + w.error);
            HappCoupon c = new HappCoupon();
            Debug.Log(w.text);
            c = JsonUtility.FromJson<HappCoupon>(w.text);
            if (c.error_code == null)
            {
                Debug.Log("www.text = " + w.text);
                Debug.Log(c.result);
                Debug.Log(c.error_code);
                Debug.Log(c.error_msg);
                Debug.Log(c.benefit);

                for (int i = 0; i < c.items.Length; ++i)
                {
                    Debug.Log("item code = " + c.items[i].item_code);
                    Debug.Log("item count = " + c.items[i].item_count);
                    PaymentManager.Get.PayPrice(int.Parse(c.items[i].item_code));
                }

                Popup_Manager.Get.Check.Show(LM.GetText(262));
            }
            else
            {
                Popup_Manager.Get.Check.Show(c.error_msg , null , false);
            }
        }
        
    }
}
