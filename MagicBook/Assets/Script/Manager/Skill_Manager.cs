﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CognitioConsulting.Numerics;

public class Skill_Manager : MonoBehaviour 
{
    [SerializeField] GameObject go_Spirit;
    [SerializeField] List<GameObject> li_Magic = new List<GameObject>();
    [SerializeField] List<Transform> li_SpiritTransform = new List<Transform>();
    List<ElementSpirit> li_Spirit = new List<ElementSpirit>();
    public void Init()
    {
        GameObject go;
        ElementSpirit tempScp;

        //3속성 만큼 정령 만들어 두기
        for (int i = 0; i < 3; i++)
        {
            go = Instantiate(go_Spirit, li_SpiritTransform[i]) as GameObject;
            tempScp = go.GetComponent<ElementSpirit>();
            tempScp.Init((MagicElement)i);
            li_Spirit.Add(tempScp);
        }
    }


#if UNITY_EDITOR
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            CastAttackSpell(MagicElement.Fire);
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            CastAttackSpell(MagicElement.Water);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            CastAttackSpell(MagicElement.Earth);
        }

        if (Input.GetKeyDown(KeyCode.Z) == true)
            UserData_Manager.Get.AddMagicDust(500000);

        if (Input.GetKeyDown(KeyCode.X) == true)
            UserData_Manager.Get.AddMagicStone(BigDecimal.Parse("1000000000000000000000000000000000"));
    }
#endif

    public int GetManaCost(UpgradeType _type)
    {
        switch (_type)
        {
            case UpgradeType.Meteor:
            case UpgradeType.AquaLaser:
            case UpgradeType.LeafStrom:
                return ConstValue.ManaCost_AttackSpell;
            case UpgradeType.SummonFire:
            case UpgradeType.SummonWater:
            case UpgradeType.SummonEarth:
                return ConstValue.ManaCost_SummonSpell;
        }

        return 0;
    }
    
    /// <summary>
    /// 리턴값 = 해당 스킬 쿨타임
    /// </summary>
    public float SummonSpirit(MagicElement _ElementType)
    {
        li_Spirit[(int)_ElementType].Show();
        UserData_Manager.Get.AddAchievmentValue(AchievementType.UseSkill, 1);

        //쿨타임 리턴 (쿨타임은 현재 레벨의 지속시간)
        switch (_ElementType)
        {
            case MagicElement.Fire:
                return UserData_Manager.Get.NowValue[(int)UpgradeType.SummonFire];
            case MagicElement.Water:
                return UserData_Manager.Get.NowValue[(int)UpgradeType.SummonWater];
            case MagicElement.Earth:
                return UserData_Manager.Get.NowValue[(int)UpgradeType.SummonEarth];
        }

        return 0;
    }


    /// <summary>
    /// 리턴값 = 해당 스킬 쿨타임
    /// </summary>
    public float CastAttackSpell(MagicElement _ElementType)
    {
        //캐릭터 애니메이션 변경 
        Ingame_Manager.Get.character.CastSpell();

        //마법 생성
        GameObject go = Instantiate(li_Magic[(int)_ElementType], this.transform) as GameObject;
        UserData_Manager.Get.AddAchievmentValue(AchievementType.UseSkill, 1);
        return ConstValue.CoolTime_AttackSpell;
    }


    public void Hide_AllSummon()
    {
        for(int i=0; i<li_Spirit.Count; i++)
        {
            li_Spirit[i].Hide();
        }
    }
}
