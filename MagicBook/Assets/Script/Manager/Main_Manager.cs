﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main_Manager : MonoBehaviour
{
    [SerializeField] TitleAnim title;
    float ServerConnectDelay = 0f;
    public bool UserDataClear = false;
    public bool NonServer = false;
    //나중에 게임 최초 로딩 같은거 여기서 관리
    private void Awake()
    {
        //절전모드 off
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        //타이틀 화면 시작
        StartCoroutine(StartTitle());
    }

    IEnumerator StartTitle()
    {
        title.lb_Loading.text = "Loading...";

        //선행 클래스 초기화
        Table_Manager.Get.LoadTable();
        Popup_Manager.Get.Init();

        //인터넷 연결 체크
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            Popup_Manager.Get.Check.Show(LM.GetText(259), Application.Quit);
            while (true)
            {
                yield return null;
            }
        }

        Ads_Manager.Get.Ads_ManagerInit();

        title.lb_Loading.text = "Sound Check...";
        Sound_Manager.Get.Init();
        title.lb_Loading.text = "Purchasing Check...";
        Purchasing.Get.InitPurchasing();

#if UNITY_EDITOR
        if (UserDataClear)
            UserDataClear_Prefab();
#endif

        
       
         
        title.lb_Loading.text = "UserData Check...";
        UserData_Manager.Get.UserDataManagerInit();
        title.lb_Loading.text = "LocalData Check...";
        LocalData_Manager.Get.LoadLocalData();


        title.lb_Loading.text = "Ingame Check...";
        //게임 클래스 초기화
        UI_Manager.Get.Init();
        Ingame_Manager.Get.Init();
        yield return null;

        title.lb_Loading.text = "GPGS Login...";

        ////데이터 로드
        Server_Manager.Get.Init();

        //게임 Start 준비
        title.lb_Loading.text = "Making Pupet";        
        Ingame_Manager.Get.ChangeNormalStage();

        yield return new WaitForSeconds(2.0f);

        //게임 준비 완료
        title.lb_Loading.text = "";
        title.SetReadyIcon();
    }

    void ServerTimeout()
    {
        Application.Quit();
    }

    void UserDataClear_Prefab()
    {
        PlayerPrefs.DeleteAll();
        //PlayerPrefs.DeleteKey("MagicStone");
        //PlayerPrefs.DeleteKey("MagicDust");
        //PlayerPrefs.DeleteKey("Stage");

        //for (int i = 0; i < (int)UpgradeType.Max; ++i)
        //{
        //    PlayerPrefs.DeleteKey("UpgradeLevel" + (UpgradeType)i);
        //}

        //for (int i = 0; i < (int)ArtifactType.Max; ++i)
        //{
        //    PlayerPrefs.DeleteKey("ArtifactLevel" + (ArtifactType)i);
        //}

        //for (int i = 0; i < (int)AchievementType.Max; ++i)
        //{
        //    PlayerPrefs.DeleteKey("NAid" + (AchievementType)i);
        //    PlayerPrefs.DeleteKey("AchievementValue" + (AchievementType)i);
        //}

        //int AchievementCount = PlayerPrefs.GetInt("AchievementCount");
        //PlayerPrefs.DeleteKey("AchievementCount");

        //for (int i = 1; i <= AchievementCount; ++i)
        //{
        //    PlayerPrefs.DeleteKey("AchievmentClear" + i);
        //}
    }
}
