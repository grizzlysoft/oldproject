﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CognitioConsulting.Numerics;

public class Table_Manager : Singleton<Table_Manager>
{
    public void LoadTable()
    {
        TableLoad("Upgrade", LoadTable_UpgradeData);
        TableLoad("Enemy", LoadTable_EnemyData);
        TableLoad("ElementDamage", LoadTable_ElementDamageData);
        TableLoad("Shop", LoadTable_ShopData);
        TableLoad("Achievements", LoadTable_AchievementData);
        TableLoad("Bonus", LoadTable_BonusData);
        TableLoad("TEXT", LoadTable_TextData);
        TableLoad("Reward", LoadTable_RewardData);
        TableLoad("Stage_Reward", LoadTable_StageRewardData);
        TableLoad("SpecialDungeon", LoadTable_SpecialDungeonData);
        TableLoad("MagicTool", LoadTable_ArtifactData);
        TableLoad("Raid_Reward", LoadTable_RaidRewardData);
        LoadTable_UpgradeCost();
    }

    ////////////테이블 파싱////////////////////////////////////
    delegate void Parse(string[] st);
    void TableLoad(string Name, Parse del)
    {
        TextAsset txObj = Resources.Load(string.Format("Table/{0}", Name), typeof(TextAsset)) as TextAsset;
        string[] strArray = txObj.text.Split('\n');
        for (int i = 0; i < strArray.Length; i++)
        {
            if (strArray[i] == "" || strArray[i][0] == '|') break;

            strArray[i] = strArray[i].Replace("\\n", "\n");
            string[] stral = strArray[i].Split('|');
            //Debug.Log(Name + " Parse : " + i);
            del(stral);
        }
    }
    static List<int> GetListData(string _ar)
    {
        List<int> temp = new List<int>();
        string[] ar = _ar.Split(',');

        if (ar[0] != "")
        {
            for (int i = 0; i < ar.Length; i++)
            {
                temp.Add(int.Parse(ar[i]));
            }
        }

        return temp;
    }
    ///////////////////////////////////////////////////////////

    #region Upgrade
    public class UpgradeData
    {
        public readonly UpgradeType ID;
        public readonly int UnLock;
        public readonly int Name;
        public readonly int Desc;
        public readonly int Lv1Abillity;
        public readonly int Lvup_Abillity;
        public readonly int MaxLevel;
        public readonly UpgradeUnlock UnlockType;
        public readonly int UnlockValue;

        public UpgradeData(string[] stral)
        {
            int cnt = 0;
            ID = (UpgradeType)int.Parse(stral[cnt]); cnt++;
            UnLock = int.Parse(stral[cnt]); cnt++;
            Name = int.Parse(stral[cnt]); cnt++;
            Desc = int.Parse(stral[cnt]); cnt++;
            Lv1Abillity = int.Parse(stral[cnt]); cnt++;
            Lvup_Abillity = int.Parse(stral[cnt]); cnt++;
            MaxLevel = int.Parse(stral[cnt]); cnt++;
            UnlockType = (UpgradeUnlock)int.Parse(stral[cnt]); cnt++;
            UnlockValue = int.Parse(stral[cnt]); cnt++;
        }
    }
    public List<UpgradeData> li_UpgradeData = new List<UpgradeData>();
    void LoadTable_UpgradeData(string[] stral)
    {
        li_UpgradeData.Add(new UpgradeData(stral));
    }
    #endregion

    #region EnemyData
    public class EnemyData
    {
        public readonly int EnemyLv;
        public readonly int Name;

        public EnemyData(string[] stral)
        {
            int cnt = 0;
            EnemyLv = int.Parse(stral[cnt]); cnt++;
            Name = int.Parse(stral[cnt]); cnt++;
        }
    }
    public List<EnemyData> li_EmenyData = new List<EnemyData>();
    void LoadTable_EnemyData(string[] stral)
    {
        li_EmenyData.Add(new EnemyData(stral));
    }
    #endregion

    #region ElementDamage
    public class ElementDamageData
    {
        public readonly MagicElement AtkElement;
        public readonly MagicElement DefElement;
        public readonly float value;

        public ElementDamageData(string[] stral)
        {
            int cnt = 0;
            AtkElement = (MagicElement)int.Parse(stral[cnt]); cnt++;
            DefElement = (MagicElement)int.Parse(stral[cnt]); cnt++;
            value = float.Parse(stral[cnt]); cnt++;
        }
    }
    List<ElementDamageData> li_ElementDamageData = new List<ElementDamageData>();
    void LoadTable_ElementDamageData(string[] stral)
    {
        li_ElementDamageData.Add(new ElementDamageData(stral));
    }

    public float GetElementDamageValue(MagicElement _ATK, MagicElement _DEF)
    {
        for(int i=0; i<li_ElementDamageData.Count; i++)
        {
            if(li_ElementDamageData[i].AtkElement == _ATK &&
               li_ElementDamageData[i].DefElement == _DEF)
            {
                return li_ElementDamageData[i].value;
            }
        }

        Debug.LogError("잘못된 속성 반환값!!");
        return 0;
    }

    #endregion

    #region ShopData
    public class ShopData
    {
        public readonly int ID;
        public readonly PayMentType PayType;
        public readonly int Cost;
        public readonly int Name;
        public readonly string StoreID;
        public readonly int RewardID;

        public ShopData(string[] stral)
        {
            int cnt = 0;
            ID = int.Parse(stral[cnt]); cnt++;
            PayType = (PayMentType)int.Parse(stral[cnt]); cnt++;
            Cost = int.Parse(stral[cnt]); cnt++;
            Name = int.Parse(stral[cnt]); cnt++;
            StoreID = stral[cnt]; cnt++;
            RewardID = int.Parse(stral[cnt]); cnt++;
        }
    }
    public List<ShopData> li_ShopData = new List<ShopData>();
    void LoadTable_ShopData(string[] stral)
    {
        li_ShopData.Add(new ShopData(stral));
    }
    #endregion

    #region AchievementData
    public class AchievementData
    {
        public readonly int ID;
        public readonly AchievementType Type;
        public readonly int Unlock;
        public readonly int Name;
        public readonly int Clear_Value;
        public readonly int MaxVlue;
        public readonly int NextID;
        public readonly int FirstCheck;
        public readonly int RewardID;
        public readonly string Gpgs_AchievementID;

        public AchievementData(string[] stral)
        {
            int cnt = 0;
            ID = int.Parse(stral[cnt]); cnt++;
            Type = (AchievementType)int.Parse(stral[cnt]); cnt++;
            Unlock = int.Parse(stral[cnt]); cnt++;
            Name = int.Parse(stral[cnt]); cnt++;
            Clear_Value = int.Parse(stral[cnt]); cnt++;
            MaxVlue = int.Parse(stral[cnt]); cnt++;
            NextID = int.Parse(stral[cnt]); cnt++;
            FirstCheck = int.Parse(stral[cnt]); cnt++;
            RewardID = int.Parse(stral[cnt]); cnt++;
            Gpgs_AchievementID = stral[cnt]; cnt++;
        }
    }
    public List<AchievementData> li_AchievementData = new List<AchievementData>();
    void LoadTable_AchievementData(string[] stral)
    {
        li_AchievementData.Add(new AchievementData(stral));
    }

    public AchievementData GetFirstAchievement(AchievementType type_)
    {
        return li_AchievementData.Find(r => r.FirstCheck == 1 && r.Type == type_);
    }
    #endregion

    #region BonusData
    public class BonusData
    {
        public readonly BonusType ID;
        public readonly int Name;
        public readonly int Cost;
        public readonly int TimeVlue;
        public readonly int TextID;

        public BonusData(string[] stral)
        {
            int cnt = 0;
            ID = (BonusType)int.Parse(stral[cnt]); cnt++;
            Name = int.Parse(stral[cnt]); cnt++;
            Cost = int.Parse(stral[cnt]); cnt++;
            TimeVlue = int.Parse(stral[cnt]); cnt++;
            TextID = int.Parse(stral[cnt]); cnt++;
        }
    }
    public List<BonusData> li_BonusData = new List<BonusData>();
    void LoadTable_BonusData(string[] stral)
    {
        li_BonusData.Add(new BonusData(stral));
    }
    #endregion

    #region TextData
    public class TextData
    {
        public readonly int ID;
        public readonly string[] Text = new string[(int)Language.Max];

        public TextData(string[] stral)
        {
            int cnt = 0;
            ID = int.Parse(stral[cnt]); cnt++;
            for(int i = 0; i < (int)Language.Max; ++i)
            {
                Text[i] = stral[cnt]; cnt++;
            }
        }
    }
    public List<TextData> li_TextData = new List<TextData>();
    void LoadTable_TextData(string[] stral)
    {
        li_TextData.Add(new TextData(stral));
    }


    #endregion

    #region RewardData
    public class RewardData
    {
        public readonly int ID;
        public readonly RewardType Type;
        public readonly string Name;
        public readonly int Count;

        public RewardData(string[] stral)
        {
            int cnt = 0;
            ID = int.Parse(stral[cnt]); cnt++;
            Type = (RewardType)int.Parse(stral[cnt]); cnt++;
            Name = stral[cnt]; cnt++;
            Count = int.Parse(stral[cnt]); cnt++;
        }
    }
    public List<RewardData> li_RewardData = new List<RewardData>();
    void LoadTable_RewardData(string[] stral)
    {
        li_RewardData.Add(new RewardData(stral));
    }
    #endregion

    #region UpgradeCostData
    public class UpgradeCostData
    {
        public readonly int ID;
        public readonly BigDecimal Cost;

        public UpgradeCostData(string[] stral)
        {
            int cnt = 0;
            ID = int.Parse(stral[cnt]); cnt++;
            Cost = BigDecimal.Parse(stral[cnt]); cnt++;
        }
    }

    Dictionary<UpgradeType, List<UpgradeCostData>> dic_UpgradeCostData = new Dictionary<UpgradeType, List<UpgradeCostData>>();
    List<UpgradeCostData> li_UpgradeCostData = new List<UpgradeCostData>();

    public BigDecimal GetUpgradeCost(UpgradeType _type , int _Level)
    {
        return dic_UpgradeCostData[_type][_Level].Cost;
    }

    public int GetUpgradeMax(UpgradeType _type)
    {
        return dic_UpgradeCostData[_type].Count;
    }

    void LoadTable_UpgradeCost()
    {
        TableLoad("Cost_ATK", LoadTable_UpgradeCostData);
        dic_UpgradeCostData.Add(UpgradeType.Attack , li_UpgradeCostData);
        li_UpgradeCostData = new List<UpgradeCostData>();

        TableLoad("Cost_Critical", LoadTable_UpgradeCostData);
        dic_UpgradeCostData.Add(UpgradeType.Critical, li_UpgradeCostData);
        li_UpgradeCostData = new List<UpgradeCostData>();

        TableLoad("Cost_ManaMax", LoadTable_UpgradeCostData);
        dic_UpgradeCostData.Add(UpgradeType.ManaMax, li_UpgradeCostData);
        li_UpgradeCostData = new List<UpgradeCostData>();

        TableLoad("Cost_ManaCharge", LoadTable_UpgradeCostData);
        dic_UpgradeCostData.Add(UpgradeType.ManaCharge, li_UpgradeCostData);
        li_UpgradeCostData = new List<UpgradeCostData>();

        TableLoad("Cost_AutoCasting", LoadTable_UpgradeCostData);
        dic_UpgradeCostData.Add(UpgradeType.AutoCasting, li_UpgradeCostData);
        li_UpgradeCostData = new List<UpgradeCostData>();

        TableLoad("Cost_Recon", LoadTable_UpgradeCostData);
        dic_UpgradeCostData.Add(UpgradeType.Recon, li_UpgradeCostData);
        li_UpgradeCostData = new List<UpgradeCostData>();

        TableLoad("Cost_Puppet", LoadTable_UpgradeCostData);
        dic_UpgradeCostData.Add(UpgradeType.UnLockTraining, li_UpgradeCostData);
        li_UpgradeCostData = new List<UpgradeCostData>();


        TableLoad("Cost_Element", LoadTable_UpgradeCostData);
        dic_UpgradeCostData.Add(UpgradeType.Fire, li_UpgradeCostData);
        dic_UpgradeCostData.Add(UpgradeType.Water, li_UpgradeCostData);
        dic_UpgradeCostData.Add(UpgradeType.Earth, li_UpgradeCostData);
        li_UpgradeCostData = new List<UpgradeCostData>();

        TableLoad("Cost_AttackSkill", LoadTable_UpgradeCostData);
        dic_UpgradeCostData.Add(UpgradeType.Meteor, li_UpgradeCostData);
        dic_UpgradeCostData.Add(UpgradeType.AquaLaser, li_UpgradeCostData);
        dic_UpgradeCostData.Add(UpgradeType.LeafStrom, li_UpgradeCostData);
        li_UpgradeCostData = new List<UpgradeCostData>();

        TableLoad("Cost_Summon", LoadTable_UpgradeCostData);
        dic_UpgradeCostData.Add(UpgradeType.SummonFire, li_UpgradeCostData);
        dic_UpgradeCostData.Add(UpgradeType.SummonWater, li_UpgradeCostData);
        dic_UpgradeCostData.Add(UpgradeType.SummonEarth, li_UpgradeCostData);
    }

    void LoadTable_UpgradeCostData(string[] stral)
    {
        li_UpgradeCostData.Add(new UpgradeCostData(stral));
    }
    #endregion


    #region StageRewardData
    public class StageRewardData
    {
        public readonly int ID;
        public readonly BigDecimal HP;
        public readonly BigDecimal Perk_Small;
        public readonly BigDecimal Perk_Big;
        public readonly BigDecimal ClearReward_1;
        public readonly BigDecimal ClearReward_2;
        public readonly BigDecimal ClearReward_3;

        public StageRewardData(string[] stral)
        {
            int cnt = 0;
            ID = int.Parse(stral[cnt]); cnt++;
            HP = BigDecimal.Parse(stral[cnt]); cnt++;
            Perk_Small    = BigDecimal.Parse(stral[cnt]); cnt++;
            Perk_Big      = BigDecimal.Parse(stral[cnt]); cnt++;
            ClearReward_1 = BigDecimal.Parse(stral[cnt]); cnt++;
            ClearReward_2 = BigDecimal.Parse(stral[cnt]); cnt++;
            ClearReward_3 = BigDecimal.Parse(stral[cnt]); cnt++;

        }
    }
    public List<StageRewardData> li_StageRewardData = new List<StageRewardData>();
    void LoadTable_StageRewardData(string[] stral)
    {
        li_StageRewardData.Add(new StageRewardData(stral));
    }
    #endregion

    #region SpecialDungeonData
    public class SpecialDungeonData
    {
        public readonly SpecialDungeonType Type;
        public readonly int NameIndex;
        public readonly int DescIndex;
        public readonly string TextureName;
        public readonly int Cost;

        public SpecialDungeonData(string[] stral)
        {
            int cnt = 0;
            Type = (SpecialDungeonType)int.Parse(stral[cnt]); cnt++;
            NameIndex = int.Parse(stral[cnt]); cnt++;
            DescIndex = int.Parse(stral[cnt]); cnt++;
            TextureName = stral[cnt]; cnt++;
            Cost = int.Parse(stral[cnt]); cnt++;
        }
    }
    public List<SpecialDungeonData> li_SpecialDungeonData = new List<SpecialDungeonData>();
    void LoadTable_SpecialDungeonData(string[] stral)
    {
        li_SpecialDungeonData.Add(new SpecialDungeonData(stral));
    }
    #endregion

    #region Artifact
    public class ArtifactData
    {
        public readonly ArtifactType ID;
        public readonly int Name;
        public readonly int Desc;
        public readonly float Lv1Abillity;
        public readonly float Lvup_Abillity;
        public readonly int MaxLevel;
        public readonly int UnlockCost;
        public readonly int UpgradeCost;


        public ArtifactData(string[] stral)
        {
            int cnt = 0;
            ID = (ArtifactType)int.Parse(stral[cnt]); cnt++;
            Name = int.Parse(stral[cnt]); cnt++;
            Desc = int.Parse(stral[cnt]); cnt++;
            Lv1Abillity = float.Parse(stral[cnt]); cnt++;
            Lvup_Abillity = float.Parse(stral[cnt]); cnt++;
            MaxLevel = int.Parse(stral[cnt]); cnt++;
            UnlockCost = int.Parse(stral[cnt]); cnt++;
            UpgradeCost = int.Parse(stral[cnt]); cnt++;
        }
    }
    public List<ArtifactData> li_ArtifactData = new List<ArtifactData>();
    void LoadTable_ArtifactData(string[] stral)
    {
        li_ArtifactData.Add(new ArtifactData(stral));
    }
    #endregion

    #region RaidRewardData
    public class RaidRewardData
    {
        public readonly int LV;
        public readonly BigDecimal HP;
        public readonly RewardType rewardType;
        public readonly BigDecimal rewardAmount;

        public RaidRewardData(string[] stral)
        {
            int cnt = 0;
            LV = int.Parse(stral[cnt]); cnt++;
            HP = BigDecimal.Parse(stral[cnt]); cnt++;
            rewardType = (RewardType)int.Parse(stral[cnt]); cnt++;
            rewardAmount = BigDecimal.Parse(stral[cnt]); cnt++;
        }
    }
    public List<RaidRewardData> li_RaidRewardData = new List<RaidRewardData>();
    void LoadTable_RaidRewardData(string[] stral)
    {
        li_RaidRewardData.Add(new RaidRewardData(stral));
    }
    #endregion


}
