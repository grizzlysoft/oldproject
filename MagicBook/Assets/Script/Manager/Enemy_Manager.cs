﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CognitioConsulting.Numerics;

public class Enemy_Manager : MonoBehaviour 
{
    [SerializeField] GameObject go_Enemy;
    Enemy CurrentEnemyModel;
    [SerializeField] Enemy_Special Pupet_Special;

    //현재 일반 적군 정보
    public MagicElement ElementType { get; private set; }
    public BigDecimal MAX_HP { get; private set; }
    public BigDecimal HP { get; private set; }
    public bool isBoss { get; private set; }
    int Grade = 0;

    //블러드문 적군 받은 피해량
    public BigDecimal BloodMoon_HitDamage;
    Table_Manager.RaidRewardData FullMoonData;

    public void DeleteCurrentEnemy()
    {
        if (CurrentEnemyModel != null)
            CurrentEnemyModel.Hide();
    }

    public void CreateEnemy(SpecialDungeonType _type)
    {
        StopAllCoroutines();

        switch (_type)
        {
            case SpecialDungeonType.BloodMoon:
                Pupet_Special.ChangeSprite(_type);
                UI_Manager.Get.EnmeyInfo.SetUI(LM.GetText(274), 165 , false);
                UI_Manager.Get.EnmeyInfo.SetHP(HP / MAX_HP);
                Pupet_Special.Show();
                break;
            case SpecialDungeonType.FullMoon:
                Pupet_Special.ChangeSprite(_type);
                //현재 던전 정보 가져온다 - 스테이지 레벨, 남은 체력
                //적 남은 체력이 0이면 현재 스테이지 정보를 가져온다
                FullMoonData = Table_Manager.Get.li_RaidRewardData[UserData_Manager.Get.Stage_FullMoon];

                //체력 셋팅
                MAX_HP = FullMoonData.HP;
                HP = MAX_HP;
                if (LocalData_Manager.Get.FullMoon_RemainHP != "0")
                    HP = BigDecimal.Parse(LocalData_Manager.Get.FullMoon_RemainHP);
                
                UI_Manager.Get.EnmeyInfo.SetUI(string.Format("{0} {1}",UserData_Manager.Get.Stage_FullMoon , LM.GetText(317)), 230);
                UI_Manager.Get.EnmeyInfo.SetHP(HP / MAX_HP);

                Pupet_Special.Show();
                break;
            case SpecialDungeonType.Normal:
                {
                    Pupet_Special.Hide();

                    //10레벨 단위로 보스 리젠 == 현제 스테이지%10 == 0
                    if (UserData_Manager.Get.Stage % 10 == 0)
                    {
                        isBoss = true;
                        StartCoroutine(ChangeElement());
                    }
                    else
                    {
                        isBoss = false;
                    }

                    //데이터 셋팅
                    Grade = UserData_Manager.Get.Level_Upgrade[(int)UpgradeType.UnLockTraining] - 1;
                    ElementType = (MagicElement)Random.Range(0, 3);
                    MAX_HP = FormulaDefine.EnemyHP(isBoss);
                    HP = MAX_HP;

                    //생성-외형
                    GameObject temp = Instantiate(go_Enemy, this.transform) as GameObject;
                    CurrentEnemyModel = temp.GetComponent<Enemy>();
                    CurrentEnemyModel.Init(isBoss, ElementType);

                    //_Grade == 허수아비 등급 0부터 시작
                    string AddLine = "";

                    //보스 태그 달기
                    if (isBoss == true)
                        AddLine = string.Format("Boss Lv.{0}", UserData_Manager.Get.Stage);
                    else
                        AddLine = string.Format("Lv.{0}", UserData_Manager.Get.Stage);

                    UI_Manager.Get.EnmeyInfo.SetUI(string.Format(LM.GetText(220), AddLine), 124);
                    UI_Manager.Get.EnmeyInfo.SetHP(HP / MAX_HP);
                }
                break;
            default:
                break;
        }
    }
    

    IEnumerator ChangeElement()
    {
        while (true)
        {
            yield return new WaitForSeconds(5.0f);
            ElementType = (MagicElement)Random.Range(0, 3);
            ChangeImage();
        }
    }

    //UI 쪽에서 이미지 변경에 접근 하기 위해서~
    public void ChangeImage()
    {
        if (CurrentEnemyModel != null)
            CurrentEnemyModel.ChangeSprite(ElementType);
    }

    Table_Manager.EnemyData GetCurrentEnemyData()
    {
        return Table_Manager.Get.li_EmenyData[Grade];
    }
  

    //몬스터 피격시 데미지 증감 계산
    public void HitEnemy(DamageData _data)
    {
        switch (Ingame_Manager.Get.IngameType)
        {
            case SpecialDungeonType.BloodMoon:
                {
                    _data.Damage = _data.Damage * (UserData_Manager.Get.Stage * ConstValue.BloodMoonDamageMultiply);

                    //누적 데미지 계산
                    BloodMoon_HitDamage += _data.Damage;

                    //데미지 텍스트 나오기
                    UI_Manager.Get.op_DamageText.ShowDamageText(_data);
                    Pupet_Special.Hit();
                }
                break;
            case SpecialDungeonType.FullMoon:
                {
                    if (HP <= 0)
                        return;

                    _data.Damage = _data.Damage * (Ingame_Manager.Get.battleCount + 1);

                    //체력 깍기
                    HP -= _data.Damage;
                    UI_Manager.Get.EnmeyInfo.SetHP(HP / MAX_HP);

                    //데미지 텍스트 나오기
                    UI_Manager.Get.op_DamageText.ShowDamageText(_data);
                    if (HP <= 0)
                    {
                        //적 죽음 애니메이션 처리
                        Pupet_Special.Die();
                    }
                    else
                    {
                        Pupet_Special.Hit();
                    }
                }
                break;
            case SpecialDungeonType.Normal:
                {
                    if (CurrentEnemyModel == null)
                        return;

                    _data.Damage = _data.Damage * Table_Manager.Get.GetElementDamageValue(_data.Type, ElementType);

                    //체력 깍기
                    HP -= _data.Damage;
                    UI_Manager.Get.EnmeyInfo.SetHP(HP / MAX_HP);
                    //데미지 텍스트 나오기
                    UI_Manager.Get.op_DamageText.ShowDamageText(_data);

                    //데미지에 따른 돈 받기
                    UserData_Manager.Get.AddMagicStone(_data.Damage);

                    //Debug.LogFormat("적 체력 {0} 데미지 {1} ", HP, _data.Damage);
                    if (HP <= 0)
                    {
                        //돈 주기
                        UserData_Manager.Get.AddMagicStone(FormulaDefine.StageClearFregment(isBoss));
                        //스테이지 ++
                        UserData_Manager.Get.StepUpStage();
                        //적 죽음 애니메이션 처리
                        CurrentEnemyModel.Die();
                        //새로 리젠
                        CreateEnemy(SpecialDungeonType.Normal);
                    }
                    else
                    {
                        CurrentEnemyModel.Hit();
                    }
                }
                break;
        }
    }

}
