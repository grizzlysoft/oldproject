﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Numerics;
using CognitioConsulting.Numerics;

public class UserData_Manager : Singleton<UserData_Manager>
{
    public void UserDataManagerInit()
    {
        for(int i = 0; i < Level_Upgrade.Length; ++i)
        {
            if(Table_Manager.Get.li_UpgradeData[i].UnLock == 1 && Level_Upgrade[i] == 0)
            {
                Level_Upgrade[i] = 1;
                SetNowLevelValue((UpgradeType)i);
            }
        }

        InitAchievement();

        LocalUserDataLoad();
    }
    public void LocalUserDataLoad()
    {
        if (PlayerPrefs.HasKey("MagicStone"))
            MagicStone = BigDecimal.Parse(PlayerPrefs.GetString("MagicStone"));

        if (PlayerPrefs.HasKey("MagicDust"))
            MagicDust = PlayerPrefs.GetInt("MagicDust");

        if (PlayerPrefs.HasKey("Ruby"))
            Cash_Value = PlayerPrefs.GetInt("Ruby");

        if(PlayerPrefs.HasKey("Stage"))
            Stage = PlayerPrefs.GetInt("Stage");

        if (PlayerPrefs.HasKey("Stage_FullMoon"))
            Stage_FullMoon = PlayerPrefs.GetInt("Stage_FullMoon");

        for (int i = 0; i < (int)UpgradeType.Max; ++i)
        {
            if (PlayerPrefs.HasKey("UpgradeLevel" + (UpgradeType)i))
                Level_Upgrade[i] = PlayerPrefs.GetInt("UpgradeLevel" + (UpgradeType)i);
        }

        for (int i = 0; i < (int)ArtifactType.Max; ++i)
        {
            if (PlayerPrefs.HasKey("ArtifactLevel" + (ArtifactType)i))
                Level_Artifact[i] = PlayerPrefs.GetInt("ArtifactLevel" + (ArtifactType)i);
        }

        for (int i = 0; i < (int)AchievementType.Max; ++i)
        {
            if (PlayerPrefs.HasKey("NAid" + (AchievementType)i))
            {
                NowAchievementID[i] = PlayerPrefs.GetInt("NAid" + (AchievementType)i);
                if (NowAchievementID[i] == 0)
                    NowAchievementID[i] = Table_Manager.Get.GetFirstAchievement((AchievementType)i).ID;
            }

            if (PlayerPrefs.HasKey("AchievementValue" + (AchievementType)i))
                AchievementValue[i] = PlayerPrefs.GetInt("AchievementValue" + (AchievementType)i);
        }

        for (int i = 1; i <= AchievementClear.Count; ++i)
        {
            if (PlayerPrefs.GetInt("AchievmentClear" + i) == 0)
                AchievementClear[i] = false;
            else
                AchievementClear[i] = true;
        }

        for (int i = 0; i < (int)UpgradeType.Max; ++i)
            SetNowLevelValue((UpgradeType)i);

        for (int i = 0; i < (int)ArtifactType.Max; ++i)
            SetNowLevelValue_Artifact((ArtifactType)i);
    }
    //values
    /// <summary> 마법석 </summary>
    public BigDecimal MagicStone { get; private set; } = 0;

    /// <summary> 마법 가루 </summary>
    public int MagicDust { get; private set; } = 0;

    /// <summary> 캐시 </summary>
    public int Cash_Value { get; private set; } = 0;
    
    /// <summary> 스테이지 </summary>
    public int Stage { get; private set; } = 1;

    /// <summary> FullMoon 허수아비 레벨 </summary>
    public int Stage_FullMoon { get; private set; } = 1;


    #region NowValue
    /// 현재 레벨에 따른 데이터 값
    public int[] NowValue { get; private set; } = new int[(int)UpgradeType.Max];
    public BigDecimal NowAttack { get; private set; } = 0;

    void SetNowLevelValue(UpgradeType _type)
    {
        int _Level = Level_Upgrade[(int)_type];

        //테이블 가져오기
        Table_Manager.UpgradeData Find = Table_Manager.Get.li_UpgradeData[(int)_type];

        if(_type == UpgradeType.Attack)
        {
            BigDecimal temp = Find.Lvup_Abillity;
            temp *= (_Level - 1);
            NowAttack = Find.Lv1Abillity + temp;
        }
        else
        {
            int temp = Find.Lv1Abillity + (Find.Lvup_Abillity * (_Level - 1));
            NowValue[(int)_type] = temp;
        }
    }

    /// 현재 레벨에 따른 데이터 값
    public float[] NowValue_Artifact { get; private set; } = new float[(int)ArtifactType.Max];
    void SetNowLevelValue_Artifact(ArtifactType _type)
    {
        int _Level = Level_Artifact[(int)_type];

        //테이블 가져오기
        Table_Manager.ArtifactData Find = Table_Manager.Get.li_ArtifactData[(int)_type];

        float temp = Find.Lv1Abillity + (Find.Lvup_Abillity * (_Level - 1));
        NowValue_Artifact[(int)_type] = temp;
    }
    #endregion

    // 현재 업그레이드 레벨
    public int[] Level_Upgrade { get; private set; } = new int[(int)UpgradeType.Max];
    // 현재 마법 도구 업그레이드 레벨
    public int[] Level_Artifact { get; private set; } = new int[(int)ArtifactType.Max];
    //현재 진행 업적
    public int[] NowAchievementID { get; private set; } = new int[(int)AchievementType.Max];
    //업적 타입별 누적값
    public int[] AchievementValue { get; private set; } = new int[(int)AchievementType.Max];
    //업적 클리어 여부
    public Dictionary<int, bool> AchievementClear { get; private set; } = new Dictionary<int, bool>();

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void AddMagicStone(BigDecimal value_)
    {
        MagicStone += value_;
        //ui 갱신
        UI_Manager.Get.UpdateMagicStoneText();
    }

    public bool UseMasicStone(BigDecimal value_)
    {
        if (MagicStone < value_)
            return false;
        else
            MagicStone = MagicStone - value_;

        //ui 갱신
        UI_Manager.Get.UpdateMagicStoneText();
        return true;
    }


    public void AddMagicDust(int value_)
    {
        MagicDust += value_;
        PlayerPrefs.SetInt("MagicDust", MagicDust);

        //ui 갱신
        UI_Manager.Get.Menu.ReFreshArtifactItems();
        UI_Manager.Get.Menu.RefreshRuby();
    }

    public bool UseMasicDust(int value_)
    {
        if (MagicDust < value_)
            return false;
        else
            MagicDust = MagicDust - value_;

        PlayerPrefs.SetInt("MagicDust", MagicDust);

        //ui 갱신
        UI_Manager.Get.Menu.ReFreshArtifactItems();
        UI_Manager.Get.Menu.RefreshRuby();
        return true;
    }



    public void AddCash_Value(int value_)
    {
        Cash_Value += value_;
        PlayerPrefs.SetInt("Ruby", Cash_Value);
        //ui갱신
        UI_Manager.Get.Menu.RefreshRuby();
    }

    public bool UseCash_Value(int value_)
    {
        if (Cash_Value < value_)
        {
            Popup_Manager.Get.YesNo.Show(LM.GetText(268), DelYesNo_CashYes);
            return false;
        }
        else
            Cash_Value = Cash_Value - value_;

        PlayerPrefs.SetInt("Ruby", Cash_Value);
        //ui 갱신
        UI_Manager.Get.Menu.RefreshRuby();
        return true;
    }

    /// <summary>
    /// 다음 스테이지 넘어갈때 실행해줄것
    /// </summary>
    public void StepUpStage()
    {
        //최대 스테이지를 넘지 않을것
        if (Stage >= Table_Manager.Get.li_StageRewardData.Count - 1)
            return;

        Stage++;
        //진행 처리
        AddAchievmentValue(AchievementType.StageLevel, 1);

        //UI 갱신 - 특전 보너스
        UI_Manager.Get.Menu.ReFreshBonusItems();

        PlayerPrefs.SetInt("Stage", Stage);
        Server_Manager.Get.ReportScore(GPGSIds.leaderboard, Stage);
    }


    /// <summary>
    /// 다음 스테이지 넘어갈때 실행해줄것
    /// </summary>
    public void StepUpStage_FullMoon()
    {
        //최대 스테이지를 넘지 않을것
        if (Stage_FullMoon >= Table_Manager.Get.li_RaidRewardData.Count - 1)
            return;

        Stage_FullMoon++;
        LocalData_Manager.Get.SetFullMoon_RemainHP("0");
        PlayerPrefs.SetInt("Stage_FullMoon", Stage_FullMoon);
    }

    #region Upgrade
    //업그래이드 처리
    public void LevelupUpgrade(UpgradeType type_, int value_, BigDecimal cost_)
    {
        if (!UpgradeConditionCheck(type_))
        {
            //선행과정있음
            return;
        }

        if (UseMasicStone(cost_))
        {
            Sound_Manager.Get.PlayEffect("Effect_click");

            Level_Upgrade[(int)type_] += value_;
            SetNowLevelValue(type_);

            switch (type_)
            {
                case UpgradeType.Attack: AddAchievmentValue(AchievementType.AttackLevel, value_); break;
                case UpgradeType.Critical:AddAchievmentValue(AchievementType.CreticalLevel, value_); break;
                case UpgradeType.ManaMax: AddAchievmentValue(AchievementType.ManaMaxLevel, value_); break;
                case UpgradeType.AutoCasting: AddAchievmentValue(AchievementType.AutoCastingLevel, value_); break;
                case UpgradeType.Recon: AddAchievmentValue(AchievementType.SerchUnitLevel, value_); break;
                case UpgradeType.UnLockTraining: AddAchievmentValue(AchievementType.UnLockTraining, value_); break;
                case UpgradeType.Fire: AddAchievmentValue(AchievementType.LevelupFire, value_); break;
                case UpgradeType.Water: AddAchievmentValue(AchievementType.LevelupWater, value_); break;
                case UpgradeType.Earth: AddAchievmentValue(AchievementType.LevelupEarth, value_); break;
            }

            //인게임 갱신 필요할떄
            if(type_ == UpgradeType.UnLockTraining)
            {
                Ingame_Manager.Get.enemy.ChangeImage();
            }

            //ui갱신
            UI_Manager.Get.Skillbar.Refresh();//스킬 활성/비활성
            UI_Manager.Get.Menu.RefreshUpGradeItems();
            UI_Manager.Get.CharacterInfo.SetManaBar();

            //playerprefab save
            PlayerPrefs.SetInt("UpgradeLevel" + type_, Level_Upgrade[(int)type_]);
        }
        else
        {
            //돈부족 처리
            Debug.Log("돈부족함");
        }
    }

    public bool UpgradeConditionCheck(UpgradeType type_)
    {
        //최대 레벨을 넘기면 안된다.
        if (Level_Upgrade[(int)type_] >= Table_Manager.Get.li_UpgradeData[(int)type_].MaxLevel)
        {
            return false;
        }

        //해제 조건 없으면 무조건 True
        if (Table_Manager.Get.li_UpgradeData[(int)type_].UnlockType == UpgradeUnlock.None)
            return true;

        BigDecimal Unlock_value = 0;
        switch (Table_Manager.Get.li_UpgradeData[(int)type_].UnlockType)
        {
            case UpgradeUnlock.Stage:
                Unlock_value = Stage;
                break;
            case UpgradeUnlock.FireLv:
                Unlock_value = Level_Upgrade[(int)UpgradeType.Fire];
                break;
            case UpgradeUnlock.WarterLv:
                Unlock_value = Level_Upgrade[(int)UpgradeType.Water];
                break;
            case UpgradeUnlock.EarthLv:
                Unlock_value = Level_Upgrade[(int)UpgradeType.Earth];
                break;
        }

        //레벨이 부족하다
        if (Unlock_value < Table_Manager.Get.li_UpgradeData[(int)type_].UnlockValue)
        {
            //조건래벨이 아직 안된다 팝업
            //Debug.Log("조건 안됨");
            return false;
        }

        return true;
    }
    #endregion

    #region Artifact
    //업그래이드 처리
    public void LevelupArtifact(ArtifactType type_, int value_, int cost_)
    {
        //최대 레벨을 넘기면 안된다.
        if (Level_Artifact[(int)type_] >= Table_Manager.Get.li_ArtifactData[(int)type_].MaxLevel)
        {
            return;
        }
 
        //마법 가루 소모 
        if(UseMasicDust(cost_) == true)
        {
            Sound_Manager.Get.PlayEffect("Effect_click");

            Level_Artifact[(int)type_] += value_;
            SetNowLevelValue_Artifact(type_);

            //ui갱신
            UI_Manager.Get.Menu.ReFreshArtifactItems();

            //playerprefab save
            PlayerPrefs.SetInt("ArtifactLevel" + type_, Level_Artifact[(int)type_]);
        }
        else
        {
            //마법 가루 부족
        }
    }

    #endregion

    #region Achievement
    public void InitAchievement()
    {
        //dataload 선행되야됨(현재 미구현)

        for(int i = 0; i < (int)AchievementType.Max; ++i)
        {
            if(NowAchievementID[i] == 0)
            {
                Table_Manager.AchievementData data = Table_Manager.Get.GetFirstAchievement((AchievementType)i);
                NowAchievementID[i] = data.ID;
                if (data.Unlock == 1)
                    AchievementValue[i] = 1;
            }
        }

        //임시용
        for(int i = 0; i < Table_Manager.Get.li_AchievementData.Count; ++i)
        {
            if(!AchievementClear.ContainsKey(Table_Manager.Get.li_AchievementData[i].ID))
            {
                AchievementClear.Add(Table_Manager.Get.li_AchievementData[i].ID, false);
            }
        }
    }

    public void AddAchievmentValue(AchievementType Type, int value_)
    {
        AchievementValue[(int)Type] += value_;
        PlayerPrefs.SetInt("AchievementValue" + Type, AchievementValue[(int)Type]);
    }

    public void ClearAchievment(AchievementType type_)
    {
        Table_Manager.AchievementData data = Table_Manager.Get.li_AchievementData.Find(r => r.ID == NowAchievementID[(int)type_]);
        
        //다음 업적이 없고 클리어 안했으면 리턴
        if (data.NextID == 0 && AchievementClear[data.ID])
            return;

        if(data.Clear_Value <= AchievementValue[(int)type_] && !AchievementClear[data.ID])
        {
            Sound_Manager.Get.PlayEffect("Effect_click");

            if (data.NextID != 0)
                NowAchievementID[(int)type_] = data.NextID;

            //저장처리
            PlayerPrefs.SetInt("NAid" + type_, NowAchievementID[(int)type_]);

            //보상처리
            PaymentManager.Get.PayPrice(data.RewardID);

            AchievementClear[data.ID] = true;
            PlayerPrefs.SetInt("AchievmentClear" + data.ID, 1);

            //구글업적
            if (data.Gpgs_AchievementID != "empty")
                Server_Manager.Get.UnlockAchievement(data.Gpgs_AchievementID);

            //ui갱신
            UI_Manager.Get.Menu.ReFreshAchievementItems();
        }
        else
        {
            Debug.Log("넌 아직 준비가 덜됐다~~~~!");
        }
    }
    #endregion

    #region Bonus
    /// <summary>
    /// 특전에 사용할 캐시
    /// </summary>
    int UseBonus_Cash = 0;
    BonusType UseBonus_Type = BonusType.Max;
    
    public void UseBonus(BonusType type_)
    {
        //조건 <- 현재 사용할려는 특전이 쿨타임이 아니여야 한다
        if (LocalData_Manager.Get.GetPerkTime(type_) > 0)
        {
            Debug.Log("쿨타임 이라서 안됨");
            return;
        }

        Sound_Manager.Get.PlayEffect("Effect_click");

        if (Table_Manager.Get.li_BonusData[(int)type_].Cost == 0) //광고
        {
            Debug.Log("광고보면 쓸수있음");
            Ads_Manager.Get.ShowAd_UnityAds((ShowAdType)type_, -1);
        }
        else //캐시
        {
            UseBonus_Cash = Table_Manager.Get.li_BonusData[(int)type_].Cost;
            UseBonus_Type = type_;
            Popup_Manager.Get.Buy.Show(LM.GetText(251), "ruby", UseBonus_Cash.ToString(), UseBonus_byCash);
        }
    }
    #endregion


    public void DelYesNo_CashYes()
    {
        Debug.Log("상점 이동");

        //다른 팝업이 열려있었다면 닫아준다
        Popup_Manager.Get.SpecialDungeon.Hide();

        //만약 메뉴가 안열려있다면 열어준다
        if (UI_Manager.Get.Menu.IsOpen == false)
            UI_Manager.Get.Menu.MenuOpen();

        UI_Manager.Get.Menu.MenuDirectOpen(MenuOpenState.Shop);
    }

    void UseBonus_byCash()
    {
        if(UseCash_Value(UseBonus_Cash))
        {
            Debug.Log("캐시사용");
            AddAchievmentValue(AchievementType.UseBonus, 1);
            switch (UseBonus_Type)
            {
                case BonusType.GetMagicStone_Big:
                    AddMagicStone(FormulaDefine.PerkBigMagicStone());
                    break;
                case BonusType.DamagePlus_Big:
                    LocalData_Manager.Get.SetPerkTime(UseBonus_Type, ConstValue.PerkTime_Cash);
                    break;
                case BonusType.ElementalMaster:
                    LocalData_Manager.Get.SetPerkTime(UseBonus_Type, ConstValue.PerkTime_Cash);
                    break;
                case BonusType.ManaFullCharge:
                    LocalData_Manager.Get.AddMana(NowValue[(int)UpgradeType.ManaMax]);
                    break;
                case BonusType.PowerPress:
                    LocalData_Manager.Get.SetPerkTime(UseBonus_Type, ConstValue.PerkTime_Cash);
                    break;
                case BonusType.SuperFocus:
                    LocalData_Manager.Get.SetPerkTime(UseBonus_Type, ConstValue.PerkTime_Cash);
                    break;
            }
            UI_Manager.Get.Menu.ReFreshBonusItems();
        }
    }

    /// <summary>
    /// 서버에서 불러온 데이터 세팅
    /// </summary>
    public void SetServerUserData(CloudSaveData data_)
    {
        MagicStone = BigDecimal.Parse(data_.MagicStone);
        MagicDust = data_.MagicDust;
        Cash_Value = data_.Ruby;
        Stage = data_.Stage;
        Stage_FullMoon = data_.Stage_FullMoon;

        for (int i = 0; i < data_.UpgradeLevel.Length; ++i)
        {
            Level_Upgrade[i] = data_.UpgradeLevel[i];
        }

        for (int i = 0; i < data_.ArtifactLevel.Length; ++i)
        {
            Level_Artifact[i] = data_.ArtifactLevel[i];
        }

        for (int i = 0; i < data_.NowAchievementid.Length; ++i)
        {
            NowAchievementID[i] = data_.NowAchievementid[i];
        }

        for(int i = 0; i < data_.AchievmenetValue.Length; ++i)
        {
            AchievementValue[i] = data_.AchievmenetValue[i];
        }

        for(int i = 0; i < data_.AchievementId.Length; ++i)
        {
            if (AchievementClear.ContainsKey(data_.AchievementId[i]))
                AchievementClear[data_.AchievementId[i]] = data_.AchievementClear[i];
            else
                AchievementClear.Add(data_.AchievementId[i], data_.AchievementClear[i]);
        }

        for(int i = 0; i < (int)UpgradeType.Max; ++i)
            SetNowLevelValue((UpgradeType)i);

        for (int i = 0; i < (int)ArtifactType.Max; ++i)
            SetNowLevelValue_Artifact((ArtifactType)i);

        //playprefab save
        PlayerPrefs.SetString("MagicStone", MagicStone.ToString());
        PlayerPrefs.SetInt("MagicDust", MagicDust);
        PlayerPrefs.SetInt("Ruby", Cash_Value);
        PlayerPrefs.SetInt("Stage", Stage);
        PlayerPrefs.SetInt("Stage_FullMoon", Stage_FullMoon);

        for (int i = 0; i < (int)UpgradeType.Max; ++i)
        {
            PlayerPrefs.SetInt("UpgradeLevel" + (UpgradeType)i, Level_Upgrade[i]);
        }

        for (int i = 0; i < (int)ArtifactType.Max; ++i)
        {
            PlayerPrefs.SetInt("ArtifactLevel" + (ArtifactType)i, Level_Artifact[i]);
        }


        for (int i = 0; i < (int)AchievementType.Max; ++i)
        {
            PlayerPrefs.SetInt("NAid" + (AchievementType)i, NowAchievementID[i]);

            PlayerPrefs.SetInt("AchievementValue" + (AchievementType)i, AchievementValue[i]);
        }

        foreach(KeyValuePair<int, bool> o in AchievementClear)
        {
            if (o.Value)
                PlayerPrefs.SetInt("AchievmentClear" + o.Key, 1);
            else
                PlayerPrefs.SetInt("AchievmentClear" + o.Key, 0);
        }

        //ui 갱신
        UI_Manager.Get.UpdateMagicStoneText();
        UI_Manager.Get.Skillbar.Refresh();
        UI_Manager.Get.Menu.ReFreshAchievementItems();
        UI_Manager.Get.Menu.RefreshUpGradeItems();
        UI_Manager.Get.Menu.ReFreshArtifactItems();

        UI_Manager.Get.Menu.RefreshRuby();
    }

    private void OnDestroy()
    {
        
    }

    private void OnApplicationPause(bool pause)
    {
        PlayerPrefs.SetString("MagicStone", MagicStone.ToString());
    }


    //각 유물 배수 구하기

    public float GetArtifactValue(ArtifactType _type , float _defalutValue)
    {
        if (Level_Artifact[(int)_type] > 0)
            return NowValue_Artifact[(int)_type];

        return _defalutValue;
    }

    public void AddReward(RewardType _type, BigDecimal _value)
    {
        int nValue;
        switch (_type)
        {
            case RewardType.Ruby:
                nValue = int.Parse(_value.ToString());
                AddCash_Value(nValue);
                break;
            case RewardType.MagicStone:
                AddMagicStone(_value);
                break;
            case RewardType.MagicDust:
                nValue = int.Parse(_value.ToString());
                AddMagicDust(nValue);
                break;
        }
    }

}
