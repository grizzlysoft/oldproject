﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Localizing_Manager
public static class LM
{
    static List<LocalizeText> li_Text = new List<LocalizeText>();

    public static void AddText(LocalizeText _data)
    {
        li_Text.Add(_data);
    }

    public static void RemoveText(LocalizeText _data)
    {
        li_Text.Remove(_data);
    }

    public static void SetAllText()
    {
        for(int i=0; i<li_Text.Count;i++)
        {
            li_Text[i].ChangeLanguage();
        }
    }

    public static string GetText(int _index)
    {
        return Table_Manager.Get.li_TextData[_index].Text[(int)LocalData_Manager.Get.SelectLanguage];
    }

    public static string GetText(int _index , params object[] _list)
    {
        //조합 문자열
        return string.Format(Table_Manager.Get.li_TextData[_index].Text[(int)LocalData_Manager.Get.SelectLanguage], _list);
    }
}
