﻿//HierarchyHighlighter.cs - Put this script under Editor folder. 
//If you do not have any Editor folder in Unity, create one.
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[InitializeOnLoad]
public class HierarchyHighlighter : Editor
{
    static HierarchyHighlighter()
    {
        //EditorApplication.hierarchyWindowItemOnGUI += HierarchyWindowItemColorBox;
    }

    private static void HierarchyWindowItemColorBox(int selectionID, Rect selectionRect)
    {
        Object o = EditorUtility.InstanceIDToObject(selectionID);
        if (o == null)
            return;
        GameObject obj = o as GameObject;

        if (Event.current.type == EventType.Repaint)
        {
            //글자색 바꾸기
            //배경색 바꾸기
            GUI.backgroundColor = GetColor(obj);
            GUI.Box(selectionRect, "");
            GUI.backgroundColor = Color.white;
            EditorApplication.RepaintHierarchyWindow();
        }
    }

    static Color GetColor(GameObject obj)
    {
        float baseAlpha = 1;
        Color transparency = new Color(1, 1, 1, 0);

        if (obj == null)
            return transparency;

        Transform tr = obj.transform;
        UIRoot[] temp = tr.GetComponentsInChildren<UIRoot>(true);

        if (true == obj.GetComponent<UIRoot>() is UIRoot)
        {
            if (obj.activeInHierarchy)
                return new Color(1, 0, 0, baseAlpha);
            else
                return transparency;
        }

        if (obj.tag == "Bone")
        {
            if (obj.activeInHierarchy)
                return new Color(0, 1, 0, baseAlpha);
            else
                return transparency;
        }

        if (obj.tag == "Equip")
        {
            if (obj.activeInHierarchy)
                return new Color(0, 0, 1, baseAlpha);
            else
                return transparency;
        }

        if (obj.tag == "Weapon")
        {
            if (obj.activeInHierarchy)
                return new Color(1, 0, 0, baseAlpha);
            else
                return transparency;
        }

        if (obj.GetComponent<UIPanel>() != null)
        {
            if (obj.activeInHierarchy)
                return new Color(1, 0, 1, baseAlpha);
            else
                return transparency;
        }


        if (obj.GetComponent<SpriteRenderer>() != null && obj.GetComponent<SpriteRenderer>().sprite == null)
        {
            if (obj.activeInHierarchy)
                return new Color(1, 0, 0, baseAlpha);
            else
                return transparency;
        }

        if (obj.GetComponent<UITexture>() != null && obj.GetComponent<UITexture>().mainTexture == null)
        {
            if (obj.activeInHierarchy)
                return new Color(1, 0, 0, baseAlpha);
            else
                return transparency;
        }

        return transparency;
    }
}