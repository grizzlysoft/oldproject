﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Main_Manager))]
public class MainManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Main_Manager mm = (Main_Manager)target;

        if(GUILayout.Button("MakeItem"))
        {
            mm.CreateItem();
        }

        if (GUILayout.Button("Teleport"))
        {
            mm.Teleport();
        }

        if (GUILayout.Button("Death"))
        {
            mm.Death();
        }
    }
}
