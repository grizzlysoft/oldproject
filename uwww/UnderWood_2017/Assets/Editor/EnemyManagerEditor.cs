﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Enemy_Manager))]
public class EnemyManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        
        Enemy_Manager em = (Enemy_Manager)target;

        if(GUILayout.Button("MakeEnemy"))
        {
            em.CreatOneEnemy(em.SpawnEnemyIndex);
        }
    }
}
