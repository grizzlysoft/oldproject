﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopStatUI : MonoBehaviour
{
    public UILabel lb_Level;
    public UISprite sp_EXP;
    public UISprite sp_HP;
    public UISprite sp_STA;
    public UILabel lb_Stat;

    public GameObject go_Save;

    public GameObject btn_Shop;

    //퀘스트
    public GameObject go_QuestOn;
    public UILabel lb_Quest;

    public UISprite sp_QuestReward;
    public UILabel lb_QuestReward;

    public Animation anim;
    public Animation anim_FlyingGoods;
    public List<UISprite> li_FlyingGoods;

    public UILabel lb_CurrentPos;


    public void SetCurrentPos(string _Pos)
    {
        lb_CurrentPos.text = _Pos;
    }
    public void SetLevel(int _Level)
    {
        lb_Level.text = string.Format("Lv.{0}", _Level);
    }
    public void SetEXP(int _EXP, int _MAX)
    {
        sp_EXP.fillAmount = (float)_EXP / _MAX;
    }
    public void SetHP(int _HP , int _MAX)
    {
        sp_HP.fillAmount = (float)_HP / _MAX;
    }
    public void SetSTA(int _STA, int _MAX)
    {
        sp_STA.fillAmount = (float)_STA / _MAX;
    }
    public void SetStat(int _ATK ,int _DEF, int _HP, Weapon _Weapon)
    {
        int crit = 0;
        string stAttack = "";
        crit = ConstValue.Crit_Incidence;
        stAttack = _ATK.ToString();
        if (_Weapon != null)
        {
            crit += _Weapon.data.Crit;
            stAttack = string.Format("{0}~{1}",
                _Weapon.data.ATK_Min + _ATK,
                _Weapon.data.ATK_Max + _ATK);
        }

        lb_Stat.text = string.Format
            ("ATK {0}  [FF9900]|[-]  DEF {1}  [FF9900]|[-]  HP {2}  [FF9900]|[-]  Crit {3}%" ,
            stAttack,
            _DEF,
            _HP,
            crit);
    }

    public void OnClick_Option()
    {
        //IngameUI가 열려있으면 눌리면 안된다
        if (Popup_Manager.Get.Ingame.gameObject.activeSelf == true)
            return;

        Sound_Manager.Get.PlayEffect("Effect_click");

        Popup_Manager.Get.Option.Show();
    }

    public void OnClick_Shop()
    {
        if (Popup_Manager.Get.Ingame.gameObject.activeSelf == true)
            return;

        if(Map_Manager.Get.GetCurrentMapData().type == MapType.Camp ||
            Map_Manager.Get.GetCurrentMapData().type == MapType.Lumber ||
            Map_Manager.Get.GetCurrentMapData().type == MapType.Mine )
        {

            Sound_Manager.Get.PlayEffect("Effect_click");
            Popup_Manager.Get.Shop.Show();
        }

    }

    public void OnClick_AD()
    {
        if (Popup_Manager.Get.Ingame.gameObject.activeSelf == true)
            return;

        if (Map_Manager.Get.GetCurrentMapData().type == MapType.Camp ||
            Map_Manager.Get.GetCurrentMapData().type == MapType.Lumber ||
            Map_Manager.Get.GetCurrentMapData().type == MapType.Mine)
        {

            Sound_Manager.Get.PlayEffect("Effect_click");
            Popup_Manager.Get.AD.Show();
        }

    }

    public void OnClick_Quest()
    {
        if (Popup_Manager.Get.Ingame.gameObject.activeSelf == true)
            return;

        if (Character.Get.QuestID == -1)
        {
            //동영상 보고 퀘스트 받을래염?
            Ads_Manager.Get.ShowRewardedAd(Character.Get.GetLoopQuest);
        }
        else
        {
            Character.Get.QuestClear();
        }
    }

    Table_Manager.QuestData questData;
    public void RefreshQuest(bool _isAnim = false)
    {
        if(Character.Get.QuestID == -1)
        {
            go_QuestOn.SetActive(false);
            lb_Quest.text = LM.DATA[52];
            sp_QuestReward.gameObject.SetActive(false);
        }
        else
        {
            questData = Table_Manager.Get.li_QuestData.Find(r => r.id == Character.Get.QuestID);

            if (Character.Get.CheckQuestClear() == true)
            {
                go_QuestOn.SetActive(true);
                //{0} {1}마리 처치({2}/{3})
                lb_Quest.text = string.Format(LM.DATA[51],
                questData.name,
                questData.count,
                Character.Get.QuestCount,
                questData.count);
            }
            else
            {
                go_QuestOn.SetActive(false);
                lb_Quest.text = string.Format(LM.DATA[50],
                    questData.name,
                    questData.count,
                    Character.Get.QuestCount,
                    questData.count);
            }

            sp_QuestReward.gameObject.SetActive(true);
            sp_QuestReward.spriteName = "Dia";
            if (questData.priceType == PriceType.Gold)
                sp_QuestReward.spriteName = "Gold";

            lb_QuestReward.text = questData.price.ToString();
        }
        

        if (_isAnim == true)
        {
            Sound_Manager.Get.PlayEffect("Effect_bookflip");
            anim.Play("QuestThrowing");
        }
    }
    
    public void ShowFlyingGoods(PriceType _type)
    {
        string spriteName = "Gold";
        if (_type == PriceType.Dia)
            spriteName = "Dia";

        for (int i = 0; i < li_FlyingGoods.Count; i++)
        {
            li_FlyingGoods[i].spriteName = spriteName;
        }

        anim_FlyingGoods.Play("FlyingGold");
    }

    public bool IsSaving()
    {
        return go_Save.activeSelf == true;
    }

    public void ShowSave()
    {
        StartCoroutine(SaveClose());
    }

    IEnumerator SaveClose()
    {
        go_Save.SetActive(true);
        yield return new WaitForSeconds(2.5f);
        go_Save.SetActive(false);
    }
}
