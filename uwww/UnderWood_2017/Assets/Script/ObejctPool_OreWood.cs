﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObejctPool_OreWood : MonoBehaviour
{
    public GameObject go_Content;
    List<OreWoodContent> li_Content = new List<OreWoodContent>();

    public void Init()
    {
        GameObject temp;
        OreWoodContent tempScp;
        for (int i = 0; i < ConstValue.Max_DropItem; i++)
        {
            temp = Instantiate(go_Content, this.transform);
            temp.transform.localPosition = Vector3.zero;
            tempScp = temp.GetComponent<OreWoodContent>();
            tempScp.Hide();

            li_Content.Add(tempScp);
        }
    }

    public void CreateOreWood(Table_Manager.MapData _mapData)
    {
        AllHide();

        int bonus = 0;
        if (_mapData.type == MapType.Mine)
        {
            bonus += Character.Get.CampData().oreLevel;
        }
        else if(_mapData.type == MapType.Lumber)
        {
            bonus += Character.Get.CampData().woodLevel;
        }

        int count = Random.Range(_mapData.OreWoodMin + bonus, _mapData.OreWoodMax + bonus);
        if (count == 0)
            return;

        //랜덤한거 생성
        int index;

        for (int i = 0; i < count; i++)
        {
            index = Random.Range(0, _mapData.li_OreWoodID.Count);
            li_Content[i].Show(_mapData.li_OreWoodID[index], Random.Range(-500, 700));
        }
    }

    public void AllHide()
    {
        for (int i = 0; i < li_Content.Count; i++)
        {
            li_Content[i].Hide();
        }
    }


}
