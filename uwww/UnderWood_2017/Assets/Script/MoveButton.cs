﻿using UnityEngine;
using System.Collections;


public class MoveButton : MonoBehaviour
{
    
    public BoxCollider[] _Collider;
    private Camera _uiCamera;
    private ButtonDirection _PressButton;
    private RaycastHit hit;

    // Use this for initialization
    void Start()
    {
        _PressButton = ButtonDirection.NONE;
        this._uiCamera = NGUITools.FindCameraForLayer(this.gameObject.layer);
    }

    // Update is called once per frame
    void Update()
    {
        //이까지 왔으면 눌린 버튼 없음.
        _PressButton = ButtonDirection.NONE;

        //현제 터치한 손가락을 찾는다 
        for (int i = 0; i < Input.touchCount; i++)
        {
            Ray ray = _uiCamera.ScreenPointToRay(Input.GetTouch(i).position);
            if (Physics.Raycast(ray, out hit))
            {
                for (int j = 0; j < _Collider.Length; j++)
                {
                    if (hit.collider == _Collider[j])
                    {
                        _PressButton = (ButtonDirection)j;
                        break;
                    }
                }
            }
            //break; 
        }


    }

    //외부 소통용
    public ButtonDirection PressButton
    {
        get
        {
            return this._PressButton;
        }
    }

}
