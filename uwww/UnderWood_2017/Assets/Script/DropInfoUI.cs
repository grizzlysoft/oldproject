﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropInfoUI : MonoBehaviour
{
    public UILabel lb_Title;

    public GameObject go_Content;
    public UIScrollView sc_View;
    public UIGrid gr_Grid;
    List<ItemContent> li_Content = new List<ItemContent>();
    Dictionary<int, List<itemSlotData>> dic_DropData = new Dictionary<int, List<itemSlotData>>();

    private int selectMap;

    public void Init()
    {
        //맵 데이터에서 텔레포트 지점이 있는 맵을 찾아서 MapID를 저장
        for (int i = 0; i < Table_Manager.Get.li_MapData.Count; i++)
        {
            if (Table_Manager.Get.li_MapData[i].isfastTrevel == true)
            {
                //드롭 정보가 저장될 리스트
                List<itemSlotData> li_temp = new List<itemSlotData>();

                //텔레포트 지점+4개의 맵의 몬스터 드롭 정보를 찾는다
                for (int j = 0; j < 5; j++)
                {
                    GetMapMonsterData(li_temp, Table_Manager.Get.li_MapData[i + j].li_Monster_Left);
                    GetMapMonsterData(li_temp, Table_Manager.Get.li_MapData[i + j].li_Monster_Right);
                }

                //리스트 정렬 
                li_temp.Sort(delegate (itemSlotData A, itemSlotData B)
                {
                    if (A.tableData.grade > B.tableData.grade) return 1;
                    else if (A.tableData.grade < B.tableData.grade) return -1;
                    else
                    {
                        if (A.tableData.id > B.tableData.id) return 1;
                        else if (A.tableData.id < B.tableData.id) return -1;
                        else return 0;
                    }
                });

                //딕셔너리에 추가
                dic_DropData.Add(Table_Manager.Get.li_MapData[i].stageID, li_temp);
            }
        }


        //가장 많은 드롭량을 찾는다.
        int MaxDrop = 0;
        foreach (var item in dic_DropData)
        {
            if (item.Value.Count > MaxDrop)
            {
                MaxDrop = item.Value.Count;
            }
        }

        GameObject temp;
        ItemContent tempScp;
        for (int i = 0; i < MaxDrop; i++)
        {
            temp = Instantiate(go_Content, gr_Grid.transform) as GameObject;
            tempScp = temp.GetComponent<ItemContent>();
            tempScp.Init(null);
            li_Content.Add(tempScp);
        }

    }

    void GetMapMonsterData(List<itemSlotData> _li_DropData, List<int> _MonsterList)
    {
        List<Table_Manager.DropItem> DropItemList;
        itemSlotData Find;

        // 등장 몬스터 판단
        for (int i = 0; i < _MonsterList.Count; i++)
        {
            //현재 인덱스 몬스터가 드롭하는 아이템 리스트를 가져온다->MonsterList[i]는 몬스터 인덱스
            DropItemList = Table_Manager.Get.li_EnemyData[_MonsterList[i]].li_DropItem;
            //해당 몹 드롭 아이템을 찾는다
            for (int j = 0; j < DropItemList.Count; j++)
            {
                //기존 리스트 안에 있는 아이템인가?
                Find = _li_DropData.Find(r => r.tableData.id == DropItemList[j].id);

                if (Find == null)
                {
                    //없던거면 만들기
                    itemSlotData temp = Character.Get.MakeItem(DropItemList[j].id);
                    temp.count = 1;
                    _li_DropData.Add(temp);
                }
                else
                {
                    //있던아이템 = count 증가
                    Find.count++;
                }
            }
        }
    }

    public void SetData(Table_Manager.MapData _Find)
    {
        selectMap = _Find.stageID;
        lb_Title.text = _Find.name.Split('-')[0];
    }

    public void Refresh()
    {
        //        
        for (int i = 0; i < li_Content.Count; i++)
        {
            li_Content[i].Hide();
        }

        for (int i = 0; i < dic_DropData[selectMap].Count; i++)
        {
            li_Content[i].Show_DropItemInfo(dic_DropData[selectMap][i]);
        }

        gr_Grid.Reposition();
        sc_View.ResetPosition();
    }


    public void OnClick_Start()
    {
        Sound_Manager.Get.PlayEffect("Effect_teleport");
        Map_Manager.Get.ChangeMap_Pos(selectMap);
        Popup_Manager.Get.Ingame.Hide();
    }


}
