﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlchemyListViewer : MonoBehaviour
{
    public GameObject go_Content;
    public List<AlchemyContent> li_Content = new List<AlchemyContent>();

    public UIGrid gr_Grid;

    public GameObject go_Content_Result;
    public List<ItemContent> li_Content_Result = new List<ItemContent>();
    public UIScrollView sc_View_Result;
    public UIGrid gr_Grid_Result;


    List<Table_Manager.AlchemyData> li_Table;

    public void Init(AlchemyUI _parent)
    {
        li_Table = Table_Manager.Get.li_Alchemy;

        int MAX = 0;
        for (int i = 0; i < li_Table.Count; i++)
        {
            if (MAX < li_Table[i].li_GetItem_ID.Count)
                MAX = li_Table[i].li_GetItem_ID.Count;
        }

        GameObject temp;
        AlchemyContent tempScp;
        ItemContent tempScp2;

        for (int i = 0; i < MAX; i++)
        {
            temp = Instantiate(go_Content, gr_Grid.transform) as GameObject;
            tempScp = temp.GetComponent<AlchemyContent>();
            tempScp.Hide();
            li_Content.Add(tempScp);

            temp = Instantiate(go_Content_Result, gr_Grid_Result.transform) as GameObject;
            tempScp2 = temp.GetComponent<ItemContent>();
            tempScp2.Init(null);
            li_Content_Result.Add(tempScp2);
        }
    }

    //아래 정보로 초기화
    public void Refresh(int _current)
    {
        for (int i = 0; i < li_Content.Count; i++)
        {
            li_Content[i].Hide();
        }

        for (int i = 0; i < li_Content_Result.Count; i++)
        {
            li_Content_Result[i].Hide();
        }

        for (int i = 0; i < li_Table[_current].li_GetItem_ID.Count; i++)
        {
            li_Content[i].Show(li_Table[_current].li_GetItem_ID[i]);
        }

        gr_Grid_Result.Reposition();
        sc_View_Result.ResetPosition();
    }

    public class itemInfo
    {
        public int id;
        public int count;

        public itemInfo(int _id, int _count)
        {
            id = _id;
            count = _count;
        }
    }
    List<itemInfo> li_result = new List<itemInfo>();

    public void SetResult(List<int> _list)
    {
        itemInfo temp;
        itemInfo Find;
        li_result.Clear();
        for (int i=0; i<_list.Count; i++)
        {
            Find = li_result.Find(r => r.id == _list[i]);
            if (Find == null)
            {
                temp = new itemInfo(_list[i],1);
                li_result.Add(temp);
            }
            else
            {
                Find.count++;
            }
        }

        for (int i = 0; i < li_result.Count; i++)
        {
            li_Content_Result[i].Show(li_result[i].id, li_result[i].count);
        }
    }
}
