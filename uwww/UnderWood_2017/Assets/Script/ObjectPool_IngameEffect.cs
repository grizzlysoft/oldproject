﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool_IngameEffect : MonoBehaviour
{
    public GameObject go_Content;
    List<IngameEffect> li_Effect = new List<IngameEffect>();

    public void Init()
    {
        GameObject temp;
        IngameEffect tempScp;
        for (int i = 0; i < ConstValue.Max_Effect; i++)
        {
            temp = Instantiate(go_Content, this.transform);
            temp.transform.localPosition = Vector3.zero;
            tempScp = temp.GetComponent<IngameEffect>();
            tempScp.Init();
            li_Effect.Add(tempScp);
        }
    }

    public void ShowEffect(Transform _tr , IngameEffect.Type _type )
    {
        for (int i = 0; i < li_Effect.Count; i++)
        {
            if (li_Effect[i].gameObject.activeSelf == false)
            {
                li_Effect[i].Show( _tr.position, _type);
                break;
            }
        }
    }

    public void AllHide()
    {
        for (int i = 0; i < li_Effect.Count; i++)
        {
            li_Effect[i].Hide();
        }
    }

}
