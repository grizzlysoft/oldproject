﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Teleport : Popup
{
    public GameObject go_Content;
    public List<TeleportContent> li_Content = new List<TeleportContent>();

    public UIScrollView sc_View;
    public UIGrid gr_Grid;
    List<Table_Manager.MapData> li_Table;

    public delegate void Del_Function();
    Del_Function del;

    void Init()
    {
        li_Table = Table_Manager.Get.li_MapData.FindAll(r => r.stageID > 0);

        GameObject temp;
        TeleportContent tempScp;
        for (int i = 0; i < li_Table.Count; i++)
        {
            temp = Instantiate(go_Content, gr_Grid.transform) as GameObject;
            tempScp = temp.GetComponent<TeleportContent>();
            tempScp.Init(this, li_Table[i]);
            li_Content.Add(tempScp);
        }
    }

    public void Show(Del_Function _del)
    {
        if (li_Table == null)
        {
            Init();
        }

        for(int i=0; i<li_Content.Count; i++)
        {
            li_Content[i].Show(Character.Get.stageLevel >= li_Table[i].stageID);
        }
        
        del = _del;
        gr_Grid.Reposition();
        sc_View.ResetPosition();

        base.Show();
    }

    public void Teleport()
    {
        del();
        Hide();
    }
    
}
