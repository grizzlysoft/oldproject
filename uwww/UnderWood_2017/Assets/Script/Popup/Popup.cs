﻿using UnityEngine;
using System.Collections;

public class Popup : MonoBehaviour
{
    public bool isBlackBG = true;
    public TweenScale tw_Scale = null;
    Vector3 tw_startPos = Vector3.zero;
    Vector3 tw_endPos = Vector3.one;
    public float tw_Time = 1.0f;
   
    virtual public void Show()
    {
        if (this.gameObject.activeSelf == false)
        {
            if (tw_Scale != null)
            {
                tw_Scale.from = tw_startPos;
                tw_Scale.to = tw_endPos;
                tw_Scale.duration = tw_Time;
                tw_Scale.ResetToBeginning();
                tw_Scale.enabled = true;
            }

            Popup_Manager.Get.PushList(this);
            this.gameObject.SetActive(true);
        }
    }

    virtual public void Hide()
    {
        if (this.gameObject.activeSelf == true)
        {
            Popup_Manager.Get.PopList(gameObject);
            this.gameObject.SetActive(false);
            
        }
    }

    public void OnClick_Hide()
    {
        Sound_Manager.Get.PlayEffect("Effect_click");
        Hide();
    }

}
