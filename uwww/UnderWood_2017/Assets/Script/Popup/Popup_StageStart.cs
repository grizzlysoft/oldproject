﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_StageStart : Popup
{
    public UILabel lb_Name;
    public UILabel lb_Stage;
    public UILabel lb_Desc;

    public TweenAlpha tw_Alpha;

    public override void Show()
    {
        Popup_Manager.Get.AllHide();

        lb_Name.text = Map_Manager.Get.GetCurrentMapData().name;

        switch (Map_Manager.Get.GetCurrentMapData().type)
        {
            case MapType.Normal:
                lb_Stage.text = LM.DATA[69];
                lb_Desc.text = LM.DATA[64];
                break;
            case MapType.TimeAttack:
                lb_Stage.text = LM.DATA[70];
                lb_Desc.text = LM.DATA[65];
                break;
            case MapType.Defence:
                lb_Stage.text = LM.DATA[71];
                lb_Desc.text = LM.DATA[66];
                break;
            case MapType.Survival:
                lb_Stage.text = LM.DATA[72];
                lb_Desc.text = LM.DATA[67];
                break;
            case MapType.Boss:
                lb_Stage.text = LM.DATA[73];
                lb_Desc.text = LM.DATA[68];
                break;
        }

        this.gameObject.SetActive(true);

        tw_Alpha.ResetToBeginning();
        tw_Alpha.enabled = true;
    }

    public override void Hide()
    {
        this.gameObject.SetActive(false);
    }
}
