﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Warning : Popup
{
    public UILabel lb_desc;
    public delegate void DelClickYes();
    DelClickYes del;

    public void Show(string _desc)
    {
        Sound_Manager.Get.PlayEffect("Effect_warning");

        lb_desc.text = _desc;
        base.Show();
    }

    public void Show(string _desc , DelClickYes _del)
    {
        Sound_Manager.Get.PlayEffect("Effect_warning");

        del = _del;
        lb_desc.text = _desc;
        base.Show();
    }

    public override void Hide()
    {
        if (del != null)
            del();

        base.Hide();
    }
}
