﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Login : Popup
{
    public UILabel lb_Login;
    public UILabel lb_Start;
    bool isHide;

    public override void Show()
    {
        lb_Login.gameObject.SetActive(true);
        lb_Start.gameObject.SetActive(false);
        isHide = false;
        base.Show();
    }
    
    public void ChangeLabel(string _Data)
    {
        lb_Login.text = _Data;
    }

    public void CanHide()
    {
        lb_Login.gameObject.SetActive(false);
        lb_Start.gameObject.SetActive(true);
        isHide = true;
    }
    
    public void HideNow()
    {
        if (isHide == false)
            return;

        base.Hide();

        //튜토리얼 조작법
        int firstStart = PlayerPrefs.GetInt("StartFirst", 0);
        if (firstStart == 0)
        {
            Popup_Manager.Get.Help.Show(0);
            PlayerPrefs.SetInt("StartFirst", 1);
        }
    }

    public override void Hide()
    {
        //base.Hide();
    }

}
