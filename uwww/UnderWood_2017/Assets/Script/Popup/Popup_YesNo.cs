﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_YesNo : Popup
{
    public UILabel lb_Title;
    public UILabel lb_Desc;
    public delegate void DelClick();
    DelClick del;
    DelClick del_Hide;

    public void Show(string _stTitle, string _stDesc, DelClick _del)
    {
        lb_Title.text = _stTitle;
        lb_Desc.text = _stDesc;

        del = _del;
        base.Show();
    }

    public void Show(string _stTitle, string _stDesc, DelClick _del , DelClick _del_Hide)
    {
        lb_Title.text = _stTitle;
        lb_Desc.text = _stDesc;

        del = _del;
        del_Hide = _del_Hide;
        base.Show();
    }

    public void OnClick_Yes()
    {
        Hide();

        if (del != null)
            del();
    }

    public override void Hide()
    {
        base.Hide();

        if (del_Hide != null)
            del_Hide();

    }

}
