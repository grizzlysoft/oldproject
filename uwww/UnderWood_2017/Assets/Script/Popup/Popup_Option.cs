﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Popup_Option : Popup
{
    public UISlider sl_BGM;
    public UISlider sl_Effect;

    public override void Show()
    {
        sl_BGM.value = Sound_Manager.Get.BGM_Volume;
        sl_Effect.value = Sound_Manager.Get.Effect_Volume;

        base.Show();
    }

 
    public void ChangeBGM()
    {
        Sound_Manager.Get.SetVolume_BGM(sl_BGM.value);
    }

    public void ChangeEffect()
    {
        Sound_Manager.Get.SetVolume_Effect(sl_Effect.value);
    }

    public void OnClick_Save()
    {
        Sound_Manager.Get.PlayEffect("Effect_click");

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            Popup_Manager.Get.Warning.Show(LM.DATA[53]);
            return;
        }

        Character.Get.SavePlayerLocalData();
#if !UNITY_EDITOR
        StartCoroutine(GPGS_SAVE());
#endif
    }

    IEnumerator GPGS_SAVE()
    {
        if (GPGS_Manager.Get.bLogin == false)
        {
            GPGS_Manager.Get.InitializeGPGS();
            GPGS_Manager.Get.LoginGPGS();
        }

        float time = 0.0f;

        while (true)
        {
            time += Time.deltaTime;

            if (GPGS_Manager.Get.bLogin == true)
            {
                GPGS_Manager.Get.SaveToCloud();
                break;
            }

            if (time > 15.0f)
            {
                Popup_Manager.Get.Warning.Show("로그인 시간 초과!\n다시 시도해주세요");
                break;
            }

            yield return null;
        }
    }


    public void OnClick_Load()
    {
        Sound_Manager.Get.PlayEffect("Effect_click");

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            Popup_Manager.Get.Warning.Show(LM.DATA[53]);
            return;
        }

        //마지막으로 로드 한 시간과 비교한다
        string temp = PlayerPrefs.GetString("LastGPGS_LOAD", "0");
        DateTime BeforeTime = new DateTime(long.Parse(temp));

        TimeSpan remainTime = BeforeTime - DateTime.Now;
        Debug.Log(remainTime.TotalSeconds);
        if (remainTime.TotalSeconds > 0)
        {
            Popup_Manager.Get.Warning.Show(string.Format("{0}시간 {1}분 후\n사용 가능합니다.", remainTime.Hours , remainTime.Minutes));
            return;
        }

#if !UNITY_EDITOR
        StartCoroutine(GPGS_LOAD());
#endif
    }

    IEnumerator GPGS_LOAD()
    {
        if (GPGS_Manager.Get.bLogin == false)
        {
            GPGS_Manager.Get.InitializeGPGS();
            GPGS_Manager.Get.LoginGPGS();
        }

        float time = 0.0f;

        while (true)
        {
            time += Time.deltaTime;

            if (GPGS_Manager.Get.bLogin == true)
            {
                PlayerPrefs.SetString("LastGPGS_LOAD", DateTime.Now.AddHours(3).Ticks.ToString());
                Popup_Manager.Get.YesNo.Show(LM.DATA[54], LM.DATA[55], GPGS_Manager.Get.LoadFromCloud);
                break;
            }

            if(time > 15.0f)
            {
                Popup_Manager.Get.Warning.Show("로그인 시간 초과!\n다시 시도해주세요");
                break;
            }

            yield return null;
        }
    }

    public void OnClick_Cafe()
    {
        Sound_Manager.Get.PlayEffect("Effect_click");
        Application.OpenURL("http://cafe.naver.com/grizzlysoft");

        //PlayerPrefs.DeleteAll();

        //GPGS_Manager.Get.UnlockAchievement(GPGSIds.achievement_hello_world);
        //Popup_Manager.Get.Warning.Show(string.Format("현재 로그인 {0}", GPGS_Manager.Get.bLogin));
    }
    public void OnClick_Review()
    {
        Sound_Manager.Get.PlayEffect("Effect_click");
        Application.OpenURL("market://details?id=com.GrizzlySoft.UnderWood");

        //Character.Get.QuestID = 1003;
        //Character.Get.topStatUI.RefreshQuest();
        //Character.Get.QuestCount = 48;
        //Character.Get.QuestClear();

    }

    public void OnClick_Help()
    {
        Sound_Manager.Get.PlayEffect("Effect_click");

        base.Hide();
        Popup_Manager.Get.Help.Show(0);
    }


}
