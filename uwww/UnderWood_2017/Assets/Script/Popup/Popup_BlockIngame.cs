﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_BlockIngame : Popup
{
    override public void Show()
    {
        isBlackBG = false;
        base.Show();
    }

    override public void Hide()
    {
        //못닫는 팝업이라서 사용 불가능
    }

    public void NowHide()
    {
        base.Hide();
    }
}
