﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Selling : Popup
{
    public UILabel lb_ItemName;
    public UILabel lb_Price;
    public UILabel lb_Count;
    public UISprite sp_Icon;

    itemSlotData data;
    int SelectCount;


    public delegate void DelClickYes(int _sellCount);
    DelClickYes del;

    public void Show(itemSlotData _data, DelClickYes _del , bool _isBlackBG)
    {
        isBlackBG = _isBlackBG;
        data = _data;
        
        SelectCount = _data.count;
        lb_ItemName.text = _data.tableData.name;
        sp_Icon.spriteName = _data.tableData.iconName;
        del = _del;

        Refresh();
        base.Show();
    }

    void Refresh()
    {
        if (data.tableData.itemType == ItemType.Equipment ||
            data.tableData.itemType == ItemType.Weapon)
        {
            lb_Count.text = "1";
            lb_Price.text = data.tableData.price.ToString();
        }
        else
        {
            lb_Count.text = SelectCount.ToString();
            lb_Price.text = (data.tableData.price * SelectCount).ToString();
        }


    }

    public void OnClick_Yes()
    {
        if (del != null)
            del(SelectCount);

        Sound_Manager.Get.PlayEffect("Effect_sell");
        Hide();
    }
    
    public void OnClick_Up()
    {
        if (data.tableData.itemType == ItemType.Equipment ||
            data.tableData.itemType == ItemType.Weapon)
            return;

        if (data.count > SelectCount)
        {
            Sound_Manager.Get.PlayEffect("Effect_select");
            SelectCount++;
            Refresh();
        }
    }

    public void OnClick_Down()
    {
        if (data.tableData.itemType == ItemType.Equipment ||
            data.tableData.itemType == ItemType.Weapon)
            return;

        if (SelectCount > 1)
        {
            Sound_Manager.Get.PlayEffect("Effect_select");
            SelectCount--;
            Refresh();
        }
    }

    public void OnClick_Up_10()
    {
        if (data.tableData.itemType == ItemType.Equipment ||
            data.tableData.itemType == ItemType.Weapon)
            return;

        Sound_Manager.Get.PlayEffect("Effect_select");
        if (data.count > SelectCount + 10)
        {
            SelectCount += 10;
        }
        else
        {
            SelectCount = data.count;
        }
        Refresh();

    }

    public void OnClick_Down_10()
    {
        if (data.tableData.itemType == ItemType.Equipment ||
            data.tableData.itemType == ItemType.Weapon)
            return;

        Sound_Manager.Get.PlayEffect("Effect_select");

        if (SelectCount > 10)
        {
            SelectCount -= 10;
        }
        else
        {
            SelectCount = 1;
        }

        Refresh();
    }
}
