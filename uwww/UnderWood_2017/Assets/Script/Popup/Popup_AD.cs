﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_AD : Popup
{
    [System.Serializable]
    public struct Content
    {
        public UILabel lb_Count;
        public UISprite sp_Icon;
        public GameObject go_Check;
    }

    public List<Content> li_Content = new List<Content>();

    bool isPress;

    public override void Show()
    {
        Refresh();
        base.Show();
    }
    
    void Refresh()
    {
        isPress = false;
        string temp = PlayerPrefs.GetString("Last_AD_WatchingDay", "");
        //만약 날짜가 바뀌면 기존 광고 카운트 리셋
        if (System.DateTime.Now.ToShortDateString() != temp)
            Character.Get.AD_Count = 0;

        for (int i = 0; i < li_Content.Count; i++)
        {
            li_Content[i].go_Check.SetActive(i < Character.Get.AD_Count);
            li_Content[i].sp_Icon.spriteName = GetIconName(i);
            li_Content[i].lb_Count.text = Table_Manager.Get.li_AD_Reward[i].rewardCount.ToString();
        }
    }

    string GetIconName(int i)
    {
        string temp = "";
        switch (Table_Manager.Get.li_AD_Reward[i].rewardType)
        {
            case AD_RewardType.Gold:
                temp = "Gold";
                break;
            case AD_RewardType.Dia:
                temp = "Dia";
                break;
            case AD_RewardType.ETC:
                temp = string.Format("Icon_{0}", Table_Manager.Get.li_AD_Reward[i].rewardID);
                break;
        }
        return temp;
    }

    public void OnClick_AD()
    {
        if (isPress == true)
            return;

        if (Character.Get.AD_Count >= 5)
        {
            Popup_Manager.Get.Warning.Show(LM.DATA[14]);
            return;
        }

        if (Table_Manager.Get.li_AD_Reward[Character.Get.AD_Count].rewardType == AD_RewardType.Gold)
        {
            if (Character.Get.CheckAddGold(Table_Manager.Get.li_AD_Reward[Character.Get.AD_Count].rewardCount) == false)
            {
                Popup_Manager.Get.Warning.Show(LM.DATA[27]);
                return;
            }
        }
        else if(Table_Manager.Get.li_AD_Reward[Character.Get.AD_Count].rewardType == AD_RewardType.ETC)
        {
            if (Character.Get.CheckAddItem() == false)
            {
                Popup_Manager.Get.Warning.Show(LM.DATA[43]);
                return;
            }
        }

        isPress = true;
        Ads_Manager.Get.ShowRewardedAd(AD_CallBack);
    }

    void AD_CallBack()
    {
        Sound_Manager.Get.PlayEffect("Effect_buy");

        switch (Table_Manager.Get.li_AD_Reward[Character.Get.AD_Count].rewardType)
        {
            case AD_RewardType.Gold:
                Character.Get.AddGold(Table_Manager.Get.li_AD_Reward[Character.Get.AD_Count].rewardCount);
                break;
            case AD_RewardType.Dia:
                Character.Get.AddDia(Table_Manager.Get.li_AD_Reward[Character.Get.AD_Count].rewardCount);
                break;
            case AD_RewardType.ETC:
                Character.Get.AddItem(Table_Manager.Get.li_AD_Reward[Character.Get.AD_Count].rewardID,
                    Table_Manager.Get.li_AD_Reward[Character.Get.AD_Count].rewardCount);
                break;
        }

        Character.Get.AD_Count++;
        Character.Get.SavePlayerLocalData();

        PlayerPrefs.SetString("Last_AD_WatchingDay", System.DateTime.Now.ToShortDateString());
        Refresh();
    }
}
