﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Buy : Popup
{
    public UISprite sp_Icon;
    public UILabel lb_Title;
    public UILabel lb_Desc;
    public UILabel lb_Price;
    public delegate void DelClickYes();
    DelClickYes del;

    public void Show(PriceType _type, string _stTitle ,string _stDesc, int _price, DelClickYes _del)
    {
        switch (_type)
        {
            case PriceType.Cash:
                sp_Icon.spriteName = "Won";
                break;
            case PriceType.Dia:
                sp_Icon.spriteName = "Dia";
                break;
            case PriceType.Gold:
                sp_Icon.spriteName = "Gold";
                break;
            default:
                break;
        }

        lb_Title.text = _stTitle;
        lb_Price.text = _price.ToString();
        lb_Desc.text = _stDesc;

        del = _del;
        base.Show();
    }


    public void OnClick_Yes()
    {
        Hide();

        if (del != null)
            del();
    }

    public void OnClick_No()
    {
        Hide();
    }
}