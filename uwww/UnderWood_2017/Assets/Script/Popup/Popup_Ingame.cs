﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Ingame : Popup
{
    public enum MapObjectType
    {
        Inven,
        Storage,
        Upgrade,
        Alchemist,
        Customizing,
        FastTravel,
        CampUpgrade,
        Craft,
        Mine,
        LumberCamp,
        GoHome,

        None,
    }

    Character character;

    [SerializeField]
    private Iventory_Tooltip Tooltip;
    [SerializeField]
    private InventoryUI invenUI;
    [SerializeField]
    private StatusUI statusUI;
    [SerializeField]
    private StorageUI storageUI;
    [SerializeField]
    private UpgradeUI upgradeUI;
    [SerializeField]
    private AlchemyUI alchemyUI;
    [SerializeField]
    private CustomizingUI customizingUI;
    [SerializeField]
    private FastTravelUI fastTravelUI;
    [SerializeField]
    private CampUpgradeUI campUpgradeUI;
    [SerializeField]
    private CraftUI craftUI;

    public MapObjectType openType;

    public void Init()
    {
        character = Character.Get;

        invenUI.Init(character);
        statusUI.Init();
        storageUI.Init(character);
        upgradeUI.Init();
        alchemyUI.Init(character);
        fastTravelUI.Init();
        campUpgradeUI.Init(character);
        craftUI.Init(character);
        customizingUI.Init();
        Hide();
    }
    
    public void ResetStatusUI()
    {
        statusUI.Init();
    }

    public void Refresh()
    {
        switch (openType)
        {
            case MapObjectType.Inven:
                invenUI.Refresh();
                break;
            case MapObjectType.Storage:
                invenUI.Refresh();
                storageUI.Refresh();
                break;
            case MapObjectType.Upgrade:
                invenUI.Refresh();
                upgradeUI.Refresh();
                break;
            case MapObjectType.Alchemist:
                invenUI.Refresh();
                alchemyUI.Refresh();
                break;
            case MapObjectType.Customizing:
                customizingUI.Refresh();
                break;
            case MapObjectType.FastTravel:
                break;
            case MapObjectType.CampUpgrade:
                invenUI.Refresh();
                campUpgradeUI.Refresh();
                break;
            case MapObjectType.Craft:
                invenUI.Refresh();
                craftUI.Refresh();
                break;
            default:
                break;
        }
    }

    public void SetEquipIcon(EquipType _type)
    {
        statusUI.SetIcon_Equip(_type);
    }
    public void SetEquipIcon(EquipType _type, string _spriteName, GradeType _grade)
    {
        statusUI.SetIcon_Equip(_type, _spriteName, _grade);
    }

    public void SetWeaponIcon()
    {
        statusUI.SetIcon_Weapon();
    }
    public void SetWeaponIcon(string _spriteName, GradeType _grade)
    {
        statusUI.SetIcon_Weapon(_spriteName, _grade);
    }

    public void SelectItemChange(itemSlotData _data)
    {
        upgradeUI.SelectItemChange(_data);
    }

    public void RefreshGold()
    {
        invenUI.RefreshGold();
    }

    Vector3 statusUiPos2 = new Vector3(110, 100, 0);
    public void Show(MapObjectType _type)
    {
        openType = _type;

        switch (_type)
        {
            case MapObjectType.Inven:
                statusUI.Show(statusUiPos2);
                invenUI.Show();
                break;
            case MapObjectType.Storage:
                Sound_Manager.Get.PlayEffect("Effect_storage_open");
                storageUI.Show();
                invenUI.Show();
                break;
            case MapObjectType.Upgrade:
                upgradeUI.Show();
                invenUI.Show();
                break;
            case MapObjectType.Alchemist:
                alchemyUI.Show();
                invenUI.Show();
                break;
            case MapObjectType.Customizing:
                customizingUI.Show();
                break;
            case MapObjectType.FastTravel:
                fastTravelUI.Show();
                break;
            case MapObjectType.CampUpgrade:
                campUpgradeUI.Show();
                invenUI.Show();
                break;
            case MapObjectType.Craft:
                craftUI.Show();
                invenUI.Show();
                break;
            default:
                break;
        }
        base.Show();
    }
    public override void Hide()
    {
        invenUI.Hide();
        statusUI.Hide();
        storageUI.Hide();
        upgradeUI.Hide();
        alchemyUI.Hide();
        customizingUI.Hide();
        fastTravelUI.Hide();
        campUpgradeUI.Hide();
        craftUI.Hide();

        base.Hide();
    }
    
    public void AddInventory(int _size)
    {
        invenUI.AddSlot(_size);
    }
    public void AddStorage(int _size)
    {
        storageUI.AddSlot(_size);
    }
    public void SetInventory(int _size)
    {
        invenUI.SetSlotSize(_size);
    }
    public void SetStorage(int _size)
    {
        storageUI.SetSlotSize(_size);
    }


    //아이템 삭제,판매용
    //중복을 막거나 하나로 통합
    public void OnClick_SellItem()
    {
        invenUI.SetState(ItemContainer.Mode.Sell);
        storageUI.SetState(ItemContainer.Mode.Sell);
    }
    public void OnClick_DeleteItem()
    {
        invenUI.SetState(ItemContainer.Mode.Delete);
        storageUI.SetState(ItemContainer.Mode.Delete);
    }

    public void OnClick_StorageHide()
    {
        Sound_Manager.Get.PlayEffect("Effect_storage_close");
        Hide();
    }


    public void ShowTooltip(Table_Manager.Item _data, Vector3 _pos)
    {
        Tooltip.Show(_data, _pos);
    }

    public void HideTooltip()
    {
        Tooltip.Hide();
    }
}
