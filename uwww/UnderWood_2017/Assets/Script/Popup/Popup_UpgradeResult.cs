﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_UpgradeResult : Popup
{
    public UILabel lb_desc;
    public UISprite[] ar_Sprite;
    public UILabel lb_Upgrade;
    public UISprite sp_Icon;
    public ParticleSystem ps;

    public void Show(bool _isSuccess , int _id)
    {
        Table_Manager.Item find = Table_Manager.Get.FindItem(_id);
        sp_Icon.spriteName = find.iconName;

        string temp = find.id.ToString().Substring(2, 1);
        if (temp != "0")
            lb_Upgrade.text = "+" + temp;
        else
            lb_Upgrade.text = "";

        if (_isSuccess == true)
        {
            Sound_Manager.Get.PlayEffect("Effect_done");

            lb_desc.text = string.Format("{0}\n[FFBB00]{1}", find.name, LM.DATA[25]);
            for (int i = 0; i < ar_Sprite.Length; i++)
            {
                ar_Sprite[i].color = ColorPreset.HexToColor("F6D626FF");
            }

            ps.gameObject.SetActive(true);
            //ps.Play();
        }
        else
        {
            Sound_Manager.Get.PlayEffect("Effect_brokenglass");

            lb_desc.text = string.Format("{0}\n[666666]{1}", find.name , LM.DATA[26]);
            for (int i=0; i<ar_Sprite.Length; i++)
            {
                ar_Sprite[i].color = ColorPreset.HexToColor("636363FF");
            }

            ps.gameObject.SetActive(false);
        }

        base.Show();
    }

}
