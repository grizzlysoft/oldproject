﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Shop : Popup
{
    public UILabel lb_HaveDia;
    public UILabel lb_HaveGold;

    public GameObject go_UpText;
    public Transform tr_GoldPos;
    public Transform tr_DiaPos;

    public GameObject go_Content;
    List<ShopContent> li_Content = new List<ShopContent>();
    
    public UIGrid gr_Grid;
    public UIScrollView sc_View;

    ShopTapType currentTap;

    List<Table_Manager.ShopData> li_Table;

    public void Init()
    {
        li_Table = Table_Manager.Get.li_ShopData;
        //최대 팝업 개수 구하기
        int MAX = 0;
        int findMax = 0;
        for (int i=0;i< (int)ShopTapType.MAX; i++)
        {
            findMax = li_Table.FindAll(r => r.tapType == (ShopTapType)i).Count;
            if (MAX < findMax)
                MAX = findMax;
        }


        GameObject go_Temp;
        ShopContent scriptTemp;
        for (int i=0; i< MAX; i++)
        {
            go_Temp = Instantiate(go_Content, gr_Grid.transform) as GameObject;
            scriptTemp = go_Temp.GetComponent<ShopContent>();
            scriptTemp.Init(this);
            li_Content.Add(scriptTemp);
        }
    }

    public override void Show()
    {
        if (li_Content.Count == 0)
            Init();

        base.Show();
        StartCoroutine(Reposition());
    }

    IEnumerator Reposition()
    {
        yield return new WaitForSeconds(0.1f);
        OnClick_Dia();
    }

    public void Refresh()
    {
        Refresh_NoReposition();
        
        gr_Grid.Reposition();
        sc_View.ResetPosition();
    }

    public void Refresh_NoReposition()
    {
        lb_HaveDia.text = Character.Get.dia.ToString();
        lb_HaveGold.text = Character.Get.gold.ToString();

        for (int i = 0; i < li_Content.Count; i++)
        {
            li_Content[i].Hide();
        }

        //현재 탭 데이터 찾기
        List<Table_Manager.ShopData> Find = li_Table.FindAll(r => r.tapType == currentTap);
        for (int i = 0; i < Find.Count; i++)
        {
            li_Content[i].Show(Find[i]);
        }
    }

    public void OnClick_Dia()
    {
        currentTap = ShopTapType.Cash;
        Refresh();
    }
    public void OnClick_Skin()
    {
        currentTap = ShopTapType.Skin;
        Refresh();
    }
    public void OnClick_Hair()
    {
        currentTap = ShopTapType.Hair;
        Refresh();
    }
    public void OnClick_Gold()
    {
        currentTap = ShopTapType.Gold;
        Refresh();
    }
    public void OnClick_ETC()
    {
        currentTap = ShopTapType.ETC;
        Refresh();
    }

    public void MakeText(PriceType _type , int _price)
    {
        GameObject temp;
        if (_type == PriceType.Gold)
        {
            temp = Instantiate(go_UpText, tr_GoldPos) as GameObject;
        }
        else
        {
            temp = Instantiate(go_UpText, tr_DiaPos) as GameObject;
        }
        temp.transform.localPosition = Vector3.zero;
        temp.GetComponent<UpFadeText>().Init(_price);
    }
}
