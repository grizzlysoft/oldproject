﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Die : Popup
{
    public UILabel lb_Desc;
    public TweenAlpha tw_alpha;

    override public void Show()
    {
        int rnd = Random.Range(0, Table_Manager.Get.li_DieDesc.Count);
        lb_Desc.text = Table_Manager.Get.li_DieDesc[rnd].desc;
        //기존에 열려있는 팝업을 다 닫아야 한다
        Popup_Manager.Get.AllHide();
        base.Show();
        tw_alpha.ResetToBeginning();
        tw_alpha.enabled = true;
    }

    override public void Hide()
    {
        //못닫는 팝업이라서 사용 불가능
    }

    public void OnClick_Home()
    {
        base.Hide();
        Map_Manager.Get.ChangeMap_Pos(0);
    }

    public void OnClick_Dia()
    {
        if (Character.Get.CheckHaveDia(1) == false)
        {
            Popup_Manager.Get.Warning.Show(LM.DATA[86]);
            return;
        }
        //다이아 구매 팝업이 뜨면안됨 (Use안에 코드가 있지만, 위에서 확인한다)
        Character.Get.UseDia(1);

        if (Map_Manager.Get.GetCurrentMapData().type == MapType.Defence)
        {
            if (Map_Manager.Get.defenceObj.HP < 40)
            {
                Map_Manager.Get.defenceObj.Show(40);
            }
        }
        else if (Map_Manager.Get.GetCurrentMapData().type == MapType.TimeAttack)
            IngameUI_Manager.Get.AddTime(30f);

        Character.Get.Resirrection();
        Character.Get.invincibility(3.5f);
        Sound_Manager.Get.PlayBGM(Map_Manager.Get.GetCurrentMapData().bgmName);
        base.Hide();
    }

    public void OnClick_AD()
    {
        Ads_Manager.Get.ShowRewardedAd(AD_CallBack);
    }

    void AD_CallBack()
    {
        if (Map_Manager.Get.GetCurrentMapData().type == MapType.Defence)
        {
            if(Map_Manager.Get.defenceObj.HP < 20)
            {
                Map_Manager.Get.defenceObj.Show(20);
            }
        }
        else if (Map_Manager.Get.GetCurrentMapData().type == MapType.TimeAttack)
            IngameUI_Manager.Get.AddTime(15f);

        Character.Get.Resirrection();
        Character.Get.invincibility(3.5f);
        Sound_Manager.Get.PlayBGM(Map_Manager.Get.GetCurrentMapData().bgmName);
        base.Hide();
    }

}
