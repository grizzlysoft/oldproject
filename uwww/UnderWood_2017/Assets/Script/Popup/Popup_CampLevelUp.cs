﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_CampLevelUp : Popup
{
    public UISprite sp_Icon;
    public UILabel lb_Desc;
    public UIGrid gr_Grid;
    public UIScrollView sc_View;
    public GameObject go_Content;
    public TweenAlpha tw_Alpha;

    List<CustomizingContent> li_Content = new List<CustomizingContent>();
    Table_Manager.CampData data;

    List<Table_Manager.CampData> li_Table;
    bool isInit = false;

    public void Init()
    {
        li_Table = Table_Manager.Get.li_CampData;
        int MAX = 0;
        //테이블에서 가장 많은 요소 만큼 미리 생성 -> Hide 해두기
        for (int i = 0; i < li_Table.Count; i++)
        {
            if (li_Table[i].li_AddCustomizing.Count == 0)
                continue;

            if (MAX < li_Table[i].li_AddCustomizing.Count)
                MAX = li_Table[i].li_AddCustomizing.Count;
        }

        GameObject temp;
        CustomizingContent tempScp;
        for (int i = 0; i < MAX; i++)
        {
            temp = Instantiate(go_Content, gr_Grid.transform) as GameObject;
            tempScp = temp.GetComponent<CustomizingContent>();
            li_Content.Add(tempScp);
        }

        isInit = true;
    }

    public override void Show()
    {
        if (isInit == false)
            Init();

        int Level = Character.Get.CampLevel;
        //코스튬은 다 꺼준다
        for(int i=0; i< li_Content.Count; i++)
        {
            li_Content[i].gameObject.SetActive(false);
        }

        data = Table_Manager.Get.li_CampData[Level];
        lb_Desc.text = data.name;

        //현재 주둔지 레벨업시 오픈 타입에 따라서 처리-
        if (data.li_AddCustomizing.Count == 0)
        {
            //일반 기능 강화/오픈
            sp_Icon.gameObject.SetActive(true);
            sp_Icon.spriteName = data.iconName;
        }
        else
        {
            sp_Icon.gameObject.SetActive(false);

            //코스튬 추가
            for (int i = 0; i < data.li_AddCustomizing.Count; i++)
            {
                li_Content[i].Init(data.li_AddCustomizing[i]);
                li_Content[i].gameObject.SetActive(true);
            }
        }
    
        gr_Grid.Reposition();
        sc_View.ResetPosition();

        base.Show();

        tw_Alpha.ResetToBeginning();
        tw_Alpha.enabled = true;
    }

    public override void Hide()
    {
    }

    public void HideNow()
    {
        base.Hide();
        if(Character.Get.CampLevel < 8)
        {
            Popup_Manager.Get.Check.Show(LM.DATA[74 + Character.Get.CampLevel]);
        }
    }

}
