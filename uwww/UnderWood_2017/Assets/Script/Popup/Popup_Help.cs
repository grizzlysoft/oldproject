﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Help : Popup
{
    public UITexture tx_BG;
    int index;

    public void Show(int _index)
    {
        index = _index;

        Texture findImg = 
            Resources.Load(string.Format("TextureImg/Help_{0}", index), typeof(Texture)) as Texture;

        tx_BG.mainTexture = findImg;

        base.Show();
    }
}
