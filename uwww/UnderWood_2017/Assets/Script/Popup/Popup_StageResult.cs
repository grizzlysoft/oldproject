﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_StageResult : Popup
{
    public UILabel lb_GetType;
    public UISprite sp_GetType;

    //팝업 리스트에 넣지 않는다 <- 캐릭터가 자유로이 움직일수있도록 + ESC 키에 의한 영향 x 
    public TweenAlpha tw_Alpha;

    public GameObject go_HomeOnly;
    public GameObject go_Normal;

    public void Show(bool _isDia,int _getValue)
    {
        Popup_Manager.Get.AllHide();

        if (_isDia == true)
        {
            sp_GetType.spriteName = "Dia";
            lb_GetType.text = string.Format("{0} - {1}", LM.DATA[58] ,_getValue);
        }
        else
        {
            sp_GetType.spriteName = "Gold";
            lb_GetType.text = string.Format("{0} - {1}", LM.DATA[59], _getValue);
        }

        //마지막 스테이지 면 마을로 가기만 활성화
        if (Map_Manager.Get.GetCurrentMapData().stageID == Table_Manager.Get.li_MapData[Table_Manager.Get.li_MapData.Count-1].stageID)
        {
            go_Normal.SetActive(false);
            go_HomeOnly.SetActive(true);
        }
        else
        {
            go_Normal.SetActive(true);
            go_HomeOnly.SetActive(false);
        }


        this.gameObject.SetActive(true);
        tw_Alpha.ResetToBeginning();
        tw_Alpha.enabled = true;
    }

    public override void Hide()
    {
        this.gameObject.SetActive(false);
    }

    public void OnClick_Home()
    {
        OnClick_Hide();
        Map_Manager.Get.ChangeMap_Pos(0);
    }

    public void OnClick_Next()
    {
        OnClick_Hide();
        Map_Manager.Get.ChangeMap_Pos(Map_Manager.Get.GetCurrentMapData().stageID + 1);
    }
}
