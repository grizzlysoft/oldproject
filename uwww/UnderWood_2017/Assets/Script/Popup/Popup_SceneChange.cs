﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_SceneChange : Popup
{
    public TweenPosition tw_Pos;

    public override void Show()
    {
        tw_Pos.gameObject.SetActive(false);
        base.Show();
    }

    public IEnumerator FadeIN()
    {
        tw_Pos.gameObject.SetActive(true);

        tw_Pos.from = new Vector3(-2800, 0, 0);
        tw_Pos.to = new Vector3(0, 0, 0);
        tw_Pos.ResetToBeginning();
        tw_Pos.enabled = true;

        while (true)
        {
            if (tw_Pos.enabled == false)
                break;

            yield return null;
        }
    }

    public IEnumerator FadeOut()
    {
        tw_Pos.gameObject.SetActive(true);

        tw_Pos.from = new Vector3(0, 0, 0);
        tw_Pos.to = new Vector3(2800, 0, 0);
        tw_Pos.ResetToBeginning();
        tw_Pos.enabled = true;

        while (true)
        {
            if (tw_Pos.enabled == false)
                break;

            yield return null;
        }
    }

    public void EndTween()
    {
        tw_Pos.enabled = false;
    }

    public override void Hide()
    {
        //this.gameObject.SetActive(false);
    }

    public void HideNow()
    {
        base.Hide();
    }
}
