﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomizingUI : MonoBehaviour
{
    public GameObject Content;
    List<CustomizingContent> li_Content = new List<CustomizingContent>();

    public UIScrollView sc_View_Skin;
    public UIGrid gr_Grid_Skin;

    public UIScrollView sc_View_Hair;
    public UIGrid gr_Grid_Hair;

    public UIScrollView sc_View_Face;
    public UIGrid gr_Grid_Face;

    public UISprite sp_btnHair;
    public UISprite sp_btnSkin;
    public UISprite sp_btnFace;

    public UISprite sp_VisibleHelm;
    public UISprite sp_VisibleChick;

    public Transform tr_priviewObj;
    public Animation anim;
    Dictionary<string, SpriteRenderer> dic_Equipment = new Dictionary<string, SpriteRenderer>();
    List<SpriteRenderer> li_BodySprite = new List<SpriteRenderer>();
    public List<SpriteRenderer> li_Weapon = new List<SpriteRenderer>();
    public SpriteRenderer sp_Shield;

    public SpriteRenderer sp_Hair;
    public SpriteMask Helm_Mask;

    public SpriteRenderer sp_Face;
    public GameObject go_Chick;


    public void Init()
    {
        //피부
        GameObject go_Temp;
        CustomizingContent scriptTemp;
        for (int i=0; i<Table_Manager.Get.li_CustomizingData.Count; i++)
        {
            if (Table_Manager.Get.li_CustomizingData[i].type == CustomizingType.Skin)
                go_Temp = Instantiate(Content, gr_Grid_Skin.transform) as GameObject;
            else if (Table_Manager.Get.li_CustomizingData[i].type == CustomizingType.Hair)
                go_Temp = Instantiate(Content, gr_Grid_Hair.transform) as GameObject;
            else if (Table_Manager.Get.li_CustomizingData[i].type == CustomizingType.Face)
                go_Temp = Instantiate(Content, gr_Grid_Face.transform) as GameObject;
            else
                continue;

            scriptTemp = go_Temp.GetComponent<CustomizingContent>();
            scriptTemp.Init(Table_Manager.Get.li_CustomizingData[i] ,this);
            li_Content.Add(scriptTemp);
        }

        SpriteRenderer[] ar_Sprite = tr_priviewObj.GetComponentsInChildren<SpriteRenderer>();
        for (int i = 0; i < ar_Sprite.Length; i++)
        {
            if (ar_Sprite[i].tag == "BodySprite")
            {
                li_BodySprite.Add(ar_Sprite[i]);
            }
            else if (ar_Sprite[i].tag == "Equip")
            {
                ar_Sprite[i].gameObject.SetActive(false);
                dic_Equipment.Add(ar_Sprite[i].name, ar_Sprite[i]);
            }
        }
    }

    public void Show()
    {
        Onclick_Hair();
        this.gameObject.SetActive(true);
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }

    public void Refresh(bool isResetPosition = false)
    {
        for (int i = 0; i < li_Content.Count; i++)
        {
            li_Content[i].Refresh();
        }

        //미리보기 동기화 - 몸색
        Table_Manager.CustomizingData Find = Table_Manager.Get.li_CustomizingData.Find(r => r.id == Character.Get.SkinIndex);
        foreach (var item in li_BodySprite)
        {
            item.color = Find.color;
        }

        //장비
        foreach (var item in dic_Equipment)
        {
            item.Value.gameObject.SetActive(Character.Get.dic_Equipment[item.Key].gameObject.activeSelf);
            item.Value.sprite = Character.Get.dic_Equipment[item.Key].sprite;
        }
        Helm_Mask.sprite = Character.Get.dic_Equipment["Equip_Helm"].sprite;

        //무기
        for (int i=0; i< li_Weapon.Count; i++)
        {
            li_Weapon[i].gameObject.SetActive(false);
        }

        sp_Shield.gameObject.SetActive(false);

        if(Character.Get.currentWeaponType != WeaponType.NoWeapon)
        {
            li_Weapon[(int)Character.Get.currentWeaponType].gameObject.SetActive(true);
            li_Weapon[(int)Character.Get.currentWeaponType].sprite = Character.Get.equip_Weapon.sp_RightWeapon.sprite;
            if (Character.Get.equip_Weapon.sp_LeftWeapon != null)
            {
                sp_Shield.gameObject.SetActive(true);
                //한손검
                sp_Shield.sprite = Character.Get.equip_Weapon.sp_LeftWeapon.sprite;
            }
        }

        //몸
        sp_Hair.sprite = Character.Get.sp_Hair.sprite;
        sp_Hair.maskInteraction = Character.Get.sp_Hair.maskInteraction;
        sp_Hair.color = Character.Get.sp_Hair.color;

        sp_Face.sprite = Character.Get.sp_Face.sprite;
        go_Chick.SetActive(Character.Get.Visible_Chick == 1);

        sp_VisibleHelm.gameObject.SetActive(Character.Get.Visible_Helm == 1);
        sp_VisibleChick.gameObject.SetActive(Character.Get.Visible_Chick == 1);

        string animName = string.Format("{0}_{1}", Character.Get.currentWeaponType, Character.Get.currentState);
        anim.Play(animName);

        if (isResetPosition == true)
        {
            gr_Grid_Skin.Reposition();
            sc_View_Skin.ResetPosition();

            gr_Grid_Hair.Reposition();
            sc_View_Hair.ResetPosition();

            gr_Grid_Face.Reposition();
            sc_View_Face.ResetPosition();
        }
    }


    public void Onclick_Hair()
    {
        Sound_Manager.Get.PlayEffect("Effect_select");

        sp_btnFace.color = ColorPreset.UI_Color_Normal;
        sp_btnSkin.color = ColorPreset.UI_Color_Normal;
        sp_btnHair.color = ColorPreset.UI_Color_Select;

        sc_View_Hair.gameObject.SetActive(true);
        sc_View_Skin.gameObject.SetActive(false);
        sc_View_Face.gameObject.SetActive(false);

        Refresh(true);
    }

    public void Onclick_Skin()
    {
        Sound_Manager.Get.PlayEffect("Effect_select");

        sp_btnFace.color = ColorPreset.UI_Color_Normal;
        sp_btnSkin.color = ColorPreset.UI_Color_Select;
        sp_btnHair.color = ColorPreset.UI_Color_Normal;

        sc_View_Hair.gameObject.SetActive(false);
        sc_View_Skin.gameObject.SetActive(true);
        sc_View_Face.gameObject.SetActive(false);

        Refresh(true);
    }

    public void Onclick_Face()
    {
        Sound_Manager.Get.PlayEffect("Effect_select");

        sp_btnFace.color = ColorPreset.UI_Color_Select;
        sp_btnSkin.color = ColorPreset.UI_Color_Normal;
        sp_btnHair.color = ColorPreset.UI_Color_Normal;


        sc_View_Hair.gameObject.SetActive(false);
        sc_View_Skin.gameObject.SetActive(false);
        sc_View_Face.gameObject.SetActive(true);

        Refresh(true);
    }

    public void OnClick_Visible_Helm()
    {
        Character.Get.ChangeVisible_Helm();
        Refresh();
    }
    

    public void OnClick_Visible_Chick()
    {
        Character.Get.ChangeVisible_Chick();
        Refresh();
    }
}