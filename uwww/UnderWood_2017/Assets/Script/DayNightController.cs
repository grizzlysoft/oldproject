﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNightController : MonoBehaviour
{
    [SerializeField]
    SpriteRenderer sp_Night;

    public void SetNight(float _darkness)
    {
        sp_Night.color = new Color(0, 0, 0, _darkness);
    }
}
    
