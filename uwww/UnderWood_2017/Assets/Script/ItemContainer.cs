﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemContainer : MonoBehaviour
{
    public enum Mode
    {
        Close,
        Sell,
        Delete,
    }

    protected Character character;

    protected List<ItemContent> li_slot = new List<ItemContent>();
    public GameObject go_Content;
    public UIScrollView scrollView;
    protected UIGrid grid = null;
    public GameObject go_Grid;

    public UISprite sp_BG;
    public UISprite sp_TitleBG;
    
    protected itemSlotData selectItem;

    public Mode currentMode;

    public void Init(Character _characterData)
    {
        character = _characterData;
    }

    //-380, 210
    public void Show()
    {
        Popup_Manager.Get.Ingame.HideTooltip();
        Refresh(true);
        this.gameObject.SetActive(true);
    }
    public void Hide()
    {
        this.gameObject.SetActive(false);
    }

    public void AddSlot(int _size)
    {
        GameObject temp;
        ItemContent tempScp;
        for (int i = 0; i < _size; i++)
        {
            temp = Instantiate(go_Content, grid.transform) as GameObject;
            tempScp = temp.GetComponent<ItemContent>();
            tempScp.Init(this);
            li_slot.Add(tempScp);
        }

        Refresh(true);
    }

    public void SetSlotSize(int _size)
    {
        li_slot.Clear();
        if (grid != null)
            Destroy(grid.gameObject);

        GameObject tempGrid = Instantiate(go_Grid, scrollView.transform) as GameObject;
        grid = tempGrid.GetComponent<UIGrid>();
        tempGrid.SetActive(true);

        AddSlot(_size);
    }



    //판매,파괴 버튼 행동
    protected void CheckDeleteItem()
    {
        if (selectItem.count == -1)
        {
            Popup_Manager.Get.Warning.Show(LM.DATA[16]);
            return;
        }
        //마지막 남은 무기면 못 부숨
        if (selectItem.tableData.itemType == ItemType.Weapon && CheckWeaponCount() <= 1)
        {
            Popup_Manager.Get.Warning.Show(LM.DATA[17]);
            return;
        }

        Popup_Manager.Get.YesNo.Show(string.Format("[ffaa00]- {0} -[-]", selectItem.tableData.name), LM.DATA[18], DeleteItem);
    }
    protected void CheckSellItem()
    {
        //MaxGold 이상 소지시 못판다
        if (ConstValue.MaxGold < character.gold + selectItem.tableData.price)
        {
            Popup_Manager.Get.Warning.Show(LM.DATA[27]);
            return;
        }
        if (selectItem.count == -1)
        {
            Popup_Manager.Get.Warning.Show(LM.DATA[16]);
            return;
        }
        //마지막 남은 무기면 못 판다 
        if (selectItem.tableData.itemType == ItemType.Weapon && CheckWeaponCount() <= 1)
        {
            Popup_Manager.Get.Warning.Show(LM.DATA[37]);
            return;
        }

        Popup_Manager.Get.Selling.Show(selectItem, SellItem, true);
    }
    int CheckWeaponCount()
    {
        int weaponCount = 0;
        for (int i = 0; i < character.GetList_Item().Count; i++)
        {
            if (character.GetList_Item()[i].tableData.itemType == ItemType.Weapon)
                weaponCount++;
        }

        for (int i = 0; i < character.GetList_Storage().Count; i++)
        {
            if (character.GetList_Storage()[i].tableData.itemType == ItemType.Weapon)
                weaponCount++;
        }
        return weaponCount;
    }
    
    public void SetState(Mode _state)
    {
        if (currentMode == _state)
            currentMode = Mode.Close;
        else
            currentMode = _state;

        switch (currentMode)
        {
            case Mode.Close:
                sp_BG.color      = ColorPreset.UI_Color_Normal;
                sp_TitleBG.color = ColorPreset.UI_Color_Normal;
                break;
            case Mode.Sell:
                sp_BG.color      = ColorPreset.UI_Color_SellItem;
                sp_TitleBG.color = ColorPreset.UI_Color_SellItem;
                break;
            case Mode.Delete:
                sp_BG.color      = ColorPreset.UI_Color_DeleteItem;
                sp_TitleBG.color = ColorPreset.UI_Color_DeleteItem;
                break;
            default:
                break;
        }

        SetStateIcon();
    }

    abstract protected void SetStateIcon();

    abstract protected void SellItem(int _sellCount);
    abstract protected void DeleteItem();

    abstract public void ClickItem(itemSlotData _data);
    abstract public void OnClick_Sort();
    abstract public void Refresh(bool isReposition = false);

}
