﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusUI : MonoBehaviour
{
    public UISprite sp_HelmSlot;
    public UISprite sp_BodySlot;
    public UISprite sp_ArmSlot;
    public UISprite sp_HandSlot;
    public UISprite sp_LegSlot;
    public UISprite sp_FootSlot;
    public UISprite sp_WeaponSlot;

    public Transform tr_Slot;

    public List<UISprite> li_Slot = new List<UISprite>();
    public UISprite sp_WSlot; //이게 색 변경용 

    public void Show(Vector3 _pos)
    {
        this.transform.localPosition = _pos;
        this.gameObject.SetActive(true);
    }
    public void Hide()
    {
        this.gameObject.SetActive(false);
    }

    public void Init()
    {
        //장착 장비 UI 초기화
        SetIcon_Equip(EquipType.Armor);
        SetIcon_Equip(EquipType.Arm);
        SetIcon_Equip(EquipType.Helm);
        SetIcon_Equip(EquipType.Leg);
        SetIcon_Equip(EquipType.Foot);
        SetIcon_Equip(EquipType.Hand);
        SetIcon_Weapon();
    }

    public void SetIcon_Equip(EquipType _type)
    {
        li_Slot[(int)_type].color = Color.white;

        switch (_type)
        {
            case EquipType.Armor:
                sp_BodySlot.spriteName = "BodySlot";
                break;
            case EquipType.Arm:
                sp_ArmSlot.spriteName = "ArmSlot";
                break;
            case EquipType.Hand:
                sp_HandSlot.spriteName = "HandSlot";
                break;
            case EquipType.Leg:
                sp_LegSlot.spriteName = "LegSlot";
                break;
            case EquipType.Foot:
                sp_FootSlot.spriteName = "FootSlot";
                break;
            case EquipType.Helm:
                sp_HelmSlot.spriteName = "HelmSlot";
                break;
            default:
                break;
        }
    }
    public void SetIcon_Equip(EquipType _type, string _spriteName, GradeType _grade)
    {
        li_Slot[(int)_type].color = ColorPreset.GetGradeColor(_grade);

        switch (_type)
        {
            case EquipType.Armor:
                sp_BodySlot.spriteName = _spriteName;
                break;
            case EquipType.Arm:
                sp_ArmSlot.spriteName = _spriteName;
                break;
            case EquipType.Hand:
                sp_HandSlot.spriteName = _spriteName;
                break;
            case EquipType.Leg:
                sp_LegSlot.spriteName = _spriteName;
                break;
            case EquipType.Foot:
                sp_FootSlot.spriteName = _spriteName;
                break;
            case EquipType.Helm:
                sp_HelmSlot.spriteName = _spriteName;
                break;
            default:
                break;
        }
    }

    public void SetIcon_Weapon()
    {
        sp_WSlot.color = Color.white;
        sp_WeaponSlot.spriteName = "WeaponSlot";
    }
    public void SetIcon_Weapon(string _spriteName, GradeType _grade)
    {
        sp_WSlot.color = ColorPreset.GetGradeColor(_grade);
        sp_WeaponSlot.spriteName = _spriteName;
    }
}
