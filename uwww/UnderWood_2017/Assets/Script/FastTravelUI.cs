﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FastTravelUI : MonoBehaviour
{
    public GameObject go_Content;
    public List<FastTravelContent> li_Content = new List<FastTravelContent>();

    public UIScrollView sc_View;
    public UIGrid gr_Grid;

    public DropInfoUI dropInfoUI;
    public GameObject go_notice;

    List<Table_Manager.MapData> li_Table;

    public void Init()
    {
        li_Table = Table_Manager.Get.li_MapData.FindAll(r=> r.isfastTrevel == true);

        GameObject temp;
        FastTravelContent tempScp;
        for (int i = 0; i < li_Table.Count; i++)
        {
            temp = Instantiate(go_Content, gr_Grid.transform) as GameObject;
            tempScp = temp.GetComponent<FastTravelContent>();
            tempScp.Init(this);
            li_Content.Add(tempScp);
        }

        dropInfoUI.Init();
        SelectMap(li_Table[0] , li_Content[0].transform);
    }

    public void Show()
    {
        //모든 요소를 꺼줌
        for (int i = 0; i < li_Content.Count; i++)
        {
            li_Content[i].Hide();
        }
        
        //오픈한 빠른이동 지점만 보여줌
        for(int i=0; i< li_Table.Count; i++)
        {
            if(Character.Get.stageLevel >= li_Table[i].stageID)
                li_Content[i].Show(li_Table[i]);
        }

        gr_Grid.Reposition();
        sc_View.ResetPosition();

        dropInfoUI.Refresh();

        this.gameObject.SetActive(true);
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }

    public void SelectMap(Table_Manager.MapData _Find , Transform _tr)
    {
        go_notice.transform.parent = _tr;
        go_notice.transform.localPosition = new Vector3(-144, 0, 0);

        dropInfoUI.SetData(_Find);
        dropInfoUI.Refresh();
    }
}
