﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextMapBox : MonoBehaviour
{
    public eDiraction Dir;

    public void OnTriggerStay2D(Collider2D coll)
    {
        if (coll.tag == "UserCharacter")
        {
            switch (Dir)
            {
                case eDiraction.Left:
                    Character.Get.SetCharacterPosition(-1); break;
                case eDiraction.Right:
                    Character.Get.SetCharacterPosition(1); break;

            }
        }
    }

}
