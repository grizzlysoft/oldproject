﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoyStickPressButton : MonoBehaviour
{
    public UISprite sp_CoolDown = null;
    float MaxTime = 0;
    float RemainTime = 0;

    public bool isPress;
	// Use this for initialization
	void Start ()
    {
        MaxTime = 0;
        RemainTime = 0;
        if(sp_CoolDown != null)
            sp_CoolDown.fillAmount = 0.0f;

        UIEventListener.Get(this.gameObject).onPress = OnPressButton;
    }
	
    void OnPressButton(GameObject _go ,bool _isPress)
    {
        isPress = _isPress;
    }

    public void SetCoolDown(float _Time)
    {
        if (sp_CoolDown == null)
            return;

        MaxTime = _Time;
        RemainTime = MaxTime;
        sp_CoolDown.fillAmount = 1.0f;
    }

    private void Update()
    {
        if(RemainTime != 0)
        {
            RemainTime -= Time.deltaTime;
            sp_CoolDown.fillAmount = RemainTime / MaxTime;

            if(RemainTime <= 0)
            {
                RemainTime = 0;
                sp_CoolDown.fillAmount = 0.0f;
            }
        }
    }
}
