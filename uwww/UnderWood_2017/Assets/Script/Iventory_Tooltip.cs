﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public class Iventory_Tooltip : MonoBehaviour
{

    public UISprite sp_Icon;
    public UILabel lb_Title;
    public UILabel lb_Desc;
    public UILabel lb_Price;
    public UISprite sp_BG;
    public UILabel lb_Grade;

    public void Show(Table_Manager.Item _data , Vector3 _pos)
    {
        this.transform.position = _pos;

        sp_Icon.spriteName = _data.iconName;
        lb_Title.text = _data.name;
        SetDesc(_data);
        lb_Price.text = _data.price.ToString();

        lb_Title.color = ColorPreset.GetGradeColor(_data.grade);
        lb_Grade.text = ColorPreset.GetGradeText(_data.grade);
        lb_Grade.color = ColorPreset.GetGradeColor(_data.grade);

        this.gameObject.SetActive(true);
    }

    void SetDesc(Table_Manager.Item _data)
    {
        switch (_data.itemType)
        {
            case ItemType.Equipment:
                Table_Manager.EquipmentData findArmor = Table_Manager.Get.li_Equipment.Find(r => r.id == _data.id);
                if (findArmor == null)
                {
                    Debug.LogError("없는 무기 정보");
                    return;
                }

                StringBuilder st = new StringBuilder();
                if (findArmor.DEF != 0)
                {
                    st.Append(string.Format("{0}  {1}", LM.DATA[1], findArmor.DEF));
                }
                if (findArmor.ATK != 0)
                {
                    if(st.ToString() != "")
                      st.AppendLine();
                    st.Append(string.Format("{0}  {1}", LM.DATA[0], findArmor.ATK));
                }

                lb_Desc.text = st.ToString();
                break;
            case ItemType.Weapon:
                //공격력
                //크리티컬
                //방어력
                Table_Manager.WeaponData findWeapon = Table_Manager.Get.li_Weapon.Find(r => r.id == _data.id);
                if(findWeapon == null)
                {
                    Debug.LogError("없는 무기 정보");
                    return;
                }

                StringBuilder st1 = new StringBuilder();
                st1.Append(string.Format("{0}  {1} ~ {2}", LM.DATA[0], findWeapon.ATK_Min, findWeapon.ATK_Max));
                if (findWeapon.Crit != 0)
                {
                    st1.AppendLine();
                    st1.Append(string.Format("{0}  {1}%", LM.DATA[4], findWeapon.Crit));
                }
                if (findWeapon.DEF != 0)
                {
                    st1.AppendLine();
                    st1.Append(string.Format("{0}  {1}",LM.DATA[1], findWeapon.DEF));
                }

                lb_Desc.text = st1.ToString();

                break;
            case ItemType.HP_Potion:
            case ItemType.STA_Potion:
                {
                    Table_Manager.UseItem findPotion = Table_Manager.Get.li_UseItem.Find(r=> r.id == _data.id);
                    lb_Desc.text = string.Format(findPotion.desc, findPotion.value);
                }
                break;
            case ItemType.Dye:
            case ItemType.TeleportScroll:
            case ItemType.ETC:
                {
                    lb_Desc.text = _data.desc;
                }
                break;
            default:
                break;
        }
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }
    
}
