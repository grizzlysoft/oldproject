﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraftListViewer : MonoBehaviour
{
    public enum ItemListType
    {
        ETC,
        Weapon,
        Equipment,
        MAX,
    }


    public GameObject go_Content;
    public List<CraftContent> li_Content = new List<CraftContent>();

    public UIScrollView sc_View;
    public UIGrid gr_Grid;

    List<Table_Manager.Item> li_ETC;
    List<Table_Manager.WeaponData> li_Weapon;
    List<Table_Manager.EquipmentData> li_Equipment;

    public UILabel lb_Title;

    ItemListType currentType;

    public void Init(CraftUI _parent)
    {
        li_ETC = Table_Manager.Get.li_ETC;
        li_Weapon = Table_Manager.Get.li_Weapon;
        li_Equipment = Table_Manager.Get.li_Equipment;

        GameObject temp;
        CraftContent tempScp;
        for (int i = 0; i < Table_Manager.Get.MAX_ITEM_CONTENT; i++)
        {
            temp = Instantiate(go_Content, gr_Grid.transform) as GameObject;
            tempScp = temp.GetComponent<CraftContent>();
            tempScp.Init(_parent);
            li_Content.Add(tempScp);
        }

        Refresh(ItemListType.ETC);
    }

    public void RefreshCurrent()
    {
        Refresh(currentType);
    }

    void Refresh(ItemListType _Type)
    {
        currentType = _Type;

        for (int i = 0; i < li_Content.Count; i++)
        {
            li_Content[i].Hide();
        }


        int index = 0;
        switch (currentType)
        {
            case ItemListType.ETC:
                //40
                lb_Title.text = LM.DATA[40];
                for (int i = 0; i < li_ETC.Count; i++)
                {
                    if (li_ETC[i].li_Material_ID == null || Character.Get.CampData().creftLevel < li_ETC[i].creftLevel)
                        continue;
                    li_Content[index].Show(li_ETC[i]);
                    index++;
                }
                break;
            case ItemListType.Weapon:
                lb_Title.text = LM.DATA[41];
                for (int i = 0; i < li_Weapon.Count; i++)
                {
                    if (li_Weapon[i].li_Material_ID == null || Character.Get.CampData().creftLevel < li_Weapon[i].creftLevel)
                        continue;
                    li_Content[index].Show(li_Weapon[i]);
                    index++;
                }
                break;
            case ItemListType.Equipment:
                lb_Title.text = LM.DATA[42];
                for (int i = 0; i < li_Equipment.Count; i++)
                {
                    if (li_Equipment[i].li_Material_ID == null || Character.Get.CampData().creftLevel < li_Equipment[i].creftLevel)
                        continue;
                    li_Content[index].Show(li_Equipment[i]);
                    index++;
                }
                break;
            default:
                break;
        }

        gr_Grid.Reposition();
        sc_View.ResetPosition();
    }

    public void OnClick_Next()
    {
        if (currentType == ItemListType.MAX -1)
            currentType = 0;
        else
            currentType++;

        Sound_Manager.Get.PlayEffect("Effect_select");
        Refresh(currentType);
    }

    public void OnClick_Prev()
    {
        if (currentType == 0)
            currentType = ItemListType.MAX - 1;
        else
            currentType--;

        Sound_Manager.Get.PlayEffect("Effect_select");
        Refresh(currentType);
    }
    
}
