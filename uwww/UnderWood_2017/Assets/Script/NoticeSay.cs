﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public class NoticeSay : MonoBehaviour
{
    public float endTime = 2.0f;
    public UILabel lb_Say;

    public void Show(string _Say , bool _isAutoDisAppear = true , float _endTime = 2.0f)
    {
        if (this.gameObject.activeSelf == true)
            StopAllCoroutines();

        endTime = _endTime;
        lb_Say.text = _Say;
        this.gameObject.SetActive(true);

        if(_isAutoDisAppear == true)
            StartCoroutine(CheckTime());

        lb_Say.color = Color.white;
    }

    public void ChangeColor(Color _color)
    {
        lb_Say.color = _color;
    }

    IEnumerator CheckTime()
    {
        yield return new WaitForSeconds(endTime);
        Hide();
    }
    
    public void Hide()
    {
        this.gameObject.SetActive(false);
    }
}

