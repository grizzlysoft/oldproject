﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool_Quiver : MonoBehaviour
{
    public GameObject go_Arrow;
    List<Arrow> li_Arrow = new List<Arrow>();
    List<int> li_ReadyArrow = new List<int>();

    public GameObject go_EnemyQuiverPref;
    [HideInInspector]
    public GameObject go_CurrentEnemyQuiver;


    // Use this for initialization
    public void Init()
    {
        GameObject temp;
        Arrow temp_Arrow;
        for(int i=0; i< ConstValue.Max_Arrow; i++)
        {
            temp = Instantiate(go_Arrow, this.transform);
            temp.transform.localPosition = Vector3.zero;
            temp_Arrow = temp.GetComponent<Arrow>();
            temp_Arrow.Init();
            li_Arrow.Add(temp_Arrow);
        }

        li_ReadyArrow.Clear();

        go_CurrentEnemyQuiver = Instantiate(go_EnemyQuiverPref, this.transform);
    }

    int count = 0;
    public void ReadyArrow()
    {
        switch (Character.Get.currentState)
        {
            case Character.CharacterState.Skill_1:
                for (int i = 0; i < li_Arrow.Count; i++)
                {
                    if (li_Arrow[i].gameObject.activeSelf == false)
                    {
                        li_ReadyArrow.Add(i);
                        li_Arrow[i].Show(Character.Get.currentState);
                        break;
                    }
                }
                break;
            case Character.CharacterState.Attack:
            case Character.CharacterState.Skill_2:
                count = 0;
                for (int i = 0; i < li_Arrow.Count; i++)
                {
                    if (li_Arrow[i].gameObject.activeSelf == false)
                    {
                        count++;
                        li_ReadyArrow.Add(i);
                        li_Arrow[i].Show(Character.Get.currentState, count * 10);

                        if (count >= 2)
                            break;
                    }
                }
                break;
            case Character.CharacterState.Skill_3:
                count = 0;
                for (int i = 0; i < li_Arrow.Count; i++)
                {
                    if (li_Arrow[i].gameObject.activeSelf == false)
                    {
                        count++;
                        li_ReadyArrow.Add(i);
                        li_Arrow[i].Show(Character.CharacterState.Skill_3, -50 + (count * 10));

                        if (count >= 10)
                            break;
                    }
                }
                break;
            default:
                break;
        }
    }

    public void ReleaseArrow()
    {
        if(Character.Get.currentState == Character.CharacterState.Skill_2 ||
           Character.Get.currentState == Character.CharacterState.Attack)
        {
            if(li_ReadyArrow.Count != 0)
            {
                li_Arrow[li_ReadyArrow[0]].ShotArrow();
                li_ReadyArrow.RemoveAt(0);
            }
        }
        else
        {
            for (int i = 0; i < li_ReadyArrow.Count; i++)
            {
                li_Arrow[li_ReadyArrow[i]].ShotArrow();
            }

            li_ReadyArrow.Clear();
        }
       
    }

    public void AllHide()
    {
        for (int i = 0; i < li_Arrow.Count; i++)
        {
            li_Arrow[i].Hide();
        }

        Destroy(go_CurrentEnemyQuiver);
        go_CurrentEnemyQuiver = Instantiate(go_EnemyQuiverPref, this.transform);
    }

}
