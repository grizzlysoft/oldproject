﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public class CampUpgradeUI : MonoBehaviour
{
    Character character;

    public UILabel lb_CurrentLevel;
    public UILabel lb_Gold;
    public UILabel lb_Desc;
    public UISprite sp_Icon;
    public GameObject go_YesBlock;

    List<Table_Manager.CampData> li_Table;
    int NextLevel;

    public void Show()
    {
        Refresh();
        this.gameObject.SetActive(true);
    }
    public void Hide(){this.gameObject.SetActive(false);}

    public void Init(Character _characterData)
    {
        character = _characterData;
        li_Table = Table_Manager.Get.li_CampData;
    }

    public void Refresh()
    {
        //주둔지 최고레벨 이면~
        if (li_Table.Count <= character.CampLevel + 1)
        {
            lb_Desc.text = LM.DATA[35];
            lb_Gold.text = "0";
            lb_CurrentLevel.text = string.Format("{0} {1}", LM.DATA[74], character.CampLevel);
            sp_Icon.spriteName = li_Table[character.CampLevel].iconName;
            go_YesBlock.SetActive(true);
            return;
        }

        //레벨업 효과 셋팅
        NextLevel = character.CampLevel + 1;

        //현재 주둔지 레벨 셋팅(+1로 보여준다)
        lb_CurrentLevel.text = string.Format("{0} {1}", LM.DATA[74], NextLevel);
        lb_Desc.text = string.Format("{0}\n{1}", LM.DATA[57], li_Table[NextLevel].name);
        sp_Icon.spriteName = li_Table[NextLevel].iconName;

        //필요골드 셋팅
        lb_Gold.text = li_Table[NextLevel].needGold.ToString();

        //버튼 블럭 셋팅
        go_YesBlock.SetActive(!isHaveAllMeterial());
    }

    bool isHaveAllMeterial()
    {
        //최고 레벨 체크
        if (li_Table.Count <= character.CampLevel + 1)
            return false;

        //골드 체크
        if (character.CheckHaveGold(li_Table[NextLevel].needGold) == false)
        {
            return false;
        }

        return true;
    }   

    public void OnClick_Yes()
    {
        //재료 체크 
        if(isHaveAllMeterial() == false)
            return;

        Sound_Manager.Get.PlayEffect("Effect_levelup");
        character.CampLevelUp();
    }
    
    public void OnClick_No()
    {
        Hide();
    }

 
}
