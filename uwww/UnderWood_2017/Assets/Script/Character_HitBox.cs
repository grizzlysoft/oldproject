﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character_HitBox : MonoBehaviour
{
    Character parent;
    BoxCollider2D box;

    public void Init(Character _parent)
    {
        parent = _parent;
        box = this.GetComponent<BoxCollider2D>();
    }

    public void GetDamaged(DamageData _Dmg , Transform _source)
    {
        parent.GetDamaged(_Dmg , _source);
    }

    public void SetCollider(bool isOn)
    {
        box.enabled = isOn;
    }
}
