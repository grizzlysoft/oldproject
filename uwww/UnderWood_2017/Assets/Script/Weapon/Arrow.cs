﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    public Transform hitEffectPos;

    Character.CharacterState state;
    public Rigidbody2D rb;
    Vector3 prevPosition;
    bool isShot = false;
    int angleValue;
    Transform leftHand;
    public TrailRenderer trender;

    List<GameObject> li_Enemy = new List<GameObject>();

    public void Init()
    {
        leftHand = Character.Get.li_Weapon[(int)WeaponType.Bow].sp_LeftWeapon.gameObject.transform;
        Hide();
    }

    //화살 준비 - 보이기만 한다 
    public void Show(Character.CharacterState _state ,int _angleValue = 0)
    {
        state = _state;
        angleValue = _angleValue;
        FollowLeftHand();
        isShot = false;
        rb.simulated = false;
        this.gameObject.SetActive(true);
    }

    public void Hide()
    {
        li_Enemy.Clear();
        isShot = false;
        rb.simulated = false;
        trender.enabled = false;
        //this.transform.localPosition = Vector3.zero;
        this.gameObject.SetActive(false);
    }


    
    void FixedUpdate()
    {
        //보이기만 하는 상태일때는 손을 따라간다.
        if (isShot == false)
        {
            FollowLeftHand();
        }
        else
        {
            Vector3 deltaPos = transform.position - prevPosition;                     // 현재위치 - 이전위치 = 방향
            float angle = Mathf.Atan2(deltaPos.y, deltaPos.x) * Mathf.Rad2Deg;// 삼각함수로 각도를 구함.

            if (0 != angle)    // 물리연산과 렌더링연산의 차이를 위해서 체크
            {
                transform.rotation = Quaternion.Euler(0, 0, angle);
                prevPosition = transform.position;
            }
        }

        if (this.transform.localPosition.y < -350 ||
            this.transform.localPosition.x > 1500 ||
            this.transform.localPosition.x < -1500)
            Hide();
    }

    void FollowLeftHand()
    {
        this.transform.position = leftHand.position;
        this.transform.rotation = leftHand.rotation;

        if (angleValue != 0)
            this.transform.Rotate(0, 0, angleValue);
    }

    float dir;
    float powerX;
    float powerY;
    //화살 발사 - 스킬 별로 화살의 방향과 중력 여부 등을 설정
    public void ShotArrow()
    {
        trender.enabled = true;

        if (Character.Get.RotationPivot.rotation.eulerAngles.y == 180)
        {
            dir = 1;
        }
        else
        {
            dir = -1;
        }

        switch (state)
        {
            case Character.CharacterState.Attack:
                rb.gravityScale = 1;
                powerY = Character.Get.chargeEndTime - Character.Get.chargeStartTime;
                powerX = 170;
                if (powerY < 0.2f)
                {
                    powerX = 130;
                    powerY = 0.2f;
                }
                else if (powerY > 1)
                {
                    powerY = 1;
                }
                
                //Debug.Log(powerX);
                rb.AddForce(new Vector2(dir * powerX, 140 * powerY), ForceMode2D.Force);
                break;
            case Character.CharacterState.Skill_1:
                this.gameObject.transform.rotation = Quaternion.identity;
                rb.gravityScale = 0;
                rb.AddForce(new Vector2(dir * 200, 0), ForceMode2D.Force);
                break;
            case Character.CharacterState.Skill_2:
                rb.gravityScale = 1;
                rb.AddForce(new Vector2(dir * 150, 0), ForceMode2D.Force);
                break;
            case Character.CharacterState.Skill_3:
                rb.gravityScale = 1;
                rb.AddForce(new Vector2(dir * (10 - angleValue), 200), ForceMode2D.Force);
                break;
            default:
                break;
        }

        rb.simulated = true;
        isShot = true;
        prevPosition = transform.position;
    }

    Enemy_AnimationInterface collTarget = null;
    //화살은 개별 충돌체를 가지기 때문에 여기의 리스트로 판단
    public void OnTriggerEnter2D(Collider2D coll)
    {
        //기존 피격 리스트에 없는 적이면 - 한번의 애니메이션에서 중복 딜링을 막기위해 -> 나중에 스킬같은건 따로 구현
        if (li_Enemy.Exists(r => r == coll.gameObject) == false)
        {
            if (coll.tag == "Enemy")
            {
                if (CheckAttackEnemy(coll.gameObject) == true)
                {
                    collTarget = coll.gameObject.GetComponent<Enemy_AnimationInterface>();
                    collTarget.GetDamaged(
                        Character.Get.li_Weapon[(int)WeaponType.Bow].FinalDamage(collTarget.GetEnemyData().data ,state), hitEffectPos);

                    //1번 스킬은 관통샷이라서 화살이 사라지면 안됨. 나머지는 전부 사라져~
                    if (state != Character.CharacterState.Skill_1 &&
                        state != Character.CharacterState.Skill_3)
                        Hide();
                }
            }
            else if (coll.tag == "OreWood")
            {
                if (CheckAttackEnemy(coll.gameObject) == true)
                {
                    coll.gameObject.GetComponent<OreWoodContent>().GetDamaged(hitEffectPos);

                    if (state != Character.CharacterState.Skill_1 &&
                        state != Character.CharacterState.Skill_3)
                        Hide();
                }
            }
            else if(coll.tag == "Ground")
            {
                Hide();
            }
        }
    }

    public bool CheckAttackEnemy(GameObject _go)
    {
        //기존 피격 리스트에 없는 적이면 - 한번의 애니메이션에서 중복 딜링을 막기위해
        if (li_Enemy.Exists(r => r == _go) == false)
        {
            li_Enemy.Add(_go.gameObject);
            return true;
        }

        return false;
    }


}
