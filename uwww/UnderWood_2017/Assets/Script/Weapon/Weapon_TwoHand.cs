﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon_TwoHand : Weapon
{
    override public DamageData FinalDamage(Table_Manager.EnemyData _TargetData)
    {
        DamageData dmgData = new DamageData();
        float tempDmg = CalcDamage();

        Penetration = 0;
        SkillMultiple = 1;

        //무기별 스킬 배율 적용
        switch (Character.Get.currentState)
        {
            case Character.CharacterState.Attack:
                SkillMultiple = 1.5f;
                Character.Get.Heal_STA(10, false);
                break;
            case Character.CharacterState.Skill_1:
                float multiple = Character.Get.chargeEndTime - Character.Get.chargeStartTime;
                //Debug.Log("multiple " + multiple);
                //차지 단계별 데미지
                if (multiple < 1f)
                {
                    SkillMultiple = 3;
                }
                else if (multiple < 2.0f) //1차지
                {
                    Penetration = _TargetData.DEF / 3;
                    SkillMultiple = 6;
                }
                else if (multiple < 3.0f) //2차지
                {
                    Penetration = _TargetData.DEF / 2;
                    SkillMultiple = 9;
                }
                else //3차지
                {
                    Penetration = _TargetData.DEF / 2;
                    SkillMultiple = 12;
                }

                if (multiple > 1f)
                {
                    float Heal = SkillMultiple + (Character.Get.Level / 5);
                    Character.Get.Heal_Pecent((int)Heal);
                }

                break;
            case Character.CharacterState.Skill_2:
                SkillMultiple = 2.5f;
                if (_TargetData.DEF > Character.Get.Level)
                    Penetration = Character.Get.Level;
                break;
            case Character.CharacterState.Skill_3:
                SkillMultiple = 2f;
                Penetration = _TargetData.DEF / 2;
                break;
            default:
                break;
        }

        //기본 데미지 - 대상 방어력
        tempDmg = (tempDmg * 10) / (10 + (_TargetData.DEF - Penetration));
        tempDmg = tempDmg * SkillMultiple;

        //크리티컬 계산
        bool isCrit = Character.Get.li_Weapon[(int)WeaponType.TwoHand].CalcCritical();
        if (isCrit == true)
            tempDmg *= 2.5f;

        dmgData.Dmg = (int)tempDmg;
        dmgData.isCritcal = isCrit;

        return dmgData;
    }
}
