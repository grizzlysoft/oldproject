﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    //왼손장비가 있을때만 ~
    public SpriteRenderer sp_LeftWeapon;
    public Transform hitEffectPos;

    ///트레일
    public XftWeapon.XWeaponTrail Trail;

    //무기정보
    public Table_Manager.WeaponData data;
    [HideInInspector]
    public SpriteRenderer sp_RightWeapon;
    protected BoxCollider2D boxColl;

    protected float SkillMultiple = 1;
    protected int Penetration = 0;

    public void Init()
    {
        boxColl = GetComponent<BoxCollider2D>();
        sp_RightWeapon = GetComponent<SpriteRenderer>();

        if (Trail != null)
        {
            Trail.Init();
        }

        Hide();
    }

    //////////////////////////////////////////////
    //virtual 함수
    //////////////////////////////////////////////
    public void Show()
    {
        if(sp_LeftWeapon != null)
            sp_LeftWeapon.gameObject.SetActive(true);
        sp_RightWeapon.gameObject.SetActive(true);
        boxColl.enabled = false;
    }
    public void Hide()
    {
        if (sp_LeftWeapon != null)
            sp_LeftWeapon.gameObject.SetActive(false);
        sp_RightWeapon.gameObject.SetActive(false);

        SetTrailRender(false);
    }
    
    public void ChangeWeapon(Table_Manager.WeaponData _data)
    {
        data = _data;

        sp_RightWeapon.sprite = Sprite_Manager.Get.GetSprite_Weapon(data.rightSpriteName);
        if (sp_LeftWeapon != null)
            sp_LeftWeapon.sprite = Sprite_Manager.Get.GetSprite_Weapon(data.leftSpriteName);

    }    

    //자식단에서 구현
    abstract public DamageData FinalDamage(Table_Manager.EnemyData _TagetData);
    virtual public DamageData FinalDamage(Table_Manager.EnemyData _TargetData, Character.CharacterState _state){return null;}

    virtual public void SetCollider(bool _isActive)
    {
        if(_isActive == true)
        {
            EndAttack();
            boxColl.enabled = true;
        }
        else
        {
            boxColl.enabled = false;
        }
    }

    public void SetTrailRender(bool _isEnable)
    {
        if (Trail == null)
            return;

        if (_isEnable == true)
        {
            Trail.MyColor = data.TrailColor;
            Trail.Activate();

        }
        else
            Trail.Deactivate();
    }

    //////////////////////////////////////////////
    /// 기본 데미지 계산
    //////////////////////////////////////////////

    public int CalcDamage()
    {
        int min = data.ATK_Min + Character.Get.ATK;
        int max = data.ATK_Max + Character.Get.ATK;
        int tempDmg = Random.Range(min, max + 1);

        return tempDmg;
    }

    public bool CalcCritical()
    {
        //크리티컬 계산 - 기본확률 캐릭터에 넣기, 무기에 따른 확률 추가?  데미지는 기본 2배
        int crit = Random.Range(0, 100);
        if (crit < ConstValue.Crit_Incidence + data.Crit)
        {
            //Debug.Log("크리티컬!!");
            return true;
        }

        return false;
    }

    //////////////////////////////////////////////
    //데미지 중복 처리 함수
    //////////////////////////////////////////////
    List<GameObject> li_Enemy = new List<GameObject>();

    protected void EndAttack()
    {
        li_Enemy.Clear();
    }

    Enemy_AnimationInterface collTarget = null;
    //공격 액션이 아닐때는 박스 콜리더 자체가 꺼져있어서 어차피 안들어옴
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Enemy")
        {
            //기존 피격 리스트에 없는 적이면 - 한번의 애니메이션에서 중복 딜링을 막기위해
            if (CheckAttackEnemy(coll.gameObject) == true)
            {
                collTarget = coll.gameObject.GetComponent<Enemy_AnimationInterface>();
                collTarget.GetDamaged(FinalDamage(collTarget.GetEnemyData().data), hitEffectPos);
            }
        }
        else if(coll.tag == "OreWood")
        {
            if (CheckAttackEnemy(coll.gameObject) == true)
            {
                coll.gameObject.GetComponent<OreWoodContent>().GetDamaged(hitEffectPos);
            }
        }
    }

    public bool CheckAttackEnemy(GameObject _go)
    {
        //기존 피격 리스트에 없는 적이면 - 한번의 애니메이션에서 중복 딜링을 막기위해
        if (li_Enemy.Exists(r => r == _go) == false)
        {
            li_Enemy.Add(_go.gameObject);
            return true;
        }

        return false;
    }

}
