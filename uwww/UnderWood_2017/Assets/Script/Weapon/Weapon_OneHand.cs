﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon_OneHand : Weapon
{
    override public DamageData FinalDamage(Table_Manager.EnemyData _TargetData)
    {
        DamageData dmgData = new DamageData();
        float tempDmg = CalcDamage();

        Penetration = 0;
        SkillMultiple = 1;

        //무기별 스킬 배율 적용
        switch (Character.Get.currentState)
        {
            case Character.CharacterState.Attack:
                Character.Get.Heal_STA(7, false);
                break;
            case Character.CharacterState.Skill_1:
                SkillMultiple = 2f;
                if (_TargetData.DEF > Character.Get.Level)
                    Penetration = Character.Get.Level;
                break;
            case Character.CharacterState.Skill_2:
                float multiple = Character.Get.chargeEndTime - Character.Get.chargeStartTime;

                //차지 단계별 데미지
                if (multiple < 1f)
                    SkillMultiple = 4;
                else if (multiple < 2.0f) //1차지
                    SkillMultiple = 6;
                else
                    SkillMultiple = 10;

                Penetration = _TargetData.DEF / 2;
                //Debug.Log(string.Format("막은 시간:{0} 공격력 배수:{1} 최종공격력:{2}", multiple, DmgValue, tempDmg));  
                break;
            case Character.CharacterState.Skill_3:
                SkillMultiple = 3.5f;
                if(_TargetData.DEF > Character.Get.Level)
                    Penetration = Character.Get.Level;

                break;
            default:
                break;
        }

        //기본 데미지 - 대상 방어력
        tempDmg = (tempDmg * 10) / (10 + (_TargetData.DEF - Penetration));
        tempDmg = tempDmg * SkillMultiple;

        //크리티컬 계산
        bool isCrit = Character.Get.li_Weapon[(int)WeaponType.OneHand].CalcCritical();
        if (isCrit == true)
            tempDmg *= 2;

        dmgData.Dmg = (int)tempDmg;
        dmgData.isCritcal = isCrit;

        return dmgData;
    }

   
}
