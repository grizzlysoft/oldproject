﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BashQuakeEffect : MonoBehaviour
{
    Enemy_AnimationInterface collTarget = null;
    DamageData temp = new DamageData();


    private void Start()
    {
        Sound_Manager.Get.PlayEffect("Effect_earthquake");
        Map_Manager.Get.CameraShake();
        StartCoroutine(Timer());
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(1.0f);
        this.gameObject.SetActive(false);
        Destroy(this.gameObject);
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Enemy")
        {
            if (Character.Get.li_Weapon[(int)WeaponType.TwoHand].CheckAttackEnemy(coll.gameObject) == true)
            {
                float multiple = Character.Get.chargeEndTime - Character.Get.chargeStartTime;
                if (multiple < 2.0f)
                    return;

                collTarget = coll.gameObject.GetComponent<Enemy_AnimationInterface>();

                temp.Dmg = Character.Get.li_Weapon[(int)WeaponType.TwoHand].CalcDamage();
                temp.isCritcal = false;

                temp.Dmg = (temp.Dmg * 10) / (10 + (collTarget.GetEnemyData().data.DEF));
                temp.Dmg *= (int)multiple;

                bool isCrit = Character.Get.li_Weapon[(int)WeaponType.TwoHand].CalcCritical();
                if (isCrit == true)
                    temp.Dmg *= 2;

                collTarget.GetDamaged(temp, coll.transform);
            }
        }
        else if (coll.tag == "OreWood")
        {
            if (Character.Get.li_Weapon[(int)WeaponType.TwoHand].CheckAttackEnemy(coll.gameObject) == true)
            {
                float multiple = Character.Get.chargeEndTime - Character.Get.chargeStartTime;
                if (multiple < 2.0f)
                    return;

                coll.gameObject.GetComponent<OreWoodContent>().GetDamaged(coll.transform);
            }
        }
    }



}
