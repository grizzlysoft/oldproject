﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon_Bow : Weapon
{
    //공격 시작 - 화살 들기 시작
    override public void SetCollider(bool _isActive)
    {
        if (_isActive == true)
        {
            ObjectPool_Manager.Get.quiver.ReadyArrow();
        }
        else
        {
            ObjectPool_Manager.Get.quiver.ReleaseArrow();
        }
    }

    public override DamageData FinalDamage(Table_Manager.EnemyData _TagetData)
    {
        return null;
    }

    public override DamageData FinalDamage(Table_Manager.EnemyData _TargetData, Character.CharacterState _state)
    {
        DamageData dmgData = new DamageData();
        float tempDmg = CalcDamage();

        Penetration = 0;
        SkillMultiple = 1;

        //무기 스킬 배율
        switch (_state)
        {
            case Character.CharacterState.Attack:
                Character.Get.Heal_STA(4, false);
                break;
            case Character.CharacterState.Skill_1:
                float multiple = Character.Get.chargeEndTime - Character.Get.chargeStartTime;

                //최소,최대값 보정
                if (multiple < 1)
                    SkillMultiple *= 2.5f;
                else if (multiple < 2)
                    SkillMultiple *= 5;
                else if (multiple < 3)
                    SkillMultiple *= 7;
                else
                    SkillMultiple *= 9;

                Penetration = _TargetData.DEF / 2;
                break;
            case Character.CharacterState.Skill_2:
                tempDmg *= 3.5f;
                break;
            case Character.CharacterState.Skill_3:
                tempDmg *= 2.5f;
                break;
            default:
                break;
        }

        //(기본데미지 * 10) / (10 + (상대 방어력 - 방어 관통력) *스킬 배수 * 크리티컬 보정 
        tempDmg = (tempDmg * 10) / (10 + (_TargetData.DEF - Penetration));
        tempDmg = tempDmg * SkillMultiple;

        //크리티컬 계산
        bool isCrit = Character.Get.li_Weapon[(int)WeaponType.Bow].CalcCritical();
        if (isCrit == true)
            tempDmg *= 2;

        dmgData.Dmg = (int)tempDmg;
        dmgData.isCritcal = isCrit;

        return dmgData;
    }


}
