﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageText : MonoBehaviour
{

    public TweenPosition Tp;
    public UILabel lb_Text;

    public void Init()
    {
        Tp.eventReceiver = this.gameObject;
        Tp.callWhenFinished = "Hide";
        Hide();
    }

    public void Hide()
    {
        Tp.ResetToBeginning();
        this.gameObject.SetActive(false);
    }

    public void Show(DamageData _data, Vector3 _pos)
    {
        this.transform.position = _pos;
        this.transform.localScale = new Vector3(1, 1, 1);

        if (_data.Dmg == 0)
        {
            lb_Text.text = "[fff45c]" + "Miss";
        }
        else if (_data.Dmg == -1)
        {
            lb_Text.text = "[8fc31f]" + "Defense";
        }
        else
        {
            switch (_data.Type)
            {
                case DamagedType.Enemy:
                    {
                        if (_data.isCritcal == true)
                        {
                            this.transform.localScale = new Vector3(1.5f, 1.5f, 1);
                            lb_Text.text = "[FFAE00]" + _data.Dmg.ToString();
                        }
                        else
                        {
                            lb_Text.text = "[FFFFFF]" + _data.Dmg.ToString();
                        }
                    }
                    break;
                case DamagedType.Player:
                        lb_Text.text = "[EE1010]" + _data.Dmg.ToString();
                    break;
                case DamagedType.Ore:
                        lb_Text.text = "[CACACA]" + _data.Dmg.ToString();
                    break;
                case DamagedType.Wood:
                        lb_Text.text = "[CEB07B]" + _data.Dmg.ToString();
                    break;
                case DamagedType.Terrain:
                    lb_Text.text = "[EE1010]" + _data.Dmg.ToString();
                    break;
                case DamagedType.DefenceObject:
                    lb_Text.text = "[A14163]" + _data.Dmg.ToString();
                    break;
                default:
                    break;
            }
        }

        this.gameObject.SetActive(true);
        Tp.PlayForward();
    }

}
