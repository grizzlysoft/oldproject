﻿using UnityEngine;
using System.Collections;

public enum ButtonDirection
{
    LEFT = 0,
    RIGHT,
    TOP,
    BOTTOM,
    NONE
}
//보기 좋게 분류하고 캐릭터 이동기능 추가할것.
public class JoyStick : MonoBehaviour
{
    public SphereCollider OuterCircleColl;
    public Transform InnerCircle;

    public float MaxRadius = 60f;

    Vector2 startpos = Vector3.zero;
    Vector2 InnerLocalPos;

    //이동방향
    public Vector2 Direction;
    public float Distance;
    private Camera uiCamera;
     
    void Awake()
    {
        uiCamera = NGUITools.FindCameraForLayer(this.gameObject.layer);
    }
    
    private RaycastHit hit;
    void Update()
    {
        if (Popup_Manager.Get.isEmptyList() == false)
            return;

        InnerCircle.localPosition = startpos;
        Direction = Vector3.zero;
        Distance = 0;

        //현제 터치한 손가락을 찾는다 
        for (int i = 0; i < Input.touchCount; i++)
        {
            Ray ray = uiCamera.ScreenPointToRay(Input.GetTouch(i).position);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider == OuterCircleColl)
                {
                    InnerCircle.position = uiCamera.ScreenToWorldPoint(Input.GetTouch(i).position);
                    InnerLocalPos = InnerCircle.localPosition;

                    Direction = (new Vector2(InnerLocalPos.x, InnerLocalPos.y) - startpos).normalized;
                    Distance = Vector3.Distance(InnerLocalPos, startpos);

                    if (Distance >= MaxRadius)
                        InnerCircle.localPosition = startpos + (Direction * MaxRadius);
                    else
                        InnerCircle.localPosition = startpos + (Direction * Distance);

                    Distance = Vector3.Distance(InnerCircle.localPosition, startpos);
                }
            }
        }
    }

    public ButtonDirection MoveJoystick()
    {
        if(Distance > MaxRadius/2)
        {
            if (Direction.x < 0)
                return ButtonDirection.LEFT;
            else
                return ButtonDirection.RIGHT;
        }

        return ButtonDirection.NONE;
    }
}
