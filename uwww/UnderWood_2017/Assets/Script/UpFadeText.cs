﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpFadeText : MonoBehaviour
{
    public UILabel lb_text;
    
    public void Init(int _price)
    {
        if (_price < 0)
        {
            lb_text.text = _price.ToString();
            lb_text.color = ColorPreset.HexToColor("FF2020FF");

        }
        else
        {
            lb_text.text = "+" + _price.ToString();
            lb_text.color = ColorPreset.HexToColor("28FF20FF");
        }


    }

    public void DestroyNow()
    {
        Destroy(this.gameObject);
    }
}
