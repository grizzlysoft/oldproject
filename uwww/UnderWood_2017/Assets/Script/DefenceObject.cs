﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenceObject : MonoBehaviour
{
    [SerializeField]
    SpriteRenderer sp_Img;
    [SerializeField]
    BoxCollider2D box;
    [SerializeField]
    GameObject pos;
    public int HP;
    int MaxHP;

    public UILabel lb_HP;

    public void Show(int _HP)
    {
        MaxHP = _HP;
        HP = MaxHP;
        box.enabled = true;
        sp_Img.sprite = Sprite_Manager.Get.GetSprite_MapObject("totem");
        lb_HP.text = string.Format("{0}\n{1}", LM.DATA[62], HP);
        lb_HP.gameObject.SetActive(true);
        this.gameObject.SetActive(true);
    }

    private void Update()
    {
        lb_HP.transform.position = pos.transform.position;
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
        lb_HP.gameObject.SetActive(false);
    }

    public void GetDamaged()
    {
        if (Character.Get.isInvincibility == true)
            return;

        DamageData _Dmg = new DamageData
        {
            Type = DamagedType.DefenceObject,
            Dmg = 1,
            isCritcal = false
        };

        ObjectPool_Manager.Get.ShowDamage(this.transform, _Dmg);

        HP -= _Dmg.Dmg;

        if (HP <= 0)
        {
            HP = 0;
            box.enabled = false;
            sp_Img.sprite = Sprite_Manager.Get.GetSprite_MapObject("totem_broken");
        }

        lb_HP.text = string.Format("{0}\n{1}", LM.DATA[62], HP);
    }

    public void Heal(int _heal)
    {
        if(HP < MaxHP)
        {
            HP += _heal;
            if (HP > MaxHP)
                HP = MaxHP;

            lb_HP.text = string.Format("{0}\n{1}", LM.DATA[62], HP);
        }
    }

}
