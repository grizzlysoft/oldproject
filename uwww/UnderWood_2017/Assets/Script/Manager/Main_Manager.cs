﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main_Manager : MonoBehaviour
{
    string url = "https://play.google.com/store/apps/details?id=com.GrizzlySoft.UnderWood";
    public string _bundleIdentifier { get { return url.Substring(url.IndexOf("details"), url.LastIndexOf("details") + 1); } }
    [HideInInspector]
    public bool isSamePlayStoreVersion = false;

    public int itemID;
    public int itemCount;
    public int mapID;

    public void Death()
    {
        Character.Get.Death();
    }

    public void CreateItem()
    {
        Character.Get.AddItem(itemID,itemCount);
    }

    public void Teleport()
    {
        Map_Manager.Get.ChangeMap_Pos(mapID);
    }

    void Start()
    {
        Application.targetFrameRate = 60;

        ColorPreset.SetColor();
        Table_Manager.Get.LoadTable();
        Popup_Manager.Get.Ingame.Init();

        StartCoroutine(Loding());
    }

    IEnumerator Loding()
    {
        //로그인 팝업 띄우기
        Popup_Manager.Get.Login.Show();
        Popup_Manager.Get.Login.ChangeLabel("Check Version...");
        yield return new WaitForSeconds(0.1f);

        //로그인 끝나면 할것들 <순서 준수
        ObjectPool_Manager.Get.Init();
        Popup_Manager.Get.Login.ChangeLabel("Object Loading...");
        yield return new WaitForSeconds(0.01f);

        Character.Get.Init();
        Popup_Manager.Get.Login.ChangeLabel("Character Loading...");
        yield return new WaitForSeconds(0.01f);

        Sound_Manager.Get.Init();
        Popup_Manager.Get.Login.ChangeLabel("Sound Loading...");
        yield return new WaitForSeconds(0.01f);

        Map_Manager.Get.Init();
        Popup_Manager.Get.Login.ChangeLabel("Map Loading...");
        yield return new WaitForSeconds(0.02f);

        Purchasing.Get.InitPurchasing();
        Popup_Manager.Get.Login.ChangeLabel("InApp Loading...");
        yield return new WaitForSeconds(0.01f);

        Popup_Manager.Get.Login.CanHide();
        yield return null;

    }

    void GoPlayStore()
    {
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.GrizzlySoft.UnderWood");
    }

}
