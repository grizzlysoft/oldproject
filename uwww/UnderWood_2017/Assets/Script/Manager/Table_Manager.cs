﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Table_Manager : Singleton<Table_Manager>
{
    public void LoadTable()
    {
        TableLoad("Equipment", LoadTable_Equipment);
        TableLoad("Equipment_Weapon", LoadTable_Weapon);
        TableLoad("Character", LoadTable_CharacterData);
        TableLoad("Enemy", LoadTable_EnemyData);
        TableLoad("Map", LoadTable_MapData); 
        TableLoad("Skill", LoadTable_SkillData);
        TableLoad("Customizing", LoadTable_CustomizingData);
        TableLoad("ETC", LoadTable_ETC);
        TableLoad("Localizing", LoadTable_Localizing);
        TableLoad("Alchemy", LoadTable_Alchemy);
        TableLoad("Camp", LoadTable_CampData);
        TableLoad("OreWood", LoadTable_OreWood);
        TableLoad("UseItem", LoadTable_UseItem);
        TableLoad("Shop", LoadTable_ShopData);
        TableLoad("Quest", LoadTable_QuestData);
        TableLoad("DieDesc", LoadTable_DieDesc);
        TableLoad("Upgrade", LoadTable_EquipUpgradeData);
        TableLoad("AD_Reward",LoadTable_AD_Reward);
        
        Get_MAX_ITEM_CONTENT();
    }

    ////////////테이블 파싱
    delegate void Parse(string[] st);
    void TableLoad(string Name, Parse del)
    {
        TextAsset txObj = Resources.Load(string.Format("Table/{0}", Name), typeof(TextAsset)) as TextAsset;
        string[] strArray = txObj.text.Split('\n');
        for (int i = 0; i < strArray.Length; i++)
        {
            if (strArray[i] == "") break;

            strArray[i] = strArray[i].Replace("\\n", "\n");
            string[] stral = strArray[i].Split('|');
            //Debug.Log(Name + " Parse : " + i);
            del(stral);
        }
    }

    //제작 할수 있는 아이템목록의 최대치 구하기
    public int MAX_ITEM_CONTENT = 0;
    public void Get_MAX_ITEM_CONTENT()
    {
        int Equip_Max = 0;
        for(int i=0; i<li_Equipment.Count; i++)
        {
            if(li_Equipment[i].li_Material_ID != null)
            {
                Equip_Max++;
            }
        }

        MAX_ITEM_CONTENT = Equip_Max;

        int Weapon_Max = 0;
        for (int i = 0; i < li_Weapon.Count; i++)
        {
            if (li_Weapon[i].li_Material_ID != null)
            {
                Weapon_Max++;
            }
        }

        if(Weapon_Max > MAX_ITEM_CONTENT)
            MAX_ITEM_CONTENT = Weapon_Max;

        int ETC_Max = 0;
        for (int i = 0; i < li_ETC.Count; i++)
        {
            if (li_ETC[i].li_Material_ID != null)
            {
                ETC_Max++;
            }
        }

        if (ETC_Max > MAX_ITEM_CONTENT)
            MAX_ITEM_CONTENT = ETC_Max;

    }

    public class Item
    {
        public readonly int id;
        public readonly int creftLevel;
        public readonly ItemType itemType;
        public readonly GradeType grade;
        public readonly string name;
        public readonly string iconName;
        public readonly int price;

        public readonly List<int> li_Material_ID;
        public readonly List<int> li_Material_Count;

        public readonly string desc;


        public Item(int _id, int _creftLevel, ItemType _itemType , GradeType _grade,
            string _name, string _iconName , int _price , List<int> _li_Material_ID , List<int> _li_Material_Count, string _desc)
        {
            id = _id;
            creftLevel = _creftLevel;
            itemType = _itemType;
            grade = _grade;
            name = _name;
            iconName = _iconName;
            price = _price;

            li_Material_ID = _li_Material_ID;
            li_Material_Count = _li_Material_Count;

            desc = _desc;
        }
    }

    //잡템
    #region ETC
    public List<Item> li_ETC = new List<Item>();
    void LoadTable_ETC(string[] stral)
    {
        int cnt = 0;

        int _id = int.Parse(stral[cnt]); ++cnt;
        int _creftLevel = int.Parse(stral[cnt]); ++cnt;
        ItemType _itemType = ItemType.ETC;
        GradeType _grade = (GradeType)int.Parse(stral[cnt]); ++cnt;
        string _name = stral[cnt]; ++cnt;
        string _iconName = string.Format("Icon_{0}", _id);
        int _price = int.Parse(stral[cnt]); ++cnt;


        List<int> _li_Material_ID = null;
        List<int> _li_Material_Count = null;

        string[] ar = stral[cnt].Split(','); ++cnt;
        if (ar[0] != "")
        {
            _li_Material_ID = new List<int>();
            for (int i = 0; i < ar.Length; i++)
            {
                _li_Material_ID.Add(int.Parse(ar[i]));
            }

            ar = stral[cnt].Split(','); ++cnt;
            _li_Material_Count = new List<int>();
            for (int i = 0; i < ar.Length; i++)
            {
                _li_Material_Count.Add(int.Parse(ar[i]));
            }
        }
        else
        {
            ++cnt;
        }

        string _desc = stral[cnt]; ++cnt;

        Item item = new Item(_id,_creftLevel, _itemType, _grade, _name, _iconName, _price, _li_Material_ID, _li_Material_Count, _desc);
        li_ETC.Add(item);
    }
    #endregion

    //소모아이템
    #region UseItem
    public class UseItem : Item
    {
        public readonly string value;

        public UseItem(int _id,int _creftLevel, ItemType _itemType, GradeType _grade, string _name, string _iconName, int _price, List<int> _li_Material_ID, List<int> _li_Material_Count,
            string _value, string _desc) 
            : base(_id, _creftLevel, _itemType, _grade, _name, _iconName, _price, _li_Material_ID, _li_Material_Count, _desc)
        {
            value = _value;
        }
    }
    public List<UseItem> li_UseItem = new List<UseItem>();
    void LoadTable_UseItem(string[] stral)
    {
        int cnt = 0;

        int _id = int.Parse(stral[cnt]); ++cnt;
        int _creftLevel = 0;
        ItemType _itemType = (ItemType)int.Parse(stral[cnt]); ++cnt; 
        GradeType _grade = (GradeType)int.Parse(stral[cnt]); ++cnt;
        string _name = stral[cnt]; ++cnt;
        string _iconName = string.Format("Icon_{0}", _id);
        string _value = stral[cnt]; ++cnt;
        int _price = int.Parse(stral[cnt]); ++cnt;
        string _desc = stral[cnt]; ++cnt;

        List<int> _li_Material_ID = null;
        List<int> _li_Material_Count = null;

        UseItem item = new UseItem(_id, _creftLevel, _itemType, _grade, _name, _iconName, _price, _li_Material_ID, _li_Material_Count,
            _value, _desc);
        li_UseItem.Add(item);
    }
    #endregion


    //장비
    #region Equipment
    public class EquipmentData : Item
    {
        public readonly EquipType equipType;
        public readonly int DEF = 0;
        public readonly int ATK = 0;
        public readonly string spriteName;
        public readonly int upgradeValue;


        public EquipmentData(int _id, int _creftLevel, ItemType _itemType, GradeType _grade, string _name, string _iconName, 
            EquipType _equipType , int _DEF, int _ATK , string _spriteName , int _price , List<int> _li_Material_ID, List<int> _li_Material_Count , string _desc,int _upgradeValue) 
            : base( _id,_creftLevel, _itemType,_grade, _name, _iconName, _price, _li_Material_ID, _li_Material_Count,  _desc)
        {
            equipType = _equipType;
            DEF = _DEF;
            ATK = _ATK;
            spriteName = _spriteName;
            upgradeValue = _upgradeValue;
        }
    }
    public List<EquipmentData> li_Equipment = new List<EquipmentData>();
    void LoadTable_Equipment(string[] stral)
    {
        int cnt = 0;

        int       _id         = int.Parse(stral[cnt]); ++cnt;
        int     _creftLevel   = int.Parse(stral[cnt]); ++cnt;

        ItemType _itemType    = ItemType.Equipment;
        EquipType _equipType  = (EquipType)int.Parse(stral[cnt]); ++cnt;
        GradeType _grade      = (GradeType)int.Parse(stral[cnt]); ++cnt;
        int     _upgradeValue = int.Parse(stral[cnt]); ++cnt;
        string    _name       = stral[cnt]; ++cnt;
        int       _DEF        = int.Parse(stral[cnt]); ++cnt;
        int       _ATK        = int.Parse(stral[cnt]); ++cnt;
        string    _spriteName = stral[cnt]; ++cnt;
        string    _iconName   = string.Format("Icon_{0}", _id);
        int       _price      = int.Parse(stral[cnt]); ++cnt;

        List<int> _li_Material_ID = null;
        List<int> _li_Material_Count = null;

        string[] ar = stral[cnt].Split(','); ++cnt;
        if (ar[0] != "")
        {
            _li_Material_ID = new List<int>();
            for (int i = 0; i < ar.Length; i++)
            {
                _li_Material_ID.Add(int.Parse(ar[i]));
            }

            ar = stral[cnt].Split(','); ++cnt;
            _li_Material_Count = new List<int>();
            for (int i = 0; i < ar.Length; i++)
            {
                _li_Material_Count.Add(int.Parse(ar[i]));
            }
        }
        else
        {
            ++cnt;
        }

        string _desc = stral[cnt]; ++cnt;

        EquipmentData item = new EquipmentData(_id,_creftLevel, _itemType, _grade, _name, _iconName, _equipType, _DEF, _ATK, _spriteName, _price, _li_Material_ID , _li_Material_Count, _desc, _upgradeValue);
        li_Equipment.Add(item);

        for (int i = 1; i < ConstValue.MaxUpgrade + 1; i++)
        {
            item = new EquipmentData(
                _id + (1000 * i),
                0,
                _itemType,
                _grade,
                string.Format("{0} +{1}", _name, i),
                _iconName,
                _equipType,
                _DEF + (_upgradeValue * i),
                _ATK,
                _spriteName,
                _price + (Mathf.RoundToInt(_price/2) * i),
                null,
                null,
                _desc,
                _upgradeValue);
            li_Equipment.Add(item);
        }
    }
    #endregion
    //무기
    #region Weapon
    public class WeaponData : Item
    {
        public readonly WeaponType weaponType;
        public readonly int        ATK_Min; 
        public readonly int        ATK_Max;
        public readonly int        DEF;
        public readonly int        Crit;
        public readonly string     leftSpriteName;
        public readonly string     rightSpriteName;
        public readonly Color      TrailColor;
        public readonly int        upgradeValue;


        public WeaponData(int _id, int _creftLevel, ItemType _itemType, GradeType _grade, string _name, string _iconName, 
            WeaponType _weaponType, int _ATK_Min, int _ATK_Max, int _DEF , int _Crit, string _leftSpriteName, string _rightSpriteName ,
            Color _TrailColor, int _price , List<int> _li_Material_ID, List<int> _li_Material_Count , string _desc,int _upgradeValue) 
            : base(_id, _creftLevel, _itemType, _grade, _name, _iconName, _price, _li_Material_ID, _li_Material_Count, _desc)
        {
            weaponType        = _weaponType;
            ATK_Min           = _ATK_Min;
            ATK_Max           = _ATK_Max;
            DEF               = _DEF;
            Crit              = _Crit;
            leftSpriteName    = _leftSpriteName;
            rightSpriteName   = _rightSpriteName;
            TrailColor        = _TrailColor;
            upgradeValue      = _upgradeValue;
        }
    }
    public List<WeaponData> li_Weapon = new List<WeaponData>();
    public void LoadTable_Weapon(string[] stral)
    {
        int cnt = 0;

        int _id = int.Parse(stral[cnt]); ++cnt;
        int _creftLevel = int.Parse(stral[cnt]); ++cnt;
        ItemType _itemType = ItemType.Weapon;
        WeaponType _weaponType = (WeaponType)int.Parse(stral[cnt]); ++cnt;
        GradeType _grade = (GradeType)int.Parse(stral[cnt]); ++cnt;
        int _upgradeValue = int.Parse(stral[cnt]); ++cnt;
        string _name = stral[cnt]; ++cnt;
        int _ATK_Min = int.Parse(stral[cnt]); ++cnt;
        int _ATK_Max = int.Parse(stral[cnt]); ++cnt;
        int _DEF = int.Parse(stral[cnt]); ++cnt;
        int _Crit = int.Parse(stral[cnt]); ++cnt;
        string _rightSpriteName = stral[cnt]; ++cnt;
        string _leftSpriteName = stral[cnt]; ++cnt;
        string _iconName = string.Format("Icon_{0}", _id);
        Color _TrailColor = ColorPreset.HexToColor(stral[cnt]); ++cnt;
        int _price = int.Parse(stral[cnt]); ++cnt;

        List<int> _li_Material_ID = null;
        List<int> _li_Material_Count = null;

        string[] ar = stral[cnt].Split(','); ++cnt;
        if (ar[0] != "")
        {
            _li_Material_ID = new List<int>();
            for (int i = 0; i < ar.Length; i++)
            {
                _li_Material_ID.Add(int.Parse(ar[i]));
            }

            ar = stral[cnt].Split(','); ++cnt;
            _li_Material_Count = new List<int>();
            for (int i = 0; i < ar.Length; i++)
            {
                _li_Material_Count.Add(int.Parse(ar[i]));
            }
        }
        else
        {
            ++cnt;
        }

        string _desc = stral[cnt]; ++cnt;

        WeaponData item = new WeaponData(_id, _creftLevel,_itemType, _grade,_name, _iconName,_weaponType, _ATK_Min, _ATK_Max, _DEF,_Crit,
            _leftSpriteName,_rightSpriteName,_TrailColor, _price , _li_Material_ID, _li_Material_Count, _desc, _upgradeValue);
        li_Weapon.Add(item);

        //MAX는 9를 넘으면 안된다 (한자리만 빌려씀)
        for(int i=1; i< ConstValue.MaxUpgrade+1; i++)
        {
             item = new WeaponData
                (_id + (1000 * i), 
                0,
                _itemType,
                _grade,
                string.Format("{0} +{1}",_name ,i),
                _iconName,
                _weaponType,
                _ATK_Min + (_upgradeValue * i),
                _ATK_Max + (_upgradeValue * i),
                _DEF,
                _Crit + i,
                _leftSpriteName,
                _rightSpriteName,
                _TrailColor,
                _price + (Mathf.RoundToInt(_price / 2) * i),
                null,
                null,
                _desc,
                _upgradeValue);

            li_Weapon.Add(item);
        }

    }
    #endregion

    //캐릭터
    #region CharacterData
    public class CharacterData
    {
        public readonly int level ;
        public readonly int maxExp;
        public readonly int maxHp ;
        public readonly int ATK   ;
        public readonly int DEF   ;
        public readonly int maxSta;

        public CharacterData(int _level, int _maxExp, int _maxHp, int _ATK, int _DEF, int _maxSta)
        {
            level       = _level ;
            maxExp      = _maxExp;
            maxHp       = _maxHp ;
            ATK         = _ATK   ;
            DEF         = _DEF   ;
            maxSta      = _maxSta;
        }
    }
    public List<CharacterData> li_CharacterData = new List<CharacterData>();
    public void LoadTable_CharacterData(string[] stral)
    {
        int cnt = 0;

        int _level = int.Parse(stral[cnt]); ++cnt;
        int _maxExp = int.Parse(stral[cnt]); ++cnt;
        int _maxHp = int.Parse(stral[cnt]); ++cnt;
        int _ATK = int.Parse(stral[cnt]); ++cnt;
        int _DEF = int.Parse(stral[cnt]); ++cnt;
        int _maxSta = int.Parse(stral[cnt]); ++cnt;

        CharacterData item = new CharacterData(_level,_maxExp,_maxHp, _ATK, _DEF, _maxSta);
        li_CharacterData.Add(item);
    }
    #endregion

    //적군 데이터
    #region EnemyData
    public struct DropItem
    {
        public readonly int id;
        public readonly int chance;

        public DropItem(int _id, int _chance)
        {
            id = _id;
            chance = _chance;
        }
    }
    [System.Serializable]
    public class EnemyData
    {
        public readonly int     id;
        public readonly int     level;
        public readonly string  name;
        public readonly int     HP;
        public readonly int     ATK_Min;
        public readonly int     ATK_Max;
        public readonly int     DEF;
        public readonly int     EXP;
        public  float   Speed;
        public  float   AtkRange;
        public  float   SightRange;
        public readonly List<DropItem> li_DropItem;
        public readonly string PrefName;


        public EnemyData(string[] stral)
        {
            int cnt = 0;

            id          = int.Parse(stral[cnt]); ++cnt;
            level       = int.Parse(stral[cnt]); ++cnt;
            name        = stral[cnt]; ++cnt;
            ++cnt;
            HP          = int.Parse(stral[cnt]); ++cnt;
            ATK_Min     = int.Parse(stral[cnt]); ++cnt;
            ATK_Max     = int.Parse(stral[cnt]); ++cnt;
            DEF         = int.Parse(stral[cnt]); ++cnt;
            EXP         = int.Parse(stral[cnt]); ++cnt;
            Speed       = float.Parse(stral[cnt]); ++cnt;
            AtkRange    = float.Parse(stral[cnt]); ++cnt;
            SightRange  = float.Parse(stral[cnt]); ++cnt;


            string[] _ar_Id = stral[cnt].Split(','); ++cnt;
            string[] _ar_Chance = stral[cnt].Split(','); ++cnt;

            li_DropItem = new List<DropItem>();
            DropItem temp;
            int dropId;
            int dropChance;
            for (int i = 0; i < _ar_Id.Length; i++)
            {
                dropId     = int.Parse(_ar_Id[i]);
                dropChance = int.Parse(_ar_Chance[i]);

                temp = new DropItem(dropId, dropChance);
                li_DropItem.Add(temp);
            }

            PrefName = stral[cnt]; ++cnt;
        }

    }
    public List<EnemyData> li_EnemyData = new List<EnemyData>();
    public void LoadTable_EnemyData(string[] stral)
    {
        EnemyData item = new EnemyData(stral);
        li_EnemyData.Add(item);
    }
    #endregion

    //맵 데이터
    #region MapData
    public struct Location
    {
        int x;
        int y;
    }
    public class MapData
    {
        public readonly int stageID;

        public readonly float darkness;
        public readonly MapType type;
        public readonly bool   isfastTrevel;
        public readonly string bgmName;
        public readonly string BG_Near;
        public readonly string BG_Mid;
        public readonly string BG_Far;
        public readonly string Ground_name;
        public readonly string MapObejct;

        public readonly List<int> li_Monster_Left;
        public readonly List<int> li_Monster_Right;
        public readonly int loop;

        public readonly List<int> li_OreWoodID;
        public readonly int OreWoodMin;
        public readonly int OreWoodMax;

        public readonly int difficult;
        public readonly string name;


        public MapData(string[] stral)
        {
            int cnt = 0;
            stageID = int.Parse(stral[cnt]); ++cnt;

            darkness = float.Parse(stral[cnt]); ++cnt;
            type = (MapType)int.Parse(stral[cnt]); ++cnt;
            isfastTrevel = stral[cnt] == "1" ? true : false; ++cnt;
            bgmName = stral[cnt]; ++cnt;
            BG_Near = stral[cnt]; ++cnt;
            BG_Mid = stral[cnt]; ++cnt;
            BG_Far = stral[cnt]; ++cnt;
            Ground_name = stral[cnt]; ++cnt;
            MapObejct = stral[cnt]; ++cnt;
            string[] ar_tempId = stral[cnt].Split(','); ++cnt;
            li_Monster_Left = new List<int>();
            if(ar_tempId[0] != "")
            {
                for (int i = 0; i < ar_tempId.Length; i++)
                {
                    li_Monster_Left.Add(int.Parse(ar_tempId[i]));
                }
            }

            ar_tempId = stral[cnt].Split(','); ++cnt;
            li_Monster_Right = new List<int>();
            if (ar_tempId[0] != "")
            {
                for (int i = 0; i < ar_tempId.Length; i++)
                {
                    li_Monster_Right.Add(int.Parse(ar_tempId[i]));
                }
            }

            loop = int.Parse(stral[cnt]); ++cnt;

            ar_tempId = stral[cnt].Split(','); ++cnt;
            li_OreWoodID = new List<int>();
            for (int i = 0; i < ar_tempId.Length; i++)
            {
                li_OreWoodID.Add(int.Parse(ar_tempId[i]));
            }

            OreWoodMin = int.Parse(stral[cnt]); ++cnt;
            OreWoodMax = int.Parse(stral[cnt]); ++cnt;

            difficult = int.Parse(stral[cnt]); ++cnt;
            name = stral[cnt]; ++cnt;
        }
    }
    public List<MapData> li_MapData = new List<MapData>();
    public void LoadTable_MapData(string[] stral)
    {
        //MapData item = new MapData(_x_y, _isBoss , _BG_Near , _BG_Mid , _BG_Far, _Ground_name,_MapObejct , ar_tempId , ar_tempPos);
        MapData item = new MapData(stral);
        li_MapData.Add(item);
    }
    #endregion

    //스킬 데이터
    #region SkillData
    public class SkillData
    {
        public readonly string name;
        public readonly WeaponType type;
        public readonly List<int> li_Cost;
        public readonly float DodgeCost;

        public SkillData(string _name , WeaponType _type ,int _cost1 , int _cost2 , int _cost3, float _DodgeCost)
        {
            name = _name;
            type = _type;
            li_Cost = new List<int>();
            li_Cost.Add(_cost1);
            li_Cost.Add(_cost2);
            li_Cost.Add(_cost3);
            DodgeCost = _DodgeCost;
        }
    }
    public List<SkillData> li_SkillData = new List<SkillData>();
    public void LoadTable_SkillData(string[] stral)
    {
        int cnt = 0;

        string _name = stral[cnt]; ++cnt;
        WeaponType _type = (WeaponType)int.Parse(stral[cnt]); ++cnt;
        int _cost1 = int.Parse(stral[cnt]); ++cnt;
        int _cost2 = int.Parse(stral[cnt]); ++cnt;
        int _cost3 = int.Parse(stral[cnt]); ++cnt;
        float _DodgeCost = float.Parse(stral[cnt]); ++cnt;

        SkillData item = new SkillData(_name,_type,_cost1,_cost2,_cost3, _DodgeCost);
        li_SkillData.Add(item);
    }
    #endregion
    
    //커스터마이징 데이터
    #region CustomizingData
    public class CustomizingData
    {
        public readonly int id;
        public readonly CustomizingType type;
        public readonly GradeType grade;
        public readonly string name;
        public readonly Color color;
        public readonly string SpriteName;

        public CustomizingData(string[] stral)
        {
            int cnt = 0;

            id = int.Parse(stral[cnt]); ++cnt;
            type = (CustomizingType)int.Parse(stral[cnt]); ++cnt;
            grade = (GradeType)int.Parse(stral[cnt]); ++cnt;
            name = stral[cnt]; ++cnt;
            color = ColorPreset.HexToColor(stral[cnt]); ++cnt;
            SpriteName = stral[cnt]; ++cnt;
        }
    }
    public List<CustomizingData> li_CustomizingData = new List<CustomizingData>();
    public void LoadTable_CustomizingData(string[] stral)
    {
        CustomizingData item = new CustomizingData(stral);
        li_CustomizingData.Add(item);
    }
    #endregion

    //연금술 데이터
    #region AlchemyData
    public class AlchemyData
    {
        public readonly int id;
        public readonly string name;

        //리스트 3개
        public readonly List<int> li_GetItem_ID;
        public readonly List<int> li_GetItem_Chance;
        public readonly List<int> li_Material_ID;
        public readonly List<int> li_Material_Count;

        public AlchemyData(string[] stral)
        {
            int cnt = 0;

            id = int.Parse(stral[cnt]); ++cnt;
            name = stral[cnt]; ++cnt;

            string[] ar = stral[cnt].Split(','); ++cnt;
            li_GetItem_ID = new List<int>();
            for (int i = 0; i < ar.Length; i++)
            {
                li_GetItem_ID.Add(int.Parse(ar[i]));
            }

            ar = stral[cnt].Split(','); ++cnt;
            li_GetItem_Chance = new List<int>();
            for (int i = 0; i < ar.Length; i++)
            {
                li_GetItem_Chance.Add(int.Parse(ar[i]));
            }

            ar = stral[cnt].Split(','); ++cnt;
            li_Material_ID = new List<int>();
            for (int i = 0; i < ar.Length; i++)
            {
                li_Material_ID.Add(int.Parse(ar[i]));
            }

            ar = stral[cnt].Split(','); ++cnt;
            li_Material_Count = new List<int>();
            for (int i = 0; i < ar.Length; i++)
            {
                li_Material_Count.Add(int.Parse(ar[i]));
            }
        }
    }
    public List<AlchemyData> li_Alchemy = new List<AlchemyData>();
    public void LoadTable_Alchemy(string[] stral)
    {
        AlchemyData item = new AlchemyData(stral);
        li_Alchemy.Add(item);
    }
    #endregion

    //캠프(주둔지) 데이터
    #region CampData
    public class CampData
    {
        public readonly int id;
        public readonly string name;

        //리스트 3개
        public readonly List<int> li_AddCustomizing;

        public readonly int needGold;
        public readonly int storeSize;
        public readonly int invenSize;
        public readonly int creftLevel;
        public readonly int alchemyLevel;
        public readonly int oreLevel;
        public readonly int woodLevel;
        public readonly int upgradeLevel;
        public readonly int sightLevel;

        public readonly string iconName;

        public readonly string IMG_Creft;
        public readonly string IMG_House;
        public readonly string IMG_Box;
        public readonly string IMG_Alchemy;


        public CampData(string[] stral)
        {
            int cnt = 0;

            id = int.Parse(stral[cnt]); ++cnt;
            name = stral[cnt]; ++cnt;

            string[] ar = stral[cnt].Split(','); ++cnt;
            li_AddCustomizing = new List<int>();
            if (ar[0] != "")
            {
                for (int i = 0; i < ar.Length; i++)
                {
                    li_AddCustomizing.Add(int.Parse(ar[i]));
                }
            }
            
            needGold  = int.Parse(stral[cnt]); ++cnt;
            storeSize = int.Parse(stral[cnt]); ++cnt;
            invenSize = int.Parse(stral[cnt]); ++cnt;

            creftLevel      = int.Parse(stral[cnt]); ++cnt;
            alchemyLevel    = int.Parse(stral[cnt]); ++cnt;
            oreLevel        = int.Parse(stral[cnt]); ++cnt;
            woodLevel       = int.Parse(stral[cnt]); ++cnt;
            upgradeLevel    = int.Parse(stral[cnt]); ++cnt;
            sightLevel      = int.Parse(stral[cnt]); ++cnt;

            iconName = stral[cnt]; ++cnt;

            IMG_Creft = stral[cnt]; ++cnt;
            IMG_House = stral[cnt]; ++cnt;
            IMG_Box = stral[cnt]; ++cnt;
            IMG_Alchemy = stral[cnt]; ++cnt;
        }
    }
    public List<CampData> li_CampData = new List<CampData>();
    public void LoadTable_CampData(string[] stral)
    {
        CampData item = new CampData(stral);
        li_CampData.Add(item);
    }
    #endregion

    //자원 데이터
    #region OreWoodData
    public class OreWoodData
    {
        public readonly int id;
        public readonly string name;
        public readonly string spriteName;
        public readonly int HP;
        public readonly int dropID;

        public OreWoodData(string[] stral)
        {
            int cnt = 0;

            id = int.Parse(stral[cnt]); ++cnt;
            name = stral[cnt]; ++cnt;
            spriteName = stral[cnt]; ++cnt;
            HP = int.Parse(stral[cnt]); ++cnt;
            dropID = int.Parse(stral[cnt]); ++cnt;
        }
    }
    public List<OreWoodData> li_OreWoodData = new List<OreWoodData>();
    public void LoadTable_OreWood(string[] stral)
    {
        OreWoodData item = new OreWoodData(stral);
        li_OreWoodData.Add(item);
    }
    #endregion

    //상점 데이터
    #region Shop
    public class ShopData
    {
        public readonly int id;
        public readonly ShopTapType tapType;
        public readonly string name;
        public readonly PriceType priceType;
        public readonly int price;
        public readonly int itemID;
        public readonly string otherData;

        public ShopData(string[] stral)
        {
            int cnt = 0;

            id = int.Parse(stral[cnt]); ++cnt;
            tapType = (ShopTapType)int.Parse(stral[cnt]); ++cnt;
            name = stral[cnt]; ++cnt;
            priceType = (PriceType)int.Parse(stral[cnt]); ++cnt;
            price = int.Parse(stral[cnt]); ++cnt;
            itemID = int.Parse(stral[cnt]); ++cnt;
            otherData = stral[cnt]; ++cnt;
        }
    }
    public List<ShopData> li_ShopData = new List<ShopData>();
    public void LoadTable_ShopData(string[] stral)
    {
        ShopData item = new ShopData(stral);
        li_ShopData.Add(item);
    }
    #endregion
    
    //퀘스트 데이터
    #region Quest
    public class QuestData
    {
        public readonly int id;
        public readonly bool loop;
        public readonly string name;
        public readonly int targetID;
        public readonly int count;
        public readonly PriceType priceType;
        public readonly int price;

        public QuestData(string[] stral)
        {
            int cnt = 0;

            id = int.Parse(stral[cnt]); ++cnt;
            loop = stral[cnt] == "1" ? true : false; ++cnt;
            name = stral[cnt]; ++cnt;
            targetID = int.Parse(stral[cnt]); ++cnt;
            count = int.Parse(stral[cnt]); ++cnt;
            priceType = (PriceType)int.Parse(stral[cnt]); ++cnt;
            price = int.Parse(stral[cnt]); ++cnt;
        }
    }
    public List<QuestData> li_QuestData = new List<QuestData>();
    public void LoadTable_QuestData(string[] stral)
    {
        QuestData item = new QuestData(stral);
        li_QuestData.Add(item);
    }
    #endregion


    //사망시 대사 데이터
    #region DieDesc
    public class DieDesc
    {
        public readonly string desc;

        public DieDesc(string[] stral)
        {
            int cnt = 0;
            desc = stral[cnt]; ++cnt;
        }
    }
    public List<DieDesc> li_DieDesc = new List<DieDesc>();
    public void LoadTable_DieDesc(string[] stral)
    {
        DieDesc item = new DieDesc(stral);
        li_DieDesc.Add(item);
    }
    #endregion



    //업그레이드 데이터
    #region EquipUpgradeData
    public class EquipUpgradeData
    {
        public readonly int level;
        public readonly int chance;

        public EquipUpgradeData(string[] stral)
        {
            int cnt = 0;
            level  = int.Parse(stral[cnt]); ++cnt;
            chance = int.Parse(stral[cnt]); ++cnt;

        }
    }
    public List<EquipUpgradeData> li_EquipUpgradeData = new List<EquipUpgradeData>();
    public void LoadTable_EquipUpgradeData(string[] stral)
    {
        EquipUpgradeData item = new EquipUpgradeData(stral);
        li_EquipUpgradeData.Add(item);
    }
    #endregion






    //광고 보상 데이터
    #region AD_Reward
    public class AD_Reward
    {
        public readonly int id;
        public readonly string name;
        public readonly AD_RewardType rewardType;
        public readonly int rewardID;
        public readonly int rewardCount;

        public AD_Reward(string[] stral)
        {
            int cnt = 0;
            id = int.Parse(stral[cnt]); ++cnt;
            name = stral[cnt]; ++cnt;
            rewardType = (AD_RewardType)int.Parse(stral[cnt]); ++cnt;
            rewardID = int.Parse(stral[cnt]); ++cnt;
            rewardCount = int.Parse(stral[cnt]); ++cnt;
        }
    }

    public List<AD_Reward> li_AD_Reward = new List<AD_Reward>();
    public void LoadTable_AD_Reward(string[] stral)
    {
        AD_Reward item = new AD_Reward(stral);
        li_AD_Reward.Add(item);
    }
    #endregion




    //////////////////////////////////////////
    //////////////////////////////////////////








    //로컬라이징
    public void LoadTable_Localizing(string[] stral)
    {
        LM.DATA.Add(stral[1]);
    }

    ////////////////////////편의성 함수
    public Item FindItem(int _id)
    {
        Item Find;
        Find = li_Equipment.Find(r => r.id == _id);
        if (Find != null)
        {
            return Find;
        }

        Find = li_Weapon.Find(r => r.id == _id);
        if (Find != null)
        {
            return Find;
        }

        Find = li_ETC.Find(r => r.id == _id);
        if (Find != null)
        {
            return Find;
        }

        Find = li_UseItem.Find(r => r.id == _id);
        if (Find != null)
        {
            return Find;
        }

        return null;
    }

    
}

