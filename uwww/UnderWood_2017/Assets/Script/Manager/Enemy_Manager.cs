﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Manager : Singleton<Enemy_Manager>
{
    int RemainEnemy = 0;
    public GameObject ParentContent;
    GameObject currentPool;

    //각 몬스터의 뎁스를 관리 - 현제 생성된 몬스터의 마지막 뎁스를 리턴
    int startDepth = 0;

    //적군 생성 버튼용
    public int SpawnEnemyIndex = 0;
    bool isEnd_Right = false;
    bool isEnd_Left = false;

    public float GapMax = 2.5f;

    /// <summary>
    /// 모든 몬스터 삭제+리스트 클리어
    /// </summary>

    public void ResetEnemy()
    {
        StopAllCoroutines();
        if (currentPool != null)
        {
            currentPool.SetActive(false);
            Destroy(currentPool);
        }

        currentPool = null;
        RemainEnemy = 0;
        startDepth = 0;
    }

    public void CreateEnemy()
    {
        currentPool = Instantiate(ParentContent, this.transform);

        switch (Map_Manager.Get.GetCurrentMapData().type)
        {
            case MapType.Normal:
            case MapType.Boss:
                GapMax = 0.0f;
                break;
            case MapType.TimeAttack:
                GapMax = -1.0f;
                break;
            case MapType.Defence:
                GapMax = 4.0f;
                break;
            case MapType.Survival:
                GapMax = 3.0f;
                break;
        }
        

        StartCoroutine(MakeMonster_Left(Map_Manager.Get.GetCurrentMapData()));
        StartCoroutine(MakeMonster_Right(Map_Manager.Get.GetCurrentMapData()));
    }


    IEnumerator MakeMonster_Left(Table_Manager.MapData _mapData)
    {
        //리젠 간격
        float makeGap = 0;
        int loop = 0;
        int EnemyIndex = 0;
        isEnd_Left = false;

        //0이면 하면 안댐...
        if (_mapData.li_Monster_Left.Count != 0)
        {
            while (true)
            {
                if (Character.Get.isLive != false)
                {
                    //리스트 갯수보다 생성한 적이 많으면 루프횟수 ++.
                    if (EnemyIndex >= _mapData.li_Monster_Left.Count)
                    {
                        EnemyIndex = 0;
                        loop++;
                        //loop가 0이면 무한리젠, loop 횟수가 다되면 생성 중지
                        if (_mapData.loop != 0 && loop >= _mapData.loop)
                        {
                            break;
                        }
                    }

                    CreatOneEnemy(_mapData.li_Monster_Left[EnemyIndex], -1100);
                    EnemyIndex++;
                    makeGap = Random.Range(1.0f + GapMax, 3.0f + GapMax);
                    yield return new WaitForSeconds(makeGap);
                }
                yield return null;
            }
        }


        isEnd_Left = true;
    }
    IEnumerator MakeMonster_Right(Table_Manager.MapData _mapData)
    {
        //리젠 간격
        float makeGap = 0;
        int loop = 0;
        int EnemyIndex = 0;
        isEnd_Right = false;

        if(_mapData.li_Monster_Right.Count != 0)
        {
            while (true)
            {
                if (Character.Get.isLive != false)
                {
                    //리스트 갯수보다 생성한 적이 많으면 루프횟수 ++.
                    if (EnemyIndex >= _mapData.li_Monster_Right.Count)
                    {
                        EnemyIndex = 0;
                        loop++;
                        //loop가 0이면 무한리젠, loop 횟수가 다되면 생성 중지
                        if (_mapData.loop != 0 && loop >= _mapData.loop)
                        {
                            break;
                        }
                    }

                    CreatOneEnemy(_mapData.li_Monster_Right[EnemyIndex], 1100);
                    EnemyIndex++;
                    makeGap = Random.Range(1.0f + GapMax, 3.0f + GapMax);
                    yield return new WaitForSeconds(makeGap);
                }
                yield return null;
            }
        }

        isEnd_Right = true;
    }

    GameObject temp;
    Enemy tempScp;
    public void CreatOneEnemy(int _Id , int _posX = 0)
    {
        Table_Manager.EnemyData Find = Table_Manager.Get.li_EnemyData.Find(r => r.id == _Id);
        if (Find == null)
        {
            Debug.LogError("없는 몬스터 데이터");
            return;
        }

        GameObject go_Find = Resources.Load(string.Format("Prefab/{0}", Find.PrefName), typeof(GameObject)) as GameObject;
        if(go_Find == null)
        {
            Debug.LogError("없는 몬스터 프리팹");
            return;
        }

        temp = Instantiate(go_Find, currentPool.transform);
        temp.transform.localPosition = new Vector3(_posX, ConstValue.pos_Y, 0);
        tempScp = temp.GetComponent<Enemy>();
        startDepth = tempScp.Init( _Id, startDepth);
        RemainEnemy++;
    }

    public int GetRemainEnemy()
    {
        return RemainEnemy;
    }

    public void DieEnemy()
    {
        RemainEnemy--;
    }

    public bool isEndCreateEnemy()
    {
        if (isEnd_Left == true && isEnd_Right)
            return true;

        return false;
    }
}
