﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class Ads_Manager : Singleton<Ads_Manager>
{
    private string
        androidGameId = "1630445",
        iosGameId = "1350598";

    [SerializeField]
    private bool testMode;

    public delegate void AdsCallBack();
    AdsCallBack Del;

    void Start()
    {
        string gameId = null;

#if UNITY_ANDROID
        gameId = androidGameId;
#elif UNITY_IOS
        gameId = iosGameId;
#endif

        if (Advertisement.isSupported && !Advertisement.isInitialized)
        {
            Advertisement.Initialize(gameId, testMode);
        }

    }

    public void ShowAd()
    {
        if (Advertisement.IsReady())
        {
            Debug.Log("광고 시청 로딩");
            Advertisement.Show();
        }
    }

    //연속 클릭 방지
    bool isShowing = false;
    public void ShowRewardedAd(AdsCallBack _del)
    {
        if (isShowing == true)
            return;


        if (Advertisement.IsReady("rewardedVideo"))
        {
            isShowing = true;
            Popup_Manager.Get.Loading.Show();

            Del = _del;
            Debug.Log("광고 시청 로딩");
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
        else
        {
            Popup_Manager.Get.Warning.Show(LM.DATA[84]);
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                //
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.
                Del();
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                Popup_Manager.Get.Warning.Show(LM.DATA[84]);
                break;
        }

        Popup_Manager.Get.Loading.HideNow();
        isShowing = false;
    }


}

