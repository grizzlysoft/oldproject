﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map_Manager : Singleton<Map_Manager>
{
    public Animation CameraAnim;

    public SpriteRenderer tx_BG_Near;
    public SpriteRenderer tx_BG_Mid;
    public SpriteRenderer tx_BG_Far;
    public SpriteRenderer tx_Ground;

    public Transform mapObject_parent;
    public List<MapObjectContent> li_CampObject = new List<MapObjectContent>();
    public Camp camp;
    GameObject currentMapObject;

    Table_Manager.MapData currentMapData;
    public Table_Manager.MapData GetCurrentMapData() { return currentMapData; }

    int moveValue = 100;
    float mapY = 110;

    public DefenceObject defenceObj;
    public GameObject go_travelPointNotice;

    bool isEnter_Mine;
    bool isEnter_Lumber;

    void Update()
    {
        tx_BG_Near.transform.localPosition = new Vector3(-Character.Get.transform.localPosition.x * 3 * moveValue / 640f, mapY, 0);
        tx_BG_Mid.transform.localPosition = new Vector3(-Character.Get.transform.localPosition.x * 2 * moveValue / 640f, mapY, 0);
        tx_BG_Far.transform.localPosition = new Vector3(-Character.Get.transform.localPosition.x * 1 * moveValue / 640f, mapY, 0);
    }

    public void Init()
    {
        mapY = tx_BG_Near.transform.localPosition.y;
        ChangeMap_Pos(0,false);
        isEnter_Mine = true;
        isEnter_Lumber = true;
    }

    public void ChangeMap_CharacterPos(int _id, bool _isSaving = true)
    {
        //광산,벌목장 예외 처리
        if ((_id == -1 && isEnter_Mine == false) ||
           (_id == -2 && isEnter_Lumber == false))
        {
            Popup_Manager.Get.Warning.Show(LM.DATA[30]);
            return;
        }

        int temp = (int)Character.Get.gameObject.transform.localPosition.x;
        ChangeMap_Pos(_id,true, temp);
    }

    public void ChangeMap_Pos(int _id , bool _isSaving = true ,int _pos = 0)
    {
        Table_Manager.MapData Find = FindMapData(_id);
        if (Find != null)
        {
            currentMapData = Find;
            if (_isSaving == true)
                Character.Get.SavePlayerLocalData();
            StartCoroutine(ChangeMap(_pos));
        }
        else
        {
            Debug.Log("해당 맵데이터가 없음. 마을로 귀환");
            Find = FindMapData(0);
            currentMapData = Find;
            if (_isSaving == true)
                Character.Get.SavePlayerLocalData();
            StartCoroutine(ChangeMap(_pos));
        }
    }
    
    Table_Manager.MapData FindMapData(int _id)
    {
        Table_Manager.MapData FindMap = null;
        for (int i=0; i<Table_Manager.Get.li_MapData.Count; i++)
        {
            if(Table_Manager.Get.li_MapData[i].stageID == _id)
            {
                FindMap = Table_Manager.Get.li_MapData[i];
            }
        }
        
        return FindMap;
    }
    

    IEnumerator ChangeMap(int _pos = 0)
    {
        Popup_Manager.Get.SceneChange.Show();
        yield return Popup_Manager.Get.SceneChange.FadeIN();

        //캐릭터 위치 변경
        Character.Get.MoveCharacterPosition(_pos);

        //배경 이미지 탐색
        Sprite findImg_BG_Near = Resources.Load(string.Format("Map/{0}", currentMapData.BG_Near), typeof(Sprite)) as Sprite;
        Sprite findImg_BG_Mid = Resources.Load(string.Format("Map/{0}", currentMapData.BG_Mid), typeof(Sprite)) as Sprite;
        Sprite findImg_BG_Far = Resources.Load(string.Format("Map/{0}", currentMapData.BG_Far), typeof(Sprite)) as Sprite;

        if (findImg_BG_Near == null)
        {
            Debug.LogError("배경 찾기 실패. 잘못된 배경이름");
        }

        //땅 이미지 탐색
        Sprite findImg_Ground = Resources.Load(string.Format("Map/{0}", currentMapData.Ground_name), typeof(Sprite)) as Sprite;
        // Sprite findImg_Ground_U = Resources.Load(string.Format("5_Map/{0}_U", _mapData.Ground_name), typeof(Sprite)) as Sprite;
        if (findImg_Ground == null)
        {
            Debug.LogError("타일 찾기 실패. 잘못된 타일 이름");
        }

        //맵 로딩
        tx_BG_Near.sprite = findImg_BG_Near;
        tx_BG_Mid.sprite = findImg_BG_Mid;
        tx_BG_Far.sprite = findImg_BG_Far;
        tx_Ground.sprite = findImg_Ground;

        //모든 오브젝트 숨기기- 땅에 떨어진 아이템이나 날아가고 있는 화살등
        ObjectPool_Manager.Get.AllHide();

        //이펙트 임시 제거
        Character.Get.SetTrail(false);

        //자원 생성
        ObjectPool_Manager.Get.oreWood.CreateOreWood(currentMapData);

        //적군 데이터가 남아있다면 삭제
        Enemy_Manager.Get.ResetEnemy();

        //스테이지 초기화 
        Stage_Manager.Get.ResetStage();

        //맵 오브젝트 데이터가 남아있다면 삭제
        if (currentMapObject != null)
            Destroy(currentMapObject);

        //빠른 이동 포인트 설정
        go_travelPointNotice.SetActive(currentMapData.isfastTrevel);

        //맵 넘어감 연출 1초
        yield return new WaitForSeconds(0.5f);

        //사운드 변경
        Sound_Manager.Get.PlayBGM(currentMapData.bgmName);

        Character.Get.topStatUI.SetCurrentPos(currentMapData.name);

        //차지 중이면 캔슬
        Character.Get.EndChargeTime();

        //맵 타입별 셋팅
        if (currentMapData.type == MapType.Camp)
        {
            //상점 버튼 OnOff
            Character.Get.topStatUI.btn_Shop.SetActive(true);

            RandomDayNightSetting();
            camp.Show();

            //마을로 돌아올때는 체력을 회복한다
            Character.Get.Resirrection();

            yield return Popup_Manager.Get.SceneChange.FadeOut();
            Popup_Manager.Get.SceneChange.HideNow();
        }
        else if (currentMapData.type == MapType.Mine)
        {
            //상점 버튼 OnOff
            Character.Get.topStatUI.btn_Shop.SetActive(true);

            SceneEffect.Get.SetDarkness(currentMapData.darkness);
            camp.Hide();
            MapObjectSetting();
            isEnter_Mine = false;

            yield return Popup_Manager.Get.SceneChange.FadeOut();
            Popup_Manager.Get.SceneChange.HideNow();
        }
        else if (currentMapData.type == MapType.Lumber)
        {
            //상점 버튼 OnOff
            Character.Get.topStatUI.btn_Shop.SetActive(true);

            RandomDayNightSetting();
            camp.Hide();
            MapObjectSetting();
            isEnter_Lumber = false;

            yield return Popup_Manager.Get.SceneChange.FadeOut();
            Popup_Manager.Get.SceneChange.HideNow();
        }
        else
        {
            //상점 버튼 OnOff
            Character.Get.topStatUI.btn_Shop.SetActive(false);

            //스테이지 진입 
            SceneEffect.Get.SetDarkness(currentMapData.darkness);
            camp.Hide();
            MapObjectSetting();

            //스테이지 진입시 재입장 가능해짐
            isEnter_Mine   = true;
            isEnter_Lumber = true;

            yield return Popup_Manager.Get.SceneChange.FadeOut();
            Popup_Manager.Get.SceneChange.HideNow();
            Stage_Manager.Get.StartStage();
        }

    }

    void RandomDayNightSetting()
    {
        //일정확률로 밤으로 만들기
        int rnd = Random.Range(0, 10);
        if (rnd < 3)
        {
            SceneEffect.Get.SetDarkness(0.9f);
        }
        else
        {
            SceneEffect.Get.SetDarkness(0f);
        }
    }

    void MapObjectSetting()
    {
        if (currentMapData.MapObejct != "")
        {
            GameObject go_Find = Resources.Load(string.Format("Prefab/{0}", currentMapData.MapObejct), typeof(GameObject)) as GameObject;
            if (go_Find == null)
            {
                Debug.LogError("없는 프리팹");
            }
            else
            {
                currentMapObject = Instantiate(go_Find, mapObject_parent.transform);
                currentMapObject.transform.localPosition = Vector3.zero;
            }
        }
    }
    
    public void CameraShake()
    {
        CameraAnim.Play("ShakeCameraX");
    }
}
