﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngameUI_Manager : Singleton<IngameUI_Manager>
{
    public enum PressButtonName
    {
        Attack = 0,
        Skill_1,
        Skill_2,
        Skill_3,
        Dodge,
    }

    public enum QuickSlotType
    {
        HP,
        STA
    }

    public JoyStick joyStick;
    public List<JoyStickPressButton> li_PressButton = new List<JoyStickPressButton>();
    public List<UISprite> li_ButtonSprite = new List<UISprite>();
    public UISprite sp_EquipWeapon;
    public UISprite sp_FunctionButton;
    public UILabel lb_StageLabel;
    public List<QuickSlot> li_QuickSlot = new List<QuickSlot>();

    float remainTime;

    public bool isPress(PressButtonName _Press)
    {
        return li_PressButton[(int)_Press].isPress;
    }

    public void SetButtonTimer(PressButtonName _Press, float _Time)
    {
        li_PressButton[(int)_Press].SetCoolDown(_Time);
    }

    public void SetButtonSprite(Table_Manager.WeaponData _data)
    {
        if (_data == null)
        {
            sp_EquipWeapon.spriteName = "SkillIcon_NoWeapon";

            li_ButtonSprite[(int)PressButtonName.Attack].gameObject.SetActive(false);
            li_ButtonSprite[(int)PressButtonName.Skill_1].spriteName = "Close";
            li_ButtonSprite[(int)PressButtonName.Skill_2].spriteName = "Close";
            li_ButtonSprite[(int)PressButtonName.Skill_3].spriteName = "Close";
            li_ButtonSprite[(int)PressButtonName.Dodge].spriteName = "Close";
            return;
        }

        //일반 공격
        sp_EquipWeapon.spriteName = "SkillIcon_Attack";

        li_ButtonSprite[(int)PressButtonName.Attack].gameObject.SetActive(true);
        li_ButtonSprite[(int)PressButtonName.Attack].spriteName = _data.iconName;
        li_ButtonSprite[(int)PressButtonName.Skill_1].spriteName = string.Format("SkillIcon_{0}_Skill_1", _data.weaponType);
        li_ButtonSprite[(int)PressButtonName.Skill_2].spriteName = string.Format("SkillIcon_{0}_Skill_2", _data.weaponType);
        li_ButtonSprite[(int)PressButtonName.Skill_3].spriteName = string.Format("SkillIcon_{0}_Skill_3", _data.weaponType);
        li_ButtonSprite[(int)PressButtonName.Dodge].spriteName = "SkillIcon_Dodge";
    }

    Popup_Ingame.MapObjectType currentMapObjectType;
    public void SetMapObjectType(Popup_Ingame.MapObjectType _type)
    {
        currentMapObjectType = _type;
        switch (currentMapObjectType)
        {
            case Popup_Ingame.MapObjectType.Inven:
                sp_FunctionButton.spriteName = "Bag";
                break;
            case Popup_Ingame.MapObjectType.Storage:
                sp_FunctionButton.spriteName = "CampUp_7";
                Character.Get.noticeSay.Show(LM.DATA[22], false);
                break;
            case Popup_Ingame.MapObjectType.Upgrade:
                sp_FunctionButton.spriteName = "CampUp_5";
                Character.Get.noticeSay.Show(LM.DATA[24], false);
                break;
            case Popup_Ingame.MapObjectType.Alchemist:
                sp_FunctionButton.spriteName = "CampUp_6";
                Character.Get.noticeSay.Show(LM.DATA[28], false);
                break;
            case Popup_Ingame.MapObjectType.Customizing:
                sp_FunctionButton.spriteName = "CustomizeIcon";
                Character.Get.noticeSay.Show(LM.DATA[23], false);
                break;
            case Popup_Ingame.MapObjectType.FastTravel:
                sp_FunctionButton.spriteName = "CampUp_0";
                Character.Get.noticeSay.Show(LM.DATA[29], false);
                break;
            case Popup_Ingame.MapObjectType.CampUpgrade:
                sp_FunctionButton.spriteName = "BluePrint";
                Character.Get.noticeSay.Show(LM.DATA[32], false);
                break;
            case Popup_Ingame.MapObjectType.Craft:
                sp_FunctionButton.spriteName = "CampUp_1";
                Character.Get.noticeSay.Show(LM.DATA[38], false);
                break;
            case Popup_Ingame.MapObjectType.Mine:
                sp_FunctionButton.spriteName = "CampUp_3";
                Character.Get.noticeSay.Show(LM.DATA[33], false);
                break;
            case Popup_Ingame.MapObjectType.LumberCamp:
                sp_FunctionButton.spriteName = "CampUp_4";
                Character.Get.noticeSay.Show(LM.DATA[34], false);
                break;
            case Popup_Ingame.MapObjectType.GoHome:
                sp_FunctionButton.spriteName = "CampUp_0";
                Character.Get.noticeSay.Show(LM.DATA[63], false);
                break;

            case Popup_Ingame.MapObjectType.None:
                sp_FunctionButton.spriteName = "Close";
                break;

            default:
                break;
        }
    }

    public void OnClick_FunctionButton()
    {
        if (currentMapObjectType == Popup_Ingame.MapObjectType.None)
            return;

        //Move 일땐 Idle로 
        if (Character.Get.currentState == Character.CharacterState.Move)
            Character.Get.ChangeState(Character.CharacterState.Idle);

        //Idle , Move가 아니면
        if (Character.Get.currentState != Character.CharacterState.Idle)
            return;

        Sound_Manager.Get.PlayEffect("Effect_click");

        if (Popup_Manager.Get.Ingame.gameObject.activeSelf == false)
        {
            switch (currentMapObjectType)
            {
                case Popup_Ingame.MapObjectType.Inven:
                case Popup_Ingame.MapObjectType.Storage:
                case Popup_Ingame.MapObjectType.Upgrade:
                case Popup_Ingame.MapObjectType.Alchemist:
                case Popup_Ingame.MapObjectType.Customizing:
                case Popup_Ingame.MapObjectType.FastTravel:
                case Popup_Ingame.MapObjectType.CampUpgrade:
                case Popup_Ingame.MapObjectType.Craft:
                    Popup_Manager.Get.Ingame.Show(currentMapObjectType);
                    break;
                case Popup_Ingame.MapObjectType.Mine:
                    Map_Manager.Get.ChangeMap_CharacterPos(-1);
                    break;
                case Popup_Ingame.MapObjectType.LumberCamp:
                    Map_Manager.Get.ChangeMap_CharacterPos(-2);
                    break;
                case Popup_Ingame.MapObjectType.GoHome:
                    Map_Manager.Get.ChangeMap_CharacterPos(0);
                    break;
                case Popup_Ingame.MapObjectType.None:
                    break;
                default:
                    break;
            }
        }
    }

    public void OnClick_InvenButton()
    {
        //Move 일땐 Idle로 
        if (Character.Get.currentState == Character.CharacterState.Move)
            Character.Get.ChangeState(Character.CharacterState.Idle);

        //Idle , Move가 아니면
        if (Character.Get.currentState != Character.CharacterState.Idle)
            return;

        if (Popup_Manager.Get.Ingame.gameObject.activeSelf == true)
        {
            if (Popup_Manager.Get.Ingame.openType == Popup_Ingame.MapObjectType.Inven)
                Popup_Manager.Get.Ingame.Hide();
            else
                return;
        }
        else
        {
            Popup_Manager.Get.Ingame.Show(Popup_Ingame.MapObjectType.Inven);
        }

        Sound_Manager.Get.PlayEffect("Effect_click");
    }

    public void ShowUI(Popup_Ingame.MapObjectType _type)
    {
        Popup_Manager.Get.Ingame.Hide();
        Popup_Manager.Get.Ingame.Show(_type);
    }

    public void CheckQuickSlot(itemSlotData _data)
    {
        switch (_data.tableData.itemType)
        {
            case ItemType.HP_Potion:
                li_QuickSlot[(int)QuickSlotType.HP].CheckData(_data);
                break;
            case ItemType.STA_Potion:
                li_QuickSlot[(int)QuickSlotType.STA].CheckData(_data);
                break;

            //나머지는 처리 안함
            default:
                return;
        }
    }

    public void SetAutoQuickSlot_Init()
    {
        SetAutoQuickSlot(ItemType.HP_Potion);
        SetAutoQuickSlot(ItemType.STA_Potion);
    }

    public void SetAutoQuickSlot(ItemType _type)
    {
        //기존에 해당 슬롯에 데이터가 있다면 리턴한다.
        switch (_type)
        {
            case ItemType.HP_Potion:
                if (li_QuickSlot[(int)QuickSlotType.HP].GetData() != null)
                    return;
                else
                    li_QuickSlot[(int)QuickSlotType.HP].Refresh();
                break;
            case ItemType.STA_Potion:
                if (li_QuickSlot[(int)QuickSlotType.STA].GetData() != null)
                    return;
                else
                    li_QuickSlot[(int)QuickSlotType.HP].Refresh();
                break;

            //나머지는 처리 안함
            default:
                return;
        }

        itemSlotData findSlotData = null;
        List<itemSlotData> Find = Character.Get.CheckHaveItem_Inventory(_type);
        if (Find.Count != 0)
        {
            findSlotData = Find[0];
            //가장 낮은 등급 포션 불러오기
            for (int i = 1; i < Find.Count; i++)
            {
                if (findSlotData.tableData.id > Find[i].tableData.id)
                {
                    findSlotData = Find[i];
                }
            }
        }

        AddQuickSlot(findSlotData,_type);
    }

    public void AddQuickSlot(itemSlotData _data , ItemType _type)
    {
        switch (_type)
        {
            case ItemType.HP_Potion:
                li_QuickSlot[(int)QuickSlotType.HP].SetSlot(_data);
                break;
            case ItemType.STA_Potion:
                li_QuickSlot[(int)QuickSlotType.STA].SetSlot(_data);
                break;
        }
    }


    public void SetActiveRemainTime(bool _isActive , float _value = 0)
    {
        lb_StageLabel.gameObject.SetActive(_isActive);
        if (_isActive == true)
        {
            remainTime = _value;
            InvokeRepeating("Invoke_RemainTime", 0, 0.01f);
        }
        else
        {
            remainTime = 0;
            CancelInvoke();
        }
    }
    
    
    void Invoke_RemainTime()
    {
        if(Character.Get.isLive == true)
        {
            remainTime -= 0.01f;
            if (remainTime < 10.0f)
                lb_StageLabel.text = string.Format("{0}\n[A90000]{1}[-]", LM.DATA[60], remainTime.ToString("F2"));

            else
                lb_StageLabel.text = string.Format("{0}\n{1}", LM.DATA[60], remainTime.ToString("F2"));
        }
     
    }

    public void AddTime(float _Time)
    {
        if (Map_Manager.Get.GetCurrentMapData().type == MapType.TimeAttack)
        {
            remainTime += _Time;
        }
    }

    public float GetRemainTime()
    {
        return remainTime;
    }
    
}
