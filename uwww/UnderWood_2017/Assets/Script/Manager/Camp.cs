﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camp : MonoBehaviour
{
    //
    public SpriteRenderer sp_Creft;
    public SpriteRenderer sp_House;
    public SpriteRenderer sp_Box;
    public SpriteRenderer sp_Alchemy;

    public List<GameObject> li_CampObject = new List<GameObject>();
    
    public void SetCampObject()
    {
        for (int i = 0; i < li_CampObject.Count; i++)
        {
            if (li_CampObject[i] == null)
                continue;

            if (i < Character.Get.CampLevel + 1)
                li_CampObject[i].SetActive(true);
            else
                li_CampObject[i].SetActive(false);
        }

        sp_Creft.sprite     = Resources.Load(string.Format("CampObject/{0}", Character.Get.CampData().IMG_Creft), typeof(Sprite)) as Sprite;
        sp_House.sprite     = Resources.Load(string.Format("CampObject/{0}", Character.Get.CampData().IMG_House), typeof(Sprite)) as Sprite;
        sp_Box.sprite       = Resources.Load(string.Format("CampObject/{0}", Character.Get.CampData().IMG_Box), typeof(Sprite)) as Sprite;
        sp_Alchemy.sprite   = Resources.Load(string.Format("CampObject/{0}", Character.Get.CampData().IMG_Alchemy), typeof(Sprite)) as Sprite;
    }

    public void Show()
    {
        this.gameObject.SetActive(true);
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }
}
