﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;


public class Sprite_Manager : Singleton<Sprite_Manager>
{
    [SerializeField]
    SpriteAtlas Atlas_Character;

    [SerializeField]
    SpriteAtlas Atlas_Icon;

    [SerializeField]
    SpriteAtlas Atlas_MapObject;

    public Sprite GetSprite_Character(string _name)
    {
        return Atlas_Character.GetSprite(_name);
    }

    public Sprite GetSprite_Weapon(string _name)
    {
        return Atlas_Character.GetSprite(_name);
    }

    public Sprite GetSprite_Equip(string _name)
    {
        return Atlas_Character.GetSprite(_name);
    }

    public Sprite GetSprite_Icon(string _name)
    {
        return Atlas_Icon.GetSprite(_name);
    }

    public Sprite GetSprite_MapObject(string _name)
    {
        return Atlas_MapObject.GetSprite(_name);
    }

}
