﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Manager : Singleton<Popup_Manager>
{
    public GameObject BlackBG;
    //팝업 목록 - 외부에서 이걸로 OnOff관리
    public Popup_YesNo YesNo;
    public Popup_Warning Warning;
    public Popup_Die Die;
    public Popup_BlockIngame BlockIngame;
    public Popup_UpgradeResult UpgradeResult;
    public Popup_Selling Selling;
    public Popup_CampLevelUp CampLevelUp;
    public Popup_Ingame Ingame;
    public Popup_Option Option;
    public Popup_Shop Shop;
    public Popup_Buy Buy;
    public Popup_Help Help;
    public Popup_Login Login;
    public Popup_Loading Loading;
    public Popup_Check Check;
    public Popup_StageStart StageStart;
    public Popup_StageResult StageResult;
    public Popup_SceneChange SceneChange;
    public Popup_AD AD;
    public Popup_Teleport Teleport;


    // 백버튼 관리 
    List<Popup> li_Popup = new List<Popup>();
    public void PushList(Popup popup)
    {
        li_Popup.Add(popup);
        CheckActiveBlackBG();
    }
    public void PopList(GameObject go)
    {
        if (li_Popup.Count > 0)
        {
            Popup temp = li_Popup.Find(r => r.gameObject == go);
            if (temp != null)
            {
                li_Popup.Remove(temp);
            }
        }

        CheckActiveBlackBG();
    }
    public bool isEmptyList()
    {
        if (li_Popup.Count == 0)
            return true;
        else
            return false;
    }
    public void ShowLog()
    {
        Debug.Log("===================");
        for (int i = 0; i < li_Popup.Count; i++)
        {
            Debug.Log(i + li_Popup[i].name);
        }
        Debug.Log("===================");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //리스트 아웃
            if (isEmptyList() == true)
            {
                if(Map_Manager.Get.GetCurrentMapData().type != MapType.Camp )
                {
                    //스테이지 클리어 , 스테이지 시작이 아닐때
                    if (StageStart.gameObject.activeSelf == false && StageResult.gameObject.activeSelf == false)
                    {
                        Time.timeScale = 0;
                        YesNo.Show("일시정지", "주둔지로 가시겠습니까?", GoHome, ResetTimeScale);
                        return;
                    }

                    return;
                }
                else
                {
                    //게임종료 팝업 Open
                    YesNo.Show("Quit Game", LM.DATA[11], ExitGame);
                    return;
                }
            }

            li_Popup[li_Popup.Count - 1].Hide();
        }

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.F5))
        {
            PlayerPrefs.DeleteAll();
            //ShowLog();
        }
#endif

    }

    public void AllHide()
    {
        int count = li_Popup.Count;
        for (int i=0; i< count; i++)
        {
            li_Popup[0].Hide();
        }
    }

    void ExitGame()
    {
        Character.Get.SavePlayerLocalData();
        Application.Quit();
    }

    void GoHome()
    {
        ResetTimeScale();
        Map_Manager.Get.ChangeMap_Pos(0);
    }

    void ResetTimeScale()
    {
        Time.timeScale = 1;
    }

    public void CheckActiveBlackBG()
    {
        for(int i=0; i<li_Popup.Count; i++)
        {
            if(li_Popup[i].isBlackBG == true)
            {
                BlackBG.SetActive(true);
                return;
            }
        }

        BlackBG.SetActive(false);
    }

}
