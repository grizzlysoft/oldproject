﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage_Manager : Singleton<Stage_Manager>
{
    //스테이지 진행중인가?
    bool isStageProgress;

    //생존 모드 활성화

    private void Update()
    {
        if (isStageProgress == false)
            return;

        //캐릭터가 죽어있다면 클리어 체크가 되지 않음
        if (Character.Get.isLive == true)
        {
            if (CheckClear() == true)
            {
                EndStage();
            }
        }
    }

    public void ResetStage()
    {
        isStageProgress = false;
        IngameUI_Manager.Get.SetActiveRemainTime(false);
        Map_Manager.Get.defenceObj.Hide();
        StopAllCoroutines();
    }

    public void StartStage()
    {
        StartCoroutine(StageProgress());
    }
    IEnumerator StageProgress()
    {
        //스테이지 진입 연출
        Popup_Manager.Get.StageStart.Show();
        while (true)
        {
            if (Popup_Manager.Get.StageStart.gameObject.activeSelf == false)
                break;

            yield return null;
        }

        //맵 타입에 따른 추가 UI/오브젝트 구성
        switch (Map_Manager.Get.GetCurrentMapData().type)
        {
            case MapType.TimeAttack:
                IngameUI_Manager.Get.SetActiveRemainTime(true, ConstValue.TimeAttack_Time - (Map_Manager.Get.GetCurrentMapData().difficult * 5));
                break;
            case MapType.Defence:
                IngameUI_Manager.Get.SetActiveRemainTime(true, ConstValue.Defence_Time);
                Map_Manager.Get.defenceObj.Show(65 - Map_Manager.Get.GetCurrentMapData().difficult * 5);
                break;
            case MapType.Survival:
                IngameUI_Manager.Get.SetActiveRemainTime(true, ConstValue.Survival_Time);
                StartCoroutine(DamagedField());
                break;
        }
        
        //몬스터 생성
        Enemy_Manager.Get.CreateEnemy();
        isStageProgress = true;

        yield return null;
    }

    IEnumerator DamagedField()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            Character.Get.GetDamaged(Map_Manager.Get.GetCurrentMapData().difficult*2);
        }
    }

    void EndStage()
    {
        ResetStage();

        //몬스터 삭제 및 데이터 초기화
        Enemy_Manager.Get.ResetEnemy();
        //현제 클리어한 던전레벨이 기존 레벨보다 높은 던전이였다면 ++
        if (Map_Manager.Get.GetCurrentMapData().stageID >= Character.Get.stageLevel)
        {
            Character.Get.AddDia(1);
            Character.Get.stageLevel++;

            //스테이지 클리어 연출
            Popup_Manager.Get.StageResult.Show(true, 1);
        }
        else
        {
            //5스테이지 마다 50원 증가
            int a = Map_Manager.Get.GetCurrentMapData().stageID / 5;
            if (a == 0)
                a = 1;

            int getGold = a * 50;
            Character.Get.AddGold(getGold);

            //스테이지 클리어 연출
            Popup_Manager.Get.StageResult.Show(false, getGold);
        }
    }

    bool CheckClear()
    {
        switch (Map_Manager.Get.GetCurrentMapData().type)
        {
            case MapType.Camp:
            case MapType.Mine:
            case MapType.Lumber:
                break;
            case MapType.Normal:
            case MapType.Boss:
                if (Enemy_Manager.Get.isEndCreateEnemy() == true && Enemy_Manager.Get.GetRemainEnemy() == 0)
                    return true;
                break;
            case MapType.TimeAttack:
                if (Enemy_Manager.Get.isEndCreateEnemy() == true && Enemy_Manager.Get.GetRemainEnemy() == 0)
                    return true;

                //시간이 지나면 캐릭터 죽여버림
                if (IngameUI_Manager.Get.GetRemainTime() <= 0)
                    Character.Get.Death();
                break;
            case MapType.Defence:
                //시간이 지나면 승리
                if (IngameUI_Manager.Get.GetRemainTime() <= 0)
                    return true;

                //오브젝트 부서지면 패배
                if(Map_Manager.Get.defenceObj.HP <= 0)
                    Character.Get.Death();

                break;
            case MapType.Survival:
                //시간이 지나면 승리
                if (IngameUI_Manager.Get.GetRemainTime() <= 0)
                    return true;
                break;
            default:
                break;
        }

        return false;
    }

}
