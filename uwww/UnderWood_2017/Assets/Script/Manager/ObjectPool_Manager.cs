﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool_Manager : Singleton<ObjectPool_Manager>
{
    public ObjectPool_Quiver quiver;
    public ObjectPool_IngameEffect hitEffect;
    public ObjectPool_IngameEffect healEffect;
    public ObjectPool_HitText hitText;
    public ObjectPool_DropItem dropItem;
    public ObejctPool_OreWood oreWood;

    public void Init()
    {
        quiver.Init();
        hitEffect.Init();
        dropItem.Init();
        oreWood.Init();
        hitText.Init();
        healEffect.Init();
    }

    public void AllHide()
    {
        quiver.AllHide();
        hitEffect.AllHide();
        dropItem.AllHide();
        oreWood.AllHide();
        hitText.AllHide();
        healEffect.AllHide();
    }

    public void ShowDamage(Transform _tr, DamageData _Dmg)
    {
        hitText.ShowEffect(_tr, _Dmg);
        
        switch (_Dmg.Type)
        {
            case DamagedType.Enemy:
                if (_Dmg.Dmg > 0)
                {
                    PlayWeaponHitSound(_Dmg);
                    hitEffect.ShowEffect(_tr, IngameEffect.Type.Damage);
                }
                break;
            case DamagedType.Player:
                if (_Dmg.Dmg > 0)
                {
                    Sound_Manager.Get.PlayEffect("Effect_Hit_Character");
                    hitEffect.ShowEffect(_tr, IngameEffect.Type.Damage);
                }
                break;
            case DamagedType.Ore:
                Sound_Manager.Get.PlayEffect("Effect_hitore");
                hitEffect.ShowEffect(_tr, IngameEffect.Type.Ore);
                break;
            case DamagedType.Wood:
                Sound_Manager.Get.PlayEffect("Effect_hitwood");
                hitEffect.ShowEffect(_tr, IngameEffect.Type.Wood);
                break;
            default:
                break;
        }
    }

    void PlayWeaponHitSound(DamageData _Dmg)
    {
        switch (Character.Get.currentWeaponType)
        {
            case WeaponType.OneHand:
            case WeaponType.TwoHand:
                if (_Dmg.isCritcal == true)
                    Sound_Manager.Get.PlayEffect("Effect_Hit_Crit");
                else
                    Sound_Manager.Get.PlayEffect("Effect_Hit");
                break;
            case WeaponType.Bow:
                if (_Dmg.isCritcal == true)
                    Sound_Manager.Get.PlayEffect("Effect_Hit_Arrow_Crit");
                else
                    Sound_Manager.Get.PlayEffect("Effect_Hit_Arrow");
                break;
            case WeaponType.NoWeapon:
                break;
            default:
                break;
        }
    }

    public void ShowHeal(Transform _tr)
    {
        Sound_Manager.Get.PlayEffect("Effect_heal");
        healEffect.ShowEffect(_tr , IngameEffect.Type.Heal);
    }

    public void ShowSTA(Transform _tr)
    {
        Sound_Manager.Get.PlayEffect("Effect_heal");
        healEffect.ShowEffect(_tr, IngameEffect.Type.STA);
    }
}
