﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverlabReward : MonoBehaviour
{
    public UILabel lb_Price;
    public TweenAlpha tw_Alpha;


    public void Show(int _price = 0)
    {
        tw_Alpha.ResetToBeginning();
        tw_Alpha.enabled = true;

        
        if(lb_Price != null)
            lb_Price.text = _price.ToString();

        this.gameObject.SetActive(true);
    }

    public void Hide()
    {
        tw_Alpha.ResetToBeginning();
        this.gameObject.SetActive(false);
    }
}

