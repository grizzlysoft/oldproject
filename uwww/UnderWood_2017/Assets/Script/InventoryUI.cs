﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUI : ItemContainer
{
    public UILabel lb_Gold;
    public UISprite sp_Delete;

    public void RefreshGold()
    {
        lb_Gold.text = character.gold.ToString();
    }

    public override void Refresh(bool isReposition = false)
    {
        RefreshGold();

        for (int i = 0; i < character.MaxInventory; i++)
        {
            li_slot[i].Hide();
        }

        //소지한 아이템으로 초기화 시키기
        for (int i=0; i < character.GetList_Item().Count; i++)
        {
            li_slot[i].Show(character.GetList_Item()[i]);
        }

        if(isReposition == true)
        {
            SetState(Mode.Close);

            grid.Reposition();
            scrollView.ResetPosition();
        }

    }
    public override void ClickItem(itemSlotData _data)
    {
        selectItem = _data;

        //아이템 파괴 모드
        if (currentMode == Mode.Delete)
        {
            CheckDeleteItem();
        }
        else if (currentMode == Mode.Sell)
        {
            CheckSellItem();
        }
        else
        {
            switch (Popup_Manager.Get.Ingame.openType)
            {
                case Popup_Ingame.MapObjectType.Inven:
                    {
                        switch (_data.tableData.itemType)
                        {
                            case ItemType.Equipment:
                                {
                                    if (_data.count == 0)
                                    {
                                        Sound_Manager.Get.PlayEffect("Effect_equip");
                                        character.ChangeArmor(_data);
                                    }
                                    else
                                    {
                                        Sound_Manager.Get.PlayEffect("Effect_unequip");
                                        character.UnEquipArmor(_data);
                                    }
                                }
                                break;
                            case ItemType.Weapon:
                                {

                                    if (_data.count == 0)
                                    {
                                        Sound_Manager.Get.PlayEffect("Effect_equip");
                                        character.ChangeWeapon(_data);
                                    }
                                    else
                                    {
                                        Sound_Manager.Get.PlayEffect("Effect_unequip");
                                        character.UnEquipWeapon(_data);
                                    }
                                }
                                break;
                            //사용하는거
                            case ItemType.Dye:
                                {
                                    Popup_Manager.Get.YesNo.Show(string.Format("[ffaa00]- {0} -[-]", _data.tableData.name),
                                        LM.DATA[44], UseItem);
                                }
                                break;
                            case ItemType.HP_Potion:
                            case ItemType.STA_Potion:
                                {
                                    IngameUI_Manager.Get.AddQuickSlot(selectItem , _data.tableData.itemType);
                                }
                                break;
                            case ItemType.TeleportScroll:
                                {                                    
                                    //만약 마을이 아니라면 사용 불가
                                    if(Map_Manager.Get.GetCurrentMapData().type != MapType.Camp)
                                    {
                                        Popup_Manager.Get.Warning.Show("마을에서만 사용 가능 합니다");
                                        return;
                                    }

                                    Popup_Manager.Get.Teleport.Show(UseItem);
                                }
                                break;
                                //잡템은 아무것도 안됨
                            case ItemType.ETC:
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case Popup_Ingame.MapObjectType.Storage:
                    {
                        //장착중인 아이템 옮기지 못함
                        if (_data.count == -1)
                        {
                            Popup_Manager.Get.Warning.Show(LM.DATA[19]);
                            return;
                        }

                        //인벤토리 체크
                        character.MoveItem_Storage(_data);
                    }
                    break;
                case Popup_Ingame.MapObjectType.Upgrade:
                    {
                        Popup_Manager.Get.Ingame.SelectItemChange(_data);
                    }
                    break;
                default:
                    break;
            }

            Popup_Manager.Get.Ingame.Refresh();
        }
    }

    void UseItem()
    {
        character.UseItem(selectItem);
    }
    
    //장비 판매,파괴
    override protected void SellItem(int _sellCount)
    {
        character.Sell_Item(selectItem, _sellCount, false);
    }
    override protected void DeleteItem()
    {
        Sound_Manager.Get.PlayEffect("Effect_brokenitem");
        character.Remove_Item(selectItem);
    }
    override public void OnClick_Sort()
    {
        Sound_Manager.Get.PlayEffect("Effect_bookflip");
        character.Sorting_Inventory();
        Refresh();
    }
    override protected void SetStateIcon()
    {
        switch (currentMode)
        {
            case Mode.Close:
            case Mode.Sell:
                sp_Delete.spriteName = "trash_Close";
                sp_Delete.color = Color.white;
                break;
            case Mode.Delete:
                sp_Delete.spriteName = "trash_Open";
                sp_Delete.color = ColorPreset.UI_Color_DeleteItem;
                break;
            default:
                break;
        }
    }

}
