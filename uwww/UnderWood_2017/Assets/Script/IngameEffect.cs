﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IngameEffect : MonoBehaviour
{
    public enum Type
    {
        Damage = 0,
        Heal,
        STA,
        Ore,
        Wood,
    }
    ParticleSystem ps;
    ParticleSystem.MainModule main;

    List<ParticleSystem.MinMaxGradient> li_Color = new List<ParticleSystem.MinMaxGradient>();
    

    public void Init()
    {
        ps = GetComponentInChildren<ParticleSystem>();
        main = ps.main;

        //Damaged
        li_Color.Add(new ParticleSystem.MinMaxGradient(
            ColorPreset.HexToColor("670000FF"),
            ColorPreset.HexToColor("FF0000FF")));

        //Heal
        li_Color.Add(new ParticleSystem.MinMaxGradient(
            ColorPreset.HexToColor("00FF00FF"),
            ColorPreset.HexToColor("C4DA76FF")));

        //STA
        li_Color.Add(new ParticleSystem.MinMaxGradient(
           ColorPreset.HexToColor("FBFF00FF"),
           ColorPreset.HexToColor("E5C200FF")));

        //Ore
        li_Color.Add(new ParticleSystem.MinMaxGradient(
            ColorPreset.HexToColor("C3C3C3FF"),
            ColorPreset.HexToColor("7E7E7EFF")));

        //Wood
        li_Color.Add(new ParticleSystem.MinMaxGradient(
           ColorPreset.HexToColor("D0AF85FF"),
           ColorPreset.HexToColor("874D33FF")));

        Hide();
    }

    private void Update()
    {
        if (ps.IsAlive() == false)
            Hide();
    }

    public void Show( Vector3 _pos, Type _type)
    {
        main.startColor = li_Color[(int)_type];

        this.transform.position = _pos;
        this.transform.localScale = new Vector3(1, 1, 1);
        this.gameObject.SetActive(true);
    }

    public void Hide()
	{
        this.gameObject.SetActive(false);
    }

}
