﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool_DropItem : MonoBehaviour
{
    public GameObject go_DropContent;
    List<DropContent> li_DropContent = new List<DropContent>(); 

	public void Init ()
    {
        GameObject temp;
        DropContent tempScp;
        for (int i = 0; i < ConstValue.Max_DropItem; i++)
        {
            temp = Instantiate(go_DropContent, this.transform);
            temp.transform.localPosition = Vector3.zero;
            tempScp = temp.GetComponent<DropContent>();
            tempScp.Init();
            li_DropContent.Add(tempScp);
        }
    }

    public void ShowDropItem(int _id , Transform _tr)
    {
        for (int i = 0; i < li_DropContent.Count; i++)
        {
            if (li_DropContent[i].gameObject.activeSelf == false)
            {
                li_DropContent[i].Show(_id, _tr.position);
                break;
            }
        }
    }


    public void AllHide()
    {
        for (int i = 0; i < li_DropContent.Count; i++)
        {
            li_DropContent[i].Hide();
        }
    }
}
