﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//싱글톤 소스
public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    protected static T instance = null;
    public static T Get
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType(typeof(T)) as T;

                if (instance == null)
                {
                    Debug.Log("Nothing" + instance.ToString());
                    return null;
                }
            }
            return instance;
        }
    }
}


public static class ColorPreset
{
    public static Color UI_Color_Normal;
    public static Color UI_Color_DeleteItem;
    public static Color UI_Color_SellItem;
    public static Color UI_Color_Select;
    public static Color UI_Color_EmptySlot;

    public static Color Item_Color_Common;
    public static Color Item_Color_UnCommon;
    public static Color Item_Color_Magic;
    public static Color Item_Color_Rare;
    public static Color Item_Color_Unique;
    public static Color Item_Color_Relic;
    public static Color Item_Color_Legendary;

    public static void SetColor()
    {
        UI_Color_Normal     = HexToColor("787878FF");
        UI_Color_DeleteItem = HexToColor("9C1515FF");
        UI_Color_SellItem   = HexToColor("E9EC00FF");
        UI_Color_Select     = HexToColor("00FF00FF");
        UI_Color_EmptySlot  = HexToColor("3C3C3CFF");

        Item_Color_Common   = HexToColor("BDBDBDFF");
        Item_Color_UnCommon = HexToColor("48C524FF");
        Item_Color_Magic    = HexToColor("568CD6FF");
        Item_Color_Rare     = HexToColor("D7DA1BFF");
        Item_Color_Unique   = HexToColor("B33FCCFF");
        Item_Color_Relic    = HexToColor("CA701EFF");
        Item_Color_Legendary= HexToColor("830000FF");
    }

    public static Color GetGradeColor(GradeType _type)
    {
        switch (_type)
        {
            case GradeType.Common:   return Item_Color_Common;
            case GradeType.UnCommon: return Item_Color_UnCommon;
            case GradeType.Magic:    return Item_Color_Magic;
            case GradeType.Rare:     return Item_Color_Rare;
            case GradeType.Unique:   return Item_Color_Unique;
            case GradeType.Relic:    return Item_Color_Relic;
            case GradeType.Legendary:return Item_Color_Legendary;
            default:                 return Item_Color_Common;
        }
    }

    public static string GetGradeText(GradeType _type)
    {
        switch (_type)
        {
            case GradeType.Common: return "일반";
            case GradeType.UnCommon: return "고급";
            case GradeType.Magic: return "마법";
            case GradeType.Rare: return "희귀";
            case GradeType.Unique: return "영웅";
            case GradeType.Relic: return "유물";
            case GradeType.Legendary: return "전설";
            default: return "일반";
        }
    }

    /// 소수점을 제거하는 거
    static string RemoveFloat(string value)
    {
        int findIndex = 0;
        for (int i = 0; i < value.Length; i++)
        {
            if (value[i] == '.')
            {
                findIndex = i;
                break;
            }
        }

        if (findIndex != 0)
        {
            return value.Substring(0, findIndex);
        }

        return value;
    }
    public static Color HexToColor(string hex)
    {
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        byte a = byte.Parse(hex.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);

        return new Color32(r, g, b, a);
    }


}


public static class ConstValue
{
    public const int TimeAttack_Time = 60;
    public const int Defence_Time    = 40;
    public const int Survival_Time   = 30;

    public const int Max_DropItem = 20;
    public const int Max_Arrow = 30;
    public const int Max_Effect = 30;
    public const int Crit_Incidence = 5; //기본 치명타율
    public const int pos_Y = -105;
    public const int pos_X= 580;
    public const int MapMax_X = 906;
    public const int Max_Inventory = 30;

    public const float MoveSpeed_NoWeapon = 0.3f;
    public const float MoveSpeed_OneHand = 0.3f;
    public const float MoveSpeed_TwoHand = 0.3f;
    public const float MoveSpeed_Bow = 0.3f;
    public const int MaxGold = 1000000000;
    public const int MaxUpgrade = 9;

    public const float NightAlpha = 0.95f;
}

public class DamageData
{
    public DamagedType Type;
    public int Dmg;
    public bool isCritcal;
    //나중에 뭐 속성같은거 들어가도 됨
}

public class itemSlotData
{
    public string Key;
    public int count; // count 는 장비아이템일 경우 0은 미장착 , -1은 장착중
    public Table_Manager.Item tableData;

    public bool SetTableData()
    {
        //키를 분리해서 id를 추출!
        string[] stral = Key.Split('_');
        int itemid = int.Parse(stral[0]);
        tableData = Table_Manager.Get.FindItem(itemid);
        if (tableData == null)
        {
            return false;
        }

        return true;
    }
}

public enum ItemType
{
    Equipment = 0,
    Weapon,
    ETC,
    Dye,
    HP_Potion,
    STA_Potion,
    TeleportScroll,
}

public enum EquipType
{
    Armor = 0,
    Arm = 1,
    Hand = 2,
    Leg = 3,
    Foot = 4,
    Helm = 5,
}

public enum WeaponType
{
    OneHand = 0,
    TwoHand = 1,
    Bow = 2,

    NoWeapon = -1
}

public enum EnemyState
{
    Idle = 0,
    Move,
    Attack,
    Die,
    Skill_1,
    Skill_2
}

public enum eDiraction
{
    Left = 0,
    Right,
    Up,
    Down,
}

public enum FaceType
{
    Idle,
    Attack,
    Surprise,
    Sleep,
}

public enum CustomizingType
{
    Skin = 0,
    Face,
    Hair,
}

public enum GradeType
{
    Common,
    UnCommon,
    Magic,
    Rare,
    Unique,
    Relic,
    Legendary
}

public enum MapType
{
    Camp = 0,
    Normal,
    TimeAttack,
    Defence,
    Survival,
    Boss,
    Mine,
    Lumber,
}

public enum ShopTapType
{
    Cash = 0,
    Skin,
    Hair,
    Gold,
    ETC,
    MAX
}

public enum PriceType
{
    Cash = 0,
    Dia,
    Gold,
    AD,
}

public enum AD_RewardType
{
    Gold = 0,
    Dia,
    ETC,
}
public enum DamagedType
{
    Enemy,
    Player,
    Ore,
    Wood,
    Terrain,
    DefenceObject,
}

