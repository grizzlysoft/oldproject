﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool_HitText : MonoBehaviour
{
    public GameObject go_Content;
    List<DamageText> li_Effect = new List<DamageText>();

    public void Init()
    {
        GameObject temp;
        DamageText tempScp;
        for (int i = 0; i < ConstValue.Max_Effect; i++)
        {
            temp = Instantiate(go_Content, this.transform);
            temp.transform.localPosition = Vector3.zero;
            tempScp = temp.GetComponent<DamageText>();
            tempScp.Init();
            li_Effect.Add(tempScp);
        }
    }

    private void Update()
    {
        this.transform.position = Map_Manager.Get.tx_BG_Near.transform.position;
    }

    public void ShowEffect(Transform _tr, DamageData _Dmg)
    {
        for (int i = 0; i < li_Effect.Count; i++)
        {
            if (li_Effect[i].gameObject.activeSelf == false)
            {
                li_Effect[i].Show(_Dmg, _tr.position);
                break;
            }
        }
    }

    public void AllHide()
    {
        for (int i = 0; i < li_Effect.Count; i++)
        {
            li_Effect[i].Hide();
        }
    }

}
