﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : Singleton<Character>
{
    public enum CharacterState
    {
        Idle,
        Move,
        Attack,
        Skill_1,
        Skill_2,
        Skill_3,
        Die,
        Sleep,
        Dodge,
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //세이브 데이터
    public int Level;
    public int EXP;
    public int FaceIndex;
    public int SkinIndex;
    public int HairIndex;
    public int HairColorIndex;
    public int CampLevel;
    public int QuestID;
    public int QuestCount;
    public int gold;
    public int dia;
    public int stageLevel;

    List<itemSlotData> li_Item = new List<itemSlotData>(); //소지한 아이템들
    List<itemSlotData> li_Storage = new List<itemSlotData>(); //창고 아이템
    List<int> li_Costumizing = new List<int>(); //커스터마이징 오픈한 것들

    //Showing 옵션
    public int Visible_Helm;
    public int Visible_Chick;

    //광고 시청 횟수
    public int AD_Count;

    /////////////////////////////////////////////                    
    //////////////////////////////////////////////////////////////////////////////////////////

    public int MaxInventory;
    public int MaxStorage;

    public List<itemSlotData> GetList_Item(){return li_Item;}
    public List<itemSlotData> GetList_Storage() { return li_Storage; }
    public List<int> GetList_Customizing() { return li_Costumizing; }


    //능력치 - 갱신하는 값    
    public bool isLive = true;
    public int ATK;
    public int DEF;
    public int HP;
    public int STA;

    //무적 여부
    public bool isInvincibility = false;

    //능력치 - 테이블 값
    public Table_Manager.CharacterData tableData;

    //고정 값 - 아직 미사용
    [Range(0,4)]
    public float Speed = 0.5f;

    //스테이트
    delegate void StateFunction();
    public WeaponType currentWeaponType = WeaponType.NoWeapon;
    public CharacterState currentState = CharacterState.Idle;
    Dictionary<WeaponType, Dictionary<CharacterState, StateFunction>> dic_Update = new Dictionary<WeaponType, Dictionary<CharacterState, StateFunction>>();

    //착용중인 무기
    [HideInInspector]
    public Weapon equip_Weapon;
    //착용중인 장비
    Table_Manager.EquipmentData equip_Armor;
    Table_Manager.EquipmentData equip_Arm;
    Table_Manager.EquipmentData equip_Hand;
    Table_Manager.EquipmentData equip_Leg;
    Table_Manager.EquipmentData equip_Foot;
    Table_Manager.EquipmentData equip_Helm;

    //컴포넌트
    public Transform RotationPivot;
    public Character_HitBox hitBox;

    public Animation anim;

    ///장비
    [HideInInspector]
    public Dictionary<string, SpriteRenderer> dic_Equipment = new Dictionary<string, SpriteRenderer>();
    //투구 마스킹
    public SpriteMask Helm_Mask;
    //몸
    List<SpriteRenderer> li_BodySprite = new List<SpriteRenderer>();
    public SpriteRenderer sp_Hair;
    public SpriteRenderer sp_Face;
    //시야범위
    public GameObject go_Touch;

    public GameObject go_Chick;

    //UI
    public TopStatUI topStatUI;
    public UISprite sp_Charge;
    public NoticeSay noticeSay;
    public LevelUpEffect levelUpEffect;

    public List<Weapon> li_Weapon = new List<Weapon>();

    //테이블
    Table_Manager TM;
    Sprite_Manager SM;

    //기모으는 스킬들 시간 변수
    public float chargeStartTime = 0;
    public float chargeEndTime = 0;
    //방어중 판정
    bool isGuardMode = false;
    bool isDodgeReady = true;
    float DodgeCoolTime = 0;

    bool isBowDodgeReady = true;


    public GameObject go_BashQuakeEffect;



    // Use this for initialization
    public void Init ()
    {
        TM = Table_Manager.Get;
        SM = Sprite_Manager.Get;

        SetUpdateDictionary();
        hitBox.Init(this);

        for (int i=0; i< li_Weapon.Count; i++)
        {
            li_Weapon[i].Init();
        }

        li_BodySprite.Clear();
        dic_Equipment.Clear();
        //스프라이트 찾아서 넣기
        //이름으로 나눠서 넣기
        SpriteRenderer[] ar_Sprite = RotationPivot.GetComponentsInChildren<SpriteRenderer>();
        for(int i=0; i< ar_Sprite.Length; i++)
        {
            if(ar_Sprite[i].tag == "BodySprite")
            {
                li_BodySprite.Add(ar_Sprite[i]);
            }
            else if(ar_Sprite[i].tag == "Equip")
            {
                ar_Sprite[i].gameObject.SetActive(false);
                dic_Equipment.Add(ar_Sprite[i].name, ar_Sprite[i]);
            }
        }
        
        InvokeRepeating("RefreshSTA", 0, 0.5f);

        LoadPlayerLocalData();
    }
    void SetUpdateDictionary()
    {
        dic_Update.Clear();

        Dictionary<CharacterState, StateFunction> temp = new Dictionary<CharacterState, StateFunction>();
        temp.Add(CharacterState.Idle, NoWeapon_Idle);
        temp.Add(CharacterState.Move, NoWeapon_Move);
        dic_Update.Add(WeaponType.NoWeapon, temp);


        temp = new Dictionary<CharacterState, StateFunction>();
        temp.Add(CharacterState.Idle, OneHand_Idle);
        temp.Add(CharacterState.Move, OneHand_Move);
        temp.Add(CharacterState.Attack, OneHand_Attack);
        temp.Add(CharacterState.Skill_1, OneHand_Skill_1);
        temp.Add(CharacterState.Skill_2, OneHand_Skill_2);
        temp.Add(CharacterState.Skill_3, OneHand_Skill_3);
        temp.Add(CharacterState.Dodge, OneHand_Dodge);
        dic_Update.Add(WeaponType.OneHand, temp);

        temp = new Dictionary<CharacterState, StateFunction>();
        temp.Add(CharacterState.Idle,    TwoHand_Idle);
        temp.Add(CharacterState.Move,    TwoHand_Move);
        temp.Add(CharacterState.Attack,  TwoHand_Attack);
        temp.Add(CharacterState.Skill_1, TwoHand_Skill_1);
        temp.Add(CharacterState.Skill_2, TwoHand_Skill_2);
        temp.Add(CharacterState.Skill_3, TwoHand_Skill_3);
        temp.Add(CharacterState.Dodge, TwoHand_Dodge);
        dic_Update.Add(WeaponType.TwoHand, temp);

        temp = new Dictionary<CharacterState, StateFunction>();
        temp.Add(CharacterState.Idle,    Bow_Idle);
        temp.Add(CharacterState.Move,    Bow_Move);
        temp.Add(CharacterState.Attack,  Bow_Attack);
        temp.Add(CharacterState.Skill_1, Bow_Skill_1);
        temp.Add(CharacterState.Skill_2, Bow_Skill_2);
        temp.Add(CharacterState.Skill_3, Bow_Skill_3);
        temp.Add(CharacterState.Dodge, Bow_Dodge);
        dic_Update.Add(WeaponType.Bow, temp);
    }

    public void LoadPlayerLocalData()
    {
        Level = PlayerPrefs.GetInt("Character_Level", 1);
        EXP = PlayerPrefs.GetInt("Character_EXP", 0);
        SkinIndex = PlayerPrefs.GetInt("Character_SkinIndex", 300000);
        FaceIndex = PlayerPrefs.GetInt("Character_FaceIndex", 310000);
        HairIndex = PlayerPrefs.GetInt("Character_HairIndex", 320000);
        HairColorIndex = PlayerPrefs.GetInt("Character_HairColorIndex", 450000);
        CampLevel = PlayerPrefs.GetInt("Character_CampLevel", 0);
        gold = PlayerPrefs.GetInt("Character_gold", 0);
        dia = PlayerPrefs.GetInt("Character_dia", 0);
        QuestID = PlayerPrefs.GetInt("Character_QuestID", 0);
        QuestCount = PlayerPrefs.GetInt("Character_QuestCount", 0);
        stageLevel = PlayerPrefs.GetInt("Character_stageLevel", 1);

        Visible_Helm = PlayerPrefs.GetInt("Character_Visible_Helm", 1);
        Visible_Chick = PlayerPrefs.GetInt("Character_Visible_Chick", 0);
        SetVisible_Helm();
        SetVisible_Chick();

        AD_Count = PlayerPrefs.GetInt("Character_AD_Count", 0);

        li_Item.Clear();
        //인벤토리 로드
        int Item_MAX = PlayerPrefs.GetInt("Character_Item_Count", 0);
        for (int i = 0; i < Item_MAX; i++)
        {
            itemSlotData temp = new itemSlotData
            {
                Key = PlayerPrefs.GetString(string.Format("Character_item_Key_{0}", i)),
                count = PlayerPrefs.GetInt(string.Format("Character_item_Count_{0}", i))
            };
            temp.SetTableData();
            li_Item.Add(temp);
        }

        //
        li_Storage.Clear();
        int Storage_MAX = PlayerPrefs.GetInt("Character_Storage_Count", 0);
        for (int i = 0; i < Storage_MAX; i++)
        {
            itemSlotData temp = new itemSlotData
            {
                Key = PlayerPrefs.GetString(string.Format("Character_Storage_Key_{0}", i)),
                count = PlayerPrefs.GetInt(string.Format("Character_Storage_Count_{0}", i))
            };
            temp.SetTableData();
            li_Storage.Add(temp);
        }

        //커마
        li_Costumizing.Clear();
        int Costumizing_MAX =  PlayerPrefs.GetInt("Character_Costumizing_Count", 0);
        for (int i = 0; i < Costumizing_MAX; i++)
        {
            int temp = PlayerPrefs.GetInt(string.Format("Character_Costumizing_{0}", i));
            li_Costumizing.Add(temp);
        }

        //Set Player
        MaxInventory = Table_Manager.Get.li_CampData[CampLevel].invenSize;
        MaxStorage = Table_Manager.Get.li_CampData[CampLevel].storeSize;

        Popup_Manager.Get.Ingame.SetInventory(MaxInventory);
        Popup_Manager.Get.Ingame.SetStorage(MaxStorage);

        ChangeFace(FaceType.Idle, FaceIndex);
        ChangeSkin(SkinIndex);
        ChangeHair(HairIndex);
        ChangeHairColor(HairColorIndex);
        AddAllCampLevelCustomizing();

        Map_Manager.Get.camp.SetCampObject();

        //기본 캐릭터 설정 셋팅
        tableData = Table_Manager.Get.li_CharacterData[Level];
        HP = tableData.maxHp;
        STA = tableData.maxSta;
        isLive = true;
        isInvincibility = false;

        //만약 무기가 한개도 없다면 기본 무기 넣어줌
        int wc = 0;
        for(int i=0; i<li_Item.Count; i++)
        {
            if (li_Item[i].tableData.itemType == ItemType.Weapon)
                wc++;
        }
        if(wc == 0)
        {
            AddItem(200000);
            AddItem(210000);
            AddItem(220000);
            AddItem(100200);
            AddItem(130200);
        }

        //무기 해제
        //장비칸 다 비워줌
        Popup_Manager.Get.Ingame.ResetStatusUI();

        for (int i = 0; i < li_Weapon.Count; i++)
        {
            li_Weapon[i].Hide();
        }
        ChangeWeaponAnimation(WeaponType.NoWeapon);
        equip_Weapon = null;
        IngameUI_Manager.Get.SetButtonSprite(null);

        //장비 해제
        foreach (var item in dic_Equipment)
        {
            item.Value.gameObject.SetActive(false);
        }
        equip_Armor = null;
        equip_Arm = null;
        equip_Hand = null;
        equip_Leg = null;
        equip_Foot = null;
        equip_Helm = null;

        //착용중인 장비 장착 
        for(int i=0; i<li_Item.Count; i++)
        {
            if(li_Item[i].count == -1)
            {
                if (li_Item[i].tableData.itemType == ItemType.Weapon)
                    ChangeWeapon(li_Item[i]);
                else if (li_Item[i].tableData.itemType == ItemType.Equipment)
                    ChangeArmor(li_Item[i]);
                else
                    Debug.LogError("잘못된 아이템 - 장비 착용");
            }
        }

        //시야범위 설정
        SetTouchSize();

        //UI 초기 설정셋팅
        IngameUI_Manager.Get.SetMapObjectType(Popup_Ingame.MapObjectType.None);
        IngameUI_Manager.Get.SetAutoQuickSlot_Init();

        //UI 갱신
        topStatUI.RefreshQuest();
        RefreshStat();
        RefreshPlates();
    }

    public void SavePlayerLocalData()
    {
        if (topStatUI.IsSaving() == true)
            return;

        Debug.Log("세이브 시작!!");
        topStatUI.ShowSave();

        PlayerPrefs.SetInt("Character_Level", Level);
        PlayerPrefs.SetInt("Character_EXP", EXP);
        PlayerPrefs.SetInt("Character_SkinIndex", SkinIndex);
        PlayerPrefs.SetInt("Character_FaceIndex", FaceIndex);
        PlayerPrefs.SetInt("Character_HairIndex", HairIndex);
        PlayerPrefs.SetInt("Character_HairColorIndex", HairColorIndex);
        PlayerPrefs.SetInt("Character_CampLevel", CampLevel);
        PlayerPrefs.SetInt("Character_gold", gold);
        PlayerPrefs.SetInt("Character_dia", dia);
        PlayerPrefs.SetInt("Character_QuestID", QuestID);
        PlayerPrefs.SetInt("Character_QuestCount", QuestCount);
        PlayerPrefs.SetInt("Character_stageLevel", stageLevel);

        PlayerPrefs.SetInt("Character_AD_Count", AD_Count);

        //인벤토리 저장
        PlayerPrefs.SetInt("Character_Item_Count", li_Item.Count);
        for (int i = 0; i < li_Item.Count; i++)
        {
            PlayerPrefs.SetString(string.Format("Character_item_Key_{0}", i), li_Item[i].Key);
            PlayerPrefs.SetInt(string.Format("Character_item_Count_{0}", i), li_Item[i].count);
        }
        //창고
        PlayerPrefs.SetInt("Character_Storage_Count", li_Storage.Count);
        for (int i = 0; i < li_Storage.Count; i++)
        {
            PlayerPrefs.SetString(string.Format("Character_Storage_Key_{0}", i), li_Storage[i].Key);
            PlayerPrefs.SetInt(string.Format("Character_Storage_Count_{0}", i), li_Storage[i].count);
        }
        //커마
        PlayerPrefs.SetInt("Character_Costumizing_Count", li_Costumizing.Count);
        for (int i = 0; i < li_Costumizing.Count; i++)
        {
            PlayerPrefs.SetInt(string.Format("Character_Costumizing_{0}", i), li_Costumizing[i]);
        }
        
    }

    void RefreshStat()
    {
        tableData = Table_Manager.Get.li_CharacterData[Level];

        ATK = AdditionalATK();
        DEF = AdditionalDEF();

        topStatUI.SetStat(ATK, DEF, tableData.maxHp ,equip_Weapon);
        topStatUI.SetLevel(Level);
        topStatUI.SetEXP(EXP, tableData.maxExp);
    }

    void RefreshSTA()
    {
        if (isLive == false)
            return;

        //스테미너 재생
        if(tableData.maxSta > STA)
        {
            STA+=4;
            if (STA > tableData.maxSta)
                STA = tableData.maxSta;

            RefreshPlates();
        }
    }
    void RefreshPlates()
    {
        topStatUI.SetHP(HP, tableData.maxHp);
        topStatUI.SetSTA(STA, tableData.maxSta);
        SceneEffect.Get.SetBlood(((float)(tableData.maxHp - HP) / tableData.maxHp));
    }

    // Update is called once per frame
    void Update ()
    {
        if (isLive == false ||
            Popup_Manager.Get.isEmptyList() == false ||
            currentState == CharacterState.Sleep)
            return;

        //현제 무기/상태에 따른 업데이트
        dic_Update[currentWeaponType][currentState]();
    }

    //////////////////////////////////////////
    //장비 관련
    //////////////////////////////////////////
    #region 장비 관련
    //장비 변경
    public void UnEquipArmor(itemSlotData _data)
    {
        //해당 장비 벗기
        _data.count = 0;

        //테이블에서 해당 장비에 관한 정보 찾기
        Table_Manager.EquipmentData find = Table_Manager.Get.li_Equipment.Find(r => r.id == _data.tableData.id);
        if (find == null)
        {
            Debug.LogError("없는 아이템");
            return;
        }

        switch (find.equipType)
        {
            case EquipType.Armor:
                equip_Armor = null;
                dic_Equipment["Equip_Armor"].gameObject.SetActive(false);
                break;
            case EquipType.Arm:
                equip_Arm = null;
                dic_Equipment["Equip_LeftArm"].gameObject.SetActive(false);
                dic_Equipment["Equip_RightArm"].gameObject.SetActive(false);
                break;
            case EquipType.Hand:
                equip_Hand = null;
                dic_Equipment["Equip_LeftHand"].gameObject.SetActive(false);
                dic_Equipment["Equip_RightHand"].gameObject.SetActive(false);
                break;
            case EquipType.Leg:
                equip_Leg = null;
                dic_Equipment["Equip_LeftLeg"].gameObject.SetActive(false);
                dic_Equipment["Equip_RightLeg"].gameObject.SetActive(false);
                break;
            case EquipType.Foot:
                equip_Foot = null;
                dic_Equipment["Equip_LeftFoot"].gameObject.SetActive(false);
                dic_Equipment["Equip_RightFoot"].gameObject.SetActive(false);
                break;
            case EquipType.Helm:
                equip_Helm = null;
                dic_Equipment["Equip_Helm"].gameObject.SetActive(false);
                sp_Hair.maskInteraction = SpriteMaskInteraction.None;
                break;
            default:
                Debug.Log("잘못된 타입 값");
                break;
        }

        Popup_Manager.Get.Ingame.SetEquipIcon(find.equipType);
        RefreshStat();
    }
    public void ChangeArmor(itemSlotData _data , bool UnEquipCurrent = true)
    {
        if (UnEquipCurrent == true)
        {
            //장비 장착 
            //기존에 차고있던 같은 부위 아이템 착용 해제
            List<itemSlotData> li_find = li_Item.FindAll(r => r.count == -1);
            for (int i = 0; i < li_find.Count; i++)
            {
                //현제 선택한 아이템과 같은 부위의 장비 찾기
                if (_data.tableData.id / 10000 == li_find[i].tableData.id / 10000)
                {
                    //장착 해제
                    li_find[i].count = 0;
                    break;
                }
            }
        }

        //선택한 아이템 장착
        _data.count = -1;

        Table_Manager.EquipmentData temp = Table_Manager.Get.li_Equipment.Find(r => r.id == _data.tableData.id);
        if (temp == null)
            return;
        
        EquipType findType = temp.equipType;
        switch (findType)
        {
            case EquipType.Armor:
                equip_Armor = temp;
                dic_Equipment["Equip_Armor"].gameObject.SetActive(true);
                dic_Equipment["Equip_Armor"].sprite = SM.GetSprite_Equip(temp.spriteName);
                break;
            case EquipType.Arm:
                equip_Arm = temp;
                dic_Equipment["Equip_LeftArm"].gameObject.SetActive(true);
                dic_Equipment["Equip_RightArm"].gameObject.SetActive(true);
                dic_Equipment["Equip_LeftArm"].sprite = SM.GetSprite_Equip(temp.spriteName);
                dic_Equipment["Equip_RightArm"].sprite = SM.GetSprite_Equip(temp.spriteName);
                break;
            case EquipType.Hand:
                equip_Hand = temp;
                dic_Equipment["Equip_LeftHand"].gameObject.SetActive(true);
                dic_Equipment["Equip_RightHand"].gameObject.SetActive(true);
                dic_Equipment["Equip_LeftHand"].sprite = SM.GetSprite_Equip(temp.spriteName);
                dic_Equipment["Equip_RightHand"].sprite = SM.GetSprite_Equip(temp.spriteName);
                break;
            case EquipType.Leg:
                equip_Leg = temp;
                dic_Equipment["Equip_LeftLeg"].gameObject.SetActive(true);
                dic_Equipment["Equip_RightLeg"].gameObject.SetActive(true);
                dic_Equipment["Equip_LeftLeg"].sprite = SM.GetSprite_Equip(temp.spriteName);
                dic_Equipment["Equip_RightLeg"].sprite = SM.GetSprite_Equip(temp.spriteName);
                break;
            case EquipType.Foot:
                equip_Foot = temp;
                dic_Equipment["Equip_LeftFoot"].gameObject.SetActive(true);
                dic_Equipment["Equip_RightFoot"].gameObject.SetActive(true);
                dic_Equipment["Equip_LeftFoot"].sprite = SM.GetSprite_Equip(temp.spriteName);
                dic_Equipment["Equip_RightFoot"].sprite = SM.GetSprite_Equip(temp.spriteName);
                break;
            case EquipType.Helm:
                equip_Helm = temp;

                if(Visible_Helm == 1)
                {
                    dic_Equipment["Equip_Helm"].gameObject.SetActive(true);
                    dic_Equipment["Equip_Helm"].sprite = SM.GetSprite_Equip(temp.spriteName);

                    Helm_Mask.sprite = dic_Equipment["Equip_Helm"].sprite;
                    sp_Hair.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
                }
                else
                {
                    dic_Equipment["Equip_Helm"].gameObject.SetActive(false);
                    sp_Hair.maskInteraction = SpriteMaskInteraction.None;

                    dic_Equipment["Equip_Helm"].sprite = SM.GetSprite_Equip(temp.spriteName);
                    Helm_Mask.sprite = dic_Equipment["Equip_Helm"].sprite;
                }

                break;
            default:
                Debug.Log("잘못된 타입 값");
                break;
        }

        Popup_Manager.Get.Ingame.SetEquipIcon(findType, temp.iconName , temp.grade);
        RefreshStat();
    }

    public void ChangeVisible_Helm()
    {
        if(Visible_Helm == 0)
            Visible_Helm = 1;
        else
            Visible_Helm = 0;

        PlayerPrefs.SetInt("Character_Visible_Helm", Visible_Helm);
        SetVisible_Helm();
    }
    
    public void SetVisible_Helm()
    {
        if (Visible_Helm == 1)
        {
            if (equip_Helm != null)
            {
                dic_Equipment["Equip_Helm"].gameObject.SetActive(true);
                sp_Hair.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
            }
        }
        else
        {
            dic_Equipment["Equip_Helm"].gameObject.SetActive(false);
            sp_Hair.maskInteraction = SpriteMaskInteraction.None;
        }

    }
    public void ChangeVisible_Chick()
    {
        if (Visible_Chick == 0)
            Visible_Chick = 1;
        else
            Visible_Chick = 0;

        PlayerPrefs.SetInt("Character_Visible_Chick", Visible_Chick);
        SetVisible_Chick();
    }
    public void SetVisible_Chick()
    {
        go_Chick.SetActive(Visible_Chick == 1);
    }

    //무기 변경
    public void UnEquipWeapon(itemSlotData _data)
    {
        _data.count = 0;

        ChangeWeaponAnimation(WeaponType.NoWeapon);
        for (int i = 0; i < li_Weapon.Count; i++)
        {
            li_Weapon[i].Hide();
        }
        equip_Weapon = null;
        Popup_Manager.Get.Ingame.SetWeaponIcon();
        RefreshStat();
        IngameUI_Manager.Get.SetButtonSprite(null);

    }
    public void ChangeWeapon(itemSlotData _data , bool UnEquipCurrent = true)
    {
        if (this.currentState == CharacterState.Die)
            return;

        if(UnEquipCurrent == true)
        {
            //기존에 차고있던 아이템 찾기
            List<itemSlotData> li_find = li_Item.FindAll(r => r.count == -1);
            for (int i = 0; i < li_find.Count; i++)
            {
                //현제 선택한 아이템과 같은 부위의 장비 찾기
                if (_data.tableData.id / 100000 == li_find[i].tableData.id / 100000)
                {
                    li_find[i].count = 0;
                    break;
                }
            }
        }

        _data.count = -1;

        //현제 무기 타입 찾기
        Table_Manager.WeaponData find = Table_Manager.Get.li_Weapon.Find(r => r.id == _data.tableData.id);
        if(find == null)
        {
            Debug.LogError("없는 무기");
            return;
        }

        for (int i = 0; i < li_Weapon.Count; i++)
        {
            li_Weapon[i].Hide();
        }

        ChangeWeaponAnimation(find.weaponType);
        equip_Weapon = li_Weapon[(int)currentWeaponType];
        equip_Weapon.Show();
        equip_Weapon.ChangeWeapon(find);

        Popup_Manager.Get.Ingame.SetWeaponIcon(find.iconName , find.grade);
        RefreshStat();
        IngameUI_Manager.Get.SetButtonSprite(equip_Weapon.data);
    }
    #endregion

    //////////////////////////////////////////
    //인벤토리 관련 함수
    //////////////////////////////////////////
    #region 인벤토리
    public bool CheckAddItem(int _id, int _count)
    {
        Table_Manager.Item FindItem = Table_Manager.Get.FindItem(_id);
        if(FindItem == null)
        {
            Debug.Log("잘못된 아이템");
            return false;
        }

        switch (FindItem.itemType)
        {
            case ItemType.Equipment:
            case ItemType.Weapon:
                if (li_Item.Count + _count > MaxInventory)
                    return false;
                break;
            case ItemType.Dye:
            case ItemType.HP_Potion:
            case ItemType.STA_Potion:
            case ItemType.TeleportScroll:
            case ItemType.ETC:
                itemSlotData find = li_Item.Find(r => r.tableData.id == _id);
                if (find == null && li_Item.Count >= MaxInventory)
                    return false;
                break;
            default:
                break;
        }

        return true;
    }
    public bool CheckAddItem(int _count = 1)
    {
        if (li_Item.Count + _count > MaxInventory)
            return false;

        return true;
    }
    public bool AddItem(int _id,int _count = 1)
    {
        itemSlotData temp;
        Table_Manager.Item findType = Table_Manager.Get.FindItem(_id);

        if (CheckAddItem(_id, _count) == false)
        {
            noticeSay.Show(LM.DATA[12]);
            return false;
        }

        /////////////////////////////////////////////////////////////////////////////////
        //아이템 데이터 테이블에서 찾기
        switch (findType.itemType)
        {
            case ItemType.Equipment:
            case ItemType.Weapon:
                for (int i=0; i<_count; i++)
                {
                    temp = MakeItem(_id);
                    li_Item.Add(temp);
                }
                break;
            case ItemType.Dye:
            case ItemType.HP_Potion:
            case ItemType.STA_Potion:
            case ItemType.TeleportScroll:
            case ItemType.ETC:
                itemSlotData find = li_Item.Find(r => r.tableData.id == _id);
                if (find != null)
                {
                    find.count += _count;
                }
                else
                {
                    temp = MakeItem(_id);
                    temp.count = _count;
                    li_Item.Add(temp);
                }
                IngameUI_Manager.Get.SetAutoQuickSlot(findType.itemType);
                break;
            default:
                break;
        }

        return true;
    }
        
    /// <summary>
    /// 인벤토리에 들어갈 아이템에 식별값을 넣어서 만든다
    /// </summary>
    /// <param name="_id">아이템 테이블 id</param>
    /// <param name="_count">장비는 0 ,잡템이나 물약등은 갯수 1개부터 시작</param>
    /// <returns></returns>
    public itemSlotData MakeItem(int _id)
    {
        Table_Manager.Item FindTable = Table_Manager.Get.FindItem(_id);
        if (FindTable == null)
        {
            Debug.LogError("존재 하지 않는 테이블 값");
            return null;
        }

        //고유값 하나 만들어서 넣기
        itemSlotData temp = new itemSlotData();
        int rnd;
        while (true)
        {
            rnd = Random.Range(0, 100000);
            temp.Key = string.Format("{0}_{1}", _id, rnd);

            //동일한 아이템 번호가 없을때 까지 rnd
            if (li_Item.Find(r => r.Key == temp.Key) == null)
            {
                break;
            }
        }

        temp.count = 0;
        temp.tableData = FindTable;

        return temp;
    }

    public void MoveItem_Inventory(itemSlotData _data)
    {
        //창고 -> 인벤
        if (CheckAddItem(_data.tableData.id , 1) == false)
        {
            Popup_Manager.Get.Warning.Show(LM.DATA[12]);
            return;
        }

        switch (_data.tableData.itemType)
        {
            case ItemType.Equipment:
            case ItemType.Weapon:
                li_Item.Add(_data);
                li_Storage.Remove(_data);
                break;
            case ItemType.Dye:
            case ItemType.HP_Potion:
            case ItemType.STA_Potion:
            case ItemType.TeleportScroll:
            case ItemType.ETC:
                itemSlotData find = li_Item.Find(r => r.tableData.id == _data.tableData.id);
                if (find != null)
                {
                    find.count += _data.count;
                    li_Storage.Remove(_data);
                }
                else
                {
                    li_Item.Add(_data);
                    li_Storage.Remove(_data);
                }
                IngameUI_Manager.Get.SetAutoQuickSlot(_data.tableData.itemType);
                break;
            default:
                break;
        }
        Sound_Manager.Get.PlayEffect("Effect_itemmove");
        Popup_Manager.Get.Ingame.Refresh();
    }

    public bool CheckAddStore(Table_Manager.Item _data, int _count)
    {
        switch (_data.itemType)
        {
            case ItemType.Equipment:
            case ItemType.Weapon:
                if (li_Storage.Count >= MaxStorage)
                    return false;
                break;
            case ItemType.Dye:
            case ItemType.HP_Potion:
            case ItemType.STA_Potion:
            case ItemType.TeleportScroll:
            case ItemType.ETC:
                itemSlotData find = li_Storage.Find(r => r.tableData.id == _data.id);
                if (find == null && li_Storage.Count >= MaxStorage)
                    return false;

                break;
            default:
                break;
        }

        return true;
    }
    public void MoveItem_Storage(itemSlotData _data)
    {
        if (CheckAddStore(_data.tableData, _data.count) == false)
        {
            Popup_Manager.Get.Warning.Show(LM.DATA[13]);
            return;
        }

        switch (_data.tableData.itemType)
        {
            case ItemType.Equipment:
            case ItemType.Weapon:
                li_Storage.Add(_data);
                li_Item.Remove(_data);
                break;
            case ItemType.Dye:
            case ItemType.HP_Potion:
            case ItemType.STA_Potion:
            case ItemType.TeleportScroll:
            case ItemType.ETC:
                itemSlotData find = li_Storage.Find(r => r.tableData.id == _data.tableData.id);
                if (find != null)
                {
                    find.count += _data.count;
                    li_Item.Remove(_data);
                }
                else
                {
                    li_Storage.Add(_data);
                    li_Item.Remove(_data);
                }
                //제거하는 아이템이 동일한 아이템이면 퀵슬롯에서 제거 
                IngameUI_Manager.Get.CheckQuickSlot(_data);
                break;
            default:
                break;
        }

        Sound_Manager.Get.PlayEffect("Effect_itemmove");
    }
    public void Sorting_Storage()
    {
        li_Storage.Sort(delegate(itemSlotData A, itemSlotData B)
        {
            if (A.tableData.id > B.tableData.id) return 1;
            else if(A.tableData.id < B.tableData.id) return -1;
            else return 0;
        }
        );
    }
    public void Sorting_Inventory()
    {
        li_Item.Sort(delegate (itemSlotData A, itemSlotData B)
        {
            //장비 아이템일때
            if(A.tableData.itemType == ItemType.Equipment ||
               A.tableData.itemType == ItemType.Weapon)
            {
                if (A.count > B.count) return 1;
                else if (A.count < B.count) return -1;
                else
                {
                    if (A.tableData.id > B.tableData.id) return 1;
                    else if (A.tableData.id < B.tableData.id) return -1;
                    else return 0;
                }
            }
            else
            {
                if (A.tableData.id > B.tableData.id) return 1;
                else if (A.tableData.id < B.tableData.id) return -1;
                else return 0;
            }
        }
       );
    }

    public List<itemSlotData> CheckHaveItem_Inventory(ItemType _Type)
    {
        List<itemSlotData> Find = li_Item.FindAll(r => r.tableData.itemType == _Type);
        return Find;
    }
    public bool CheckHaveItem_Inventory(int _id, int _count = 0)
    {
        itemSlotData Find = li_Item.Find(r => r.tableData.id == _id);
        if (Find == null)
            return false;

        //갯수를 검사해야 할때 , 소지한 갯수가 _count 보다 작으면 부족한것으로 false 리턴
        if(_count != 0 && Find.count < _count)
           return false;

        return true;
    }
    public void Remove_Item(itemSlotData _data , bool isStorage = false)
    {
        //제거하는 아이템이 동일한 아이템이면 퀵슬롯에서 제거 
        IngameUI_Manager.Get.CheckQuickSlot(_data);

        if (isStorage == true)
            li_Storage.Remove(_data);
        else
            li_Item.Remove(_data);

        Popup_Manager.Get.Ingame.Refresh();
    }
    public void Remove_Item(itemSlotData _data , int _count , bool isStorage = false)
    {
        if (_data == null)
        {
            Debug.LogError("뭐야 없는 아이템 지울려고 하네?");
            return;
        }

        //1보다 작으면 잡템이 아님
        if (_data.count < 1)
        {
            Debug.Log("지울려는 아이템이 갯수가 있는 아이템이 아님");
            return;
        }

        List<itemSlotData> li_temp = li_Item;
        if (isStorage == true)
            li_temp = li_Storage;

        //잡템은 갯수를 지울수도 있어야 한다
        //슬롯의 아이템이 지울려는 갯수보다 많아야함
        if (_data.count > _count)
        {
            _data.count -= _count;
        }
        else if (_data.count == _count)
        {
            li_temp.Remove(_data);
        }
        else
        {
            Debug.Log("소지갯수보다 많은 아이템을 지울려고 함!");
            return;
        }

        Popup_Manager.Get.Ingame.Refresh();
    }
    public void Sell_Item(itemSlotData _data, int _sellCount, bool _isStorage)
    {
        //제거하는 아이템이 동일한 아이템이면 퀵슬롯에서 제거 
        IngameUI_Manager.Get.CheckQuickSlot(_data);

        List<itemSlotData> li_temp = li_Item;
        if (_isStorage == true)
            li_temp = li_Storage;

        if (_sellCount > 0)
        {
            AddGold(_data.tableData.price * _sellCount);
            _data.count -= _sellCount;
            if (_data.count < 1)
            {
                li_temp.Remove(_data);
            }
        }
        else
        {
            AddGold(_data.tableData.price);
            //돈주기
            li_temp.Remove(_data);
        }

        Popup_Manager.Get.Ingame.Refresh();
    }
   
    public bool CheckHaveGold(int _gold)
    {
        if (gold < _gold)
        {
            return false;
        }

        return true;
    }
    public bool CheckAddGold(int _AddGold)
    {
        if (ConstValue.MaxGold < gold + _AddGold)
        {
            return false;
        }
        return true;
    }
    public bool AddGold(int _AddGold)
    {
        if (ConstValue.MaxGold < gold + _AddGold)
        {
            gold = ConstValue.MaxGold;
            Popup_Manager.Get.Warning.Show(LM.DATA[27]);
            return false;
        }

        gold += _AddGold;
        Popup_Manager.Get.Ingame.RefreshGold();
        return true;
    }
    public bool UseGold(int _UseGold)
    {
        if (CheckHaveGold(_UseGold) == false)
        {
            Popup_Manager.Get.Warning.Show(LM.DATA[31]);
            return false;
        }

        gold -= _UseGold;
        Popup_Manager.Get.Ingame.RefreshGold();
        return true;
    }

    public bool CheckHaveDia(int _dia)
    {
        if (dia < _dia)
        {
            return false;
        }

        return true;
    }
    public bool AddDia(int _AddDia)
    {
        dia += _AddDia;
        return true;
    }
    public bool UseDia(int _UseDia)
    {
        if (CheckHaveDia(_UseDia) == false)
        {
            Popup_Manager.Get.Warning.Show(LM.DATA[46]);
            return false;
        }

        dia -= _UseDia;
        return true;
    }
    
    public void UseItem(itemSlotData _data)
    {
        Table_Manager.UseItem Find;
        switch (_data.tableData.itemType)
        {
            case ItemType.Dye:
                Sound_Manager.Get.PlayEffect("Effect_dye");
                ChangeHairColor(_data.tableData.id);
                break;
            case ItemType.HP_Potion:
                //캐릭터 회복
                Find = Table_Manager.Get.li_UseItem.Find(r => r.id == _data.tableData.id);
                Heal_HP(int.Parse(Find.value));
                break;
            case ItemType.STA_Potion:
                //캐릭터 회복
                Find = Table_Manager.Get.li_UseItem.Find(r => r.id == _data.tableData.id);
                Heal_STA(int.Parse(Find.value));
                if(Map_Manager.Get.GetCurrentMapData().type == MapType.Defence)
                {
                    int temp = Mathf.RoundToInt(float.Parse(Find.value) / 10.0f);
                    Map_Manager.Get.defenceObj.Heal(temp);
                }
                break;
            case ItemType.TeleportScroll:
                break;
            default:
                break;
        }

        Remove_Item(_data, 1);
    }

    #endregion
    #region 연금술
    public List<int> AlchemyItem(int _TableIndex , int _count)
    {
        List<int> getItem = new List<int>();

        Table_Manager.AlchemyData data = Table_Manager.Get.li_Alchemy[_TableIndex];
        itemSlotData findSlot;
        //뽑기에 필요한 재료 차감
        for (int i = 0; i < data.li_Material_ID.Count; i++)
        {
            findSlot = li_Item.Find(r => r.tableData.id == data.li_Material_ID[i]);
            Remove_Item(findSlot, data.li_Material_Count[i] * _count);
        }

        for(int j=0; j<_count; j++)
        {
            //헤어 나올 확률 계산
            int find_id = -1;
            int Rnd = Random.Range(0, 10000);
            int ChanceValue = 0;

            int alchemyLevelChance = 300 * CampData().alchemyLevel;
            float fbonus = ((float)alchemyLevelChance / (data.li_GetItem_ID.Count - 1));
            int bonus = (int)Mathf.Round(fbonus);

            //머리 종류 만큼 돌기
            for (int i = 0; i < data.li_GetItem_ID.Count; i++)
            {
                if (i == 0)
                {
                    //첫아이템(제일 잘나오는템)의 확률이 감소한다
                    ChanceValue += data.li_GetItem_Chance[i] - alchemyLevelChance;
                }
                else
                {
                    //나머지 아이템은 첫아이템에서 깍인 수치를 균등하게 나눠서 보너스를 받는다
                    ChanceValue += data.li_GetItem_Chance[i] + bonus;
                }

                if (Rnd < ChanceValue)
                {
                    //id는 헤어 모양의 아이디 , 00형태로 바꿈
                    find_id = data.li_GetItem_ID[i];
                    break;
                }
            }
            if (find_id == -1)
            {
                Debug.LogError("테이블에 헤어 확률 합이 10000이 아닌듯");
                continue;
            }

            //염색약 인벤토리에 추가
            AddItem(find_id);
            getItem.Add(find_id);
        }

        Popup_Manager.Get.Ingame.Refresh();
        IngameUI_Manager.Get.SetAutoQuickSlot(ItemType.HP_Potion);
        IngameUI_Manager.Get.SetAutoQuickSlot(ItemType.STA_Potion);

        return getItem;
    }

    public bool AddCustomizing(int _id)
    {
        //기존에 없는 코스튬일 경우 추가
        if(CheckHaveCustomizing(_id) == false)
        {
            li_Costumizing.Add(_id);
            return true;
        }
        else
        {
            //이미 소유한 
            //Debug.Log("이미 소지한 코스튬");
            return false;
        }
    }
    public bool CheckHaveCustomizing(int _id)
    {
        //Find 써도 되는데 걍 써봄
        for (int i = 0; i < li_Costumizing.Count; i++)
        {
            if (_id == li_Costumizing[i])
            {
                return true;
            }
        }

        return false;
    }

    Table_Manager.CustomizingData FindFace;
    public void ChangeFace(FaceType _type , int _id = 0)
    {
        //얼굴 변경 요청이 들어옴
        if (_id != 0)
        {
            FaceIndex = _id;
            FindFace = Table_Manager.Get.li_CustomizingData.Find(r => r.id == _id);
            //sp_Face.color = FindFace.color; //얼굴은 색상 변경X
        }

        //현제 얼굴 번호(모양) + 표정
        sp_Face.sprite = SM.GetSprite_Character(string.Format("{0}_{1}", FindFace.SpriteName, _type));
    }
    public void ChangeSkin(int _id)
    {
        SkinIndex = _id;
        Table_Manager.CustomizingData Find = Table_Manager.Get.li_CustomizingData.Find(r => r.id == _id);
        
        for(int i=0; i< li_BodySprite.Count; i++)
        {
            li_BodySprite[i].color = Find.color;
        }   
    }
    public void ChangeHair(int _id)
    {
        HairIndex = _id;
        Table_Manager.CustomizingData Find = Table_Manager.Get.li_CustomizingData.Find(r => r.id == _id);
        sp_Hair.sprite = SM.GetSprite_Character(Find.SpriteName);

        if (dic_Equipment["Equip_Helm"].gameObject.activeSelf == false)
            sp_Hair.maskInteraction = SpriteMaskInteraction.None;
        else
            sp_Hair.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
    }
    public void ChangeHairColor(int _id)
    {
        HairColorIndex = _id;
        Table_Manager.UseItem Find = Table_Manager.Get.li_UseItem.Find(r => r.id == _id);
        sp_Hair.color = ColorPreset.HexToColor(Find.value);
    }

    /// <summary> 주둔지 레벨업 </summary>
    public void CampLevelUp()
    {
        CampLevel++;
        Table_Manager.CampData data = Table_Manager.Get.li_CampData[CampLevel];
        UseGold(data.needGold);
        //추가 외형
        AddCampLevelCustomizing();
        //주둔지 기능 열기- 창고사이즈 조정 포함
        StoreExtend_CampLevel();
        //시야 범위 조정
        SetTouchSize();

        Map_Manager.Get.camp.SetCampObject();
        Popup_Manager.Get.Ingame.Refresh();
        Popup_Manager.Get.CampLevelUp.Show();
        SavePlayerLocalData();
    }
    
    /// <summary> 현재 주둔지 레벨의 커스터마이징을 추가 </summary>
    void AddCampLevelCustomizing()
    {
        List<int> li_Data = Table_Manager.Get.li_CampData[CampLevel].li_AddCustomizing;
        if (Table_Manager.Get.li_CampData[CampLevel].li_AddCustomizing.Count == 0)
            return;
        for (int i=0; i< li_Data.Count; i++)
        {
            AddCustomizing(li_Data[i]);
        }
    }
    /// <summary> 현재 주둔지 레벨이하의 모든 커스터마이징을 추가 </summary>
    void AddAllCampLevelCustomizing()
    {
        List<int> li_Data;
        for (int i = 0; i < CampLevel+1; i++)
        {
            li_Data = Table_Manager.Get.li_CampData[i].li_AddCustomizing;
            if (li_Data.Count == 0)
                continue;
            for (int j=0; j< li_Data.Count; j++)
            {
                AddCustomizing(li_Data[j]);
            }
        }
    }
    /// <summary> 현재 주둔지 창고 갯수 설정  </summary>
    void StoreExtend_CampLevel()
    {
        int addStore = Table_Manager.Get.li_CampData[CampLevel].storeSize - MaxStorage;
        MaxStorage   = Table_Manager.Get.li_CampData[CampLevel].storeSize;
        if (addStore > 0)
        {
            Popup_Manager.Get.Ingame.AddStorage(addStore);
        }

        int addInven = Table_Manager.Get.li_CampData[CampLevel].invenSize - MaxInventory;
        MaxInventory = Table_Manager.Get.li_CampData[CampLevel].invenSize;
        if (addInven > 0)
        {
            Popup_Manager.Get.Ingame.AddInventory(addInven);
        }
    }

    void SetTouchSize()
    {
        go_Touch.transform.localScale = new Vector3(CampData().sightLevel, CampData().sightLevel, 1);
    }
    #endregion

    //////////////////////////////////////////
    //전투 관련
    //////////////////////////////////////////
    #region 전투관련
    //추가 공격력 = 기본+장비(방어구)
    public int AdditionalATK()
    {
        int temp = tableData.ATK;
        temp += GetEquipmentATK(equip_Armor);
        temp += GetEquipmentATK(equip_Arm);
        temp += GetEquipmentATK(equip_Hand);
        temp += GetEquipmentATK(equip_Leg);
        temp += GetEquipmentATK(equip_Foot);
        temp += GetEquipmentATK(equip_Helm);
        return temp;
    }
    int GetEquipmentATK(Table_Manager.EquipmentData _data)
    {
        if (_data != null)
            return _data.ATK;

        return 0;
    }
    //추가 방어력 = 기본+장비(방어구)
    public int AdditionalDEF()
    {
        int temp = tableData.DEF;
        temp += GetEquipmentDEF(equip_Armor);
        temp += GetEquipmentDEF(equip_Arm);
        temp += GetEquipmentDEF(equip_Hand);
        temp += GetEquipmentDEF(equip_Leg);
        temp += GetEquipmentDEF(equip_Foot);
        temp += GetEquipmentDEF(equip_Helm);
        temp += GetEquipmentDEF(equip_Weapon);
        return temp;
    }
    int GetEquipmentDEF(Table_Manager.EquipmentData _data)
    {
        if (_data != null)
            return _data.DEF;

        return 0;
    }
    int GetEquipmentDEF(Weapon _data)
    {
        if (_data != null)
            return _data.data.DEF;

        return 0;
    }
    
    public void AddExp(int _exp)
    {
        if (Level >= Table_Manager.Get.li_CharacterData.Count - 1)
        {
            EXP = 0;
            Debug.Log("최고레벨 도달");
            return;
        }

        EXP += _exp;
        if (EXP >= tableData.maxExp)
        {
            EXP = EXP - tableData.maxExp;
            Level++;
            topStatUI.SetLevel(Level);
            tableData = Table_Manager.Get.li_CharacterData[Level];
            HP = tableData.maxHp;
            STA = tableData.maxSta;
            RefreshPlates();
            levelUpEffect.Show();

        }

        topStatUI.SetEXP(EXP, tableData.maxExp);
        RefreshStat();
    }
    public void GetDamaged(DamageData _Dmg, Transform _source)
    {
        if (isLive == false || currentState == CharacterState.Dodge || isInvincibility == true ||
            CheckCurrentWeaponSkill(WeaponType.Bow, CharacterState.Skill_2) == true)
            return;

        _Dmg.Type = DamagedType.Player;

        //한손검의 경우 방어확률을 가진다-등 뒤는 처맞음
        if(currentWeaponType == WeaponType.OneHand && (RotationPivot.rotation != _source.rotation))
        {
            int blockChance = 2000;
            switch (currentState)
            {
                case CharacterState.Attack:
                    blockChance = 3500;
                    break;
                case CharacterState.Skill_1:
                    blockChance = 2000;
                    break;
                case CharacterState.Skill_2:
                    if (isGuardMode == true)
                        blockChance = 10000;
                    else
                        blockChance = 2000;
                    break;
                case CharacterState.Skill_3:
                    blockChance = 7000;
                    break;
            }

            if (currentState == CharacterState.Skill_2 )
            {
                if(STA > 5)
                {
                    //방어 성공
                    if (Random.Range(0, 10000) < blockChance)
                    {
                        STA -= 5;
                        Sound_Manager.Get.PlayEffect("Effect_Block");
                        _Dmg.Dmg = -1;
                    }
                }
            }
            else
            {
                //방어 성공
                if (Random.Range(0, 10000) < blockChance)
                {
                    Sound_Manager.Get.PlayEffect("Effect_Block");
                    _Dmg.Dmg = -1;
                }
            }
           
        }
        
        //0은 miss -1은 block
        if(_Dmg.Dmg > 0)
        {
            if (_Dmg.Dmg <= 0)
            {
                _Dmg.Dmg = Random.Range(0, 2);
            }
            
            HP -= _Dmg.Dmg;
        }

        ObjectPool_Manager.Get.ShowDamage(dic_Equipment["Equip_Helm"].transform, _Dmg);

        if (HP <= 0)
            Death();
        else
            RefreshPlates();
    }
    public void GetDamaged(int _nDmg)
    {
        if (isLive == false || isInvincibility == true)
            return;

        DamageData _Dmg = new DamageData
        {
            Type = DamagedType.Terrain,
            isCritcal = false,
            Dmg = _nDmg
        };

        HP -= _Dmg.Dmg;
     
        ObjectPool_Manager.Get.ShowDamage(dic_Equipment["Equip_Helm"].transform, _Dmg);

        if (HP <= 0)
            Death();
        else
            RefreshPlates();
    }
    #endregion
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    
    //////////////////////////////////////////
    //기타 편의성 함수
    //////////////////////////////////////////
   

    //현재 캠프의 레벨 정보를 가져온다
    public Table_Manager.CampData CampData()
    {
        return Table_Manager.Get.li_CampData[CampLevel];
    }

    //현재 사용중인 무기스킬
    public bool CheckCurrentWeaponSkill(WeaponType _type , CharacterState _state )
    {
        if (_type == currentWeaponType && _state == currentState)
            return true;

        return false;
    }

    //캐릭터 맵 밖으로 못나가게 막는 함수
    public void SetCharacterPosition(int _dir)
    {
        this.transform.localPosition = new Vector3(_dir * ConstValue.pos_X, ConstValue.pos_Y, 0);
    }

    //캐릭터 특정 위치로 이동
    public void MoveCharacterPosition(int _dirX)
    {
        this.transform.localPosition = new Vector3(_dirX, ConstValue.pos_Y, 0);
    }

    //캐릭터 죽여버리기
    public void Death()
    {
        HP = 0;
        isLive = false;
        EndChargeTime();
        ChangeState(CharacterState.Die);
        Sound_Manager.Get.PlayBGM("Bgm_death");
        RefreshPlates();
    }

    //캐릭터 회복Resurrection
    public void Resirrection()
    {
        isLive = true;
        ChangeState(CharacterState.Idle);
        HP = tableData.maxHp;
        STA = tableData.maxSta;
        RefreshPlates();
    }

    //회복 
    public void Heal_Pecent(float _value)
    {
        float temp = tableData.maxHp * (_value / 100);
        HP += (int)temp;
        if (tableData.maxHp < HP)
            HP = tableData.maxHp;

        RefreshPlates();
    }

    //회복 
    public void Heal_HP(int _value, bool _isEffect = true)
    {
        HP += _value;
        if (tableData.maxHp < HP)
            HP = tableData.maxHp;

        if (_isEffect == true)
            ObjectPool_Manager.Get.ShowHeal(this.transform);

        RefreshPlates();
    }
    //스테미나 회복 
    public void Heal_STA(int _value, bool _isEffect = true)
    {
        STA += _value;
        if (tableData.maxSta < STA)
            STA = tableData.maxSta;

        if(_isEffect == true)
            ObjectPool_Manager.Get.ShowSTA(this.transform);

        RefreshPlates();
    }
    

    //캐릭터 일정시간 무적 만들기
    public void invincibility(float _time)
    {
        noticeSay.Show(LM.DATA[61], true, _time);
        noticeSay.ChangeColor(ColorPreset.HexToColor("5AEBFFFF"));
        StartCoroutine(Setinvincibility(_time));
    }
    IEnumerator Setinvincibility(float _time)
    {
        isInvincibility = true;
        yield return new WaitForSeconds(_time);
        isInvincibility = false;
    }

    //////////////////////////////////////////
    //퀘스트 관련 함수
    //////////////////////////////////////////
    public bool CheckQuestClear()
    {
        Table_Manager.QuestData Find = Table_Manager.Get.li_QuestData.Find(r => r.id == QuestID);
        if (Find == null)
        {
            Debug.LogError("찾는 퀘스트 없음");
            return false;
        }

        if (Find.count > QuestCount)
            return false;

        return true;
    }
    public void QuestClear()
    {
        if (CheckQuestClear() == false)
            return;

        Table_Manager.QuestData Find = Table_Manager.Get.li_QuestData.Find(r => r.id == QuestID);
        if (Find == null)
        {
            Debug.LogError("찾는 퀘스트 없음");
            return;
        }

        Sound_Manager.Get.PlayEffect("Effect_getgold");
        if (Find.priceType == PriceType.Gold)
        {
            topStatUI.ShowFlyingGoods(PriceType.Gold);
            AddGold(Find.price);
        }
        else if (Find.priceType == PriceType.Dia)
        {
            topStatUI.ShowFlyingGoods(PriceType.Dia);
            AddDia(Find.price);
        }


        //현재 퀘스트가 반복 퀘스트인가?
        if (Find.loop == true)
        {
            QuestID = -1;
            QuestCount = 0;
        }
        else
        {
            QuestID++;
            QuestCount = 0;

            //만약 해당하는 퀘스트가 없다면 다음은 반복퀘스트임<
            Find = Table_Manager.Get.li_QuestData.Find(r => r.id == QuestID);
            if (Find == null)
                QuestID = -1;
        }

        topStatUI.RefreshQuest(true);
    }

    public void GetLoopQuest()
    {
        List<Table_Manager.QuestData> LoopQuestList =
                 Table_Manager.Get.li_QuestData.FindAll(r => r.loop == true);
        int rnd = Random.Range(0, LoopQuestList.Count);
        QuestID = LoopQuestList[rnd].id;
        QuestCount = 0;

        PlayerPrefs.SetInt("Character_QuestID", QuestID);
        PlayerPrefs.SetInt("Character_QuestCount", QuestCount);
        topStatUI.RefreshQuest(true);
    }

    public void CheckKillQuestMonster(int _id)
    {
        if (QuestID == -1)
            return;

        Table_Manager.QuestData Find = Table_Manager.Get.li_QuestData.Find(r => r.id == QuestID);
        if (Find == null)
        {
            Debug.LogError("찾는 퀘스트 없음");
            return;
        }

        if (Find.targetID == _id && Find.count > QuestCount)
        {
            QuestCount++;
            topStatUI.RefreshQuest();
        }
    }



    //////////////////////////////////////////
    //캐릭터 애니,이동컨트롤 함수
    //////////////////////////////////////////
    #region 이동,애니관련

    //애니메이션에서 호출할 자식용 인터페이스 함수
    public void Attack(bool _isAttack)
    {
        //공격 끝났을때
        equip_Weapon.SetCollider(_isAttack);
    }
    public void SetTrail(bool _isOn)
    {
        if(equip_Weapon == true)
            equip_Weapon.SetTrailRender(_isOn);
    }
    public void SetChargeTime()
    {
        chargeStartTime = RealTime.time;
        sp_Charge.fillAmount = 0;
        sp_Charge.gameObject.SetActive(true);

        if (currentWeaponType == WeaponType.TwoHand)
        {
            Sound_Manager.Get.PlayEffect("Effect_TwoHand_Charge", true);
        }
        else if(currentWeaponType == WeaponType.Bow)
        {
            if (currentState == CharacterState.Attack)
            {
                Sound_Manager.Get.PlayEffect("Effect_Bow_Charge", true);
            }
            else if (currentState == CharacterState.Skill_1)
            {
                Sound_Manager.Get.PlayEffect("Effect_Bow_ChargeLong", true);
            }
        }
        else if(currentWeaponType == WeaponType.OneHand)
        {
        }
        //Debug.Log("차지 시작 : " + chargeStartTime);
    }
    public void EndChargeTime()
    {
        chargeEndTime = RealTime.time;
        sp_Charge.fillAmount = 0;
        sp_Charge.gameObject.SetActive(false);

        if (currentWeaponType == WeaponType.TwoHand)
        {
            Sound_Manager.Get.StopEffect("Effect_TwoHand_Charge");
            Sound_Manager.Get.PlayEffect("Effect_TwoHand_Swing");
        }
        else if (currentWeaponType == WeaponType.Bow)
        {
            if(currentState == CharacterState.Attack)
            {
                Sound_Manager.Get.StopEffect("Effect_Bow_Charge");
                Sound_Manager.Get.PlayEffect("Effect_Bow_Release");
            }
            else if (currentState == CharacterState.Skill_1)
            {
                Sound_Manager.Get.StopEffect("Effect_Bow_ChargeLong");
                Sound_Manager.Get.PlayEffect("Effect_Bow_ReleaseSkill");
            }
        }
        else if (currentWeaponType == WeaponType.OneHand)
        {
        }
        //Debug.Log("차지 끝 : " + chargeEndTime);
    }

    public void EndAnimation()
    {
        if(CheckCurrentWeaponSkill(WeaponType.Bow,CharacterState.Skill_2) == true)
        {
            isBowDodgeReady = false;
            IngameUI_Manager.Get.SetButtonTimer(IngameUI_Manager.PressButtonName.Skill_2, 1.5f);
            StartCoroutine(CheckBowDodgeTime(1.5f));
        }
        else if (currentState == CharacterState.Dodge)
        {
            isDodgeReady = false;
            DodgeCoolTime += TM.li_SkillData[(int)currentWeaponType].DodgeCost;
            IngameUI_Manager.Get.SetButtonTimer(IngameUI_Manager.PressButtonName.Dodge, DodgeCoolTime);
            StartCoroutine(CheckDodgeTime());
        }
        ChangeState(CharacterState.Idle);
    }

    IEnumerator CheckBowDodgeTime(float _time)
    {
        yield return new WaitForSeconds(_time);
        isBowDodgeReady = true;
    }

    IEnumerator CheckDodgeTime()
    {
        yield return new WaitForSeconds(DodgeCoolTime);
        isDodgeReady = true;
        DodgeCoolTime = 0;
    }

    void ChangeWeaponAnimation(WeaponType _type)
    {
        currentWeaponType = _type;
        string animName = string.Format("{0}_{1}", currentWeaponType, currentState);
        anim.Play(animName);

        switch (_type)
        {
            case WeaponType.OneHand:
                break;
            case WeaponType.TwoHand:
                break;
            case WeaponType.Bow:
                //Speed = ConstValue.NoWeaponSpeed;
                break;
            case WeaponType.NoWeapon:
                //Speed = ConstValue.NoWeaponSpeed;
                break;
            default:
                break;
        }
    }
    public void ChangeState(CharacterState _state , bool _CrossFade = false)
    {
        if(currentState == _state)
        {
            return;
        }

        //Debug.Log("상태 변경: " + currentState + " -> " + _state);
        currentState = _state;
        string animName = string.Format("{0}_{1}", currentWeaponType, currentState);
        switch (currentState)
        {
            case CharacterState.Idle:
            case CharacterState.Move:
                PlayAnimation(animName, _CrossFade);
                ChangeFace(FaceType.Idle);
                SetTrail(false);
                break;
            case CharacterState.Attack:
            case CharacterState.Skill_1:
            case CharacterState.Skill_2:
            case CharacterState.Skill_3:
                Sound_Manager.Get.PlayEffect(string.Format("Effect_{0}_{1}", currentWeaponType, currentState));
                PlayAnimation(animName, _CrossFade);
                SetTrail(true);
                break;
            case CharacterState.Die:
                isDodgeReady = true;
                PlayAnimation("Die", false);
                ChangeFace(FaceType.Surprise);
                SetTrail(false);
                break;
            case CharacterState.Sleep:
                animName = string.Format("{0}_{1}", currentWeaponType, CharacterState.Idle);
                PlayAnimation(animName, false);
                ChangeFace(FaceType.Sleep);
                SetTrail(false);
                break;
            case CharacterState.Dodge:
                Sound_Manager.Get.PlayEffect("Effect_dodge");
                PlayAnimation(animName, false);
                ChangeFace(FaceType.Idle);
                SetTrail(true);
                break;
            default:
                break;
        }
       
    }
    void PlayAnimation(string _animName ,bool _CrossFade)
    {
        if (_CrossFade == false)
            anim.Play(_animName);
        else
            anim.CrossFade(_animName);
    }

    public bool CheckKey_Attack()
    {
        if (IngameUI_Manager.Get.isPress(IngameUI_Manager.PressButtonName.Attack) == true || 
            Input.GetKey(KeyCode.Z) == true)
        {
            return true;
        }
        return false;
    }
    bool CheckKey_Skill_1()
    {
        if (IngameUI_Manager.Get.isPress(IngameUI_Manager.PressButtonName.Skill_1) == true || 
            Input.GetKey(KeyCode.X) == true)
        {
            return true;
        }
        return false;
    }
    bool CheckKey_Skill_2()
    {
        if (IngameUI_Manager.Get.isPress(IngameUI_Manager.PressButtonName.Skill_2)  == true || 
            Input.GetKey(KeyCode.C) == true)
        {
            return true;
        }
        return false;
    }
    bool CheckKey_Skill_3()
    {
        if (IngameUI_Manager.Get.isPress(IngameUI_Manager.PressButtonName.Skill_3) == true || 
            Input.GetKey(KeyCode.V) == true)
        {
            return true;
        }
        return false;
    }
    bool CheckKey_Move()
    {
        if (IngameUI_Manager.Get.joyStick.MoveJoystick() == ButtonDirection.RIGHT || Input.GetKey(KeyCode.RightArrow) == true ||
            IngameUI_Manager.Get.joyStick.MoveJoystick() == ButtonDirection.LEFT || Input.GetKey(KeyCode.LeftArrow) == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    bool CheckKey_Dodge()
    {
        if (IngameUI_Manager.Get.isPress(IngameUI_Manager.PressButtonName.Dodge) == true || 
            Input.GetKey(KeyCode.Space) == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool CheckUseDodge()
    {
        if (isDodgeReady == false)
            return false;

        //박스 컬라이더 켜져있는거 꺼줌
        Attack(false);
        RefreshPlates();
        ChangeState(CharacterState.Dodge);
        return true;
    }

    void CheckUseSkill(CharacterState _changeState,bool _isUseCharge = false)
    {
        int skillNum = (int)_changeState - (int)CharacterState.Skill_1;//스킬1번을 인덱스 0으로 만들기 위해서`~
        if (STA < TM.li_SkillData[(int)currentWeaponType].li_Cost[skillNum])
        {
            //Debug.Log("스테미나 부족으로 사용 불가 cost : " + TM.li_SkillData[(int)currentWeaponType].li_Cost[skillNum]);
            noticeSay.Show(LM.DATA[15]);
            return;
        }

        STA -= TM.li_SkillData[(int)currentWeaponType].li_Cost[skillNum];
        RefreshPlates();
        ChangeState(_changeState);
        if(_isUseCharge == true)
        {
            SetChargeTime();
        }
    }
    void Move()
    {
        if (IngameUI_Manager.Get.joyStick.MoveJoystick() == ButtonDirection.RIGHT || Input.GetKey(KeyCode.RightArrow) == true)
        {
            RotationPivot.localScale = Vector3.one;
            RotationPivot.localRotation = Quaternion.Euler(new Vector3(0, 180, 0));
            this.transform.Translate(Speed * Time.deltaTime, 0, 0);
        }
        else if (IngameUI_Manager.Get.joyStick.MoveJoystick() == ButtonDirection.LEFT || Input.GetKey(KeyCode.LeftArrow) == true)
        {
            RotationPivot.localScale = new Vector3(1, 1, -1);
            RotationPivot.localRotation = Quaternion.identity;
            this.transform.Translate(-Speed * Time.deltaTime, 0, 0);
        }
    }
    //무기 장착 해제 상태
    void NoWeapon_Idle()
    {
        if (CheckKey_Move() == true)
            ChangeState(CharacterState.Move);
    }
    void NoWeapon_Move()
    {
        if(CheckKey_Move() == true)
            Move();
        else
            ChangeState(CharacterState.Idle);
    }

    
    //무기 상태별 함수
    #region 한손검 업데이트
    void OneHand_Idle()
    {
        if (CheckKey_Move() == true)
            ChangeState(CharacterState.Move,true);
        else if(CheckKey_Attack() == true)
            ChangeState(CharacterState.Attack);
        else if (CheckKey_Skill_1() == true)
            CheckUseSkill(CharacterState.Skill_1);
        else if(CheckKey_Skill_2() == true)
            CheckUseSkill(CharacterState.Skill_2,true);
        else if(CheckKey_Skill_3() == true)
            CheckUseSkill(CharacterState.Skill_3);
        else if(CheckKey_Dodge() == true)
            CheckUseDodge();
    }
    void OneHand_Move()
    {
        if (CheckKey_Move() == true)
        {
            Move();

            if (CheckKey_Attack() == true)
                ChangeState(CharacterState.Attack);
            else if(CheckKey_Skill_1() == true)
                CheckUseSkill(CharacterState.Skill_1);
            else if(CheckKey_Skill_2() == true)
                CheckUseSkill(CharacterState.Skill_2, true);
            else if (CheckKey_Skill_3() == true)
                CheckUseSkill(CharacterState.Skill_3);
            else if (CheckKey_Dodge() == true)
                CheckUseDodge();
        }
        else
        {
            ChangeState(CharacterState.Idle, true);
        }
    }
    void OneHand_Dodge()
    {
        if (anim["OneHand_Dodge"].time < FrameToSecond(20))
            Move_AnimationUpdate(-1.0f);
        else
            Move_AnimationUpdate(-1.5f);
    }
    void OneHand_Attack()
    {
        Move();
    }
    void OneHand_Skill_1()
    {
        Move();
    }
    void OneHand_Skill_2()
    {
        sp_Charge.fillAmount = (RealTime.time - chargeStartTime) / 3;

        if (anim["OneHand_Skill_2"].time > FrameToSecond(30) && anim["OneHand_Skill_2"].time < FrameToSecond(120))
        {
            if(STA < 5 || CheckKey_Skill_2() == false)
            {
                isGuardMode = false;
                anim["OneHand_Skill_2"].time = FrameToSecond(120);
                EndChargeTime();
            }
            else if (CheckKey_Skill_2() == true)
            {
                isGuardMode = true;
                anim["OneHand_Skill_2"].time = FrameToSecond(60);
            }
        }
    }
     
    void OneHand_Skill_3()
    {
        if (anim["OneHand_Skill_3"].time > FrameToSecond(20) && anim["OneHand_Skill_3"].time < FrameToSecond(60))
        {
            Move_AnimationUpdate(1.6f);
        }
        else if (anim["OneHand_Skill_3"].time >= FrameToSecond(60) && anim["OneHand_Skill_3"].time < FrameToSecond(90))
        {
            Move_AnimationUpdate(1.0f);
        }

    }
    #endregion
    #region 양손검 업데이트
    void TwoHand_Idle()
    {
        if (CheckKey_Move() == true)
            ChangeState(CharacterState.Move, true);
        else if (CheckKey_Attack() == true)
            ChangeState(CharacterState.Attack);
        else if (CheckKey_Skill_1() == true)
            CheckUseSkill(CharacterState.Skill_1,true);
        else if (CheckKey_Skill_2() == true)
            CheckUseSkill(CharacterState.Skill_2);
        else if (CheckKey_Skill_3() == true)
            CheckUseSkill(CharacterState.Skill_3);
        else if (CheckKey_Dodge() == true)
            CheckUseDodge();
    }
    void TwoHand_Move()
    {
        if (CheckKey_Move() == true)
        {
            Move();

            if (CheckKey_Attack() == true)
                ChangeState(CharacterState.Attack);
            else if (CheckKey_Skill_1() == true)
                CheckUseSkill(CharacterState.Skill_1, true);
            else if (CheckKey_Skill_2() == true)
                CheckUseSkill(CharacterState.Skill_2);
            else if (CheckKey_Skill_3() == true)
                CheckUseSkill(CharacterState.Skill_3);
            else if (CheckKey_Dodge() == true)
                CheckUseDodge();
        }
        else
        {
            ChangeState(CharacterState.Idle, true);
        }
    }

    //회피
    void TwoHand_Dodge()
    {
        if (RotationPivot.rotation.eulerAngles.y == 180)
            this.transform.Translate((Speed * 1) * Time.deltaTime, 0, 0);
        else
            this.transform.Translate(-(Speed * 1) * Time.deltaTime, 0, 0);
    }

    void TwoHand_Attack()
    {
        if (anim["TwoHand_Attack"].time >= 0.5f)
        {
            if (CheckKey_Dodge() == true)
                CheckUseDodge();
        }

    }
    void TwoHand_Skill_1()
    {

        if (CheckKey_Dodge() == true)
        {
            if (CheckUseDodge() == true)
            {
                EndChargeTime();
                DodgeCoolTime += 1f;
            }
        }

        sp_Charge.fillAmount = (RealTime.time - chargeStartTime) / 4;

        if (anim["TwoHand_Skill_1"].time > FrameToSecond(30) && anim["TwoHand_Skill_1"].time < FrameToSecond(270))
        {
            if (CheckKey_Skill_1() == false)
            {
                anim["TwoHand_Skill_1"].time = FrameToSecond(273);
                EndChargeTime();
            }
        }

    }
    void TwoHand_Skill_2()
    {
        if (CheckKey_Dodge() == true)
        {
            if (CheckUseDodge() == true)
                DodgeCoolTime += 1f;
        }

        if (anim["TwoHand_Skill_2"].time < FrameToSecond(20))
        {
            Move_AnimationUpdate(2);
        }
        else if (anim["TwoHand_Skill_2"].time < FrameToSecond(30))
        {
        }
        else if (anim["TwoHand_Skill_2"].time < FrameToSecond(45))
        {
            Move_AnimationUpdate(1);
        }
    }
    void TwoHand_Skill_3()
    {
        if (anim["TwoHand_Skill_3"].time < FrameToSecond(45))
        {
            Move();
        }
        else
        {
            if (CheckKey_Dodge() == true)
            {
                if (CheckUseDodge() == true)
                    DodgeCoolTime += 1f;
            }
        }
    }
    #endregion
    #region 활 업데이트
    void Bow_Idle()
    {
        if (CheckKey_Move() == true)
            ChangeState(CharacterState.Move, true);
        else if (CheckKey_Attack() == true)
            ChangeState(CharacterState.Attack);
        else if (CheckKey_Skill_1() == true)
            CheckUseSkill(CharacterState.Skill_1);
        else if (CheckKey_Skill_2() == true && isBowDodgeReady == true)
            CheckUseSkill(CharacterState.Skill_2);
        else if (CheckKey_Skill_3() == true)
            CheckUseSkill(CharacterState.Skill_3);
        else if (CheckKey_Dodge() == true)
            CheckUseDodge();
    }
    void Bow_Move()
    {
        if (CheckKey_Move() == true)
        {
            Move();

            if (CheckKey_Attack() == true)
                ChangeState(CharacterState.Attack);
            else if (CheckKey_Skill_1() == true)
                CheckUseSkill(CharacterState.Skill_1);
            else if (CheckKey_Skill_2() == true && isBowDodgeReady == true)
                CheckUseSkill(CharacterState.Skill_2);
            else if (CheckKey_Skill_3() == true)
                CheckUseSkill(CharacterState.Skill_3);
            else if (CheckKey_Dodge() == true)
                CheckUseDodge();
        }
        else
        {
            ChangeState(CharacterState.Idle, true);
        }
    }
    //회피
    void Bow_Dodge()
    {
        if (RotationPivot.rotation.eulerAngles.y == 180)
            this.transform.Translate((Speed * 1.5f) * Time.deltaTime, 0, 0);
        else
            this.transform.Translate(-(Speed * 1.5f) * Time.deltaTime, 0, 0);
    }
    void Bow_Attack()
    {
        if (anim["Bow_Attack"].time >= FrameToSecond(10) && anim["Bow_Attack"].time < FrameToSecond(70))
        {
            sp_Charge.fillAmount = RealTime.time - chargeStartTime;
            //Debug.Log(RealTime.time - chargeStartTime);

            if (CheckKey_Attack() == false)
            {
                anim["Bow_Attack"].time = FrameToSecond(70);
            }
        }
        else if ( anim["Bow_Attack"].time >= FrameToSecond(85))
        {
            Move();

            if (CheckKey_Dodge() == true)
                CheckUseDodge();
        }
    }
    void Bow_Skill_1()
    {
        if (anim["Bow_Skill_1"].time >= FrameToSecond(20) && anim["Bow_Skill_1"].time < FrameToSecond(230))
        {
            sp_Charge.fillAmount = (RealTime.time - chargeStartTime) / 3.5f;

            if (CheckKey_Skill_1() == false)
            {
                anim["Bow_Skill_1"].time = FrameToSecond(230);
            }
        }
    }
    void Bow_Skill_2()
    {
        if (anim["Bow_Skill_2"].time <= FrameToSecond(55))
        {
            if (RotationPivot.rotation.eulerAngles.y == 180)
                this.transform.Translate(-Speed * Time.deltaTime, 0, 0);
            else
                this.transform.Translate(Speed * Time.deltaTime, 0, 0);
        }
        else if (anim["Bow_Skill_2"].time <= FrameToSecond(70))
        {
            if (RotationPivot.rotation.eulerAngles.y == 180)
                this.transform.Translate(-Speed * 0.75f * Time.deltaTime, 0, 0);
            else
                this.transform.Translate(Speed * 0.75f * Time.deltaTime, 0, 0);
        }
        else
        {

        }
    }
    void Bow_Skill_3()
    {
    }
    #endregion


    public void SetSkillObject()
    {
        switch (currentWeaponType)
        {
            case WeaponType.OneHand:
                break;
            case WeaponType.TwoHand:
                switch (currentState)
                {
                    case CharacterState.Skill_1:
                        float multiple = chargeEndTime - chargeStartTime;
                        if (multiple > 2f)
                        {
                            Instantiate(go_BashQuakeEffect, RotationPivot);
                        }
                        break;
                    case CharacterState.Skill_2:
                        break;
                    case CharacterState.Skill_3:
                        break;
                }
                break;
            case WeaponType.Bow:
                break;
            case WeaponType.NoWeapon:
                break;
            default:
                break;
        }
    }

#endregion
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////

    float FrameToSecond(float _fFrame)
    {
        return _fFrame / 60.0f;
    }

    void Move_AnimationUpdate(float _speed)
    {
        if (RotationPivot.rotation.eulerAngles.y == 180)
            this.transform.Translate((Speed * _speed) * Time.deltaTime, 0, 0);
        else
            this.transform.Translate(-(Speed * _speed) * Time.deltaTime, 0, 0);
    }
}
