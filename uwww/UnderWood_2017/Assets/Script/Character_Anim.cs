﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character_Anim : MonoBehaviour
{
    public Character parent;

    public void StartAttack()
    {
        parent.Attack(true);
    }
    public void EndAttack()
    {
        parent.Attack(false);
    }

    public void SetChargeTime()
    {
        parent.SetChargeTime();
    }
    public void EndChargeTime()
    {
        parent.EndChargeTime();
    }

    public void EndAnimation()
    {
        parent.EndAnimation();
    }

    public void DieEnd()
    {
        Popup_Manager.Get.Die.Show();
    }

    public void ChangeFace(FaceType _type)
    {
        parent.ChangeFace(_type);
    }

    public void SetSkillObject()
    {
        parent.SetSkillObject();
    }

    public void ShakeCamera()
    {
        Sound_Manager.Get.PlayEffect("Effect_earthquake");
        Map_Manager.Get.CameraShake();
    }
}
