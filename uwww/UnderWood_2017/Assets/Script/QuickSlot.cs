﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickSlot : MonoBehaviour
{
    itemSlotData data;
    public UISprite sp_Icon;
    public UILabel lb_Count;

    public void CheckData(itemSlotData _data)
    {
        if(data == _data)
        {
            data = null;
            Refresh();
        }
    }

    public itemSlotData GetData()
    {
        return data;
    }

    public void Refresh()
    {
        if (data != null)
        {
            sp_Icon.spriteName = data.tableData.iconName;
            lb_Count.text = data.count.ToString();
        }
        else
        {
            sp_Icon.spriteName = "EmptyIcon";
            lb_Count.text = "";
        }
    }

    public void SetSlot(itemSlotData _data)
    {
        data = _data;
        Refresh();
    }

    public void OnClick_Slot()
    {
        if (Character.Get.isLive == false)
            return;

        if (data == null)
            return;

        if (data.count <= 0)
        {
            SetSlot(null);
        }
        //마지막 남은 포션인가?
        else if (data.count == 1)
        {
            Character.Get.UseItem(data);
            ItemType temp = data.tableData.itemType;
            data = null;
            IngameUI_Manager.Get.SetAutoQuickSlot(temp);
        }
        else
        {
            Character.Get.UseItem(data);
            lb_Count.text = data.count.ToString();
        }
    }
}
