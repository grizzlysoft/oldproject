﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraftUI : MonoBehaviour
{
    Character character;

    public GameObject go_Content;
    public List<MaterialContent> li_Content = new List<MaterialContent>();
    public UIScrollView sc_View;
    public UIGrid gr_Grid;

    public UILabel lb_Title;
    public UILabel lb_Count;

    public UISprite sp_IconBG;
    public UISprite sp_Icon;
    public GameObject go_YesBlock;

    public CraftListViewer CraftViewer;
    public OverlabReward result;

    int count = 1;

    Table_Manager.Item selectItem;

    public GameObject go_Arrow;

    public void Show()
    {
        selectItem = null;
        Refresh();
        CraftViewer.RefreshCurrent();
        this.gameObject.SetActive(true);
    }
    public void Hide()
    {
        result.Hide();
        this.gameObject.SetActive(false);
    }

    public void Init(Character _characterData)
    {
        character = _characterData;
        
        GameObject temp;
        MaterialContent tempScp;
        for (int i=0; i<10; i++)//재료가 10개는 안넘도록 하자
        {
            temp = Instantiate(go_Content, gr_Grid.transform) as GameObject;
            tempScp = temp.GetComponent<MaterialContent>();
            tempScp.Hide();
            li_Content.Add(tempScp);
        }

        CraftViewer.Init(this);
    }

    public void Refresh()
    {
        //모든 요소를 꺼줌
        for (int i = 0; i < li_Content.Count; i++)
        {
            li_Content[i].Hide();
        }

        if (selectItem == null)
        {
            lb_Title.text = LM.DATA[38];
            sp_Icon.spriteName = "QuestionMark";
            sp_IconBG.color = ColorPreset.UI_Color_Normal;
            go_YesBlock.SetActive(true);
            lb_Count.text = "";
            go_Arrow.SetActive(false);
        }
        else
        {
            lb_Title.text = string.Format( LM.DATA[39] , selectItem.name);
            sp_Icon.spriteName = selectItem.iconName;
            sp_IconBG.color = ColorPreset.GetGradeColor(selectItem.grade);
            go_YesBlock.SetActive(!isHaveAllMeterial());
            lb_Count.text = string.Format("x{0}",count);


            //재료를 셋팅
            for (int i = 0; i < selectItem.li_Material_ID.Count; i++)
            {
                li_Content[i].Show(
                    selectItem.li_Material_ID[i],
                    selectItem.li_Material_Count[i] * count);
            }
            go_Arrow.SetActive(selectItem.li_Material_ID.Count > 3);
        }

        gr_Grid.Reposition();
        sc_View.ResetPosition();
    }

    public void SetMaterial(Table_Manager.Item _data)
    {
        selectItem = _data;
        count = 1;
        Refresh();
    }

    bool isHaveAllMeterial()
    {
        if (selectItem == null)
            return false;

        //하나라도 없으면 false
        for (int i=0; i< selectItem.li_Material_ID.Count; i++)
        {
            if (character.CheckHaveItem_Inventory(
                selectItem.li_Material_ID[i],
                selectItem.li_Material_Count[i] * count) == false)
                return false;
        }

        return true;
    }
    

    public void OnClick_Yes()
    {
        if (isHaveAllMeterial() == false)
            return;
       
        //인벤토리 빈칸이 남아있는지 검색
        if(Character.Get.CheckAddItem(selectItem.id , count) == false)
        {
            Popup_Manager.Get.Warning.Show(LM.DATA[43]);
            return;
        }

        Sound_Manager.Get.PlayEffect("Effect_hammer");
        MakeItem(selectItem, count);
        result.Show();
    }

    #region 아이템 제작
    public void MakeItem(Table_Manager.Item _data, int _count)
    {
        itemSlotData findSlot;
        //필요한 재료 차감
        for (int i = 0; i < _data.li_Material_ID.Count; i++)
        {
            findSlot = character.GetList_Item().Find(r => r.tableData.id == _data.li_Material_ID[i]);
            character.Remove_Item(findSlot, _data.li_Material_Count[i] * _count);
        }

        character.AddItem(_data.id, _count);
        Popup_Manager.Get.Ingame.Refresh();
    }
    #endregion

    public void OnClick_Up()
    {
        if (selectItem == null)
            return;

        Sound_Manager.Get.PlayEffect("Effect_select");
        count++;
        Refresh();
    }
    public void OnClick_Down()
    {
        if (selectItem == null)
            return;

        if (count > 1)
        {
            Sound_Manager.Get.PlayEffect("Effect_select");
            count--;
            Refresh();
        }
    }
    
    public void OnClick_Help()
    {
        Sound_Manager.Get.PlayEffect("Effect_click");
        Popup_Manager.Get.Help.Show(2);
    }
}
