﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlchemyUI : MonoBehaviour
{
    Character character;

    public Animation anim;

    public GameObject go_Content;
    public List<MaterialContent> li_Content = new List<MaterialContent>();
    public UIScrollView sc_View;
    public UIGrid gr_Grid;

    public UILabel lb_Title;
    public UILabel lb_Name;

    public UISprite sp_Pot;
    public UISprite sp_IconBG;
    public UISprite sp_Icon;

    public GameObject btn_prev;
    public GameObject btn_next;
    public GameObject go_YesBlock;
    public GameObject go_NoBlock;

    public OverlabReward Overlab;
    public AlchemyListViewer alchemyListViewer;
    public GameObject go_Arrow;

    public List<ParticleSystem> li_ps = new List<ParticleSystem>();

    List<Table_Manager.AlchemyData> li_Table;
    int currentIndex = 0;
    int count = 1;
    public UILabel lb_Count;


    public void Show()
    {
        count = 1;
        currentIndex = 0;
        Refresh();
        this.gameObject.SetActive(true);
    }
    public void Hide(){this.gameObject.SetActive(false);}

    public void Init(Character _characterData)
    {
        character = _characterData;
        li_Table = Table_Manager.Get.li_Alchemy;
        int MAX = 0;
        //테이블에서 가장 많은 요소 만큼 미리 생성 -> Hide 해두기
        for (int i=0; i< li_Table.Count; i++)
        {
            if(MAX < li_Table[i].li_Material_ID.Count)
                MAX = li_Table[i].li_Material_ID.Count;
        }
        
        GameObject temp;
        MaterialContent tempScp;
        for (int i=0; i<MAX; i++)
        {
            temp = Instantiate(go_Content, gr_Grid.transform) as GameObject;
            tempScp = temp.GetComponent<MaterialContent>();
            tempScp.Hide();
            li_Content.Add(tempScp);
        }

        alchemyListViewer.Init(this);
    }

    void SetDefaultIcon()
    {
        sp_Icon.spriteName = "QuestionMark";
        sp_Icon.color = Color.white;
        sp_IconBG.color = ColorPreset.UI_Color_Normal;
        lb_Name.color = Color.white;
        lb_Name.text = "";
    }

    public void Refresh()
    {
        btn_prev.SetActive((currentIndex != 0));
        btn_next.SetActive((currentIndex != li_Table.Count - 1));

        go_YesBlock.SetActive(!isHaveAllMeterial());

        lb_Title.text = li_Table[currentIndex].name;

        SetDefaultIcon();

        lb_Count.text = string.Format("제조 횟수 {0}", count);

        //모든 요소를 꺼줌
        for (int i = 0; i < li_Content.Count; i++)
        {
            li_Content[i].Hide();
        }

        //현재 가차의 재료를 셋팅
        for (int i=0; i<li_Table[currentIndex].li_Material_ID.Count; i++)
        {
            li_Content[i].Show(
                li_Table[currentIndex].li_Material_ID[i],
                li_Table[currentIndex].li_Material_Count[i] * count);
        }

        go_Arrow.SetActive(li_Table[currentIndex].li_Material_ID.Count > 3);

        gr_Grid.Reposition();
        sc_View.ResetPosition();

        for (int i = 0; i < li_ps.Count; i++)
        {
            li_ps[i].gameObject.SetActive(false);
        }

        alchemyListViewer.Refresh(currentIndex);
    }

    bool isHaveAllMeterial()
    {
        //하나라도 없으면 false
        for(int i=0; i<li_Table[currentIndex].li_Material_ID.Count; i++)
        {
            if (character.CheckHaveItem_Inventory(
                li_Table[currentIndex].li_Material_ID[i],
                li_Table[currentIndex].li_Material_Count[i] * count) == false)
                return false;
        }

        return true;
    }

    public void OnClick_Yes()
    {
        if (anim.isPlaying == true)
        {
            Debug.Log("현재 애니메이션 플레이중!!");
            return;
        }
        
        if(isHaveAllMeterial() == false)
            return;
       
        //나올수 있는 아이템 갯수 만큼 체크
        if (Character.Get.CheckAddItem(li_Table[currentIndex].li_GetItem_ID.Count) == false)
        {
            Popup_Manager.Get.Warning.Show(string.Format(LM.DATA[85], li_Table[currentIndex].li_GetItem_ID.Count));
            return;
        }
        
        SetDefaultIcon();

        //버튼 못눌리게 막기-버튼 음영처리는 애니메이션에서도 함
        Popup_Manager.Get.BlockIngame.Show();

        //애니메이션 연출 
        anim.Play("HairGasha");
        

        Sound_Manager.Get.PlayEffect("Effect_bubbling");

        go_YesBlock.SetActive(true);
        go_NoBlock.SetActive(true);
    }

    public void EndAnim()
    {
        Popup_Manager.Get.BlockIngame.NowHide();
        go_NoBlock.SetActive(false);
        go_YesBlock.SetActive(!isHaveAllMeterial());
    }

    public void ShowResult()
    {
        List<int> makeID = character.AlchemyItem(currentIndex, count);
        alchemyListViewer.SetResult(makeID);
        Table_Manager.UseItem Find = Table_Manager.Get.li_UseItem.Find(r => r.id == makeID[0]);
        Table_Manager.UseItem Temp;

        for (int i=1; i<makeID.Count; i++)
        {
            Temp = Table_Manager.Get.li_UseItem.Find(r => r.id == makeID[i]);

            if (Find.grade < Temp.grade)
            {
                Find = Temp;
            }
        }

        sp_Icon.spriteName = Find.iconName;
        sp_IconBG.color = ColorPreset.GetGradeColor(Find.grade);
        lb_Name.color = sp_IconBG.color;
        lb_Name.text = Find.name;

        li_ps[(int)Find.grade].gameObject.SetActive(true);
        li_ps[(int)Find.grade].Play();

        if(Find.grade == GradeType.Relic)
            Sound_Manager.Get.PlayEffect("Effect_gasha1");
        else if (Find.grade == GradeType.Legendary)
            Sound_Manager.Get.PlayEffect("Effect_gasha1");
        else
            Sound_Manager.Get.PlayEffect("Effect_potionmake");

        Character.Get.SavePlayerLocalData();
    }

    public void OnClick_Next()
    {
        if (currentIndex < li_Table.Count-1)
        {
            Sound_Manager.Get.PlayEffect("Effect_select");
            currentIndex++;
        }
        Refresh();
    }
    public void OnClick_Prev()
    {
        if (currentIndex > 0)
        {
            Sound_Manager.Get.PlayEffect("Effect_select");
            currentIndex--;
        }
        Refresh();
    }


    public void OnClick_Help()
    {
        Sound_Manager.Get.PlayEffect("Effect_click");
        Popup_Manager.Get.Help.Show(2);
    }


    public void OnClick_Up()
    {
        Sound_Manager.Get.PlayEffect("Effect_select");
        count++;
        Refresh();
    }
    public void OnClick_Down()
    {
        if (count > 1)
        {
            Sound_Manager.Get.PlayEffect("Effect_select");
            count--;
            Refresh();
        }
    }

}
