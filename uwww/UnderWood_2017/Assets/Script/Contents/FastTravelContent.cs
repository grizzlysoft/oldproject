﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FastTravelContent : MonoBehaviour
{
    public UILabel lb_Name;
    public UISprite sp_BG;
    FastTravelUI parent;

    Table_Manager.MapData Find;
    
    public void Init(FastTravelUI _parent)
    {
        parent = _parent;
        Hide();
    }
    
    public void Show(Table_Manager.MapData _data)
    {
        Find = _data;
        
        lb_Name.text = Find.name.Split('-')[0];

        if (Find.Ground_name == "Ground_Forest")
            sp_BG.color = ColorPreset.HexToColor("0F8100FF");
        else if (Find.Ground_name == "Ground_Swamp")
            sp_BG.color = ColorPreset.HexToColor("91A56BFF");
        else if (Find.Ground_name == "Ground_Cave")
            sp_BG.color = ColorPreset.HexToColor("3A4A69FF");
        else if (Find.Ground_name == "Ground_Snow")
            sp_BG.color = ColorPreset.HexToColor("A1E4FFFF");
        else
            sp_BG.color = Color.white;


        this.gameObject.SetActive(true);
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }


    public void OnClick_Travel()
    {
        parent.SelectMap(Find, this.gameObject.transform);
    }


}
