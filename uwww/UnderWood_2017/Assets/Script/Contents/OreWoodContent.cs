﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OreWoodContent : MonoBehaviour
{
    Table_Manager.OreWoodData data;
    [SerializeField]
    SpriteRenderer sp_Img;
    [SerializeField]
    BoxCollider2D box;
    [SerializeField]
    Animation anim;
    
    int HP;
    DamagedType type;

    public void Show(int _id, int _xPos)
    {
        data = Table_Manager.Get.li_OreWoodData.Find(r=> r.id == _id);
        sp_Img.sprite = Sprite_Manager.Get.GetSprite_MapObject(data.spriteName);
        sp_Img.color = Color.white;
        this.transform.localPosition = new Vector3(_xPos, ConstValue.pos_Y , 0);

        //나무일때 박스크기 바꾸기
        if(data.spriteName.Contains("Wood") == true)
        {
            type = DamagedType.Wood;
            box.offset = new Vector2(20, 45);
            box.size = new Vector2(50, 90);
        }
        else
        {
            type = DamagedType.Ore;
            box.offset = new Vector2(0, 25);
            box.size = new Vector2(100, 50);
        }

        box.enabled = true;
        HP = data.HP;

        this.gameObject.SetActive(true);
    }
    public void Hide()
    {
        this.gameObject.SetActive(false);
    }
    
    public void GetDamaged(Transform _pos)
    {
        if (HP <= 0)
            return;

        DamageData _Dmg = new DamageData
        {
            Type = type,
            Dmg = 1,
            isCritcal = false
        };

        ObjectPool_Manager.Get.ShowDamage(_pos, _Dmg);

        HP -= _Dmg.Dmg;

        if (HP <= 0)
        {
            HP = 0;
            //아이템 드랍
            ObjectPool_Manager.Get.dropItem.ShowDropItem(data.dropID, this.gameObject.transform);
            box.enabled = false;
            anim.Play("OreWoodFadeOut");
        }
    }
}
