﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemContent : MonoBehaviour
{
    ItemContainer parentScp;
    public UISprite sp_Icon;
    public UISprite sp_BG;
    public itemSlotData data;
    public UILabel lb_Upgrade;
    public UILabel lb_Equip;

    Color black;

    // Use this for initialization
    public void Init (ItemContainer _parent)
    {
        black = ColorPreset.UI_Color_EmptySlot;
        parentScp = _parent;
        Hide();
    }


    public void Show(itemSlotData _data)
    {
        data = _data;
        sp_Icon.spriteName = _data.tableData.iconName;
        sp_Icon.gameObject.SetActive(true);

        lb_Equip.text = "";
        lb_Upgrade.text = "";

        //장비인지 잡템인지
        if (data.count > 0)
        {
            //0보다 크면 잡템
            lb_Upgrade.text = data.count.ToString();
        }
        else
        {
            //장착장비 장착여부 on
            if (data.count == -1)
            {
                lb_Equip.text = "E";
            }

            //강화 여부 - 강화도 추출
            string temp = _data.tableData.id.ToString().Substring(2, 1);
            if (temp != "0")
                lb_Upgrade.text = "+" + temp;
        }
      
        //레어도 
        sp_BG.color = ColorPreset.GetGradeColor(data.tableData.grade);
    }

    public void Show(int _id, int _count , bool _isGetChance = false)
    {
        data = null;

        Table_Manager.Item find = Table_Manager.Get.FindItem(_id);

        sp_Icon.spriteName = find.iconName;
        sp_Icon.gameObject.SetActive(true);

        lb_Equip.text = "";
        if (_isGetChance == true)
            lb_Upgrade.text = "";// GetChance(_count);
        else
            lb_Upgrade.text = _count.ToString();
        
        //레어도 
        sp_BG.color = ColorPreset.GetGradeColor(find.grade);
    }

    public void Show_DropItemInfo(itemSlotData _data)
    {
        data = _data;
        sp_Icon.spriteName = _data.tableData.iconName;
        sp_Icon.gameObject.SetActive(true);

        lb_Equip.text = "";
        lb_Upgrade.text = "";
     
        //레어도 
        sp_BG.color = ColorPreset.GetGradeColor(data.tableData.grade);
    }

    string GetChance(int _count)
    {
        if(_count < 2){return "+";}
        else if (_count < 20){return "++";}
        else if (_count < 30){return "+++";}
        else if (_count < 40) { return "++++";}
        else { return "+++++";}
    }

    public void Hide()
    {
        lb_Upgrade.text = "";
        lb_Equip.text = "";
        data = null;
        sp_BG.color = black;
        sp_Icon.gameObject.SetActive(false);
    }

    public void OnClick()
    {
        if (data == null || parentScp == null)
            return;

        //아이템 타입 별 사용
        parentScp.ClickItem(data);
    }

    public void OnPress()
    {
        if (data == null)
            return;

        Invoke("ShowTooltip", 0.7f);
    }

    public void OnRelease()
    {
        if (data == null)
            return;

        CancelInvoke();
        Popup_Manager.Get.Ingame.HideTooltip();
    }

    void ShowTooltip()
    {
        if (parentScp == null)
        {
            Popup_Manager.Get.Ingame.ShowTooltip(data.tableData, this.transform.position);
        }
        else
        {
            if (parentScp.scrollView.isDragging == false)
                Popup_Manager.Get.Ingame.ShowTooltip(data.tableData, this.transform.position);
        }
    }
}
