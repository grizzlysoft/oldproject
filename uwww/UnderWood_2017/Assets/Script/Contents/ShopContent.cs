﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopContent : MonoBehaviour
{
    public UILabel lb_Name;
    public UILabel lb_Price;
    public UISprite sp_Price;
    public UISprite sp_Icon;
    Table_Manager.ShopData data;

    Popup_Shop parent;

    public void Init(Popup_Shop _parent)
    {
        parent = _parent;
    }
    
    //데이터로 셋업
    public void Show(Table_Manager.ShopData _data)
    {
        data = _data;

        lb_Name.text = data.name;
        lb_Price.text = data.price.ToString();
        sp_Icon.color = Color.white;
        SetPriceIcon();
        switch (data.tapType)
        {
            case ShopTapType.Cash:
                sp_Icon.spriteName = string.Format("Icon_BuyDia_{0}", data.id);
                break;
            case ShopTapType.Skin:
            case ShopTapType.Hair:
                Table_Manager.CustomizingData Find = Table_Manager.Get.li_CustomizingData.Find(r => r.id == data.itemID);
                sp_Icon.spriteName = Find.SpriteName;
                sp_Icon.color = Find.color;

                //이미 보유중이라면~
                if (Character.Get.CheckHaveCustomizing(Find.id) == true)
                {
                    sp_Price.spriteName = "Check";
                    lb_Price.text = LM.DATA[48];
                }
                break;
            case ShopTapType.Gold:
                sp_Icon.spriteName = string.Format("Icon_BuyGold_{0}", data.id);
                break;
            case ShopTapType.ETC:
                Table_Manager.Item FindETC = Table_Manager.Get.FindItem(data.itemID);
                sp_Icon.spriteName = FindETC.iconName;
                break;
        }

        this.gameObject.SetActive(true);
    }

    void SetPriceIcon()
    {
        switch (data.priceType)
        {
            case PriceType.Cash:
                sp_Price.spriteName = "Won";
                break;
            case PriceType.Dia:
                sp_Price.spriteName = "Dia";
                break;
            case PriceType.Gold:
                sp_Price.spriteName = "Gold";
                break;
            case PriceType.AD:
                sp_Price.spriteName = "AD_Icon";
                break;
            default:
                break;
        }
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }

    public void OnCilck()
    {
        Sound_Manager.Get.PlayEffect("Effect_click");

        switch (data.tapType)
        {
            case ShopTapType.Cash:
                Purchasing.Get.Buy(data.otherData , Buy_Dia);
                break;
            case ShopTapType.Skin:
            case ShopTapType.Hair:
                //이미 소지한 장비는 하지않는당...
                if (sp_Price.spriteName == "Check")
                    return;

                if (CheckItemPrice() == false)
                    return;

                Popup_Manager.Get.Buy.Show(data.priceType, data.name, LM.DATA[47] ,data.price, Buy_Customizing);
                break;
            case ShopTapType.Gold:
                if (CheckItemPrice() == false)
                    return;

                if (Character.Get.CheckAddGold(data.itemID) == false)
                {
                    Popup_Manager.Get.Warning.Show(LM.DATA[27]);
                    return;
                }

                Popup_Manager.Get.Buy.Show(data.priceType, data.name, LM.DATA[47], data.price, Buy_Gold);
                break;

            case ShopTapType.ETC:
                if (CheckItemPrice() == false)
                    return;

                //인벤토리 빈공간 체크
                if (Character.Get.CheckAddItem() == false)
                {
                    Popup_Manager.Get.Warning.Show(LM.DATA[43]);
                    return;
                }

                Popup_Manager.Get.Buy.Show(data.priceType, data.name, LM.DATA[47], data.price, Buy_ETC);
                break;
        }
    }

    bool CheckItemPrice()
    {
        switch (data.priceType)
        {
            case PriceType.Dia:
                if (Character.Get.CheckHaveDia(data.price) == false)
                {
                    Popup_Manager.Get.YesNo.Show(LM.DATA[49], LM.DATA[46], parent.OnClick_Dia);
                    return false;
                }
                break;
            case PriceType.Gold:
                if (Character.Get.CheckHaveGold(data.price) == false)
                {
                    Popup_Manager.Get.YesNo.Show(LM.DATA[82], LM.DATA[83], parent.OnClick_Gold);
                    return false;
                }
                break;
        }

        return true;
    }

    public void Buy_Dia()
    {
        Character.Get.AddDia(data.itemID);
        parent.MakeText(PriceType.Dia, data.itemID);
        parent.Refresh_NoReposition();
    }

    public void Buy_Customizing()
    {
        Sound_Manager.Get.PlayEffect("Effect_buy");

        Character.Get.UseDia(data.price);
        Character.Get.AddCustomizing(data.itemID);

        parent.MakeText(PriceType.Dia, -data.price);

        parent.Refresh_NoReposition();
    }
    public void Buy_Gold()
    {
        Sound_Manager.Get.PlayEffect("Effect_buy");

        Character.Get.UseDia(data.price);
        Character.Get.AddGold(data.itemID);
        parent.MakeText(PriceType.Dia, -data.price);
        parent.MakeText(PriceType.Gold, data.itemID);

        parent.Refresh_NoReposition();
    }

    public void Buy_ETC()
    {
        Sound_Manager.Get.PlayEffect("Effect_buy");

        if(data.priceType == PriceType.Dia)
        {
            Character.Get.UseDia(data.price);
            parent.MakeText(PriceType.Dia, -data.price);
        }
        else if(data.priceType == PriceType.Gold)
        {
            parent.MakeText(PriceType.Gold, -data.price);
            Character.Get.UseGold(data.price);
        }

        Character.Get.AddItem(data.itemID, int.Parse(data.otherData));

        parent.Refresh_NoReposition();
    }
}
