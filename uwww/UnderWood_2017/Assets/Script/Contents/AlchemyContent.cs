﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlchemyContent : MonoBehaviour
{
    public UISprite sp_BG;
    public UISprite sp_Icon;

    Table_Manager.Item data;

    public void Show(int _colorID)
    {
        data = Table_Manager.Get.FindItem(_colorID);
        sp_BG.color = ColorPreset.GetGradeColor(data.grade);
        sp_Icon.spriteName = data.iconName;
        this.gameObject.SetActive(true);
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }

    public void OnPress()
    {
        if (data == null)
            return;

        Invoke("ShowTooltip", 1.0f);
    }

    public void OnRelease()
    {
        if (data == null)
            return;

        CancelInvoke();
        Popup_Manager.Get.Ingame.HideTooltip();
    }

    void ShowTooltip()
    {
        Vector3 pos = new Vector3(this.transform.position.x + 1, this.transform.position.y, this.transform.position.z);
        Popup_Manager.Get.Ingame.ShowTooltip(data, pos);
    }

}
