﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialContent : MonoBehaviour
{
    public UISprite sp_BG;
    public UISprite sp_Icon;
    public UILabel lb_Count;
    public GameObject go_Check;

    int id;
    int count;

    Table_Manager.Item data;

    public void Hide() { this.gameObject.SetActive(false); }

    public void Show(int _id, int _count)
    {
        data = Table_Manager.Get.li_ETC.Find(r => r.id == _id);
        if (data == null)
        {
            Debug.LogError("없는 아이템 형식");
            return;
        }

        id = _id;
        count = _count;

        sp_BG.color = ColorPreset.GetGradeColor(data.grade);
        sp_Icon.spriteName = data.iconName;
        lb_Count.text = string.Format("x{0}", _count.ToString());

        go_Check.SetActive(Character.Get.CheckHaveItem_Inventory(id, count));

        this.gameObject.SetActive(true);
    }

    public void OnPress()
    {
        if (data == null)
            return;

        Invoke("ShowTooltip", 0.7f);
    }

    public void OnRelease()
    {
        if (data == null)
            return;

        CancelInvoke();
        Popup_Manager.Get.Ingame.HideTooltip();
    }

    void ShowTooltip()
    {
        Vector3 pos = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
        Popup_Manager.Get.Ingame.ShowTooltip(data, pos);
    }
}
