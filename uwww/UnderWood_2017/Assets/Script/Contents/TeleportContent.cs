﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportContent : MonoBehaviour
{
    public UILabel lb_Name;
    public UISprite sp_BG;
    Popup_Teleport parent;

    Table_Manager.MapData Find;
    bool isOpen = false;
    public GameObject go_Block;

    public void Init(Popup_Teleport _parent, Table_Manager.MapData _data)
    {
        parent = _parent;
        Find = _data;

        lb_Name.text = Find.name;

        if (Find.Ground_name == "Ground_Forest")
            sp_BG.color = ColorPreset.HexToColor("0F8100FF");
        else if (Find.Ground_name == "Ground_Swamp")
            sp_BG.color = ColorPreset.HexToColor("91A56BFF");
        else if (Find.Ground_name == "Ground_Cave")
            sp_BG.color = ColorPreset.HexToColor("3A4A69FF");
        else if (Find.Ground_name == "Ground_Snow")
            sp_BG.color = ColorPreset.HexToColor("A1E4FFFF");
        else
            sp_BG.color = Color.white;

        this.gameObject.SetActive(true);
    }

    public void Show(bool _isOpen)
    {
        isOpen = _isOpen;
        go_Block.SetActive(!isOpen);
    }

    public void OnClick_Teleport()
    {
        if (isOpen == false)
            return;

        Popup_Manager.Get.YesNo.Show(Find.name, LM.DATA[87], Del_Teleport);
    }

    void Del_Teleport()
    {
        parent.Teleport();
        Sound_Manager.Get.PlayEffect("Effect_teleport");
        Map_Manager.Get.ChangeMap_Pos(Find.stageID);
        Popup_Manager.Get.Ingame.Hide();
    }
}
