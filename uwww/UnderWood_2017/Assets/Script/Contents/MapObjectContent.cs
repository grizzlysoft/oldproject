﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapObjectContent : MonoBehaviour
{
    //타입
    public Popup_Ingame.MapObjectType type;
    
    private void OnTriggerExit2D(Collider2D _coll)
    {
        if (_coll.tag == "UserCharacter")
        {
            Character.Get.noticeSay.Hide();
            IngameUI_Manager.Get.SetMapObjectType(Popup_Ingame.MapObjectType.None);
        }
    }

    private void OnTriggerStay2D(Collider2D _coll)
    {
        if (_coll.tag == "UserCharacter")
        {
            IngameUI_Manager.Get.SetMapObjectType(type);
        }
    }
  
}
