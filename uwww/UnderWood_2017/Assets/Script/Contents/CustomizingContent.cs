﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomizingContent : MonoBehaviour
{
    public Table_Manager.CustomizingData data;

    CustomizingUI parent;
    public UISprite BG;
    public GameObject Lock;
    public GameObject Equip;
    bool isLock;

    public UISprite icon_Hair;
    public UISprite icon_Skin;
    public UISprite icon_Face;
    public UISprite icon_Face_Head;

    public void Init(int _id)
    {
        data = Table_Manager.Get.li_CustomizingData.Find(r=>r.id == _id);
        isLock = true;
        Lock.SetActive(false);
        Equip.SetActive(false);

        icon_Skin.gameObject.SetActive(false);
        icon_Hair.gameObject.SetActive(false);
        icon_Face.gameObject.SetActive(false);
        icon_Face_Head.gameObject.SetActive(false);

        BG.color = ColorPreset.GetGradeColor(data.grade);

        BG.gameObject.SetActive(false);

        switch (data.type)
        {
            case CustomizingType.Skin:
                icon_Skin.color = data.color;
                icon_Skin.gameObject.SetActive(true);
                break;
            case CustomizingType.Hair:
                icon_Hair.spriteName = data.SpriteName;
                icon_Hair.color = data.color;
                icon_Hair.gameObject.SetActive(true);
                break;
            case CustomizingType.Face:
                icon_Face.spriteName = string.Format("{0}_Idle", data.SpriteName);
                icon_Face.gameObject.SetActive(true);
                icon_Face_Head.gameObject.SetActive(true);
                break;
            default:
                break;
        }
    }


    public void Init(Table_Manager.CustomizingData _data , CustomizingUI _parent)
    {
        data = _data;
        parent = _parent;
        isLock = true;
        Lock.SetActive(isLock);
        Equip.SetActive(false);


        icon_Skin.gameObject.SetActive(false);
        icon_Hair.gameObject.SetActive(false);
        icon_Face.gameObject.SetActive(false);
        icon_Face_Head.gameObject.SetActive(false);

        BG.color = ColorPreset.GetGradeColor(data.grade);

        switch (data.type)
        {
            case CustomizingType.Skin:
                icon_Skin.color = data.color;
                icon_Skin.gameObject.SetActive(true);
                break;
            case CustomizingType.Hair:
                icon_Hair.spriteName = data.SpriteName;
                icon_Hair.color = Character.Get.sp_Hair.color;
                icon_Hair.gameObject.SetActive(true);
                break;
            case CustomizingType.Face:
                icon_Face.spriteName = string.Format("{0}_Idle", data.SpriteName);
                icon_Face.gameObject.SetActive(true);
                icon_Face_Head.gameObject.SetActive(true);
                break;
            default:
                break;
        }
        
    }

    public void Refresh()
    {
        isLock = !Character.Get.CheckHaveCustomizing(data.id);
        Lock.SetActive(isLock);

        //캐릭터의 정보에 따라셋팅
        //지금 보여주는거 타입 
        switch (data.type)
        {
            case CustomizingType.Skin:
                SetEquip(Character.Get.SkinIndex);
                break;
            case CustomizingType.Hair:
                SetEquip(Character.Get.HairIndex);
                icon_Hair.color = Character.Get.sp_Hair.color;
                break;
            case CustomizingType.Face:
                SetEquip(Character.Get.FaceIndex);

                //현재 피부색 적용
                Table_Manager.CustomizingData Find = Table_Manager.Get.li_CustomizingData.Find(r => r.id == Character.Get.SkinIndex);
                icon_Face_Head.color = Find.color;
                break;
            default:
                break;
        }
    }
    
    void SetEquip(int _id)
    {
        if (_id == data.id)
        {
            Equip.SetActive(true);
        }
        else
        {
            Equip.SetActive(false);
        }
    }

    public void OnClick()
    {
        if (isLock == true)
            return;

        Sound_Manager.Get.PlayEffect("Effect_equip");

        switch (data.type)
        {
            case CustomizingType.Skin:
                Character.Get.ChangeSkin(data.id);
                break;
            case CustomizingType.Hair:
                Character.Get.ChangeHair(data.id);
                break;
            case CustomizingType.Face:
                Character.Get.ChangeFace(FaceType.Idle,data.id);
                break;
            default:
                break;
        }

        parent.Refresh();
    }

}
