﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropContent : MonoBehaviour
{
    public int id;
    public Table_Manager.Item item;
    public SpriteRenderer sp_Icon;
    public Animation anim;
    public BoxCollider2D box;

    public void Init()
    {
        box.enabled = false;
        Hide();
    }

    public void Show(int _id, Vector3 _pos)
    {
        id = _id;
        item = Table_Manager.Get.FindItem(id);
        sp_Icon.sprite = Sprite_Manager.Get.GetSprite_Icon(item.iconName);
        this.transform.position = _pos;
        this.gameObject.SetActive(true);
    }
    
    public void Hide()
    {
        this.gameObject.SetActive(false);
    }

    public void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "UserCharacter")
        {
            if (Character.Get.AddItem(id) == true)
            {
                Sound_Manager.Get.PlayEffect("Effect_getitem");
                box.enabled = false;
                anim.Play("GetItem");
                //Debug.Log("아이템 추가: " + id);
            }
        }
    }
}
