﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraftContent : MonoBehaviour
{

    CraftUI parent;
    public UISprite sp_BG;
    public UISprite sp_Icon;

    Table_Manager.Item data;

    public void Init(CraftUI _parent)
    {
        parent = _parent;
        Hide();
    }
    public void Hide() { this.gameObject.SetActive(false); }
    
    public void Show(Table_Manager.Item _data)
    {
        data = _data;
        sp_BG.color = ColorPreset.GetGradeColor(data.grade);
        sp_Icon.spriteName = data.iconName;

        this.gameObject.SetActive(true);
    }

    public void OnClick()
    {
        parent.SetMaterial(data);
    }

    public void OnPress()
    {
        if (data == null)
            return;

        Invoke("ShowTooltip", 0.7f);
    }

    public void OnRelease()
    {
        if (data == null)
            return;

        CancelInvoke();
        Popup_Manager.Get.Ingame.HideTooltip();
    }

    void ShowTooltip()
    {
        Vector3 pos = new Vector3(this.transform.position.x + 1, this.transform.position.y, this.transform.position.z);
        Popup_Manager.Get.Ingame.ShowTooltip(data, pos);
        
    }
}
