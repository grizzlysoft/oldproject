﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorageUI : ItemContainer
{
    public UISprite sp_SellButton;

    public override void Refresh(bool isReposition = false)
    {
        for (int i = 0; i < character.MaxStorage; i++)
        {
            li_slot[i].Hide();
        }

        //소지한 아이템으로 초기화 시키기 - 창고 목록으로~
        int StorageCount = character.GetList_Storage().Count;
        if (character.GetList_Storage().Count > character.MaxStorage)
        {
            StorageCount = character.MaxStorage;
        }

        for (int i = 0; i < StorageCount; i++)
        { 
            li_slot[i].Show(character.GetList_Storage()[i]);
        }

        if (isReposition == true)
        {
            SetState(Mode.Close);

            grid.Reposition();
            scrollView.ResetPosition();
        }
    }
    public override void ClickItem(itemSlotData _data)
    {
        selectItem = _data;

        //아이템 파괴 모드
        if (currentMode == Mode.Delete)
        {
            CheckDeleteItem();
        }
        else if (currentMode == Mode.Sell)
        {
            CheckSellItem();
        }
        else
        {
            //캐릭터 인벤토리로 아이템 이동 
            character.MoveItem_Inventory(selectItem);
        }
    }

    //장비 판매,파괴
    override protected void SellItem(int _sellCount)
    {
        character.Sell_Item(selectItem, _sellCount, true);
    }
    override protected void DeleteItem()
    {
        Sound_Manager.Get.PlayEffect("Effect_brokenitem");
        character.Remove_Item(selectItem , true);
    }
    override public void OnClick_Sort()
    {
        Sound_Manager.Get.PlayEffect("Effect_bookflip");
        character.Sorting_Storage();
        Refresh();
    }

    override protected void SetStateIcon()
    {
        switch (currentMode)
        {
            case Mode.Close:
            case Mode.Delete:
                sp_SellButton.color = ColorPreset.UI_Color_Normal;
                break;
            case Mode.Sell:
                sp_SellButton.color = ColorPreset.UI_Color_SellItem;
                break;
        }
    }


}
