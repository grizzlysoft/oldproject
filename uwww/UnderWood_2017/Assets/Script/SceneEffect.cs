﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneEffect : Singleton<SceneEffect>
{
    [SerializeField]
    private SpriteRenderer sr_blood;
    [SerializeField]
    private DayNightController DayNightController;

    public void SetBlood(float _value)
    {
        sr_blood.color = new Color(0.25f, 0, 0, _value);
    }

    public void SetDarkness(float _darkness)
    {
        DayNightController.SetNight(_darkness);
    }
    
}

