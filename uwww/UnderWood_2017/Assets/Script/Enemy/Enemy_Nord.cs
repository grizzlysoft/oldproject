﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Nord : Enemy
{
    public Sprite[] li_hair;
    public Sprite[] li_face;
    public Sprite[] li_weapon;
    public Sprite[] li_weapon2;

    public Sprite[] li_helm;

    public SpriteRenderer sp_face;
    public SpriteRenderer sp_hair;
    public SpriteRenderer sp_helm;
    public SpriteRenderer sp_weapon;
    public SpriteRenderer sp_weapon2;

    public SpriteMask sp_mask;


    public override int Init(int _Id, int _depth)
    {
        int index = Random.Range(0, li_hair.Length);
        sp_hair.sprite = li_hair[index];

        if (li_face.Length != 0)
        {
            index = Random.Range(0, li_face.Length);
            sp_face.sprite = li_face[index];
        }

        if (li_weapon.Length != 0)
        {
            index = Random.Range(0, li_weapon.Length);
            sp_weapon.sprite = li_weapon[index];
        }


        if (li_weapon2.Length != 0)
        {
            index = Random.Range(0, li_weapon2.Length);
            sp_weapon2.sprite = li_weapon2[index];
        }

        if (li_helm.Length != 0)
        {
            index = Random.Range(0, li_helm.Length);
            sp_helm.sprite = li_helm[index];
            sp_mask.sprite = li_helm[index];
            sp_hair.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
            index = Random.Range(0, 2);
            if (index > 0)
            {
                sp_helm.gameObject.SetActive(false);
                sp_hair.maskInteraction = SpriteMaskInteraction.None;
            }
        }

        return base.Init(_Id, _depth);
    }

    override protected void AI_Update()
    {
        CheckSightRange();

        switch (State)
        {
            case EnemyState.Idle:
                IdleUpdate(0.35f, 3);
                break;
            case EnemyState.Move:
                MoveUpdate();
                break;
            case EnemyState.Attack:
                LookPlayer();
                break;
            default:
                break;
        }
    }
}
