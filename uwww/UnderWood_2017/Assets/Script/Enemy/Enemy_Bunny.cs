﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Bunny : Enemy
{
    override protected void AI_Update()
    {
        switch (State)
        {
            case EnemyState.Idle:
                IdleUpdate(0.3f, 3);
                break;
            case EnemyState.Move:
                MoveUpdate();
                break;
            case EnemyState.Attack:
                LookPlayer();
                break;
            default:
                break;
        }

    }
    
}
