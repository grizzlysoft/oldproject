﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_UseLaser : MonoBehaviour
{
    Enemy parent;

    private void Awake()
    {
        parent = GetComponentInParent<Enemy>();
    }

    //////////////////레이저 쏘는 애들이 쓰는 것들///////////////////////
    public GameObject go_Laser;
    [SerializeField]
    public Transform[] ar_LaserPos;

    public void FireLasser(int i)
    {
        GameObject temp = Instantiate(go_Laser, ObjectPool_Manager.Get.quiver.go_CurrentEnemyQuiver.transform) as GameObject;
        temp.transform.localScale = Vector3.one;

        Enemy_AttackBox_Laser laserScp = temp.GetComponent<Enemy_AttackBox_Laser>();
        laserScp.FireLaser(parent, ar_LaserPos[i]);
    }
    /////////////////////////////////////////////////////////////////////

}
