﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Golem : Enemy
{
    float skill_1_Range = 0.2f;

    public override int Init(int _Id, int _depth)
    {
        skillCoolTime_1 = Random.Range(5, 10);

        return base.Init(_Id, _depth);
    }

    protected override bool Attack()
    {

        if (skillCoolTime_1 == 0 && CheckRange(skill_1_Range) == true)
        {
            //x초마다 체크 하기 위해서 아래처럼 변경
            skillCoolTime_1 = Random.Range(8, 10);

            LookPlayer();
            SetState(EnemyState.Skill_1);
            return true;
        }

        return base.Attack();
    }

    override protected void AI_Update()
    {
        CheckSightRange();

        switch (State)
        {
            case EnemyState.Idle:
                IdleUpdate(0.35f, 5);
                break;
            case EnemyState.Move:
                MoveUpdate();
                break;
            case EnemyState.Attack:
                LookPlayer();
                break;
            default:
                break;
        }
    }


    override public DamageData FinalDamage()
    {
        DamageData DmgData = base.FinalDamage();

        switch (State)
        {
            case EnemyState.Attack:
                break;
            case EnemyState.Skill_1:
                DmgData.Dmg += 10;
                break;
            default:
                break;
        }

        return DmgData;
    }
}
