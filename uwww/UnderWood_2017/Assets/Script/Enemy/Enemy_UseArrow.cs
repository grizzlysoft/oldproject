﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_UseArrow : MonoBehaviour
{
    Enemy parent;

    //////////////////화살 쏘는 애들이 쓰는 것들///////////////////////
    public GameObject go_Arrow;
    public Transform tr_Hand;

    //생성한 화살
    Enemy_AttackBox_Arrow Arrow;

    private void Awake()
    {
        parent = GetComponentInParent<Enemy>();
    }

    public void ReadyArrow()
    {
        GameObject temp = Instantiate(go_Arrow, tr_Hand) as GameObject;
        Arrow = temp.GetComponent<Enemy_AttackBox_Arrow>();
        Arrow.Init(parent);
        Arrow.transform.localPosition = Vector3.zero;
        Arrow.transform.localScale = Vector3.one;
        Arrow.gameObject.SetActive(true);
    }

    public void RealseArrow()
    {
        Arrow.transform.parent = ObjectPool_Manager.Get.quiver.go_CurrentEnemyQuiver.transform;
        //Arrow.transform.localScale = Vector3.one;
        Arrow.Shot();
    }
    /////////////////////////////////////////////////////////////////////



}
