﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour
{
    int Id;
    public EnemyState State;
    //기본 능력치
    protected bool isLive;
    protected int HP;
    protected bool BattleMode;
    protected Vector3 moveDir;
    //공격 대상
    protected Transform tr_Target;

    public Table_Manager.EnemyData data;

    protected Animator anim;
    [HideInInspector]
    public Transform RotationPivot;

    //랜덤 이동 관련
    protected bool randomMove_MoveLeft;
    protected float randomMove_Distance;
    protected float randomMove_Time;

    ///스킬 관련
    protected float skillCoolTime_1;
    protected float skillCoolTime_2;

    private float Scale;
    
    //뎁스 조정
    SpriteRenderer[] ar_Sprite;

    ///////////////////////////////////////////////////////////
    #region public Member
    // Use this for initialization
    public virtual int Init(int _Id , int _depth)
    {
        anim = GetComponentInChildren<Animator>();
        RotationPivot = transform.Find("RotationPivot").transform;

        SetState(EnemyState.Idle);
        //공격 대상을 지정한다 - 캐릭터 / 혹은 토템
        if (Map_Manager.Get.GetCurrentMapData().type == MapType.Defence)
            tr_Target = Map_Manager.Get.defenceObj.transform;
        else
            tr_Target = Character.Get.hitBox.transform;

        //비선공 없음
        BattleMode = true;

        //테이블 데이터 가져오기
        Id = _Id;
        isLive = true;

        data = Table_Manager.Get.li_EnemyData[Id];

        HP = data.HP;
        RefreshPlates();

        //기본 크기 저장
        Scale = RotationPivot.transform.localScale.x;

        // 뎁스 조정
        return SetDepth(_depth);
    }

    public void DestroyThis()
    {
        Enemy_Manager.Get.DieEnemy();
        this.gameObject.SetActive(false);
    }

    public void GetDamaged(DamageData _Dmg, Transform _pos)
    {
        if (isLive == false)
            return;

        _Dmg.Type = DamagedType.Enemy;

        if (_Dmg.Dmg <= 0)
        {
            _Dmg.Dmg = Random.Range(0, 2);
        }

        ObjectPool_Manager.Get.ShowDamage(_pos, _Dmg);
        
        HP -= _Dmg.Dmg;
        BattleMode = true;

        if (HP <= 0)
        {
            HP = 0;
            isLive = false;
            DieAnim();

            //만약 서바이벌 스테이지면 캐릭터 회복
            if (Map_Manager.Get.GetCurrentMapData().type == MapType.Survival)
                Character.Get.Heal_Pecent(5);

            //경험치 주기
            Character.Get.AddExp(data.EXP);
            //아이템 드랍 - 드롭 테이블 참조
            int findItemID = FindDropItem();
            if (findItemID > 0)
                ObjectPool_Manager.Get.dropItem.ShowDropItem(findItemID, this.gameObject.transform);

            //퀘스트 카운트 +
            Character.Get.CheckKillQuestMonster(data.id);
        }

        RefreshPlates();
    }

    #endregion
    ///////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////
    #region private Member

    void Update()
    {
        if (isLive == false)
        {
            //현재 애니메이션이 죽은 모션이 아니면 죽여준다.
            if( anim.GetCurrentAnimatorStateInfo(0).IsName("Die") == false)
            {
                DieAnim();
            }
            return;
        }

        if (Character.Get.isLive == false)
        {
            SetState(EnemyState.Idle,true);
            return;
        }

        moveDir = tr_Target.position - this.transform.position;
        CheckTimer();
        AI_Update();

        //SetCharacterPosition();
    }


    void SetCharacterPosition()
    {
        if (this.transform.localPosition.x < -ConstValue.MapMax_X)
        {
            this.transform.localPosition = new Vector3(-ConstValue.MapMax_X, ConstValue.pos_Y, 0);

        }
        else if (this.transform.localPosition.x > ConstValue.MapMax_X)
        {
            this.transform.localPosition = new Vector3(ConstValue.MapMax_X, ConstValue.pos_Y, 0);
        }
    }


    int SetDepth(int _depth)
    {
        ar_Sprite = RotationPivot.GetComponentsInChildren<SpriteRenderer>();
        //가장 높은 소팅 오더 구하기
        int highValue = 0;

        for (int i = 0; i < ar_Sprite.Length; i++)
        {
            ar_Sprite[i].sortingOrder += _depth;
            if (highValue < ar_Sprite[i].sortingOrder)
                highValue = ar_Sprite[i].sortingOrder;
        }

        return highValue;
    }

    int FindDropItem()
    {
        int findItemId = 0;
        int Rnd = Random.Range(0, 10000);
        int ChanceValue = 0;
        for (int i = 0; i < data.li_DropItem.Count; i++)
        {
            ChanceValue += data.li_DropItem[i].chance;
            if (Rnd < ChanceValue)
            {
                findItemId = data.li_DropItem[i].id;
                break;
            }
        }
        

        return findItemId;
    }

    #endregion
    ///////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////
    #region inheritance Class
    virtual public DamageData FinalDamage()
    {
        DamageData DmgData = new DamageData();
        DmgData.Dmg = Random.Range(data.ATK_Min, data.ATK_Max + 1);
        DmgData.Dmg = (DmgData.Dmg * 10) / (10 + Character.Get.DEF);
        DmgData.isCritcal = false;

        return DmgData;
    }

    abstract protected void AI_Update();
    virtual public void CheckEndState(EnemyState _state)
    {
        switch (_state)
        {
            case EnemyState.Attack:
                if (CheckAtkRange() == false)
                {
                    SetState(EnemyState.Move, true);
                }
                else
                {
                    LookPlayer();
                    Attack();
                }
                break;
            case EnemyState.Skill_1:
            case EnemyState.Skill_2:
                if (CheckAtkRange() == false)
                    SetState(EnemyState.Move);
                else
                    SetState(EnemyState.Attack);
                break;
            default:
                break;
        }
    }

    protected void IdleUpdate(float _distance = 0, float _checkDelay = 0)
    {
        if (BattleMode == true)
        {
            if (Attack() == false)
                SetState(EnemyState.Move, true);
        }
        else
        {
            if(_distance != 0 && _checkDelay != 0)
                CheckRandomMove(_distance, _checkDelay);
        }
    }

    protected void MoveUpdate()
    {
        if (BattleMode == true)
        {
             Move();
             Attack();
        }
        else
        {
            if(randomMove_Distance > 0)
            {
                RandomMove();
            }
        }
    }

    virtual protected bool Attack()
    {
        if (CheckAtkRange() == true)
        {
            LookPlayer();
            SetState(EnemyState.Attack);
            return true;
        }

        return false;
    }

    protected void SetState(EnemyState _state , bool _Cross = false)
    {
        if (State == _state)
        {
            //Debug.Log("이전과같은 상태: " + State.ToString());
            return;
        }

        //Debug.Log("현제 State : " + State.ToString() + "  변경 State : " + _state.ToString());
        State = _state;
        if (_Cross == false)
            anim.Play(State.ToString(), -1, 0f);
        else
            anim.CrossFade(State.ToString(),0.2f,-1,0f);
    }

    virtual protected void DieAnim()
    {
        SetState(EnemyState.Die);
    }

    virtual protected void Move()
    {
        if (moveDir.x < 0)
        {
            Flip_Left();
            this.gameObject.transform.Translate(-data.Speed * Time.deltaTime, 0, 0);
        }
        else if (moveDir.x > 0)
        {
            Flip_Right();
            this.gameObject.transform.Translate(data.Speed * Time.deltaTime, 0, 0);
        }
    }

    protected void RandomMove()
    {
        if (randomMove_MoveLeft == true)
        {
            Flip_Left();
            this.gameObject.transform.Translate(-data.Speed * Time.deltaTime, 0, 0);
        }
        else
        {
            Flip_Right();
            this.gameObject.transform.Translate(data.Speed * Time.deltaTime, 0, 0);
        }

        randomMove_Distance -= data.Speed * Time.deltaTime;
        if (randomMove_Distance < 0)
        {
            randomMove_Distance = 0;
            SetState(EnemyState.Idle, true);
        }
    }

    protected void RefreshPlates()
    {
       // sp_HP.fillAmount = (float)HP / data.HP;
    }

    protected void CheckSightRange()
    {
        if (BattleMode == true)
            return;

        //시야 범위안에 있을때 전투상태로 변경.
        if (-data.SightRange < moveDir.x && moveDir.x < data.SightRange)
        {
            BattleMode = true;
        }
    }

    protected bool CheckAtkRange()
    {
        if (Mathf.Abs(moveDir.x) < data.AtkRange)
        {
            return true;
        }

        return false;
    }

    protected bool CheckRange(float _value)
    {
        if (Mathf.Abs(moveDir.x) < _value)
        {
            return true;
        }

        return false;
    }

    protected void CheckTimer()
    {
        if (randomMove_Time > 0)
        {
            randomMove_Time -= Time.deltaTime;
            if (randomMove_Time < 0)
            {
                randomMove_Time = 0;
            }
        }

        if (skillCoolTime_1 > 0)
        {
            skillCoolTime_1 -= Time.deltaTime;
            if (skillCoolTime_1 < 0)
            {
                skillCoolTime_1 = 0;
            }
        }


        if (skillCoolTime_2 > 0)
        {
            skillCoolTime_2 -= Time.deltaTime;
            if (skillCoolTime_2 < 0)
            {
                skillCoolTime_2 = 0;
            }
        }
    }

    protected void CheckRandomMove(float _distance, float _checkDelay)
    {
        if (randomMove_Time == 0)
        {
            int dir = Random.Range(0, 2);
            if (dir == 0)
            {
                randomMove_Distance = _distance;
                //매번 좌우 좌우 움직이게끔
                randomMove_MoveLeft = !randomMove_MoveLeft;
                SetState(EnemyState.Move);
            }

            randomMove_Time = _checkDelay;
        }
    }
    

    protected void LookPlayer() 
    {
        //캐릭터를 바라보기
        if (moveDir.x < 0)
        {
            Flip_Left();
        }
        else if (moveDir.x > 0)
        {
            Flip_Right();
        }
    }

    protected void Flip_Left()
    {
        RotationPivot.rotation = Quaternion.Euler(0, 0, 0);
        RotationPivot.localScale = new Vector3(Scale, Scale, 1);
    }
    protected void Flip_Right()
    {
        RotationPivot.rotation = Quaternion.Euler(0, 180, 0);
        RotationPivot.localScale = new Vector3(Scale, Scale, -1);
    }
    #endregion
    ///////////////////////////////////////////////////////////
}