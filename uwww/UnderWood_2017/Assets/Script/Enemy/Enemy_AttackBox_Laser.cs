﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_AttackBox_Laser : MonoBehaviour
{
    Enemy parent;

    float speed = 2.5f;

    public void FireLaser(Enemy _par,Transform _pos)
    {
        parent = _par;
        this.gameObject.transform.position = _pos.position;
        this.gameObject.transform.rotation = _pos.rotation;
        this.gameObject.SetActive(true);
    }

    private void Update()
    {
        if (this.gameObject.activeSelf == true)
            this.gameObject.transform.Translate(-speed * Time.deltaTime, 0, 0);
    }

    public void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "UserCharacter")
        {
            coll.gameObject.GetComponent<Character_HitBox>().GetDamaged(parent.FinalDamage(), parent.RotationPivot.transform);
            Hide();
        }
        else if (coll.tag == "Ground")
        {
            Hide();
        }
        else if (coll.tag == "DefenceObject")
        {
            coll.gameObject.GetComponent<DefenceObject>().GetDamaged();
            Hide();
        }
    }
    
    void Hide()
    {
        Destroy(this.gameObject);
    }
}
