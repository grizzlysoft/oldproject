﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_AnimationInterface : MonoBehaviour
{
    [HideInInspector]
    public bool isArrow = false;
    [HideInInspector]
    public bool isLaser = false;

    Enemy parent;

    private void Awake()
    {
        parent = GetComponentInParent<Enemy>();
    }

    public Enemy GetEnemyData()
    {
        return parent;
    }

    public void GetDamaged(DamageData _Dmg , Transform _pos)
    {
        parent.GetDamaged(_Dmg, _pos);
    }

    public void CheckEndState(EnemyState _state)
    {
        parent.CheckEndState(_state);
    }

    public void Die()
    {
        parent.DestroyThis();
    }

    public void CameraShake()
    {
        Sound_Manager.Get.PlayEffect("Effect_earthquake");
        Map_Manager.Get.CameraShake();
    }

    public void CameraShake2()
    {
        Map_Manager.Get.CameraShake();
    }
}
