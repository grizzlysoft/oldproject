﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_AttackBox : MonoBehaviour
{
    Enemy parent;

    private void Awake()
    {
        parent = GetComponentInParent<Enemy>();
    }

    public void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "UserCharacter")
        {
            coll.gameObject.GetComponent<Character_HitBox>().GetDamaged(parent.FinalDamage() , parent.RotationPivot.transform);
        }
        else if (coll.tag == "DefenceObject")
        {
            coll.gameObject.GetComponent<DefenceObject>().GetDamaged();
        }
    }
}
