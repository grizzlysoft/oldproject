﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_IceGolem : Enemy
{
    override protected void AI_Update()
    {
        CheckSightRange();

        switch (State)
        {
            case EnemyState.Idle:
                IdleUpdate(0.35f, 5);
                break;
            case EnemyState.Move:
                MoveUpdate();
                break;
            case EnemyState.Attack:
                LookPlayer();
                break;
            default:
                break;
        }
    }
}
