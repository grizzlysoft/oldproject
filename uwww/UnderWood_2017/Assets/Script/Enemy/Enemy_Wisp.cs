﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Wisp : Enemy
{
    override protected void AI_Update()
    {
        switch (State)
        {
            case EnemyState.Idle:
                IdleUpdate(0.35f, 2f);
                break;
            case EnemyState.Move:
                MoveUpdate();
                break;
            case EnemyState.Attack:
                LookPlayer();
                break;
            default:
                break;
        }
    }
}
