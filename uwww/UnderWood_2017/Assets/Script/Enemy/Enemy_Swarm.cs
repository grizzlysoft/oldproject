﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Swarm : Enemy
{
    override protected void AI_Update()
    {
        CheckSightRange();

        switch (State)
        {
            case EnemyState.Idle:
                IdleUpdate(0.35f, 2f);
                break;
            case EnemyState.Move:
                MoveUpdate();
                break;
            case EnemyState.Attack:
                Move_Close();
                break;
            default:
                break;
        }
    }

    void Move_Close()
    {
        this.transform.position = new Vector3(
            tr_Target.position.x,
            this.transform.position.y,
            this.transform.position.z);

        RotationPivot.localRotation = Character.Get.RotationPivot.transform.localRotation;
    }

}


