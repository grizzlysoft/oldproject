﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Goblin : Enemy
{
    public Sprite[] li_head;
    public Sprite[] li_body;
    public Sprite[] li_weapon;

    public SpriteRenderer sp_head;
    public SpriteRenderer sp_body;
    public SpriteRenderer sp_weapon;

    public GameObject go_shield;

    public override int Init(int _Id, int _depth)
    {
        if(go_shield != null)
        {
            if (Random.Range(0, 2) == 1)
                go_shield.SetActive(true);
            else
                go_shield.SetActive(false);
        }

        int index = Random.Range(0, li_head.Length);
        sp_head.sprite = li_head[index];

        index = Random.Range(0, li_body.Length);
        sp_body.sprite = li_body[index];

        index = Random.Range(0, li_weapon.Length);
        sp_weapon.sprite = li_weapon[index];

        return base.Init(_Id, _depth);
    }

    override protected void AI_Update()
    {
        //선공 여부 - 이거 있으면 선공 치는거임
        CheckSightRange();

        switch (State)
        {
            case EnemyState.Idle:
                IdleUpdate(0.35f, 3);
                break;
            case EnemyState.Move:
                MoveUpdate();
                break;
            case EnemyState.Attack:
                LookPlayer();
                break;
            default:
                break;
        }
    }
}
