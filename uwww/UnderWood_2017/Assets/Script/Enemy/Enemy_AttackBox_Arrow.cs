﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_AttackBox_Arrow : MonoBehaviour
{
    Enemy parent;
    bool isShot = false;
    Vector3 prevPosition;
    Rigidbody2D rb;

    public void Init(Enemy _parent)
    {
        parent = _parent;
        rb = this.GetComponent<Rigidbody2D>();
        rb.simulated = false;
        isShot = false;

    }

    public void Shot()
    {
        int dir;
        if (parent.RotationPivot.rotation.eulerAngles.y == 180)
            dir = 1;
        else
            dir = -1;


        rb.gravityScale = 1;
        //Debug.Log(powerX);
        rb.AddForce(new Vector2(dir * 170, 70 * 1), ForceMode2D.Force);

        rb.simulated = true;
        isShot = true;
    }

    void Update()
    {
        //보이기만 하는 상태일때는 손을 따라간다.
        if (isShot == false)
        {
            return;
        }

        if (this.transform.localPosition.y < -350 ||
            this.transform.localPosition.x > 1500 ||
            this.transform.localPosition.x < -1500)
            Hide();
    }

    void FixedUpdate()
    {
        if (isShot == true)
        {
            Vector3 deltaPos = transform.position - prevPosition;                     // 현재위치 - 이전위치 = 방향
            float angle = Mathf.Atan2(deltaPos.y, deltaPos.x) * Mathf.Rad2Deg;// 삼각함수로 각도를 구함.

            if (0 != angle)    // 물리연산과 렌더링연산의 차이를 위해서 체크
            {
                transform.rotation = Quaternion.Euler(0, 0, angle);
                prevPosition = transform.position;
            }
        }
    }

    public void Hide()
    {
        Destroy(this.gameObject);
    }

    public void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "UserCharacter")
        {
            if (parent != null)
            {
                coll.gameObject.GetComponent<Character_HitBox>().GetDamaged(parent.FinalDamage(), parent.RotationPivot.transform);
            }
            Hide();
        }
        else if (coll.tag == "Ground")
        {
            Hide();
        }
        else if (coll.tag == "DefenceObject")
        {
            coll.gameObject.GetComponent<DefenceObject>().GetDamaged();
            Hide();
        }
    }
}
