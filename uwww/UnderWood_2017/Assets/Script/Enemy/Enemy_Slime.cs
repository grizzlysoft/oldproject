﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Slime : Enemy
{
    public SpriteRenderer sp_body;

    override protected void AI_Update()
    {
        CheckSightRange();

        switch (State)
        {
            case EnemyState.Idle:
                IdleUpdate(0.15f, 3);
                break;
            case EnemyState.Move:
                MoveUpdate();
                break;
            case EnemyState.Attack:
                LookPlayer();
                break;
            default:
                break;
        }
    }
    
    

}
