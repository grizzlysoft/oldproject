﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_NordHero : Enemy
{
    public Sprite[] li_hair;
    public Sprite[] li_helm;

    public SpriteRenderer sp_hair;
    public SpriteRenderer sp_helm;

    float skill_1_Range = 0.2f;
    float skill_2_Range = 0.4f;

    public SpriteMask sp_mask;

    public override int Init(int _Id, int _depth)
    {
        int index = Random.Range(0, li_hair.Length);
        sp_hair.sprite = li_hair[index];

        index = Random.Range(0, li_helm.Length);
        sp_helm.sprite = li_helm[index];
        sp_mask.sprite = li_helm[index];
        sp_hair.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
        index = Random.Range(0, 2);
        if (index > 0)
        {
            sp_helm.gameObject.SetActive(false);
            sp_hair.maskInteraction = SpriteMaskInteraction.None;
        }

        //스킬 설정
        skillCoolTime_1 = Random.Range(5, 10);
        skillCoolTime_2 = Random.Range(1, 5);

        return base.Init(_Id, _depth);
    }

    override protected void AI_Update()
    {
        CheckSightRange();

        switch (State)
        {
            case EnemyState.Idle:
                IdleUpdate(0.35f, 3);
                break;
            case EnemyState.Move:
                MoveUpdate();
                break;
            case EnemyState.Attack:
                LookPlayer();
                break;
            case EnemyState.Skill_1:
                LookPlayer();
                break;
            default:
                break;
        }
    }

    override protected bool Attack()
    {
        if (skillCoolTime_2 == 0 && CheckRange(skill_2_Range) == true)
        {
            //x초마다 체크 하기 위해서 아래처럼 변경
            skillCoolTime_2 = Random.Range(6, 12);

            LookPlayer();
            SetState(EnemyState.Skill_2);
            return true;
        }

        //Debug.Log("남은 스킬 쿨: " + skillCoolTime_1);
        //Debug.Log("스킬 사거리: " + skill_1_Range);

        if (skillCoolTime_1 == 0 && CheckRange(skill_1_Range) == true)
        {
            //x초마다 체크 하기 위해서 아래처럼 변경
            skillCoolTime_1 = Random.Range(3, 5);

            LookPlayer();
            SetState(EnemyState.Skill_1);
            return true;
        }

        return base.Attack();
    }

    override public DamageData FinalDamage()
    {
        DamageData DmgData = base.FinalDamage();

        switch (State)
        {
            case EnemyState.Attack:
                break;
            case EnemyState.Skill_1:
                DmgData.Dmg += 70;
                break;
            case EnemyState.Skill_2:
                DmgData.Dmg *= 12;
                break;
            default:
                break;
        }

        return DmgData;
    }

}
