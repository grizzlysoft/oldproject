﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Crocodile : Enemy
{
    float skill_1_Range = 2.0f;

    bool isLeft;

    public override int Init(int _Id, int _depth)
    {
        skillCoolTime_1 = Random.Range(3, 5);

        return base.Init(_Id, _depth);
    }

    protected override bool Attack()
    {

        if (skillCoolTime_1 == 0 && CheckRange(skill_1_Range) == true)
        {
            //x초마다 체크 하기 위해서 아래처럼 변경
            skillCoolTime_1 = Random.Range(5, 8);

            LookPlayer();
            if (moveDir.x < 0)
                isLeft = true;
            else
                isLeft = false;

            SetState(EnemyState.Skill_1);
            return true;
        }

        return base.Attack();
    }
    

    override public DamageData FinalDamage()
    {
        DamageData DmgData = base.FinalDamage();

        switch (State)
        {
            case EnemyState.Attack:
                break;
            case EnemyState.Skill_1:
                DmgData.Dmg += 30;
                break;
            default:
                break;
        }

        return DmgData;
    }


    override protected void AI_Update()
    {
        CheckSightRange();

        switch (State)
        {
            case EnemyState.Idle:
                IdleUpdate(0.4f, 3);
                break;
            case EnemyState.Move:
                MoveUpdate();
                break;
            case EnemyState.Attack:
                break;
            case EnemyState.Skill_1:
                if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.15f)
                {
                    if (isLeft == true)
                    {
                        Flip_Left();
                        this.gameObject.transform.Translate(-data.Speed * 3 * Time.deltaTime, 0, 0);
                    }
                    else
                    {
                        Flip_Right();
                        this.gameObject.transform.Translate(data.Speed * 3 * Time.deltaTime, 0, 0);
                    }
                }
                break;
            default:
                break;
        }
    }
}
