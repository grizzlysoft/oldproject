﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Beholder : Enemy
{
    float skill_1_Range = 1.0f;
    float skill_2_Range = 1.5f;

    public override int Init(int _Id, int _depth)
    {
        skillCoolTime_1 = Random.Range(0, 6);
        skillCoolTime_2 = Random.Range(7, 25);

        return base.Init(_Id, _depth);
    }

    override protected void AI_Update()
    {
        CheckSightRange();

        switch (State)
        {
            case EnemyState.Idle:
                IdleUpdate(0.35f, 3);
                break;
            case EnemyState.Move:
                MoveUpdate();
                break;
            case EnemyState.Skill_1:
                LookPlayer();
                break;
            default:
                break;
        }
    }

    override protected bool Attack()
    {
        if (skillCoolTime_2 == 0 && CheckRange(skill_2_Range) == true)
        {
            //x초마다 체크 하기 위해서 아래처럼 변경
            skillCoolTime_2 = Random.Range(10, 20);

            LookPlayer();
            SetState(EnemyState.Skill_2);
            return true;

        }

        if (skillCoolTime_1 == 0 && CheckRange(skill_1_Range) == true)
        {
            //x초마다 체크 하기 위해서 아래처럼 변경
            skillCoolTime_1 = Random.Range(5, 14);

            LookPlayer();
            SetState(EnemyState.Skill_1);
            return true;

        }

        return base.Attack();
    }
    
    override public DamageData FinalDamage()
    {
        DamageData DmgData = base.FinalDamage();

        switch (State)
        {
            case EnemyState.Attack:
                break;
            case EnemyState.Skill_1:
                DmgData.Dmg += 60;
                break;
            case EnemyState.Skill_2:
                DmgData.Dmg *= 9;
                break;
            default:
                break;
        }

        return DmgData;
    }
    
}
