﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Lizard : Enemy
{
    override protected void AI_Update()
    {
        CheckSightRange();

        switch (State)
        {
            case EnemyState.Idle:
                IdleUpdate(0.4f, 3);
                break;
            case EnemyState.Move:
                MoveUpdate();
                break;
            case EnemyState.Attack:
                break;
            default:
                break;
        }
    }
}
