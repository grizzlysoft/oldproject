﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Bat : Enemy
{
    override protected void AI_Update()
    {
        CheckSightRange();

        switch (State)
        {
            case EnemyState.Idle:
                IdleUpdate();
                break;
            case EnemyState.Move:
                MoveUpdate();
                break;
            default:
                break;
        }
    }

    override protected bool Attack()
    {
        if (CheckAtkRange() == true)
        {
            LookPlayer();
            SetState(EnemyState.Attack,true);
            return true;
        }

        return false;
    }

    protected override void DieAnim()
    {
        SetState(EnemyState.Die, true);
    }

}
