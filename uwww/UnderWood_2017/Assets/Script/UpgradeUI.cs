﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeUI : MonoBehaviour
{
    public UISprite sp_Icon;
    public UISprite sp_BG;
    itemSlotData selectItem;
    public UILabel lb_Upgrade;
    public UILabel lb_Prograss;
    public Animation anim;
    public ParticleSystem ps;
    public GameObject go_YesBlock;
    public UILabel lb_Gold;
    public UILabel lb_Chance;

    int needGold = 0;
    int up_value = 0;

    public void Init()
    {
    }

    public void Show()
    {
        Refresh();
        this.gameObject.SetActive(true);
    }

    public void Refresh()
    {
        up_value = 0;
        if (selectItem == null)
        {
            sp_BG.color = ColorPreset.UI_Color_Normal;
            lb_Prograss.text = LM.DATA[5];
            lb_Upgrade.text = "";
            sp_Icon.gameObject.SetActive(false);
            selectItem = null;
            go_YesBlock.SetActive(true);

            needGold = 0;
            lb_Gold.text = needGold.ToString();
            lb_Chance.text = "";
        }
        else
        {
            //강화 여부 - 강화도 추출
            up_value = GetUpgradeValue(selectItem.tableData.id);
            if (up_value != 0)
                lb_Upgrade.text = "+" + up_value;
            else
                lb_Upgrade.text = "";

            sp_BG.color = ColorPreset.GetGradeColor(selectItem.tableData.grade);
            sp_Icon.spriteName = selectItem.tableData.iconName;
            sp_Icon.gameObject.SetActive(true);

            if (up_value >= ConstValue.MaxUpgrade)
            {
                needGold = 0;
                lb_Gold.text = "MAX";
                go_YesBlock.SetActive(true);
                lb_Chance.text = "";
            }
            else
            {
                float tempGold = selectItem.tableData.price * ((up_value + 1) * 0.3f);
                needGold = Mathf.RoundToInt(tempGold);
                lb_Gold.text = needGold.ToString();
                go_YesBlock.SetActive(false);
                int Chance = Table_Manager.Get.li_EquipUpgradeData[up_value].chance + Character.Get.CampData().upgradeLevel;
                if (Chance > 10000)
                    Chance = 10000;
                float fChance = Chance / 100;
                lb_Chance.text = string.Format("{0}%",fChance.ToString("F2"));
            }
        }
    }

    public void Hide()
    {
        selectItem = null;
        StopAllCoroutines();
        this.gameObject.SetActive(false);
    }

    public void OnClick_Upgrade()
    {
        //선택 아이템이 없거나 골드가 부족하면 ~
        if (selectItem == null)
            return;

        //골드 삭감
        if (Character.Get.UseGold(needGold) == false)
            return;

        go_YesBlock.SetActive(true);
        StartCoroutine(UpgradeItem(selectItem));
    }


    public IEnumerator UpgradeItem(itemSlotData _data)
    {
        //강화도에 따른 연출
        //현제 도전하는 강화도 ->

        //강화중 연출 시작 - 끝나야 아래 진행
        yield return StartUpgrade_Effective();

        //성공률 측정
        int Rnd = Random.Range(0, 10000);

        //강화 성공률 계산
        int Chance = Table_Manager.Get.li_EquipUpgradeData[up_value].chance + Character.Get.CampData().upgradeLevel;
        if (Rnd < Chance) //성공
        {
            //강화 성공 연출
            Character.Get.Remove_Item(_data);
            Character.Get.AddItem(_data.tableData.id + 1000);
            Popup_Manager.Get.UpgradeResult.Show(true, _data.tableData.id + 1000);
        }
        else //실패
        {
            //강화 실패 연출
            Popup_Manager.Get.UpgradeResult.Show(false, _data.tableData.id + 1000);
        }

        selectItem = null;
        Popup_Manager.Get.Ingame.Refresh();
        Popup_Manager.Get.BlockIngame.NowHide();
        Character.Get.SavePlayerLocalData();

    }

    int GetUpgradeValue(int _id)
    {
        string temp = _id.ToString().Substring(2, 1);
        return int.Parse(temp);
    }

    public void SelectItemChange(itemSlotData _data)
    {
        if(_data.tableData.itemType == ItemType.Weapon ||
           _data.tableData.itemType == ItemType.Equipment ) 
        {
            //장착중 체크
            if (_data.count == -1)
            {
                Popup_Manager.Get.Warning.Show(LM.DATA[6]);
                return;
            }

            //다음 강화가 있는지 체크 
            int up_value = GetUpgradeValue(_data.tableData.id);
            if (up_value >= ConstValue.MaxUpgrade)
            {
                Popup_Manager.Get.Warning.Show(LM.DATA[7]);
                return;
            }

            selectItem = _data;
        }
        else
        {
            Popup_Manager.Get.Warning.Show(LM.DATA[8]);
            Refresh();
            return;
        }
    }

    public IEnumerator StartUpgrade_Effective()
    {
        //연출중에는 취소가 안됨
        Popup_Manager.Get.BlockIngame.Show();
        int count = 1;

        for (int j=0; j< count; j++)
        {
            anim.Play("Upgrading");
            lb_Prograss.text = LM.DATA[9];
            yield return new WaitForSeconds(0.3f);
            lb_Prograss.text = LM.DATA[9]+".";
            yield return new WaitForSeconds(0.3f);
            lb_Prograss.text = LM.DATA[9] + "..";
            yield return new WaitForSeconds(0.3f);
            lb_Prograss.text = LM.DATA[9] + "...";
            yield return new WaitForSeconds(0.3f);
            lb_Prograss.text = LM.DATA[9] + "....";
            yield return new WaitForSeconds(0.3f);
        }
    }

    public void HitEffect()
    {
        ps.Play();
        Sound_Manager.Get.PlayEffect("Effect_hammer");
    }

    public void OnClick_Help()
    {
        Sound_Manager.Get.PlayEffect("Effect_click");
        Popup_Manager.Get.Help.Show(1);
    }

}
