0|기본 기능 오픈|300000,310000,320000|0|6|25|0|0|0|0|0|500|CampUp_0|Creft_0|House_0|Box_0|Alchemy_0|
1|아이템 제작가능||50|6|25|0|0|0|0|0|500|CampUp_1|Creft_0|House_0|Box_0|Alchemy_0|
2|창고 사용가능||100|6|25|0|0|0|0|0|500|CampUp_7|Creft_0|House_0|Box_0|Alchemy_0|
3|광산 이용가능||500|6|25|0|0|0|0|0|500|CampUp_3|Creft_0|House_0|Box_0|Alchemy_0|
4|벌목장 이용가능||700|6|25|0|0|0|0|0|500|CampUp_4|Creft_0|House_0|Box_0|Alchemy_0|
5|강화 사용가능||1000|6|25|0|0|0|0|0|500|CampUp_5|Creft_0|House_0|Box_0|Alchemy_0|
6|연금술 사용가능||1200|6|25|0|0|0|0|0|500|CampUp_6|Creft_0|House_0|Box_0|Alchemy_0|
7|외형 변경 사용가능|300001,310001,320001|1500|6|25|0|0|0|0|0|500|CampUp_2|Creft_0|House_0|Box_0|Alchemy_0|
8|제작 품목 증가||2000|6|25|1|0|0|0|0|500|CampUp_8|Creft_1|House_0|Box_0|Alchemy_0|
9|새로운 외형 추가|300002,300003,320002,320003,310002,310003|2500|6|25|1|0|0|0|0|500|CampUp_16|Creft_1|House_1|Box_0|Alchemy_0|
10|강화 확률 증가 +2%||3000|6|25|1|0|0|0|200|500|CampUp_14|Creft_1|House_1|Box_0|Alchemy_0|
11|야간 시야범위 증가||3500|6|25|1|0|0|0|200|600|CampUp_15|Creft_1|House_1|Box_0|Alchemy_0|
12|창고 확장||4000|12|25|1|0|0|0|200|600|CampUp_9|Creft_1|House_1|Box_1|Alchemy_0|
13|인벤토리 확장||4500|12|30|1|0|0|0|200|600|CampUp_10|Creft_1|House_1|Box_1|Alchemy_0|
14|등장 광물량 증가||5000|12|30|1|0|3|0|200|600|CampUp_11|Creft_1|House_1|Box_1|Alchemy_0|
15|등장 나무량 증가||7500|12|30|1|0|3|3|200|600|CampUp_12|Creft_1|House_1|Box_1|Alchemy_0|
16|연금술 레어 획득율 증가 +3%||10000|12|30|1|1|3|3|200|600|CampUp_13|Creft_1|House_1|Box_1|Alchemy_1|
17|제작 품목 증가||20000|12|30|2|1|3|3|200|600|CampUp_8|Creft_2|House_1|Box_1|Alchemy_1|
18|새로운 외형 추가|300004,300005,320004,320005,310004,310005|25000|12|30|2|1|3|3|200|600|CampUp_16|Creft_2|House_2|Box_1|Alchemy_1|
19|강화 확률 증가 +2%||30000|12|30|2|1|3|3|400|600|CampUp_14|Creft_2|House_2|Box_1|Alchemy_1|
20|야간 시야범위 증가||35000|12|30|2|1|3|3|400|700|CampUp_15|Creft_2|House_2|Box_1|Alchemy_1|
21|창고 확장||40000|18|30|2|1|3|3|400|700|CampUp_9|Creft_2|House_2|Box_2|Alchemy_1|
22|인벤토리 확장||45000|18|35|2|1|3|3|400|700|CampUp_10|Creft_2|House_2|Box_2|Alchemy_1|
23|등장 광물량 증가||50000|18|35|2|1|6|3|400|700|CampUp_11|Creft_2|House_2|Box_2|Alchemy_1|
24|등장 나무량 증가||55000|18|35|2|1|6|6|400|700|CampUp_12|Creft_2|House_2|Box_2|Alchemy_1|
25|연금술 레어 획득율 증가||60000|18|35|2|2|6|6|400|700|CampUp_13|Creft_2|House_2|Box_2|Alchemy_2|
26|제작 품목 증가||100000|18|35|3|2|6|6|400|700|CampUp_8|Creft_3|House_2|Box_2|Alchemy_2|
27|새로운 외형 추가|300006,320006,310006|120000|18|35|3|2|6|6|400|700|CampUp_16|Creft_3|House_3|Box_2|Alchemy_2|
28|강화 확률 증가 +2%||140000|18|35|3|2|6|6|600|700|CampUp_14|Creft_3|House_3|Box_2|Alchemy_2|
29|야간 시야범위 증가||160000|18|35|3|2|6|6|600|700|CampUp_15|Creft_3|House_3|Box_2|Alchemy_2|
30|창고 확장||180000|24|35|3|2|6|6|600|800|CampUp_9|Creft_3|House_3|Box_3|Alchemy_2|
31|인벤토리 확장||200000|24|40|3|2|6|6|600|800|CampUp_10|Creft_3|House_3|Box_3|Alchemy_2|
32|등장 광물량 증가||220000|24|40|3|2|9|6|600|800|CampUp_11|Creft_3|House_3|Box_3|Alchemy_2|
33|등장 나무량 증가||240000|24|40|3|2|9|9|600|800|CampUp_12|Creft_3|House_3|Box_3|Alchemy_2|
34|연금술 레어 획득율 증가||300000|24|40|3|3|9|9|600|800|CampUp_13|Creft_3|House_3|Box_3|Alchemy_3|
