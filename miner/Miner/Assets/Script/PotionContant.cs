﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PotionType
{
    TapGoldx10,
    AutoGoldx10,
    TapAtkx5,
    AutoAtkx5,

    NewbiePackA,
    NewbiePackB,
    AlchemistPack,
    OwnerPack,
}

public class PotionContant : MonoBehaviour
{
    PotionType Type;

    public UISprite sp_Icon;
    public UILabel lb_Name;
    public UILabel lb_Cost;


    public void Init(PotionType _type)
    {
        Type = _type;

        lb_Name.text = SetName();
        sp_Icon.spriteName = SetIcon();
        lb_Cost.text = SetCost();
    }

    public void OnClick_Buy()
    {
        SoundManager.Get.PlaySound(SoundType.Click);

        switch (Type)
        {
            case PotionType.TapGoldx10:
            case PotionType.AutoGoldx10:
            case PotionType.TapAtkx5:
            case PotionType.AutoAtkx5:
                Time_Manager.Get.Start_PotionBuff(Type);
                break;
            case PotionType.NewbiePackA:
                if (GameData.BuyNewbiePackA == 0)
                    CallPurchasing(0);
                else
                    Popup_Manager.Get.Error.Show("이미 구매한 항목입니다");
                break;
            case PotionType.NewbiePackB:
                if (GameData.BuyNewbiePackB == 0)
                    CallPurchasing(1);
                else
                    Popup_Manager.Get.Error.Show("이미 구매한 항목입니다");
                break;
            case PotionType.AlchemistPack:
                if (GameData.BuyAlchemistPack == 0)
                    CallPurchasing(2);
                else
                    Popup_Manager.Get.Error.Show("이미 구매한 항목입니다");
                break;
            case PotionType.OwnerPack:
                if (GameData.BuyOwnerPack == 0)
                    CallPurchasing(3);
                else
                    Popup_Manager.Get.Error.Show("이미 구매한 항목입니다");
                break;
            default:
                break;
        }
    }

    void CallPurchasing(int type_)
    {
        Purchasing.Get.Buy(type_);
    }

    //void CallInAppBuy()
    //{
    //    //성공시 호출 - Type은 다른곳에 임시로 저장해둬야 할듯 -
    //    GameData.BuyPack(Type);
    //    UI_Manager.Get.Refresh(true);
    //    UI_Manager.Get.SetBuffState();
    //    //구매한 패키지에 따라서 분기처리 할것
    //    Popup_Manager.Get.BuyPackage.Show(Type);
    //}

    string SetCost()
    {
        switch (Type)
        {
            case PotionType.TapGoldx10:
            case PotionType.AutoGoldx10:
            case PotionType.TapAtkx5:
            case PotionType.AutoAtkx5:
                return "1개";
            case PotionType.NewbiePackA:
                return "1,100원";
            case PotionType.NewbiePackB:
                return "1,100원";
            case PotionType.AlchemistPack:
                return "3,300원";
            case PotionType.OwnerPack:
                return "33,000원";
            default:
                return "오류";
        }
    }

    string SetName()
    {
        switch (Type)
        {
            case PotionType.TapGoldx10:
                return "10초간 탭당 골드 10배";
            case PotionType.AutoGoldx10:
                return "10초간 초당 골드 10배";
            case PotionType.TapAtkx5:
                return "10초간 탭당 공격력 5배";
            case PotionType.AutoAtkx5:
                return "10초간 초당 공격력 5배";
            case PotionType.NewbiePackA:
                return "초보자 패키지 A\n물약 30개 + 탭당 수입 10% 증가";
            case PotionType.NewbiePackB:
                return "초보자 패키지 B\n물약 30개 + 초당 수입 10% 증가";
            case PotionType.AlchemistPack:
                return "연금술사 패키지\n물약 100개 + 세공 확률 10% 증가";
            case PotionType.OwnerPack:
                return "광산주 패키지\n물약 1000개 + 초당,탭당 수입 30% 증가";
            default:
                return "오류";
        }
    }

    string SetIcon()
    {
        switch (Type)
        {
            case PotionType.TapGoldx10:
                return "Cartoon RPG UI_Game Icon - Coin";
            case PotionType.AutoGoldx10:
                return "AutoGold";
            case PotionType.TapAtkx5:
                return "ClickAtk";
            case PotionType.AutoAtkx5:
                return "Cartoon RPG UI_Game Icon - Attack";
            case PotionType.NewbiePackA:
                return "Cartoon RPG UI_Game Icon - Wooden Treasure Box";
            case PotionType.NewbiePackB:
                return "Cartoon RPG UI_Game Icon - Iron Treasure Box";
            case PotionType.AlchemistPack:
                return "Cartoon RPG UI_Game Icon - Silver Treasure Box";
            case PotionType.OwnerPack:
                return "Cartoon RPG UI_Game Icon - Gold Treasure Box";

            default:
                return "오류";
        }
    }
}
