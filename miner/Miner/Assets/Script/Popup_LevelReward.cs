﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_LevelReward : PopupBase
{
    public UILabel lb_Level;
    public UILabel lb_Count;

    public void Show(string text, int level , int count)
    {
        lb_Level.text = string.Format("{0}\n{1}레벨 달성 보상", text, level);
        lb_Count.text = count.ToString();
        base.Show();
    }

}
