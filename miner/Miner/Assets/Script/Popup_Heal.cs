﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Heal : PopupBase
{
    public UILabel lb_Name;
    public UILabel lb_Cost;
    CustomDataType.BigInteger HealCost;
    int Type;
    bool free;

    /// <summary>
    /// 광부 단계별로 0 1 2 
    /// </summary>
    /// <param name="Type"></param>
    public void Show(int _Type)
    {
        Type = _Type;
        lb_Name.text = GetName(Type);
        HealCost = GetHealCost(Type);

        base.Show();
    }

    public string GetName(int _Type)
    {
        switch (Type)
        {
            case 0: return "초급 숙련 광부를";
            case 1: return "중급 숙련 광부를";
            case 2: return "고급 숙련 광부를";
        }

        return "";
    }

    CustomDataType.BigInteger GetHealCost(int Type)
    {
        //해당 광부 자동 골드 캐는 비용 30배 - 사제레벨에 따른 감소 비율 
        //광부의 기본 치료비용
        CustomDataType.BigInteger temp;
        switch (Type)
        {
            case 0:
                temp = Table_Manager.Get.m_MinerNoobData[GameData.Miner_Noob_Lv].get_gold * 30;
                break;
            case 1:
                temp = Table_Manager.Get.m_MinerExpertData[GameData.Miner_Expert_Lv].get_gold * 30;
                break;
            case 2:
                temp = Table_Manager.Get.m_MinerMasterData[GameData.Miner_Master_Lv].get_gold * 30;
                break;
            default:
                temp = 0;
                Debug.Log("버그!! 잘못 정의된 치료 광부타입");
                break;
        }

        //1퍼센트의 금액  
        CustomDataType.BigInteger onePercent = temp / 100;
        //할인율 퍼센트( Sale이 100이면 100프로 할인 , 25면 25퍼센트 할인)
        int Sale = Table_Manager.Get.m_ExpertHealerData[GameData.Expert_Healer_Lv].abillityValue / 100;
        temp -= (onePercent * Sale);

        if (Sale == 100)
        {
            lb_Cost.text = "무료";
            free = true;
        }
        else
        {
            lb_Cost.text = UI_Manager.Get.GetGoldString(temp.ToString(), 3);
            free = false;
        }

        return temp;
    }

    public void OnClick_Heal()
    {        
        //돈 체크  -돈 없으면 돈없다 팝업 뜨기
        if (UI_Manager.Get.CheckHaveMoney(HealCost) == false)
        {
            Popup_Manager.Get.NoGold.Show();
            Hide();
            return;
        }

        //치료 하기
        //광부는 Type에 저장되있음
        if (free == false)
            GameData.AddGold(-HealCost);
            

        Debug.Log("치료됨");
        Hide();
        Ingame.Get.HealMiner(Type);
    }

   
}
