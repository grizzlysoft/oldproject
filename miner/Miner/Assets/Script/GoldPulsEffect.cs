﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldPulsEffect : MonoBehaviour
{
    public UILabel lb_Text;

    public void SetGoldPulsEffect(string text)
    {
        this.gameObject.transform.localScale = Vector3.one;
        this.gameObject.transform.position = Vector3.zero;

        lb_Text.text = string.Format("+{0}G" ,text);

        this.gameObject.SetActive(true);
    }

    public void SetUpgrade(bool isSuccess)
    {
        this.gameObject.transform.localScale = Vector3.one;
        this.gameObject.transform.position = Vector3.zero;

        if (isSuccess)
        {
            SoundManager.Get.PlaySound(SoundType.success, true);
            lb_Text.text = "세공 성공";
            lb_Text.color = new Color(0, 1, 0);
        }
        else
        {
            SoundManager.Get.PlaySound(SoundType.fail, true);
            lb_Text.text = "세공 실패";
            lb_Text.color = new Color(1, 0, 0);
        }

        this.gameObject.SetActive(true);
    }

    public void Destroy()
    {
        Destroy(this.gameObject);
    }
}
