﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Time_Manager : Singleton<Time_Manager>
{
    //게임 실행하고 지난 시간
    public int PlayTime = 0;

    float AutoGoldTime = 1f; //자동골드 생성 주기
    float AutoGoldDeltaTime = 0f;

    // Use this for initialization
    public void Init ()
    {
        //이벤트 팝업 초기화
        PlayTime = 0;
        InvokeRepeating("UpdateTime_OneSec", 0f, 1.0f);
        InputBattleInvoke();
        InvokeRepeating("UpdateTime_OneMin", 60.0f, 60.0f);

        LoadOfflineGold();
    }
	
    //1초마다 들어오는 곳
	void UpdateTime_OneSec()
    {
        //게임 하고 지난 시간
        PlayTime++;

        Ingame.Get.IngameOneSecWork();

        //최고등급 보석에 따른 매초 수익 더하기
        GameData.AllOreGetGold();
        UI_Manager.Get.Refresh();
    }

    //3분마다 들어오는곳
    void UpdateTime_ThreeMin()
    {
        if (Ingame.Get.GetState() != IngameState.Battle)
        {
            Ingame.Get.StartWarning();
        }
    }

    void UpdateTime_OneMin()
    {
        Ingame.Get.Accident();

        //게임 시간 저장
        PlayerPrefs.SetString("SaveLastTime", System.DateTime.Now.ToString());
    }

    public void CancelBattleInvoke()
    {
        CancelInvoke("UpdateTime_ThreeMin");
    }

    public void InputBattleInvoke()
    {
        InvokeRepeating("UpdateTime_ThreeMin", 180f, 180f);
    }


    //버프 사용
    public void Start_PotionBuff(PotionType _type)
    {
        //포션 체크
        if (UI_Manager.Get.CheckHavePotion(1) == false)
        {
            Popup_Manager.Get.LowPotion.Show();
            return;
        }

        //각 버프별로 예외처리 - 이미적용중인 버프는 다시 못쓰게~
        switch (_type)
        {
            case PotionType.TapGoldx10:
                if (GameData.bTapGoldx10 == true)
                    return;
                GameData.bTapGoldx10 = true;
                Invoke("OffTapGoldx10", 10.0f);
                break;
            case PotionType.AutoGoldx10:
                if (GameData.bAutoGoldx10 == true)
                    return;
                GameData.bAutoGoldx10 = true;
                Invoke("OffAutoGoldx10", 10.0f);
                break;
            case PotionType.TapAtkx5:
                if (GameData.bTapAtkx5 == true)
                    return;
                GameData.bTapAtkx5 = true;
                Invoke("OffTapAtkx5", 10.0f);
                break;
            case PotionType.AutoAtkx5:
                if (GameData.bAutoAtkx5 == true)
                    return;
                GameData.bAutoAtkx5 = true;
                Invoke("OffAutoAtkx5", 10.0f);
                break;
            default:
                Debug.LogError("잘못된 포션 버프 타입");
                break;
        }

        GameData.AddPotion(-1);
        UI_Manager.Get.Refresh(true);
        UI_Manager.Get.SetBuffState();
    }

    void OffTapGoldx10()
    {
        GameData.bTapGoldx10 = false;
        UI_Manager.Get.Refresh();
        UI_Manager.Get.SetBuffState();
    }
    void OffAutoGoldx10()
    {
        GameData.bAutoGoldx10 = false;
        UI_Manager.Get.Refresh(true);
        UI_Manager.Get.SetBuffState();
    }
    void OffTapAtkx5()
    {
        GameData.bTapAtkx5 = false;
        UI_Manager.Get.Refresh(true);
        UI_Manager.Get.SetBuffState();
    }
    void OffAutoAtkx5()
    {
        GameData.bAutoAtkx5 = false;
        UI_Manager.Get.Refresh(true);
        UI_Manager.Get.SetBuffState();
    }

    //오프라인 획득 골드 지급
    void LoadOfflineGold()
    {
        //시간 경과후 
        string lastTime = PlayerPrefs.GetString("SaveLastTime","없음");
        if(lastTime == "없음")
            return;

        System.DateTime lastDateTime = System.DateTime.Parse(lastTime);
        System.TimeSpan compareTime = System.DateTime.Now - lastDateTime;

        Debug.Log("접종후 시간 : " + compareTime.TotalSeconds);
        int sec = (int)compareTime.TotalSeconds;

        //최대 24시간으로 적용
        if(sec > 86400)
            sec = 86400;

        //(초당수입 * 시간) / 30
        CustomDataType.BigInteger TotalGetGold;
        TotalGetGold = (GameData.CalcGetAutoGold_ALL() * sec) / 30;

        Debug.Log("오프라인 수익 : " + TotalGetGold.ToString());
        //시간을 다시 저장!
        PlayerPrefs.SetString("SaveLastTime", System.DateTime.Now.ToString());

        if (TotalGetGold == 0)
            return;
        //돈 입금
        GameData.AddGold(TotalGetGold);
        //팝업 띄우기
        Popup_Manager.Get.GetOfflineGold.Show(TotalGetGold.ToString());
    }

    /*
     *  [확인]을 클릭하여 보상 팝업을 닫으면 즉시 해당 골드가 획득 된다. - 확인 안누르고 팝업 뜰때로 조건 변경

- 전체 초당 수입의 1/30 만큼 지급한다. (최대 24시간까지 적용)
  인앱 구매로 인한 초당 수입 버프 효과가 적용된다.
  ex) 1분 뒤 재접속 하였을 때 
       현재 초당 수입이 10,000,000 골드 라면,
       (10000000 * 60) / 30 = 20,000,000 골드 획득!
       //저장 시간
PlayerPrefs.SetString("SaveLastTime", System.DateTime.Now.ToString());
 
//시간 경과후 
string lastTime = PlayerPrefs.GetString("SaveLastTime");
System.DateTime lastDateTime = System.DateTime.Parse(lastTime);
System.TimeSpan compareTime =  System.DateTime.Now - lastDateTime;
 
print(compareTime .TotalSeconds);

     */
}
