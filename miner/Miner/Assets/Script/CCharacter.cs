﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCharacter : MonoBehaviour
{
    public Animator Ani;
    string[] AniName = { "Attacking", "Dying", "Hurt", "Idle", "Idle Blinking", "Jump Loop", "Jump Start", "Taunt", "Walking", "Casting Spells", "Dying 01", "Dying 02" };

    string[] jewl_name = { "Cartoon RPG UI_Game Icon - Fire Resistant Gem",
                           "Cartoon RPG UI_Game Icon - Defense Gem",
                           "Cartoon RPG UI_Game Icon - Poison Resistant Gem",
                           "Cartoon RPG UI_Game Icon - Water Gem",
                           "Cartoon RPG UI_Game Icon - Diamond"};

    // Use this for initialization
    void Start ()
    {
        ChangeAni(AniType.Idle);
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void ChangeAni(AniType ani)
    {
        Ani.Play(AniName[(int)ani]);
    }

    
    public void GetJewl(int type_)
    {
        GameObject jewl = Instantiate(Resources.Load("Jwel/Jwel")) as GameObject;

        jewl.GetComponent<UISprite>().spriteName = jewl_name[type_];

        jewl.transform.parent = transform;
    }
}
