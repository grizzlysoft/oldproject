﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Exit : PopupBase
{

    public void OnClick_ExitPopup_YES()
    {
        Application.Quit();
    }

}
