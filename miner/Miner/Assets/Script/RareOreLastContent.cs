﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RareOreLastContent : MonoBehaviour
{
    OreType Type;
    int Grade;

    public UISprite sp_Icon_Name;
    public UILabel lb_Name;
    public UILabel lb_Count;
    public UILabel lb_Price;
    public UILabel lb_AutoGold;

    public void Init(OreType _type, int _grade)
    {
        Type = _type;
        Grade = _grade;

        sp_Icon_Name.spriteName = SpriteName();

        lb_Name.text = OreName();
        lb_Price.text = UI_Manager.Get.GetGoldString(GameData.OnePrice(Type, Grade).ToString(), 1);
        Refresh();
    }

    public void Refresh()
    {
        lb_Count.text = GameData.dic_Ore[Type][Grade].ToString();
        lb_AutoGold.text = string.Format("초당 {0} 골드 획득", AutoGetGold() );
    }

    public void OnClick_Sell()
    {
        if (GameData.dic_Ore[Type][Grade] > 0)
        {
            SoundManager.Get.PlaySound(SoundType.SellGold);

            GameObject go = Instantiate(UI_Manager.Get.GetEffect(UI_Effect.UpText), this.gameObject.transform);
            go.GetComponent<GoldPulsEffect>().SetGoldPulsEffect(UI_Manager.Get.GetGoldString(GameData.SellOre(Type, Grade), 3));
            UI_Manager.Get.Refresh();
        }
    }
    
    string SpriteName()
    {
        string name = "";
        switch (Type)
        {
            case OreType.Purple: name = "Cartoon RPG UI_Game Icon - Diamond"; break;
            case OreType.Blue: name = "Cartoon RPG UI_Game Icon - Water Gem"; break;
            case OreType.Green: name = "Cartoon RPG UI_Game Icon - Poison Resistant Gem"; break;
            case OreType.Yellow: name = "Cartoon RPG UI_Game Icon - Defense Gem"; break;
            case OreType.Red: name = "Cartoon RPG UI_Game Icon - Fire Resistant Gem"; break;
        }

        return name;
    }
    string OreName()
    {
        string name = "";
        
        switch (Type)
        {
            case OreType.Purple: name += "최고급 보라색 광석"; break;
            case OreType.Blue: name += "최고급 푸른색 광석"; break;
            case OreType.Green: name += "최고급 녹색 광석"; break;
            case OreType.Yellow: name += "최고급 황금 광석"; break;
            case OreType.Red: name += "최고급 붉은 광석"; break;
        }

        return name;
    }
    string AutoGetGold()
    {
        return UI_Manager.Get.GetGoldString(GameData.LastOreAutoGetGold(Type,Grade).ToString(), 1);
    }
}
