﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolemDestroyer : MonoBehaviour
{
    public SpriteRenderer [] sp = new SpriteRenderer[15];
    float DestroyTime = 1.5f;
    float deltatime = 0f;
    bool start = false;
    // Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (start)
        {
            deltatime += RealTime.deltaTime;
            if (deltatime < DestroyTime)
            {
                for (int i = 0; i < sp.Length; ++i)
                {
                    sp[i].color = new Color(1f, 1f, 1f, sp[i].color.a - 0.05f);
                }
            }
            else
            {
                for (int i = 0; i < sp.Length; ++i)
                {
                    sp[i].color = new Color(1f, 1f, 1f, 0f);
                }
                start = false;
                Ingame.Get.ReturnMiner();
            }
        }
	}

    public void StartDestroy()
    {
        deltatime = 0f;
        start = true;
    }

    public void Reset()
    {
        start = false;
        for (int i = 0; i < sp.Length; ++i)
        {
            sp[i].color = new Color(1f, 1f, 1f, 1f);
        }
    }
}
