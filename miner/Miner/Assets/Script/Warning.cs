﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warning : MonoBehaviour
{
    public TweenAlpha WarningText;
    public UILabel LevelText;
    public UISprite bg;

    float bgtime = 1f;
    float ShowTime = 5f;
    float deltatime = 0f;
	
    // Use this for initialization
	void Start ()
    {
       
	}

    public void SetStart()
    {
        deltatime = 0f;
        bg.height = 2;
        LevelText.text = string.Format("Lv {0} 골램 등장!", GameData.GolemLv);
        LevelText.GetComponent<TweenPosition>().ResetToBeginning();
        WarningText.ResetToBeginning();
        LevelText.gameObject.SetActive(false);
        WarningText.gameObject.SetActive(false);
        SoundManager.Get.PlaySound(SoundType.warning, true);
    }
	
	// Update is called once per frame
	void Update ()
    {
        deltatime += Time.deltaTime;
        if(bgtime >= deltatime)
        {
            bg.height += 4;

            if(bg.height > 150)
            {
                bg.height = 150;
            }
        }
        else
        {
            if(WarningText.gameObject.activeInHierarchy == false)
            {
                WarningText.gameObject.SetActive(true);
            }

            if (LevelText.gameObject.activeInHierarchy == false)
            {
                LevelText.gameObject.SetActive(true);
            }
        }

        if (deltatime >= ShowTime)
        {
            gameObject.SetActive(false);
            SoundManager.Get.StopSound(SoundType.warning);
            if (Ingame.Get.GetState() != IngameState.Battle)
            {
                Ingame.Get.EnterBattle();
            }
        }
	}
}
