﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameData
{
    //페이스북 최초 한번 포션 주기
    public static int FaceBook = 0;

    //세이브 데이터
    public static int PickaxeLv = 1; //광산주 채굴용 곡괭이 레벨
    public static int AttackAxeLv = 1; //광산주 공격력 레벨

    //광부들 레벨
    public static int Miner_Noob_Lv = 0;
    public static int Miner_Expert_Lv = 0;
    public static int Miner_Master_Lv = 0;

    //전문가 레벨
    public static int Expert_Warrior_Lv = 0;
    public static int Expert_Alchemist_Lv = 0;
    public static int Expert_Healer_Lv = 0;

    //소지 희귀 광석 갯수
    public static Dictionary<OreType, int[]> dic_Ore = new Dictionary<OreType,int[]>();

    //패키지 구매여부
    public static int BuyNewbiePackA;
    public static int BuyNewbiePackB;
    public static int BuyAlchemistPack;
    public static int BuyOwnerPack;

    //재화
    public static int Potion;
    public static CustomDataType.BigInteger Gold = new CustomDataType.BigInteger(0);
    
    //몬스터 킬수
    public static int BestKill;


    //사운드
    public static int Sound;


    //버프 - 저장 안하는 변수로 사용
    public static bool bTapGoldx10 = false;
    public static bool bAutoGoldx10 = false;
    public static bool bTapAtkx5 = false;
    public static bool bAutoAtkx5 = false;

    //골램 레벨
    public static int GolemLv = 1;

    //디버프(산재)
    public static int[] bAccident = { 0, 0 };

    //로드
    public static void LoadData()
    {

        FaceBook             = PlayerPrefs.GetInt("FaceBook", 0);

        PickaxeLv            = PlayerPrefs.GetInt("PickaxeLv", 1);
        AttackAxeLv          = PlayerPrefs.GetInt("AttackAxeLv", 1);

        Miner_Noob_Lv        = PlayerPrefs.GetInt("Miner_Noob_Lv", 0);
        Miner_Expert_Lv      = PlayerPrefs.GetInt("Miner_Expert_Lv", 0);
        Miner_Master_Lv      = PlayerPrefs.GetInt("Miner_Master_Lv", 0);

        Expert_Warrior_Lv    = PlayerPrefs.GetInt("Expert_Warrior_Lv", 0);
        Expert_Alchemist_Lv  = PlayerPrefs.GetInt("Expert_Alchemist_Lv", 0);
        Expert_Healer_Lv     = PlayerPrefs.GetInt("Expert_Healer_Lv", 0);
        
        dic_Ore[OreType.Purple] = new int[4];
        dic_Ore[OreType.Blue] = new int[4];
        dic_Ore[OreType.Green] = new int[4];
        dic_Ore[OreType.Yellow] = new int[4];
        dic_Ore[OreType.Red] = new int[4];

        string name = "";
        foreach (var item in dic_Ore)
        {
            for (int i = 0; i < item.Value.Length; i++)
            {
                name = string.Format("Ore_{0}_{1}", item.Key.ToString(), i);
                item.Value[i] = PlayerPrefs.GetInt(name, 0);
            }
        }


        BuyNewbiePackA = PlayerPrefs.GetInt("BuyNewbiePackA", 0);
        BuyNewbiePackB = PlayerPrefs.GetInt("BuyNewbiePackB", 0);
        BuyAlchemistPack = PlayerPrefs.GetInt("BuyAlchemistPack", 0);
        BuyOwnerPack = PlayerPrefs.GetInt("BuyOwnerPack", 0);


        Potion = PlayerPrefs.GetInt("Potion", 0);
        Gold = new CustomDataType.BigInteger(PlayerPrefs.GetString("Gold", "0"));

        //최대치 
        BestKill = PlayerPrefs.GetInt("BestKill", 0);


        Sound = PlayerPrefs.GetInt("Sound", 1);

        //임시변수 값셋팅
        bTapGoldx10 = false;
        bAutoGoldx10 = false;
        bTapAtkx5 = false;
        bAutoAtkx5 = false;

        GolemLv = PlayerPrefs.GetInt("GolemLv", 1);
        bAccident[0] = PlayerPrefs.GetInt("Accident01", 0);
        bAccident[1] = PlayerPrefs.GetInt("Accident02", 0);

        //저장값이 최고치를 넘으면 갱신
        //광산주
        if (PickaxeLv >= Table_Manager.Get.m_PickaxeData.Count - 1)
            PickaxeLv = Table_Manager.Get.m_PickaxeData.Count - 1;

        if (AttackAxeLv >= Table_Manager.Get.m_AttackAxeData.Count - 1)
            AttackAxeLv = Table_Manager.Get.m_AttackAxeData.Count - 1;

        //광
        if (Miner_Noob_Lv >= Table_Manager.Get.m_MinerNoobData.Count - 1)
            Miner_Noob_Lv = Table_Manager.Get.m_MinerNoobData.Count - 1;

        if (Miner_Expert_Lv >= Table_Manager.Get.m_MinerExpertData.Count - 1)
            Miner_Expert_Lv = Table_Manager.Get.m_MinerExpertData.Count - 1;

        if (Miner_Master_Lv >= Table_Manager.Get.m_MinerMasterData.Count - 1)
            Miner_Master_Lv = Table_Manager.Get.m_MinerMasterData.Count - 1;

        //용
        if (Expert_Warrior_Lv >= Table_Manager.Get.m_ExpertWarriorData.Count - 1)
            Expert_Warrior_Lv = Table_Manager.Get.m_ExpertWarriorData.Count - 1;

        if (Expert_Alchemist_Lv >= Table_Manager.Get.m_ExpertAlchemistData.Count - 1)
            Expert_Alchemist_Lv = Table_Manager.Get.m_ExpertAlchemistData.Count - 1;

        if (Expert_Healer_Lv >= Table_Manager.Get.m_ExpertHealerData.Count - 1)
            Expert_Healer_Lv = Table_Manager.Get.m_ExpertHealerData.Count - 1;
    }

    //값 로컬 저장 - 클라우드 저장
    public static void AllSave()
    {
        SavePickaxeLv();
        SaveAttackAxeLv();
        SavePotion();
        SaveGold();
        SaveMiner();
        SaveExpert();
        SaveOre();
        SaveBuyPack();
    }
    public static void SavePickaxeLv()
    {
        PlayerPrefs.SetInt("PickaxeLv", PickaxeLv);
    }
    public static void SaveAttackAxeLv()
    {
        PlayerPrefs.SetInt("AttackAxeLv", AttackAxeLv);
    }
    public static void SavePotion()
    {
        PlayerPrefs.SetInt("Potion", Potion);
    }
    public static void SaveGold()
    {
        PlayerPrefs.SetString("Gold", Gold.ToString());
    }
    public static void SaveMiner()
    {
        PlayerPrefs.SetInt("Miner_Noob_Lv", Miner_Noob_Lv);
        PlayerPrefs.SetInt("Miner_Expert_Lv", Miner_Expert_Lv);
        PlayerPrefs.SetInt("Miner_Master_Lv", Miner_Master_Lv);
    }
    public static void SaveExpert()
    {
        PlayerPrefs.SetInt("Expert_Warrior_Lv", Expert_Warrior_Lv);
        PlayerPrefs.SetInt("Expert_Alchemist_Lv", Expert_Alchemist_Lv);
        PlayerPrefs.SetInt("Expert_Healer_Lv", Expert_Healer_Lv);
    }
    public static void SaveOre()
    {
        string name = "";

        foreach (var item in dic_Ore)
        {
            for (int i = 0; i < item.Value.Length; i++)
            {
                name = string.Format("Ore_{0}_{1}", item.Key.ToString(),i);
                PlayerPrefs.SetInt(name, item.Value[i]);
            }
        }
        
    }
    public static void SaveBuyPack()
    {
        PlayerPrefs.SetInt("BuyNewbiePackA", BuyNewbiePackA);
        PlayerPrefs.SetInt("BuyNewbiePackB", BuyNewbiePackB);
        PlayerPrefs.SetInt("BuyAlchemistPack", BuyAlchemistPack);
        PlayerPrefs.SetInt("BuyOwnerPack", BuyOwnerPack);
    }
    public static void SaveBestKill()
    {
        PlayerPrefs.SetInt("BestKill", BestKill);
    }
    public static void SaveFaceBook()
    {
        PlayerPrefs.SetInt("FaceBook", FaceBook);
    }


    //클라우드 미저장
    public static void SaveGolemLv()
    {
        PlayerPrefs.SetInt("GolemLv", GolemLv);
    }
    public static void SaveAccident()
    {
        PlayerPrefs.SetInt("Accident01", bAccident[0]);
        PlayerPrefs.SetInt("Accident02", bAccident[1]);
    }
    public static void SaveSound()
    {
        PlayerPrefs.SetInt("Sound", Sound);
    }


    //재화 ++
    public static void AddGold(CustomDataType.BigInteger amount)
    {
        if (amount == 0)
            return;

         Gold += amount;
         SaveGold();
    }
    public static void AddGold(string stAmount)
    {
        CustomDataType.BigInteger amount = new CustomDataType.BigInteger(stAmount);

        if (amount == 0)
            return;

        Gold += amount;
        SaveGold();
    }

    public static void AddPotion(int value)
    {
        Potion += value;
        SavePotion();

#if UNITY_EDITOR
#elif UNITY_ANDROID
        GPGS_Manager.Get.ReportScore(GPGSIds.leaderboard_best_potion, Potion);
#else
#endif

    }

    public static void KillMonster(int value)
    {
        BestKill += value;
        SaveBestKill();

#if UNITY_EDITOR
#elif UNITY_ANDROID
        GPGS_Manager.Get.ReportScore(GPGSIds.leaderboard_kill_monster, BestKill);
#else
#endif
    }

    public static void AddPickaxeLv(int value)
    {
        PickaxeLv += value;
        SavePickaxeLv();
        CheckAchieveOwner();
    }
    public static void AddAttackAxeLv(int value)
    {
        AttackAxeLv += value;
        SaveAttackAxeLv();
        CheckAchieveOwner();
    }

    public static void MinerLevelUp(int target)
    {
        switch (target)
        {
            case 0: Miner_Noob_Lv++; break;
            case 1: Miner_Expert_Lv++; break;
            case 2: Miner_Master_Lv++; break;
            default:
                break;
        }

        SaveMiner();
        CheckAchieveMiner();
    }

    public static void ExpertLevelUp(int target)
    {
        switch (target)
        {
            case 0: Expert_Warrior_Lv++; break;
            case 1: Expert_Alchemist_Lv++; break;
            case 2: Expert_Healer_Lv++; break;
            default:
                break;
        }

        SaveExpert();
        CheckAchieveExpert();
    }

    public static void AddOre(OreType type, int grade , int count = 1)
    {
        dic_Ore[type][grade] += count;
        SaveOre();
    }
    public static bool CombineOre(OreType type, int grade)
    {
        //성공률 
        //30 , 10 , 1 + 연금술사
        int successRate = 0;
        switch (grade)
        {
            case 0: successRate = 5000; break;
            case 1: successRate = 2000; break;
            case 2: successRate = 1000; break;
        }

        //패키지 효과
        if(BuyAlchemistPack == 1)
        {
            successRate += 1000;
        }

        //연금술사 레벨 가져오기
        successRate = successRate + Table_Manager.Get.m_ExpertAlchemistData[Expert_Alchemist_Lv].abillityValue;
        int Rnd = Random.Range(0, 10000);

        if(successRate < Rnd)
        {
            //실패
            dic_Ore[type][grade] -= 5;
            SaveOre();
            return false;
        }
        else
        {
            //성공
            dic_Ore[type][grade] -= 5;
            dic_Ore[type][grade + 1]++;
            SaveOre();
            return true;
        }

    }
    public static string SellOre(OreType Type, int Grade)
    {
        //판매
        //보유수량 x 개당 금액
        CustomDataType.BigInteger temp = new CustomDataType.BigInteger();
        temp = OnePrice(Type, Grade) * dic_Ore[Type][Grade];

        AddGold(temp);
        dic_Ore[Type][Grade] = 0;
        SaveOre();

        return temp.ToString();
    }

    public static void BuyPack(PotionType packType)
    {
        switch (packType)
        {
            case PotionType.NewbiePackA:
                BuyNewbiePackA = 1;
                AddPotion(30);
                break;
            case PotionType.NewbiePackB:
                BuyNewbiePackB = 1;
                AddPotion(30);
                break;
            case PotionType.AlchemistPack:
                BuyAlchemistPack = 1;
                AddPotion(100);
                break;
            case PotionType.OwnerPack:
                BuyOwnerPack = 1;
                AddPotion(1000);
                break;
            default:
                Debug.LogError("잘못된 패키지 타입");
                break;
        }

        SavePotion();
        SaveBuyPack();

        UI_Manager.Get.Refresh(true);
    }
    public static void AddGolemLv()
    {
        GolemLv++;
        if(GolemLv > Table_Manager.Get.m_MonsterData.Count)
        {
            GolemLv = Table_Manager.Get.m_MonsterData.Count;
        }
        SaveGolemLv();
    }

    //패키지,기타 효과로 인한 탭당 수입 계산
    public static CustomDataType.BigInteger CalcTapGoldPrice()
    {
        CustomDataType.BigInteger temp = Table_Manager.Get.m_PickaxeData[PickaxeLv].get_gold;

        //증가 % 계산
        //만약 초보자 패키지 구입
        if (BuyNewbiePackA == 1)
            temp += Table_Manager.Get.m_PickaxeData[PickaxeLv].get_gold / 10;
        if (BuyOwnerPack == 1)
            temp += (Table_Manager.Get.m_PickaxeData[PickaxeLv].get_gold / 10) * 3;
        if (bTapGoldx10 == true)
            temp *= 10;

        return temp;
    }

    /// <summary>
    /// 패키지,기타 효과로 인한 자동 수입 계산
    /// </summary>
    /// <param name="type">0.Miner_Noob_Lv   1.Miner_Expert_Lv    2.Miner_Master_Lv </param>
    public static CustomDataType.BigInteger CalcGetAutoGold_Miner(int type , bool addgold = true)
    {
        CustomDataType.BigInteger temp = 0;

        switch (type)
        {
            case 0: temp = CalcMinerAutoGold(Table_Manager.Get.m_MinerNoobData[Miner_Noob_Lv].get_gold); break;
            case 1: temp = CalcMinerAutoGold(Table_Manager.Get.m_MinerExpertData[Miner_Expert_Lv].get_gold); break;
            case 2: temp = CalcMinerAutoGold(Table_Manager.Get.m_MinerMasterData[Miner_Master_Lv].get_gold); break;
                
            default:
                return 0;
        }

        //산재에 대한 처리
        if (type == 0 && bAccident[0] != 0)
            temp = temp / 2;

        if (type == 1 && bAccident[1] != 0)
            temp = temp / 2;

        if(addgold == true)
            AddGold(temp);

        return temp;
    }
    static CustomDataType.BigInteger CalcMinerAutoGold(CustomDataType.BigInteger gold)
    {
        if (gold == 0)
            return 0;

        CustomDataType.BigInteger temp = new CustomDataType.BigInteger();
        temp += gold;

        //증가 % 계산
        if (BuyNewbiePackB == 1)
            temp += gold / 10;
        if (BuyOwnerPack == 1)
            temp += (gold / 10) * 3;
        if (bAutoGoldx10 == true)
            temp *= 10;

        return temp;
    }
    public static CustomDataType.BigInteger CalcGetAutoGold_ALL()
    {
        return CalcGetAutoGold_Miner(0 ,false) + CalcGetAutoGold_Miner(1, false) + CalcGetAutoGold_Miner(2, false);
    }

    //광석 개당 가격 등
    public static CustomDataType.BigInteger OnePrice(OreType Type, int Grade)
    {
        return Table_Manager.Get.m_RareOreData[(int)Type].Grade[Grade];
    }
    public static CustomDataType.BigInteger LastOreAutoGetGold(OreType Type, int Grade)
    {
        // *보석갯수
        return Table_Manager.Get.m_RareOreData[(int)Type].LastAutoGold * dic_Ore[Type][Grade];
    }
    public static void AllOreGetGold()
    {
        AddGold(LastOreAutoGetGold(OreType.Purple, 3) +
               LastOreAutoGetGold(OreType.Blue, 3) +
               LastOreAutoGetGold(OreType.Green, 3) +
               LastOreAutoGetGold(OreType.Yellow, 3) +
               LastOreAutoGetGold(OreType.Red, 3));
    }

    static void CheckAchieveOwner()
    {
        if (AttackAxeLv >= 250)
            GPGS_Manager.Get.UnlockAchievement(GPGSIds.achievement_battle_owner);
        if (PickaxeLv >= 250)
            GPGS_Manager.Get.UnlockAchievement(GPGSIds.achievement_mining_owner);
        if (AttackAxeLv >= 500 && PickaxeLv >= 500)
            GPGS_Manager.Get.UnlockAchievement(GPGSIds.achievement_universal_owner);
    }

    static void CheckAchieveMiner()
    {
        if (Miner_Noob_Lv >= 500 && Miner_Expert_Lv >= 500 && Miner_Master_Lv >= 500)
            GPGS_Manager.Get.UnlockAchievement(GPGSIds.achievement_the_best_slave);
    }

    static void CheckAchieveExpert()
    {
        if (Expert_Warrior_Lv >= 100)
            GPGS_Manager.Get.UnlockAchievement(GPGSIds.achievement_hired_solider_100_level);
        if (Expert_Healer_Lv >= 100)
            GPGS_Manager.Get.UnlockAchievement(GPGSIds.achievement_priest_100_level);
        if (Expert_Alchemist_Lv >= 100)
            GPGS_Manager.Get.UnlockAchievement(GPGSIds.achievement_alchemist_100_level);
    }
    
}
