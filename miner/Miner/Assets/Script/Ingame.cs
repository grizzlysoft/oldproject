﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum IngameState
{
    Miner,
    Battle,
}

public class Ingame : Singleton<Ingame>
{
    public UIScrollView scroll;
    public UIGrid Grid;

    public Transform bg1;
    public Transform bg2;

    public CMinerManager MinerRoot;
    public CBattleRoot BattleRoot;

    public GameObject Warning;

    IngameState state = IngameState.Miner;
    
	// Use this for initialization
	void Start ()
    {
        scroll.GetComponent<UICenterOnChild>().CenterOn(bg1);
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    public void InitMiner()
    {
        if (GameData.Miner_Noob_Lv > 0)
        {
            MinerRoot.SettingGetMiner(0);
        }
        else
        {
            MinerRoot.Miners[0].gameObject.SetActive(false);
        }

        if (GameData.Miner_Expert_Lv > 0)
        {
            MinerRoot.SettingGetMiner(1);
        }
        else
        {
            MinerRoot.Miners[1].gameObject.SetActive(false);
        }

        if (GameData.Miner_Master_Lv > 0)
        {
            MinerRoot.SettingGetMiner(2);
        }
        else
        {
            MinerRoot.Miners[2].gameObject.SetActive(false);
        }

        if(GameData.bAccident[0] != 0)
        {
            MinerRoot.Miners[0].ChangeAni(AniType.Hurt);
            MinerRoot.MinerBtn[0].SetActive(true);
        }

        if (GameData.bAccident[1] != 0)
        {
            MinerRoot.Miners[1].ChangeAni(AniType.Hurt);
            MinerRoot.MinerBtn[1].SetActive(true);
        }
    }

    public void IngameOneSecWork()
    {
        if (state == IngameState.Miner)
            MinerRoot.VeiwGold(GameData.CalcGetAutoGold_Miner(0), GameData.CalcGetAutoGold_Miner(1), GameData.CalcGetAutoGold_Miner(2));
        else if (state == IngameState.Battle)
            BattleRoot.SpecialistAttack();

        MinerRoot.JwelMining();
    }

    public void MinerHire(int type)
    {
        MinerRoot.SettingGetMiner(type);
    }

    public void Accident()
    {
        if (GameData.Miner_Expert_Lv > 0)
        {
            int acc = Random.Range(0, 10);
            if (acc < 3 && GameData.bAccident[0] == 0)
            {
                //다침
                GameData.bAccident[0] = 1;
                GameData.SaveAccident();
                if (MinerRoot.ActiveMiners[0] == true)
                {
                    MinerRoot.Miners[0].ChangeAni(AniType.Hurt);
                    MinerRoot.MinerBtn[0].SetActive(true);
                }
            }
        }

        if (GameData.Miner_Master_Lv > 0)
        {
            int acc2 = Random.Range(0, 10);
            if (acc2 < 3 && GameData.bAccident[1] == 0)
            {
                //다침
                GameData.bAccident[1] = 1;
                GameData.SaveAccident();
                if (MinerRoot.ActiveMiners[1] == true)
                {
                    MinerRoot.Miners[1].ChangeAni(AniType.Hurt);
                    MinerRoot.MinerBtn[1].SetActive(true);
                }
            }
        }
    }

    void SetMinerBtn()
    {
        if (GameData.bAccident[1] != 0)
        {
            MinerRoot.MinerBtn[1].SetActive(true);
        }
        else
            MinerRoot.MinerBtn[1].SetActive(false);

        if (GameData.bAccident[0] != 0)
        {
            MinerRoot.MinerBtn[0].SetActive(true);
        }
        else
            MinerRoot.MinerBtn[0].SetActive(false);
    }
	
	// Update is called once per frame
	void Update ()
    {
        //if (Input.GetKeyDown("1"))
        //{
        //    scroll.GetComponent<UICenterOnChild>().CenterOn(bg1);
        //}

        //if (Input.GetKeyDown("2"))
        //{
        //    scroll.GetComponent<UICenterOnChild>().CenterOn(bg2);
        //    BattleRoot.InitBattleView();
        //    state = IngameState.Battle;
        //}
    }

    public IngameState GetState()
    {
        return state;
    }

    public void ReturnMiner()
    {
        scroll.GetComponent<UICenterOnChild>().CenterOn(bg1);
        state = IngameState.Miner;
        Time_Manager.Get.InputBattleInvoke();
        GameData.GolemLv++;
        GameData.SaveGolemLv();
        BattleRoot.AddGolemIndex();
        SetMinerBtn();
    }

    public void EnterBattle()
    {
        Time_Manager.Get.CancelBattleInvoke();
        scroll.GetComponent<UICenterOnChild>().CenterOn(bg2);
        BattleRoot.InitBattleView();
        state = IngameState.Battle;
        MinerRoot.MinerBtn[0].SetActive(false);
        MinerRoot.MinerBtn[1].SetActive(false);
    }

    public void HealMiner(int type)
    {
        GameData.bAccident[type] = 0;
        MinerRoot.Miners[type].ChangeAni(AniType.Attacking);
        MinerRoot.MinerBtn[type].SetActive(false);
        GameData.SaveAccident();
    }

    public void StartWarning()
    {
        Warning.GetComponent<Warning>().SetStart();
        Warning.SetActive(true);
    }
}
