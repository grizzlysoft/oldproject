﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_LowPotion : PopupBase
{
    
    public void OnClick_ShowAD()
    {
        SoundManager.Get.PlaySound(SoundType.Click);
#if UNITY_EDITOR
        Debug.Log("광고 시청 클릭");
#elif UNITY_ANDROID
        Ads_Manager.Get.ShowRewardedAd();
#endif
        Hide();
    }

}
