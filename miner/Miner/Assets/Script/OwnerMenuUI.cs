﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OwnerMenuUI : MonoBehaviour
{
    public UILabel lb_Pickaxe_Name;
    public UILabel lb_Pickaxe_GetGold;
    public UILabel lb_Pickaxe_Cost;
    public GameObject go_Pickaxe_Block;

    public UILabel lb_AttackAxe_Name;
    public UILabel lb_AttackAxe_GetGold;
    public UILabel lb_AttackAxe_Cost;
    public GameObject go_AttackAxe_Block;

    public void Init()
    {
    }

    public void Refresh()
    {
        lb_Pickaxe_Name.text = "광산주 채굴용 곡괭이 Lv " + GameData.PickaxeLv;
        lb_Pickaxe_GetGold.text = UI_Manager.Get.GetGoldString(Table_Manager.Get.m_PickaxeData[GameData.PickaxeLv].get_gold.ToString(), 3);
        go_Pickaxe_Block.SetActive(!UI_Manager.Get.CheckHaveMoney(Table_Manager.Get.m_PickaxeData[GameData.PickaxeLv].cost));

        lb_AttackAxe_Name.text = "광산주 공격력 Lv " + GameData.AttackAxeLv;
        lb_AttackAxe_GetGold.text = UI_Manager.Get.GetGoldString(Table_Manager.Get.m_AttackAxeData[GameData.AttackAxeLv].dmg.ToString(), 3);
        go_AttackAxe_Block.SetActive(!UI_Manager.Get.CheckHaveMoney(Table_Manager.Get.m_AttackAxeData[GameData.AttackAxeLv].cost));

        if (GameData.PickaxeLv >= Table_Manager.Get.m_PickaxeData.Count - 1)
            lb_Pickaxe_Cost.text = "최고레벨";
        else
            lb_Pickaxe_Cost.text = UI_Manager.Get.GetGoldString(Table_Manager.Get.m_PickaxeData[GameData.PickaxeLv].cost.ToString(), 1);

        if (GameData.AttackAxeLv >= Table_Manager.Get.m_AttackAxeData.Count - 1)
            lb_AttackAxe_Cost.text = "최고레벨";
        else
            lb_AttackAxe_Cost.text = UI_Manager.Get.GetGoldString(Table_Manager.Get.m_AttackAxeData[GameData.AttackAxeLv].cost.ToString(), 1);

    }

    public void OnClick_PickaxeLvUp()
    {
        if (GameData.PickaxeLv >= Table_Manager.Get.m_PickaxeData.Count - 1)
        {
            Popup_Manager.Get.Error.Show("최고 레벨 입니다");
            return;
        }

        if (UI_Manager.Get.CheckHaveMoney(Table_Manager.Get.m_PickaxeData[GameData.PickaxeLv].cost) == false)
            return;

        SoundManager.Get.PlaySound(SoundType.Click);

        GameData.AddGold(-Table_Manager.Get.m_PickaxeData[GameData.PickaxeLv].cost);
        GameData.AddPickaxeLv(1);
        UI_Manager.Get.Refresh();

        Popup_Manager.Get.CheckLevelUpReward("광산주 채굴용 곡괭이", GameData.PickaxeLv);
    }
    public void OnClick_AttackLvUp()
    {
        if (GameData.AttackAxeLv >= Table_Manager.Get.m_AttackAxeData.Count - 1)
        {
            Popup_Manager.Get.Error.Show("최고 레벨 입니다");
            return;
        }

        if (UI_Manager.Get.CheckHaveMoney(Table_Manager.Get.m_AttackAxeData[GameData.AttackAxeLv].cost) == false)
            return;

        SoundManager.Get.PlaySound(SoundType.Click);

        GameData.AddGold(-Table_Manager.Get.m_AttackAxeData[GameData.AttackAxeLv].cost);
        GameData.AddAttackAxeLv(1);
        UI_Manager.Get.Refresh();

        Popup_Manager.Get.CheckLevelUpReward("광산주 공격력", GameData.AttackAxeLv);
    }
}
