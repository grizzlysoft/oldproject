﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpertMenuUI : MonoBehaviour
{
    public UISprite sp_Warrior_Icon;
    public UILabel lb_Warrior_Hire;
    public UILabel lb_Warrior_Name;
    public UILabel lb_Warrior_Dmg;
    public UILabel lb_Warrior_Cost;
    public GameObject go_Warrior_Block;

    public UISprite sp_Alchemist_Icon;
    public UILabel lb_Alchemist_Hire;
    public UILabel lb_Alchemist_Name;
    public UILabel lb_Alchemist_Dmg;
    public UILabel lb_Alchemist_Cost;
    public UILabel lb_Alchemist_Desc;
    public GameObject go_Alchemist_Block;


    public UISprite sp_Healer_Icon;
    public UILabel lb_Healer_Hire;
    public UILabel lb_Healer_Name;
    public UILabel lb_Healer_Dmg;
    public UILabel lb_Healer_Cost;
    public UILabel lb_Healer_Desc;
    public GameObject go_Healer_Block;

    int HireCost = 2;
    
    //변수가 너무 길어서 짦게 쓰기 위한 임시 변수 + 계속적으로 호출 방지(포인터)
    List<Table_Manager.ExpertTable_Warrior> m_WarriorTable;
    List<Table_Manager.ExpertTable_Alchemist> m_AlchemistTable;
    List<Table_Manager.ExpertTable_Healer> m_HealerTable;

    public void Init()
    {
        m_WarriorTable = Table_Manager.Get.m_ExpertWarriorData;
        m_AlchemistTable = Table_Manager.Get.m_ExpertAlchemistData;
        m_HealerTable = Table_Manager.Get.m_ExpertHealerData;
    }

    public void Refresh()
    {
        //용병
        if(GameData.Expert_Warrior_Lv == 0)
        {
            sp_Warrior_Icon.spriteName = "Cartoon RPG UI_Game Icon - Healt Potion";
            lb_Warrior_Hire.text = "고용";
            lb_Warrior_Name.text = "용병";
            lb_Warrior_Dmg.text = "전투 전문가";
            lb_Warrior_Cost.text = HireCost.ToString();
            go_Warrior_Block.SetActive(!UI_Manager.Get.CheckHavePotion(HireCost));
        }
        else
        {
            sp_Warrior_Icon.spriteName = "Cartoon RPG UI_Game Icon - Coin";
            lb_Warrior_Hire.text = "훈련 비용";
            lb_Warrior_Name.text = "용병 Lv " + GameData.Expert_Warrior_Lv;
            lb_Warrior_Dmg.text = UI_Manager.Get.GetGoldString(m_WarriorTable[GameData.Expert_Warrior_Lv].dmg.ToString(), 3);
            go_Warrior_Block.SetActive(!UI_Manager.Get.CheckHaveMoney(m_WarriorTable[GameData.Expert_Warrior_Lv].cost));

            if (GameData.Expert_Warrior_Lv >= m_WarriorTable.Count - 1)
                lb_Warrior_Cost.text = "최고레벨";
            else
                lb_Warrior_Cost.text = UI_Manager.Get.GetGoldString(m_WarriorTable[GameData.Expert_Warrior_Lv].cost.ToString(), 1);
            
        }

        //연금술사
        if (GameData.Expert_Alchemist_Lv == 0)
        {
            sp_Alchemist_Icon.spriteName = "Cartoon RPG UI_Game Icon - Healt Potion";
            lb_Alchemist_Hire.text = "고용";
            lb_Alchemist_Name.text = "연금술사";
            lb_Alchemist_Dmg.text = "광석 세공 전문가";
            lb_Alchemist_Cost.text = HireCost.ToString();
            lb_Alchemist_Desc.text = "광석 세공 확률 증가";

            go_Alchemist_Block.SetActive(!UI_Manager.Get.CheckHavePotion(HireCost));
        }
        else
        {
            sp_Alchemist_Icon.spriteName = "Cartoon RPG UI_Game Icon - Coin";
            lb_Alchemist_Hire.text = "훈련 비용";
            lb_Alchemist_Name.text = "연금술사 Lv " + GameData.Expert_Alchemist_Lv;
            lb_Alchemist_Dmg.text = UI_Manager.Get.GetGoldString(m_AlchemistTable[GameData.Expert_Alchemist_Lv].dmg.ToString(), 3);
            lb_Alchemist_Desc.text = AlchemistAbillityDesc(m_AlchemistTable[GameData.Expert_Alchemist_Lv].abillityValue);
            go_Alchemist_Block.SetActive(!UI_Manager.Get.CheckHaveMoney(m_AlchemistTable[GameData.Expert_Alchemist_Lv].cost));

            if (GameData.Expert_Alchemist_Lv >= m_AlchemistTable.Count - 1)
                lb_Alchemist_Cost.text = "최고레벨";
            else
                lb_Alchemist_Cost.text = UI_Manager.Get.GetGoldString(m_AlchemistTable[GameData.Expert_Alchemist_Lv].cost.ToString(), 1);

        }

        //힐러
        if (GameData.Expert_Healer_Lv == 0)
        {
            sp_Healer_Icon.spriteName = "Cartoon RPG UI_Game Icon - Healt Potion";
            lb_Healer_Hire.text = "고용";
            lb_Healer_Name.text = "사제";
            lb_Healer_Dmg.text = "회복 전문가";
            lb_Healer_Cost.text = HireCost.ToString();
            lb_Healer_Desc.text = "산재 치유 비용 감소";

            go_Healer_Block.SetActive(!UI_Manager.Get.CheckHavePotion(HireCost));
        }
        else
        {
            sp_Healer_Icon.spriteName = "Cartoon RPG UI_Game Icon - Coin";
            lb_Healer_Hire.text = "훈련 비용";
            lb_Healer_Name.text = "사제 Lv " + GameData.Expert_Healer_Lv;
            lb_Healer_Dmg.text = UI_Manager.Get.GetGoldString(m_HealerTable[GameData.Expert_Healer_Lv].dmg.ToString(), 3);
            lb_Healer_Desc.text = HealerAbillityDesc(m_HealerTable[GameData.Expert_Healer_Lv].abillityValue);
            go_Healer_Block.SetActive(!UI_Manager.Get.CheckHaveMoney(m_HealerTable[GameData.Expert_Healer_Lv].cost));

            if (GameData.Expert_Healer_Lv >= m_HealerTable.Count - 1)
                lb_Healer_Cost.text = "최고레벨";
            else
                lb_Healer_Cost.text = UI_Manager.Get.GetGoldString(m_HealerTable[GameData.Expert_Healer_Lv].cost.ToString(), 1);

        }
    }

    string AlchemistAbillityDesc(int value)
    {
        float fValue = value / 100.0f;
        return string.Format("광석 세공 확률 {0}% 증가", fValue.ToString("F2"));
    }
    string HealerAbillityDesc(int value)
    {
        int fValue = value / 100;
        return string.Format("즉시 치유 비용 {0}% 감소", fValue.ToString());
    }

    public void OnClick_WarriorLvUp()
    {
        if (GameData.Expert_Warrior_Lv >= m_WarriorTable.Count - 1)
        {
            Popup_Manager.Get.Error.Show("최고 레벨 입니다");
            return;
        }

        if (GameData.Expert_Warrior_Lv == 0)
        {
            if (UI_Manager.Get.CheckHavePotion(HireCost) == false)
                return;

            GameData.AddPotion(-HireCost);
            GameData.ExpertLevelUp(0);
            Ingame.Get.BattleRoot.GetSpecialist(0);
        }
        else
        {
            if (UI_Manager.Get.CheckHaveMoney(m_WarriorTable[GameData.Expert_Warrior_Lv].cost) == false)
                return;

            GameData.AddGold(-m_WarriorTable[GameData.Expert_Warrior_Lv].cost);
            GameData.ExpertLevelUp(0);

            Popup_Manager.Get.CheckLevelUpReward("용병", GameData.Expert_Warrior_Lv);
        }

        SoundManager.Get.PlaySound(SoundType.Click);

        UI_Manager.Get.Refresh();
    }

    public void OnClick_AlchemistLvUp()
    {
        if (GameData.Expert_Alchemist_Lv >= m_AlchemistTable.Count - 1)
        {
            Popup_Manager.Get.Error.Show("최고 레벨 입니다");
            return;
        }

        if (GameData.Expert_Alchemist_Lv == 0)
        {
            if (UI_Manager.Get.CheckHavePotion(HireCost) == false)
                return;

            GameData.AddPotion(-HireCost);
            GameData.ExpertLevelUp(1);
            Ingame.Get.BattleRoot.GetSpecialist(1);
        }
        else
        {
            if (UI_Manager.Get.CheckHaveMoney(m_AlchemistTable[GameData.Expert_Alchemist_Lv].cost) == false)
                return;

            GameData.AddGold(-m_AlchemistTable[GameData.Expert_Alchemist_Lv].cost);
            GameData.ExpertLevelUp(1);

            Popup_Manager.Get.CheckLevelUpReward("연금술사", GameData.Expert_Alchemist_Lv);
        }

        SoundManager.Get.PlaySound(SoundType.Click);

        UI_Manager.Get.Refresh();
    }

    public void OnClick_HealerLvUp()
    {
        if (GameData.Expert_Healer_Lv >= m_HealerTable.Count - 1)
        {
            Popup_Manager.Get.Error.Show("최고 레벨 입니다");
            return;
        }

        if (GameData.Expert_Healer_Lv == 0)
        {
            if (UI_Manager.Get.CheckHavePotion(HireCost) == false)
                return;

            GameData.AddPotion(-HireCost);
            GameData.ExpertLevelUp(2);
            Ingame.Get.BattleRoot.GetSpecialist(2);
        }
        else
        {
            if (UI_Manager.Get.CheckHaveMoney(m_HealerTable[GameData.Expert_Healer_Lv].cost) == false)
                return;

            GameData.AddGold(-m_HealerTable[GameData.Expert_Healer_Lv].cost);
            GameData.ExpertLevelUp(2);

            Popup_Manager.Get.CheckLevelUpReward("사제", GameData.Expert_Healer_Lv);
        }

        SoundManager.Get.PlaySound(SoundType.Click);

        UI_Manager.Get.Refresh();
    }

}
