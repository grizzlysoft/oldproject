﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_GetOfflineGold : PopupBase
{
    public UILabel lb_Count;

    public void Show(string count)
    {
        lb_Count.text = UI_Manager.Get.GetGoldString(count ,3);
        base.Show();
    }
}
