﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupBase : MonoBehaviour
{
    public bool isShow()
    {
        return this.gameObject.activeSelf;
    }

    public void Show()
    {
        this.gameObject.SetActive(true);
    }

    public void Hide_NoSound()
    {
        this.gameObject.SetActive(false);
    }

    public void Hide()
    {
        SoundManager.Get.PlaySound(SoundType.Click);
        this.gameObject.SetActive(false);
    }
}
