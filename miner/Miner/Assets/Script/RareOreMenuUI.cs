﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum OreType
{
    Purple = 0,
    Blue,
    Green,
    Yellow,
    Red,
}

public class RareOreMenuUI : MonoBehaviour
{
    int currentTap = 0;

    public GameObject go_Tap0;
    public GameObject go_Tap1;
    public GameObject go_Tap2;
    public GameObject go_Tap3;

    //광석 - 이건 직접 그냥 붙여넣자
    public List<RareOreContent> li_Ore_0 = new List<RareOreContent>();
    public List<RareOreContent> li_Ore_1 = new List<RareOreContent>();
    public List<RareOreContent> li_Ore_2 = new List<RareOreContent>();
    public List<RareOreLastContent> li_Ore_3 = new List<RareOreLastContent>();


    public void Init()
    {
        for(int i=0; i<li_Ore_0.Count; i++)
        {
            li_Ore_0[i].Init((OreType)i, 0);
        }

        for (int i = 0; i < li_Ore_1.Count; i++)
        {
            li_Ore_1[i].Init((OreType)i, 1);
        }

        for (int i = 0; i < li_Ore_2.Count; i++)
        {
            li_Ore_2[i].Init((OreType)i, 2);
        }

        for (int i = 0; i < li_Ore_3.Count; i++)
        {
            li_Ore_3[i].Init((OreType)i, 3);
        }

        currentTap = 0;
    }

    public void Refresh()
    {
        go_Tap0.SetActive(false);
        go_Tap1.SetActive(false);
        go_Tap2.SetActive(false);
        go_Tap3.SetActive(false);


        //현제 탭 열기
        switch (currentTap)
        {
            case 0:
                {
                    go_Tap0.SetActive(true);

                    for (int i = 0; i < li_Ore_0.Count; i++)
                    {
                        li_Ore_0[i].Refresh();
                    }
                }
                break;
            case 1:
                {
                    go_Tap1.SetActive(true);

                    for (int i = 0; i < li_Ore_1.Count; i++)
                    {
                        li_Ore_1[i].Refresh();
                    }
                }
                break;
            case 2:
                {
                    go_Tap2.SetActive(true);

                    for (int i = 0; i < li_Ore_2.Count; i++)
                    {
                        li_Ore_2[i].Refresh();
                    }
                }
                break;
            case 3:
                {
                    go_Tap3.SetActive(true);
                    for (int i = 0; i < li_Ore_3.Count; i++)
                    {
                        li_Ore_3[i].Refresh();
                    }
                }
                break;
        }
    }
    
    public void OnClickNext()
    {
        if(currentTap < 3)
            currentTap++;
        SoundManager.Get.PlaySound(SoundType.Click);

        Refresh();
    }

    public void OnClickPrev()
    {
        if(currentTap > 0)
            currentTap--;
        SoundManager.Get.PlaySound(SoundType.Click);

        Refresh();
    }
}
