﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_BuyPackage : PopupBase
{
    public UILabel lb_Title;
    public UILabel lb_Count;
    public UILabel lb_Desc;

    public void Show(PotionType type)
    {
        switch (type)
        {
            case PotionType.NewbiePackA:
                lb_Title.text = "초보자 패키지A 구매 완료";
                lb_Count.text = "30";
                lb_Desc.text = "탭당 수입 10% 증가";
                break;
            case PotionType.NewbiePackB:
                lb_Title.text = "초보자 패키지B 구매 완료";
                lb_Count.text = "30";
                lb_Desc.text = "초당 수입 10% 증가";
                break;
            case PotionType.AlchemistPack:
                lb_Title.text = "연금술사 패키지 구매 완료";
                lb_Count.text = "100";
                lb_Desc.text = "세공 확률 10% 증가";
                break;
            case PotionType.OwnerPack:
                lb_Title.text = "광산주 패키지 구매 완료";
                lb_Count.text = "1000";
                lb_Desc.text = "초당,탭당 수입 30% 증가";
                break;
        }

        base.Show();
    }
    

}
