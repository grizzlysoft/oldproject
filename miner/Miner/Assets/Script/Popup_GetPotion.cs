﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_GetPotion : PopupBase
{
    public UILabel lb_Count;

    public void Show(int count)
    {
        lb_Count.text = count.ToString();
        base.Show();
    }
}
