﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionMenuUI : MonoBehaviour
{
    public UILabel lb_Sound;

    public void Init()
    {

    }

    public void Refresh()
    {
        if (GameData.Sound == 1)
            lb_Sound.text = "사운드 On";
        else
            lb_Sound.text = "사운드 Off";
    }

    public void OnClick_SoundOnOff()
    {
        SoundManager.Get.PlaySound(SoundType.Click);

        if (GameData.Sound == 1)
            GameData.Sound = 0;
        else
            GameData.Sound = 1;

        GameData.SaveSound();
        Refresh();
        SoundManager.Get.SetSoundState();
    }

    public void OnClick_Save()
    {
        SoundManager.Get.PlaySound(SoundType.Click);

        GPGS_Manager.Get.SaveToCloud();
    }


    public void OnClick_Load()
    {
        if(Ingame.Get.GetState() == IngameState.Battle)
        {
            Popup_Manager.Get.Error.Show("전투중에는 로딩하실수 없습니다.");
            return;
        }
        SoundManager.Get.PlaySound(SoundType.Click);

        GPGS_Manager.Get.LoadFromCloud();
    }


    public void OnClick_FaceBook()
    {
        SoundManager.Get.PlaySound(SoundType.Click);

        Application.OpenURL("https://www.facebook.com/GrizzlySoft/");

        if (GameData.FaceBook == 0)
        {
            GameData.FaceBook = 1;
            GameData.SaveFaceBook();
            GameData.AddPotion(1);
            Popup_Manager.Get.GetPotion.Show(1);
        }
    }

    public void OnClick_Google()
    {
        SoundManager.Get.PlaySound(SoundType.Click);

        Application.OpenURL("market://details?id=com.GrizzlySoft.MinerCraft");
    }

    public void OnClick_DeleteAllData()
    {
        SoundManager.Get.PlaySound(SoundType.Click);

        PlayerPrefs.DeleteAll();
        GameData.LoadData();
        Ingame.Get.InitMiner();
    }
}
