﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickEffectController : MonoBehaviour
{
    public Camera UI_Cam;
    public GameObject contents;
    public GameObject pool;
    public List<UI2DSpriteAnimation> li_Pool = new List<UI2DSpriteAnimation>();

	// Use this for initialization
	void Awake ()
    {
        GameObject temp;
        for(int i=0; i<50; i++)
        {
            temp = Instantiate(contents, pool.transform) as GameObject;
            temp.transform.position = Vector3.zero;
            temp.transform.localScale = Vector3.one;
            temp.SetActive(false);
            li_Pool.Add(temp.GetComponent<UI2DSpriteAnimation>());
        }
    }
	
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            OnEffect();
        }
    }

    public void OnEffect()
    {
        Vector3 pos = UI_Cam.ScreenToWorldPoint(Input.mousePosition);
        for (int i=0; i<li_Pool.Count; i++)
        {
            if (li_Pool[i].gameObject.activeSelf == false)
            {
                li_Pool[i].gameObject.transform.position = pos;
                li_Pool[i].gameObject.SetActive(true);
                li_Pool[i].ResetToBeginning();
                li_Pool[i].Play();
                return;
            }
        }
    }

}
