﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Manager : Singleton<Popup_Manager>
{
    public Popup_Exit           Exit;
    public Popup_BuyPackage     BuyPackage;
    public Popup_BuyFail        BuyFail;
    public Popup_GetPotion      GetPotion;
    public Popup_ADFail         ADFail;
    public Popup_LowPotion      LowPotion;
    public Popup_GetOfflineGold GetOfflineGold;
    public Popup_Heal           Heal;
    public Popup_NoGold         NoGold;
    public Popup_Error          Error;
    public Popup_Loading        Loading;
    public Popup_LevelReward    LevelReward;


    public void Init()
    {
        AllHide();
    }
    public void AllHide()
    {
        Exit.Hide_NoSound();
        BuyPackage.Hide_NoSound();
        BuyFail.Hide_NoSound();
        GetPotion.Hide_NoSound();
        ADFail.Hide_NoSound();
        LowPotion.Hide_NoSound();
        GetOfflineGold.Hide_NoSound();
        Heal.Hide_NoSound();
        NoGold.Hide_NoSound();
        Error.Hide_NoSound();
        Loading.Hide_NoSound();
        LevelReward.Hide_NoSound();
    }
    public bool isPopup()
    {
        if (Exit.isShow() == true)
            return true;
        if (BuyPackage.isShow() == true)
            return true;
        if (BuyFail.isShow() == true)
            return true;
        if (GetPotion.isShow() == true)
            return true;
        if (ADFail.isShow() == true)
            return true;
        if (LowPotion.isShow() == true)
            return true;
        if (GetOfflineGold.isShow() == true)
            return true;
        if (Heal.isShow() == true)
            return true;
        if (NoGold.isShow() == true)
            return true;
        if (Error.isShow() == true)
            return true;
        if (LevelReward.isShow() == true)
            return true;

        return false;
    }
    

    //레벨업 팝업 조건 체크
    public void CheckLevelUpReward(string text, int level)
    {
        if (level % 100 != 0)
            return;

        int count = level / 100;
        GameData.AddPotion(count);
        UI_Manager.Get.Refresh();

        LevelReward.Show(text, level, count);
    }

}
