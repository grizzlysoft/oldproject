﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinerMenuUI : MonoBehaviour
{
    public UILabel lb_Noob_Hire;
    public UILabel lb_Noob_Name;
    public UILabel lb_Noob_GetGold;
    public UILabel lb_Noob_Cost;
    public GameObject go_Noob_Block;

    public UISprite sp_Expert_Icon;
    public UILabel lb_Expert_Hire;
    public UILabel lb_Expert_Name;
    public UILabel lb_Expert_GetGold;
    public UILabel lb_Expert_Cost;
    public UILabel lb_Expert_RareChance;
    public GameObject go_Expert_Block;
    public GameObject go_Expert_AllBlock;

    public UISprite sp_Master_Icon;
    public UILabel lb_Master_Hire;
    public UILabel lb_Master_Name;
    public UILabel lb_Master_GetGold;
    public UILabel lb_Master_Cost;
    public UILabel lb_Master_RareChance;
    public GameObject go_Master_Block;
    public GameObject go_Master_AllBlock;

    int noob_HireCost = 10000;
    int expert_HireCost = 1;
    int master_HireCost = 2;

    List<Table_Manager.MinerTable_Noob> m_NoobTable;
    List<Table_Manager.MinerTable_Expert> m_ExpertTable;
    List<Table_Manager.MinerTable_Master> m_MasterTable;


    public void Init()
    {
        m_NoobTable = Table_Manager.Get.m_MinerNoobData;
        m_ExpertTable = Table_Manager.Get.m_MinerExpertData;
        m_MasterTable = Table_Manager.Get.m_MinerMasterData;
    }

    public void Refresh()
    {
        SetMiner_Noob();
        SetMiner_Expert();
        SetMiner_Master();
    }
    
    void SetMiner_Noob()
    {
        if (GameData.Miner_Noob_Lv == 0)
        {
            lb_Noob_Hire.text = "고용";
            lb_Noob_Name.text = "초급 숙련 광부";
            lb_Noob_GetGold.text = "자동채굴";
            lb_Noob_Cost.text = noob_HireCost.ToString();
            go_Noob_Block.SetActive(!UI_Manager.Get.CheckHaveMoney(noob_HireCost));
        }
        else
        {
            lb_Noob_Hire.text = "훈련 비용";
            lb_Noob_Name.text = "초급 숙련 광부 Lv " + GameData.Miner_Noob_Lv;
            lb_Noob_GetGold.text = UI_Manager.Get.GetGoldString(m_NoobTable[GameData.Miner_Noob_Lv].get_gold.ToString(), 3);

            if (GameData.Miner_Noob_Lv >= m_NoobTable.Count - 1)
                lb_Noob_Cost.text = "최고레벨";
            else
                lb_Noob_Cost.text = UI_Manager.Get.GetGoldString(m_NoobTable[GameData.Miner_Noob_Lv].cost.ToString(), 1);

            go_Noob_Block.SetActive(!UI_Manager.Get.CheckHaveMoney(m_NoobTable[GameData.Miner_Noob_Lv].cost));
        }
    }
    void SetMiner_Expert()
    {
        if (GameData.Miner_Expert_Lv == 0)
        {
            sp_Expert_Icon.spriteName = "Cartoon RPG UI_Game Icon - Healt Potion";
            lb_Expert_Hire.text = "고용";
            lb_Expert_Name.text = "중급 숙련 광부";
            lb_Expert_GetGold.text = "자동채굴";
            lb_Expert_Cost.text = expert_HireCost.ToString();
            lb_Expert_RareChance.text = "희귀광석 획득 확률 증가";

            //만약 초급 레벨이 0이면 무조건 
            if (GameData.Miner_Noob_Lv == 0)
            {
                go_Expert_AllBlock.SetActive(true);
                go_Expert_Block.SetActive(false);
            }
            else
            {
                go_Expert_AllBlock.SetActive(false);
                go_Expert_Block.SetActive(!UI_Manager.Get.CheckHavePotion(expert_HireCost));
            }
        }
        else
        {
            sp_Expert_Icon.spriteName = "Cartoon RPG UI_Game Icon - Coin";
            lb_Expert_Hire.text = "훈련 비용";
            lb_Expert_Name.text = "중급 숙련 광부 Lv " + GameData.Miner_Expert_Lv;
            lb_Expert_GetGold.text = UI_Manager.Get.GetGoldString(m_ExpertTable[GameData.Miner_Expert_Lv].get_gold.ToString(), 3);

            if (GameData.Miner_Expert_Lv >= m_ExpertTable.Count - 1)
                lb_Expert_Cost.text = "최고레벨";
            else
                lb_Expert_Cost.text = UI_Manager.Get.GetGoldString(m_ExpertTable[GameData.Miner_Expert_Lv].cost.ToString(), 1); ;

            lb_Expert_RareChance.text = RareChanceDesc(m_ExpertTable[GameData.Miner_Expert_Lv].rareChance);
            go_Expert_Block.SetActive(!UI_Manager.Get.CheckHaveMoney(m_ExpertTable[GameData.Miner_Expert_Lv].cost));
        }

    }
    void SetMiner_Master()
    {
        if (GameData.Miner_Master_Lv == 0)
        {
            sp_Master_Icon.spriteName = "Cartoon RPG UI_Game Icon - Healt Potion";
            lb_Master_Hire.text = "고용";
            lb_Master_Name.text = "고급 숙련 광부";
            lb_Master_GetGold.text = "자동채굴";
            lb_Master_Cost.text = master_HireCost.ToString();
            lb_Master_RareChance.text = "희귀광석 획득 확률 증가\n산업재해를 입지 않는다";
            //중급이 오픈안되있으면 안댐
            if (GameData.Miner_Expert_Lv == 0)
            {
                go_Master_AllBlock.SetActive(true);
                go_Master_Block.SetActive(false);
            }
            else
            {
                go_Master_AllBlock.SetActive(false);
                go_Master_Block.SetActive(!UI_Manager.Get.CheckHavePotion(master_HireCost));
            }
        }
        else
        {
            sp_Master_Icon.spriteName = "Cartoon RPG UI_Game Icon - Coin";
            lb_Master_Hire.text = "훈련 비용";
            lb_Master_Name.text = "고급 숙련 광부 Lv " + GameData.Miner_Master_Lv;
            lb_Master_GetGold.text = UI_Manager.Get.GetGoldString(m_MasterTable[GameData.Miner_Master_Lv].get_gold.ToString(), 3);

            if (GameData.Miner_Master_Lv >= m_MasterTable.Count - 1)
                lb_Master_Cost.text = "최고레벨";
            else
                lb_Master_Cost.text = UI_Manager.Get.GetGoldString(m_MasterTable[GameData.Miner_Master_Lv].cost.ToString(), 1); ;

            lb_Master_RareChance.text = RareChanceDesc(m_MasterTable[GameData.Miner_Master_Lv].rareChance) +
                "\n산업재해를 입지 않는다";
            go_Master_Block.SetActive(!UI_Manager.Get.CheckHaveMoney(m_MasterTable[GameData.Miner_Master_Lv].cost));
        }
    }


    string RareChanceDesc(int value)
    {
        float fValue = value / 100.0f;
        return string.Format("희귀광석 획득 확률 {0}% 증가", fValue.ToString("F2"));
    }

    public void OnClick_MinerNoobLvUp()
    {
        if (GameData.Miner_Noob_Lv >= m_NoobTable.Count - 1)
        {
            Popup_Manager.Get.Error.Show("최고 레벨 입니다");
            return;
        }


        if (GameData.Miner_Noob_Lv == 0)
        {
            if (UI_Manager.Get.CheckHaveMoney(noob_HireCost) == false)
                return;

            GameData.AddGold(-noob_HireCost);
            GameData.MinerLevelUp(0);
            Ingame.Get.MinerHire(0);
        }
        else
        {
            if (UI_Manager.Get.CheckHaveMoney(m_NoobTable[GameData.Miner_Noob_Lv].cost) == false)
                return;

            GameData.AddGold(-m_NoobTable[GameData.Miner_Noob_Lv].cost);
            GameData.MinerLevelUp(0);

            Popup_Manager.Get.CheckLevelUpReward("초급 숙련 광부", GameData.Miner_Noob_Lv);
        }
        SoundManager.Get.PlaySound(SoundType.Click);

        UI_Manager.Get.Refresh();
    }
    public void OnClick_MinerExpertLvUp()
    {
        if (GameData.Miner_Expert_Lv >= m_ExpertTable.Count - 1)
        {
            Popup_Manager.Get.Error.Show("최고 레벨 입니다");
            return;
        }

        if (GameData.Miner_Noob_Lv == 0)
            return;


        if (GameData.Miner_Expert_Lv == 0)
        {
            if (UI_Manager.Get.CheckHavePotion(expert_HireCost) == false)
                return;

            GameData.AddPotion(-expert_HireCost);
            GameData.MinerLevelUp(1);
            Ingame.Get.MinerHire(1);
        }
        else
        {
            if (UI_Manager.Get.CheckHaveMoney(m_ExpertTable[GameData.Miner_Expert_Lv].cost) == false)
                return;

            GameData.AddGold(-m_ExpertTable[GameData.Miner_Expert_Lv].cost);
            GameData.MinerLevelUp(1);

            Popup_Manager.Get.CheckLevelUpReward("중급 숙련 광부", GameData.Miner_Expert_Lv);
        }
        SoundManager.Get.PlaySound(SoundType.Click);

        UI_Manager.Get.Refresh();
    }
    public void OnClick_MinerMasterLvUp()
    {
        if (GameData.Miner_Master_Lv >= m_MasterTable.Count - 1)
        {
            Popup_Manager.Get.Error.Show("최고 레벨 입니다");
            return;
        }

        if (GameData.Miner_Expert_Lv == 0)
            return;

        if (GameData.Miner_Master_Lv == 0)
        {
            if (UI_Manager.Get.CheckHavePotion(master_HireCost) == false)
                return;

            GameData.AddPotion(-master_HireCost);
            GameData.MinerLevelUp(2);
            Ingame.Get.MinerHire(2);
        }
        else
        {
            if (UI_Manager.Get.CheckHaveMoney(m_MasterTable[GameData.Miner_Master_Lv].cost) == false)
                return;

            GameData.AddGold(-m_MasterTable[GameData.Miner_Master_Lv].cost);
            GameData.MinerLevelUp(2);

            Popup_Manager.Get.CheckLevelUpReward("고급 숙련 광부", GameData.Miner_Master_Lv);
        }
        SoundManager.Get.PlaySound(SoundType.Click);

        UI_Manager.Get.Refresh();
    }

}
