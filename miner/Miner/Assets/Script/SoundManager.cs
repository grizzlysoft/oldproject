﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum SoundType
{
    BGM_001 = 0,
    Pickaxe,
    Click,
    SellGold,
    hit,
    success,
    fail,
    warning,
    Count,
}

public class SoundManager : Singleton<SoundManager>
{
    [System.Serializable]
    public class SoundData
    {
        public AudioSource Sound = null;
        public int Ref = 0;
    }

    public bool isBGM;
    public bool isEffect;
    SoundType currentBGM = SoundType.BGM_001;

    public List<SoundData> li_Sound = new List<SoundData>();
    
    //
    public void PlaySound(SoundType _type, bool isSetFirst = false)
    {
        if (isEffect == false)
            return;

        if (isSetFirst == true)
            li_Sound[(int)_type].Sound.time = 0;

        li_Sound[(int)_type].Sound.Play();
    }

    public void StopSound(SoundType _type)
    {
        li_Sound[(int)_type].Sound.Stop();
    }

    public void SetSpeed(SoundType _type, float _value)
    {
        li_Sound[(int)_type].Sound.pitch = _value;
    }

    public void SetSoundState()
    {
        isBGM = (GameData.Sound == 1) ? true : false;
        isEffect = (GameData.Sound == 1) ? true : false;
        CheckBGMSound();
        CheckEffectSound();
    }

    //BGM 관련
    public void ChangeBGM(SoundType _type)
    {
        li_Sound[(int)currentBGM].Sound.Stop();
        li_Sound[(int)_type].Sound.Play();
        currentBGM = _type;
    }

    void SoundVolume(SoundType _type, float _volume)
    {
        li_Sound[(int)_type].Sound.volume = _volume;
    }

    //BGM on/off 여부에 따라서 bgm 키고 끄기
    public void CheckBGMSound()
    {
        for (int i = 0; i < li_Sound.Count; i++)
        {
            if (CheckBGM((SoundType)i) == true)
            {
                if (isBGM == false)
                    SoundVolume((SoundType)i, 0);
                else
                    SoundVolume((SoundType)i, 1);
            }
        }
    }

    //Effect on/off 여부에 따라서 Effect 키고 끄기
    public void CheckEffectSound()
    {
        for (int i = 0; i < li_Sound.Count; i++)
        {
            if (CheckBGM((SoundType)i) == false)
            {
                if (isEffect == false)
                    SoundVolume((SoundType)i, 0);
                else
                    SoundVolume((SoundType)i, 1);
            }
        }
    }


    bool CheckBGM(SoundType Type)
    {
        switch (Type)
        {
            case SoundType.BGM_001:return true;
        }

        return false;
    }


    ///일단 안쓸꺼
    public void PlaySound_Ref(SoundType _type)
    {
        if (isEffect == false)
            return;

        li_Sound[(int)_type].Ref++;
        li_Sound[(int)_type].Sound.Play();

        //Debug.Log(li_Sound[(int)_type].Ref + " 사운드 발생횟수");
    }

    public void StopSound_Ref(SoundType _type)
    {
        li_Sound[(int)_type].Ref--;
        //Debug.Log(li_Sound[(int)_type].Ref + " 사운드 발생횟수");

        if (li_Sound[(int)_type].Ref <= 0)
        {
            li_Sound[(int)_type].Ref = 0;
            li_Sound[(int)_type].Sound.Stop();
        }
    }

    public void PlayOneShotSound(SoundType _type, bool isSetFirst = false)
    {
        if (isEffect == false)
            return;

        if (isSetFirst == true)
            li_Sound[(int)_type].Sound.time = 0;

        li_Sound[(int)_type].Sound.PlayOneShot(li_Sound[(int)_type].Sound.clip);
    }
}
