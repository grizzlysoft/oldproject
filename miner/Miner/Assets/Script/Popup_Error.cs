﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Error : PopupBase
{
    public UILabel lb_Text;

    public void Show(string text)
    {
        lb_Text.text = text;

        base.Show();
    }

}
