﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GolemState
{
    Live,
    Dying,
    Die,
}

public class CBattleRoot : MonoBehaviour
{
    public CCharacter[] Golem = new CCharacter[4];
    public CCharacter[] Specialist = new CCharacter[3];

    public UISprite GolemHp;
    public UILabel GolemHpValue;

    CustomDataType.BigInteger Hp = new CustomDataType.BigInteger();
    CustomDataType.BigInteger MaxHp = new CustomDataType.BigInteger();
    int GolemIndex = 0;

    GolemState Golemstate = GolemState.Live;

    // Use this for initialization
    void Start ()
    {
		
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void InitBattleView()
    {
        //오브젝트를 먼저 세팅
        //용병 셋팅
        for (int i = 0; i < Golem.Length; ++i)
        {
            Golem[i].ChangeAni(AniType.Attacking);
        }

        if (GameData.Expert_Warrior_Lv > 0)
        {
            Specialist[0].gameObject.SetActive(true);
            Specialist[0].ChangeAni(AniType.Attacking);
        }
        else
            Specialist[0].gameObject.SetActive(false);

        if (GameData.Expert_Alchemist_Lv > 0)
        {
            Specialist[1].gameObject.SetActive(true);
            Specialist[1].ChangeAni(AniType.CastingSpell);
        }
        else
            Specialist[1].gameObject.SetActive(false);

        if (GameData.Expert_Healer_Lv > 0)
        {
            Specialist[2].gameObject.SetActive(true);
            Specialist[2].ChangeAni(AniType.CastingSpell);
        }
        else
            Specialist[2].gameObject.SetActive(false);

        for(int i = 0; i < Golem.Length; ++i)
        {
            Golem[i].gameObject.SetActive(false);
        }
        Golem[GolemIndex].GetComponent<GolemDestroyer>().Reset();
        Golem[GolemIndex].gameObject.SetActive(true);
        Golem[GolemIndex].ChangeAni(AniType.Attacking);

        MaxHp = Table_Manager.Get.m_MonsterData[GameData.GolemLv].hp;
        Hp = MaxHp;
        RefreshGolemHp();

        Golemstate = GolemState.Live;
    }

    public void SpecialistAttack()
    {
        if (Golemstate == GolemState.Live)
        {
            CustomDataType.BigInteger damage = new CustomDataType.BigInteger();

            damage = Table_Manager.Get.m_ExpertWarriorData[GameData.Expert_Warrior_Lv].dmg
                   + Table_Manager.Get.m_ExpertAlchemistData[GameData.Expert_Alchemist_Lv].dmg
                   + Table_Manager.Get.m_ExpertHealerData[GameData.Expert_Healer_Lv].dmg;

            if (GameData.bAutoAtkx5)
            {
                damage = damage * 5;
            }

            Hp = Hp - damage;
            if (Hp < 0)
            {
                Hp = 0;
            }
            RefreshGolemHp();
        }
    }

    public void RefreshGolemHp()
    {
        float value;

        ulong hp = CustomDataType.BigInteger.ToUInt64(Hp);
        ulong maxhp = CustomDataType.BigInteger.ToUInt64(MaxHp);
       
        value = hp / (float)maxhp;

        GolemHp.fillAmount = value;
        GolemHpValue.text = ValueCutting(UI_Manager.Get.GetGoldString(Hp.ToString(),3))  + " / " + ValueCutting(UI_Manager.Get.GetGoldString(MaxHp.ToString(), 3));

        if(hp == 0)
        {
            Golemstate = GolemState.Dying;
            for (int i = 0; i < Specialist.Length; ++i)
            {
                Specialist[i].ChangeAni(AniType.Taunt);
            }
            Golem[GolemIndex].ChangeAni(AniType.IdleBlinking);
            Golem[GolemIndex].GetComponent<GolemDestroyer>().StartDestroy();

            GameObject gold1 = GameObject.Instantiate(Resources.Load("GetGold") as GameObject);
            gold1.GetComponent<UILabel>().text = "+" + UI_Manager.Get.GetGoldString(Table_Manager.Get.m_MonsterData[GameData.GolemLv].rewardGold.ToString(), 1);
            gold1.transform.parent = Golem[GolemIndex].transform;
            gold1.transform.localScale = new Vector3(0.15f, 0.15f, 0.15f);
            GameData.AddGold(Table_Manager.Get.m_MonsterData[GameData.GolemLv].rewardGold);
            GameData.KillMonster(1);
            UI_Manager.Get.Refresh();
        }
    }

    string ValueCutting(string s)
    {
        string temp = s.Replace("0000해", "");
        temp = temp.Replace("0000경", "");
        temp = temp.Replace("0000조", "");
        temp = temp.Replace("0000억", "");
        temp = temp.Replace("0000만", "");

        return temp;
    }

    CustomDataType.BigInteger Dmg;
    public void TouchAttackGolem()
    {
        if (Golemstate == GolemState.Live)
        {
            SoundManager.Get.PlayOneShotSound(SoundType.hit, true);
            Dmg = Table_Manager.Get.m_AttackAxeData[GameData.AttackAxeLv].dmg;

            if (GameData.bTapAtkx5 == true)
                Dmg = Dmg * 5;

            Hp = Hp - Dmg;

            if (Hp < 0)
            {
                Hp = 0;
            }
            RefreshGolemHp();
        }
    }

    public void GetSpecialist(int type_)
    {
        Specialist[type_].gameObject.SetActive(true);
        if (type_ == 0)
            Specialist[type_].ChangeAni(AniType.Attacking);
        else
            Specialist[type_].ChangeAni(AniType.CastingSpell);
    }

    public void AddGolemIndex()
    {
        GolemIndex++;

        if(GolemIndex >= Golem.Length)
        {
            GolemIndex = 0;
        }
    }
}
