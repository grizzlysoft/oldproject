﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AniType
{
    Attacking,
    Dying,
    Hurt,
    Idle,
    IdleBlinking,
    JumpLoop,
    JumpStart,
    Taunt,
    Walking,
    CastingSpell,
    Dying01,
    Dying02,
}

public class CMinerManager : MonoBehaviour
{
    public CCharacter[] Miners = new CCharacter[3];

    public bool[] ActiveMiners = { false, false, false };

    public GameObject[] MinerBtn = new GameObject[2];

	// Use this for initialization
	void Start ()
    {
       
	}

	// Update is called once per frame
	void Update ()
    {
		
    }

    public void SettingGetMiner(int type_)
    {
        if(type_ < GameData.bAccident.Length)
        {
            GameData.bAccident[type_] = 0;
        }

        Miners[type_].gameObject.SetActive(true);
        Miners[type_].ChangeAni(AniType.Attacking);
        ActiveMiners[type_] = true;
    }

   public void VeiwGold(CustomDataType.BigInteger a, CustomDataType.BigInteger b, CustomDataType.BigInteger c)
   {
        if (a > 0)
        {
            GameObject gold1 = GameObject.Instantiate(Resources.Load("GetGold") as GameObject);
            gold1.GetComponent<UILabel>().text = "+" + UI_Manager.Get.GetGoldString(a.ToString(), 1);
            gold1.transform.parent = Miners[0].transform;
            gold1.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
        }

        if (b > 0)
        {
            GameObject gold2 = GameObject.Instantiate(Resources.Load("GetGold") as GameObject);
            gold2.GetComponent<UILabel>().text = "+" + UI_Manager.Get.GetGoldString(b.ToString(), 1);
            gold2.transform.parent = Miners[1].transform;
            gold2.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
        }

        if (c > 0)
        {
            GameObject gold3 = GameObject.Instantiate(Resources.Load("GetGold") as GameObject);
            gold3.GetComponent<UILabel>().text = "+" + UI_Manager.Get.GetGoldString(c.ToString(), 1);
            gold3.transform.parent = Miners[2].transform;
            gold3.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
        }
    }

    public void JwelMining()
    {
        if (GameData.Miner_Expert_Lv > 0)
        {
            int r = Random.Range(0, 10000);

            if (r <= Table_Manager.Get.m_MinerExpertData[GameData.Miner_Expert_Lv].rareChance)
            {
                int j = Random.Range(0, 100);
                if (j == 0)
                {
                    Miners[1].GetJewl(0);
                    GameData.AddOre(OreType.Red, 0);
                }
                else if (j < 2)
                {
                    Miners[1].GetJewl(1);
                    GameData.AddOre(OreType.Yellow, 0);
                }
                else if (j < 7)
                {
                    Miners[1].GetJewl(2);
                    GameData.AddOre(OreType.Green, 0);
                }
                else if (j < 20)
                {
                    Miners[1].GetJewl(3);
                    GameData.AddOre(OreType.Blue, 0);
                }
                else
                {
                    Miners[1].GetJewl(4);
                    GameData.AddOre(OreType.Purple, 0);
                }

                UI_Manager.Get.Refresh();
            }
        }

        if (GameData.Miner_Master_Lv > 0)
        {
            int r2 = Random.Range(0, 10000);

            if (r2 <= Table_Manager.Get.m_MinerMasterData[GameData.Miner_Master_Lv].rareChance)
            {
                int j = Random.Range(0, 100);
                if (j == 0)
                {
                    Miners[2].GetJewl(0);
                    GameData.AddOre(OreType.Red, 0);
                }
                else if (j < 2)
                {
                    Miners[2].GetJewl(1);
                    GameData.AddOre(OreType.Yellow, 0);
                }
                else if (j < 7)
                {
                    Miners[2].GetJewl(2);
                    GameData.AddOre(OreType.Green, 0);
                }
                else if (j < 20)
                {
                    Miners[2].GetJewl(3);
                    GameData.AddOre(OreType.Blue, 0);
                }
                else
                {
                    Miners[2].GetJewl(4);
                    GameData.AddOre(OreType.Purple, 0);
                }

                UI_Manager.Get.Refresh();
            }
        }
    }

    public void MinerClicked_01()
    {
        //Debug.Log("MinerClicked_01");
        Popup_Manager.Get.Heal.Show(0);
    }

    public void MinerClicked_02()
    {
        Popup_Manager.Get.Heal.Show(1);
    }
}
