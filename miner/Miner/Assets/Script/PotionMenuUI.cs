﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionMenuUI : MonoBehaviour
{
    public List<PotionContant> li_Potion;

    public void Init()
    {
        for(int i=0; i<li_Potion.Count; i++)
        {
            li_Potion[i].Init((PotionType)i);
        }

    }

    public void Refresh()
    {

    }

    public void OnClickAD()
    {
        SoundManager.Get.PlaySound(SoundType.Click);

#if UNITY_EDITOR
        Debug.Log("광고 시청 클릭");
#elif UNITY_ANDROID
        Ads_Manager.Get.ShowRewardedAd();
#endif
    }

}
