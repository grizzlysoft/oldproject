﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RareOreContent : MonoBehaviour
{
    OreType Type;
    int Grade;

    public UISprite sp_Icon_Name;
    public UILabel lb_Name;
    public UILabel lb_Count;
    public UILabel lb_Price;
    public UISprite sp_Icon_Craft;
    public GameObject go_Block;
    
    public void Init(OreType _type , int _grade)
    {
        Type = _type;
        Grade = _grade;

        sp_Icon_Name.spriteName = SpriteName();
        sp_Icon_Craft.spriteName = sp_Icon_Name.spriteName;

        lb_Name.text = OreName();
        lb_Price.text = UI_Manager.Get.GetGoldString(GameData.OnePrice(Type, Grade).ToString(), 1);
        Refresh();
    }

    public void Refresh()
    {
        lb_Count.text = GameData.dic_Ore[Type][Grade].ToString();
        go_Block.SetActive(GameData.dic_Ore[Type][Grade] < 5);
    }

    public void OnClick_Sell()
    {
        if (GameData.dic_Ore[Type][Grade] > 0)
        {
            SoundManager.Get.PlaySound(SoundType.SellGold);

            string selloreGold = GameData.SellOre(Type, Grade);

            GameObject go = Instantiate(UI_Manager.Get.GetEffect(UI_Effect.UpText), this.gameObject.transform);
            go.GetComponent<GoldPulsEffect>().SetGoldPulsEffect(UI_Manager.Get.GetGoldString(selloreGold, 3));
            UI_Manager.Get.Refresh();
        }
    }

    public void OnClick_Craft()
    {
        if (GameData.dic_Ore[Type][Grade] < 5)
            return;

        //세공
        bool CombineSuccess = GameData.CombineOre(Type, Grade);

        GameObject go = Instantiate(UI_Manager.Get.GetEffect(UI_Effect.UpText), this.gameObject.transform);
        go.GetComponent<GoldPulsEffect>().SetUpgrade(CombineSuccess);

        UI_Manager.Get.Refresh();
        SoundManager.Get.PlaySound(SoundType.Click);

    }

    string SpriteName()
    {
        string name ="";
        switch (Type)
        {
            case OreType.Purple: name = "Cartoon RPG UI_Game Icon - Diamond"; break;
            case OreType.Blue:   name = "Cartoon RPG UI_Game Icon - Water Gem"; break;
            case OreType.Green:  name = "Cartoon RPG UI_Game Icon - Poison Resistant Gem"; break;
            case OreType.Yellow: name = "Cartoon RPG UI_Game Icon - Defense Gem"; break;
            case OreType.Red:    name = "Cartoon RPG UI_Game Icon - Fire Resistant Gem"; break;
        }

        return name;
    }
    string OreName()
    {
        string name = "";

        if(Grade != 0)
            name = Grade + "차 세공 ";
       
        switch (Type)
        {
            case OreType.Purple:    name += "보라색 광석";break;
            case OreType.Blue:      name += "푸른색 광석";break;
            case OreType.Green:     name += "녹색 광석"; break;
            case OreType.Yellow:    name += "황금 광석"; break;
            case OreType.Red:       name += "붉은 광석"; break;
        }

        return name;
    }
    
}
