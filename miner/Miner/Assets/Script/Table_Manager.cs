﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Table_Manager : Singleton<Table_Manager>
{
    public List<PickaxeTable> m_PickaxeData = new List<PickaxeTable>();
    public List<AttackAxeTable> m_AttackAxeData = new List<AttackAxeTable>();

    public List<MinerTable_Noob> m_MinerNoobData = new List<MinerTable_Noob>();
    public List<MinerTable_Expert> m_MinerExpertData = new List<MinerTable_Expert>();
    public List<MinerTable_Master> m_MinerMasterData = new List<MinerTable_Master>();

    public List<ExpertTable_Warrior> m_ExpertWarriorData        = new List<ExpertTable_Warrior>();
    public List<ExpertTable_Alchemist> m_ExpertAlchemistData    = new List<ExpertTable_Alchemist>();
    public List<ExpertTable_Healer> m_ExpertHealerData          = new List<ExpertTable_Healer>();

    public List<MonsterTable> m_MonsterData = new List<MonsterTable>();
    public List<RareOreTable> m_RareOreData = new List<RareOreTable>();

    private void Awake()
    {
        Load_PickaxeData();
        Load_AttackAxeData();

        Load_MinerNoobData();
        Load_MinerExpertData();
        Load_MinerMasterData();

        Load_ExpertWarriorData();
        Load_ExpertAlchemistData();
        Load_ExpertHealerData();

        Load_MonsterData();
        Load_RareOreData();
    }

    public struct PickaxeTable
    {
        public int lv;
        public CustomDataType.BigInteger get_gold;
        public CustomDataType.BigInteger cost;
    }
    public void Load_PickaxeData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/채굴용곡괭이", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 1; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                PickaxeTable item = new PickaxeTable();

                int cnt = 0;

                item.lv = int.Parse(stral[cnt]); ++cnt;
                item.get_gold = new CustomDataType.BigInteger(stral[cnt]); ++cnt;
                item.cost = new CustomDataType.BigInteger(stral[cnt]); ++cnt;

                m_PickaxeData.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Load_LocalizingTable");
        }

    }

    public struct AttackAxeTable
    {
        public int lv;
        public CustomDataType.BigInteger dmg;
        public CustomDataType.BigInteger cost;
    }
    public void Load_AttackAxeData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/응징용곡괭이", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 1; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                AttackAxeTable item = new AttackAxeTable();

                int cnt = 0;

                item.lv = int.Parse(stral[cnt]); ++cnt;
                item.dmg = new CustomDataType.BigInteger(stral[cnt]); ++cnt;
                item.cost = new CustomDataType.BigInteger(stral[cnt]); ++cnt;

                m_AttackAxeData.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Load_LocalizingTable");
        }

    }

    public struct MinerTable_Noob
    {
        public int lv;
        public CustomDataType.BigInteger get_gold;
        public CustomDataType.BigInteger cost;
    }
    public struct MinerTable_Expert
    {
        public int lv;
        public CustomDataType.BigInteger get_gold;
        public int rareChance;
        public CustomDataType.BigInteger cost;
    }
    public struct MinerTable_Master
    {
        public int lv;
        public CustomDataType.BigInteger get_gold;
        public int rareChance;
        public CustomDataType.BigInteger cost;
    }
    public void Load_MinerNoobData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/초급숙련광부", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 1; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                MinerTable_Noob item = new MinerTable_Noob();

                int cnt = 0;

                item.lv = int.Parse(stral[cnt]); ++cnt;
                item.get_gold = new CustomDataType.BigInteger(stral[cnt]); ++cnt;
                item.cost = new CustomDataType.BigInteger(stral[cnt]); ++cnt;

                m_MinerNoobData.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Load_LocalizingTable");
        }

    }
    public void Load_MinerExpertData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/중급숙련광부", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 1; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                MinerTable_Expert item = new MinerTable_Expert();

                int cnt = 0;

                item.lv = int.Parse(stral[cnt]); ++cnt;
                item.get_gold = new CustomDataType.BigInteger(stral[cnt]); ++cnt;
                item.rareChance = int.Parse(stral[cnt]); ++cnt;
                item.cost = new CustomDataType.BigInteger(stral[cnt]); ++cnt;

                m_MinerExpertData.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Load_LocalizingTable");
        }

    }
    public void Load_MinerMasterData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/고급숙련광부", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 1; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                MinerTable_Master item = new MinerTable_Master();

                int cnt = 0;

                item.lv = int.Parse(stral[cnt]); ++cnt;
                item.get_gold = new CustomDataType.BigInteger(stral[cnt]); ++cnt;
                item.rareChance = int.Parse(stral[cnt]); ++cnt;
                item.cost = new CustomDataType.BigInteger(stral[cnt]); ++cnt;

                m_MinerMasterData.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Load_LocalizingTable");
        }

    }

    public struct ExpertTable_Warrior
    {
        public int lv;
        public CustomDataType.BigInteger dmg;
        public CustomDataType.BigInteger cost;
    }
    public struct ExpertTable_Alchemist
    {
        public int lv;
        public int abillityValue;
        public CustomDataType.BigInteger dmg;
        public CustomDataType.BigInteger cost;
    }
    public struct ExpertTable_Healer
    {
        public int lv;
        public int abillityValue;
        public CustomDataType.BigInteger dmg;
        public CustomDataType.BigInteger cost;
    }
    public void Load_ExpertWarriorData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/용병", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 1; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                ExpertTable_Warrior item = new ExpertTable_Warrior();

                int cnt = 0;

                item.lv = int.Parse(stral[cnt]); ++cnt;
                item.dmg = new CustomDataType.BigInteger(stral[cnt]); ++cnt;
                item.cost = new CustomDataType.BigInteger(stral[cnt]); ++cnt;

                m_ExpertWarriorData.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Load_LocalizingTable");
        }

    }
    public void Load_ExpertAlchemistData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/연금술사", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 1; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                ExpertTable_Alchemist item = new ExpertTable_Alchemist();

                int cnt = 0;

                item.lv = int.Parse(stral[cnt]); ++cnt;
                item.abillityValue = int.Parse(stral[cnt]); ++cnt;
                item.dmg = new CustomDataType.BigInteger(stral[cnt]); ++cnt;
                item.cost = new CustomDataType.BigInteger(stral[cnt]); ++cnt;

                m_ExpertAlchemistData.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Load_LocalizingTable");
        }

    }
    public void Load_ExpertHealerData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/사제", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 1; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                ExpertTable_Healer item = new ExpertTable_Healer();

                int cnt = 0;

                item.lv = int.Parse(stral[cnt]); ++cnt;
                item.abillityValue = int.Parse(stral[cnt]); ++cnt;
                item.dmg = new CustomDataType.BigInteger(stral[cnt]); ++cnt;
                item.cost = new CustomDataType.BigInteger(stral[cnt]); ++cnt;

                m_ExpertHealerData.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Load_LocalizingTable");
        }

    }

    public struct MonsterTable
    {
        public int lv;
        public CustomDataType.BigInteger hp;
        public CustomDataType.BigInteger rewardGold;
    }
    public void Load_MonsterData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/광산 몬스터", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 1; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                MonsterTable item = new MonsterTable();

                int cnt = 0;
                item.lv = int.Parse(stral[cnt]); ++cnt;
                item.hp = new CustomDataType.BigInteger(stral[cnt]); ++cnt;
                item.rewardGold = new CustomDataType.BigInteger(stral[cnt]); ++cnt;

                m_MonsterData.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Load_LocalizingTable");
        }

    }


    public struct RareOreTable
    {
        public string name;
        public List<CustomDataType.BigInteger> Grade;
        public CustomDataType.BigInteger LastAutoGold;
    }
    public void Load_RareOreData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/희귀광석", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                RareOreTable item = new RareOreTable();

                int cnt = 0;
                item.name = stral[cnt]; ++cnt;
                item.Grade = new List<CustomDataType.BigInteger>();
                item.Grade.Add(new CustomDataType.BigInteger(stral[cnt]));++cnt;
                item.Grade.Add(new CustomDataType.BigInteger(stral[cnt])); ++cnt;
                item.Grade.Add(new CustomDataType.BigInteger(stral[cnt])); ++cnt;
                item.Grade.Add(new CustomDataType.BigInteger(stral[cnt])); ++cnt;
                item.LastAutoGold = new CustomDataType.BigInteger(stral[cnt]); ++cnt;

                m_RareOreData.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Load_LocalizingTable");
        }

    }

    /// <summary>
    /// 소수점을 제거하는 거
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    string RemoveFloat(string value)
    {
        int findIndex = 0;
        for(int i=0; i<value.Length; i++)
        {
            if(value[i] == '.')
            {
                findIndex = i;
                break;
            }
        }

        if(findIndex != 0)
        {
            return value.Substring(0, findIndex);
        }

        return value;
    }
}

