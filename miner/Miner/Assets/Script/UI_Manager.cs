﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UI_Effect
{
    UpText = 0,
}

public enum UI_Buff
{
    All_30per = 0,
    ClickGold_10per,
    AutoGold_10per,
    Craft_10per,
    ClickGold_x10,
    AutoGold_x10,
    ClickAtk_x5,
    AutoAtk_x5,
}

public class UI_Manager : Singleton<UI_Manager>
{
    public enum Menu
    {
        Main = 0,
        Owner,
        Miner,
        Expert,
        RareOre,
        Potion,
        Option
    }
    
    //현제 메뉴
    public Menu currentMenu = Menu.Main;

    //메뉴 버튼 리스트
    public List<GameObject> go_Menu;
    
    //GNB 
    public UILabel lb_Gold;
    public UILabel lb_Potion;
    public UILabel lb_ClickGold;
    public UILabel lb_AutoGold;

    //Buff
    public UIGrid gr_Buff;
    public List<GameObject> li_Buff;


    //각 메뉴 클래스
    public OwnerMenuUI OwnerMenu;
    public MinerMenuUI MinerMenu;
    public ExpertMenuUI ExpertMenu;
    public RareOreMenuUI RareOreMenu;
    public PotionMenuUI PotionMenu;
    public OptionMenuUI OptionMenu;

    public List<GameObject> li_Effect;


    /////////////////////////////////////////////////////////////////
    /////////////////////Function////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    private void Awake()
    {

#if UNITY_EDITOR
#else
        GPGS_Manager.Get.InitializeGPGS();
        GPGS_Manager.Get.LoginGPGS();
#endif

    }

    private void Start()
    {
        //게임데이터 로드
        GameData.LoadData();

        Ingame.Get.InitMiner();

        //UI 초기화
        OwnerMenu.Init();
        MinerMenu.Init();
        ExpertMenu.Init();
        RareOreMenu.Init();
        PotionMenu.Init();
        OptionMenu.Init();

        Popup_Manager.Get.Init();
        
        //타임 매니져 작동
        Time_Manager.Get.Init();

        //메인화면 오픈
        Open_Menu(Menu.Main);

        //BGM 온
        SoundManager.Get.SetSoundState();
        SoundManager.Get.ChangeBGM(SoundType.BGM_001);

       
        //테스트 코드
        //GameData.AddGold("1000000000000000000000000000000");
        //GameData.AddPotion(10);
        //GameData.PickaxeLv = Table_Manager.Get.m_PickaxeData.Count - 1;
        //GameData.AttackAxeLv = Table_Manager.Get.m_AttackAxeData.Count - 1;

        //GameData.Miner_Noob_Lv   = Table_Manager.Get.m_MinerNoobData.Count - 1;
        //GameData.Miner_Expert_Lv = Table_Manager.Get.m_MinerExpertData.Count - 1;
        //GameData.Miner_Master_Lv = Table_Manager.Get.m_MinerMasterData.Count - 1;

        //GameData.Expert_Warrior_Lv    = Table_Manager.Get.m_ExpertWarriorData.Count - 1;
        //GameData.Expert_Alchemist_Lv  = Table_Manager.Get.m_ExpertAlchemistData.Count - 1;
        //GameData.Expert_Healer_Lv     = Table_Manager.Get.m_ExpertHealerData.Count - 1;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) == true)
        {
            if (Popup_Manager.Get.isPopup() == true)
                Popup_Manager.Get.AllHide();
            else
                Popup_Manager.Get.Exit.Show();
        }

        //if (Input.GetKeyDown(KeyCode.A) == true)
        //{
        //    GameData.AddOre(OreType.Red, 0,15);

        //    //GameData.AddOre(OreType.Red, 1);
        //    //GameData.AddOre(OreType.Red, 2);
        //    //GameData.AddOre(OreType.Red, 3);
        //}

        //if (Input.GetKeyDown(KeyCode.S) == true)
        //{
        //    GameData.AddGold("5000000");
        //    GameData.AddPotion(50);
        //}
    }

    void Open_Menu(Menu MenuName)
    {
        currentMenu = MenuName;

        for (int i=0; i<go_Menu.Count; i++)
        {
            if(i == (int)MenuName)
                go_Menu[i].SetActive(true);
            else
                go_Menu[i].SetActive(false);
        }

        Refresh();
    }
  
    public void Refresh(bool noChild = false)
    {
        lb_Gold.text = GetGoldString(GameData.Gold.ToString(),3);
        lb_Potion.text = GameData.Potion.ToString();
        lb_ClickGold.text = GetGoldString(GameData.CalcTapGoldPrice().ToString(),3); 
        lb_AutoGold.text = GetGoldString(GameData.CalcGetAutoGold_ALL().ToString(), 3);

        //버프 상태 로딩
        SetBuffState();

        if (noChild == true)
            return;

        //하위 UI도 갱신
        switch (currentMenu)
        {
            case Menu.Main:
                break;
            case Menu.Owner:
                OwnerMenu.Refresh();
                break;
            case Menu.Miner:
                MinerMenu.Refresh();
                break;
            case Menu.Expert:
                ExpertMenu.Refresh();
                break;
            case Menu.RareOre:
                RareOreMenu.Refresh();
                break;
            case Menu.Potion:
                PotionMenu.Refresh();
                break;
            case Menu.Option:
                OptionMenu.Refresh();
                break;
            default:
                break;
        }
    }
    

    //G 만 억 조 경 해 
    public string GetGoldString(string gold, int cuttingValue)
    {
        string temp = gold;

        int insertPos = 0;
        for (int i = temp.Length; i > 0; i--)
        {
            switch (insertPos)
            {
                case 0: { temp = temp + " "; } break;
                case 4: { temp = temp.Insert(i, "만 "); } break;
                case 8: { temp = temp.Insert(i, "억 "); } break;
                case 12: { temp = temp.Insert(i, "조 "); } break;
                case 16: { temp = temp.Insert(i, "경 "); } break;
                case 20: { temp = temp.Insert(i, "해 "); } break;
            }

            insertPos++;
        }

        insertPos = 0;
        int findPos = 0;
        for (int i = 0; i < temp.Length; i++)
        {
            findPos++;

            if (temp[i] == ' ')
                insertPos++;

            if (insertPos == cuttingValue)
                break;
        }


        return temp.Substring(0, findPos-1);
    }

    //버튼 함수
    public void OnClick_Main()
    {
        SoundManager.Get.PlaySound(SoundType.Click);
        Open_Menu(Menu.Main);
    }
    public void OnClick_Owner()
    {
        SoundManager.Get.PlaySound(SoundType.Click);
        Open_Menu(Menu.Owner);
    }
    public void OnClick_Miner()
    {
        SoundManager.Get.PlaySound(SoundType.Click);
        Open_Menu(Menu.Miner);
    }
    public void OnClick_Expert()
    {
        SoundManager.Get.PlaySound(SoundType.Click);
        Open_Menu(Menu.Expert);
    }
    public void OnClick_RareOre()
    {
        SoundManager.Get.PlaySound(SoundType.Click);
        Open_Menu(Menu.RareOre);
    }
    public void OnClick_Potion()
    {
        SoundManager.Get.PlaySound(SoundType.Click);
        Open_Menu(Menu.Potion);
    }
    public void OnClick_Option()
    {
        SoundManager.Get.PlaySound(SoundType.Click);
        Open_Menu(Menu.Option);
    }


    public void OnClick_Achieve()
    {
        SoundManager.Get.PlaySound(SoundType.Click);
        GPGS_Manager.Get.ShowAchievement();
    }
    public void OnClick_LeaderBoard()
    {
        SoundManager.Get.PlaySound(SoundType.Click);
        GPGS_Manager.Get.ShowLeaderBoard();
    }

    public void OnClick_Addgold()
    {
        //광산주 레벨에 따른 획득 골드
        if (Ingame.Get.GetState() == IngameState.Miner)
        {
            SoundManager.Get.PlaySound(SoundType.Pickaxe, true);
            GameData.AddGold(GameData.CalcTapGoldPrice());
            Refresh();
        }
        else
        {
            Ingame.Get.BattleRoot.TouchAttackGolem();
        }
    }


    //현제 내 골드 체크
    public bool CheckHaveMoney(CustomDataType.BigInteger needGold)
    {
        if (GameData.Gold < needGold)
            return false;

        return true;
    }
    public bool CheckHaveMoney(int needGold)
    {
        if (GameData.Gold < needGold)
            return false;

        return true;
    }
    public bool CheckHavePotion(int needPotion)
    {
        if (GameData.Potion < needPotion)
            return false;

        return true;
    }

    public GameObject GetEffect(UI_Effect type)
    {
        return li_Effect[(int)type];
    }
    

    public void SetBuffState()
    {
        //패키지
        OnOffBuff(UI_Buff.All_30per, GameData.BuyOwnerPack);
        OnOffBuff(UI_Buff.ClickGold_10per, GameData.BuyNewbiePackA);
        OnOffBuff(UI_Buff.AutoGold_10per, GameData.BuyNewbiePackB);
        OnOffBuff(UI_Buff.Craft_10per, GameData.BuyAlchemistPack);

        //일시적인 버프 갱신
        OnOffBuff(UI_Buff.ClickGold_x10, GameData.bTapGoldx10);
        OnOffBuff(UI_Buff.AutoGold_x10, GameData.bAutoGoldx10);
        OnOffBuff(UI_Buff.ClickAtk_x5, GameData.bTapAtkx5);
        OnOffBuff(UI_Buff.AutoAtk_x5, GameData.bAutoAtkx5);

        gr_Buff.Reposition();
    }

    public void OnOffBuff(UI_Buff buff , int isOn)
    {
        if (isOn == 1)
            li_Buff[(int)buff].SetActive(true);
        else
            li_Buff[(int)buff].SetActive(false);
    }
    public void OnOffBuff(UI_Buff buff, bool isOn)
    {
        if (isOn == true)
            li_Buff[(int)buff].SetActive(true);
        else
            li_Buff[(int)buff].SetActive(false);
    }
}
