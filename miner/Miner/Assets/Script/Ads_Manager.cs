﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using GoogleMobileAds.Api;

public class Ads_Manager : Singleton<Ads_Manager>
{
    private string
        androidGameId = "1431175",
        iosGameId = "1350598";

    [SerializeField]
    private bool testMode;

    void Start()
    {
        string gameId = null;

#if UNITY_ANDROID
        gameId = androidGameId;
#elif UNITY_IOS
        gameId = iosGameId;
#endif

        if (Advertisement.isSupported && !Advertisement.isInitialized)
        {
            Advertisement.Initialize(gameId, testMode);
        }

        //전면광고
        RequestBanner();
    }

    public void ShowAd()
    {
        if (Advertisement.IsReady())
        {
            Debug.Log("광고 시청 로딩");
            Advertisement.Show();
        }
    }

    public void ShowRewardedAd()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            Debug.Log("광고 시청 로딩");
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                //
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.

                GameData.AddPotion(1);
                UI_Manager.Get.Refresh(true);
                //연출 필요할듯??
                Popup_Manager.Get.GetPotion.Show(1);
                //광고 보기 업적 카운트 ++
                GPGS_Manager.Get.IncrementAchieve(GPGSIds.achievement_watching_video, 1);

                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                Popup_Manager.Get.ADFail.Show();
                break;
        }
    }

    //전면 광고
    BannerView bannerView;
    private void RequestBanner()
    {
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
        string adUnitId = "ca-app-pub-9910842226837850/8132043920";
#elif UNITY_IPHONE
        string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the banner with the request.
        bannerView.LoadAd(request);
    }

    private void OnDestroy()
    {
        bannerView.Destroy();
    }

}

