﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class BoxWindow : Window
{
    public UIScrollView InvenScroll;
    public UICenterOnChild InvenCenter;

    public GameObject Inven_Flower;
    public GameObject Inven_Boquet;
    public GameObject Inven_Basket;
    public GameObject Inven_UesItem;

    public UIScrollView[] Scroll = new UIScrollView[(int)BoxTab.Max];

    public UIGrid[] Inven_Scroll_Grid = new UIGrid[(int)BoxTab.Max];

    public UILabel[] TapNames = new UILabel[(int)BoxTab.Max];

    public UIButton CloseBtn;

    public UILabel SlotCount;

    List<InvenItem>[] Inven = new List<InvenItem>[(int)BoxTab.Max];

    string[] TapNameText = { "생화", "꽃다발", "바구니", "아이템" };

    public override void WindowInit()
    {
        WinType = WindowType.Box;
        for (int i = 0; i < (int)BoxTab.Max; ++i)
        {
            Inven[i] = new List<InvenItem>();
        }
        TextSetting();
        base.WindowInit();
    }

    public override void OpenWindow()
    {
        WindowAni.Play("SubWork_In");
        
        for(int i = 0; i < (int)BoxTab.Max; ++i)
        {
            ScrollSetting((BoxTab)i);
        }

        SlotCount.text = string.Format("슬롯 : {0}/{1}", InventoryManager.Get.InventoryItem.Count, UserDataManager.Get.InvenMax);

        base.OpenWindow();
    }

    public override void CloseWindow()
    {
        WindowAni.Play("SubWork_Out");
        base.CloseWindow();
    }

    public override void OpenAniEnd()
    {
        AllScrollPostionReset();
        CloseBtn.gameObject.SetActive(true);
        base.OpenAniEnd();
    }

    public override void CloseAniStart()
    {
        CloseBtn.gameObject.SetActive(false);
        base.CloseAniStart();
    }

    public override void TextSetting()
    {
        for(int i = 0; i < TapNames.Length; ++i)
        {
            TapNames[i].text = TapNameText[i];
        }

        base.TextSetting();
    }

    /////////////////////////////////////////////
    public void ScrollSetting(BoxTab type_)
    {
        //오브젝트 생성
        if (Inven[(int)type_].Count < UserDataManager.Get.InvenMax)
        {
            int createcount = UserDataManager.Get.InvenMax - Inven[(int)type_].Count;

            for (int i = 0; i < createcount; ++i)
            {
                GameObject o = GameObject.Instantiate(Resources.Load("Prefab/Box_item")) as GameObject;
                o.transform.parent = Inven_Scroll_Grid[(int)type_].transform;
                o.transform.localScale = Vector3.one;

                Inven[(int)type_].Add(o.GetComponent<InvenItem>());
            }
        }

        //데이터 배치
        List<InventoryItem> list = InventoryManager.Get.GetTypeList(type_);

        for(int i = 0; i < Inven[(int)type_].Count; ++i)
        {
            if(i < list.Count)
            {
                Inven[(int)type_][i].SetData(list[i]);
            }
            else
            {
                Inven[(int)type_][i].SetData(null);
            }
        }

        Inven_Scroll_Grid[(int)type_].Reposition();
    }

    public void ScrollPostionReset(BoxTab type_)
    {
        Scroll[(int)type_].ResetPosition();
        SlotCount.text = string.Format("슬롯 : {0}/{1}", InventoryManager.Get.InventoryItem.Count, UserDataManager.Get.InvenMax);
    }

    public void AllScrollPostionReset()
    {
        for(int i = 0; i < Scroll.Length; ++i)
        {
            Scroll[i].ResetPosition();
        }
    }
    /////////////////////////////////////////////

    public void OnClickedCloseBtn()
    {
        CloseWindow();
        MainUIManager.Get.QuickOpenWin(WindowType.Main);
    }

    public void OnClickedFlowerTab()
    {
        InvenCenter.CenterOn(Inven_Flower.transform);
    }

    public void OnClickedBoquetTab()
    {
        InvenCenter.CenterOn(Inven_Boquet.transform);
    }

    public void OnClickedBasketTab()
    {
        InvenCenter.CenterOn(Inven_Basket.transform);
    }

    public void OnClickedUseItemTab()
    {
        InvenCenter.CenterOn(Inven_UesItem.transform);
    }

    public override void ReFreshWindow()
    {
        for(int i = 0; i < (int)BoxTab.Max; ++i)
        {
            ScrollSetting((BoxTab)i);
        }
        SlotCount.text = string.Format("슬롯 : {0}/{1}", InventoryManager.Get.InventoryItem.Count, UserDataManager.Get.InvenMax);
        base.ReFreshWindow();
    }
}
