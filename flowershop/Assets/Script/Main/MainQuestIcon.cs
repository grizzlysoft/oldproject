﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainQuestIcon : MonoBehaviour
{
    public UIButton Btn;
    public UISprite Icon;
    public UIProgressBar Count;
    public int Type;

    public GameObject Block;
    public UILabel Delay;

    QuestData q = null;

    
    public bool Active
    {
        get { return this.gameObject.activeInHierarchy; }
    }

    public void SetInfo()
    {
        q = null;

        if (Type == 0)
            q = QuestManager.Get.NomarlQuest;
        else
            q = QuestManager.Get.MainQuest;

        if (q == null)
            this.gameObject.SetActive(false);
        else
        {
            gameObject.SetActive(true);
            Icon.spriteName = q.Res;

            int max = 0;
            int now = 0;
            for (int i = 0; i < q.RequestID.Count; ++i)
            {
                ReQuestData r = DataManager.Get.GetReqeustData(q.RequestID[i]);

                max += r.Count;
                now += InventoryManager.Get.GetItemCount(r.ItemID);
            }

            if (Type == 0)
            {
                if (QuestManager.Get.GetAcceptNormal == 1)
                    Count.value = (float)now / (float)max;
            }
            else
            {
                Count.value = (float)now / (float)max;
            }
            
        }
    }

    public void Refresh()
    {
        if (q == null)
            return;
        else
        {
            int max = 0;
            int now = 0;
            for (int i = 0; i < q.RequestID.Count; ++i)
            {
                ReQuestData r = DataManager.Get.GetReqeustData(q.RequestID[i]);

                max += r.Count;
                now += InventoryManager.Get.GetItemCount(r.ItemID);
            }

            if (Type == 0)
            {
                if (QuestManager.Get.GetAcceptNormal == 1)
                    Count.value = (float)now / (float)max;
            }
            else
            {
                Count.value = (float)now / (float)max;
            }
        }
    }

    public void OnClickedQuestIcon()
    {
        PopupManager.Get.OpenPopup("Popup_Quest", q);
    }

    public void Update()
    {
        if(null != q && q.Type == Enum.QuestType.Loop && QuestManager.Get.NomarlQuestDelay > System.DateTime.Now)
        {
            Block.SetActive(true);
            Delay.text = string.Format("00:{0}", (int)(QuestManager.Get.NomarlQuestDelay - System.DateTime.Now).TotalSeconds);
        }
        else
        {
            Block.SetActive(false);
        }
    }
}
