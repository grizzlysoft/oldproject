﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Enum;

public class MiniGameBlock : MonoBehaviour {

    public GameObject CardBlock;
    public UILabel Card_TimeText;

    public GameObject WhereHouseBlock;
    public UILabel WhereHouse_TimeText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(MiniGameManager.Get.Card_CoolTime > DateTime.Now)
        {
            CardBlock.SetActive(true);
            TimeSpan latetime = MiniGameManager.Get.Card_CoolTime - DateTime.Now;
            if (latetime.Hours < 1)
                Card_TimeText.text = GetTimeText(latetime.Minutes) + ":" + GetTimeText(latetime.Seconds);
        }
        else
        {
            CardBlock.SetActive(false);
        }

        if (MiniGameManager.Get.WhereHouse_CoolTime > DateTime.Now)
        {
            WhereHouseBlock.SetActive(true);
            TimeSpan latetime = MiniGameManager.Get.WhereHouse_CoolTime - DateTime.Now;
            if (latetime.Hours < 1)
                WhereHouse_TimeText.text = GetTimeText(latetime.Minutes) + ":" + GetTimeText(latetime.Seconds);
        }
        else
        {
            WhereHouseBlock.SetActive(false);
        }
    }

    string GetTimeText(int t_)
    {
        if (t_ >= 10)
            return t_.ToString();
        else
            return "0" + t_;
    }

    public void OnClickedCardBlock()
    {
        Message message = new Message(null, ShowAD_CardCoolTime, "미니게임 쿨타임 중입니다.\n(광고를 보면 초기화할 수 있습니다.)", "확인", "광고보기", MessagePopupType.Ok_Cancel);

        PopupManager.Get.OpenPopup("Popup_Message", message);
    }

    public void OnClickedWhereHouseBlock()
    {
        Message message = new Message(null, ShowAD_WhereHouseCoolTime, "미니게임 쿨타임 중입니다.\n(광고를 보면 초기화할 수 있습니다.)", "확인", "광고보기", MessagePopupType.Ok_Cancel);

        PopupManager.Get.OpenPopup("Popup_Message", message);
    }

    void ShowAD_CardCoolTime()
    {
        PopupManager.Get.ClosePopup("Popup_Message");
        Ads_Manager.Get.ShowAd_UnityAds(ADShowType.MiniGame_Card);
    }

    void ShowAD_WhereHouseCoolTime()
    {
        PopupManager.Get.ClosePopup("Popup_Message");
        Ads_Manager.Get.ShowAd_UnityAds(ADShowType.MiniGame_WhereHouse);
    }
}
