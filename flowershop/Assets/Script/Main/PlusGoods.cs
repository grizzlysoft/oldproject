﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlusGoods : MonoBehaviour
{
    public UILabel Text;
	
    // Use this for initialization
	void Start ()
    {
	}

    public void SetData(int data_)
    {
        if(data_ > 0)
            Text.text = string.Format("[FFFF00]+{0:##,##0}", data_);
        else
            Text.text = string.Format("[FF0000]{0:##,##0}", data_);
    }

    public void SetData(long data_)
    {
        if (data_ > 0)
            Text.text = string.Format("[FFFF00]+{0:##,##0}", data_);
        else
            Text.text = string.Format("[FF0000]{0:##,##0}", data_);
    }

    public void OnFinshedTween()
    {
        Destroy(this.gameObject);
    }
	
	// Update is called once per frame
	void Update ()
    {
        //transform.localPosition = new Vector3(0f, transform.localPosition.y + 2f, 0f);	
	}
}
