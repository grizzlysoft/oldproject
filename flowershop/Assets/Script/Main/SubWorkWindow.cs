﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class SubWorkWindow : Window
{
    public UIGrid ScrollGrid;
    public UIScrollView Scroll;

    public UIButton CloseBtn;

    List<SubWorkItem> ScrollItemList = new List<SubWorkItem>();

    public override void WindowInit()
    {
        WinType = Enum.WindowType.SubWork;
        CreateSubWork();
        base.WindowInit();
    }

    public override void OpenWindow()
    {
        WindowAni.Play("SubWork_In");
        base.OpenWindow();
    }

    public override void OpenAniEnd()
    {
        CloseBtn.gameObject.SetActive(true);
        base.OpenAniEnd();
    }

    public override void CloseAniStart()
    {
        CloseBtn.gameObject.SetActive(false);
        base.CloseAniStart();
    }

    void CreateSubWork()
    {
        List<SubWorkData> list = DataManager.Get.GetSubWorkList();

        for (int i = 0; i < list.Count; ++i)
        {
            GameObject o = GameObject.Instantiate(Resources.Load("Prefab/SubWork_Item")) as GameObject;
            o.transform.parent = ScrollGrid.transform;
            o.transform.localScale = Vector3.one;
            o.GetComponent<SubWorkItem>().SetData(list[i]);
            ScrollItemList.Add(o.GetComponent<SubWorkItem>());
        }

        ScrollGrid.Reposition();
    }

    public void RefreshInfo()
    {
        for(int i = 0; i < ScrollItemList.Count; ++i)
        {
            ScrollItemList[i].InfoSetting();
        }
    }

    public void OnClickedCloseBtn()
    {
        CloseWindow();
        MainUIManager.Get.OpenWindow(WindowType.Main);
    }

    public override void CloseWindow()
    {
        WindowAni.Play("SubWork_Out");
        Scroll.ResetPosition();
        base.CloseWindow();
    }

    public override void ReFreshWindow()
    {
        List<SubWorkData> list = DataManager.Get.GetSubWorkList();
        for (int i = 0; i < list.Count; ++i)
        {
            ScrollItemList[i].SetData(list[i]);
        }

        base.ReFreshWindow();
    }
}
