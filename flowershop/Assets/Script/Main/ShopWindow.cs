﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class ShopWindow : Window
{
    public UIGrid TapGrid;
    public UIButton[] Taps = new UIButton[3];
    public UILabel[] TapsLabel = new UILabel[3];
    public UIScrollView[] ShopScroll = new UIScrollView[3];
    public UIGrid[] ShopScrollGrid = new UIGrid[3];

    List<Shop_ScrollItem>[] ShopScrollData = new List<Shop_ScrollItem>[(int)ShopTapType.Max];

    ShopTapType SelectTap = ShopTapType.Item;

    public override void WindowInit()
    {
        WinType = WindowType.Shop;
        CreateScrollItem(ShopTapType.Crystal);
        CreateScrollItem(ShopTapType.Gold);
        CreateScrollItem(ShopTapType.Item);
        TextSetting();
        base.WindowInit();
    }

    public override void OpenWindow()
    {
        WindowAni.Play("SubWork_In");
        SetSeletTap();
        base.OpenWindow();
    }

    void CreateScrollItem(ShopTapType type_)
    {
        List<ShopData> datalist = DataManager.Get.GetShopDataList(type_);

        for (int z = 0; z < datalist.Count; ++z)
        {
            GameObject o = Instantiate(Resources.Load("Prefab/Shop_ScrollItem")) as GameObject;
            o.transform.parent = ShopScrollGrid[(int)type_].transform;
            o.transform.localScale = Vector3.one;
            o.transform.localPosition = Vector3.zero;

            o.GetComponent<Shop_ScrollItem>().SetData(datalist[z]);

            if (null == ShopScrollData[(int)type_])
                ShopScrollData[(int)type_] = new List<Shop_ScrollItem>();

            ShopScrollData[(int)type_].Add(o.GetComponent<Shop_ScrollItem>());
        }

        ShopScrollGrid[(int)type_].Reposition();
    }

    string[] TapName = { "크리스탈", "골드", "아이템" };

    public override void TextSetting()
    {
        for(int i = 0; i < TapsLabel.Length; ++i)
        {
            TapsLabel[i].text = TapName[i];
        }

        base.TextSetting();
    }

    public void SetSeletTap()
    {
        for(int i = 0; i < ShopScroll.Length; ++i)
        {
            if ((ShopTapType)i == SelectTap)
                ShopScroll[i].gameObject.SetActive(true);
            else
                ShopScroll[i].gameObject.SetActive(false);
        }
    }

    public void OnClickedCrystalTap()
    {
        if(SelectTap != ShopTapType.Crystal)
        {
            SelectTap = ShopTapType.Crystal;
            SetSeletTap();
        }
    }

    public void OnClickedGoldTap()
    {
        if (SelectTap != ShopTapType.Gold)
        {
            SelectTap = ShopTapType.Gold;
            SetSeletTap();
        }
    }

    public void OnClickedItemTap()
    {
        if (SelectTap != ShopTapType.Item)
        {
            SelectTap = ShopTapType.Item;
            SetSeletTap();
        }
    }

    public void ChangeSelectTap(ShopTapType type_)
    {
        SelectTap = type_;
        SetSeletTap();
    }

    public void OnClickedCloseBtn()
    {
        CloseWindow();
        MainUIManager.Get.OpenWindow(WindowType.Main);
    }

    public override void CloseWindow()
    {
        WindowAni.Play("SubWork_Out");
        base.CloseWindow();
    }
}
