﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class DressRoomWindow : Window
{
    public UIButton CloseBtn;

    public UIGrid ScrollGrid;
    public UIScrollView ScrollView;

    public UILabel Title;

    List<DressRoomScorll_Item> ScrollItemList = new List<DressRoomScorll_Item>();

    public override void WindowInit()
    {
        WinType = Enum.WindowType.DressRoom;
        ScrollItemCreate();
        base.WindowInit();
    }

    public override void OpenAniEnd()
    {
        CloseBtn.gameObject.SetActive(true);
        base.OpenAniEnd();
    }

    public override void CloseAniStart()
    {
        CloseBtn.gameObject.SetActive(false);
        ScrollView.ResetPosition();
        base.CloseAniStart();
    }

    public override void OpenWindow()
    {
        WindowAni.Play("SubWork_In");
        base.OpenWindow();
    }

    void ScrollItemCreate()
    {
        List<CharacterClothes> list = DataManager.Get.GetClothesDataList();

        for(int i = 0; i < list.Count; ++i)
        {
            DressRoomScorll_Item item = (GameObject.Instantiate(Resources.Load("Prefab/DressRoomScorll_Item")) as GameObject).GetComponent<DressRoomScorll_Item>();
            item.transform.parent = ScrollGrid.transform;
            item.transform.localScale = Vector3.one;
            item.SetData(list[i]);

            ScrollItemList.Add(item);
        }

        ScrollGrid.Reposition();
    }

    public void OnClickedCloseBtn()
    {
        CloseWindow();
        MainUIManager.Get.OpenWindow(WindowType.Main);
    }

    public override void TextSetting()
    {
        Title.text = "옷장";
        base.TextSetting();
    }

    public override void CloseWindow()
    {
        WindowAni.Play("SubWork_Out");
        base.CloseWindow();
    }

    public override void ReFreshWindow()
    {
        for(int i = 0; i < ScrollItemList.Count; ++i)
        {
            ScrollItemList[i].ReFresh();
        }
        base.ReFreshWindow();
    }
}
