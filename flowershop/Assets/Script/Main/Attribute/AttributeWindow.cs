﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class AttributeWindow : Window
{
    public UIGrid ScrollGrid;

    public UIButton CloseBtn;

    List<Attribute_Item> ListItem = new List<Attribute_Item>();

    public override void WindowInit()
    {
        WinType = Enum.WindowType.Attribute;
        CreateAttribute();
        base.WindowInit();
    }

    public override void OpenAniEnd()
    {
        CloseBtn.gameObject.SetActive(true);
        base.OpenAniEnd();
    }

    public override void CloseAniStart()
    {
        CloseBtn.gameObject.SetActive(false);
        base.CloseAniStart();
    }

    void CreateAttribute()
    {
        for(int i = 0; i < (int)AttributeType.Max; ++i)
        {
            GameObject o = Instantiate(Resources.Load("Prefab/Attribute_Item")) as GameObject;
            o.transform.parent = ScrollGrid.transform;
            o.transform.localScale = Vector3.one;
            o.GetComponent<Attribute_Item>().SetData(DataManager.Get.GetAttributeData((AttributeType)i,
                UserDataManager.Get.GetAttributeLevel((AttributeType)i)));

            ListItem.Add(o.GetComponent<Attribute_Item>());
        }

        ScrollGrid.Reposition();
    }

    public void ReFlashListItem(AttributeType type_)
    {
        ListItem[(int)type_].SetData(DataManager.Get.GetAttributeData(type_,
                UserDataManager.Get.GetAttributeLevel(type_)));
    }

    public override void OpenWindow()
    {
        WindowAni.Play("SubWork_In");
        base.OpenWindow();
    }

    public override void CloseWindow()
    {
        WindowAni.Play("SubWork_Out");
        base.CloseWindow();
    }

    public void OnClickedCloseBtn()
    {
        CloseWindow();
        MainUIManager.Get.OpenWindow(WindowType.Main);
    }

    public override void ReFreshWindow()
    {
        for (int i = 0; i < (int)AttributeType.Max; ++i)
        {
            ReFlashListItem((AttributeType)i);
        }

        base.ReFreshWindow();
    }
}
