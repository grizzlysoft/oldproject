﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attribute_Item : MonoBehaviour
{
    public UISprite Icon;
    public UILabel AttributeName;
    public UILabel Text;
    public UILabel BtnText;
    public UILabel CostText;
    public UISprite CostIcon;
    public UIButton Btn_Upgrade;
    public UILabel LabelMax;

    AttributeData Data;

    public void SetData(AttributeData data_)
    {
        Data = data_;
        InfoSetting();
    }

    public void InfoSetting()
    {
        Icon.spriteName = Data.Res;
        
        Text.text = Data.Text;

        if (Data.Level > 0)
        {
            if (Data.Level >= DataManager.Get.GetAttributeMaxLevel(Data.Type))
            {
                AttributeName.text = Data.Name + " Lv.MAX";
                LabelMax.gameObject.SetActive(true);
                BtnText.gameObject.SetActive(false);
                CostText.gameObject.SetActive(false);
                CostIcon.gameObject.SetActive(false);
            }
            else
            {
                AttributeName.text = Data.Name + " Lv." + Data.Level;
                LabelMax.gameObject.SetActive(false);
                BtnText.gameObject.SetActive(true);
                CostText.gameObject.SetActive(true);
                CostIcon.gameObject.SetActive(true);
                BtnText.text = "레벨업";
                CostText.text = string.Format("{0:##,##0}", Data.UpgradeCost);
                if (Data.CostType == Enum.AttributeCostType.Gold)
                    CostIcon.spriteName = "Gold_Icon";
                else
                    CostIcon.spriteName = "Crystal_Icon";
            }
        }
        else
        {
            AttributeName.text = Data.Name;
            LabelMax.gameObject.SetActive(false);
            BtnText.gameObject.SetActive(true);
            CostText.gameObject.SetActive(true);
            CostIcon.gameObject.SetActive(true);
            BtnText.text = "해제";
            CostText.text = string.Format("{0:##,##0}", Data.UnlockCost);
            if (Data.CostType == Enum.AttributeCostType.Gold)
                CostIcon.spriteName = "Gold_Icon";
            else
                CostIcon.spriteName = "Crystal_Icon";
        }
    }

    public void OnClickedBtnUpgrade()
    {
        if (Data.Level > 0)
        {
            if (Data.Level >= DataManager.Get.GetAttributeMaxLevel(Data.Type))
            {
                Message message = new Message(null, null, "최대 레벨입니다.", "확인", string.Empty, Enum.MessagePopupType.Ok);
                PopupManager.Get.OpenPopup("Popup_Message", message);
            }
            else
            {
                if (Data.CostType == Enum.AttributeCostType.Gold)
                {
                    if (UserDataManager.Get.UseGold(Data.UpgradeCost))
                        UserDataManager.Get.AttributeLevelUp(Data.Type);
                    else
                    {
                        Message message = new Message(MainUIManager.Get.MoveShopGoldTapDelegate, null, "골드가 부족합니다.\n상점으로 이동하시겠습니까?", "확인", "취소", Enum.MessagePopupType.Ok_Cancel);
                        PopupManager.Get.OpenPopup("Popup_Message", message);
                    }
                }
                else
                {
                    if (UserDataManager.Get.UseCrystal((int)Data.UpgradeCost))
                        UserDataManager.Get.AttributeLevelUp(Data.Type);
                    else
                    {
                        Message message = new Message(MainUIManager.Get.MoveShopCrystalTapDelegate, null, "크리스탈이 부족합니다.\n상점으로 이동하시겠습니까?", "확인", "취소", Enum.MessagePopupType.Ok_Cancel);
                        PopupManager.Get.OpenPopup("Popup_Message", message);
                    }
                }
            }
        }
        else
        {
            if (Data.CostType == Enum.AttributeCostType.Gold)
            {
                if (UserDataManager.Get.UseGold(Data.UnlockCost))
                    UserDataManager.Get.AttributeLevelUp(Data.Type);
                else
                {
                    Message message = new Message(MainUIManager.Get.MoveShopGoldTapDelegate, null, "골드가 부족합니다.\n상점으로 이동하시겠습니까?", "확인", "취소", Enum.MessagePopupType.Ok_Cancel);
                    PopupManager.Get.OpenPopup("Popup_Message", message);
                }
            }
            else
            {
                if (UserDataManager.Get.UseCrystal(Data.UnlockCost))
                    UserDataManager.Get.AttributeLevelUp(Data.Type);
                else
                {
                    Message message = new Message(MainUIManager.Get.MoveShopCrystalTapDelegate, null, "크리스탈이 부족합니다.\n상점으로 이동하시겠습니까?", "확인", "취소", Enum.MessagePopupType.Ok_Cancel);
                    PopupManager.Get.OpenPopup("Popup_Message", message);
                }
            }
        }

        MainUIManager.Get.GetWindow(Enum.WindowType.Attribute).GetComponent<AttributeWindow>().ReFlashListItem(Data.Type);
    }


}
