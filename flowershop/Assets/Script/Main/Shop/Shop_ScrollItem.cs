﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop_ScrollItem : MonoBehaviour
{
    public UISprite Icon;
    public UILabel Text;
    public UIButton Btn_Buy;
    public UILabel BuyText;
    public UILabel Cost;
    public UISprite CostIcon;

    ShopData Data;

    public void SetData(ShopData data_)
    {
        Data = data_;
        SetInfo();
    }

    public void SetInfo()
    {
        if(null != Data)
        {
            Icon.spriteName = Data.Res;
            Text.text = Data.Text.Replace("\\n", "\n");
            if(Data.PayType == Enum.Shop_PayType.Cash)
            {
                CostIcon.gameObject.SetActive(false);
                Cost.text = "￦" + string.Format("{0:##,##0}", Data.Cost);
            }
            else
            {
                CostIcon.gameObject.SetActive(true);
                switch(Data.PayType)
                {
                    case Enum.Shop_PayType.Ads_AdMob:
                    case Enum.Shop_PayType.Ads_Unity:
                        Cost.text = "광고보기";
                        break;
                    case Enum.Shop_PayType.Crystal:
                        CostIcon.spriteName = "Crystal_Icon";
                        Cost.text = string.Format("{0:##,##0}", Data.Cost);
                        break;
                    case Enum.Shop_PayType.Gold:
                        CostIcon.spriteName = "Gold_Icon";
                        Cost.text = string.Format("{0:##,##0}", Data.Cost);
                        break;

                }
            }
        }
    }

    public void OnClickedBuyBtn()
    {
        if (Data.PayType == Enum.Shop_PayType.Ads_AdMob || Data.PayType == Enum.Shop_PayType.Ads_Unity)
        {
            PaymentManager.Get.BuyPrice(Data);
        }
        else
        {
            Message message = new Message(ConnectPaymentManager, null, Data.Text.Replace("\\n", "\n") + "\n구매하시겠습니까?", "구매", "취소", Enum.MessagePopupType.Ok_Cancel);
            PopupManager.Get.OpenPopup("Popup_Message", message);
        }
    }

    public void ConnectPaymentManager()
    {
        PopupManager.Get.ClosePopup("Popup_Message");
        switch (Data.PayType)
        {
            case Enum.Shop_PayType.Ads_AdMob:
            case Enum.Shop_PayType.Cash:
            case Enum.Shop_PayType.Ads_Unity:
                PaymentManager.Get.BuyPrice(Data);
                break;
            case Enum.Shop_PayType.Crystal:
                if (UserDataManager.Get.UseCrystal(Data.Cost))
                    PaymentManager.Get.BuyPrice(Data);
                else
                {
                    Message message = new Message(MoveCrystalTap, null, "크리스탈이 부족합니다.\n크리스탈 구매로 이동하시겠습니까?", "확인", "취소", Enum.MessagePopupType.Ok_Cancel);
                    PopupManager.Get.OpenPopup("Popup_Message", message);
                }
                break;
            case Enum.Shop_PayType.Gold:
                if (UserDataManager.Get.UseGold(Data.Cost))
                    PaymentManager.Get.BuyPrice(Data);
                else
                {
                    Message message = new Message(MoveGoldTap, null, "골드가 부족합니다.\n골드구매로 이동하시겠습니까?", "확인", "취소", Enum.MessagePopupType.Ok_Cancel);
                    PopupManager.Get.OpenPopup("Popup_Message", message);
                }
                break;
        }
    }

    void MoveCrystalTap()
    {
        PopupManager.Get.ClosePopup("Popup_Message");
        MainUIManager.Get.GetWindow(Enum.WindowType.Shop).GetComponent<ShopWindow>().ChangeSelectTap(Enum.ShopTapType.Item);
    }

    void MoveGoldTap()
    {
        PopupManager.Get.ClosePopup("Popup_Message");
        MainUIManager.Get.GetWindow(Enum.WindowType.Shop).GetComponent<ShopWindow>().ChangeSelectTap(Enum.ShopTapType.Item);
    }

    public void TextSetting()
    {
        BuyText.text = "구매";
    }

}
