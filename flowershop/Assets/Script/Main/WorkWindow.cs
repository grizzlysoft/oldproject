﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class WorkWindow : Window
{
    WorkMenuState MenuState = WorkMenuState.Menu;

    public Animation[] MenuAni = new Animation[(int)WorkMenuState.Max];

    public UIGrid FlowerGrid;
    public UILabel Nutrients_Count;
    public UIButton BtnNutrients_UseCheck_Flower;
    public GameObject NutrientsCheckImg_Flower;
    public UIScrollView FlowerScroll;
    public UISprite BuffIcon_Flower;

    public UIGrid BouquetGrid;
    public UILabel BuffItemCount_Bouquet;
    public UIButton BtnUseBuff_Bouquet;
    public GameObject UseBuffCheckImg_Bouquet;
    public UIScrollView BouquetScroll;
    public UISprite BuffIcon_Bouquet;

    public UIGrid BasketGrid;
    public UILabel BuffItemCount_Basket;
    public UIButton BtnUseBuff_Basket;
    public GameObject UseBuffCheckImg_Basket;
    public UIScrollView BasketScroll;
    public UISprite BuffIcon_Basket;

    public Working WorKingWin;

    public UISprite MenuBg;

    public UIButton CloseBtn;

    List<Work_Flower_Item> FlowerListItem = new List<Work_Flower_Item>();
    List<Work_Bouqeut_Item> BouquetListItem = new List<Work_Bouqeut_Item>();
    List<Work_Basket_Item> BasketListItem = new List<Work_Basket_Item>();

    /// <summary>
    /// 작업할것 리스트 아이템 id와 갯수로 표시한다
    /// </summary>
    List<WorkListData> WorkList = new List<WorkListData>();

    bool UseBuff = false;

    public override void WindowInit()
    {
        WinType = Enum.WindowType.Work;
        SetFlowerScroll();
        SetBouquetScroll();
        SetBasketScroll();
        if(WorkManager.Get.WorkingData != null)
        {
            WorKingWin.SetData();
            MenuAni[(int)MenuState].Play("Work_" + MenuState.ToString() + "_Out");
        }

        BuffIcon_Flower.spriteName = DataManager.Get.GetItemData(37).Res;
        BuffIcon_Bouquet.spriteName = DataManager.Get.GetItemData(38).Res;
        BuffIcon_Basket.spriteName = DataManager.Get.GetItemData(38).Res;
        base.WindowInit();
    }

    public override void OpenWindow()
    {
        for (int i = 0; i < (int)WorkMenuState.Max; ++i)
        {
            if ((WorkMenuState)i == WorkMenuState.Menu && WorkManager.Get.WorkingData == null)
            {
                MenuAni[i].gameObject.SetActive(true);
                MenuAni[i].gameObject.transform.localPosition = new Vector3(0f, 250f, 0f);
                MenuBg.color = Color.white;
                MenuState = WorkMenuState.Menu;
            }
            else
                MenuAni[i].gameObject.transform.localPosition = new Vector3(0f, -560f, 0f);
        }

        WindowAni.Play("SubWork_In");
        
        //FlowerScroll.ResetPosition();
        //BouquetScroll.ResetPosition();
        //BasketScroll.ResetPosition();
        base.OpenWindow();
    }

    public void OnClickedCloseBtn()
    {
        CloseWindow();
        MainUIManager.Get.OpenWindow(WindowType.Main);
    }

    public override void CloseWindow()
    {
        WindowAni.Play("SubWork_Out");
        base.CloseWindow();
    }

    public override void OpenAniEnd()
    {
        CloseBtn.gameObject.SetActive(true);
        base.OpenAniEnd();
    }

    public override void CloseAniStart()
    {
        CloseBtn.gameObject.SetActive(false);
        base.CloseAniStart();
    }

    ////////////////////////////

    public void SetFlowerScroll()
    {
        List<Flower> list = DataManager.Get.GetFlowerList();
        for(int i = 0; i < list.Count; ++i)
        {
            if (FlowerListItem.Count <= i && (list[i].UnLockCost == 0 || UserDataManager.Get.CheckUnLockFlower(list[i - 1].ID)))
            {
                GameObject o = GameObject.Instantiate(Resources.Load("Prefab/Work_Flower_Item")) as GameObject;
                o.transform.parent = FlowerGrid.transform;
                o.transform.localScale = Vector3.one;
                o.GetComponent<Work_Flower_Item>().SetData(list[i]);
                FlowerListItem.Add(o.GetComponent<Work_Flower_Item>());
            }
            else
            {

            }
        }
        FlowerGrid.Reposition();
    }

    public void SetBouquetScroll()
    {
        List<BouquetData> list = DataManager.Get.GetBouquetList();

        for(int i = 0; i < list.Count; ++i)
        {
            GameObject o = GameObject.Instantiate(Resources.Load("Prefab/Work_Bouquet_Item")) as GameObject;
            o.transform.parent = BouquetGrid.transform;
            o.transform.localScale = Vector3.one;
            o.GetComponent<Work_Bouqeut_Item>().SetData(list[i]);
            BouquetListItem.Add(o.GetComponent<Work_Bouqeut_Item>());
        }

        BouquetGrid.Reposition();
    }

    public void SetBasketScroll()
    {
        List<BasketData> list = DataManager.Get.GetBasketList();

        for (int i = 0; i < list.Count; ++i)
        {
            GameObject o = GameObject.Instantiate(Resources.Load("Prefab/Work_Basket_Item")) as GameObject;
            o.transform.parent = BasketGrid.transform;
            o.transform.localScale = Vector3.one;
            o.GetComponent<Work_Basket_Item>().SetData(list[i]);
            BasketListItem.Add(o.GetComponent<Work_Basket_Item>());
        }

        BasketGrid.Reposition();
    }

    public bool AddWorkingList(int id_, int count_, float time_, List<int> matid_, List<int>matcount_)
    {
        //작업목록에 있는 아이템을 추가할때
        for (int i = 0; i < WorkList.Count; ++i)
        {
            if (WorkList[i].Itemid == id_)
            {
                WorkList[i].Count += count_;
                WorkList[i].Time += time_;

                if (null != matid_)
                {
                    for (int z = 0; z < matid_.Count; ++z)
                        WorkList[i].AddMat(matid_[z], matcount_[z]);
                }
                //다 추가했으면 꺼져
                return true;
            }
        }

        if (WorkList.Count >= 3)
        {
            Message message = new Message(null, null, "한번에 가능한 작업은 3종류 입니다", "확인", string.Empty, MessagePopupType.Ok);
            PopupManager.Get.OpenPopup("Popup_Message", message);
            return false;
        }
        else
        {
            WorkListData item = new WorkListData();
            item.Itemid = id_;
            item.Count = count_;
            item.Time = time_;
            if (null != matid_)
            {
                for (int i = 0; i < matid_.Count; ++i)
                    item.AddMat(matid_[i], matcount_[i]);
            }
            WorkList.Add(item);
            return true;
        }
    }

    public void CancelWorkingList(int id_)
    {
        int index = -1;
        for(int i = 0; i < WorkList.Count; ++i)
        {
            if (WorkList[i].Itemid == id_)
                index = i;
        }

        if (index != -1)
            WorkList.RemoveAt(index);

    }

    public void AllCancelWorkingList()
    {
        WorkList.Clear();
    }

    public void WorkStart()
    {
        if (WorkList.Count <= 0)
        {
            Message message = null;
            switch (MenuState)
            {
                case WorkMenuState.FlowerList:
                    message = new Message(null, null, "생산할 생화를 선택해 주세요", "확인", string.Empty, MessagePopupType.Ok);
                    break;
                case WorkMenuState.Boquet:
                    message = new Message(null, null, "생산할 물품을 선택해 주세요", "확인", string.Empty, MessagePopupType.Ok);
                    break;
                case WorkMenuState.Basket:
                    message = new Message(null, null, "생산할 꽃바구니을 선택해 주세요", "확인", string.Empty, MessagePopupType.Ok);
                    break;
            }

            PopupManager.Get.OpenPopup("Popup_Message", message);
            return;
        }

        ItemData[] itemes = { null, null, null };
        int[] counts = { 0, 0, 0 };
        float time = 0;
        int cost = 0;

        if (MenuState == WorkMenuState.Boquet)
            cost = GetAllGoldCost_bouquet();
        else if (MenuState == WorkMenuState.Basket)
            cost = GetAllGoldCost_basket();

        if (UserDataManager.Get.GetGold() < cost)
        {
            Message message = new Message(MainUIManager.Get.MoveShopGoldTapDelegate, null, "골드가 부족합니다.\n상점으로 이동하시겠습니까?", "확인", "취소", Enum.MessagePopupType.Ok_Cancel);
            PopupManager.Get.OpenPopup("Popup_Message", message);
            return;
        }


        for (int i = 0; i < WorkList.Count; ++i)
        {
            if (WorkList[i].MatList.Count > 0)
            {
                foreach (int o in WorkList[i].MatList.Keys)
                {
                    if (!InventoryManager.Get.CheckUseItem(o, WorkList[i].MatList[o]))
                    {
                        //재료 부족
                        Debug.Log("재료 부족");
                        return;
                    }
                    else
                    {
                        InventoryManager.Get.UseItem(o, WorkList[i].MatList[o]);
                    }
                }
            }

            itemes[i] = DataManager.Get.GetItemData(WorkList[i].Itemid);
            counts[i] = WorkList[i].Count;
            time += (float)WorkList[i].Time;
            //Debug.Log("각각 시간 " + i + " = " + (float)WorkList[i].Time);
        }

        //완료시 인벤체크
        List<int> id = new List<int>();
        List<int> count = new List<int>();

        for(int i = 0; i < WorkList.Count; ++i)
        {
            id.Add(WorkList[i].Itemid);
            count.Add(WorkList[i].Count);
        }

        if(!InventoryManager.Get.AddCheck(id, count))
        {
            BigMessage message = new BigMessage(new EventDelegate(MainUIManager.Get.MoveAttributeWindowDelegate),
                new EventDelegate(MainUIManager.Get.MoveBoxWindowDelegate), "창고가 꽉 찼어요!!\n가게를 증축하거나\n꽃을 판매해주세요!", "강화이동", "창고이동", null);

            PopupManager.Get.OpenPopup("Popup_BigMessage", message);

            //뺏던 재료들 다시 넣어줌
            for(int i = 0; i < WorkList.Count; ++i)
            {
                if (WorkList[i].MatList.Count > 0)
                {
                    foreach (int o in WorkList[i].MatList.Keys)
                    {
                        InventoryManager.Get.AddItem(o, WorkList[i].MatList[o]);
                    }
                }
            }

            return;
        }

        UserDataManager.Get.UseGold(cost);

        if (UseBuff)
        {
            time = time * 0.7f;
            if (MenuState == WorkMenuState.FlowerList)
            {
                InventoryManager.Get.UseItem(37, 1);
                AchievementManager.Get.AchievementStepUp(AchievementType.UseBuff_Flower, 1);
            }
            else
            {
                InventoryManager.Get.UseItem(38, 1);
                AchievementManager.Get.AchievementStepUp(AchievementType.UseBuff_Glove, 1);
            }
        }

        WorkingData data = new WorkingData(itemes[0], itemes[1], itemes[2], counts[0], counts[1], counts[2], time, System.DateTime.Now);

        WorkManager.Get.WorkingData = data;

        WorKingWin.SetData();

        WorkList.Clear();

        for (int i = 0; i < FlowerListItem.Count; ++i)
        {
            FlowerListItem[i].Clear();
        }

        for (int i = 0; i < BouquetListItem.Count; ++i)
        {
            BouquetListItem[i].Clear();
        }

        for(int i = 0; i < BasketListItem.Count; ++i)
        {
            BasketListItem[i].Clear();
        }

        UseBuff = false;

        //업적 처리
        switch (MenuState)
        {
            case WorkMenuState.FlowerList:
                AchievementManager.Get.AchievementStepUp(AchievementType.MakeFlower, counts[0] + counts[1] + counts[2]);
                break;
            case WorkMenuState.Boquet:
                AchievementManager.Get.AchievementStepUp(AchievementType.MakeBoquet, counts[0] + counts[1] + counts[2]);
                break;
            case WorkMenuState.Basket:
                AchievementManager.Get.AchievementStepUp(AchievementType.MakeBasket, counts[0] + counts[1] + counts[2]);
                break;
        }

        MenuAni[(int)MenuState].Play("Work_" + MenuState.ToString() + "_Out");

        
    }

    public void WorkClose()
    {
        WorKingWin.CloseWorking();
    }

    public void WorkItemAllClear()
    {
        for (int i = 0; i < FlowerListItem.Count; ++i)
        {
            FlowerListItem[i].Clear();
        }

        for (int i = 0; i < BouquetListItem.Count; ++i)
        {
            BouquetListItem[i].Clear();
        }

        for (int i = 0; i < BasketListItem.Count; ++i)
        {
            BasketListItem[i].Clear();
        }
    }
    ////////////////////////////



    //////////////////////////// bouquet
    public int GetAllMatCount_bouquet(int id_)
    {
        int cnt = 0;
        for(int i = 0; i < BouquetListItem.Count; ++i)
        {
            cnt += BouquetListItem[i].GetMatCount(id_);
        }
        
        return cnt;
    }

    public int GetAllGoldCost_bouquet()
    {
        int cost = 0;

        for(int i = 0; i < BouquetListItem.Count; ++i)
        {
            cost += BouquetListItem[i].GetGoldCost();
        }

       return cost;
    }
    ////////////////////////////

    ////////////////////////////basket
    public int GetAllMatCount_basket(int id_)
    {
        int cnt = 0;
        for (int i = 0; i < BasketListItem.Count; ++i)
        {
            cnt += BasketListItem[i].GetMatCount(id_);
        }

        return cnt;
    }

    public int GetAllGoldCost_basket()
    {
        int cost = 0;

        for (int i = 0; i < BasketListItem.Count; ++i)
        {
            cost += BasketListItem[i].GetGoldCost();
        }

        return cost;
    }
    ////////////////////////////

    public void OnClickedFlowerBtn()
    {
        MenuAni[(int)WorkMenuState.FlowerList].gameObject.SetActive(true);
        MenuAni[(int)WorkMenuState.Menu].Play("Work_Menu_Out");
        MenuAni[(int)WorkMenuState.FlowerList].Play("Work_FlowerList_In");
        Nutrients_Count.text = InventoryManager.Get.GetItemCount(37).ToString();
        MenuState = WorkMenuState.FlowerList;
        NutrientsCheckImg_Flower.SetActive(false);
        UseBuff = false;
    }

    public void OnClickedBoquetBtn()
    {
        MenuAni[(int)WorkMenuState.Boquet].gameObject.SetActive(true);
        MenuAni[(int)WorkMenuState.Menu].Play("Work_Menu_Out");
        MenuAni[(int)WorkMenuState.Boquet].Play("Work_Boquet_In");
        MenuState = WorkMenuState.Boquet;

        UseBuffCheckImg_Bouquet.SetActive(false);
        UseBuff = false;
        BuffItemCount_Bouquet.text = InventoryManager.Get.GetItemCount(38).ToString();
    }

    public void OnClickedBasketBtn()
    {
        MenuAni[(int)WorkMenuState.Basket].gameObject.SetActive(true);
        MenuAni[(int)WorkMenuState.Menu].Play("Work_Menu_Out");
        MenuAni[(int)WorkMenuState.Basket].Play("Work_Basket_In");
        MenuState = WorkMenuState.Basket;

        UseBuffCheckImg_Basket.SetActive(false);
        UseBuff = false;
        BuffItemCount_Basket.text = InventoryManager.Get.GetItemCount(38).ToString();
    }

    public void OnClickedFlowerClose()
    {
        MenuAni[(int)WorkMenuState.Menu].gameObject.SetActive(true);
        MenuAni[(int)WorkMenuState.Menu].Play("Work_Menu_In");
        MenuAni[(int)WorkMenuState.FlowerList].Play("Work_FlowerList_Out");
        AllCancelWorkingList();
        MenuState = WorkMenuState.Menu;
        for (int i = 0; i < FlowerListItem.Count; ++i)
        {
            FlowerListItem[i].Clear();
        }
        FlowerScroll.ResetPosition();
    }

    public void OnClickedBoquetClose()
    {
        MenuAni[(int)WorkMenuState.Menu].gameObject.SetActive(true);
        MenuAni[(int)WorkMenuState.Menu].Play("Work_Menu_In");
        MenuAni[(int)WorkMenuState.Boquet].Play("Work_Boquet_Out");
        AllCancelWorkingList();
        MenuState = WorkMenuState.Menu;
        for (int i = 0; i < BouquetListItem.Count; ++i)
        {
            BouquetListItem[i].Clear();
        }
        BouquetScroll.ResetPosition();
    }

    public void OnClickedBasketClose()
    {
        MenuAni[(int)WorkMenuState.Menu].gameObject.SetActive(true);
        MenuAni[(int)WorkMenuState.Menu].Play("Work_Menu_In");
        MenuAni[(int)WorkMenuState.Basket].Play("Work_Basket_Out");
        AllCancelWorkingList();
        MenuState = WorkMenuState.Menu;
        for (int i = 0; i < BasketListItem.Count; ++i)
        {
            BasketListItem[i].Clear();
        }
        BasketScroll.ResetPosition();
    }

    public void OnClickedMakeFlowerUseNutrients()
    {
        if (UseBuff)
        {
            NutrientsCheckImg_Flower.SetActive(false);
            Nutrients_Count.text = InventoryManager.Get.GetItemCount(37).ToString();
            UseBuff = false;
        }
        else if (InventoryManager.Get.CheckUseItem(37, 1))
        {
            NutrientsCheckImg_Flower.SetActive(true);
            Nutrients_Count.text = (InventoryManager.Get.GetItemCount(37) - 1).ToString();
            UseBuff = true;
        }
        else
        {
            PopupManager.Get.OpenPopup("Popup_BuyBuffItem", 37);
        }
    }

    public void OnClickedBuff_Bouquet()
    {
        if(UseBuff)
        {
            UseBuffCheckImg_Bouquet.SetActive(false);
            UseBuff = false;
            BuffItemCount_Bouquet.text = InventoryManager.Get.GetItemCount(38).ToString();
        }
        else if(InventoryManager.Get.CheckUseItem(38, 1))
        {
            UseBuff = true;
            UseBuffCheckImg_Bouquet.SetActive(true);
            BuffItemCount_Bouquet.text = string.Format("{0}", InventoryManager.Get.GetItemCount(38) - 1);
        }
        else
        {
            PopupManager.Get.OpenPopup("Popup_BuyBuffItem", 38);
        }
    }

    public void OnClickedBuff_Basket()
    {
        if (UseBuff)
        {
            UseBuffCheckImg_Basket.SetActive(false);
            UseBuff = false;
            BuffItemCount_Basket.text = InventoryManager.Get.GetItemCount(38).ToString();
        }
        else if(InventoryManager.Get.CheckUseItem(38, 1))
        {
            UseBuff = true;
            UseBuffCheckImg_Basket.SetActive(true);
            BuffItemCount_Basket.text = string.Format("{0}", InventoryManager.Get.GetItemCount(38) - 1);
        }
        else
        {
            PopupManager.Get.OpenPopup("Popup_BuyBuffItem", 38);
        }
    }

    public void MenuOpen()
    {
        MenuAni[(int)WorkMenuState.Menu].gameObject.SetActive(true);
        MenuAni[(int)WorkMenuState.Menu].Play("Work_Menu_In");
        MenuState = WorkMenuState.Menu;
    }

    public void BuffItemRefrash()
    {
        switch(MenuState)
        {
            case WorkMenuState.FlowerList:
                Nutrients_Count.text = InventoryManager.Get.GetItemCount(37).ToString();
                break;
            case WorkMenuState.Boquet:
                BuffItemCount_Bouquet.text = InventoryManager.Get.GetItemCount(38).ToString();
                break;
            case WorkMenuState.Basket:
                BuffItemCount_Basket.text = InventoryManager.Get.GetItemCount(38).ToString();
                break;
        }
    }

    public override void ReFreshWindow()
    {
        BuffItemRefrash();

        List<Flower> list = DataManager.Get.GetFlowerList();

        for(int i = 0; i < FlowerListItem.Count; ++i)
        {
            Destroy(FlowerListItem[i].gameObject);
        }

        FlowerListItem.Clear();

        for (int i = 0; i < list.Count; ++i)
        {
            if (FlowerListItem.Count <= i && (list[i].UnLockCost == 0 || UserDataManager.Get.CheckUnLockFlower(list[i - 1].ID)))
            {
                GameObject o = GameObject.Instantiate(Resources.Load("Prefab/Work_Flower_Item")) as GameObject;
                o.transform.parent = FlowerGrid.transform;
                o.transform.localScale = Vector3.one;
                o.GetComponent<Work_Flower_Item>().SetData(list[i]);
                FlowerListItem.Add(o.GetComponent<Work_Flower_Item>());
            }
        }
        FlowerGrid.Reposition();

        base.ReFreshWindow();
    }
}
