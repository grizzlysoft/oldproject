﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;
using Keiwando.BigInteger;

public class TopBar : MonoBehaviour
{
    public UILabel GoldLabel;
    public UILabel CrystalLabel;
    public UILabel HeartLabel;
   
    public void UpdateGold(double value_)
    {
        //BigInteger g = new BigInteger((long)value_);
        GoldLabel.text = string.Format("{0:##,##0}", value_);
        //GoldLabel.text = NumericalFormatter.Format(g);
    }

    public void UpdataeCrystalLabel(int value_)
    {
        CrystalLabel.text = string.Format("{0:##,##0}", value_);
    }

    public void UpdateHeartLabel(int value_)
    {
        if (value_ >= 999)
            HeartLabel.text = "Max";
        else
            HeartLabel.text = string.Format("{0:##,##0}", value_);
    }

    public void PlusGoods(int type_, int value_)
    {
        GameObject o = GameObject.Instantiate(Resources.Load("Prefab/PlusGoods")) as GameObject;
        switch(type_)
        {
            case 0:
                o.transform.parent = GoldLabel.transform;
                break;
            case 1:
                o.transform.parent = CrystalLabel.transform;
                break;
            case 2:
                o.transform.parent = HeartLabel.transform;
                break;
        }

        o.transform.localScale = Vector3.one;
        o.transform.localPosition = Vector3.zero;
        o.GetComponent<PlusGoods>().SetData(value_);
    }

    public void PlusGoods(int type_, long value_)
    {
        GameObject o = GameObject.Instantiate(Resources.Load("Prefab/PlusGoods")) as GameObject;
        switch (type_)
        {
            case 0:
                o.transform.parent = GoldLabel.transform;
                break;
            case 1:
                o.transform.parent = CrystalLabel.transform;
                break;
            case 2:
                o.transform.parent = HeartLabel.transform;
                break;
        }

        o.transform.localScale = Vector3.one;
        o.transform.localPosition = Vector3.zero;
        o.GetComponent<PlusGoods>().SetData(value_);
    }

    public void OnClickedMoveShopToGold()
    {
        MainUIManager.Get.QuickOpenWin(WindowType.Shop);
    }

    public void OnClickedMoveShopToCrystal()
    {
        MainUIManager.Get.QuickOpenWin(WindowType.Shop);
    }
}