﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class MainWindow : Window
{
    public UIGrid MenuGrid;

    public UIButton[] Menu = new UIButton[6];
    public UILabel[] MenuBtnText = new UILabel[6];

    public MainQuestTap QuestTap;
    public UIButton AchievementBtn;
    public GameObject Btns;

    public GameObject BtnCheat;

    public GameObject MiniGameMenu;

    public GameObject PackageBtn;

    public GameObject ActiveUIBtn;
    public GameObject HideUIBtn;

    public List<GameObject> HideObjList = new List<GameObject>();

    MainWinMove state = MainWinMove.Center;

    public override void WindowInit()
    {
        WinType = Enum.WindowType.Main;
        QuestTap.QuestTapInit();
        TextSetting();

        if (Main.Get.IsTest)
            BtnCheat.SetActive(true);
        else
            BtnCheat.SetActive(false);

        if (AchievementManager.Get.GetAchievementStep(AchievementType.AllPackageBuy) >= 1)
            PackageBtn.SetActive(false);
        else
            PackageBtn.SetActive(true);

        base.WindowInit();
    }

    public void MoveMainWindow(MainWinMove type_)
    {
        switch (type_)
        {
            case MainWinMove.Up_in:
                WindowAni.Play("Main_In_up");
                break;
            case MainWinMove.Up_out:
                WindowAni.Play("Main_Out_up");
                break;
            case MainWinMove.Left_in:
                WindowAni.Play("Main_In_left");
                break;
            case MainWinMove.Left_out:
                WindowAni.Play("Main_Out_left");
                break;
            case MainWinMove.Right_in:
                WindowAni.Play("Main_In_right");
                break;
            case MainWinMove.Right_out:
                WindowAni.Play("Main_Out_right");
                break;
            case MainWinMove.Down_in:
                WindowAni.Play("Main_In_down");
                break;
            case MainWinMove.Down_out:
                WindowAni.Play("Main_Out_down");
                break;
        }

        state = type_;
    }

    public override void OpenWindow()
    {
        switch (state)
        {
            case MainWinMove.Up_in:
                WindowAni.Play("Main_Out_up");
                break;
            case MainWinMove.Up_out:
                WindowAni.Play("Main_In_up");
                break;
            case MainWinMove.Left_in:
                WindowAni.Play("Main_Out_left");
                break;
            case MainWinMove.Left_out:
                WindowAni.Play("Main_In_left");
                break;
            case MainWinMove.Right_in:
                WindowAni.Play("Main_Out_right");
                break;
            case MainWinMove.Right_out:
                WindowAni.Play("Main_In_right");
                break;
            case MainWinMove.Down_in:
                WindowAni.Play("Main_Out_down");
                break;
            case MainWinMove.Down_out:
                WindowAni.Play("Main_In_down");
                break;
        }

        //if (AchievementManager.Get.GetAchievementStep(AchievementType.AllPackageBuy) >= 1)
        //    PackageBtn.SetActive(false);
        //else
        //    PackageBtn.SetActive(true);

        base.OpenWindow();
    }
    string[] btntext = { "작업", "부업", "강화", "창고", "옷장", "상점" };
    public override void TextSetting()
    {
        
        for(int i = 0; i < (int)MainMenuBtn.Max; ++i)
        {
            MenuBtnText[i].text = btntext[i];
        }


        base.TextSetting();
    }

    //자동이동으로 인해 실행될 경우
    public void CloseWindow(WindowType type_)
    {
        switch (type_)
        {
            case WindowType.Box: MoveMainWindow(MainWinMove.Left_out); break;
            case WindowType.Work: MoveMainWindow(MainWinMove.Left_out); break;
            case WindowType.SubWork: MoveMainWindow(MainWinMove.Left_out); break;
            case WindowType.Shop: MoveMainWindow(MainWinMove.Left_out); break;
            case WindowType.Attribute: MoveMainWindow(MainWinMove.Left_out); break;
            case WindowType.DressRoom: MoveMainWindow(MainWinMove.Left_out); break;
        }

        base.CloseWindow();
    }

    public void OnInAniEnd()
    {
        QuestTap.gameObject.SetActive(true);
        QuestTap.QuestTapInit();
        AchievementBtn.gameObject.SetActive(true);
        Btns.SetActive(true);
        if (AchievementManager.Get.GetAchievementStep(AchievementType.AllPackageBuy) >= 1)
            PackageBtn.SetActive(false);
        else
            PackageBtn.SetActive(true);

        HideUIBtn.SetActive(true);
    }

    public void OnOutAniStart()
    {
        QuestTap.transform.localPosition = new Vector3(300f, QuestTap.transform.localPosition.y, 0f);
        QuestTap.QuestIconOnOff(false);
        AchievementBtn.gameObject.SetActive(false);
        QuestTap.gameObject.SetActive(false);
        Btns.SetActive(false);
        MiniGameMenu.SetActive(false);
        PackageBtn.SetActive(false);
        HideUIBtn.SetActive(false);
    }

    public void OnClickedBtn_Box()
    {
        if (Main.Get.SceneMove_End)
        {
            MainUIManager.Get.OpenWindow(WindowType.Box);
            MoveMainWindow(MainWinMove.Left_out);
        }
    }

    public void OnClickedBtn_Shop()
    {
        if (Main.Get.SceneMove_End)
        {
            MainUIManager.Get.OpenWindow(WindowType.Shop);
            MoveMainWindow(MainWinMove.Left_out);
        }
    }

    public void OnClickedBtn_Work()
    {
        if (Main.Get.SceneMove_End)
        {
            MainUIManager.Get.OpenWindow(WindowType.Work);
            MoveMainWindow(MainWinMove.Left_out);
        }
    }

    public void OnClickedBtn_SubWork()
    {
        if (Main.Get.SceneMove_End)
        {
            MainUIManager.Get.OpenWindow(WindowType.SubWork);
            MoveMainWindow(MainWinMove.Left_out);
        }
    }

    public void OnClickedBtn_DressRoom()
    {
        if (Main.Get.SceneMove_End)
        {
            MainUIManager.Get.OpenWindow(WindowType.DressRoom);
            MoveMainWindow(MainWinMove.Left_out);
        }
    }

    public void OnClickedBtn_Reinforce()
    {
        if (Main.Get.SceneMove_End)
        {
            MainUIManager.Get.OpenWindow(WindowType.Attribute);
            MoveMainWindow(MainWinMove.Left_out);
        }
    }

    public void OnClickedCoupon()
    {
        //CouponManager.Get.UseYtop10Coupon("N5u0ichDTOxwq6b5");
        CouponManager.Get.UseHungryAppCoupon("Q746XGHV86");
    }

    public void OnClickedAchievement()
    {
        if (Main.Get.SceneMove_End)
        {
            PopupManager.Get.OpenPopup("Popup_Achievement");
        }
    }

    public void OnClickedGoogleLeaderBorad()
    {
        if (Main.Get.SceneMove_End)
        {
            GPGS_Manager.Get.ShowLeaderBoard();
        }
    }

    public void OnClickedMiniGames()
    {
        if (Main.Get.SceneMove_End)
        {
            MiniGameMenu.SetActive(!MiniGameMenu.activeInHierarchy);
        }
    }

    public void OnClickedSetting()
    {
        if (Main.Get.SceneMove_End)
        {
            PopupManager.Get.OpenPopup("Popup_Setting");
        }
    }

    public void OnClickedCheatBtn()
    {
        PopupManager.Get.OpenPopup("Popup_Cheat");
    }

    public void OnClickedAdShow()
    {
        if (Main.Get.SceneMove_End)
        {
            PopupManager.Get.OpenPopup("Popup_ShowAd");
        }
    }

    public void OnClickedBtnCardGame()
    {
        PopupManager.Get.OpenPopup("Popup_CardGameExplanation");
    }

    public void OnClickedBtnBoxGame()
    {
        PopupManager.Get.OpenPopup("Popup_WhereHouseExplanation");
    }

    public void OnClickedPackageBtn()
    {
        if (Main.Get.SceneMove_End)
        {
            PopupManager.Get.OpenPopup("Popup_Package");
        }
    }

    public void OnClickedHideUIBtn()
    {
        if (Main.Get.SceneMove_End)
        {
            for (int i = 0; i < HideObjList.Count; ++i)
                HideObjList[i].SetActive(false);

            MainUIManager.Get.HideMainUI = true;
            ActiveUIBtn.SetActive(true);
        }
    }

    public void OnClickedAcitveUIBtn()
    {
        for (int i = 0; i < HideObjList.Count; ++i)
            HideObjList[i].SetActive(true);

        MainUIManager.Get.HideMainUI = false;

        if (AchievementManager.Get.GetAchievementStep(AchievementType.AllPackageBuy) >= 1)
            PackageBtn.SetActive(false);
        else
            PackageBtn.SetActive(true);

        ActiveUIBtn.SetActive(false);
    }

    public void ActiveHideObj()
    {
        for (int i = 0; i < HideObjList.Count; ++i)
            HideObjList[i].SetActive(true);

        MainUIManager.Get.HideMainUI = false;

        if (AchievementManager.Get.GetAchievementStep(AchievementType.AllPackageBuy) >= 1)
            PackageBtn.SetActive(false);
        else
            PackageBtn.SetActive(true);

        ActiveUIBtn.SetActive(false);
    }
}
