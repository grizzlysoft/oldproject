﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Working : MonoBehaviour
{
    public Animation ani;

    public UILabel Text;

    public UIGrid IconGrid;
    public UISprite[] Icons = new UISprite[3];
    public UISprite CharacterImg;

    public UIProgressBar ProgressBar;
    public UILabel TimeText;

    public UIButton BtnComplte;
    public UILabel ComplteText;
    public UILabel Cost;
    public UISprite BtnIcon;

    public Animator CharAni;

    int CompleteCost = 1;
    int cancelgold = 0;
    // Use this for initialization
    void Start ()
    {
		
	}

    public void SetData()
    {
        this.gameObject.SetActive(true);
        cancelgold = 0;
        for(int i = 0; i < Icons.Length; ++i)
        {
            if (WorkManager.Get.WorkingData.WorkingItem[i] != null)
            {
                Icons[i].transform.parent.gameObject.SetActive(true);
                Icons[i].spriteName = WorkManager.Get.WorkingData.WorkingItem[i].Res;
                cancelgold += WorkManager.Get.WorkingData.WorkingItem[i].Sell * WorkManager.Get.WorkingData.ItemCount[i] / 2;
            }
            else
                Icons[i].transform.parent.gameObject.SetActive(false);
        }

        if (WorkManager.Get.WorkingData.WorkingItem[0].Type == 0)
            CharAni.Play("WorkingAni");
        else
            CharAni.Play("WorkingAni2");

        IconGrid.Reposition();
        Cost.text = string.Format("{0:##,##0}", CompleteCost);
        ani.Play("Work_WorkingOpen");
    }

    public void CloseAniEnd()
    {
        gameObject.SetActive(false);
    }

    public void OnClickedCompleteBtn()
    {
       
        IconTextData icontextdata = new IconTextData(Enum.IconTextType.Crystal, CompleteCost, -1);
        UseCrystalPopupData message = new UseCrystalPopupData("크리스탈을 사용하여\n작업을 바로 완료하시겠습니까 ?", Enum.MessagePopupType.Ok_Cancel, 
            icontextdata, "확인", "취소", UseCrystalDelegate, null);

        PopupManager.Get.OpenPopup("Popup_UesCrystal", message);
    }

    public void OnClickedWorkCancelBtn()
    {
        IconTextData icontextdata = new IconTextData(Enum.IconTextType.Gold, cancelgold, -1);
        UseCrystalPopupData message = new UseCrystalPopupData("골드를 사용하여\n작업을 취소 하시겠습니까 ?", Enum.MessagePopupType.Ok_Cancel,
            icontextdata, "확인", "취소", UseGoldDelegate, null);

        PopupManager.Get.OpenPopup("Popup_UesCrystal", message);
    }

    void UseCrystalDelegate()
    {
        if (UserDataManager.Get.UseCrystal(CompleteCost))
        {
            ani.Play("Work_WorkingClose");
            MainUIManager.Get.WorkWindowMenuOpen();
            WorkManager.Get.WorkEnd();
        }
        else
        {
            PopupManager.Get.OpenPopup("Popup_BuyCrystal", CompleteCost);
        }
    }

    void UseGoldDelegate()
    {
        if(UserDataManager.Get.UseGold(cancelgold))
        {
            PopupManager.Get.ClosePopup("Popup_UesCrystal");
            ani.Play("Work_WorkingClose");
            MainUIManager.Get.WorkWindowMenuOpen();
            WorkManager.Get.WorkCancel();
        }
        else
        {
            Message message = new Message(MainUIManager.Get.MoveShopGoldTapDelegate, null, "골드가 부족합니다.\n상점으로 이동하시겠습니까?", "확인", "취소", Enum.MessagePopupType.Ok_Cancel);
            PopupManager.Get.OpenPopup("Popup_Message", message);
        }
    }

    public void CloseWorking()
    {
        ani.Play("Work_WorkingClose");
        CompleteCost = 1;
    }
    
    // Update is called once per frame
    void Update()
    {
        if (WorkManager.Get.WorkingData != null)
        {
            TimeSpan lapse = WorkManager.Get.WorkingData.EndTime - DateTime.Now;
            float value = 1f - (float)lapse.TotalSeconds / WorkManager.Get.WorkingData.TotalTime;
            ProgressBar.value = value;

            string h = string.Empty;
            string m = string.Empty;
            string s = string.Empty;

            if (lapse.Hours < 10)
                h += "0" + lapse.Hours;
            else
                h += lapse.Hours.ToString();

            if (lapse.Minutes < 10)
                m += "0" + lapse.Minutes;
            else
                m += lapse.Minutes.ToString();

            if (lapse.Seconds < 10)
                s += "0" + lapse.Seconds;
            else
                s += lapse.Seconds.ToString();

            TimeText.text = "남은시간 : " + h + ":" + m + ":" + s;

            if ((int)lapse.TotalMinutes > 0 && CompleteCost != (int)lapse.TotalMinutes)
            {
                CompleteCost = (int)lapse.TotalMinutes;
                Cost.text = string.Format("{0:##,##0}", CompleteCost);
            }
        }
    }
}
