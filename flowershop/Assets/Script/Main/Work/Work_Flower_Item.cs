﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Work_Flower_Item : MonoBehaviour
{
    public UISprite FlowerIcon;
    public UILabel MakeCount;

    public UILabel Text;

    public GameObject BtnsGrid;
    public UIButton BtnPlus_1;
    public UIButton BtnPlus_10;
    public UIButton BtnPlus_100;
    public UIButton BtnCancel;

    public GameObject BtnUnLock;

    Flower FlowerData;
    int makecount = 0;

    public void SetData(Flower data_)
    {
        FlowerData = data_;
        Setting();
    }

    public void Setting()
    {
        bool unlock = UserDataManager.Get.CheckUnLockFlower(FlowerData.ID);

        FlowerIcon.spriteName = DataManager.Get.GetItemData(FlowerData.ResultID).Res;

        if (unlock)
        {
            BtnUnLock.SetActive(false);
            BtnsGrid.SetActive(true);
            MakeCount.gameObject.SetActive(true);
            MakeInfoUpdate();
        }
        else
        {
            BtnsGrid.SetActive(false);
            BtnUnLock.SetActive(true);
            MakeCount.gameObject.SetActive(false);
            Text.text = FlowerData.Name + "(구입 : " + FlowerData.UnLockCost + " 골드)";
        }
    }

    public void MakeInfoUpdate()
    {
        float time = FlowerData.WorkTime - DataManager.Get.GetAttributeData(Enum.AttributeType.Lend, UserDataManager.Get.GetAttributeLevel(Enum.AttributeType.Lend)).Abillity;

        if (time < 0.1f) time = 0.1f;

        MakeCount.text = makecount.ToString();
        Text.text = FlowerData.Name + "(작업시간 : [FFFF00]" + (time * makecount) + " [FFFFFF]초)";
    }

    public void Clear()
    {
        makecount = 0;
        bool unlock = UserDataManager.Get.CheckUnLockFlower(FlowerData.ID);

        if (unlock)
            MakeInfoUpdate();
    }

    public int GetCount()
    {
        return makecount;
    }

    public Flower GetFlowerData()
    {
        return FlowerData;
    }

    public void OnClickedBtnPlus_1()
    {
        float time = FlowerData.WorkTime - DataManager.Get.GetAttributeData(Enum.AttributeType.Lend, UserDataManager.Get.GetAttributeLevel(Enum.AttributeType.Lend)).Abillity;

        if (time < 0.1f) time = 0.1f;

        if (MainUIManager.Get.GetWindow(Enum.WindowType.Work).GetComponent<WorkWindow>().AddWorkingList(FlowerData.ResultID, 1, time, null, null))
        {
            makecount += 1;
            MakeInfoUpdate();
        }
    }

    public void OnClickedBtnPlus_10()
    {
        float time = FlowerData.WorkTime - DataManager.Get.GetAttributeData(Enum.AttributeType.Lend, UserDataManager.Get.GetAttributeLevel(Enum.AttributeType.Lend)).Abillity;

        if (time < 0.1f) time = 0.1f;

        if (MainUIManager.Get.GetWindow(Enum.WindowType.Work).GetComponent<WorkWindow>().AddWorkingList(FlowerData.ResultID, 10, time * 10, null, null))
        {
            makecount += 10;
            MakeInfoUpdate();
        }
    }

    public void OnClickedBtnPlus_100()
    {
        float time = FlowerData.WorkTime - DataManager.Get.GetAttributeData(Enum.AttributeType.Lend, UserDataManager.Get.GetAttributeLevel(Enum.AttributeType.Lend)).Abillity;

        if (time < 0.1f) time = 0.1f;

        if (MainUIManager.Get.GetWindow(Enum.WindowType.Work).GetComponent<WorkWindow>().AddWorkingList(FlowerData.ResultID, 100, time * 100, null, null))
        {
            makecount += 100;
            MakeInfoUpdate();
        }
    }

    public void OnClickedBtnCancel()
    {
        MainUIManager.Get.GetWindow(Enum.WindowType.Work).GetComponent<WorkWindow>().CancelWorkingList(FlowerData.ResultID);
       
        makecount = 0;
        MakeInfoUpdate();
    }

    public void OnClickedBtnUnLock()
    {
        if(!UserDataManager.Get.CheckUnLockFlower(FlowerData.UnlockConditionID))
        {
            Message message = new Message(null, null, DataManager.Get.GetFlowerData(FlowerData.UnlockConditionID).Name + "을 먼저 구입해야합니다.", "확인", string.Empty, Enum.MessagePopupType.Ok);
            PopupManager.Get.OpenPopup("Popup_Message", message);
            return;
        }

        if(UserDataManager.Get.UseGold(FlowerData.UnLockCost))
        {
            UserDataManager.Get.UnLockFlower(FlowerData.ID);
            Setting();
            MainUIManager.Get.GetWindow(Enum.WindowType.Work).GetComponent<WorkWindow>().SetFlowerScroll();
        }
        else
        {
            Message message = new Message(MainUIManager.Get.MoveShopGoldTapDelegate, null, "골드가 부족합니다.\n상점으로 이동하시겠습니까?", "확인", "취소", Enum.MessagePopupType.Ok_Cancel);
            PopupManager.Get.OpenPopup("Popup_Message", message);
        }
    }
}
