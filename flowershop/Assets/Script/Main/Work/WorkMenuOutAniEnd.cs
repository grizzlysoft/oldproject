﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkMenuOutAniEnd : MonoBehaviour
{
    public void AniEnd()
    {
        this.gameObject.SetActive(false);
    }
}
