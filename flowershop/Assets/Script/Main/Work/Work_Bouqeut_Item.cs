﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Work_Bouqeut_Item : MonoBehaviour
{
    public UISprite Icon;
    public UILabel MakeCount;

    public UILabel Text;

    public GameObject BtnsGrid;
    public UIButton BtnPlus_1;
    public UIButton BtnPlus_10;
    public UIButton BtnPlus_100;
    public UIButton BtnCancel;

    public UILabel MatText;
    public GameObject MatGrid;
    public GameObject[] MatFrame = new GameObject[3];
    public UISprite[] MatIcon = new UISprite[3];
    public UILabel[] MatCount = new UILabel[3];
    public UILabel GoldCost;

    BouquetData Data = null;
    int makecount = 0;

    ItemData[] Mat = new ItemData[3];

    public void SetData(BouquetData data_)
    {
        Data = data_;
        InfoSetting();
    }

    public void InfoSetting()
    {
        Icon.spriteName = DataManager.Get.GetItemData(Data.Result_ID).Res;
        MakeCount.text = makecount.ToString();
        Text.text = Data.Name + "(작업시간 : [FFFF00]0 [FFFFFF]초)";

        for (int i = 0; i < Data.Mat.Length; ++i)
        {
            if (Data.Mat[i].ID == -1)
            {
                Mat[i] = null;
                MatFrame[i].SetActive(false);
            }
            else
            {
                Mat[i] = DataManager.Get.GetItemData(Data.Mat[i].ID);
                MatFrame[i].SetActive(true);
                MatIcon[i].spriteName = Mat[i].Res;
                MatCount[i].text = "[FFFFFF]0";
                GoldCost.text = "[FFFFFF]0";
            }
        }
    }

    public void InfoUpdate()
    {
        MakeCount.text = makecount.ToString();
        float time = Data.WorkTime - (DataManager.Get.GetAttributeData(Enum.AttributeType.Work, UserDataManager.Get.GetAttributeLevel(Enum.AttributeType.Work)).Abillity);

        if (time < 0.1f) time = 0.1f;

        time = time * makecount;

        Text.text = string.Format(Data.Name + "(작업시간 : [FFFF00]{0} [FFFFFF]초)", time);

        for (int i = 0; i < Data.Mat.Length; ++i)
        {
            if (Data.Mat[i].ID != -1)
            {
                if (InventoryManager.Get.CheckUseItem(Data.Mat[i].ID, MainUIManager.Get.GetWindow(Enum.WindowType.Work).GetComponent<WorkWindow>().GetAllMatCount_bouquet(Data.Mat[i].ID)))
                    MatCount[i].text = "[FFFFFF]" + Data.Mat[i].Count * makecount;
                else
                    MatCount[i].text = "[FF0000]" + Data.Mat[i].Count * makecount;
            }
        }

        if(UserDataManager.Get.GetGold() >= MainUIManager.Get.GetWindow(Enum.WindowType.Work).GetComponent<WorkWindow>().GetAllGoldCost_bouquet())
        {
            GoldCost.text = string.Format("[FFFFFF]{0:##,##0}", Data.GoldCost * makecount);
        }
        else
        {
            GoldCost.text = string.Format("[FF0000]{0:##,##0}", Data.GoldCost * makecount);
        }
    }

    public int GetMatCount(int id_)
    {
        for(int i = 0; i < Data.Mat.Length; ++i)
        {
            if (id_ == Data.Mat[i].ID)
                return Data.Mat[i].Count * makecount;
        }

        return 0;
    }

    public int GetGoldCost()
    {
        return Data.GoldCost * makecount;
    }

    public void Clear()
    {
        makecount = 0;
        InfoUpdate();
    }

    public void OnClickedBtnPlus_1()
    {
        float time = Data.WorkTime - DataManager.Get.GetAttributeData(Enum.AttributeType.Work, UserDataManager.Get.GetAttributeLevel(Enum.AttributeType.Work)).Abillity;

        if (time < 0.1f) time = 0.1f;

        List<int> matid = new List<int>();
        List<int> matcount = new List<int>();
        for(int i = 0; i < Data.Mat.Length; ++i)
        {
            matid.Add(Data.Mat[i].ID);
            matcount.Add(Data.Mat[i].Count);
        }

        if (MainUIManager.Get.GetWindow(Enum.WindowType.Work).GetComponent<WorkWindow>().AddWorkingList(Data.Result_ID, 1, time, matid, matcount))
        {
            makecount += 1;
            InfoUpdate();
        }
    }

    public void OnClickedBtnPlus_10()
    {
        float time = Data.WorkTime - DataManager.Get.GetAttributeData(Enum.AttributeType.Work, UserDataManager.Get.GetAttributeLevel(Enum.AttributeType.Work)).Abillity;

        if (time < 0.1f) time = 0.1f;

        List<int> matid = new List<int>();
        List<int> matcount = new List<int>();
        for (int i = 0; i < Data.Mat.Length; ++i)
        {
            matid.Add(Data.Mat[i].ID);
            matcount.Add(Data.Mat[i].Count * 10);
        }

        if (MainUIManager.Get.GetWindow(Enum.WindowType.Work).GetComponent<WorkWindow>().AddWorkingList(Data.Result_ID, 10, time * 10, matid, matcount))
        {
            makecount += 10;
            InfoUpdate();
        }
    }

    public void OnClickedBtnPlus_100()
    {
        float time = Data.WorkTime - DataManager.Get.GetAttributeData(Enum.AttributeType.Work, UserDataManager.Get.GetAttributeLevel(Enum.AttributeType.Work)).Abillity;

        if (time < 0.1f) time = 0.1f;

        List<int> matid = new List<int>();
        List<int> matcount = new List<int>();
        for (int i = 0; i < Data.Mat.Length; ++i)
        {
            matid.Add(Data.Mat[i].ID);
            matcount.Add(Data.Mat[i].Count * 100);
        }

        if (MainUIManager.Get.GetWindow(Enum.WindowType.Work).GetComponent<WorkWindow>().AddWorkingList(Data.Result_ID, 100, time * 100, matid, matcount))
        {
            makecount += 100;
            InfoUpdate();
        }
    }

    public void OnClickedBtnCancel()
    {
        MainUIManager.Get.GetWindow(Enum.WindowType.Work).GetComponent<WorkWindow>().CancelWorkingList(Data.Result_ID);

        makecount = 0;
        InfoUpdate();
    }
}
