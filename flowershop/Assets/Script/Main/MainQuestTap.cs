﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class MainQuestTap : MonoBehaviour
{
    public UIGrid Grid;
    public MainQuestIcon[] QuestIcon = new MainQuestIcon[2];

    public void QuestTapInit()
    {
        for(int i = 0; i < QuestIcon.Length; ++i)
        {
            QuestIcon[i].SetInfo();
        }
        Grid.Reposition();
    }

    public void QuestIconRefresh()
    {
        for (int i = 0; i < QuestIcon.Length; ++i)
        {
            QuestIcon[i].Refresh();
        }
        Grid.Reposition();
    }

    public void QuestIconOnOff(bool active_)
    {
        for (int i = 0; i < QuestIcon.Length; ++i)
        {
            QuestIcon[i].gameObject.SetActive(active_);
        }
    }
}
