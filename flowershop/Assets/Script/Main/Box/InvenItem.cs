﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvenItem: MonoBehaviour
{
    public UISprite Icon;
    public UILabel Count;

    InventoryItem Item;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetData(InventoryItem data_)
    {
        if (data_ == null)
        {
            gameObject.SetActive(false);
            return;
        }
        else
        {
            gameObject.SetActive(true);
            Item = data_;
            ItemData item = DataManager.Get.GetItemData(data_.ID);
            Icon.spriteName = item.Res;
            Count.text = data_.Count.ToString();
        }
    }

    public void OnClickedItem()
    {
        //이제 판매 팝업 뜨면 됨
        PopupManager.Get.OpenPopup("Popup_ItemSell", Item.ID);
    }
}
