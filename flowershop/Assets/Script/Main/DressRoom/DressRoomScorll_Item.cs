﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DressRoomScorll_Item : MonoBehaviour
{
    public GameObject Block;
    public UILabel BuyLabel;
    public UISprite CostIcon;
    public UILabel Cost;

    public UISprite Clothes;
    public GameObject NameObj;
    public UILabel Name;

    public UISprite Background;

    CharacterClothes Data;

    public void SetData(CharacterClothes data_)
    {
        Data = data_;

        Clothes.spriteName = Data.ShopRes;

        if(UserDataManager.Get.CheckUnLockClothes(Data.ID))
        {
            NameObj.SetActive(true);
            Name.text = Data.Name;
            Block.SetActive(false);
            if (UserDataManager.Get.GetEquipClothesID() == Data.ID)
                Background.spriteName = "btn_clothes02";
            else
                Background.spriteName = "btn_clothes01";
        }
        else
        {
            NameObj.SetActive(false);
            Block.SetActive(true);
            switch(Data.Type)
            {
                case Enum.ClothesType.Crystal:
                    CostIcon.spriteName = "Crystal_Icon";
                    break;
                case Enum.ClothesType.Gold:
                    CostIcon.spriteName = "Gold_Icon";
                    break;
                case Enum.ClothesType.Heart:
                    CostIcon.spriteName = "Heart_Icon";
                    break;
            }
            Cost.text = string.Format("{0:##,##0}", Data.Cost);
        }
        SetText();
    }

    public void SetText()
    {
        BuyLabel.text = Data.Name;
    }

    public void ReFresh()
    {
        Clothes.spriteName = Data.ShopRes;
       
        if (UserDataManager.Get.CheckUnLockClothes(Data.ID))
        {
            NameObj.SetActive(true);
            Name.text = Data.Name;
            Block.SetActive(false);
            if (UserDataManager.Get.GetEquipClothesID() == Data.ID)
            {
                Background.spriteName = "btn_clothes02";
                Background.GetComponent<UIButton>().normalSprite = "btn_clothes02";
            }
            else
            {
                Background.spriteName = "btn_clothes01";
                Background.GetComponent<UIButton>().normalSprite = "btn_clothes01";
            }
        }
        else
        {
            NameObj.SetActive(false);
            Block.SetActive(true);
            switch (Data.Type)
            {
                case Enum.ClothesType.Crystal:
                    CostIcon.spriteName = "Crystal_Icon";
                    break;
                case Enum.ClothesType.Gold:
                    CostIcon.spriteName = "Gold_Icon";
                    break;
                case Enum.ClothesType.Heart:
                    CostIcon.spriteName = "Heart_Icon";
                    break;
            }
            Cost.text = string.Format("{0:##,##0}", Data.Cost);
        }
        SetText();
    }

    public void OnClickedUnLockBlock()
    {
        Message m = new Message(OnClickedBuy, null, Data.Name + "\n구입하시겠습니까?", "구입", "취소", Enum.MessagePopupType.Ok_Cancel);
        PopupManager.Get.OpenPopup("Popup_Message", m);
    }

    void OnClickedBuy()
    {
        PopupManager.Get.ClosePopup("Popup_Message");
        switch (Data.Type)
        {
            case Enum.ClothesType.Crystal:
                if (UserDataManager.Get.UseCrystal(Data.Cost))
                {
                    UserDataManager.Get.UnLockClothes(Data.ID);
                    ReFresh();
                }
                else
                {
                    Message m = new Message(MainUIManager.Get.MoveShopCrystalTapDelegate, null, "크리스탈이 부족합니다\n구매 하시겠습니까?", "이동", "취소", Enum.MessagePopupType.Ok_Cancel);
                    PopupManager.Get.OpenPopup("Popup_Message", m);
                }
                break;
            case Enum.ClothesType.Gold:
                if (UserDataManager.Get.UseGold(Data.Cost))
                {
                    UserDataManager.Get.UnLockClothes(Data.ID);
                    ReFresh();
                }
                else
                {
                    Message m = new Message(MainUIManager.Get.MoveShopGoldTapDelegate, null, "골드가 부족합니다\n구매 하시겠습니까?", "이동", "취소", Enum.MessagePopupType.Ok_Cancel);
                    PopupManager.Get.OpenPopup("Popup_Message", m);
                }
                break;
            case Enum.ClothesType.Heart:
                if (UserDataManager.Get.GetHeart() >= Data.Cost)
                {
                    UserDataManager.Get.UnLockClothes(Data.ID);
                    ReFresh();
                }
                else
                {
                    Message m = new Message(null, null, "평판이 부족합니다.", "확인", string.Empty);
                    PopupManager.Get.OpenPopup("Popup_Message", m);
                }
                break;
        }
    }

    public void OnClickedEquip()
    {
        if (Data.ID != UserDataManager.Get.GetEquipClothesID())
        {
            UserDataManager.Get.EquipClothes(Data.ID);
            MainUIManager.Get.GetWindow(Enum.WindowType.DressRoom).GetComponent<DressRoomWindow>().ReFreshWindow();
            MainUIManager.Get.ChangeCharacterClothes();
        }
    }
}
