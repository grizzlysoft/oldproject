﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class MainSceneCharacter : MonoBehaviour
{
    public GameObject TalkBox;
    public UISprite Box;
    public UILabel Text;
    bool IsCoolTime = false;

    // Use this for initialization
	void Start ()
    {
        TalkBox.SetActive(false);
	}

    public void OnClickedCharacter()
    {
        if (!IsCoolTime)
        {
            TalkBoxText data = DataManager.Get.GetRandomTalkBoxData();
            Text.text = data.Text;
            Text.fontSize = data.Size;

            Box.width = Text.width + 20;
            Box.height = Text.height + 43;

            if (!TalkBox.activeInHierarchy)
            {
                TalkBox.SetActive(true);
            }
            else
            {
                CancelInvoke("TalkBoxHide");
            }
            IsCoolTime = true;
            Invoke("TalkBoxHide", 5f);
            Invoke("CoolTimeEnd", 2f);
        }
    }

    void TalkBoxHide()
    {
        TalkBox.SetActive(false);
    }

    void CoolTimeEnd()
    {
        IsCoolTime = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
