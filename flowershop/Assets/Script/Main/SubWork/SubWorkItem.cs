﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubWorkItem : MonoBehaviour
{
    public UISprite Icon;
    public UILabel Name;
    public UILabel TimeAndResult;
    public UILabel BtnName;
    public UILabel Btn_CostText;
    public UISprite Btn_CostIcon;
    public UIProgressBar Progress;
    public UIButton Btn;
    public UILabel LabelMax;

    SubWorkData Data = null;
    int level = 0;
    float WorkTime = 0;
    int result = 0;
    public void SetData(SubWorkData data_)
    {
        Data = data_;
        InfoSetting();
    }

    public void InfoSetting()
    {
        level = SubWorkManager.Get.GetSubWorkLevel(Data.Type);

        WorkTime = Data.WorkTime - (DataManager.Get.GetAttributeData(Enum.AttributeType.SubWork, UserDataManager.Get.GetAttributeLevel(Enum.AttributeType.SubWork)).Abillity);
        if (WorkTime < 1f) WorkTime = 1f;

        Icon.spriteName = Data.Res;

        if (level > 0)
        {
            if (Data.MaxLv <= level)
            {
                Name.text = Data.Name + "Lv.MAX";
                Name.transform.localPosition = new Vector3(-17f, 28f, 0f);
                BtnName.gameObject.SetActive(false);
                Btn_CostText.gameObject.SetActive(false);
                Btn_CostIcon.gameObject.SetActive(false);
                LabelMax.gameObject.SetActive(true);
            }
            else
            {
                Name.text = Data.Name + " Lv." + level;
                Name.transform.localPosition = new Vector3(-17f, 28f, 0f);
                BtnName.gameObject.SetActive(true);
                Btn_CostText.gameObject.SetActive(true);
                Btn_CostIcon.gameObject.SetActive(true);
                LabelMax.gameObject.SetActive(false);
                BtnName.text = "레벨업";
                Btn_CostText.text = (Data.FirstUpgradeCost + (Data.LvupUpgradeCost * (level - 1))).ToString();
            }

            result = Data.Lv1Result + (Data.LvupResut * (level - 1));
            result += (int)(result * (DataManager.Get.GetAttributeData(Enum.AttributeType.Insentive, UserDataManager.Get.GetAttributeLevel(Enum.AttributeType.Insentive)).Abillity * 0.01));
            Progress.gameObject.SetActive(true);
            TimeAndResult.text = WorkTime + "초 / " + result + "골드";
        }
        else
        {
            Name.text = Data.Name;
            TimeAndResult.text = string.Empty;
            Name.transform.localPosition = new Vector3(-17f, 0f, 0f);
            BtnName.text = "신규";
            Btn_CostText.text = Data.UnLockCost.ToString();
            Progress.gameObject.SetActive(false);
            BtnName.gameObject.SetActive(true);
            Btn_CostText.gameObject.SetActive(true);
            Btn_CostIcon.gameObject.SetActive(true);
            LabelMax.gameObject.SetActive(false);
        }
    }

    public void OnClickedBtn()
    {
        int cost = 0;

        if (level >= Data.MaxLv)
        {
            Message message = new Message(null, null, "최대 레벨입니다.", "확인", string.Empty, Enum.MessagePopupType.Ok);
            PopupManager.Get.OpenPopup("Popup_Message", message);
            return;
        }

        if (level > 0)
        {
            cost = Data.FirstUpgradeCost + (Data.LvupUpgradeCost * (level - 1));
        }
        else
        {
            if (Data.UnLockConditionID != -1 && SubWorkManager.Get.GetSubWorkLevel(Data.UnLockConditionID) == 0)
            {
                Debug.Log(Data.UnLockConditionID);
                Message message = new Message(null, null, DataManager.Get.GetSubWorkData(Data.UnLockConditionID).Name + "이(가)\nLv.1 이상 이여야 합니다", "확인", string.Empty, Enum.MessagePopupType.Ok);
                PopupManager.Get.OpenPopup("Popup_Message", message);
                return;
            }
            else
                cost = Data.UnLockCost;
        }
        //Debug.Log("cost = " + cost);
        if (UserDataManager.Get.UseGold(cost))
        {
            SubWorkManager.Get.LevelupSubWork(Data.Type);
            InfoSetting();
        }
        else
        {
            Message message = new Message(MainUIManager.Get.MoveShopGoldTapDelegate, null, "골드가 부족합니다.\n상점으로 이동하시겠습니까?", "확인", "취소", Enum.MessagePopupType.Ok_Cancel);
            PopupManager.Get.OpenPopup("Popup_Message", message);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (level > 0)
        {
            Progress.value = SubWorkManager.Get.GetDeltatime(Data.Type) / WorkTime;
            System.TimeSpan t = new System.TimeSpan(0, 0, (int)(WorkTime - SubWorkManager.Get.GetDeltatime(Data.Type)));
            int m = (int)t.Minutes;
            int s = (int)t.Seconds;
            string text = string.Empty;
            m += t.Hours * 60;

            if (m > 0)
                text += m + "분 ";

            text += s + "초";
            TimeAndResult.text = text + " / " + result + "골드";
        }
    }
}
