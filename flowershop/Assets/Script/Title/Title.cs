﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Title : MonoBehaviour
{
    public UILabel TitleText;
    public TweenAlpha TextAlpha;

    public void OnClickedTitleBg()
    {
#if !UNITY_EDITOR
        if (GPGS_Manager.Get.bLogin)
        {
            //Debug.Log("눌렀다고!");
            if (TutorialManager.Get.Clear != 0)
                Main.Get.ChangeScene(Enum.SceneType.Main);
            else
                Main.Get.ChangeScene(Enum.SceneType.Event);
        }
#else
        if (TutorialManager.Get.Clear != 0)
            Main.Get.ChangeScene(Enum.SceneType.Main);
        else
            Main.Get.ChangeScene(Enum.SceneType.Event);
#endif
    }

    void Update()
    {
        if(GPGS_Manager.Get.bLogin)
        {
            TitleText.text = "화면을 터치해 주세요.";
            if (TextAlpha.enabled == false)
                TextAlpha.PlayForward();
        }
        else
        {
            TitleText.text = "로그인 중입니다.";
        }
    }
}
