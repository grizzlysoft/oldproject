﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class MainUIManager : Singleton<MainUIManager>
{
    public TopBar TopBar;
    public List<Window> WinList = new List<Window>();

    public UITexture CharacterTex;

    WindowType OpenState = WindowType.Main;
    int reward = 0;

    [HideInInspector]
    public bool HideMainUI = false;

    public void Awake()
    {
        if(!SoundManager.Get.Bgm_Obj.isPlaying)
            SoundManager.Get.PlayBgm(Bgm.Like_a_Flower);
        //Debug.Log("gold = " + UserDataManager.Get.GetGold());
    }

    public void Start()
    {
        ChangeCharacterClothes();
        TopBar.UpdateGold(UserDataManager.Get.GetGold());
        TopBar.UpdataeCrystalLabel(UserDataManager.Get.GetCrystal());
        TopBar.UpdateHeartLabel(UserDataManager.Get.GetHeart());

        if (!UserDataManager.Get.GetOfflineReward && UserDataManager.Get.OfflineStart < System.DateTime.Now)
        {
            System.TimeSpan offtime = System.DateTime.Now - UserDataManager.Get.OfflineStart;

            int min = 0;

            if (offtime.TotalHours > 24)
            {
                min = 60 * 24;
            }
            else if (offtime.TotalMinutes <= 0)
                min = 1;
            else
                min = (int)offtime.TotalMinutes;

            reward = DataManager.Get.GetOfflineReward() * min;

            if (reward > 0)
            {
                UseCrystalPopupData message = new UseCrystalPopupData("오프라인 보상으로\n" + reward + "골드를 받았습니다\n(광고 시청시 2배획득)", MessagePopupType.Ok_Cancel, 
                    new IconTextData(IconTextType.Gold, reward, -1), "확인", "광고", OnClickedOfflineOkBtn, OnClickedOfflineShowAD);

                PopupManager.Get.OpenPopup("Popup_UesCrystal", message, false);
            }
        }
    }

    void OnClickedOfflineOkBtn()
    {
        UserDataManager.Get.GetOfflineReward = true;
        UserDataManager.Get.AddGold(reward);
        reward = 0;
        PopupManager.Get.ClosePopup("Popup_UesCrystal");
    }

    void OnClickedOfflineShowAD()
    {
        Ads_Manager.Get.showAd_admob(ADShowType.OffLine, reward);
    }

    public WindowType WinState
    {
        get
        {
            return OpenState;
        }

        set
        {
            OpenState = value;
        }
    }

    public void OpenWindow(WindowType type_)
    {
        WinList[(int)type_].OpenWindow();
    }

    public void QuickOpenWin(WindowType type_)
    {
        if (OpenState == type_) return;

        if (OpenState != WindowType.Main)
            WinList[(int)OpenState].CloseWindow();
        else
            WinList[(int)OpenState].GetComponent<MainWindow>().CloseWindow(type_);

        WinList[(int)type_].OpenWindow();
    }

    public void QuickOpenShop(ShopTapType type_)
    {
        if (OpenState == WindowType.Shop) return;

        type_ = ShopTapType.Item;
        WinList[(int)OpenState].CloseWindow();
        WinList[(int)WindowType.Shop].GetComponent<ShopWindow>().ChangeSelectTap(type_);
        WinList[(int)WindowType.Shop].OpenWindow();
    }

    /// <summary>
    /// work 창에 메뉴를 오픈한다
    /// 작업이 끝났을 시 사용
    /// </summary>
    public void WorkWindowMenuOpen()
    {
        WinList[(int)WindowType.Work].GetComponent<WorkWindow>().MenuOpen();
    }

    public Window GetWindow(WindowType type_)
    {
        return WinList[(int)type_];
    }

    public void MoveAttributeWindowDelegate()
    {
        PopupManager.Get.AllClosePopup();

        if (WinState == WindowType.Work)
            WinList[(int)WindowType.Work].GetComponent<WorkWindow>().WorkItemAllClear();

        QuickOpenWin(WindowType.Attribute);
    }

    public void MoveBoxWindowDelegate()
    {
        PopupManager.Get.AllClosePopup();

        if (WinState == WindowType.Work)
            WinList[(int)WindowType.Work].GetComponent<WorkWindow>().WorkItemAllClear();

        QuickOpenWin(WindowType.Box);
    }

    public void MoveShopGoldTapDelegate()
    {
        PopupManager.Get.AllClosePopup();

        if (WinState == WindowType.Work)
            WinList[(int)WindowType.Work].GetComponent<WorkWindow>().WorkItemAllClear();

        QuickOpenShop(ShopTapType.Gold);
    }

    public void MoveShopCrystalTapDelegate()
    {
        PopupManager.Get.AllClosePopup();

        if (WinState == WindowType.Work)
            WinList[(int)WindowType.Work].GetComponent<WorkWindow>().WorkItemAllClear();

        QuickOpenShop(ShopTapType.Crystal);
    }

    public void ReFreshWindows()
    {
        WinList[(int)WindowType.Work].ReFreshWindow();
        WinList[(int)WindowType.SubWork].ReFreshWindow();
        WinList[(int)WindowType.Attribute].ReFreshWindow();
        WinList[(int)WindowType.Box].ReFreshWindow();
        WinList[(int)WindowType.DressRoom].ReFreshWindow();
    }

    public void ChangeCharacterClothes()
    {
        Resources.UnloadAsset(CharacterTex.mainTexture);
        CharacterTex.mainTexture = Resources.Load("Clothes/" + DataManager.Get.GetClothesData(UserDataManager.Get.GetEquipClothesID()).Res) as Texture;
    }

    public void ActiveHideMainObj()
    {
        WinList[(int)WindowType.Main].GetComponent<MainWindow>().ActiveHideObj();
    }
}
