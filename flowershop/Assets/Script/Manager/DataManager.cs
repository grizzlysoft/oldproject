﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class DataManager : Singleton<DataManager>
{
    ParseUtile utlie = new ParseUtile();

    List<Flower> FlowerList = new List<Flower>();

    List<ItemData> ItemDataList = new List<ItemData>();

    List<SubWorkData> SubWorkDataList = new List<SubWorkData>();

    Dictionary<AttributeType, List<AttributeData>> AttributeDataDic = new Dictionary<AttributeType, List<AttributeData>>();

    List<BouquetData> BouquetDataList = new List<BouquetData>();

    List<BasketData> BasketDataList = new List<BasketData>();

    List<ShopData> ShopDataList = new List<ShopData>();

    List<RewardData> RewardDataList = new List<RewardData>();

    int[] AttributeMaxLevel = new int[(int)AttributeType.Max];

    List<OffLineReward> OffLineRewardDataList = new List<OffLineReward>();

    List<AchievementData> AchievementDataList = new List<AchievementData>();

    List<QuestData> QuestDataList = new List<QuestData>();

    List<ReQuestData> ReQuestDataList = new List<ReQuestData>();

    List<CharacterClothes> ClothesDataList = new List<CharacterClothes>();

    List<ShowAdsData> ShowAdsDataList = new List<ShowAdsData>();

    Dictionary<int, List<EventScriptData>> EventScriptDataDic = new Dictionary<int, List<EventScriptData>>();

    List<TalkBoxText> TalkBoxTextDataList = new List<TalkBoxText>();

    public void DataLoad()
    {
        Load_FlowerData();
        Load_ItemData();
        Load_SubWorkData();
        Load_AttributeData();
        Load_BouquetData();
        Load_BasketData();
        Load_ShopData();
        Load_RewardData();
        Load_OffLineRewardData();
        Load_AchievementData();
        Load_QuestData();
        Load_ReQuestData();
        Load_ClothesData();
        Load_ShowAdsData();
        Load_ConstantData();
        Load_EventScriptData();
        Load_TalkBoxData();
    }

    public void Load_FlowerData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Flower", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                int cnt = 0;
                if (strArray[i] == "") break;

                string[] stral = strArray[i].Split('|');

                int id = int.Parse(stral[cnt]); cnt++;
                string name = stral[cnt]; cnt++;
                string text = stral[cnt]; cnt++;
                float worktime = float.Parse(stral[cnt]); cnt++;
                int unlockcost = int.Parse(stral[cnt]); cnt++;
                int resultid = int.Parse(stral[cnt]); cnt++;
                int condition = int.Parse(stral[cnt]); cnt++;

                Flower item = new Flower(id, name, text, worktime, unlockcost, resultid, condition);

                FlowerList.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Flower");
        }
    }

    public void Load_ItemData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Storage", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                int cnt = 0;

                int type = int.Parse(stral[cnt]); cnt++;
                int id = int.Parse(stral[cnt]); cnt++;
                string name = stral[cnt]; cnt++;
                string text = stral[cnt]; cnt++;
                int max = int.Parse(stral[cnt]); cnt++;
                int sell = int.Parse(stral[cnt]); cnt++;
                string res = stral[cnt]; cnt++;

                ItemData item = new ItemData(id, name, text, max, sell, res, type);

                ItemDataList.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Item");
        }
    }

    public void Load_SubWorkData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/2ndWork", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                int cnt = 0;
                if (strArray[i] == "") break;

                string[] stral = strArray[i].Split('|');

                int type = int.Parse(stral[cnt]); cnt++;
                string name = stral[cnt]; cnt++;
                int worktime = int.Parse(stral[cnt]); cnt++;
                int firstresult = int.Parse(stral[cnt]); cnt++;
                int upgraderesult = int.Parse(stral[cnt]); cnt++;
                int fristupgradecost = int.Parse(stral[cnt]); cnt++;
                int upgradecost = int.Parse(stral[cnt]); cnt++;
                int unlockcost = int.Parse(stral[cnt]); cnt++;
                int maxlv = int.Parse(stral[cnt]); cnt++;
                string res = stral[cnt]; cnt++;
                int condition = int.Parse(stral[cnt]); cnt++;

                SubWorkData item = new SubWorkData(type, name, worktime, firstresult, upgraderesult, fristupgradecost, upgradecost, unlockcost, maxlv, res, condition);

                SubWorkDataList.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  subwork");
        }
    }

    public void Load_AttributeData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Upgrade", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                int cnt = 0;
                if (strArray[i] == "") break;

                string[] stral = strArray[i].Split('|');
                
                AttributeType type = (AttributeType)int.Parse(stral[cnt]); cnt++;
                int level = int.Parse(stral[cnt]); cnt++;
                string name = stral[cnt]; cnt++;
                string text = stral[cnt]; cnt++;
                long upcost = long.Parse(stral[cnt]); cnt++;
                float aillity = float.Parse(stral[cnt]); cnt++;
                int unlock = int.Parse(stral[cnt]); cnt++;
                AttributeCostType costtype = (AttributeCostType)int.Parse(stral[cnt]); cnt++;
                string res = stral[cnt]; cnt++;

                AttributeData item = new AttributeData(type, level, name, text, upcost, aillity, unlock, costtype, res);

                if (AttributeDataDic.ContainsKey(type))
                    AttributeDataDic[type].Add(item);
                else
                {
                    AttributeDataDic.Add(type, new List<AttributeData>());
                    AttributeDataDic[type].Add(item);
                }

                if (AttributeMaxLevel[(int)type] < level)
                    AttributeMaxLevel[(int)type] = level;
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  attribute");
        }
    }

    public void Load_BouquetData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Bouquet", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                int cnt = 0;
                if (strArray[i] == "") break;

                string[] stral = strArray[i].Split('|');

                int id = int.Parse(stral[cnt]); cnt++;
                string name = stral[cnt]; cnt++;
                string text = stral[cnt]; cnt++;
                int mat1_id = int.Parse(stral[cnt]); cnt++;
                int mat1_count = int.Parse(stral[cnt]); cnt++;
                int mat2_id = int.Parse(stral[cnt]); cnt++;
                int mat2_count = int.Parse(stral[cnt]); cnt++;
                int mat3_id = int.Parse(stral[cnt]); cnt++;
                int mat3_count = int.Parse(stral[cnt]); cnt++;
                float worktime = float.Parse(stral[cnt]); cnt++;
                int goldcost = int.Parse(stral[cnt]); cnt++;
                int resultid = int.Parse(stral[cnt]); cnt++;

                MatData mat1 = new MatData(mat1_id, mat1_count);
                MatData mat2 = new MatData(mat2_id, mat2_count);
                MatData mat3 = new MatData(mat3_id, mat2_count);

                MatData[] mat = { mat1, mat2, mat3 };

                BouquetData item = new BouquetData(id, name, text, mat, worktime, goldcost, resultid);

                BouquetDataList.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Bouquet");
        }
    }

    public void Load_BasketData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/FlowerBasket", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                int cnt = 0;
                if (strArray[i] == "") break;

                string[] stral = strArray[i].Split('|');

                int id = int.Parse(stral[cnt]); cnt++;
                string name = stral[cnt]; cnt++;
                string text = stral[cnt]; cnt++;
                int mat1_id = int.Parse(stral[cnt]); cnt++;
                int mat1_count = int.Parse(stral[cnt]); cnt++;
                int mat2_id = int.Parse(stral[cnt]); cnt++;
                int mat2_count = int.Parse(stral[cnt]); cnt++;
                int mat3_id = int.Parse(stral[cnt]); cnt++;
                int mat3_count = int.Parse(stral[cnt]); cnt++;
                float worktime = float.Parse(stral[cnt]); cnt++;
                int goldcost = int.Parse(stral[cnt]); cnt++;
                int resultid = int.Parse(stral[cnt]); cnt++;

                MatData mat1 = new MatData(mat1_id, mat1_count);
                MatData mat2 = new MatData(mat2_id, mat2_count);
                MatData mat3 = new MatData(mat3_id, mat2_count);

                MatData[] mat = { mat1, mat2, mat3 };

                BasketData item = new BasketData(id, name, text, mat, worktime, goldcost, resultid);

                BasketDataList.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  BasketData");
        }
    }

    public void Load_ShopData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Shop", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                int cnt = 0;
                if (strArray[i] == "") break;

                string[] stral = strArray[i].Split('|');

                int id = int.Parse(stral[cnt]); cnt++;
                ShopTapType type = (ShopTapType)int.Parse(stral[cnt]); cnt++;
                Shop_PayType paytype = (Shop_PayType)int.Parse(stral[cnt]); cnt++;
                int cost = int.Parse(stral[cnt]); cnt++;
                string name = stral[cnt]; cnt++;
                string text = stral[cnt]; cnt++;
                string res = stral[cnt]; cnt++;
                List<int> rewardid = utlie.StringPareser(stral[cnt]); cnt++;
                string storeid = stral[cnt]; cnt++;

                ShopData item = new ShopData(id, type, paytype, cost, name, text, res, rewardid, storeid);

                ShopDataList.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  ShopData");
        }
    }

    public void Load_RewardData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Reward", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                int cnt = 0;
                if (strArray[i] == "") break;

                string[] stral = strArray[i].Split('|');

                int id = int.Parse(stral[cnt]); cnt++;
                RewardType type = (RewardType)int.Parse(stral[cnt]); cnt++;
                string name = stral[cnt]; cnt++;
                int itemid = int.Parse(stral[cnt]); cnt++;
                int value = int.Parse(stral[cnt]); cnt++;

                RewardData item = new RewardData(id, type, name, itemid, value);

                RewardDataList.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  RewardData");
        }
    }

    public void Load_OffLineRewardData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Offline_Reward", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                int cnt = 0;
                if (strArray[i] == "") break;

                string[] stral = strArray[i].Split('|');

                int id = int.Parse(stral[cnt]); cnt++;
                int reward = int.Parse(stral[cnt]); cnt++;

                OffLineReward item = new OffLineReward(id, reward);

                OffLineRewardDataList.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  OffLineRewardData");
        }
    }

    public void Load_AchievementData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Challenge", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                int cnt = 0;
                if (strArray[i] == "") break;

                string[] stral = strArray[i].Split('|');

                AchievementType type = (AchievementType)int.Parse(stral[cnt]); cnt++;
                int id = int.Parse(stral[cnt]); cnt++;
                string name = stral[cnt]; cnt++;
                int clearvalue = int.Parse(stral[cnt]); cnt++;
                int nextid = int.Parse(stral[cnt]); cnt++;
                int first = int.Parse(stral[cnt]); cnt++;
                int rewardid = int.Parse(stral[cnt]); cnt++;
                string gpgsid = stral[cnt]; cnt++;

                AchievementData item = new AchievementData(type, id, name, clearvalue, nextid, first, rewardid, gpgsid);

                AchievementDataList.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  OffLineRewardData");
        }
    }

    public void Load_QuestData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Quest", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                int cnt = 0;
                if (strArray[i] == "") break;

                string[] stral = strArray[i].Split('|');

                QuestType type = (QuestType)int.Parse(stral[cnt]); cnt++;
                int id = int.Parse(stral[cnt]); cnt++;
                string name = stral[cnt]; cnt++;
                string res = stral[cnt]; cnt++;
                string startscript = stral[cnt]; cnt++;
                int eventid = int.Parse(stral[cnt]); cnt++;
                List<int> requestid = utlie.StringPareser(stral[cnt]); cnt++;
                int mingood = int.Parse(stral[cnt]); cnt++;
                int maxgood = int.Parse(stral[cnt]); cnt++;
                List<int> rewardid = utlie.StringPareser(stral[cnt]); cnt++;
                int refusegood = int.Parse(stral[cnt]); cnt++;

                QuestData item = new QuestData(type, id, name, res, startscript, eventid, requestid, mingood, maxgood, rewardid, refusegood);

                QuestDataList.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  QuestData");
        }
    }

    public void Load_ReQuestData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Quest_Request", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                int cnt = 0;
                if (strArray[i] == "") break;

                string[] stral = strArray[i].Split('|');

                int id = int.Parse(stral[cnt]); cnt++;
                string name = stral[cnt]; cnt++;
                int itemid = int.Parse(stral[cnt]); cnt++;
                int count = int.Parse(stral[cnt]); cnt++;

                ReQuestData item = new ReQuestData(id, name, itemid, count);

                ReQuestDataList.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  QuestData");
        }
    }

    public void Load_ClothesData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Clothes", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                int cnt = 0;
                if (strArray[i] == "") break;

                string[] stral = strArray[i].Split('|');

                int id = int.Parse(stral[cnt]); cnt++;
                ClothesType type = (ClothesType)int.Parse(stral[cnt]); cnt++;
                string name = stral[cnt]; cnt++;
                int cost = int.Parse(stral[cnt]); cnt++;
                string res = stral[cnt]; cnt++;
                string shopres = stral[cnt]; cnt++;

                CharacterClothes item = new CharacterClothes(id, type, name, cost, res, shopres);

                ClothesDataList.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  ClothesData");
        }
    }

    public void Load_ShowAdsData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/ADS", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                int cnt = 0;
                if (strArray[i] == "") break;

                string[] stral = strArray[i].Split('|');

                int id = int.Parse(stral[cnt]); cnt++;
                string name = stral[cnt]; cnt++;
                int count = int.Parse(stral[cnt]); cnt++;
                string res = stral[cnt]; cnt++;
                int rewardid = int.Parse(stral[cnt]); cnt++;

                ShowAdsData item = new ShowAdsData(id, name, count, res, rewardid);

                ShowAdsDataList.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  ADS");
        }
    }

    public void Load_ConstantData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/MinigameReward", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                int cnt = 0;
                if (strArray[i] == "") break;

                string[] stral = strArray[i].Split('|');

                MiniGameManager.Get.CardBasic_Reward = int.Parse(stral[cnt]); cnt++;
                MiniGameManager.Get.CardBestRecode_Reward = int.Parse(stral[cnt]); cnt++;
                MiniGameManager.Get.CardSuccece_Reward = int.Parse(stral[cnt]); cnt++;
                MiniGameManager.Get.WhereHouseMove_Reward = int.Parse(stral[cnt]); cnt++;
                MiniGameManager.Get.whereHouseCombo_Reward = int.Parse(stral[cnt]); cnt++;
                MiniGameManager.Get.WhereHouseBest_Reward = int.Parse(stral[cnt]); cnt++;
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  MinigameReward");
        }
    }

    public void Load_EventScriptData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Event_Script", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                int cnt = 0;
                if (strArray[i] == "") break;

                string[] stral = strArray[i].Split('|');
               
                int id = int.Parse(stral[cnt]); cnt++;
                string eventname = stral[cnt]; cnt++;
                string l_res = stral[cnt]; cnt++;
                string r_res = stral[cnt]; cnt++;
                string charname = stral[cnt]; cnt++;
                string script = stral[cnt]; cnt++;

                EventScriptData item = new EventScriptData(id, eventname, l_res, r_res, charname, script);

                if(EventScriptDataDic.ContainsKey(item.ID))
                {
                    EventScriptDataDic[item.ID].Add(item);
                }
                else
                {
                    List<EventScriptData> list = new List<EventScriptData>();
                    list.Add(item);
                    EventScriptDataDic.Add(item.ID, list);
                }

            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  QuestData");
        }
    }

    public void Load_TalkBoxData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Scripts", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                int cnt = 0;
                if (strArray[i] == "") break;

                string[] stral = strArray[i].Split('|');

                int id = int.Parse(stral[cnt]); cnt++;
                int size = int.Parse(stral[cnt]); cnt++;
                string text = stral[cnt]; cnt++;

                TalkBoxText item = new TalkBoxText(id, size, text);

                TalkBoxTextDataList.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Scripts");
        }
    }

    public ItemData GetItemData(int id_)
    {
        for (int i = 0; i < ItemDataList.Count; ++i)
        {
            if (id_ == ItemDataList[i].ID)
            {
                return ItemDataList[i];
            }
        }

        return null;
    }

    public List<ItemData> GetItemList()
    {
        return ItemDataList;
    }

    public List<Flower> GetFlowerList()
    {
        return FlowerList;
    }

    public Flower GetFlowerData(int id_)
    {
        for (int i = 0; i < FlowerList.Count; ++i)
        {
            if (id_ == FlowerList[i].ID)
            {
                return FlowerList[i];
            }
        }

        return null;
    }

    public List<BouquetData> GetBouquetList()
    {
        return BouquetDataList;
    }

    public List<BasketData> GetBasketList()
    {
        return BasketDataList;
    }

    public List<SubWorkData> GetSubWorkList()
    {
        return SubWorkDataList;
    }

    public SubWorkData GetSubWorkData(int id_)
    {
        for (int i = 0; i < SubWorkDataList.Count; ++i)
        {
            if (id_ == SubWorkDataList[i].Type)
                return SubWorkDataList[i];
        }

        return null;
    }

    public AttributeData GetAttributeData(AttributeType type_, int level_)
    {
        for (int i = 0; i < AttributeDataDic[type_].Count; ++i)
        {
            if (level_ == AttributeDataDic[type_][i].Level)
                return AttributeDataDic[type_][i];
        }

        return null;
    }

    public List<ShopData> GetShopDataList(ShopTapType type_)
    {
        List<ShopData> list = new List<ShopData>();
        for (int i = 0; i < ShopDataList.Count; ++i)
        {
            if (type_ == ShopDataList[i].Type)
                list.Add(ShopDataList[i]);
        }

        return list;
    }

    public List<ShopData> GetShopDataList(Shop_PayType type_)
    {
        List<ShopData> list = new List<ShopData>();
        for (int i = 0; i < ShopDataList.Count; ++i)
        {
            if (type_ == ShopDataList[i].PayType)
                list.Add(ShopDataList[i]);
        }

        return list;
    }

    public ShopData GetShopData(int id_)
    {
        for (int i = 0; i < ShopDataList.Count; ++i)
        {
            if (id_ == ShopDataList[i].ID)
                return ShopDataList[i];
        }

        return null;
    }

    public ShopData GetShopData(string id_)
    {
        for (int i = 0; i < ShopDataList.Count; ++i)
        {
            if (id_ == ShopDataList[i].StoreID)
                return ShopDataList[i];
        }

        return null;
    }

    public RewardData GetRewardData(int id_)
    {
        for (int i = 0; i < RewardDataList.Count; ++i)
        {
            if (id_ == RewardDataList[i].ID)
                return RewardDataList[i];
        }

        return null;
    }

    public int GetAttributeMaxLevel(AttributeType type_)
    {
        return AttributeMaxLevel[(int)type_];
    }

    public int GetOfflineReward()
    {
        int id = -1;

        for (int i = 0; i < SubWorkDataList.Count; ++i)
        {
            if (SubWorkManager.Get.GetSubWorkLevel(SubWorkDataList[i].Type) > 0 && id < SubWorkDataList[i].Type)
            {
                id = SubWorkDataList[i].Type;
            }
        }

        for (int i = 0; i < OffLineRewardDataList.Count; ++i)
        {
            if (OffLineRewardDataList[i].SubWorkID == id)
                return OffLineRewardDataList[i].Reward;
        }

        return 0;
    }

    public AchievementData GetAchievementData(int id_)
    {
        for (int i = 0; i < AchievementDataList.Count; ++i)
        {
            if (AchievementDataList[i].ID == id_)
                return AchievementDataList[i];
        }

        return null;
    }

    public List<AchievementData> GetAchievementDataList()
    {
        return AchievementDataList;
    }

    public List<AchievementData> GetAchievementDataList(AchievementType type_)
    {
        List<AchievementData> list = new List<AchievementData>();

        for (int i = 0; i < AchievementDataList.Count; ++i)
        {
            if (AchievementDataList[i].Type == type_)
                list.Add(AchievementDataList[i]);
        }

        return list;
    }

    public List<QuestData> GetQuestDataList()
    {
        return QuestDataList;
    }

    public List<int> GetQuestDataList(QuestType type_)
    {
        List<int> list = new List<int>();

        for (int i = 0; i < QuestDataList.Count; ++i)
        {
            if (QuestDataList[i].Type == type_)
                list.Add(QuestDataList[i].ID);
        }

        return list;
    }

    public List<QuestData> GetQuestDataList(QuestType type_, int good_)
    {
        List<QuestData> list = new List<QuestData>();

        for (int i = 0; i < QuestDataList.Count; ++i)
        {
            if (QuestDataList[i].Type == type_ && QuestDataList[i].MinGood <= good_ && QuestDataList[i].MaxGood >= good_)
                list.Add(QuestDataList[i]);
        }

        return list;
    }

    public QuestData GetQuestData(int id_)
    {
        for(int i = 0; i < QuestDataList.Count; ++i)
        {
            if (QuestDataList[i].ID == id_)
                return QuestDataList[i];
        }

        return null;
    }

    public ReQuestData GetReqeustData(int id_)
    {
        for(int i = 0; i < ReQuestDataList.Count; ++i)
        {
            if (ReQuestDataList[i].ID == id_)
                return ReQuestDataList[i];
        }

        return null;
    }

    public List<CharacterClothes> GetClothesDataList()
    {
        return ClothesDataList;
    }

    public CharacterClothes GetClothesData(int id_)
    {
        for(int i = 0; i < ClothesDataList.Count; ++i)
        {
            if (id_ == ClothesDataList[i].ID)
                return ClothesDataList[i];
        }

        return null;
    }

    public ShowAdsData GetShowADsData(int count_)
    {
        for(int i = 0; i < ShowAdsDataList.Count; ++i)
        {
            if (ShowAdsDataList[i].Count == count_)
                return ShowAdsDataList[i];
        }

        return null;
    }

    public List<EventScriptData> GetEventScriptDataList(int id_)
    {
        if(EventScriptDataDic.ContainsKey(id_))
        {
            return EventScriptDataDic[id_];
        }

        return null;
    }

    public TalkBoxText GetRandomTalkBoxData()
    {
        int index = Random.Range(0, TalkBoxTextDataList.Count);

        return TalkBoxTextDataList[index];
    }
}
