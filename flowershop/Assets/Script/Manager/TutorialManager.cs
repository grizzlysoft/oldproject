﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : Singleton<TutorialManager>
{
    int TutorialClear = 0;
    public int Clear
    {
        get { return TutorialClear; }
        set
        {
            TutorialClear = value;
            PlayerPrefs.SetInt("TutorialClear", TutorialClear);
        }
    }

    public void TutorialInit()
    {
        if(PlayerPrefs.HasKey("TutorialClear"))
        {
            TutorialClear = PlayerPrefs.GetInt("TutorialClear");
        }
    }
}
