﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class PaymentManager : Singleton<PaymentManager>
{
    public void BuyPrice(ShopData data_)
    {
        List<int> itemid = new List<int>();
        List<int> count = new List<int>();
        Message m;
        //Debug.Log("rewardcount = " + data_.RewardID.Count);
        for (int i = 0; i < data_.RewardID.Count; ++i)
        {
            RewardData r = DataManager.Get.GetRewardData(data_.RewardID[i]);

            if (r.ItemID != -1)
            {
                itemid.Add(r.ItemID);
                count.Add(r.Value);
            }
        }

        if(!InventoryManager.Get.AddCheck(itemid, count))
        {
            Debug.Log("인벤 부족");
        }

        switch(data_.PayType)
        {
            case Shop_PayType.Cash:
                //for(int i = 0; i < data_.RewardID.Count; ++i)
                //{
                //    PayPrice(data_.RewardID[i]);
                //}
                m = new Message(null, null, "구매 요청중.", "확인", string.Empty, MessagePopupType.Ok);
                PopupManager.Get.OpenPopup("Popup_Message", m);
                Purchasing.Get.Buy(data_.StoreID);
                break;
            case Shop_PayType.Crystal:
                for (int i = 0; i < data_.RewardID.Count; ++i)
                {
                    PayPrice(data_.RewardID[i]);
                }
                m = new Message(null, null, "상품이 정상적으로 구매 되었습니다.", "확인", string.Empty, MessagePopupType.Ok);
                PopupManager.Get.OpenPopup("Popup_Message", m);
                break;
            case Shop_PayType.Gold:
                for (int i = 0; i < data_.RewardID.Count; ++i)
                {
                    PayPrice(data_.RewardID[i]);
                }
                m = new Message(null, null, "상품이 정상적으로 구매 되었습니다.", "확인", string.Empty, MessagePopupType.Ok);
                PopupManager.Get.OpenPopup("Popup_Message", m);
                break;
            case Shop_PayType.Ads_Unity:
                Ads_Manager.Get.ShowAd_UnityAds(ADShowType.Shop, data_.RewardID);
                break;
            case Shop_PayType.Ads_AdMob:
                Ads_Manager.Get.showAd_admob(data_.RewardID);
                break;
        }

    }

    /// <summary>
    /// 상품지급
    /// </summary>
    /// <param name="id_">리워드 시트 id</param>
    public void PayPrice(int id_)
    {
        RewardData r = DataManager.Get.GetRewardData(id_);

        switch(r.Type)
        {
            case RewardType.Crystal:
                UserDataManager.Get.AddCrystal(r.Value);
                break;
            case RewardType.Gold:
                UserDataManager.Get.AddGold(r.Value);
                break;
            case RewardType.Item:
                InventoryManager.Get.AddItem(r.ItemID, r.Value);
                break;
            case RewardType.Heart:
                UserDataManager.Get.AddHeart(r.Value);
                break;
        }
    }

    /// <summary>
    /// 상품지급
    /// </summary>
    /// <param name="id_">스토어 id</param>
    public void PayPrice(string id_)
    {
        ShopData s = DataManager.Get.GetShopData(id_);

        for(int i = 0; i < s.RewardID.Count; ++i)
        {
            PayPrice(s.RewardID[i]);
        }

        Message m = new Message(null, null, "상품이 정상적으로 구매 되었습니다.", "확인", string.Empty, MessagePopupType.Ok);
        PopupManager.Get.OpenPopup("Popup_Message", m);
    }

}
