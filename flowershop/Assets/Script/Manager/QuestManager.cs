﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class QuestManager : Singleton<QuestManager>
{
    /// <summary>
    /// key = quest id, value = 0 not clear, 1 clear
    /// </summary>
    Dictionary<int, int> QuestClear = new Dictionary<int, int>();
    public Dictionary<int, int> QuestClearState
    {
        get { return QuestClear; }
    }

    QuestData NowNomralQuest = null;

    public QuestData NomarlQuest
    {
        get { return NowNomralQuest; }
    }

    QuestData NowMainQuest = null;

    public QuestData MainQuest
    {
        get { return NowMainQuest; }
    }

    int AcceptNomralQuest = 0;

    public int GetAcceptNormal
    {
        get { return AcceptNomralQuest; }
    }

    int AcceptMainQuest = 0;

    public int GetAcceptMain
    {
        get { return AcceptMainQuest; }
    }

    System.DateTime QuestDelayEnd = System.DateTime.MinValue;

    public System.DateTime NomarlQuestDelay
    {
        get { return QuestDelayEnd; }
    }

    public void InitQuestManager()
    {
        List<QuestData> list = DataManager.Get.GetQuestDataList();

        for (int i = 0; i < list.Count; ++i)
        {
            //Debug.Log("QuestClear " + list[i].ID + "hashkey = " + PlayerPrefs.HasKey("QuestClear" + list[i].ID));
            if (PlayerPrefs.HasKey("QuestClear" + list[i].ID))
            {
                //Debug.Log(list[i].ID + "clear = " + PlayerPrefs.GetInt("QuestClear" + list[i].ID));
                QuestClear.Add(list[i].ID, PlayerPrefs.GetInt("QuestClear" + list[i].ID));
            }
            else
            {
                QuestClear.Add(list[i].ID, 0);
            }
        }

        if (PlayerPrefs.HasKey("NowNomralQuest"))
            NowNomralQuest = DataManager.Get.GetQuestData(PlayerPrefs.GetInt("NowNomralQuest"));

        if (NowNomralQuest != null && QuestClear[NowNomralQuest.ID] == 1)
            NowNomralQuest = null;

        if (PlayerPrefs.HasKey("NowMainQuest"))
        {
            NowMainQuest = DataManager.Get.GetQuestData(PlayerPrefs.GetInt("NowMainQuest"));
            if(NowMainQuest != null && QuestClear[NowMainQuest.ID] == 1)
            {
                NowMainQuest = null;
            }
        }

        if (PlayerPrefs.HasKey("NomarlQuestAccept"))
            AcceptNomralQuest = PlayerPrefs.GetInt("NomarlQuestAccept");

        if (PlayerPrefs.HasKey("MainQuestAccept"))
            AcceptMainQuest = PlayerPrefs.GetInt("MainQuestAccept");

        if (PlayerPrefs.HasKey("QuestDelayEnd"))
            QuestDelayEnd = new System.DateTime(long.Parse(PlayerPrefs.GetString("QuestDelayEnd")));

        if (NowNomralQuest == null)
        {
            QuestOccur_Nomarl();
        }

        if(NowMainQuest == null)
        {
            QuestOccur_Main();
        }
    }

    public void QuestOccur_Nomarl()
    {
        int good = UserDataManager.Get.GetHeart();

        //노말리스트 클리어 여부 체크
        List<int> list_nomarlid = DataManager.Get.GetQuestDataList(QuestType.Nomral);
        list_nomarlid.Sort();

        bool nomarl_allclear = false;

        for (int i = 0; i < list_nomarlid.Count; ++i)
        {
            if (QuestClear[list_nomarlid[i]] == 1)
                nomarl_allclear = true;
            else
            {
                nomarl_allclear = false;
                break;
            }
        }

        //노말퀘스트 다 못깼으면 준다
        if (!nomarl_allclear)
        {
            for (int i = 0; i < list_nomarlid.Count; ++i)
            {
                if (QuestClear[list_nomarlid[i]] == 0)
                {
                    NowNomralQuest = DataManager.Get.GetQuestData(list_nomarlid[i]);
                    break;
                }
            }
        }
        //노말퀘스트 다 깼으면 반복퀘 준다(good 수치에 따라서)
        else
        {
            List<QuestData> loop = DataManager.Get.GetQuestDataList(QuestType.Loop, good);
            if (loop.Count > 0)
                NowNomralQuest = loop[Random.Range(0, loop.Count)];
            else
                NowNomralQuest = null;
        }

        //퀘스트 갱신됐으니 ui도 갱신해준다
        if (NowNomralQuest != null)
        {
            PlayerPrefs.SetInt("NowNomralQuest", NowNomralQuest.ID);
            AcceptNomralQuest = 0;
            PlayerPrefs.SetInt("NomarlQuestAccept", AcceptNomralQuest);
        }
        if (Main.Get.GetNowScene() == SceneType.Main)
            MainUIManager.Get.GetWindow(WindowType.Main).GetComponent<MainWindow>().QuestTap.QuestTapInit();
    }

    public void QuestOccur_Main()
    {
        int good = UserDataManager.Get.GetHeart();

        //메인퀘스트 클리어 여부 체크
        List<int> list_mainlid = DataManager.Get.GetQuestDataList(QuestType.Event);
        list_mainlid.Sort();

        bool Main_allclear = false;

        for (int i = 0; i < list_mainlid.Count; ++i)
        {
            if (QuestClear[list_mainlid[i]] == 1)
                Main_allclear = true;
            else
            {
                Main_allclear = false;
                break;
            }
        }

        //메인퀘스트 다 못깼으면 준다
        if (!Main_allclear)
        {
            for (int i = 0; i < list_mainlid.Count; ++i)
            {
                QuestData q = DataManager.Get.GetQuestData(list_mainlid[i]);
                //Debug.Log(q.ID + " quset clear = " + QuestClear[q.ID]);
                if (QuestClear[q.ID] == 0 && q.MinGood <= good && q.MaxGood >= good)
                {
                    NowMainQuest = q;
                    break;
                }
            }
        }
        else
        {
            NowMainQuest = null;
        }

        if (NowMainQuest != null)
        {
            PlayerPrefs.SetInt("NowMainQuest", NowMainQuest.ID);
            AcceptNomralQuest = 0;
            PlayerPrefs.SetInt("MainQuestAccept", AcceptNomralQuest);
            if (Main.Get.GetNowScene() == SceneType.Main)
                MainUIManager.Get.GetWindow(WindowType.Main).GetComponent<MainWindow>().QuestTap.QuestTapInit();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="type_">0 = nomarl, loop, 1 = main</param>
    public bool CompleteQuest(int type_)
    {
        if (type_ == 0)
        {
            if (!InventoryManager.Get.CheckRequestItem(NowNomralQuest.RequestID))
            {
                //퀘완료 아님
                Message message = new Message(null, null, "완료조건이 충족되지 않았습니다.", "확인", string.Empty, MessagePopupType.Ok);
                PopupManager.Get.OpenPopup("Popup_Message", message);
                return false;
            }
            else
            {
                for(int i = 0; i < NowNomralQuest.RequestID.Count; ++i)
                {
                    ReQuestData r = DataManager.Get.GetReqeustData(NowNomralQuest.RequestID[i]);
                    InventoryManager.Get.UseItem(r.ItemID, r.Count);
                }

                //퀘완료 저장
                QuestClear[NowNomralQuest.ID] = 1;
                PlayerPrefs.SetInt("QuestClear" + NowNomralQuest.ID, 1);
                Debug.Log("save qeustclear " + NowNomralQuest.ID + " = " + QuestClear[NowNomralQuest.ID]);
                AcceptNomralQuest = 0;
                //보상지급
                List<IconTextData> icontextdatalist = new List<IconTextData>();
                for(int i = 0; i < NowNomralQuest.RewardID.Count; ++i)
                {
                    PaymentManager.Get.PayPrice(NowNomralQuest.RewardID[i]);
                    icontextdatalist.Add(DataManager.Get.GetRewardData(NowNomralQuest.RewardID[i]).ConvertIconText());
                }

                GoodsMessage message = new GoodsMessage("보상을 받았습니다.", icontextdatalist, "확인", string.Empty, null, null);
                PopupManager.Get.OpenPopup("Popup_GoodsMessage", message);
                AchievementManager.Get.AchievementStepUp(AchievementType.NomarlQuestClear, 1);
                NowNomralQuest = null;

                QuestOccur_Nomarl();

                return true;
            }
        }
        else
        {
            if (!InventoryManager.Get.CheckRequestItem(NowMainQuest.RequestID))
            {
                //퀘완료 아님
                Message message = new Message(null, null, "완료조건이 충족되지 않았습니다.", "확인", string.Empty, MessagePopupType.Ok);
                PopupManager.Get.OpenPopup("Popup_Message", message);
                return false;
            }
            else
            {
                for (int i = 0; i < NowMainQuest.RequestID.Count; ++i)
                {
                    ReQuestData r = DataManager.Get.GetReqeustData(NowMainQuest.RequestID[i]);
                    InventoryManager.Get.UseItem(r.ItemID, r.Count);
                }

                //퀘완료 저장
                QuestClear[NowMainQuest.ID] = 1;
                PlayerPrefs.SetInt("QuestClear" + NowMainQuest.ID, 1);
                AcceptMainQuest = 0;
                //보상지급
                List<IconTextData> icontextdatalist = new List<IconTextData>();
                for (int i = 0; i < NowMainQuest.RewardID.Count; ++i)
                {
                    PaymentManager.Get.PayPrice(NowMainQuest.RewardID[i]);
                    icontextdatalist.Add(DataManager.Get.GetRewardData(NowMainQuest.RewardID[i]).ConvertIconText());
                }
                //event id 세팅
                if (NowMainQuest.EventID != -1)
                    UserDataManager.Get.Event_ID = NowMainQuest.EventID;

                GoodsMessage message = new GoodsMessage("보상을 받았습니다.", icontextdatalist, "확인", string.Empty, MainQuestComplet, null);
                PopupManager.Get.OpenPopup("Popup_GoodsMessage", message);

                NowMainQuest = null;

                QuestOccur_Main();

                return true;
            }
        }
    }

    void MainQuestComplet()
    {
        if (UserDataManager.Get.Event_ID != -1)
            Main.Get.ChangeScene(SceneType.Event);
    }

    public void AcceptQuest_Nomral()
    {
        AcceptNomralQuest = 1;
        PlayerPrefs.SetInt("NomarlQuestAccept", AcceptNomralQuest);

        if (Main.Get.GetNowScene() == SceneType.Main)
            MainUIManager.Get.GetWindow(WindowType.Main).GetComponent<MainWindow>().QuestTap.QuestIconRefresh();
    }

    public void CancelQuest_Nomral()
    {
        AcceptNomralQuest = 0;
        PlayerPrefs.SetInt("NomarlQuestAccept", AcceptNomralQuest);
        NowNomralQuest = null;
        QuestOccur_Nomarl();
        QuestDelayEnd = System.DateTime.Now.AddSeconds(60);
        PlayerPrefs.SetString("QuestDelayEnd", QuestDelayEnd.Ticks.ToString());
    }

    //메인퀘는 캔슬이 없다
    public void AcceptQuest_Main()
    {
        AcceptMainQuest = 1;
        PlayerPrefs.SetInt("NomarlQuestAccept", AcceptMainQuest);

        if (Main.Get.GetNowScene() == SceneType.Main)
            MainUIManager.Get.GetWindow(WindowType.Main).GetComponent<MainWindow>().QuestTap.QuestIconRefresh();
    }

    public void QuestRefresh()
    {
        NowMainQuest = null;
        NowNomralQuest = null;
        QuestOccur_Main();
        QuestOccur_Nomarl();
    }
}
