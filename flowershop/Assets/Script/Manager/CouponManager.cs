﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CouponManager : Singleton<CouponManager>
{
    [HideInInspector]
    public int GamsaCouponGet = 0;

    public void CouponManagerInit()
    {
        GamsaCouponGet = PlayerPrefs.GetInt("Gamsa");
    }

    public void UseHungryAppCoupon(string s)
    {
        if (s == "GAMSAYOM")
        {
            if (GamsaCouponGet == 0)
            {
                GamsaCouponGet = 1;
                PlayerPrefs.SetInt("Gamsa", GamsaCouponGet);
                IconTextData icontext = new IconTextData(Enum.IconTextType.Crystal, 10, -1);
                List<IconTextData> Icontextlist = new List<IconTextData>();
                Icontextlist.Add(icontext);
                UserDataManager.Get.AddCrystal(10);
                GoodsMessage message = new GoodsMessage("아이템을 지급 받았습니다", Icontextlist, "확인", string.Empty, null, null);
                PopupManager.Get.OpenPopup("Popup_GoodsMessage", message);
            }
            else
            {
                Message message = new Message(null, null, "이미 사용한 쿠폰입니다.", "확인", string.Empty);
                PopupManager.Get.OpenPopup("Popup_Message", message);
            }
        }
        else
        {
            WWW w = new WWW("http://couponsystems.kr/api/UseCoupon.php?key=0edcf328cc93871a&coupon=" + s + "&id=" + Social.localUser.id);
            StartCoroutine(ConnentHungryAppCoupon(w));
        }
        
    }

    IEnumerator ConnentHungryAppCoupon(WWW w)
    {
        yield return w;

        if (w == null)
        {
            Debug.Log("w = null!");
        }
        else if (w != null)
        {
            //Debug.Log("www error = " + w.error);
            HappCoupon c = new HappCoupon();

            c = JsonUtility.FromJson<HappCoupon>(w.text);

            //Debug.Log("www.text = " + w.text);
            //Debug.Log(c.result);
            //Debug.Log(c.error_code);
            //Debug.Log(c.error_msg);
            //Debug.Log(c.benefit);

            //Debug.Log(c.items);
            if (c.result == "1")
            {
                List<IconTextData> Icontextlist = new List<IconTextData>();
                for (int i = 0; i < c.items.Length; ++i)
                {
                    switch (c.items[i].item_code)
                    {
                        case "CRYSTAL":
                            UserDataManager.Get.AddCrystal(int.Parse(c.items[i].item_count));
                            Icontextlist.Add(new IconTextData(Enum.IconTextType.Crystal, int.Parse(c.items[i].item_count), -1));
                            break;
                        case "GOLD":
                            UserDataManager.Get.AddGold(int.Parse(c.items[i].item_count));
                            Icontextlist.Add(new IconTextData(Enum.IconTextType.Gold, int.Parse(c.items[i].item_count), -1));
                            break;
                        case "NUTRIENTS":
                            InventoryManager.Get.AddItem(37, int.Parse(c.items[i].item_count));
                            Icontextlist.Add(new IconTextData(Enum.IconTextType.Item, int.Parse(c.items[i].item_count), 37));
                            break;
                        case "GLOVES":
                            InventoryManager.Get.AddItem(38, int.Parse(c.items[i].item_count));
                            Icontextlist.Add(new IconTextData(Enum.IconTextType.Item, int.Parse(c.items[i].item_count), 38));
                            break;
                    }
                }

                GoodsMessage message = new GoodsMessage("아이템을 지급 받았습니다", Icontextlist, "확인", string.Empty, null, null);
                PopupManager.Get.OpenPopup("Popup_GoodsMessage", message);
            }
            else
            {
                Message message = new Message(null, null, c.error_msg, "확인", string.Empty);
                PopupManager.Get.OpenPopup("Popup_Message", message);
            }
        }
    }
}
