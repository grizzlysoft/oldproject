﻿using Enum;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public  class Main : Singleton<Main>
{
    public TweenAlpha Fade;
    public Camera FadeCam;
    public bool DataClear = false;
    public bool IsTest = false;

    public UISpriteAnimation ClickEffect;

    Camera MainCamea;

    SceneType moveto = SceneType.Start;
    SceneType now = SceneType.Start;
    bool SceneMoveEnd = true;
    public bool SceneMove_End { get { return SceneMoveEnd; } }

    private void Awake()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        Application.targetFrameRate = 30;
        Assets.SimpleAndroidNotifications.NotificationManager.CancelAll();
    }

    void Start()
    {
#if UNITY_EDITOR
        if (DataClear)
            PlayerPrefs.DeleteAll();
#endif
        //Screen.orientation = ScreenOrientation.LandscapeLeft;
       
        DontDestroyOnLoad(this.gameObject);
        DontDestroyOnLoad(GameObject.Find("FadeInOut"));
        DontDestroyOnLoad(GameObject.Find("Managers"));
        DontDestroyOnLoad(GameObject.Find("SoundManager"));

        DataManager.Get.DataLoad();
        UserDataManager.Get.LoadPlayPrefab();
        QuestManager.Get.InitQuestManager();

        AchievementManager.Get.AchievementInit();
        MiniGameManager.Get.ManagerInit();
        TutorialManager.Get.TutorialInit();
        CouponManager.Get.CouponManagerInit();

        Purchasing.Get.InitPurchasing();

        SceneManager.sceneLoaded += OnSceneLoad;
        SceneManager.sceneUnloaded += OnSceneUnLoad;

        PopupManager.Get.SetPopupParent(GameObject.Find("Popups").transform);

        GPGS_Manager.Get.InitializeGPGS();
        LogoEnd();
        System.DateTime nextdaymidnight = new System.DateTime(System.DateTime.Now.AddDays(1).Year, System.DateTime.Now.AddDays(1).Month, System.DateTime.Now.AddDays(1).Day);
        Invoke("DateChange", (float)(nextdaymidnight - System.DateTime.Now).TotalSeconds);
        //Debug.Log("ad count reset value = " + (float)(nextdaymidnight - System.DateTime.Now).TotalSeconds);
        //Debug.Log("next midnight = " + nextdaymidnight);
    }

    void LogoEnd()
    {
        ChangeScene(SceneType.Title);
    }
   
	// Update is called once per frame
	void Update ()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if (PopupManager.Get.GetOpenPopupCnt() <= 0)
            {
                if (now == SceneType.Main && MainUIManager.Get.HideMainUI)
                    MainUIManager.Get.ActiveHideMainObj();
                else
                    OpenExitPopup();
            }
            else
            {
                PopupManager.Get.ClosePopup();
            }
        }

        if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            if (null != MainCamea)
            {
                Touch t = Input.GetTouch(0);
                ClickEffect.transform.position = MainCamea.ScreenToWorldPoint(t.position);
                ClickEffect.ResetToBeginning();
                ClickEffect.gameObject.SetActive(true);
                ClickEffect.Play();
            }
        }

        //if (Input.GetMouseButtonDown(0))
        //{
        //    if (null != MainCamea)
        //    {
        //        ClickEffect.transform.position = MainCamea.ScreenToWorldPoint(Input.mousePosition);
        //        ClickEffect.ResetToBeginning();
        //        ClickEffect.gameObject.SetActive(true);
        //        ClickEffect.Play();
        //    }
        //}

        if (!ClickEffect.isPlaying)
        {
            ClickEffect.gameObject.SetActive(false);
        }
    }

    void DateChange()
    {
        //Debug.Log("DateChange");
        CancelInvoke("DateChange");
        UserDataManager.Get.ResetAdsCount();
        System.DateTime nextdaymidnight = new System.DateTime(System.DateTime.Now.AddDays(1).Year, System.DateTime.Now.AddDays(1).Month, System.DateTime.Now.AddDays(1).Day);
        Invoke("DateChange", (float)(nextdaymidnight - System.DateTime.Now).TotalSeconds);
    }

    public void ChangeScene(SceneType type)
    {
        moveto = type;
        Fade.onFinished.Clear();
        Fade.onFinished.Add(new EventDelegate(FadeFinsh));
        FadeCam.gameObject.SetActive(true);
        PopupManager.Get.AllClosePopup();
        PopupManager.Get.ObjDestroy();
        FadeIn();
        SceneMoveEnd = false;
    }

    void FadeFinsh()
    {
        SceneManager.LoadScene((int)moveto);
    }

    void OnSceneLoad(Scene scene, LoadSceneMode mode)
    {
        //Debug.Log("OnSceneLoaded: " + scene.name);
        //Debug.Log(mode);
        FadeOut();
        //PopupManager.Get.SetPopupParent(GameObject.Find("Popups").transform);
    }

    void OnSceneUnLoad(Scene scene)
    {
        //Debug.Log("OnSceneLoaded: " + scene.name);
        now = moveto;
    }

    void FadeIn()
    {
        Fade.PlayForward();
    }

    void FadeOut()
    {
        Fade.onFinished.RemoveAt(0);
        Fade.onFinished.Add(new EventDelegate(Fade_PlayReverseEnd));
        Fade.PlayReverse();
        Resources.UnloadUnusedAssets();
        GameObject cam = GameObject.Find(now.ToString() + "Camera");
        if(null != cam)
            MainCamea = cam.GetComponent<Camera>();
    }

    void Fade_PlayReverseEnd()
    {
        FadeCam.gameObject.SetActive(false);
        if (now == SceneType.Event)
            EventSceneManager.Get.EventStart();

        if (now == SceneType.Main)
            SubWorkManager.Get.Is_Update = true;

        SceneMoveEnd = true;
    }

    public SceneType GetNowScene()
    {
        return now;
    }

    void OpenExitPopup()
    {
        Message message = new Message(ExitGame, null, "게임을 종료하시겠습니까?", "확인", "취소", MessagePopupType.Ok_Cancel);
        PopupManager.Get.OpenPopup("Popup_Message", message);
    }

    void ExitGame()
    {
        Application.Quit();
    }

    void ExitCancel()
    {
    }

    void PauseCancel()
    {
    }

    void RegisterNotification()
    {
        if (1 == UserDataManager.Get.Push)
        {
            if (WorkManager.Get.WorkingData != null)
                Assets.SimpleAndroidNotifications.NotificationManager.SendWithAppIcon(WorkManager.Get.WorkingData.EndTime - System.DateTime.Now, "카렌의 꽃가게", "작업이 완료 되었습니다.", Color.white);

            if (MiniGameManager.Get.Card_CoolTime > System.DateTime.Now)
                Assets.SimpleAndroidNotifications.NotificationManager.SendWithAppIcon(MiniGameManager.Get.Card_CoolTime - System.DateTime.Now, "카렌의 꽃가게", "짝맞추기 대기시간 완료.", Color.white);

            if (MiniGameManager.Get.WhereHouse_CoolTime > System.DateTime.Now)
                Assets.SimpleAndroidNotifications.NotificationManager.SendWithAppIcon(MiniGameManager.Get.WhereHouse_CoolTime - System.DateTime.Now, "카렌의 꽃가게", "창고정리 대기시간 완료.", Color.white);
        }
    }

    private void OnApplicationPause(bool pause)
    {
        if(pause)
        {
            //Debug.Log("pause");
            PlayerPrefs.SetString("LastExitTime", System.DateTime.Now.Ticks.ToString());
            PlayerPrefs.SetFloat("Sound_bgm", SoundManager.Get.Valume_Bgm);
            PlayerPrefs.SetFloat("Sound_Effect", SoundManager.Get.Valume_Effect);
            RegisterNotification();
        }
        else
        {
            if(Ads_Manager.Get.AdMobReward)
            {
                Ads_Manager.Get.PayAdReward();
                Ads_Manager.Get.AdMobReward = false;
            }
            else
            {
                Assets.SimpleAndroidNotifications.NotificationManager.CancelAll();
            }
        }
    }

    private void OnApplicationQuit()
    {
        //Debug.Log("exit game")
        Assets.SimpleAndroidNotifications.NotificationManager.CancelAll();
        RegisterNotification();
        PlayerPrefs.SetString("LastExitTime", System.DateTime.Now.Ticks.ToString());
        PlayerPrefs.SetFloat("Sound_bgm", SoundManager.Get.Valume_Bgm);
        PlayerPrefs.SetFloat("Sound_Effect", SoundManager.Get.Valume_Effect);
    }
}
