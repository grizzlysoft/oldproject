﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Enum;

public class InventoryManager : Singleton<InventoryManager>
{
    List<InventoryItem> ItemList = new List<InventoryItem>();

    public List<InventoryItem> InventoryItem
    {
        get
        {
            return ItemList;
        }
    }

    public void AddItem(int id_, int count_)
    {
        //Debug.Log("add item id = " + id_);
        //Debug.Log("add item cout = " + count_);
        int rest = count_;
        ItemData itemdata = DataManager.Get.GetItemData(id_);
        for (int i = 0; i < ItemList.Count; ++i)
        {
            if (id_ == ItemList[i].ID && ItemList[i].Count + rest <= itemdata.Max)
            {
                ItemList[i].Count += rest;
                PlayerPrefs.SetInt("InvenItemId" + i, ItemList[i].ID);
                PlayerPrefs.SetInt("InvenItemCount" + i, ItemList[i].Count);
                rest = 0;
            }
            else if (id_ == ItemList[i].ID && ItemList[i].Count < itemdata.Max && ItemList[i].Count + rest > itemdata.Max)
            {
                rest = rest + ItemList[i].Count - DataManager.Get.GetItemData(ItemList[i].ID).Max;
                ItemList[i].Count = DataManager.Get.GetItemData(ItemList[i].ID).Max;
                PlayerPrefs.SetInt("InvenItemId" + i, ItemList[i].ID);
                PlayerPrefs.SetInt("InvenItemCount" + i, ItemList[i].Count);
            }
        }

        while(rest > 0)
        {
            if (rest >= itemdata.Max)
            {
                InventoryItem newitem = new InventoryItem(id_, itemdata.Max);
                ItemList.Add(newitem);
                PlayerPrefs.SetInt("InvenItemId" + (InventoryItem.Count -1), id_);
                PlayerPrefs.SetInt("InvenItemCount" + (InventoryItem.Count -1), newitem.Count);
                rest = rest - itemdata.Max;
            }
            else
            {
                InventoryItem newitem = new InventoryItem(id_, rest);
                ItemList.Add(newitem);
                PlayerPrefs.SetInt("InvenItemId" + (InventoryItem.Count - 1), id_);
                PlayerPrefs.SetInt("InvenItemCount" + (InventoryItem.Count - 1), newitem.Count);
                rest = 0;
            }
        }

        SortInven();

        if (Main.Get.GetNowScene() == SceneType.Main)
            MainUIManager.Get.GetWindow(WindowType.Box).GetComponent<BoxWindow>().AllScrollPostionReset();
    }

    public bool AddCheck(List<int> id_, List<int> count_)
    {
        if (ItemList.Count >= UserDataManager.Get.InvenMax)
            return false;

        int addslot = 0;
        for(int i = 0; i < id_.Count; ++i)
        {
            ItemData item = DataManager.Get.GetItemData(id_[i]); //max치를 알기위해 itemdata 가져옴

            //현재 아이탬에서 슬롯에 들어갈 자리 총 갯수에서 맥스를 나눠서 나머지가 있으면 그 슬롯에 들어가야됨
            int remainder = GetItemCount(id_[i]) % item.Max;
            
            if(remainder == 0) //나머지가 0이면 count로 계산하면 바로 슬롯갯수나옴
            {
                int need_remainder = count_[i] % item.Max;
                int need_slot = count_[i] / item.Max;

                if (need_remainder > 0) need_slot++;

                addslot += need_slot;
            }
            else //나머지가 있으면 모자란 슬롯을 채우고 슬롯을 구해야지
            {
                int lastslotcount = GetLastSlotCount(id_[i]); //아이템의 마지막 슬롯 아이템 겟수를 구해온다
                if(lastslotcount + count_[i] <= item.Max) //마지막 슬롯하고 만들 아이탬 갯수하고 더해서 맥스보다 낮으면 슬롯 필요없다
                {

                }
                else
                {
                    int count = count_[i]; //만들 겟수
                    count -= item.Max - lastslotcount; //마지막 슬롯에 필요한 갯수를 뺀다

                    int need_remainder = count % item.Max;
                    int need_slot = count / item.Max;

                    if (need_remainder > 0) need_slot++;

                    addslot += need_slot;
                }
            }
        }
        
        if (ItemList.Count + addslot <= UserDataManager.Get.InvenMax)
            return true;
        else
            return false;
    }

    int GetLastSlotCount(int id_)
    {
        for(int i = 0; i < ItemList.Count; ++i)
        {
            if(id_ == ItemList[i].ID && ItemList[i].Count < DataManager.Get.GetItemData(ItemList[i].ID).Max)
            {
                return ItemList[i].Count;
            }
        }

        return -1;
    }


    public bool CheckUseItem(int id_, int count_)
    {
        int total = 0;

        for(int i = 0; i < ItemList.Count; ++i)
        {
            if (id_ == ItemList[i].ID)
                total += ItemList[i].Count;
        }

        if (total < count_)
        {
            //개수가 적어서 사용할수없음
            //Debug.Log("아이템 모자름");
            return false;
        }
        else
            return true;
    }

    public bool SellItem(int id_, int count_)
    {
        return false;
    }

    public bool UseItem(int id_, int count_)
    {
        List<int> indexlist = new List<int>();
        List<int> removeindex = new List<int>();

        int totalcount = 0;

        for (int i = 0; i < ItemList.Count; ++i)
        {
            if (ItemList[i].ID == id_)
            {
                indexlist.Add(i);
                totalcount += ItemList[i].Count;
            }
        }

        if (totalcount < count_)
            return false;

        int temp = count_;
        for(int i = indexlist.Count -1; i >= 0; --i)
        {
            if(temp >= ItemList[indexlist[i]].Count )
            {
                removeindex.Add(indexlist[i]);
                temp -= ItemList[indexlist[i]].Count;
            }
            else
            {
                ItemList[indexlist[i]].Count -= temp;
                temp = 0;
                break;
            }
        }

        for(int i = 0; i < removeindex.Count; ++i)
        {
            ItemList.RemoveAt(removeindex[i]);
        }

        SortInven();

        ResetSaveItem();

        if(Main.Get.GetNowScene() == SceneType.Main)
            MainUIManager.Get.GetWindow(Enum.WindowType.Main).GetComponent<MainWindow>().QuestTap.QuestIconRefresh();

        return true;
    }

    //아이템 소모하면 해준다
    public void ResetSaveItem()
    {
        int max = UserDataManager.Get.InvenMax;
        for (int i = 0; i < max; ++i)
        {
            if (PlayerPrefs.HasKey("InvenItemId" + i))
            {
                PlayerPrefs.DeleteKey("InvenItemId" + i);
                PlayerPrefs.DeleteKey("InvenItemCount" + i);

                //Debug.Log("DeletePrefab InvenItemId" + i);
            }
        }

        for(int i = 0; i < ItemList.Count; ++i)
        {
            PlayerPrefs.SetInt("InvenItemId" + i, ItemList[i].ID);
            //Debug.Log("save invenitemid" + i + " = " + ItemList[i].ID);
            PlayerPrefs.SetInt("InvenItemCount" + i, ItemList[i].Count);
            //Debug.Log("save invenitemcount" + i + " = " + ItemList[i].Count);
        }
    }

    public List<InventoryItem> GetTypeList(BoxTab type_)
    {
        List<InventoryItem> list = new List<InventoryItem>();

        for(int i = 0; i < ItemList.Count; ++i)
        {
            if((int)type_ == DataManager.Get.GetItemData(ItemList[i].ID).Type)
            {
                list.Add(ItemList[i]);
            }
        }

        return list;
    }

    public int GetItemCount(int id_)
    {
        int total = 0;

        for(int i = 0; i < ItemList.Count; ++i)
        {
            if (id_ == ItemList[i].ID)
                total += ItemList[i].Count;
        }

        return total;
    }

    public int GetItemSlotCount(int id_)
    {
        int slot = 0;

        for (int i = 0; i < ItemList.Count; ++i)
        {
            if (id_ == ItemList[i].ID)
                slot++;
        }

        return slot;
    }

    public bool CheckRequestItem(List<int> requestidlist_)
    {
        for(int i = 0; i < requestidlist_.Count; ++i)
        {
            ReQuestData r = DataManager.Get.GetReqeustData(requestidlist_[i]);
            if (!CheckUseItem(r.ItemID, r.Count))
                return false;
        }

        return true;
    }

    public void InvenClear()
    {
        ItemList.Clear();
    }

    public void SortInven()
    {
        ItemList.Sort(delegate(InventoryItem a, InventoryItem b) 
        {
            if (a.ID > b.ID)
                return 1;
            else if (a.ID < b.ID)
                return -1;
            else
            {
                if (a.Count > b.Count)
                    return -1;
                else if (a.Count < b.Count)
                    return 1;
                else return 0;
            }
        });
    }
}
