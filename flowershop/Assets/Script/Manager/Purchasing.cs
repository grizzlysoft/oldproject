﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class Purchasing : Singleton<Purchasing>, IStoreListener
{

    private static IStoreController m_StoreController;             // Reference to the Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider;    // Reference to store-specific Purchasing subsystems.

    // 구입가능한 모든 상품의 식별자: 결제를 편하게 하기위한 식별자 그리고 그들의 store-specific 식별자 대응? 
    // 유니티의 외부 결제를 위한(?) 정의 store-specific identifiers also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

    private static string kProductIDConsumable = "consumable";      // General handle for the consumable product.
    private static string kProductIDNonConsumable = "nonconsumable";   // General handle for the non-consumable product.
    private static string kProductIDSubscription = "subscription";    // General handle for the subscription product.

    // 구글 스토어 식별자 코드
    //private static string kProductNameGooglePlayConsumable = "com.unity3d.test.services.purchasing.consumable";        // Google Play Store identifier for the consumable product.
    //private static string kProductNameGooglePlayNonConsumable = "com.unity3d.test.services.purchasing.nonconsumable";     // Google Play Store identifier for the non-consumable product.
    //private static string kProductNameGooglePlaySubscription = "com.unity3d.test.services.purchasing.subscription";  // Google Play Store identifier for the subscription product.

    public void InitPurchasing()
    {
        // If we haven't set up the Unity Purchasing reference
        if (m_StoreController == null)
        {
            // Begin to configure our connection to Purchasing
            InitializePurchasing();
        }
    }

    public void InitializePurchasing()
    {
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            // ... we are done here.
            return;
        }

        // Create a builder, first passing in a suite of Unity provided stores.
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        // Add a product to sell / restore by way of its identifier, associating the general identifier with its store-specific identifiers.
        //builder.AddProduct(kProductIDConsumable, ProductType.Consumable, new IDs() { { string.Empty, AppleAppStore.Name }, { kProductNameGooglePlayConsumable, GooglePlay.Name }, });// Continue adding the non-consumable product.
        //builder.AddProduct(kProductIDNonConsumable, ProductType.NonConsumable, new IDs() { { string.Empty, AppleAppStore.Name }, { kProductNameGooglePlayNonConsumable, GooglePlay.Name }, });// And finish adding the subscription product.
        //builder.AddProduct(kProductIDSubscription, ProductType.Subscription, new IDs() { { string.Empty, AppleAppStore.Name }, { kProductNameGooglePlaySubscription, GooglePlay.Name }, });// Kick off the remainder of the set-up with an asynchrounous call, passing the configuration and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.

        List<ShopData> DataList = DataManager.Get.GetShopDataList(Enum.Shop_PayType.Cash);

        for (int i = 0; i < DataList.Count; ++i)
        {
            string id = DataList[i].StoreID;

            if (id == "start_package" || id == "basic_package" || id == "expert_package")
                builder.AddProduct(id, ProductType.Consumable);
            else if (id != "empty")
                builder.AddProduct(id, ProductType.Consumable);
        }

        UnityPurchasing.Initialize(this, builder);
    }


    private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    // 소모품 아이템 구매
    public void Buy(string id_)
    {
        Debug.Log("store id = " + id_);
        // Buy the consumable product using its general identifier. Expect a response either through ProcessPurchase or OnPurchaseFailed asynchronously.
        BuyProductID(id_);
    }


    // 비 소모품 아이템 구매
    //public void BuyNonConsumable()
    //{
    //    // Buy the non-consumable product using its general identifier. Expect a response either through ProcessPurchase or OnPurchaseFailed asynchronously.
    //    BuyProductID(kProductIDNonConsumable);
    //}


    //// 이놈은 무슨 아이템인지 모르겠다.. 나중에 구글신님께 물어봐야지.
    //public void BuySubscription()
    //{
    //    // Buy the subscription product using its the general identifier. Expect a response either through ProcessPurchase or OnPurchaseFailed asynchronously.
    //    BuyProductID(kProductIDSubscription);
    //}


    // 실제 구매가 실행되는 함수. 매개변수로 상품의 프로젝트ID 를 받는다.
    void BuyProductID(string productId)
    {
        // If the stores throw an unexpected exception, use try..catch to protect my logic here.
        try
        {
            // If Purchasing has been initialized ...
            if (IsInitialized())
            {
                // ... look up the Product reference with the general product identifier and the Purchasing system's products collection.
                Product product = m_StoreController.products.WithID(productId);

                // If the look up found a product for this device's store and that product is ready to be sold ... 
                if (product != null && product.availableToPurchase)
                {
                    PopupManager.Get.ClosePopup("Popup_Message");
                    Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));// ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed asynchronously.
                    m_StoreController.InitiatePurchase(product);
                }
                // Otherwise ...
                else
                {
                    // ... report the product look-up failure situation  
                    Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                }
            }
            // Otherwise ...
            else
            {
                // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or retrying initiailization.
                Debug.Log("BuyProductID FAIL. Not initialized.");
            }
        }
        // Complete the unexpected exception handling ...
        catch (System.Exception e)
        {
            // ... by reporting any unexpected exception for later diagnosis.
            Debug.Log("BuyProductID: FAIL. Exception during purchase. " + e);
        }
    }


    // 애플로 출시할때는 이 코드를 추가해야하나봄??? 
    // Restore purchases previously made by this customer. Some platforms automatically restore purchases. Apple currently requires explicit purchase restoration for IAP.
    public void RestorePurchases()
    {
        // If Purchasing has not yet been set up ...
        if (!IsInitialized())
        {
            // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        // If we are running on an Apple device ... 
        //if (Application.platform == RuntimePlatform.IPhonePlayer ||
        //    Application.platform == RuntimePlatform.OSXPlayer)
        //{
        //    // ... begin restoring purchases
        //    Debug.Log("RestorePurchases started ...");

        //    // Fetch the Apple store-specific subsystem.
        //    var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
        //    // Begin the asynchronous process of restoring purchases. Expect a confirmation response in the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
        //    apple.RestoreTransactions((result) => {
        //        // The first phase of restoration. If no more responses are received on ProcessPurchase then no purchases are available to be restored.
        //        Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
        //    });
        //}
        //// Otherwise ...
        //else
        //{
        //    // We are not running on an Apple device. No work is necessary to restore purchases.
        //    Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        //}
    }


    //  
    // --- IStoreListener
    //

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        // Purchasing has succeeded initializing. Collect our Purchasing references.
        //Debug.Log("OnInitialized: PASS");

        // Overall Purchasing system, configured with products for this application.
        m_StoreController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        m_StoreExtensionProvider = extensions;
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }


    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        if(!args.purchasedProduct.availableToPurchase)
        {
            Message m = new Message(null, null, "애러", "확인", string.Empty, Enum.MessagePopupType.Ok);
            PopupManager.Get.OpenPopup("Popup_Message", m);
            return PurchaseProcessingResult.Complete;
        }

        if (!args.purchasedProduct.definition.id.Contains("package"))
        {
            //Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));//If the consumable item has been successfully purchased, add 100 coins to the player's in-game score.
            PaymentManager.Get.PayPrice(args.purchasedProduct.definition.id);
        }
        else if (args.purchasedProduct.definition.id.Contains("package"))
        {
            //Debug.Log("price store id = " + args.purchasedProduct.definition.id);
            if (!PlayerPrefs.HasKey(args.purchasedProduct.definition.id))
            {
                PlayerPrefs.SetInt(args.purchasedProduct.definition.id, 1);
                PaymentManager.Get.PayPrice(args.purchasedProduct.definition.id);
                //모든 패키지 구매 업적 채크
                if(PlayerPrefs.HasKey("start_package") && PlayerPrefs.HasKey("basic_package") && PlayerPrefs.HasKey("expert_package"))
                {
                    AchievementManager.Get.AchievementStepUp(Enum.AchievementType.AllPackageBuy, 1);
                    if (Main.Get.GetNowScene() == Enum.SceneType.Main)
                        MainUIManager.Get.GetWindow(Enum.WindowType.Main).GetComponent<MainWindow>().PackageBtn.SetActive(false);
                }
            }
        }// Or ... a subscription product has been purchased by this user.
        else if (String.Equals(args.purchasedProduct.definition.id, kProductIDSubscription, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        }// Or ... an unknown product has been purchased by this user. Fill in additional products here.
        else
        {
            Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
        }


        return PurchaseProcessingResult.Complete;
    }


    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing this reason with the user.
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }
}


