﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class AchievementManager : Singleton<AchievementManager>
{
    Dictionary<int, int> AchievementClear = new Dictionary<int, int>(); //key = id
    public Dictionary<int, int> AchievementClearDic
    {
        get { return AchievementClear; }
    }
    Dictionary<AchievementType, int> AchievementStep = new Dictionary<AchievementType, int>(); //key = type value = count
    public Dictionary<AchievementType, int> AchievementStepDic
    {
        get { return AchievementStep; }
    }
    Dictionary<AchievementType, int> NowAchievement = new Dictionary<AchievementType, int>(); //key = type value = id
    public Dictionary<AchievementType, int> NowAchievementDic
    {
        get { return NowAchievement; }
    }

    public void AchievementInit()
    {
        List<AchievementData> list = DataManager.Get.GetAchievementDataList();

        for(int i = 0; i < list.Count; ++i)
        {
            AchievementClear.Add(list[i].ID, 0);
        }

        for(int i = 0; i < (int)AchievementType.Max; ++i)
        {
            AchievementStep.Add((AchievementType)i, 0);
            NowAchievement.Add((AchievementType)i, -1);
        }

        LoadPlayPrefab();
    }

    public void LoadPlayPrefab()
    {
        for(int i = 0; i < (int)AchievementType.Max; ++i)
        {
            if(PlayerPrefs.HasKey((AchievementType)i + "step"))
            {
                AchievementStep[(AchievementType)i] = PlayerPrefs.GetInt((AchievementType)i + "step");
            }

            if (PlayerPrefs.HasKey((AchievementType)i + "now"))
            {
                NowAchievement[(AchievementType)i] = PlayerPrefs.GetInt((AchievementType)i + "now");
                Debug.Log("now = " + (AchievementType)i + " : " + PlayerPrefs.GetInt((AchievementType)i + "now"));
            }
        }

        List<AchievementData> list = DataManager.Get.GetAchievementDataList();
        for (int i = 0; i < list.Count; ++i)
        {
            if (PlayerPrefs.HasKey("Achievement" + list[i].ID))
                AchievementClear[list[i].ID] = PlayerPrefs.GetInt("Achievement" + list[i].ID);
        }

        for(int i = 0; i < (int)AchievementType.Max; ++i)
        {
            if (NowAchievement[(AchievementType)i] == -1)
                SetFirstNowAchievement((AchievementType)i);
        }
    }

    public void SetFirstNowAchievement(AchievementType type_)
    {
        List<AchievementData> list = DataManager.Get.GetAchievementDataList(type_);

        if (list.Count == 1)
            NowAchievement[type_] = list[0].ID;
        else
        {
            for (int i = 0; i < list.Count; ++i)
            {
                if (list[i].First == 1)
                    NowAchievement[type_] = list[i].ID;
            }
        }
    }

    public void AchievementStepUp(AchievementType type_, int value_)
    {
        AchievementData d = DataManager.Get.GetAchievementData(NowAchievement[type_]);

        if(AchievementClear[d.ID] == 1 || (d.ClearValue <= AchievementStep[type_] && d.Next == -2))
        {
            return;
        }

        AchievementStep[type_] += value_;

        if (AchievementStep[type_] >= 2100000000 - 1)
            AchievementStep[type_] = 2100000000;

        PlayerPrefs.SetInt(type_ + "step", AchievementStep[type_]);
    }

    public bool AchievementClearAndNext(int id_)
    {
        AchievementData data = DataManager.Get.GetAchievementData(id_);

        if (AchievementClear[id_] == 1)
            return false;

        if(AchievementStep[data.Type] >= data.ClearValue)
        {
            AchievementClear[data.ID] = 1;
            PlayerPrefs.SetInt("Achievement" + data.ID, 1);
            Debug.Log("다음 업적 id = " + data.Next);
            if (data.Next != -2)
                NowAchievement[data.Type] = data.Next;

            PlayerPrefs.SetInt(data.Type + "now", NowAchievement[data.Type]);
            if (data.GpgsID != "empty")
                GPGS_Manager.Get.UnlockAchievement(data.GpgsID);

            PaymentManager.Get.PayPrice(data.RewardID);
            RewardData r = DataManager.Get.GetRewardData(data.RewardID);

            List<IconTextData> list = new List<IconTextData>();
            list.Add(r.ConvertIconText());
            GoodsMessage message = new GoodsMessage("보상을 획득 하셨습니다", list, "확인", string.Empty, null, null, Enum.MessagePopupType.Ok);

            PopupManager.Get.OpenPopup("Popup_GoodsMessage", message);

            return true;
        }
        else
        {
            return false;
        }
    }

    public void AllComplete()
    {
        List<IconTextData> list = new List<IconTextData>();

        for (int i = 0; i < (int)AchievementType.Max; ++i)
        {
            AchievementData data = DataManager.Get.GetAchievementData(NowAchievement[(AchievementType)i]);
            if (data.ClearValue <= AchievementStep[(AchievementType)i] && AchievementClear[data.ID] == 0)
            {
                AchievementClear[data.ID] = 1;
                PlayerPrefs.SetInt("Achievement" + data.ID, 1);
                //Debug.Log("다음 업적 id = " + data.Next);
                if (data.Next != -2)
                    NowAchievement[data.Type] = data.Next;

                PlayerPrefs.SetInt(data.Type + "now", NowAchievement[data.Type]);
                if (data.GpgsID != "empty")
                    GPGS_Manager.Get.UnlockAchievement(data.GpgsID);

                PaymentManager.Get.PayPrice(data.RewardID);
                RewardData r = DataManager.Get.GetRewardData(data.RewardID);
                list.Add(r.ConvertIconText());
            }
        }

        if (list.Count > 0)
        {
            GoodsMessage message = new GoodsMessage("보상을 획득 하셨습니다", list, "확인", string.Empty, null, null, Enum.MessagePopupType.Ok);

            PopupManager.Get.OpenPopup("Popup_GoodsMessage", message);
        }
        else
        {
            Message message = new Message(null, null, "완료된 업적이 없습니다.", "확인", string.Empty);
            PopupManager.Get.OpenPopup("Popup_Message", message);
        }
    }

    public int GetAchievementStep(AchievementType type_)
    {
        return AchievementStep[type_];
    }

    public int GetNowAchievement(AchievementType type_)
    {
        Debug.Log("now achievement " + type_ + " id = " + NowAchievement[type_]);
        return NowAchievement[type_];
    }

    public bool CheckClearAchievement(int id_)
    {
        if (1 == AchievementClear[id_])
            return true;
        else
            return false;
    }
}
