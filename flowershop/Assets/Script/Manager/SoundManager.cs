﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;


public class SoundManager : Singleton<SoundManager>
{
    List<AudioClip> SoundList_Bgm = new List<AudioClip>();
    List<AudioSource> SoundList_Sfx = new List<AudioSource>();
    public AudioSource Bgm_Obj;
    public Transform Sfx_Obj;

    float BgmVolume = 1.0f;
    public float Valume_Bgm
    {
        get
        {
            return BgmVolume;
        }

        set
        {
            BgmVolume = value;
            Bgm_Obj.volume = value;
        }
    }
    float EffectVolume = 1.0f;
    public float Valume_Effect
    {
        get
        {
            return EffectVolume;
        }

        set
        {
            EffectVolume = value;
        }
    }

    // Use this for initialization
    void Start ()
    {
        if (PlayerPrefs.HasKey("Sound_bgm"))
            BgmVolume = PlayerPrefs.GetFloat("Sound_bgm");
        if (PlayerPrefs.HasKey("Sound_Effect"))
            EffectVolume = PlayerPrefs.GetFloat("Sound_Effect");

        //Debug.Log("bgmvolume = " + BgmVolume);
        //Debug.Log("sfx volume = " + EffectVolume);
        LoadBgm();
        SfxSetting();
    }

    public void SetChanageBgmVolume()
    {
        Bgm_Obj.volume = BgmVolume;
    }

    void LoadBgm()
    {
        for(int i = 0; i < (int)Bgm.Max; ++i)
        {
            SoundList_Bgm.Add(Resources.Load("Sound/" + (Bgm)i) as AudioClip);
        }
    }

    public void SfxSetting()
    {
        for(int i = 0; i < (int)Sfx.Max; ++i)
        {
            GameObject o = GameObject.Instantiate(Resources.Load("Prefab/Sfx_Sound") as GameObject);
            o.transform.parent = Sfx_Obj;
            o.transform.localPosition = Vector3.zero;
            o.GetComponent<AudioSource>().clip = Resources.Load("Sound/" + (Sfx)i) as AudioClip;
            SoundList_Sfx.Add(o.GetComponent<AudioSource>());
        }
    }

    public void PlayBgm(Bgm bgm_)
    {
        Bgm_Obj.clip = SoundList_Bgm[(int)bgm_];
        Bgm_Obj.loop = true;
        Bgm_Obj.volume = BgmVolume;
        Bgm_Obj.Play();
    }

    public void StopBgm()
    {
        Bgm_Obj.Stop();
    }
    
    public void PlaySfx(Sfx sfx_)
    {
        if (!SoundList_Sfx[(int)sfx_].isPlaying)
        {
            SoundList_Sfx[(int)sfx_].PlayOneShot(SoundList_Sfx[(int)sfx_].clip, EffectVolume);
        }
    }

    public void SoundOff()
    {
        
    }

    public void SoundOn()
    {
        
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
