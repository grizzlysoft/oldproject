﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerCheckManager : Singleton<VerCheckManager>
{
    string url = "https://play.google.com/store/apps/details?id=com.GrizzlySoft.FlowerShop";
    //public string Ver;        //버전을 표시할 텍스트

    //유니티 자체에서 bundleIdentifier를 읽을수도 있지만, 이렇게 읽을 수 도 있다.
    public string _bundleIdentifier { get { return url.Substring(url.IndexOf("details"), url.LastIndexOf("details") + 1); } }


    [HideInInspector]
    public bool isSamePlayStoreVersion = false;

    bool isTestMode = false;        //테스트 모드 여부


    private void Start()
    {
        if (isTestMode == false)
            StartCoroutine(PlayStoreVersionCheck());
        else
            isSamePlayStoreVersion = true;
    }

    /// <summary>
    /// 버전체크를 하여, 강제업데이트를 체크한다.
    /// </summary>
    /// <returns></returns>
    private IEnumerator PlayStoreVersionCheck()
    {
        WWW www = new WWW(url);
        yield return www;

        //인터넷 연결 에러가 없다면, 
        if (www.error == null)
        {
            int index = www.text.IndexOf("softwareVersion");
            //Debug.Log(www.text);
            string versionText = www.text.Substring(index, 30);

            //플레이스토어에 올라간 APK의 버전을 가져온다.
            int softwareVersion = versionText.IndexOf(">");
            string playStoreVersion = versionText.Substring(softwareVersion + 1, Application.version.Length + 1);
            //Debug.Log("play store version = " + playStoreVersion + "lenght = " + playStoreVersion.Length);
            //Debug.Log("build ver = " + Application.version + "lenght = " + Application.version.Length);
           
            if(float.Parse(playStoreVersion) > float.Parse(Application.version))
            {
                Debug.Log("업데이트 팝업");
                Message m = new Message(GoPlayStore, ExitGame, "업데이트가 있습니다\n스토어로 이동하시겠습니까?", "스토어", "종료", Enum.MessagePopupType.Ok_Cancel);
                PopupManager.Get.OpenPopup("Popup_Message", m);
            }
        }
        else
        {
            //인터넷 연결 에러시
            Debug.LogWarning(www.error);
        }
    }
    
    void GoPlayStore()
    {
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.GrizzlySoft.FlowerShop");
    }

    void ExitGame()
    {
        Application.Quit();
    }
}
