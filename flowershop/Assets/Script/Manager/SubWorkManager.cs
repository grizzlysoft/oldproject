﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SubWorkManager : Singleton<SubWorkManager>
{
    Dictionary<int, SubWorkUserData> SubWorkUserDataList = new Dictionary<int, SubWorkUserData>();
    bool IsUpdate = false;

    public bool Is_Update
    {
        set { IsUpdate = value; }
    }

    private void Start()
    {

    }

    public Dictionary<int, SubWorkUserData> GetSubWorkUserDataDic()
    {
        return SubWorkUserDataList;
    }

    public void SetLoadSubWork(int id_, int level_, System.DateTime endtime_)
    {
        if (SubWorkUserDataList.ContainsKey(id_))
        {
            if (endtime_ <= System.DateTime.Now)
            {
                SubWorkUserDataList[id_].Deltatime = 0;
                SubWorkData sub = DataManager.Get.GetSubWorkData(id_);
                SubWorkUserDataList[id_].Level = level_;
                int result = sub.Lv1Result + (sub.LvupResut * (SubWorkUserDataList[id_].Level - 1));
                result += (int)(result * DataManager.Get.GetAttributeData(Enum.AttributeType.Insentive, UserDataManager.Get.GetAttributeLevel(Enum.AttributeType.Insentive)).Abillity);
                UserDataManager.Get.AddGold(result);
                //Debug.Log("subwork id = " + id_ + " result = " + result);
            }
            else
            {
                System.DateTime start = endtime_.AddSeconds(-(DataManager.Get.GetSubWorkData(id_).WorkTime - DataManager.Get.GetAttributeData(Enum.AttributeType.SubWork, UserDataManager.Get.GetAttributeLevel(Enum.AttributeType.SubWork)).Abillity));
                System.TimeSpan deltatime = System.DateTime.Now - start;
                SubWorkUserDataList[id_].Level = level_;
                SubWorkUserDataList[id_].Deltatime = deltatime.Seconds;
            }
        }
        else
        {
            if (endtime_ <= System.DateTime.Now)
            {
                SubWorkUserData data = new SubWorkUserData(level_, 0);
                SubWorkUserDataList.Add(id_, data);
                SubWorkData sub = DataManager.Get.GetSubWorkData(id_);
                int result = sub.Lv1Result + (sub.LvupResut * (SubWorkUserDataList[id_].Level - 1));
                result += (int)(result * DataManager.Get.GetAttributeData(Enum.AttributeType.Insentive, UserDataManager.Get.GetAttributeLevel(Enum.AttributeType.Insentive)).Abillity);
                UserDataManager.Get.AddGold(result);
                //Debug.Log("subwork id = " + id_ + " result = " + result);
            }
            else
            {
                System.DateTime start = endtime_.AddSeconds(-(DataManager.Get.GetSubWorkData(id_).WorkTime - DataManager.Get.GetAttributeData(Enum.AttributeType.SubWork, UserDataManager.Get.GetAttributeLevel(Enum.AttributeType.SubWork)).Abillity));
                System.TimeSpan deltatime = System.DateTime.Now - start;

                SubWorkUserData data = new SubWorkUserData(level_, deltatime.Seconds);
                SubWorkUserDataList.Add(id_, data);
            }
        }
    }

    public void SetSubWorkUserData(int id_, float deltatime_, int level_)
    {
        if (SubWorkUserDataList.ContainsKey(id_))
            SubWorkUserDataList[id_].Deltatime = deltatime_;
        else
        {
            SubWorkUserData data = new SubWorkUserData(level_, deltatime_);
            SubWorkUserDataList.Add(id_, data);
        }
    }

    public SubWorkUserData GetSsubWorkUserData(int id_)
    {
        if (SubWorkUserDataList.ContainsKey(id_))
            return SubWorkUserDataList[id_];
        else
            return null;
    }

    public int GetSubWorkLevel(int id_)
    {
        if (SubWorkUserDataList.ContainsKey(id_))
            return SubWorkUserDataList[id_].Level;
        else
            return 0;
    }

    public float GetDeltatime(int id_)
    {
        if (SubWorkUserDataList.ContainsKey(id_))
            return SubWorkUserDataList[id_].Deltatime;
        else
            return 0;
    }

    public void LevelupSubWork(int id_)
    {
        if (SubWorkUserDataList.ContainsKey(id_) == false)
        {
            SubWorkUserDataList.Add(id_, new SubWorkUserData(1, 0));
            PlayerPrefs.SetInt("SubWorkLv" + DataManager.Get.GetSubWorkData(id_).Type, 1);
            PlayerPrefs.SetString("SubWorkEndTime" + DataManager.Get.GetSubWorkData(id_).Type,
                (DateTime.Now.AddSeconds(DataManager.Get.GetSubWorkData(id_).WorkTime - DataManager.Get.GetAttributeData(Enum.AttributeType.SubWork, UserDataManager.Get.GetAttributeLevel(Enum.AttributeType.SubWork)).Abillity)).ToString());
        }
        else
        {
            SubWorkUserDataList[id_].Level++;
            PlayerPrefs.SetInt("SubWorkLv" + DataManager.Get.GetSubWorkData(id_).Type, SubWorkUserDataList[id_].Level);

            DateTime EndTime = DateTime.Now.AddSeconds(DataManager.Get.GetSubWorkData(id_).WorkTime - DataManager.Get.GetAttributeData(Enum.AttributeType.SubWork, UserDataManager.Get.GetAttributeLevel(Enum.AttributeType.SubWork)).Abillity).AddSeconds(-(SubWorkUserDataList[id_].Deltatime));
            PlayerPrefs.SetString("SubWorkEndTime" + DataManager.Get.GetSubWorkData(id_).Type, EndTime.ToString());
        }

        bool allunlock = false;
        for(int i = 0; i < DataManager.Get.GetSubWorkList().Count; ++i)
        {
            if (GetSubWorkLevel(DataManager.Get.GetSubWorkList()[i].Type) > 0)
                allunlock = true;
            else allunlock = false;
        }

        if(allunlock)
        {
            AchievementManager.Get.AchievementStepUp(Enum.AchievementType.AllSubWorkUnlock, 1);
        }
    }

    // Update is called once per frame
    void Update ()
    {
        if (Main.Get.GetNowScene() == Enum.SceneType.Main && IsUpdate)
        {
            foreach (int id_ in SubWorkUserDataList.Keys)
            {
                if (SubWorkUserDataList[id_].Level > 0)
                {
                    SubWorkUserDataList[id_].Deltatime += RealTime.deltaTime;

                    SubWorkData sub = DataManager.Get.GetSubWorkData(id_);

                    float WorkTime = sub.WorkTime - (DataManager.Get.GetAttributeData(Enum.AttributeType.SubWork, UserDataManager.Get.GetAttributeLevel(Enum.AttributeType.SubWork)).Abillity);
                    if (WorkTime < 1f) WorkTime = 1f;

                    if (null != sub && SubWorkUserDataList[id_].Deltatime >= WorkTime)
                    {
                        int result = sub.Lv1Result + (sub.LvupResut * (SubWorkUserDataList[id_].Level - 1));
                        result += (int)(result * (DataManager.Get.GetAttributeData(Enum.AttributeType.Insentive, UserDataManager.Get.GetAttributeLevel(Enum.AttributeType.Insentive)).Abillity * 0.01));
                        UserDataManager.Get.AddGold_SubWork(result);
                        //Debug.Log("subwork id = " + id_ + " result = " + result);
                        SubWorkUserDataList[id_].Deltatime = 0f;

                        DateTime EndTime = DateTime.Now.AddSeconds(DataManager.Get.GetSubWorkData(id_).WorkTime - DataManager.Get.GetAttributeData(Enum.AttributeType.SubWork, UserDataManager.Get.GetAttributeLevel(Enum.AttributeType.SubWork)).Abillity);
                        PlayerPrefs.SetString("SubWorkEndTime" + DataManager.Get.GetSubWorkData(id_).Type, EndTime.ToString());
                    }
                }
            }
        }

		
	}
}
