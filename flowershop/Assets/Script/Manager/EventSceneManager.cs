﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventSceneManager : Singleton<EventSceneManager>
{
    public TopBar TopBar;

    public UITexture LeftChar;
    public UITexture RightChar;

    public UILabel CharName;
    public UILabel ScriptText;

    List<EventScriptData> DataList;
    int EventIndex = 0;
    int ScriptIndex = 0;

    bool EventEnd = false;

    public void Awake()
    {
        ScriptText.text = string.Empty;
        if (!SoundManager.Get.Bgm_Obj.isPlaying)
            SoundManager.Get.PlayBgm(Enum.Bgm.Like_a_Flower);
    }

    public void Start()
    {
        //TopBar.UpdateGold(UserDataManager.Get.GetGold());
        //TopBar.UpdataeCrystalLabel(UserDataManager.Get.GetCrystal());
        //TopBar.UpdateHeartLabel(UserDataManager.Get.GetHeart());

        DataList = DataManager.Get.GetEventScriptDataList(UserDataManager.Get.Event_ID);

        if (DataList[EventIndex].L_Res != "empty")
            LeftChar.mainTexture = Resources.Load("CharacterTex/" + DataList[EventIndex].L_Res) as Texture;
        else
            LeftChar.mainTexture = null;

        if (DataList[EventIndex].R_Res != "empty")
            RightChar.mainTexture = Resources.Load("CharacterTex/" + DataList[EventIndex].R_Res) as Texture;
        else
            RightChar.mainTexture = null;

        if (DataList[EventIndex].CharName != "empty")
            CharName.text = DataList[EventIndex].CharName;
        else
            CharName.text = string.Empty;

        ScriptText.text = string.Empty;
    }

    public void EventStart()
    {
        InvokeRepeating("ScriptView", 0, 0.1f);
    }

    void ScriptView()
    {
        ScriptText.text += DataList[EventIndex].Script[ScriptIndex];
        ScriptIndex++;
        if(ScriptIndex == DataList[EventIndex].Script.Length)
        {
            CancelInvoke("ScriptView");
        }
    }

    public void OnClickedTextArea()
    {
        if (EventEnd) return;

        if (ScriptIndex != DataList[EventIndex].Script.Length)
        {
            ScriptText.text = DataList[EventIndex].Script;
            CancelInvoke("ScriptView");
            ScriptIndex = DataList[EventIndex].Script.Length;
        }
        else
        {
            EventIndex++;
            if(EventIndex >= DataList.Count)
            {
                if (UserDataManager.Get.Event_ID == 1)
                    TutorialManager.Get.Clear = 1;

                EventEnd = true;

                //event end
                Main.Get.ChangeScene(Enum.SceneType.Main);
            }
            else
            {
                if (DataList[EventIndex].L_Res != "empty")
                {
                    LeftChar.mainTexture = Resources.Load("CharacterTex/" + DataList[EventIndex].L_Res) as Texture;
                    LeftChar.color = Color.white;
                    LeftChar.depth = 2;
                }
                else if (LeftChar.mainTexture != null)
                {
                    LeftChar.color = Color.gray;
                    LeftChar.depth = 1;
                }

                if (DataList[EventIndex].R_Res != "empty")
                {
                    RightChar.mainTexture = Resources.Load("CharacterTex/" + DataList[EventIndex].R_Res) as Texture;
                    RightChar.color = Color.white;
                    RightChar.depth = 2;
                }
                else if (RightChar.mainTexture != null)
                {
                    RightChar.color = Color.gray;
                    RightChar.depth = 1;
                }

                if (DataList[EventIndex].CharName != "empty")
                    CharName.text = DataList[EventIndex].CharName;
                else
                    CharName.text = string.Empty;

                ScriptText.text = string.Empty;
                ScriptIndex = 0;
                InvokeRepeating("ScriptView", 0, 0.1f);
            }
        }
    }
}
