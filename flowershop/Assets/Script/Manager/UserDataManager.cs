﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;
using System;

[Serializable]
public class UserData
{
    public double Gold = 0;
    public int Crystal = 0;
    public int Heart = 0;

    public Dictionary<int, bool> UnLockFlower = new Dictionary<int, bool>();

    public int[] AttributeLevel = new int[(int)AttributeType.Max];

    public Dictionary<int, bool> UnLockClothes = new Dictionary<int, bool>();

    public int EquipClothes = 0;

    public int ShowAdsCount = 0;

    public UserData()
    {
        UnLockFlower.Add(1, true);
        UnLockFlower.Add(2, true);
        UnLockFlower.Add(3, true);
        UnLockClothes.Add(0, true);
    }
}

public class UserDataManager : Singleton<UserDataManager>
{
    static UserData user = new UserData();

    [HideInInspector]
    public int Facebook_GetBonus = 0;

    static DateTime LastGameExitTime = DateTime.MaxValue;

    public DateTime OfflineStart
    {
        get { return LastGameExitTime; }
    }

    [HideInInspector]
    public bool GetOfflineReward = false;

    private int InvenSlotMax = 30;

    public int InvenMax
    {
        get
        {
            return InvenSlotMax + (int)DataManager.Get.GetAttributeData(AttributeType.InvenSlot, GetAttributeLevel(AttributeType.InvenSlot)).Abillity;
        }
    }

    private int EventID = 1;
    public int Event_ID
    {
        get { return EventID; }
        set { EventID = value; }
    }

    int PushTrigger = 1;
    public int Push
    {
        get { return PushTrigger; }
        set
        {
            PushTrigger = value;
            PlayerPrefs.SetInt("Push", PushTrigger);
        }
    }

    public void LoadPlayPrefab()
    {
        //gold
        if (PlayerPrefs.HasKey("Gold"))
            AddGold(double.Parse(PlayerPrefs.GetString("Gold")));

        //gem
        if (PlayerPrefs.HasKey("Gem"))
           AddCrystal(PlayerPrefs.GetInt("Gem"));

        //good
        if (PlayerPrefs.HasKey("Good"))
            AddHeart(PlayerPrefs.GetInt("Good"));

        //flower unlock
        List<Flower> flowerlist = DataManager.Get.GetFlowerList();
        for (int i = 0; i < flowerlist.Count; ++i)
        {
            if (PlayerPrefs.HasKey("Unlock_Flower" + flowerlist[i].ID))
            {
                int unlock = PlayerPrefs.GetInt("Unlock_Flower" + flowerlist[i].ID);

                if (unlock > 0 && !user.UnLockFlower.ContainsKey(flowerlist[i].ID))
                    user.UnLockFlower.Add(flowerlist[i].ID, true);
                else if(!user.UnLockFlower.ContainsKey(flowerlist[i].ID))
                    user.UnLockFlower.Add(flowerlist[i].ID, false);
            }
        }

        //attribute
        for(int i = 0; i < (int)AttributeType.Max; ++i)
        {
            if(PlayerPrefs.HasKey(((AttributeType)i).ToString()))
            {
                user.AttributeLevel[i] = PlayerPrefs.GetInt(((AttributeType)i).ToString());
            }
        }

        //subjobtime
        List<SubWorkData> subworklist = DataManager.Get.GetSubWorkList();
        for (int i = 0; i < subworklist.Count; ++i)
        {
            if (PlayerPrefs.HasKey("SubWorkLv" + subworklist[i].Type))
            {
                int lv = PlayerPrefs.GetInt("SubWorkLv" + subworklist[i].Type);
                
                System.DateTime EndTime = System.DateTime.Parse(PlayerPrefs.GetString("SubWorkEndTime" + subworklist[i].Type));
                SubWorkManager.Get.SetLoadSubWork(subworklist[i].Type, lv, EndTime);
            }
        }

        //inven
        for (int i = 0; i < InvenMax; ++i)
        {
            if(PlayerPrefs.HasKey("InvenItemId" + i))
            {
                int id = PlayerPrefs.GetInt("InvenItemId" + i);
                int count = PlayerPrefs.GetInt("InvenItemCount" + i);
                InventoryManager.Get.AddItem(id, count);
            }
        }

        //workingdata
        if(PlayerPrefs.HasKey("WorkingItemId0"))
        {
            ItemData[] item = { null, null, null };
            int[] count = { 0, 0, 0 };

            for(int i = 0; i < item.Length; ++i)
            {
                if(PlayerPrefs.HasKey("WorkingItemId" + i))
                {
                    item[i] = DataManager.Get.GetItemData(PlayerPrefs.GetInt("WorkingItemId" + i));
                    Debug.Log(item[i].Name);
                    count[i] = PlayerPrefs.GetInt("WorkingItemCount" + i);
                }
            }

            DateTime start = new DateTime(long.Parse(PlayerPrefs.GetString("WorkingStartTime")));
            float totaltime = PlayerPrefs.GetFloat("WorkingTotalTime");

            WorkingData working = new WorkingData(item[0], item[1], item[2], count[0], count[1], count[2], totaltime, start);
            WorkManager.Get.WorkingData = working;
        }

        //lastgameexittime
        
        if(PlayerPrefs.HasKey("LastExitTime"))
        {
            LastGameExitTime = new DateTime(long.Parse(PlayerPrefs.GetString("LastExitTime")));
            GetOfflineReward = false;
        }

        //clothes
        if(PlayerPrefs.HasKey("Equip"))
        {
            user.EquipClothes = PlayerPrefs.GetInt("Equip");
        }

        List<CharacterClothes> clotheslist = DataManager.Get.GetClothesDataList();
        for(int i = 0; i < clotheslist.Count; ++i)
        {
            if(PlayerPrefs.HasKey("Clothes" + clotheslist[i].ID))
            {
                int unlock = PlayerPrefs.GetInt("Clothes" + clotheslist[i].ID);
                if(user.UnLockClothes.ContainsKey(clotheslist[i].ID))
                {
                    if (unlock > 0)
                        user.UnLockClothes[clotheslist[i].ID] = true;
                    else
                        user.UnLockClothes[clotheslist[i].ID] = false;
                }
                else
                {
                    if (unlock > 0)
                        user.UnLockClothes.Add(clotheslist[i].ID, true);
                    else
                        user.UnLockClothes.Add(clotheslist[i].ID, false);
                }

            }
        }

        //show ads count
        if(PlayerPrefs.HasKey("ShowAdsCount"))
            user.ShowAdsCount = PlayerPrefs.GetInt("ShowAdsCount");

        //Debug.Log("show ad count = " + user.ShowAdsCount);

        if (LastGameExitTime.Day != DateTime.Now.Day)
            user.ShowAdsCount = 0;

        //Debug.Log("lastgameexittime day = " + LastGameExitTime.Day);
        //Debug.Log("now day = " + DateTime.Now.Day);
        //Debug.Log("show ad count 2 = " + user.ShowAdsCount);

        if (PlayerPrefs.HasKey("Push"))
            PushTrigger = PlayerPrefs.GetInt("Push");
    }

    public void SavePlayerPrefab()
    {
        //gold
        PlayerPrefs.SetString("Gold", user.Gold.ToString());
        
        //gem
        PlayerPrefs.SetInt("Gem", user.Crystal);
        
        //flower unlock
        foreach(int o in user.UnLockFlower.Keys)
        {
            if (user.UnLockFlower[o])
                PlayerPrefs.SetInt("Unlock_Flower" + o, 1);
            else
                PlayerPrefs.SetInt("Unlock_Flower" + o, 0);
        }

        //subjobtime
        Dictionary<int, SubWorkUserData> dic = SubWorkManager.Get.GetSubWorkUserDataDic();
        foreach(int o in dic.Keys)
        {
            PlayerPrefs.SetInt("SubWorkLv" + o, dic[o].Level);
            PlayerPrefs.SetFloat("SubWorkDeltatime" + o, dic[o].Deltatime);
        }
        
        //attribute
        for(int i = 0; i < (int)AttributeType.Max; ++i)
        {
            PlayerPrefs.SetInt(((AttributeType)i).ToString(), user.AttributeLevel[i]);
        }

        //inven
        InventoryManager.Get.ResetSaveItem();
    }

    public void SetUserDataToCloud(GPGS_Manager.CloudSaveData data_)
    {
        user.Gold = double.Parse(data_.Gold);
        PlayerPrefs.SetString("Gold", user.Gold.ToString());
        user.Crystal = data_.Crystal;
        PlayerPrefs.SetInt("Gem", user.Crystal);
        user.Heart = data_.Heart;
        PlayerPrefs.SetInt("Good", user.Heart);

        Facebook_GetBonus = data_.FaceBookBonus;

        for(int i = 0; i < data_.UnLockFlowerID.Length; ++i)
        {
            if (data_.UnLockFlowerState[i])
                UnLockFlower(data_.UnLockFlowerID[i]);
        }

        user.AttributeLevel = data_.AttributeLevel;

        for(int i = 0; i < (int)AttributeType.Max; ++i)
            PlayerPrefs.SetInt(((AttributeType)i).ToString(), user.AttributeLevel[i]);

        SubWorkManager.Get.GetSubWorkUserDataDic().Clear();
        for (int i = 0; i < data_.SubWorkID.Length; ++i)
        {
            if(SubWorkManager.Get.GetSubWorkUserDataDic().ContainsKey(data_.SubWorkID[i]))
            {
                SubWorkManager.Get.GetSubWorkUserDataDic()[data_.SubWorkID[i]].Level = data_.SubWorkLevel[i];
                SubWorkManager.Get.GetSubWorkUserDataDic()[data_.SubWorkID[i]].Deltatime = 0f;
            }
            else
            {
                SubWorkManager.Get.GetSubWorkUserDataDic().Add(data_.SubWorkID[i], new SubWorkUserData(data_.SubWorkLevel[i], 0f));
            }

            PlayerPrefs.SetInt("SubWorkLv" + DataManager.Get.GetSubWorkData(data_.SubWorkID[i]).Type, SubWorkManager.Get.GetSubWorkUserDataDic()[data_.SubWorkID[i]].Level);

            DateTime EndTime = DateTime.Now.AddSeconds(DataManager.Get.GetSubWorkData(data_.SubWorkID[i]).WorkTime - DataManager.Get.GetAttributeData(AttributeType.SubWork, GetAttributeLevel(AttributeType.SubWork)).Abillity).AddSeconds(-(SubWorkManager.Get.GetSubWorkUserDataDic()[data_.SubWorkID[i]].Deltatime));
            PlayerPrefs.SetString("SubWorkEndTime" + DataManager.Get.GetSubWorkData(data_.SubWorkID[i]).Type, EndTime.ToString());
        }

        InventoryManager.Get.InvenClear();
        for(int i = 0; i < data_.ItemID.Length; ++i)
        {
            InventoryManager.Get.AddItem(data_.ItemID[i], data_.ItemCount[i]);
        }

        QuestManager.Get.QuestClearState.Clear();
        for(int i = 0; i < data_.QuestID.Length; ++i)
        {
            if(QuestManager.Get.QuestClearState.ContainsKey(data_.QuestID[i]))
            {
                QuestManager.Get.QuestClearState[data_.QuestID[i]] = data_.QuestClearState[i];
            }
            else
            {
                QuestManager.Get.QuestClearState.Add(data_.QuestID[i], data_.QuestClearState[i]);
            }

            PlayerPrefs.SetInt("QuestClear" + data_.QuestID[i], data_.QuestClearState[i]);
        }

        AchievementManager.Get.AchievementClearDic.Clear();
        for(int i = 0; i < data_.AchievementClear_ID.Length; ++i)
        {
            if(AchievementManager.Get.AchievementClearDic.ContainsKey(data_.AchievementClear_ID[i]))
            {
                AchievementManager.Get.AchievementClearDic[data_.AchievementClear_ID[i]] = data_.AchievementClear_State[i];
            }
            else
            {
                AchievementManager.Get.AchievementClearDic.Add(data_.AchievementClear_ID[i], data_.AchievementClear_State[i]);
            }
            PlayerPrefs.SetInt("Achievement" + data_.AchievementClear_ID[i], data_.AchievementClear_State[i]);
        }

        AchievementManager.Get.AchievementStepDic.Clear();
        AchievementManager.Get.NowAchievementDic.Clear();
        for(int i = 0; i < data_.AchievementType.Length; ++i)
        {
            if(AchievementManager.Get.AchievementStepDic.ContainsKey((AchievementType)data_.AchievementType[i]))
            {
                AchievementManager.Get.AchievementStepDic[(AchievementType)data_.AchievementType[i]] = data_.AchievementStep[i];
                PlayerPrefs.SetInt(((AchievementType)data_.AchievementType[i]) + "step", data_.AchievementStep[i]);
                AchievementManager.Get.NowAchievementDic[(AchievementType)data_.AchievementType[i]] = data_.AchievementNowID[i];
                PlayerPrefs.SetInt(((AchievementType)data_.AchievementType[i]) + "now", data_.AchievementNowID[i]);
            }
        }

        for(int i = 0; i < data_.ClothesID.Length; ++i)
        {
            if (user.UnLockClothes.ContainsKey(data_.ClothesID[i]))
                user.UnLockClothes[data_.ClothesID[i]] = data_.ClothesUnlock[i];
            else
                user.UnLockClothes.Add(data_.ClothesID[i], data_.ClothesUnlock[i]);

            if(data_.ClothesUnlock[i] == true)
            {
                PlayerPrefs.SetInt("Clothes" + data_.ClothesID[i], 1);
            }
            else
            {
                PlayerPrefs.SetInt("Clothes" + data_.ClothesID[i], 0);
            }
        }

        

        CouponManager.Get.GamsaCouponGet = data_.GamsaCoupon;
        PlayerPrefs.SetInt("Gamsa", CouponManager.Get.GamsaCouponGet);

        if (data_.PackageBuy[0] == 1)
            PlayerPrefs.SetInt("start_package", data_.PackageBuy[0]);

        if (data_.PackageBuy[1] == 1)
            PlayerPrefs.SetInt("basic_package", data_.PackageBuy[1]);

        if (data_.PackageBuy[2] == 1)
            PlayerPrefs.SetInt("expert_package", data_.PackageBuy[2]);

        QuestManager.Get.QuestRefresh();
        if (Main.Get.GetNowScene() == SceneType.Main)
        {
            MainUIManager.Get.TopBar.UpdateGold(user.Gold);
            MainUIManager.Get.TopBar.UpdataeCrystalLabel(user.Crystal);
            MainUIManager.Get.TopBar.UpdateHeartLabel(user.Heart);
            MainUIManager.Get.ReFreshWindows();
        }
    }

    public double GetGold()
    {
        return user.Gold;
    }

    public int GetCrystal()
    {
        return user.Crystal;
    }

    public int GetHeart()
    {
        return user.Heart;
    }

    public void AddGold(double gold_)
    {
        if (user.Gold + gold_ <= double.MaxValue - 1)
            user.Gold += gold_;
        else
            user.Gold = double.MaxValue -1;

        PlayerPrefs.SetString("Gold", user.Gold.ToString());

        if (Main.Get.GetNowScene() == SceneType.Main)
        {
            MainUIManager.Get.TopBar.UpdateGold(user.Gold);
            MainUIManager.Get.TopBar.PlusGoods(0, (int)gold_);
        }
        else if(Main.Get.GetNowScene() == SceneType.Event)
        {
            //EventSceneManager.Get.TopBar.UpdateGold(user.Gold);
            //EventSceneManager.Get.TopBar.PlusGoods(0, (int)gold_);
        }

        //Debug.Log("addgold");
    }

    public void AddGold_SubWork(long gold_)
    {
        if (user.Gold + gold_ <= long.MaxValue - 1)
            user.Gold += gold_;
        else
            user.Gold = long.MaxValue - 1;

        PlayerPrefs.SetString("Gold", user.Gold.ToString());

        if (Main.Get.GetNowScene() == SceneType.Main)
        {
            MainUIManager.Get.TopBar.UpdateGold(user.Gold);
        }
        else if (Main.Get.GetNowScene() == SceneType.Event)
        {
            //EventSceneManager.Get.TopBar.UpdateGold(user.Gold);
            //EventSceneManager.Get.TopBar.PlusGoods(0, (int)gold_);
        }
    }

    public void AddCrystal(int crystal_)
    {
        if (user.Crystal + crystal_ < 2100000000)
            user.Crystal += crystal_;
        else
            user.Crystal = 2100000000;

        PlayerPrefs.SetInt("Gem", user.Crystal);

        if (Main.Get.GetNowScene() == SceneType.Main)
        {
            MainUIManager.Get.TopBar.UpdataeCrystalLabel(user.Crystal);
            MainUIManager.Get.TopBar.PlusGoods(1, crystal_);
        }
        else if (Main.Get.GetNowScene() == SceneType.Event)
        {
            //EventSceneManager.Get.TopBar.UpdataeCrystalLabel(user.Crystal);
            //EventSceneManager.Get.TopBar.PlusGoods(1, crystal_);
        }
    }

    public void AddHeart(int heart_)
    {
        if (user.Heart + heart_ < 2100000000)
            user.Heart += heart_;
        else
            user.Heart = 2100000000;

        PlayerPrefs.SetInt("Good", user.Heart);

        if (Main.Get.GetNowScene() == SceneType.Main)
        {
            MainUIManager.Get.TopBar.UpdateHeartLabel(user.Heart);
            MainUIManager.Get.TopBar.PlusGoods(2, heart_);
        }
        else if (Main.Get.GetNowScene() == SceneType.Event)
        {
            //EventSceneManager.Get.TopBar.UpdateHeartLabel(user.Heart);
            //EventSceneManager.Get.TopBar.PlusGoods(2, heart_);
        }

        if (null == QuestManager.Get.MainQuest && Main.Get.GetNowScene() != SceneType.Start)
            QuestManager.Get.QuestOccur_Main();
    }       

    public bool UseGold(int gold_)
    {
        if(gold_ > user.Gold)
        {
            //골드 부족 처리
            return false;
        }
        else
            user.Gold -= gold_;

        PlayerPrefs.SetString("Gold", user.Gold.ToString());
        if (Main.Get.GetNowScene() == SceneType.Main)
        {
            MainUIManager.Get.TopBar.UpdateGold(user.Gold);
            MainUIManager.Get.TopBar.PlusGoods(0, -gold_);
        }
        else if (Main.Get.GetNowScene() == SceneType.Event)
        {
            //EventSceneManager.Get.TopBar.UpdateGold(user.Gold);
            //EventSceneManager.Get.TopBar.PlusGoods(0, -gold_);
        }
        return true;
    }

    public bool UseGold(long gold_)
    {
        if (gold_ > user.Gold)
        {
            //골드 부족 처리
            return false;
        }
        else
            user.Gold -= gold_;

        PlayerPrefs.SetString("Gold", user.Gold.ToString());
        if (Main.Get.GetNowScene() == SceneType.Main)
        {
            MainUIManager.Get.TopBar.UpdateGold(user.Gold);
            MainUIManager.Get.TopBar.PlusGoods(0, -gold_);
        }
        else if (Main.Get.GetNowScene() == SceneType.Event)
        {
            //EventSceneManager.Get.TopBar.UpdateGold(user.Gold);
            //EventSceneManager.Get.TopBar.PlusGoods(0, -gold_);
        }
        return true;
    }

    public bool UseCrystal(int crystal_)
    {
        if (crystal_ > user.Crystal)
        {
            //잼 부족 처리
            return false;
        }
        else
            user.Crystal -= crystal_;

      
        PlayerPrefs.SetInt("Gem", user.Crystal);
        if (Main.Get.GetNowScene() == SceneType.Main)
        {
            MainUIManager.Get.TopBar.UpdataeCrystalLabel(user.Crystal);
            MainUIManager.Get.TopBar.PlusGoods(1, -crystal_);
        }
        else if (Main.Get.GetNowScene() == SceneType.Event)
        {
            //EventSceneManager.Get.TopBar.UpdataeCrystalLabel(user.Crystal);
            //EventSceneManager.Get.TopBar.PlusGoods(1, -crystal_);
        }
        return true;
    }

    public void UseGood(int good_)
    {
        if (good_ > user.Heart)
            user.Heart = 0;
        else
            user.Heart -= good_;

        PlayerPrefs.SetInt("Good", user.Heart);
        if (Main.Get.GetNowScene() == SceneType.Main)
        {
            MainUIManager.Get.TopBar.UpdateHeartLabel(user.Heart);
            MainUIManager.Get.TopBar.PlusGoods(2, -good_);
        }
        if (Main.Get.GetNowScene() == SceneType.Event)
        {
            //EventSceneManager.Get.TopBar.UpdateHeartLabel(user.Heart);
            //EventSceneManager.Get.TopBar.PlusGoods(2, -good_);
        }
    }

    public bool CheckUnLockFlower(int id_)
    {
        if (user.UnLockFlower.ContainsKey(id_))
            return user.UnLockFlower[id_];
        else
            return false;
    }

    public void UnLockFlower(int id_)
    {
        if (user.UnLockFlower.ContainsKey(id_))
            user.UnLockFlower[id_] = true;
        else
            user.UnLockFlower.Add(id_, true);

        PlayerPrefs.SetInt("Unlock_Flower" + id_, 1);
    }

    public int GetAttributeLevel(AttributeType type_)
    {
        return user.AttributeLevel[(int)type_];
    }

    public void AttributeLevelUp(AttributeType type_)
    {
        user.AttributeLevel[(int)type_]++;
        PlayerPrefs.SetInt(type_.ToString(), user.AttributeLevel[(int)type_]);

        if(Main.Get.GetNowScene() == SceneType.Main)
        {
            MainUIManager.Get.GetWindow(WindowType.SubWork).GetComponent<SubWorkWindow>().RefreshInfo();
        }

        bool allunlock = false;
        for(int i = 0; i < (int)AttributeType.Max; ++i)
        {
            if (user.AttributeLevel[i] > 0)
                allunlock = true;
            else allunlock = false;
        }

        if (allunlock) AchievementManager.Get.AchievementStepUp(AchievementType.AllAttributeUnLock, 1);
    }
    
    public Dictionary<int, bool> GetUnLockFlower()
    {
        return user.UnLockFlower;
    }

    public int[] GetAttributeLevelArray()
    {
        return user.AttributeLevel;
    }

    public int GetEquipClothesID()
    {
        return user.EquipClothes; 
    }

    public bool CheckUnLockClothes(int id_)
    {
        if (user.UnLockClothes.ContainsKey(id_))
            return user.UnLockClothes[id_];
        else
            return false;
    }

    public void UnLockClothes(int id_)
    {
        if (user.UnLockClothes.ContainsKey(id_))
            user.UnLockClothes[id_] = true;
        else
            user.UnLockClothes.Add(id_, true);

        PlayerPrefs.SetInt("Clothes" + id_, 1);
    }

    public void EquipClothes(int id_)
    {
        user.EquipClothes = id_;
        PlayerPrefs.SetInt("Equip", id_);
    }

    public Dictionary<int, bool> GetUnLockClothesDic()
    {
        return user.UnLockClothes;
    }

    public int GetShowAdsCount()
    {
        return user.ShowAdsCount;
    }

    public void PlusShowAdsCount()
    {
        user.ShowAdsCount++;
        if (user.ShowAdsCount > 6)
            user.ShowAdsCount = 6;

        PlayerPrefs.SetInt("ShowAdsCount", user.ShowAdsCount);
    }

    public void ResetAdsCount()
    {
        user.ShowAdsCount = 0;
        PlayerPrefs.SetInt("ShowAdsCount", user.ShowAdsCount);
    }
}
