﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using GoogleMobileAds.Api;
using System;
using Enum;

public class Ads_Manager : Singleton<Ads_Manager>
{
    private string
        androidGameId = "1622210",
        iosGameId = "1350598";

    [SerializeField]
    private bool testMode;

    private RewardBasedVideoAd rewardBasedVideo;
    string adUnitId_Banner;
    string adUnitId_View;
    [HideInInspector]
    public bool AdMobReward = false;

    /// <summary>
    /// 보상관련 ㅅㅂ 나도 모르겠다 추가되는데로 변수하나하나 만들테다
    /// </summary>
    List<int> reward = null;
    ADShowType ShowType = ADShowType.OffLine;
    int RewardGold = 0;

    void Start()
    {
        string gameId = null;

#if UNITY_ANDROID
        gameId = androidGameId;
#elif UNITY_IOS
        gameId = iosGameId;
#endif
        SetAdmobid();

#if !UNITY_EDITOR
        if (Advertisement.isSupported && !Advertisement.isInitialized)
        {
            Advertisement.Initialize(gameId, testMode);
        }


        //admob init
        MobileAds.Initialize("ca-app-pub-9910842226837850~3947415340");
        rewardBasedVideo = RewardBasedVideoAd.Instance;
        rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad; //광고 로드 실패시
        rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded; //보상받을때?
        rewardBasedVideo.OnAdClosed += HandleOnAdClosed;
        //전면광고
        RequestBanner();
        //Build_AdMob();
#endif
    }

    void SetAdmobid()
    {
#if UNITY_EDITOR
        adUnitId_View = "unused";
        adUnitId_Banner = "unused";
#elif UNITY_ANDROID
        if(testMode)
        {
            adUnitId_View = "ca-app-pub-3940256099942544/5224354917";
            adUnitId_Banner = "ca-app-pub-3940256099942544/6300978111";
        }
        else
        {
            adUnitId_View = "ca-app-pub-9910842226837850/5662965463";
            adUnitId_Banner = "ca-app-pub-9910842226837850/6910875637";
        }
#elif UNITY_IPHONE
            adUnitId_View = "unexpected_platform";
            adUnitId_Banner = "unexpected_platform";
#else
            adUnitId_View = "unexpected_platform";
            adUnitId_Banner = "unexpected_platform";
#endif
    }

    //public void ShowAd_UnityAds(List<int> reward_)
    //{
    //    if (Advertisement.IsReady())
    //    {
    //        Debug.Log("광고 시청 로딩");
    //        Advertisement.Show();
    //        reward = reward_;
    //    }
    //}

    public void ShowAd_UnityAds(ADShowType type_, List<int> reward_)
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            ShowType = type_;
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
            reward = reward_;
        }
    }

    public void ShowAd_UnityAds(ADShowType type_, int rewardgold_)
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            ShowType = type_;
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
            RewardGold = rewardgold_;
        }
    }

    public void ShowAd_UnityAds(ADShowType type_)
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            ShowType = type_;
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
    }

    /// <summary>
    /// 연속광고에서 사용한다
    /// </summary>
    public void ShowAd_UnityAds()
    {
        Message m = new Message(null, null, "잠시 기다려주세요.", string.Empty, string.Empty, MessagePopupType.Loading);
        PopupManager.Get.OpenPopup("Popup_Message", m, false);
        Invoke("UnityAdsTimeOut", 15f);
        if (Advertisement.IsReady("rewardedVideo"))
        {
            CancelInvoke("UnityAdsTimeOut");
            ShowType = ADShowType.ShowCombo;
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
    }

    void UnityAdsTimeOut()
    {
        PopupManager.Get.ClosePopup("Popup_Message");

        Message m = new Message(null, null, "잠시후 다시 시도해 보세요.", "확인", string.Empty);
        PopupManager.Get.OpenPopup("Popup_Message", m);
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                //Debug.Log("The ad was successfully shown.");
                //
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.

                switch(ShowType)
                {
                    case ADShowType.OffLine:
                        UserDataManager.Get.AddGold(RewardGold * 2);
                        UserDataManager.Get.GetOfflineReward = true;
                        PopupManager.Get.ClosePopup("Popup_UesCrystal");
                        break;
                    case ADShowType.Shop:
                        PayAdReward();
                        break;
                    case ADShowType.ShowCombo:
                        PopupManager.Get.ClosePopup("Popup_Message");
                        UserDataManager.Get.PlusShowAdsCount();
                        RewardData r = DataManager.Get.GetRewardData(DataManager.Get.GetShowADsData(UserDataManager.Get.GetShowAdsCount()).RewardID);
                        PaymentManager.Get.PayPrice(r.ID);
                        List<IconTextData> list = new List<IconTextData>();
                        list.Add(r.ConvertIconText());
                        GoodsMessage message = new GoodsMessage("보상을 얻었습니다", list, "확인", string.Empty, null, null, MessagePopupType.Ok);
                        PopupManager.Get.OpenPopup("Popup_GoodsMessage", message);
                        PopupManager.Get.RefreshPopup("Popup_ShowAd");
                        break;
                    case ADShowType.RewardDouble:
                        UserDataManager.Get.AddGold(RewardGold * 2);
                        PopupManager.Get.AllClosePopup();
                        break;
                    case ADShowType.MiniGame_Card:
                        MiniGameManager.Get.Card_CoolTime = DateTime.MinValue;
                        break;
                    case ADShowType.MiniGame_WhereHouse:
                        MiniGameManager.Get.WhereHouse_CoolTime = DateTime.MinValue;
                        break;
                    case ADShowType.Crystal_One:
                        UserDataManager.Get.AddCrystal(1);
                        break;
                }

                
                //연출 필요할듯??
                //광고 보기 업적 카운트 ++
                AchievementManager.Get.AchievementStepUp(Enum.AchievementType.ShowAd, 1);
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }

    //전면 광고
    BannerView bannerView;
    private void RequestBanner()
    {
        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(adUnitId_Banner, AdSize.Banner, AdPosition.Bottom);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();

        // Load the banner with the request.
        bannerView.LoadAd(request);
    }

    
    public void Build_AdMob()
    {
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded video ad with the request.
        rewardBasedVideo.LoadAd(request, adUnitId_View);
    }

    public void showAd_admob(List<int>reward_)
    {
        PopupManager.Get.AllClosePopup();
        if (rewardBasedVideo.IsLoaded())
        {
            AdMobReward = true;
            //Subscribe to Ad event
            rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
            rewardBasedVideo.Show();
            reward = reward_;
        }
        else
        {
            Message message = new Message(null, null, "잠시 후 다시 시도해주세요.", "확인", string.Empty);
            PopupManager.Get.OpenPopup("Popup_Message", message);
            AdRequest request = new AdRequest.Builder().Build();
            rewardBasedVideo.LoadAd(request, adUnitId_View);
        }
    }

    /// <summary>
    /// 오프라인 보상용
    /// </summary>
    /// <param name="type_"></param>
    /// <param name="reward_"></param>
    public void showAd_admob(Enum.ADShowType type_, int reward_)
    {
        //PopupManager.Get.AllClosePopup();
        if (rewardBasedVideo.IsLoaded())
        {
            AdMobReward = true;
            //Subscribe to Ad event
            rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
            rewardBasedVideo.Show();
            RewardGold = reward_;
            ShowType = type_;
        }
        else
        {
            Message message = new Message(null, null, "잠시 후 다시 시도해주세요.", "확인", string.Empty);
            PopupManager.Get.OpenPopup("Popup_Message", message);
            AdRequest request = new AdRequest.Builder().Build();
            rewardBasedVideo.LoadAd(request, adUnitId_View);
        }
    }

    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        Message message = new Message(null, null, "광고를 불러오는 중\n오류가 발생했습니다.\n" + args.Message , "확인", string.Empty, Enum.MessagePopupType.Ok);
        PopupManager.Get.OpenPopup("Popup_Message", message);
    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        AchievementManager.Get.AchievementStepUp(Enum.AchievementType.ShowAd, 1);
        
        Debug.Log("ad result = " + amount.ToString() + " " + type);

        AdMobReward = true;
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        //   Load it here
        if (!rewardBasedVideo.IsLoaded())
        {
            AdRequest request = new AdRequest.Builder().Build();
            // Load the rewarded video ad with the request.
            rewardBasedVideo.LoadAd(request, adUnitId_View);
        }
    }

    public void PayAdReward()
    {
        switch (ShowType)
        {
            case ADShowType.Shop:
                for (int i = 0; i < reward.Count; ++i)
                {
                    PaymentManager.Get.PayPrice(reward[i]);
                }
                RewardData r = DataManager.Get.GetRewardData(reward[0]);

                List<IconTextData> icontextdatas = new List<IconTextData>();

                icontextdatas.Add(r.ConvertIconText());

                GoodsMessage message = new GoodsMessage("보상을 받았습니다", icontextdatas, "확인", string.Empty, null, null);
                PopupManager.Get.OpenPopup("Popup_GoodsMessage", message);
                reward = null;
                break;
            case ADShowType.OffLine:
                Debug.Log("광고 2배 보상이다!");
                UserDataManager.Get.AddGold(RewardGold * 2);
                UserDataManager.Get.GetOfflineReward = true;
                PopupManager.Get.ClosePopup("Popup_UesCrystal");
                Message m = new Message(null, null, (RewardGold * 2) + "골드를 받았습니다.", "확인", string.Empty);
                PopupManager.Get.OpenPopup("Popup_Message", m);
                break;
        }
    }

    private void OnDestroy()
    {
#if !UNITY_EDITOR
        bannerView.Destroy();
#endif
    }

}

