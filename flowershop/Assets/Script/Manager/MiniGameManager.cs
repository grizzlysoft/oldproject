﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MiniGameManager : Singleton<MiniGameManager>
{
    // 상수들
    int CardBasicReward = 0;
    public int CardBasic_Reward
    {
        get { return CardBasicReward; }
        set { CardBasicReward = value; }
    }

    int CardBestRecodeReward = 0;
    public int CardBestRecode_Reward
    {
        get { return CardBestRecodeReward; }
        set { CardBestRecodeReward = value; }
    }

    int CardSucceceReward = 0;
    public int CardSuccece_Reward
    {
        get { return CardSucceceReward; }
        set { CardSucceceReward = value; }
    }

    int WhereHouseMoveReward = 0;
    public int WhereHouseMove_Reward
    {
        get { return WhereHouseMoveReward; }
        set { WhereHouseMoveReward = value; }
    }

    int WhereHouseComboReward = 0;
    public int whereHouseCombo_Reward
    {
        get { return WhereHouseComboReward; }
        set { WhereHouseComboReward = value; }
    }

    int WhereHouseBestReward = 0;
    public int WhereHouseBest_Reward
    {
        get { return WhereHouseBestReward; }
        set { WhereHouseBestReward = value; }
    }

    float CardMinTime = 60f;

    int CardBasicGold = 1000;
    public int CardBasic { get { return CardBasicGold; } }

    int CardMaxCombo = 0;

    float Card_BestRecode = 0f;
    public float Card_Best
    {
        get { return Card_BestRecode; }
        set { Card_BestRecode = value; }
    }

    int WhereHouseBestCombo = 0;
    public int WhereHouse_Best
    {
        get { return WhereHouseBestCombo; }
    }

    DateTime CardCoolTime = DateTime.MinValue;
    public DateTime Card_CoolTime
    {
        get { return CardCoolTime; }
        set
        {
            CardCoolTime = value;
            PlayerPrefs.SetString("CardCoolTime", CardCoolTime.Ticks.ToString());
        }
    }

    DateTime WhereHouseCoolTime = DateTime.MinValue;
    public DateTime WhereHouse_CoolTime
    {
        get { return WhereHouseCoolTime; }
        set
        {
            WhereHouseCoolTime = value;
            PlayerPrefs.SetString("WhereHouseCoolTime", WhereHouseCoolTime.Ticks.ToString());
        }
    }

    public void ManagerInit()
    {
        if (PlayerPrefs.HasKey("CardBestRecode"))
            Card_BestRecode = PlayerPrefs.GetFloat("CardBestRecode");

        if (PlayerPrefs.HasKey("WhereHouseBestCombo"))
            WhereHouseBestCombo = PlayerPrefs.GetInt("WhereHouseBestCombo");

        if (PlayerPrefs.HasKey("CardCoolTime"))
            CardCoolTime = new DateTime(long.Parse(PlayerPrefs.GetString("CardCoolTime")));

        if (PlayerPrefs.HasKey("WhereHouseCoolTime"))
            WhereHouseCoolTime = new DateTime(long.Parse(PlayerPrefs.GetString("WhereHouseCoolTime")));
    }

    public int CalculateReward_Card(float playtime_, int scroe_)
    {
        int reward = 0;
        if(playtime_ > 0)
        {
            //시간안에 성공
            reward += CardSucceceReward;
            reward += CardBasicGold * (int)playtime_; //남은 시간 보상

            if (playtime_ > Card_BestRecode)
            {
                Card_BestRecode = playtime_; //최고 기록 갱신
                PlayerPrefs.SetFloat("CardBestRecode", Card_BestRecode); //저장
                GPGS_Manager.Get.ReportScore(GPGSIds.leaderboard, (int)Card_BestRecode);
                reward += CardBestRecodeReward; //최고기록 보상
            }
        }

        reward += CardBasicReward * scroe_; //점수 보상

        return reward;
    }

    public int CalculateReward_WhereHouse(int combo_, int scroe_)
    {
        int reward = 0;

        if(WhereHouseBestCombo < combo_)
        {
            WhereHouseBestCombo = combo_;
            PlayerPrefs.SetInt("WhereHouseBestCombo", WhereHouseBestCombo);
            GPGS_Manager.Get.ReportScore(GPGSIds.leaderboard_2, WhereHouseBestCombo);
            reward += WhereHouseBestReward;
        }

        reward += scroe_ * WhereHouseMoveReward;
        reward += combo_ * WhereHouseComboReward;

        return reward;
    }

}
