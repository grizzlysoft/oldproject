﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class WorkManager : Singleton<WorkManager>
{
    WorkingData Data = null;

    public WorkingData WorkingData
    {
        get
        {
            return Data;
        }

        set
        {
            Data = value;

            for (int i = 0; i < Data.WorkingItem.Length; ++i)
            {
                if (Data.WorkingItem[i] != null)
                {
                    PlayerPrefs.SetInt("WorkingItemId" + i, Data.WorkingItem[i].ID);
                    PlayerPrefs.SetInt("WorkingItemCount" + i, Data.ItemCount[i]);
                }
            }

            PlayerPrefs.SetString("WorkingStartTime", Data.StartTime.Ticks.ToString());
            PlayerPrefs.SetFloat("WorkingTotalTime", Data.TotalTime);
        }
    }

    public void WorkEnd()
    {
        for (int i = 0; i < Data.WorkingItem.Length; ++i)
        {
            if (Data.WorkingItem[i] != null)
            {
                InventoryManager.Get.AddItem(Data.WorkingItem[i].ID, Data.ItemCount[i]);
            }
        }

        if(Main.Get.GetNowScene() == Enum.SceneType.Main)
        {
            //PopupManager.Get.AllClosePopup();
            if(MainUIManager.Get.WinState == Enum.WindowType.Work)
                PopupManager.Get.ClosePopup("Popup_UesCrystal");

            MainUIManager.Get.GetWindow(Enum.WindowType.Work).GetComponent<WorkWindow>().WorkClose();
            MainUIManager.Get.GetWindow(Enum.WindowType.Box).GetComponent<BoxWindow>().ScrollSetting(Enum.BoxTab.Flower);
            MainUIManager.Get.GetWindow(Enum.WindowType.Box).GetComponent<BoxWindow>().ScrollSetting(Enum.BoxTab.Boquet);
            MainUIManager.Get.GetWindow(Enum.WindowType.Box).GetComponent<BoxWindow>().ScrollSetting(Enum.BoxTab.Basket);

            WorkCompleteData complete = new WorkCompleteData(Data.WorkingItem, Data.ItemCount);
            PopupManager.Get.OpenPopup("Popup_WorkComplete", complete);

            MainUIManager.Get.GetWindow(Enum.WindowType.Main).GetComponent<MainWindow>().QuestTap.QuestIconRefresh();
        }

        for (int i = 0; i < Data.WorkingItem.Length; ++i)
        {
            if (Data.WorkingItem[i] != null)
            {
                PlayerPrefs.DeleteKey("WorkingItemId" + i);
                PlayerPrefs.DeleteKey("WorkingItemCount" + i);
            }
        }

        PlayerPrefs.DeleteKey("WorkingStartTime");
        PlayerPrefs.DeleteKey("WorkingTotalTime");

        Data = null;
    }

    public void WorkCancel()
    {
        for (int i = 0; i < Data.WorkingItem.Length; ++i)
        {
            if (Data.WorkingItem[i] != null)
            {
                PlayerPrefs.DeleteKey("WorkingItemId" + i);
                PlayerPrefs.DeleteKey("WorkingItemCount" + i);
            }
        }
        Data = null;
        PlayerPrefs.DeleteKey("WorkingStartTime");
        PlayerPrefs.DeleteKey("WorkingTotalTime");
        Message m = new Message(null, null, "작업이 취소 되었습니다.", "확인", string.Empty);
        PopupManager.Get.OpenPopup("Popup_Message", m);
    }

    private void Update()
    {
        if (Data != null && Main.Get.GetNowScene() == Enum.SceneType.Main)
        {
            if (Data.EndTime < DateTime.Now)
            {
                if (Main.Get.GetNowScene() == Enum.SceneType.Main)
                    MainUIManager.Get.WorkWindowMenuOpen();

                WorkEnd();
            }
        }
    }
}
