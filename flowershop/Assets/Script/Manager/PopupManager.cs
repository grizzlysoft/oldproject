﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (UIPanel))]
[RequireComponent(typeof(TweenScale))]
public class Popup : MonoBehaviour
{
    public GameObject PopupObj;
    bool _isOpen = false;

    [HideInInspector]
    public bool _isBackButton_exit = true;

    /// <summary>
    /// false 넣으면 백버튼 눌러도 안꺼짐
    /// </summary>
    /// <param name="back"></param>
    public virtual void Open(bool back = true) { PopupObj.SetActive(true); _isOpen = true; GetComponent<TweenScale>().PlayForward(); _isBackButton_exit = back; SetText(); }

    public virtual void Close() {PopupObj.SetActive(false);  _isOpen = false; GetComponent<TweenScale>().ResetToBeginning(); }

    public virtual void SetData(object data_) { }

    public bool IsOpen() { return _isOpen; }

    public virtual void FirstLoad() { }

    public virtual void SetText() { }

    public virtual void Refresh() { }
}

public class PopupManager : Singleton<PopupManager>
{
    List<Popup> PopupList = new List<Popup>();

    List<string> OpenPopupList = new List<string>();

    Transform PopupParent = null;

    int PopupDepth = 100;

    bool WorkComplte = false;
    object WorkComplteData = null;
	
    public void OpenPopup(string name_, object data_ = null, bool backaction_ = true)
    {
        if (name_ == "Popup_WorkComplete" && WorkComplte == false)
        {
            WorkComplte = true;
            WorkComplteData = data_;
            return;
        }

        for(int i = 0; i < OpenPopupList.Count; ++i)
        {
            if(OpenPopupList[i] == name_)
            {
                //이미 열린 팝업
                Debug.Log(name_ + "이미 열림");
                return;
            }
        }

        //열렸었던 팝업이면 오픈해준다
        for(int i = 0; i < PopupList.Count; ++i)
        {
            if(PopupList[i].name == name_)
            {
                if (data_ != null)
                    PopupList[i].SetData(data_);

                PopupList[i].PopupObj.GetComponent<UIPanel>().depth = PopupDepth;
                PopupDepth += 5;

                PopupList[i].Open(backaction_);
                OpenPopupList.Add(name_);

                if (name_ == "Popup_WorkComplete")
                {
                    WorkComplte = false;
                    WorkComplteData = null;
                    SoundManager.Get.PlaySfx(Enum.Sfx.workComplete_se);
                }
                return;
            }
        }

        //안열렸던 팝업이면 불러와준다
        Popup popup = (Instantiate(Resources.Load("Prefab/Popup/" + name_)) as GameObject).GetComponent<Popup>();
        popup.name = popup.name.Replace("(Clone)", "");
        if (null != PopupParent)
            popup.transform.parent = PopupParent;

        popup.transform.localScale = Vector3.one;
        popup.transform.localPosition = Vector3.zero;
        popup.PopupObj.GetComponent<UIPanel>().depth = PopupDepth;
        popup.FirstLoad();
        PopupDepth += 5;

        if (null != popup)
        {
            PopupList.Add(popup);
            if (data_ != null)
            {
                popup.SetData(data_);
            }

            popup.Open(backaction_);

            OpenPopupList.Add(name_);

            if (name_ == "Popup_WorkComplete")
            {
                WorkComplte = false;
                WorkComplteData = null;
                SoundManager.Get.PlaySfx(Enum.Sfx.workComplete_se);
            }
        }
        else
        {
            Debug.LogError("해당 팝업을 열수 없습니다(popup = null)");
        }
    }

    public void SetPopupParent(Transform t)
    {
        //Debug.Log(t.name);
        PopupParent = t;
    }

    public void ClearPopupParent()
    {
        PopupParent = null;
        PopupDepth = 0;
    }

    /// <summary>
    /// 마지막으로 열린 팝업을 닫는다
    /// </summary>
    public void ClosePopup()
    {
        if (OpenPopupList.Count > 0)
        {
            Popup p = FindPopup(OpenPopupList[OpenPopupList.Count - 1]);

            if (p != null && p.IsOpen() == true && p._isBackButton_exit == true)
            {
                p.Close();
                OpenPopupList.RemoveAt(OpenPopupList.Count - 1);
            }
        }
    }

    public void ClosePopup(string s)
    {
        Popup p = FindPopup(s);
        if (p != null && p.IsOpen() == true)
            p.Close();

        OpenPopupList.Remove(s);
    }

    public Popup FindPopup(string name_)
    {
        for(int i = 0; i < PopupList.Count; ++i)
        {
            if(PopupList[i].name == name_)
            {
                return PopupList[i];
            }
        }

        return null;
    }

    public void AllClosePopup()
    {
        for(int i = 0; i < OpenPopupList.Count; ++i)
        {
            //Debug.Log("openpopuplist = " + OpenPopupList[i]);
            Popup p = FindPopup(OpenPopupList[i]);
            if (p != null && p.IsOpen() == true)
                p.Close();
        }

        OpenPopupList.Clear();
    }

    public void ObjDestroy()
    {
        for(int i = 0; i < PopupList.Count; ++i)
        {
            Destroy(PopupList[i].gameObject);
        }

        PopupList.Clear();
    }

    public int GetOpenPopupCnt()
    {
        return OpenPopupList.Count;
    }

    public void RefreshPopup(string name_)
    {
        for(int i = 0; i < PopupList.Count; ++i)
        {
            if (PopupList[i].name == name_)
                PopupList[i].Refresh();
        }
    }

    private void Update()
    {
        if(WorkComplte && OpenPopupList.Count == 0)
        {
            OpenPopup("Popup_WorkComplete", WorkComplteData);
        }
    }
}
