﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;
using System;

public class Flower
{
    public readonly int ID;
    public readonly string Name;
    public readonly string Text;
    public readonly float WorkTime;
    public readonly int UnLockCost;
    public readonly int ResultID;
    public readonly int UnlockConditionID;

    public Flower(int id_, string name_, string text_, float worktime_, int unlockcost_, int resultid_, int condition_)
    {
        ID = id_;
        Name = name_;
        Text = text_;
        WorkTime = worktime_;
        UnLockCost = unlockcost_;
        ResultID = resultid_;
        UnlockConditionID = condition_;
    }
}

public class ItemData
{
    public readonly int ID;
    public readonly string Name;
    public readonly string Text;
    public readonly int Max;
    public readonly int Sell;
    public readonly string Res;
    public readonly int Type;

    public ItemData(int id_, string name_, string text_, int max_, int sell_, string res_, int type_)
    {
        ID = id_;
        Name = name_;
        Text = text_;
        Max = max_;
        Sell = sell_;
        Res = res_;
        Type = type_;
    }

}

public class SubWorkData
{
    public readonly int Type;
    public readonly string Name;
    public readonly int WorkTime;
    public readonly int Lv1Result;
    public readonly int LvupResut;
    public readonly int FirstUpgradeCost;
    public readonly int LvupUpgradeCost;
    public readonly int UnLockCost;
    public readonly int MaxLv;
    public readonly string Res;
    public readonly int UnLockConditionID;

    public SubWorkData(int type_, string name_, int worktime_, int lv1result_, int lvupresult_, int firstupgradecost_, int lvupgradecost_, int unlockcost_, int maxlv_, string res_, int condition_)
    {
        Type = type_;
        Name = name_;
        WorkTime = worktime_;
        Lv1Result = lv1result_;
        LvupResut = lvupresult_;
        FirstUpgradeCost = firstupgradecost_;
        LvupUpgradeCost = lvupgradecost_;
        UnLockCost = unlockcost_;
        MaxLv = maxlv_;
        Res = res_;
        UnLockConditionID = condition_;
    }
}

public class InventoryItem
{
    public readonly int ID;
    public int Count;

    public InventoryItem(int id_, int count_)
    {
        ID = id_;
        Count = count_;
    }
}

public class WorkingData
{
    public readonly ItemData[] WorkingItem = new ItemData[3];
    public readonly int[] ItemCount = new int[3];
    public readonly float TotalTime;
    public readonly DateTime StartTime;
    public readonly DateTime EndTime;
    public float TimeLapse = 0;

    public WorkingData(ItemData item1_, ItemData item2_, ItemData item3_, int itemcount1_, int itemcount2_, int itemcount3_,
        float totaltime_, DateTime starttime_)
    {
        WorkingItem[0] = item1_;
        WorkingItem[1] = item2_;
        WorkingItem[2] = item3_;
        ItemCount[0] = itemcount1_;
        ItemCount[1] = itemcount2_;
        ItemCount[2] = itemcount3_;
        TotalTime = totaltime_;
        StartTime = starttime_;
        EndTime = starttime_.AddSeconds(totaltime_);
    }
}

public class WorkListData
{
    public int Itemid;
    public int Count;
    public float Time;

    public Dictionary<int, int> MatList = new Dictionary<int, int>();

    public void AddMat(int id_, int count_)
    {
        if (MatList.ContainsKey(id_))
            MatList[id_] += count_;
        else
            MatList.Add(id_, count_);
    }

    public void RemoveMat(int id_, int count_)
    {
        if (MatList.ContainsKey(id_))
        {
            if (MatList[id_] <= count_)
                MatList.Remove(id_);
        }
    }

    public void DeletMat(int id_)
    {
        MatList.Remove(id_);
    }
}

public class SubWorkUserData
{
    public int Level;
    public float Deltatime;

    public SubWorkUserData(int level_, float deltatime_)
    {
        Level = level_;
        Deltatime = deltatime_;
    }
}

public class AttributeData
{
    public readonly AttributeType Type;
    public readonly int Level;
    public readonly string Name;
    public readonly string Text;
    public readonly long UpgradeCost;
    public readonly float Abillity;
    public readonly int UnlockCost;
    public readonly AttributeCostType CostType;
    public readonly string Res;

    public AttributeData(AttributeType type_, int level_, string name_, string text_, long upcost_, float abillity_, int unlockcost_, AttributeCostType costtype_, string res_)
    {
        Type = type_;
        Level = level_;
        Name = name_;
        Text = text_;
        UpgradeCost = upcost_;
        Abillity = abillity_;
        UnlockCost = unlockcost_;
        CostType = costtype_;
        Res = res_;
    }
}

public class MatData
{
    public readonly int ID;
    public readonly int Count;

    public MatData(int id_, int count_)
    {
        ID = id_;
        Count = count_;
    }
}

public class BouquetData
{
    public readonly int ID;
    public readonly string Name;
    public readonly string Text;
    public readonly MatData[] Mat;
    public readonly float WorkTime;
    public readonly int GoldCost;
    public readonly int Result_ID;

    public BouquetData (int id_, string name_, string text_, MatData[] mat_, float worktime_, int goldcost_, int result_id_)
    {
        ID = id_;
        Name = name_;
        Text = text_;
        Mat = mat_;
        WorkTime = worktime_;
        GoldCost = goldcost_;
        Result_ID = result_id_;
    }
}

public class BasketData
{
    public readonly int ID;
    public readonly string Name;
    public readonly string Text;
    public readonly MatData[] Mat;
    public readonly float WorkTime;
    public readonly int GoldCost;
    public readonly int Result_ID;

    public BasketData(int id_, string name_, string text_, MatData[] mat_, float worktime_, int goldcost_, int result_id_)
    {
        ID = id_;
        Name = name_;
        Text = text_;
        Mat = mat_;
        WorkTime = worktime_;
        GoldCost = goldcost_;
        Result_ID = result_id_;
    }
}

public class ShopData
{
    public readonly int ID;
    public readonly ShopTapType Type;
    public readonly Shop_PayType PayType;
    public readonly int Cost;
    public readonly string Name;
    public readonly string Text;
    public readonly string Res;
    public readonly List<int> RewardID;
    public readonly string StoreID;

    public ShopData(int id_, ShopTapType type_, Shop_PayType paytype_, int cost_, string name_, string text_, string res_, List<int> rewardid_, string storeid_)
    {
        ID = id_;
        Type = type_;
        PayType = paytype_;
        Cost = cost_;
        Name = name_;
        Text = text_;
        Res = res_;
        RewardID = rewardid_;
        StoreID = storeid_;
    }
}

public class RewardData
{
    public readonly int ID;
    public readonly RewardType Type;
    public readonly string Name;
    public readonly int ItemID;
    public readonly int Value;

    public RewardData(int id_, RewardType type_, string name_, int itemid_, int value_)
    {
        ID = id_;
        Type = type_;
        Name = name_;
        ItemID = itemid_;
        Value = value_;
    }

    public IconTextData ConvertIconText()
    {
        switch (Type)
        {
            case Enum.RewardType.Crystal:
                return new IconTextData(Enum.IconTextType.Crystal, Value, ItemID);
            case Enum.RewardType.Gold:
                return new IconTextData(Enum.IconTextType.Gold, Value, ItemID);
            case Enum.RewardType.Heart:
                return new IconTextData(Enum.IconTextType.Heart, Value, ItemID);
            case Enum.RewardType.Item:
                return new IconTextData(Enum.IconTextType.Item, Value, ItemID);
            default:
                return null;
        }
    }
}

public class WorkCompleteData
{
    public readonly ItemData[] Item;
    public readonly int[] Count;

    public WorkCompleteData(ItemData[] item_, int[] count_)
    {
        Item = item_;
        Count = count_;
    }
}
public delegate void onBtnClicked();

public class Message
{
    public readonly MessagePopupType Type;
    public readonly string Text = string.Empty;
    public readonly string OkText = string.Empty;
    public readonly string CancelText = string.Empty;

    public onBtnClicked Ok = null;
    public onBtnClicked Cancel = null;


    public Message(onBtnClicked ok_, onBtnClicked cancel_, string text_, string oktext_, string canceltext_, MessagePopupType type_ = MessagePopupType.Ok)
    {
        Text = text_;
        OkText = oktext_;
        CancelText = canceltext_;
        Type = type_;
        Ok = ok_;
        Cancel = cancel_;
    }
}

public class BigMessage
{
    public readonly string Text = string.Empty;
    public readonly string LeftText = string.Empty;
    public readonly string RightText = string.Empty;
    public EventDelegate Leftdelelgate = null;
    public EventDelegate Rightdelelgate = null;
    public EventDelegate Closedelegate = null;

    public BigMessage(EventDelegate left_, EventDelegate right_, string text_, string lefttext_, string righttext_, EventDelegate close_)
    {
        Text = text_;
        LeftText = lefttext_;
        RightText = righttext_;
        Leftdelelgate = left_;
        Rightdelelgate = right_;
        Closedelegate = close_;
    }
}

public class GoodsMessage
{
    public readonly string Text;
    public readonly MessagePopupType BtnType;
    public readonly List<IconTextData> IconTextDatas = null;
    public readonly string Ok_Text;
    public readonly string Cancel_Text;
    public onBtnClicked Ok = null;
    public onBtnClicked Cancel = null;

    public GoodsMessage(string text_, List<IconTextData> list_, string ok_text_, string cancel_text_, onBtnClicked ok_, onBtnClicked cancel_, MessagePopupType btntype_ = MessagePopupType.Ok)
    {
        Text = text_;
        IconTextDatas = list_;
        Ok_Text = ok_text_;
        Cancel_Text = cancel_text_;
        BtnType = btntype_;
        Ok = ok_;
        Cancel = cancel_;
    }
}

public class OffLineReward
{
    public readonly int SubWorkID;
    public readonly int Reward;

    public OffLineReward(int subworkid_, int reward_)
    {
        SubWorkID = subworkid_;
        Reward = reward_;
    }
}

public class Ytop10Coupon
{
    public string result;
    public string code;
    public string message;
    public string uuid;
}

[System.Serializable]
public class C_Items
{
    public string item_code = "Empty";
    public string item_count = "Empty";
}

[System.Serializable]
public class HappCoupon
{
    public string result;
    public string error_code;
    public string error_msg;
    public string benefit;
    public C_Items[] items;
}

public class AchievementData
{
    public readonly AchievementType Type;
    public readonly int ID;
    public readonly string Name;
    public readonly int ClearValue;
    public readonly int Next;
    public readonly int First;
    public readonly int RewardID;
    public readonly string GpgsID;

    public AchievementData(AchievementType type_, int id_, string name_, int clearvalue_, int next_, int first_, int rewardid_, string gpgsid_)
    {
        Type = type_;
        ID = id_;
        Name = name_;
        ClearValue = clearvalue_;
        Next = next_;
        First = first_;
        RewardID = rewardid_;
        GpgsID = gpgsid_;
    }
}

public class QuestData
{
    public readonly QuestType Type;
    public readonly int ID;
    public readonly string Name;
    public readonly string Res;
    public readonly string StartScript;
    public readonly int EventID;
    public readonly List<int> RequestID;
    public readonly int MinGood;
    public readonly int MaxGood;
    public readonly List<int> RewardID;
    public readonly int RefuseGood;

    public QuestData (QuestType type_, int id_, string name_, string res_, string startscript_, int eventid_,
        List<int> requestid_, int mingood_, int maxgood_, List<int> rewardid_, int refusegood_)
    {
        Type = type_;
        ID = id_;
        Name = name_;
        Res = res_;
        StartScript = startscript_;
        EventID = eventid_;
        RequestID = requestid_;
        MinGood = mingood_;
        MaxGood = maxgood_;
        RewardID = rewardid_;
        RefuseGood = refusegood_;
    }
}

public class ReQuestData
{
    public readonly int ID;
    public readonly string Name;
    public readonly int ItemID;
    public readonly int Count;

    public ReQuestData(int id_, string name_, int itemid_, int count_)
    {
        ID = id_;
        Name = name_;
        ItemID = itemid_;
        Count = count_;
    }
}

public class IconTextData
{
    public readonly IconTextType Type;
    public readonly int Count;
    public readonly int ItemID;

    public IconTextData (IconTextType type_, int count_, int itemid_)
    {
        Type = type_;
        Count = count_;
        ItemID = itemid_;
    }
}

public class UseCrystalPopupData
{
    public readonly string Text;
    public readonly MessagePopupType BtnType;
    public readonly IconTextData iconTextData;
    public readonly string OkText;
    public readonly string CancelText;
    public onBtnClicked Ok = null;
    public onBtnClicked Cancel = null;

    public UseCrystalPopupData(string text_, MessagePopupType type_, IconTextData iconText_, string oktext_, string canceltext_, onBtnClicked ok_, onBtnClicked cancel_)
    {
        Text = text_;
        BtnType = type_;
        iconTextData = iconText_;
        OkText = oktext_;
        CancelText = canceltext_;
        Ok = ok_;
        Cancel = cancel_;
    }
}

public class CharacterClothes
{
    public readonly int ID;
    public readonly ClothesType Type;
    public readonly string Name;
    public readonly int Cost;
    public readonly string Res;
    public readonly string ShopRes;

    public CharacterClothes(int id_, ClothesType type_, string name_, int cost_, string res_, string shopres_)
    {
        ID = id_;
        Type = type_;
        Name = name_;
        Cost = cost_;
        Res = res_;
        ShopRes = shopres_;
    }
}

public class ShowAdsData
{
    public readonly int ID;
    public readonly string Name;
    public readonly int Count;
    public readonly string Res;
    public readonly int RewardID;

    public ShowAdsData(int id_, string name_, int count_, string res_, int rewardid_)
    {
        ID = id_;
        Name = name_;
        Count = count_;
        Res = res_;
        RewardID = rewardid_;
    }
}

public delegate void OnClickedCard(MiniGameCard card_);

public static class Suffle
{
    public static void ShuffleArray<T>(T[] array)
    {
        int random1;
        int random2;

        T tmp;

        for (int index = 0; index < array.Length * 3; ++index)
        {
            random1 = UnityEngine.Random.Range(0, array.Length);
            random2 = UnityEngine.Random.Range(0, array.Length);

            tmp = array[random1];
            array[random1] = array[random2];
            array[random2] = tmp;
        }
    }

    public static void ShuffleList<T>(List<T> list)
    {
        int random1;
        int random2;

        T tmp;

        for (int index = 0; index < list.Count * 3; ++index)
        {
            random1 = UnityEngine.Random.Range(0, list.Count);
            random2 = UnityEngine.Random.Range(0, list.Count);

            tmp = list[random1];
            list[random1] = list[random2];
            list[random2] = tmp;
        }
    }
}

public class EventScriptData
{
    public readonly int ID;
    public readonly string EventName;
    public readonly string L_Res;
    public readonly string R_Res;
    public readonly string CharName;
    public readonly string Script;

    public EventScriptData(int id_, string eventname_, string l_res_, string r_res_, string charname_, string script_)
    {
        ID = id_;
        EventName = eventname_;
        L_Res = l_res_;
        R_Res = r_res_;
        CharName = charname_;
        Script = script_;
    }
}

public class TalkBoxText
{
    public readonly int ID;
    public readonly int Size;
    public readonly string Text;

    public TalkBoxText(int id_, int size_, string text_)
    {
        ID = id_;
        Size = size_;
        Text = text_;
    }
}

