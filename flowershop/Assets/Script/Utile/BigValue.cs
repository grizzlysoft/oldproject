﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Keiwando.BigInteger;
using System.Text;

public static class NumericalFormatter
{
    static List<string> _hackedValues = new List<string>();

    public static string Format(BigInteger number)
    {
        return FormatNumberString(number.ToString());
    }

    static string FormatNumberString(string number)
    {
        if (number.Length < 5)
        {
            return number;
        }

        if (number.Length < 7)
        {
            return FormatThousands(number);
        }

        return FormatGeneral(number);
    }

    static string FormatThousands(string number)
    {
        string leadingNumbers = number.Substring(0, number.Length - 3);
        string decimals = number.Substring(number.Length - 3);

        return CreateNumericalFormat(leadingNumbers, decimals, "K");
    }

    static string CreateNumericalFormat(string leadingNumbers, string decimals, string suffix)
    {
        return String.Format("{0}.{1}{2}", leadingNumbers, decimals, suffix);
    }

    static string FormatGeneral(string number)
    {
        int amountOfLeadingNumbers = (number.Length - 7) % 3 + 1;
        string leadingNumbers = number.Substring(0, amountOfLeadingNumbers);
        string decimals = number.Substring(amountOfLeadingNumbers, 3);

        return CreateNumericalFormat(leadingNumbers, decimals, GetSuffixForNumber(number));
    }

    static string GetSuffixForNumber(string number)
    {
        int numberOfThousands = (number.Length - 1) / 3;

        switch (numberOfThousands)
        {
            case 1:
                return "K";
            case 2:
                return "M";
            case 3:
                return "B";
            case 4:
                return "T";
            case 5:
                return "Q";
            default:
                return GetProceduralSuffix(numberOfThousands - 5);
        }
    }

    static string GetProceduralSuffix(int value)
    {
        if (_hackedValues.Count == 0)
        {
            PrecomputeValues();
        }

        return _hackedValues[value];
    }

    public static void Reverse(this StringBuilder sb)
    {
        for (int i = 0, j = sb.Length - 1; i < sb.Length / 2; i++, j--)
        {
            char chT = sb[i];

            sb[i] = sb[j];
            sb[j] = chT;
        }
    }

    static void PrecomputeValues()
    {
        // 531441 = 27 ^ 4, i.e. the first 5-digit base 27 number.
        // That's a large enough number to ensure that the output
        // include "aaaz", and indeed almost all of the 4-digit
        // base 27 numbers
        for (int i = 0; i < 531441; i++)
        {
            string text = ToBase27AlphaString(i);

            if (!text.Contains("`"))
            {
                _hackedValues.Add(text);
            }
        }
    }

    static string ToBase27AlphaString(int value)
    {
        StringBuilder sb = new StringBuilder();

        while (value > 0)
        {
            int digit = value % 27;

            sb.Append((char)('`' + digit));
            value /= 27;
        }

        if (sb.Length == 0)
        {
            sb.Append('`');
        }

        sb.Reverse();
        return sb.ToString();
    }



}
