﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enum
{
    public enum SceneType
    {
        Start,
        Title,
        Main,
        Event,
    }

    public enum Shop_PayType
    {
        Cash = 1,
        Crystal = 2,
        Gold = 3,
        Ads_AdMob = 4,
        Ads_Unity = 5,
    }

    public enum RewardDataType
    {
        Gem = 1,
        Gold,
    }

    public enum MessagePopupType
    {
        None,
        Ok,
        Ok_Cancel,
        Loading,
    }

    public enum Bgm
    {
        Like_a_Flower,
        Max,
    }

    public enum Sfx
    {
        card_se,
        click_se,
        combo_se,
        sell_se,
        workComplete_se,
        Max,
    }

    public enum MainWinMove
    {
        Up_in,
        Up_out,
        Right_in,
        Right_out,
        Left_in,
        Left_out,
        Down_in,
        Down_out,
        Center,
    }

    public enum WindowType
    {
        Main,
        Work,
        SubWork,
        Box,
        Shop,
        Attribute,
        DressRoom,
    }

    public enum WorkMenuState
    {
        Menu,
        FlowerList,
        Boquet,
        Basket,
        Max,
    }

    public enum BoxTab
    {
        Flower,
        Boquet,
        Basket,
        UseItem,
        Max,
    }

    public enum AttributeType
    {
        Lend,
        Work,
        InvenSlot,
        Sell,
        SubWork,
        Insentive,
        Max,
    }

    public enum ShopTapType
    {
        Crystal,
        Gold,
        Item,
        Ex,
        Package,
        Max,
    }

    public enum RewardType
    {
        Crystal = 1,
        Gold,
        Item,
        Heart,
        Max,
    }

    public enum GoodsMessageType
    {
        Crystal,
        Gold,
        Heart,
        Item,
    }

    public enum CharacterRes
    {
        Character_Normal,
        Character_CloseEyes,
        Max,
    }

    public enum AchievementType
    {
        MakeFlower,
        MakeBoquet,
        MakeBasket,
        AllSubWorkUnlock,
        AllAttributeUnLock,
        Sell,
        ShowAd,
        UseBuff_Flower,
        UseBuff_Glove,
        AllPackageBuy,
        NomarlQuestClear,
        Max,
    }

    public enum QuestType
    {
        Nomral,
        Loop,
        Event,
        Max,
    }

    public enum AttributeCostType
    {
        Crystal,
        Gold,
    }

    public enum IconTextType
    {
        Crystal,
        Gold,
        Heart,
        Item,
    }

    public enum MainMenuBtn
    {
        Work,
        SubWork,
        Reinforce,
        Box,
        DressRoom,
        Shop,
        Max,
    }

    public enum ADShowType
    {
        OffLine,
        Shop,
        MiniGame_Card,
        MiniGame_WhereHouse,
        ShowCombo,
        RewardDouble,
        Crystal_One,
    }

    public enum ClothesType
    {
        Basic,
        Crystal,
        Gold,
        Heart,
        Max,
    }

    public enum MiniGmaeCardType
    {
        None,
        castle_gate,
        cyclamen_basket,
        daisy,
        desk,
        forget_me_not_bouquet,
        fountain,
        knightage,
        lavender_basket,
        set_bouquet_b,
        town_festival,
        tulip_basket,
        royal_festival,
        Event_textend,
        flower_arrangement,
        edelweiss,
    }

    public enum WhereHouseIconType
    {
        carnation,
        crocus,
        cyclamen,
        daffodil,
        daisy,
        edelweiss,
        forget_me_not,
        lavender,
        lilac,
        tulip,
        Max,
    }
}
