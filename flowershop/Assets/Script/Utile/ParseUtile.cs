﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParseUtile
{
    public List<int> StringPareser(string s)
    {
        List<int> returndata = new List<int>();

        string[] temp = s.Split(',');

        //Debug.Log(temp);

        for(int i = 0; i < temp.Length; ++i)
        {
            returndata.Add(int.Parse(temp[i].ToString()));
            //Debug.Log("return data[" + i + "] = " + returndata[i]);
        }

        return returndata;
    }

    public List<float> StringPareserByFloat(string s)
    {
        List<float> returndata = new List<float>();

        string temp = s.Replace(",", "");

        //Debug.Log(temp);

        for (int i = 0; i < temp.Length; ++i)
        {
            string fs = string.Empty;
            fs += temp[i];
            if(i + 1 < temp.Length)
            {
                if(temp[i + 1] == '.')
                {
                    fs += temp[i + 1].ToString() + temp[i + 2].ToString();
                    i += 2;
                }
            }
            //Debug.Log("fs = " + fs);
            returndata.Add(float.Parse(fs.ToString()));
            //Debug.Log("return data[" + i + "] = " + returndata[i]);
        }

        return returndata;
    }
}
