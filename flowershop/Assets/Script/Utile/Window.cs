﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class Window : MonoBehaviour
{
    public UISprite BackGraund;
    public Animation WindowAni;
    [HideInInspector]
    public WindowType WinType;
    
    private void Start()
    {
        WindowInit();
    }

    public virtual void WindowInit() { }
    public virtual void OpenWindow() { TextSetting(); }
    public virtual void CloseWindow() { }
    public virtual void OpenAniEnd() { MainUIManager.Get.WinState = WinType; }
    public virtual void CloseAniStart() { }
    public virtual void TextSetting() { }
    public virtual void ReFreshWindow() { }
}
