﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class Popup_Message : Popup
{
    public UILabel Text;
    public UILabel Ok_Text;
    public UILabel Cancel_Text;
    public UIEventTrigger Btn_Ok;
    public UIEventTrigger Btn_Cancel;
    
    onBtnClicked Ok = null;
    onBtnClicked Cancel = null;

    Message MessageData = null;

    public override void FirstLoad()
    {
        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        MessageData = data_ as Message;
        base.SetData(data_);
    }

    public override void Open(bool back = true)
    {
        switch(MessageData.Type)
        {
            case MessagePopupType.None:
            case MessagePopupType.Loading:
                Btn_Ok.gameObject.SetActive(false);
                Btn_Cancel.gameObject.SetActive(false);
                break;
            case MessagePopupType.Ok:
                Btn_Ok.gameObject.transform.localPosition = new Vector3(0f, Btn_Ok.transform.localPosition.y, 0f);
                Btn_Ok.gameObject.SetActive(true);
                Btn_Cancel.gameObject.SetActive(false);
                break;
            case MessagePopupType.Ok_Cancel:
                Btn_Ok.gameObject.SetActive(true);
                Btn_Cancel.gameObject.SetActive(true);
                Btn_Ok.gameObject.transform.localPosition = new Vector3(-(Btn_Cancel.transform.localPosition.x), Btn_Ok.transform.localPosition.y, 0f);
                break;
        }

        Text.text = MessageData.Text;
        Ok_Text.text = MessageData.OkText;
        Cancel_Text.text = MessageData.CancelText;

        if (null != MessageData.Ok)
            Ok = MessageData.Ok;

        if (null != MessageData.Cancel)
            Cancel = MessageData.Cancel;

        base.Open(back);
    }

    public void OnClickedOk()
    {
        if (Ok == null)
        {
            CloseMessagePopup();
            return;
        }

        Ok();
        //CloseMessagePopup();
    }

    public void OnClickedCancel()
    {
        if (Cancel == null)
        {
            CloseMessagePopup();
            return;
        }
        Cancel();
        //CloseMessagePopup();
    }

    void CloseMessagePopup()
    {
        PopupManager.Get.ClosePopup("Popup_Message");
    }

    public override void Close()
    {
        MessageData = null;
        Ok = null;
        Cancel = null;
        //Debug.Log("message popup close");
        base.Close();
    }
}
