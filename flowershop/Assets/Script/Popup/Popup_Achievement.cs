﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class Popup_Achievement : Popup
{
    public UIScrollView Scroll;
    public UIGrid Grid;

    List<Achievement_ScrollItem> ScrollItem = new List<Achievement_ScrollItem>();

    public override void FirstLoad()
    {
        for(int i = 0; i < (int)AchievementType.Max; ++i)
        {
            GameObject o = GameObject.Instantiate(Resources.Load("Prefab/Popup/Achievement_ScrollItem")) as GameObject;

            o.transform.parent = Grid.transform;
            o.transform.localScale = Vector3.one;
            o.GetComponent<Achievement_ScrollItem>().SetData(DataManager.Get.GetAchievementData(AchievementManager.Get.GetNowAchievement((AchievementType)i)));
            ScrollItem.Add(o.GetComponent<Achievement_ScrollItem>());
        }

        Grid.Reposition();

        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        base.SetData(data_);
    }

    public override void Open(bool back = true)
    {
        for(int i = 0; i < ScrollItem.Count; ++i)
        {
            ScrollItem[i].RefreshInfo();
        }

        Scroll.GetComponent<UIPanel>().depth = this.GetComponent<UIPanel>().depth + 1;

        base.Open(back);

        Scroll.ResetPosition();
    }

    public override void SetText()
    {
        base.SetText();
    }

    public void OnClickedClose()
    {
        PopupManager.Get.ClosePopup("Popup_Achievement");
    }

    public void OnClickedGoogle()
    {
        GPGS_Manager.Get.ShowAchievement();
    }

    public void OnClickedAllComplete()
    {
        AchievementManager.Get.AllComplete();
        for (int i = 0; i < ScrollItem.Count; ++i)
        {
            ScrollItem[i].ReFresh();
        }
    }

    public override void Close()
    {
        base.Close();
    }
}
