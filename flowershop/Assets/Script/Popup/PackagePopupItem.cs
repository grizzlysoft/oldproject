﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PackagePopupItem : MonoBehaviour
{
    public UILabel PriceText;
    public UILabel BuyText;
    public UILabel CostText;
    public UIGrid Grid;
    public IconText_Item[] IconTextItems = new IconText_Item[4];

    ShopData Data;

    public void SetData(ShopData data_)
    {
        Data = data_;

        Refresh();
    }

    public void Refresh()
    {
        PriceText.text = Data.Text.Replace("\\n", "\n");
        BuyText.text = "구매하기";
        CostText.text = "￦" + string.Format("{0:##,##0}", Data.Cost);

        for(int i = 0; i < IconTextItems.Length; ++i)
        {
            IconTextData data = DataManager.Get.GetRewardData(Data.RewardID[i]).ConvertIconText();
            IconTextItems[i].SetData(data.Type, data.Count, data.ItemID);
        }
    }

    public void OnClickedBuyBtn()
    {
        Debug.Log(Data.StoreID);
        if (PlayerPrefs.GetInt(Data.StoreID) == 1)
        {
            Message message = new Message(null, null, "이미 구매한 상품입니다.", "확인", string.Empty);
            PopupManager.Get.OpenPopup("Popup_Message", message);
        }
        else
        {
            Message message = new Message(ConnectPaymentManager, null, Data.Text.Replace("\\n", "\n") + "\n구매하시겠습니까?", "구매", "취소", Enum.MessagePopupType.Ok_Cancel);
            PopupManager.Get.OpenPopup("Popup_Message", message);
        }
    }

    public void ConnectPaymentManager()
    {
        PopupManager.Get.ClosePopup("Popup_Message");
        PaymentManager.Get.BuyPrice(Data);
    }
}
