﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_BuyBuffItem : Popup
{
    public UILabel Text;
    public UIButton Btn_Close;
    public UIButton Btn_Buy1;
    public UIButton Btn_Buy11;

    public UILabel Buy1Cost;
    public UILabel Buy11Cost;

    public UISprite Buy1CostIcon;
    public UISprite Buy11CostIcon;

    int ItemID = -1;

    ShopData Buy1Data = null;
    ShopData Buy11Data = null;

    public override void FirstLoad()
    {
        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        ItemID = (int)data_;
        base.SetData(data_);
    }

    public override void Open(bool back = true)
    {
        if (ItemID == 37)
        {
            Buy1Data = DataManager.Get.GetShopData(13);
            Buy11Data = DataManager.Get.GetShopData(14);
        }
        else if(ItemID == 38)
        {
            Buy1Data = DataManager.Get.GetShopData(17);
            Buy11Data = DataManager.Get.GetShopData(18);
        }

        Text.text = DataManager.Get.GetItemData(ItemID).Name + "(이)가 부족합니다.\n구매 하시겠습니까?";
        
        Buy1Cost.text = string.Format("{0:##,##0}", Buy1Data.Cost);
        Buy11Cost.text = string.Format("{0:##,##0}", Buy11Data.Cost);

        if (Buy1Data.PayType == Enum.Shop_PayType.Crystal)
            Buy1CostIcon.spriteName = "Crystal_Icon";
        else if(Buy1Data.PayType == Enum.Shop_PayType.Gold)
            Buy1CostIcon.spriteName = "Gold_Icon";

        if (Buy11Data.PayType == Enum.Shop_PayType.Crystal)
            Buy11CostIcon.spriteName = "Crystal_Icon";
        else if (Buy11Data.PayType == Enum.Shop_PayType.Gold)
            Buy11CostIcon.spriteName = "Gold_Icon";

        base.Open(back);
    }

    public void OnClickedBtnBuy1()
    {
        if (Buy1Data.PayType == Enum.Shop_PayType.Gold)
        {
            if (UserDataManager.Get.UseGold(Buy1Data.Cost))
            {
                PaymentManager.Get.BuyPrice(Buy1Data);
            }
            else
            {
                Message message = new Message(MainUIManager.Get.MoveShopGoldTapDelegate, null, "골드가 부족합니다.\n상점으로 이동하시겠습니까?", "확인", "취소", Enum.MessagePopupType.Ok_Cancel);
            }
        }
        else if(Buy1Data.PayType == Enum.Shop_PayType.Crystal)
        {
            if (UserDataManager.Get.UseCrystal(Buy1Data.Cost))
            {
                PaymentManager.Get.BuyPrice(Buy1Data);
            }
            else
            {
                Message message = new Message(MainUIManager.Get.MoveShopCrystalTapDelegate, null, "크리스탈이 부족합니다.\n상점으로 이동하시겠습니까?", "확인", "취소", Enum.MessagePopupType.Ok_Cancel);
            }
        }
        PopupManager.Get.ClosePopup("Popup_BuyBuffItem");
    }

    public void OnClickedBtnBuy11()
    {
        if (Buy11Data.PayType == Enum.Shop_PayType.Gold)
        {
            if (UserDataManager.Get.UseGold(Buy11Data.Cost))
            {
                PaymentManager.Get.BuyPrice(Buy11Data);
            }
            else
            {
                Message message = new Message(MainUIManager.Get.MoveShopGoldTapDelegate, null, "골드가 부족합니다.\n상점으로 이동하시겠습니까?", "확인", "취소", Enum.MessagePopupType.Ok_Cancel);
            }
        }
        else if (Buy11Data.PayType == Enum.Shop_PayType.Crystal)
        {
            if (UserDataManager.Get.UseCrystal(Buy11Data.Cost))
            {
                PaymentManager.Get.BuyPrice(Buy11Data);
            }
            else
            {
                Message message = new Message(MainUIManager.Get.MoveShopCrystalTapDelegate, null, "크리스탈이 부족합니다.\n상점으로 이동하시겠습니까?", "확인", "취소", Enum.MessagePopupType.Ok_Cancel);
            }
        }

        PopupManager.Get.ClosePopup("Popup_BuyBuffItem");
    }

    public void OnClickedBtnClose()
    {
        PopupManager.Get.ClosePopup("Popup_BuyBuffItem");
    }

    public override void Close()
    {
        MainUIManager.Get.GetWindow(Enum.WindowType.Work).GetComponent<WorkWindow>().BuffItemRefrash();
        base.Close();
    }


}
