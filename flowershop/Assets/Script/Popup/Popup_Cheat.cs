﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Cheat : Popup
{
    public UIInput Input;

    public override void FirstLoad()
    {
        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        base.SetData(data_);
    }

    public override void Open(bool back = true)
    {
        base.Open(back);
    }

    public override void SetText()
    {
        base.SetText();
    }

    public override void Close()
    {
        Input.value = "0";
        base.Close();
    }

    public void OnClickedClose()
    {
        PopupManager.Get.ClosePopup("Popup_Cheat");
    }

    public void OnClickedGoldBtn()
    {
        if (double.Parse(Input.value) > 0)
            UserDataManager.Get.AddGold(double.Parse(Input.value));

        PopupManager.Get.ClosePopup("Popup_Cheat");
    }

    public void OnClickedCrystalBtn()
    {
        if (int.Parse(Input.value) > 0)
            UserDataManager.Get.AddCrystal(int.Parse(Input.value));

        PopupManager.Get.ClosePopup("Popup_Cheat");
    }

    public void OnClickedHeartBtn()
    {
        if (int.Parse(Input.value) > 0)
            UserDataManager.Get.AddHeart(int.Parse(Input.value));

        PopupManager.Get.ClosePopup("Popup_Cheat");
    }

}
