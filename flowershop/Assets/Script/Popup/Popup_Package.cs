﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Package : Popup
{
    public UIGrid Grid;

    List<PackagePopupItem> PackageItem = new List<PackagePopupItem>();

    public override void FirstLoad()
    {
        List<ShopData> packagelist = DataManager.Get.GetShopDataList(Enum.ShopTapType.Package);

        for(int i = 0; i < packagelist.Count; ++i)
        {
            PackagePopupItem item = (GameObject.Instantiate(Resources.Load("Prefab/Popup/Package_PopupItem")) as GameObject).GetComponent<PackagePopupItem>();
            item.transform.parent = Grid.transform;
            item.transform.localScale = Vector3.one;
            item.transform.localPosition = Vector3.zero;
            item.SetData(packagelist[i]);
            PackageItem.Add(item);
        }
        Grid.Reposition();
        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        base.SetData(data_);
    }

    public override void Open(bool back = true)
    {
        for(int i = 0; i < PackageItem.Count; ++i)
        {
            PackageItem[i].Refresh();
        }
        base.Open(back);
    }

    public override void SetText()
    {
        base.SetText();
    }

    public void OnClickedCloseBtn()
    {
        PopupManager.Get.ClosePopup("Popup_Package");
    }

    public override void Refresh()
    {
        base.Refresh();
    }

    public override void Close()
    {
        base.Close();
    }
}
