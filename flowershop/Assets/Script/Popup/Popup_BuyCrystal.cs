﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_BuyCrystal : Popup
{
    public UILabel Text;

    public UIButton Btn_Left;
    public UILabel BtnLeft_Text;
    public UILabel LeftCost;
    public UISprite LeftIcon;

    public UIButton Btn_Right;
    public UILabel BtnRight_Text;
    public UILabel RightCost;
    public UISprite RightIcon;

    public UIButton BtnClose;

    ShopData AdsPrice;
    ShopData CashPrice;

    int NeedCrystal = 0;

    public override void FirstLoad()
    {
        AdsPrice = DataManager.Get.GetShopData(1);
        CashPrice = DataManager.Get.GetShopData(2);
        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        NeedCrystal = (int)data_;
        base.SetData(data_);
    }

    public override void Open(bool back = true)
    {
        LeftCost.text = "+ " + 1;

        if(NeedCrystal < 11)
        {
            CashPrice = DataManager.Get.GetShopData(24);
        }
        else if(NeedCrystal < 31)
        {
            CashPrice = DataManager.Get.GetShopData(25);
        }
        else if(NeedCrystal < 51)
        {
            CashPrice = DataManager.Get.GetShopData(26);
        }
        else if(NeedCrystal < 101)
        {
            CashPrice = DataManager.Get.GetShopData(27);
        }
        else
        {
            CashPrice = DataManager.Get.GetShopData(28);
        }

        base.Open(back);

        SetText();
    }

    public override void SetText()
    {
        Text.text = "크리스탈이 부족합니다!\n지금 바로 구매하시겠습니까 ?";
        BtnLeft_Text.text = "광고보기";
        BtnRight_Text.text = CashPrice.Text;
        BtnRight_Text.text.Replace("\\n", "\n");
        RightCost.text = "￦" + string.Format("{0:##,##0}", CashPrice.Cost);
        base.SetText();
    }

    public void OnClickedBtnAds()
    {
        Ads_Manager.Get.ShowAd_UnityAds(Enum.ADShowType.Crystal_One);
        PopupManager.Get.ClosePopup("Popup_BuyCrystal");
    }

    public void OnClickedBtnCash()
    {
        PaymentManager.Get.BuyPrice(CashPrice);
        PopupManager.Get.ClosePopup("Popup_BuyCrystal");
    }

    public void OnClickedBtnClose()
    {
        PopupManager.Get.ClosePopup("Popup_BuyCrystal");
    }

    public override void Close()
    {
        base.Close();
    }
}
