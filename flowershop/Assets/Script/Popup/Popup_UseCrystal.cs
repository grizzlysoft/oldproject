﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class Popup_UseCrystal : Popup
{
    public UILabel Text;
    public UILabel Ok_Text;
    public UILabel Cancel_Text;
    public UIButton BtnOk;
    public UIButton BtnCancel;
    public IconText_Item IconText;

    onBtnClicked Ok = null;
    onBtnClicked Cancel = null;

    UseCrystalPopupData MessageData;

    public override void FirstLoad()
    {
        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        MessageData = data_ as UseCrystalPopupData;
        base.SetData(data_);
    }

    public override void Open(bool back = true)
    {
        switch (MessageData.BtnType)
        {
            case MessagePopupType.None:
            case MessagePopupType.Loading:
                BtnOk.gameObject.SetActive(false);
                BtnCancel.gameObject.SetActive(false);
                break;
            case MessagePopupType.Ok:
                BtnOk.gameObject.transform.localPosition = new Vector3(0f, BtnOk.transform.localPosition.y, 0f);
                BtnOk.gameObject.SetActive(true);
                BtnCancel.gameObject.SetActive(false);
                break;
            case MessagePopupType.Ok_Cancel:
                BtnOk.gameObject.SetActive(true);
                BtnCancel.gameObject.SetActive(true);
                BtnOk.gameObject.transform.localPosition = new Vector3(-(BtnCancel.transform.localPosition.x), BtnOk.transform.localPosition.y, 0f);
                break;
        }

        Text.text = MessageData.Text;
        Ok_Text.text = MessageData.OkText;
        Cancel_Text.text = MessageData.CancelText;

        IconText.SetData(MessageData.iconTextData.Type, MessageData.iconTextData.Count, MessageData.iconTextData.ItemID);

        if (null != MessageData.Ok)
            Ok = MessageData.Ok;

        if (null != MessageData.Cancel)
            Cancel = MessageData.Cancel;

        base.Open(back);
    }

    public void OnClickedOk()
    {
        if (Ok == null)
        {
            ClosePopup();
            return;
        }

        Ok();
    }

    public void OnClickedCancel()
    {
        if (Cancel == null)
        {
            ClosePopup();
            return;
        }
        Cancel();
    }

    void ClosePopup()
    {
        PopupManager.Get.ClosePopup("Popup_UesCrystal");
    }

    public override void SetText()
    {
        base.SetText();
    }

    public override void Close()
    {
        Ok = null;
        Cancel = null;
        MessageData = null;
        base.Close();
    }
}
