﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_ShowAd : Popup
{
    public UIGrid Grid;
    public ShowAD_Item[] ShowAdsItem = new ShowAD_Item[5];
    public UILabel ShowAdLabel_1;
    public UILabel ShowAdLabel_2;

    public override void FirstLoad()
    {
        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        base.SetData(data_);
    }

    public override void Open(bool back = true)
    {
        for (int i = 0; i < ShowAdsItem.Length; ++i)
        {
            ShowAdsItem[i].SetData(DataManager.Get.GetShowADsData(i + 1));
        }

        if (UserDataManager.Get.GetShowAdsCount() <= 4)
        {
            ShowAdLabel_1.gameObject.SetActive(true);
            ShowAdLabel_2.gameObject.SetActive(false);
        }
        else
        {
            ShowAdLabel_1.gameObject.SetActive(false);
            ShowAdLabel_2.gameObject.SetActive(true);
        }
        base.Open(back);
    }

    public override void SetText()
    {
        base.SetText();
    }

    public void OnClickedBtnClose()
    {
        PopupManager.Get.ClosePopup("Popup_ShowAd");
    }

    public void OnClickedShowAds()
    {
        Ads_Manager.Get.ShowAd_UnityAds();
    }

    public override void Refresh()
    {
        for (int i = 0; i < ShowAdsItem.Length; ++i)
        {
            ShowAdsItem[i].SetData(DataManager.Get.GetShowADsData(i + 1));
        }

        if (UserDataManager.Get.GetShowAdsCount() <= 4)
        {
            ShowAdLabel_1.gameObject.SetActive(true);
            ShowAdLabel_2.gameObject.SetActive(false);
        }
        else
        {
            ShowAdLabel_1.gameObject.SetActive(false);
            ShowAdLabel_2.gameObject.SetActive(true);
        }
        base.Refresh();
    }

    public override void Close()
    {
        base.Close();
    }
}
