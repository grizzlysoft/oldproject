﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class Popup_WhereHouseGame : Popup
{
    public GameObject BlockObj;
    public UILabel StartText;
    public UILabel TimeText;
    public UILabel Time;
    public UIGrid Grid;
    public UILabel ComboText;

    public UISprite[] CenterItems = new UISprite[6];
    public UISprite LeftItem;
    public UISprite RightItem;
    public UISprite SelectItem;

    int MaxCombo = 0;
    bool GameStart = false;
    int Score = 0;
    float PlayTime = 60f;
    int Reward = 0;

    //0 = left, 1, right
    WhereHouseIconType[] IconType = { WhereHouseIconType.carnation, WhereHouseIconType.daisy };

    public override void FirstLoad()
    {
        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        base.SetData(data_);
    }

    public override void Open(bool back = true)
    {
        BlockObj.SetActive(true);
        while (true)
        {
            IconType[0] = (WhereHouseIconType)Random.Range(0, (int)WhereHouseIconType.Max);
            IconType[1] = (WhereHouseIconType)Random.Range(0, (int)WhereHouseIconType.Max);

            if (IconType[0] != IconType[1])
                break;
        }

        for(int i = 0; i < CenterItems.Length; ++i)
        {
            CenterItems[i].spriteName = IconType[Random.Range(0, 2)].ToString();
        }

        RightItem.spriteName = IconType[1].ToString();
        LeftItem.spriteName = IconType[0].ToString();

        SelectItem.spriteName = "empty";

        base.Open(back);
    }

    public override void SetText()
    {
        base.SetText();
    }

    void ProgressUp()
    {
        SelectItem.spriteName = CenterItems[0].spriteName;

        CenterItems[0].spriteName = CenterItems[1].spriteName;
        CenterItems[1].spriteName = CenterItems[2].spriteName;
        CenterItems[2].spriteName = CenterItems[3].spriteName;
        CenterItems[3].spriteName = CenterItems[4].spriteName;
        CenterItems[4].spriteName = IconType[Random.Range(0, 2)].ToString();
    }

    void GameEnd()
    {
        Reward = MiniGameManager.Get.CalculateReward_WhereHouse(MaxCombo, Score);
        Score = 0;
        MaxCombo = 0;
        UseCrystalPopupData message = new UseCrystalPopupData("게임이 종료되었습니다.\n(광고시청시 보상2배)", MessagePopupType.Ok_Cancel,
                  new IconTextData(IconTextType.Gold, Reward, -1), "확인", "광고보기", OnClickedOfflineOkBtn, OnClickedOfflineShowAD);

        PopupManager.Get.OpenPopup("Popup_UesCrystal", message, false);
    }

    void OnClickedOfflineOkBtn()
    {
        UserDataManager.Get.AddGold(Reward);
        PopupManager.Get.ClosePopup("Popup_UesCrystal");
        PopupManager.Get.ClosePopup("Popup_WhereHouseGame");
    }

    void OnClickedOfflineShowAD()
    {
        Ads_Manager.Get.ShowAd_UnityAds(ADShowType.RewardDouble, Reward);
    }

    public void OnClickedStart()
    {
        BlockObj.SetActive(false);
        Score = 0;
        MaxCombo = 0;
        PlayTime = 60f;
        ProgressUp();
        GameStart = true;
        MiniGameManager.Get.WhereHouse_CoolTime = System.DateTime.Now.AddHours(1);
    }

    public void OnClickedClose()
    {
        PopupManager.Get.ClosePopup("Popup_WhereHouseGame");
    }

    void ComboTextHide()
    {
        ComboText.gameObject.SetActive(false);
    }

    public void OnClickedArrow_Left()
    {
        CancelInvoke("ComboTextHide");
        if(LeftItem.spriteName == SelectItem.spriteName)
        {
            //맞춘것
            MaxCombo++;
            if (MaxCombo > 1)
                ComboText.text = MaxCombo + "c";
            else
                ComboText.text = string.Empty;

            SoundManager.Get.PlaySfx(Sfx.combo_se);

            Score++;
        }
        else
        {
            MaxCombo = 0;
            ComboText.text = "m";
        }
        ComboText.gameObject.SetActive(true);
        Invoke("ComboTextHide", 0.5f);

        ProgressUp();
    }

    public void OnClickedArrow_Right()
    {
        CancelInvoke("ComboTextHide");
        if (RightItem.spriteName == SelectItem.spriteName)
        {
            //맞춘것
            MaxCombo++;
            if (MaxCombo > 1)
                ComboText.text = MaxCombo + "c";
            else
                ComboText.text = string.Empty;

            Score++;
        }
        else
        {
            MaxCombo = 0;
            ComboText.text = "m";
        }
        ComboText.gameObject.SetActive(true);
        Invoke("ComboTextHide", 0.5f);

        ProgressUp();
    }

    public override void Refresh()
    {
        base.Refresh();
    }

    public override void Close()
    {
        base.Close();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameStart)
        {
            PlayTime -= RealTime.deltaTime;
            Time.text = ((int)PlayTime).ToString();

            if (PlayTime <= 0f)
            {
                GameStart = false;
                Invoke("GameEnd", 0.5f);
            }
        }
    }

    private void OnApplicationPause(bool pause)
    {
        if(pause)
        {
            GameStart = false;
        }
        else
        {
            if (BlockObj.activeInHierarchy == false)
                GameStart = true;
        }
    }
}
