﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowAD_Item : MonoBehaviour
{
    public UISprite Icon;
    public GameObject CheckObj;
    public UILabel CountText;
    public UILabel RewardText;

    ShowAdsData Data;

    public void SetData(ShowAdsData data_)
    {
        Data = data_;
        SetInfo();
    }

    public void SetInfo()
    {
        Icon.spriteName = Data.Res;
        CountText.text = Data.Count + "회차";

        if (Data.Count <= UserDataManager.Get.GetShowAdsCount())
            CheckObj.SetActive(true);
        else
            CheckObj.SetActive(false);

        RewardData r = DataManager.Get.GetRewardData(Data.RewardID);
        switch(r.Type)
        {
            case Enum.RewardType.Crystal:
                RewardText.text = "크리스탈 [FFFF00]" + r.Value + "[FFFFFF]개";
                break;
            case Enum.RewardType.Gold:
                RewardText.text = "[FFFF00]" + r.Value + "[FFFFFF]골드";
                break;
            case Enum.RewardType.Heart:
                RewardText.text = "[FFFF00]" + r.Value + "[FFFFFF]";
                break;
            case Enum.RewardType.Item:
                RewardText.text = "[FFFF00]" + DataManager.Get.GetItemData(r.ItemID).Name + "[FFFFFF]" + r.Value + "개";
                break;
            default:
                RewardText.text = Data.Name;
                break;
        }
    }
}
