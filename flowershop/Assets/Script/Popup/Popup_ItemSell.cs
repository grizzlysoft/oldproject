﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class Popup_ItemSell : Popup
{
    public UILabel ItemName;
    public UILabel ItemText;
    public UILabel ItemCount;
    public UILabel ItemSellPrice;

    public UIInput SellCountInput;

    public UISprite ItemIcon;

    public UIButton Btn_Up;
    public UIButton Btn_Down;
    public UIButton Btn_All;
    public UIButton Btn_Sell;

    public UIButton Btn_Close;

    int Itemid = -1;
    ItemData ItemData = null;
    int totalcount = 0;
    int SellCount = 0;

    public override void FirstLoad()
    {
        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        Itemid = (int)data_;
        ItemData = DataManager.Get.GetItemData(Itemid);
        totalcount = InventoryManager.Get.GetItemCount(Itemid);
        base.SetData(data_);
    }

    public override void Open(bool back = true)
    {
        ItemName.text = ItemData.Name;
        ItemCount.text = string.Format("보유 개수 : {0}", totalcount);
        ItemSellPrice.text = string.Format("개당 가격 : {0}", (int)(ItemData.Sell + (ItemData.Sell * (DataManager.Get.GetAttributeData(AttributeType.Sell,
            UserDataManager.Get.GetAttributeLevel(AttributeType.Sell)).Abillity * 0.01))));

        ItemIcon.spriteName = ItemData.Res;
        ItemText.text = ItemData.Text;
        SellCountInput.value = "0";

        base.Open(back);
    }

    public override void Close()
    {
        SellCount = 0;
        base.Close();
    }

    public void OnChangeInput()
    {
        if (int.Parse(SellCountInput.value) > totalcount)
            SellCountInput.value = totalcount.ToString();

        SellCount = int.Parse(SellCountInput.value);
    }

    public void OnClickedBtn_Up()
    {
        SellCount++;

        if (SellCount > totalcount)
            SellCount = totalcount;

        SellCountInput.value = SellCount.ToString();
    }

    public void OnClickedBtn_Down()
    {
        SellCount--;

        if (SellCount < 0)
            SellCount = 0;

        SellCountInput.value = SellCount.ToString();
    }

    public void OnClickedBtn_All()
    {
        SellCount = totalcount;

        SellCountInput.value = SellCount.ToString();
    }

    public void OnClickedBtn_Sell()
    {
        if(SellCount == 0)
        {
            PopupManager.Get.ClosePopup("Popup_ItemSell");
        }
        else if (InventoryManager.Get.UseItem(Itemid, SellCount))
        {
            int sell = (int)(ItemData.Sell + (ItemData.Sell * (DataManager.Get.GetAttributeData(AttributeType.Sell,
            UserDataManager.Get.GetAttributeLevel(AttributeType.Sell)).Abillity * 0.01))) * SellCount;
            UserDataManager.Get.AddGold(sell);
            MainUIManager.Get.GetWindow(WindowType.Box).GetComponent<BoxWindow>().ScrollSetting((BoxTab)ItemData.Type);
            MainUIManager.Get.GetWindow(WindowType.Box).GetComponent<BoxWindow>().ScrollPostionReset((BoxTab)ItemData.Type);
            Message message = new Message(null, null, "판매가 완료되었습니다.\n총 판매액 : " + sell + "G", "확인", string.Empty, MessagePopupType.Ok);
            PopupManager.Get.OpenPopup("Popup_Message", message, true);

            AchievementManager.Get.AchievementStepUp(AchievementType.Sell, sell);

            PopupManager.Get.ClosePopup("Popup_ItemSell");
            SoundManager.Get.PlaySfx(Sfx.sell_se);
        }
        else
        {
            Message message = new Message(null, null, "아이탬의 수량이 다릅니다", "확인", string.Empty, MessagePopupType.Ok);
            PopupManager.Get.OpenPopup("Popup_Message", message, true);
            PopupManager.Get.ClosePopup("Popup_ItemSell");
            //애러 팝업
        }
    }

    public void OnClickedBtn_Close()
    {
        PopupManager.Get.ClosePopup("Popup_ItemSell");
    }
}
