﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_CardGameExplanation : Popup
{
    public UILabel BestTime;

    public override void FirstLoad()
    {
        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        base.SetData(data_);
    }

    public override void Open(bool back = true)
    {
        BestTime.text = "현재 최고 점수 : [FFFF00]" + (int)MiniGameManager.Get.Card_Best;
        base.Open(back);
    }

    public override void SetText()
    {
        base.SetText();
    }

    public override void Refresh()
    {
        base.Refresh();
    }

    public void OnClickedBtnStart()
    {
        PopupManager.Get.ClosePopup("Popup_CardGameExplanation");
        PopupManager.Get.OpenPopup("Popup_CardGame", null, false);
    }

    public void OnClickedClose()
    {
        PopupManager.Get.ClosePopup("Popup_CardGameExplanation");
    }

    public override void Close()
    {
        base.Close();
    }

}
