﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Creadit : Popup
{
    public UIPanel TextPanel;
    public UILabel Text;
    public TweenPosition tween;

    public override void FirstLoad()
    {
        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        base.SetData(data_);
    }

    public override void Open(bool back = true)
    {
        TextPanel.depth = GetComponent<UIPanel>().depth + 1;
        tween.from = new Vector3(0f, Text.transform.localPosition.y, 0f);
        tween.to = new Vector3(0f, Text.transform.localPosition.y + (Text.height / 2), 0f);
        tween.PlayForward();
        base.Open(back);
    }

    public override void SetText()
    {
        base.SetText();
    }

    public void OnClickedBtnClose()
    {
        PopupManager.Get.ClosePopup("Popup_Creadit");
    }

    public override void Close()
    {
        tween.ResetToBeginning();
        base.Close();
    }

}
