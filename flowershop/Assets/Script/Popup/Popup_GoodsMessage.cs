﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class Popup_GoodsMessage : Popup
{
    public UILabel Text;

    public UIScrollView Scroll;
    public UIGrid Grid;

    public UIButton Btn_Ok;
    public UILabel Btn_OkText;
    public UIButton Btn_Cancel;
    public UILabel Btn_CancelText;

    GoodsMessage Data = null;

    List<IconText_Item> IconText_Items = new List<IconText_Item>();

    public override void FirstLoad()
    {
        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        Data = data_ as GoodsMessage;
        base.SetData(data_);
    }

    public override void Open(bool back = true)
    {
        Text.text = Data.Text;
        Btn_OkText.text = Data.Ok_Text;
        Btn_CancelText.text = Data.Cancel_Text;

        for (int i = 0; i < Data.IconTextDatas.Count; ++i)
        {
            if (IconText_Items.Count <= i)
            {
                IconText_Item item = (GameObject.Instantiate(Resources.Load("Prefab/Popup/IconText_Item")) as GameObject).GetComponent<IconText_Item>();
                item.transform.parent = Grid.transform;
                item.transform.localScale = Vector3.one;
                item.SetData(Data.IconTextDatas[i].Type, Data.IconTextDatas[i].Count, Data.IconTextDatas[i].ItemID);

                IconText_Items.Add(item);
            }
            else
            {
                IconText_Items[i].SetData(Data.IconTextDatas[i].Type, Data.IconTextDatas[i].Count, Data.IconTextDatas[i].ItemID);
                IconText_Items[i].OnOff(true);
            }
        }

        Grid.Reposition();
        Scroll.ResetPosition();

        switch (Data.BtnType)
        {
            case MessagePopupType.Ok:
                Btn_Cancel.gameObject.SetActive(false);
                Btn_Ok.transform.localPosition = new Vector3(0f, Btn_Ok.transform.localPosition.y, 0f);
                break;
            case MessagePopupType.Ok_Cancel:
                Btn_Cancel.gameObject.SetActive(true);
                Btn_Ok.transform.localPosition = new Vector3(-(Btn_Cancel.transform.localPosition.x), Btn_Ok.transform.localPosition.y, 0f);
                break;
        }

        Scroll.GetComponent<UIPanel>().depth = this.GetComponent<UIPanel>().depth + 1;

        base.Open(back);
    }

    void CloseDelegate()
    {
        PopupManager.Get.ClosePopup("Popup_GoodsMessage");
    }

    public void OnOkClicked()
    {
        if (Data.Ok == null)
            CloseDelegate();
        else
        {
            Data.Ok();
            CloseDelegate();
        }
    }

    public void OnCancelClicked()
    {
        if (Data.Cancel == null)
            CloseDelegate();
        else
        {
            Data.Cancel();
            CloseDelegate();
        }
    }

    public override void SetText()
    {
        base.SetText();
    }

    public override void Close()
    {
        Data = null;
        for(int i = 0; i < IconText_Items.Count; ++i)
        {
            IconText_Items[i].OnOff(false);
        }
        base.Close();
    }
}
