﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Coupon : Popup
{
    public UIInput CouponInput;
    public UILabel BtnText;

    public override void FirstLoad()
    {
        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        base.SetData(data_);
    }

    public override void Open(bool back = true)
    {
        base.Open(back);
    }

    public void OnClickedCouPonBtn()
    {
        CouponManager.Get.UseHungryAppCoupon(CouponInput.value);
        PopupManager.Get.ClosePopup("Popup_Coupon");
    }

    public void OnClickedCloseBtn()
    {
        PopupManager.Get.ClosePopup("Popup_Coupon");
    }

    public override void SetText()
    {
        base.SetText();
    }

    public override void Close()
    {
        base.Close();
    }

}
