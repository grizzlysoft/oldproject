﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Quest : Popup
{
    public UIButton BtnExit;

    public UISprite CharIcon;
    public UILabel QuestName;
    public UILabel QuestScript;

    public UILabel NeedText;
    public UIScrollView NeedScroll;
    public UIGrid NeedGrid;

    List<IconText_Item> NeedScrollItems = new List<IconText_Item>();

    public UILabel RewardText;
    public UIScrollView RewardScroll;
    public UIGrid RewardGrid;

    public UILabel Btn_OkText;
    public UILabel Btn_CancelText;

    public UIButton BtnOk;
    public UIButton BtnCancel;

    List<IconText_Item> RewardScrollItems = new List<IconText_Item>();

    QuestData Data = null;

    public override void FirstLoad()
    {
        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        Data = data_ as QuestData;
        base.SetData(data_);
    }

    public override void Open(bool back = true)
    {
        NeedScroll.GetComponent<UIPanel>().depth = this.GetComponent<UIPanel>().depth + 1;
        RewardScroll.GetComponent<UIPanel>().depth = this.GetComponent<UIPanel>().depth + 1;
        CharIcon.spriteName = Data.Res;
        QuestName.text = Data.Name;
        QuestScript.text = Data.StartScript;
        CreateItemText();

        if (Data.Type == Enum.QuestType.Loop)
        {
            BtnCancel.gameObject.SetActive(true);
            BtnOk.transform.localPosition = new Vector3(-(BtnCancel.transform.localPosition.x), BtnOk.transform.localPosition.y, 0f);
        }
        else
        {
            BtnOk.transform.localPosition = new Vector3(0f, BtnOk.transform.localPosition.y, 0f);
            BtnCancel.gameObject.SetActive(false);
        }
        base.Open(back);
    }

    void CreateItemText()
    {
        for(int i = 0; i < Data.RequestID.Count; ++i)
        {
            ReQuestData r = DataManager.Get.GetReqeustData(Data.RequestID[i]);

            if(NeedScrollItems.Count >= i)
            {
                IconText_Item item = (GameObject.Instantiate(Resources.Load("Prefab/Popup/IconText_Item")) as GameObject).GetComponent<IconText_Item>();
                item.transform.parent = NeedGrid.transform;
                item.transform.localScale = Vector3.one;
                item.SetData(Enum.IconTextType.Item, r.Count, r.ItemID);

                NeedScrollItems.Add(item);
            }
            else
            {
                NeedScrollItems[i].SetData(Enum.IconTextType.Item, r.Count, r.ItemID);
                NeedScrollItems[i].OnOff(true);
            }
        }

        NeedGrid.Reposition();

        for(int i = 0; i < Data.RewardID.Count; ++i)
        {
            RewardData r = DataManager.Get.GetRewardData(Data.RewardID[i]);
            
            if(RewardScrollItems.Count >= i)
            {
                IconText_Item item = (GameObject.Instantiate(Resources.Load("Prefab/Popup/IconText_Item")) as GameObject).GetComponent<IconText_Item>();
                item.transform.parent = RewardGrid.transform;
                item.transform.localScale = Vector3.one;

                switch (r.Type)
                {
                    case Enum.RewardType.Gold:
                        item.SetData(Enum.IconTextType.Gold, r.Value, -1);
                        break;
                    case Enum.RewardType.Crystal:
                        item.SetData(Enum.IconTextType.Crystal, r.Value, -1);
                        break;
                    case Enum.RewardType.Heart:
                        item.SetData(Enum.IconTextType.Heart, r.Value, -1);
                        break;
                    case Enum.RewardType.Item:
                        item.SetData(Enum.IconTextType.Item, r.Value, r.ItemID);
                        break;
                }

                RewardScrollItems.Add(item);
            }
            else
            {
                switch (r.Type)
                {
                    case Enum.RewardType.Gold:
                        RewardScrollItems[i].SetData(Enum.IconTextType.Gold, r.Value, -1);
                        RewardScrollItems[i].OnOff(true);
                        break;
                    case Enum.RewardType.Crystal:
                        RewardScrollItems[i].SetData(Enum.IconTextType.Crystal, r.Value, -1);
                        RewardScrollItems[i].OnOff(true);
                        break;
                    case Enum.RewardType.Heart:
                        RewardScrollItems[i].SetData(Enum.IconTextType.Heart, r.Value, -1);
                        RewardScrollItems[i].OnOff(true);
                        break;
                    case Enum.RewardType.Item:
                        RewardScrollItems[i].SetData(Enum.IconTextType.Item, r.Value, r.ItemID);
                        RewardScrollItems[i].OnOff(true);
                        break;
                }

                RewardScrollItems[i].OnOff(true);
            }
        }

        RewardGrid.Reposition();

        NeedScroll.ResetPosition();
        RewardScroll.ResetPosition();
    }

    public override void SetText()
    {
        NeedText.text = "필요 재료";
        RewardText.text = "보상";

        if (Data.Type == Enum.QuestType.Loop || Data.Type == Enum.QuestType.Nomral)
        {
            if (QuestManager.Get.GetAcceptNormal == 1)
            {
                //완료
                Btn_OkText.text = "완료";
                Btn_CancelText.text = "포기";
            }
            else
            {
                //수락
                Btn_OkText.text = "수락";
                Btn_CancelText.text = "거절";
            }

        }
        else if (Data.Type == Enum.QuestType.Event)
            Btn_OkText.text = "완료";

        base.SetText();
    }

    public void OnClickedClose()
    {
        PopupManager.Get.ClosePopup("Popup_Quest");
    }

    public void OnClickedAceepetBtn()
    {
        if(Data.Type == Enum.QuestType.Loop || Data.Type == Enum.QuestType.Nomral)
        {
            if (QuestManager.Get.GetAcceptNormal == 1)
            {
                //완료
                QuestManager.Get.CompleteQuest(0);
                PopupManager.Get.ClosePopup("Popup_Quest");
            }
            else
            {
                //수락
                QuestManager.Get.AcceptQuest_Nomral();
                PopupManager.Get.ClosePopup("Popup_Quest");
            }

        }
        else
        {
            QuestManager.Get.CompleteQuest(1);
            PopupManager.Get.ClosePopup("Popup_Quest");
        }
    }

    public void OnClickedCancelBtn()
    {
        if (Data.Type == Enum.QuestType.Loop || Data.Type == Enum.QuestType.Nomral)
        {
            if (QuestManager.Get.GetAcceptNormal == 1)
            {
                //포기
                PopupManager.Get.ClosePopup("Popup_Quest");
                QuestManager.Get.CancelQuest_Nomral();
            }
            else
            {
                //거절
                PopupManager.Get.ClosePopup("Popup_Quest");
                QuestManager.Get.CancelQuest_Nomral();
            }

        }
    }

    public override void Close()
    {
        for(int i = 0; i < NeedScrollItems.Count; ++i)
        {
            NeedScrollItems[i].OnOff(false);
        }

        for(int i = 0; i < RewardScrollItems.Count; ++i)
        {
            RewardScrollItems[i].OnOff(false);
        }
        base.Close();
    }
}
