﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class Popup_BigMessage : Popup
{
    public UILabel Text;

    public UIButton Btn_Left;
    public UILabel BtnLeft_Text;

    public UIButton Btn_Right;
    public UILabel BtnRight_Text;

    public UIButton BtnClose;

    BigMessage Message;

    public override void FirstLoad()
    {
        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        Message = data_ as BigMessage;
        base.SetData(data_);
    }

    public override void Open(bool back = true)
    {
        Text.text = Message.Text;

        if (null != Message.Leftdelelgate)
            Btn_Left.onClick.Add(Message.Leftdelelgate);
        else
            Btn_Left.onClick.Add(new EventDelegate(CloseDelegate));

        if (null != Message.Rightdelelgate)
            Btn_Right.onClick.Add(Message.Rightdelelgate);
        else
            Btn_Right.onClick.Add(new EventDelegate(CloseDelegate));

        if (null != Message.Closedelegate)
            BtnClose.onClick.Add(Message.Closedelegate);
        else
            BtnClose.onClick.Add(new EventDelegate(CloseDelegate));

        BtnLeft_Text.text = Message.LeftText;
        BtnRight_Text.text = Message.RightText;

        base.Open(back);
    }

    void CloseDelegate()
    {
        PopupManager.Get.ClosePopup("Popup_BigMessage");
    }

    public override void Close()
    {
        Btn_Left.onClick.Clear();
        Btn_Right.onClick.Clear();
        base.Close();
    }

}
