﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_WorkComplete : Popup
{
    public UILabel Text;
    public UIGrid Grid;
    public GameObject[] Item = new GameObject[3];
    public UISprite[] Icon = new UISprite[3];
    public UILabel[] Count = new UILabel[3];
    public UIButton BtnOk;
    public UILabel BtnText;

    WorkCompleteData Data = null;

    public override void FirstLoad()
    {
        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        Data = data_ as WorkCompleteData;
        base.SetData(data_);
    }

    public override void Open(bool back = true)
    {
        if(null != Data)
        {
            for (int i = 0; i < Data.Item.Length; ++i)
            {
                if (Data.Item[i] == null)
                    Item[i].SetActive(false);
                else
                {
                    Item[i].SetActive(true);
                    Icon[i].spriteName = Data.Item[i].Res;
                    Count[i].text = string.Format("x{0}", Data.Count[i]);
                }
            }
        }

        Grid.Reposition();

        SetText();
        base.Open(back);
    }

    public override void SetText()
    {
        Text.text = "모든 작업이 끝났어!";
        BtnText.text = "확인";
        base.SetText();
    }

    public void OnClickedBtnOk()
    {
        Data = null;
        PopupManager.Get.ClosePopup("Popup_WorkComplete");
    }

    public override void Close()
    {
        base.Close();
    }
}
