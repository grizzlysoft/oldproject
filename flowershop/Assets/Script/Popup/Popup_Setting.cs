﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Setting : Popup
{
    public UILabel TitleText;
    public UISlider BackgroundVolume;
    public UISlider EffectVolume;
    public UILabel BackgroundVolumeText;
    public UILabel EffectVolumeText;

    public UILabel VerText;

    public UILabel[] BtnsText = new UILabel[8];

    public UIButton BtnClose;

    string[] Texts = { "저장", "불러오기", "Push", "쿠폰",  "공식카페", "게임평가", "페이스북", "만든사람들" };


    public override void FirstLoad()
    {
       
        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        base.SetData(data_);
    }

    public override void Open(bool back = true)
    {
        BackgroundVolume.value = SoundManager.Get.Valume_Bgm;
        EffectVolume.value = SoundManager.Get.Valume_Effect;
        base.Open(back);
    }

    public void OnChangeedBackgroundVolume()
    {
        SoundManager.Get.Valume_Bgm = BackgroundVolume.value;
    }

    public void OnChangedEffectVolume()
    {
        SoundManager.Get.Valume_Effect = EffectVolume.value;
    }

    public void OnClickedSaveBtn()
    {
        GPGS_Manager.Get.SaveToCloud();
    }

    public void OnClickedLoadBtn()
    {
        GPGS_Manager.Get.LoadFromCloud();
    }

    public void OnClickedCuponBtn()
    {
        PopupManager.Get.OpenPopup("Popup_Coupon");
    }

    public void OnClickedGoGoolgplayBtn()
    {
        Application.OpenURL("market://details?id=com.GrizzlySoft.FlowerShop");
    }

    public void OnClickedCafeBtn()
    {
        Application.OpenURL("http://cafe.naver.com/grizzlysoft");
    }

    public void OnClickedFaceBookBtn()
    {
        Application.OpenURL("https://www.facebook.com/GrizzlySoft/");
    }

    public void OnClickedCreaditBtn()
    {
        PopupManager.Get.OpenPopup("Popup_Creadit");
    }

    public void OnClickedCloseBtn()
    {
        PopupManager.Get.ClosePopup("Popup_Setting");
    }

    public void OnClickedPushBtn()
    {
        if (UserDataManager.Get.Push == 1)
            UserDataManager.Get.Push = 0;
        else
            UserDataManager.Get.Push = 1;

        SetText();
    }

    public override void SetText()
    {
        for(int i = 0; i < BtnsText.Length; ++i)
        {
            if (Texts[i] == "Push")
                BtnsText[i].text = UserDataManager.Get.Push == 1 ? Texts[i] + " On" : Texts[i] + " Off";
            else
                BtnsText[i].text = Texts[i];
        }
        VerText.text = "현재버전 ver." + Application.version;
        base.SetText();
    }

    public override void Close()
    {
        base.Close();
    }
}
