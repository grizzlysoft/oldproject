﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class Popup_CardGame : Popup
{
    public GameObject BlockObj;
    public UILabel StartText;
    public UILabel TimeText;
    public UILabel Time;
    public UIGrid Grid;
    public UILabel ComboText;
    
    public MiniGameCard[] Cards = new MiniGameCard[30];
    public GameObject[] FailedObj = new GameObject[2];

    bool GameStart = false;

    int Combo = 0;
    int ClickedIndex = -1;
    int[] type = { 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15 };
    int[] CloseIndex = { -1, -1 };
    MiniGmaeCardType SelectType = MiniGmaeCardType.None;
    bool ClickBlock = false;

    int Score = 0;
    float PlayTime = 60f;
    int Reward = 0;
    public override void FirstLoad()
    {
        for(int i = 0; i < Cards.Length; ++i)
        {
            Cards[i].Setting(CardClicked, i);
        }
        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        base.SetData(data_);
    }

    public override void Open(bool back = true)
    {
        BlockObj.SetActive(true);
        PlayTime = 60f;
        for(int i = 0; i < Cards.Length; ++i)
        {
            Cards[i].transform.localPosition = Vector3.zero;
        }
        Time.text = ((int)PlayTime).ToString();
        base.Open(back);
    }

    public void OnClickedStart()
    {
        Suffle.ShuffleArray(type);

        BlockObj.SetActive(false);

        for(int i = 0; i < type.Length; ++i)
        {
            Cards[i].SetType((Enum.MiniGmaeCardType)type[i]);
        }
        Score = 0;
        Grid.Reposition();
        Invoke("AllOpenCards", 0.7f);
        MiniGameManager.Get.Card_CoolTime = System.DateTime.Now.AddHours(1);
    }

    public void OnClickedClose()
    {
        PopupManager.Get.ClosePopup("Popup_CardGame");
    }

    public override void SetText()
    {
        base.SetText();
    }

    public override void Refresh()
    {
        base.Refresh();
    }

    public override void Close()
    {
        base.Close();
    }

    void CardClicked(MiniGameCard card_)
    {
        if (Score >= Cards.Length / 2)
        {
            GameStart = false;
            Invoke("GameEnd", 0.5f);
        }

        if (ClickBlock || !GameStart) return;

        if(SelectType == MiniGmaeCardType.None)
        {
            SelectType = card_.Type;
            card_.Open();
            ClickedIndex = card_.Id;
        }
        else
        {
            card_.Open();
            if (SelectType == card_.Type)
            {
                Combo++;
                Score++;
                if (Combo > 1)
                    ComboText.text = Combo + "c";
                else
                    ComboText.text = string.Empty;

                SoundManager.Get.PlaySfx(Sfx.combo_se);

                if (Score >= Cards.Length / 2)
                {
                    GameStart = false;
                    Invoke("GameEnd", 0.5f);
                }
            }
            else
            {
                CloseIndex[0] = card_.Id;
                CloseIndex[1] = ClickedIndex;
                Combo = 0;
                ComboText.text = "m";
                Invoke("CloseCard_Miss", 0.5f);
                ClickBlock = true;
            }
            ComboText.gameObject.SetActive(true);
            SelectType = MiniGmaeCardType.None;
            ClickedIndex = -1;
            Invoke("ComboTextHide", 0.5f);
        }
    }
   
    void CloseCard_Miss()
    {
        //Debug.Log("Closeindex 0 = " + CloseIndex[0]);
        //Debug.Log("Closeindex 1 = " + CloseIndex[1]);
        Cards[CloseIndex[0]].Close();
        Cards[CloseIndex[1]].Close();
        CloseIndex[0] = -1;
        CloseIndex[1] = -1;
        ClickBlock = false;
    }

    void AllOpenCards()
    {
        for(int i = 0; i < Cards.Length; ++i)
        {
            Cards[i].Open();
        }

        Invoke("AllCloseCards", 2f);
    }

    void AllCloseCards()
    {
        for(int i = 0; i < Cards.Length; ++i)
        {
            Cards[i].Close();
        }

        GameStart = true;
    }

    void GameEnd()
    {
        for (int i = 0; i < Cards.Length; ++i)
        {
            Cards[i].Close();
        }

        Reward = MiniGameManager.Get.CalculateReward_Card(PlayTime, Score);
        Score = 0;
        UseCrystalPopupData message = new UseCrystalPopupData("게임이 종료되었습니다.\n(광고시청시 보상2배)", MessagePopupType.Ok_Cancel,
                   new IconTextData(IconTextType.Gold, Reward, -1), "확인", "광고보기", OnClickedOfflineOkBtn, OnClickedOfflineShowAD);

        PopupManager.Get.OpenPopup("Popup_UesCrystal", message, false);
    }

    void OnClickedOfflineOkBtn()
    {
        UserDataManager.Get.AddGold(Reward);
        PopupManager.Get.ClosePopup("Popup_UesCrystal");
        PopupManager.Get.ClosePopup("Popup_CardGame");
    }

    void OnClickedOfflineShowAD()
    {
        Ads_Manager.Get.ShowAd_UnityAds(ADShowType.RewardDouble, Reward);
    }

    void ComboTextHide()
    {
        ComboText.gameObject.SetActive(false);
    }

	// Update is called once per frame
	void Update ()
    {
		if(GameStart)
        {
            PlayTime -= RealTime.deltaTime;
            Time.text = ((int)PlayTime).ToString();

            if (PlayTime <= 0f)
            {
                GameStart = false;
                Invoke("GameEnd", 0.5f);
            }
        }
	}

    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            GameStart = false;
        }
        else
        {
            if(BlockObj.activeInHierarchy == false)
                GameStart = true;
        }
    }
}
