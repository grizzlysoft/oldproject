﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Achievement_ScrollItem : MonoBehaviour
{
    public UILabel Name;
    public UILabel RewardText;
    public UISprite RewardIcon;
    public UILabel RewardValue;
    public UILabel RewardStep;
    public UIProgressBar StepProgress;
    public UILabel BtnText;
    public UIButton Btn;

    AchievementData Data = null;

    public void SetData(AchievementData data_)
    {
        Data = data_;
        SetInfo();
    }

    public void SetInfo()
    {
        Name.text = Data.Name;

        RewardData r = DataManager.Get.GetRewardData(Data.RewardID);

        if (r.Type == Enum.RewardType.Crystal)
            RewardIcon.spriteName = "Crystal_Icon";
        else if(r.Type == Enum.RewardType.Gold)
            RewardIcon.spriteName = "Gold_Icon";

        RewardValue.text = string.Format("{0:##,##0}", r.Value);

        int step = AchievementManager.Get.GetAchievementStep(Data.Type);

        RewardStep.text = step + "/" + Data.ClearValue;
        StepProgress.value = (float)step / (float)Data.ClearValue;

        SetText();

        if (AchievementManager.Get.CheckClearAchievement(Data.ID) && Data.Next == -2)
        {
            BtnText.text = "모두완료";
            RewardStep.text = "Complete";
            return;
        }

    }

    public void RefreshInfo()
    {
        int step = AchievementManager.Get.GetAchievementStep(Data.Type);

        RewardStep.text = step + "/" + Data.ClearValue;
        StepProgress.value = (float)step / (float)Data.ClearValue;

        if (AchievementManager.Get.CheckClearAchievement(Data.ID) && Data.Next == -2)
        {
            BtnText.text = "모두완료";
            RewardStep.text = "Complete";
            return;
        }
    }

    public void ReFresh()
    {
        SetData(DataManager.Get.GetAchievementData(AchievementManager.Get.GetNowAchievement(Data.Type)));
    }

    public void SetText()
    {
        RewardText.text = "보상";
        BtnText.text = "완료";
    }

    public void OnClickedComplteBtn()
    {
        if(AchievementManager.Get.CheckClearAchievement(Data.ID))
        {
            Message message = new Message(null, null, "이미 완료한 업적입니다.", "확인", string.Empty, Enum.MessagePopupType.Ok);
            PopupManager.Get.OpenPopup("Popup_Message", message);
            return;
        }

        if(AchievementManager.Get.AchievementClearAndNext(Data.ID))
        {
            
            SetData(DataManager.Get.GetAchievementData(AchievementManager.Get.GetNowAchievement(Data.Type)));
        }
        else
        {
            Message message = new Message(null, null, "업적을 완료 하지 못했습니다.", "확인", string.Empty, Enum.MessagePopupType.Ok);

            PopupManager.Get.OpenPopup("Popup_Message", message);
        }
    }
}
