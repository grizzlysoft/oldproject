﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_WhereHouseExplanation : Popup
{
    public UILabel BestCombo;

    public override void FirstLoad()
    {
        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        base.SetData(data_);
    }

    public override void Open(bool back = true)
    {
        BestCombo.text = "현재 최고 콤보 : [FFFF00]" + MiniGameManager.Get.WhereHouse_Best;
        base.Open(back);
    }

    public void OnClickedBtnStart()
    {
        PopupManager.Get.ClosePopup("Popup_WhereHouseExplanation");
        PopupManager.Get.OpenPopup("Popup_WhereHouseGame", null, false);
    }

    public void OnClickedClose()
    {
        PopupManager.Get.ClosePopup("Popup_WhereHouseExplanation");
    }

    public override void SetText()
    {
        base.SetText();
    }

    public override void Refresh()
    {
        base.Refresh();
    }

    public override void Close()
    {
        base.Close();
    }
}
