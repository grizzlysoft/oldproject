﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class MiniGameCard : MonoBehaviour
{
    public UIButton CardBack;
    public UISprite Icon;

    OnClickedCard Clicked;

    MiniGmaeCardType CardType;
    public MiniGmaeCardType Type
    {
        get { return CardType; }
    }

    int ID;
    public int Id
    {
        get { return ID; }
    }

    

    public void Setting(OnClickedCard Card_, int index_)
    {
        Clicked = Card_;
        ID = index_;
    }

    public void SetType(MiniGmaeCardType type_)
    {
        CardType = type_;

        Icon.spriteName = CardType.ToString();
    }

    public void Open()
    {
        CardBack.gameObject.SetActive(false);
    }

    public void Close()
    {
        CardBack.gameObject.SetActive(true);
    }

    public void OnClicked()
    {
        Clicked(this);
    }


}
