﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class IconText_Item : MonoBehaviour
{
    public UILabel Text;
    public UISprite Icon;

    public void SetData(IconTextType type_, int value_, int id_)
    {
        switch(type_)
        {
            case IconTextType.Gold:
                Icon.spriteName = "Gold_Icon";
                Text.text = "x " + string.Format("{0:##,##0}", value_);
                break;
            case IconTextType.Crystal:
                Icon.spriteName = "Crystal_Icon";
                Text.text = "x " + string.Format("{0:##,##0}", value_);
                break;
            case IconTextType.Heart:
                Icon.spriteName = "Heart_Icon";
                Text.text = "x " + string.Format("{0:##,##0}", value_);
                break;
            case IconTextType.Item:
                ItemData item = DataManager.Get.GetItemData(id_);
                Icon.spriteName = item.Res;
                Text.text = item.Name + " x " + string.Format("{0:##,##0}", value_);
                break;

        }
    }

    public void OnOff(bool trigger_)
    {
        this.gameObject.SetActive(trigger_);
    }
}
