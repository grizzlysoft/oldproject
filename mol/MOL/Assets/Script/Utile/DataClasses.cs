﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class Character
{
    public readonly int level;
    public readonly int hp;
    public readonly int atk;
    public readonly int def;
    public readonly int crt;
    public readonly int spd;
    public readonly float movespd;
    public readonly string res;
    public readonly int scale;

    public Character(int level_, int hp_, int atk_, int def_, int crt_, int spd_, float movespd_, string res_, int scale_)
    {
        level = level_;
        hp = hp_;
        atk = atk_;
        def = def_;
        crt = crt_;
        spd = spd_;
        movespd = movespd_;
        res = res_;
        scale = scale_;
    }
}

public class Mercenary : Character
{
    public readonly int cooltime;
    public readonly int costingame;
    public readonly int costupgrade;
    public readonly int costgem;
    public readonly int passiveid;

    public Mercenary(int level_, int hp_, int atk_, int def_, int crt_, int spd_, float movespd_, string res_, int scale_,
        int cooltime_, int costingame_, int costupgrade_, int costgem_, int passiveid_) : base (level_, hp_, atk_, def_, crt_, spd_, movespd_, res_, scale_)
    {
        cooltime = cooltime_;
        costingame = costingame_;
        costupgrade = costupgrade_;
        costgem = costgem_;
        passiveid = passiveid_;
    }
}

public class Monster : Character
{
    public readonly int reward_gold;

    public Monster(int level_, int hp_, int atk_, int def_, int crt_, int spd_, float movespd_, string res_, int scale_,
        int reward_gold_) : base(level_, hp_, atk_, def_, crt_, spd_, movespd_, res_, scale_)
    {
        reward_gold = reward_gold_;
    }
}

public class Tower
{
    public readonly int lv;
    public readonly int atk;
    public readonly int spd;
    public readonly int upgrade_cost;
    public readonly int gem_cost;

    public Tower(int lv_, int atk_, int spd_, int upgrade_cost_, int gem_cost_)
    {
        lv = lv_;
        atk = atk_;
        spd = spd_;
        upgrade_cost = upgrade_cost_;
        gem_cost = gem_cost_;
    }
}

public class Castle
{
    public readonly int lv;
    public readonly int hp;
    public readonly int upgrade_cost;
    public readonly int gem_cost;

    public Castle(int lv_, int hp_, int upgrade_cost_, int gem_cost_)
    {
        lv = lv_;
        hp = hp_;
        upgrade_cost = upgrade_cost_;
        gem_cost = gem_cost_;
    }
}

public class Mana
{
    public readonly int lv;
    public readonly int[] maxvalue = { 0, 0, 0, 0, 0 }; //각 래벨별 max 수치
    public readonly int[] lvup_cost = { 0, 0, 0, 0}; //인게임 레벨업 코스트(올리면 max수치 늘어남)
    public readonly int charge_sce; //초당 차는 마나
    public readonly int upgrade_cost;

    public Mana(int lv_, int[] maxvalue_, int[] lvup_cost_, int charge_sce_, int upgrade_cost_)
    {
        lv = lv_;
        maxvalue = maxvalue_;
        lvup_cost = lvup_cost_;
        charge_sce = charge_sce_;
        upgrade_cost = upgrade_cost_;
    }
}

public class Skill_GunFire
{
    public readonly int lv;
    public readonly int atk;
    public readonly int cooltime;
    public readonly int ingame_cost;
    public readonly int upgrade_cost;

    public Skill_GunFire(int lv_, int atk_, int cooltime_, int ingame_cost_, int upgrade_cost_)
    {
        lv = lv_;
        atk = atk_;
        cooltime = cooltime_;
        ingame_cost = ingame_cost_;
        upgrade_cost = upgrade_cost_;
    }
}

public class WaveMonsterInfo
{
    public readonly int type;
    public readonly int lv;
    public readonly float summondelay;

    public WaveMonsterInfo(int type_, int lv_, float summondelay_)
    {
        type = type_;
        lv = lv_;
        summondelay = summondelay_;
    }
}

public class WaveInfo
{
    public readonly int wave;
    public readonly List<WaveMonsterInfo> monsterinfo = new List<WaveMonsterInfo>();
    public readonly int reward;
    public readonly int boss;
    public readonly int reward_gem;

    public WaveInfo(int wave_, List<WaveMonsterInfo> monsterinfo_, int reward_, int boss_, int reward_gem_)
    {
        wave = wave_;
        monsterinfo = monsterinfo_;
        reward = reward_;
        boss = boss_;
        reward_gem = reward_gem_;
    }
}

public class ShopData
{
    public readonly int Id;
    public readonly int Type;
    public readonly Enum.Shop_PayType PayType;
    public readonly int Cost;
    public readonly string Name;
    public readonly string Text;
    public readonly string Res;
    public readonly List<int> RewardList;
    public readonly string PURCHASE_ID;

    public ShopData(int id_, int type_, Enum.Shop_PayType paytype_, int cost_, string name_, string text_, string res_, List<int>rewardlist_, string purchase_id_)
    {
        Id = id_;
        Type = type_;
        PayType = paytype_;
        Cost = cost_;
        Name = name_;
        Text = text_;
        Res = res_;
        RewardList = rewardlist_;
        PURCHASE_ID = purchase_id_;
    }
}

public class ShopReward
{
    public readonly int Id;
    public readonly Enum.RewardDataType Type;
    public readonly string Name;
    public readonly int Value;

    public ShopReward(int id_, Enum.RewardDataType type_, string name_, int value_)
    {
        Id = id_;
        Type = type_;
        Name = name_;
        Value = value_;
    }
}

public class PassiveSkill
{
    public readonly int Id;
    public readonly string Name;
    public readonly PassiveTarget Target;
    public readonly PassiveType Parametertype;
    public readonly int Stat_Per;
    public readonly int Duration;
    public readonly int Probability;
    public readonly string Text;

    public PassiveSkill(int id_, string name_, PassiveTarget target_, PassiveType parametertype_, int stat_per_, int duration_, int probability_, string text_)
    {
        Id = id_;
        Name = name_;
        Target = target_;
        Parametertype = parametertype_;
        Stat_Per = stat_per_;
        Duration = duration_;
        Probability = probability_;
        Text = text_;
    }

}



