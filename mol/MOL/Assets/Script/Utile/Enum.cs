﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enum
{
    public enum SceneType
    {
        Start,
        Lobby,
        Ingame,
    }

    public enum GameState
    {
        Title,
        Menu,
        Ingame,
        None,
    }

    public enum UnitState
    {
        Summon,
        Live,
        Dying,
        Die,
    }

    public enum Grade
    {
        General,
        Expert,
        Lengend,
    }

    public enum AniState
    {
        Warlk,
        Battle,
        Move,
        Die,
        Taunt,
        NuckBackStart,
        NuckBack_Ing,
        NuckBackEnd,
    }

    public enum AliseType
    {
        Archer,
        Warrior,
        Barbarian,
        Wizard,
        King,
        Knight,
        Max,
    }

    public enum MonsterType
    {
        Jako = 1,
        Goblin,
        Ogre,
        Cyclops,
        Golem,
        Skull,
        Viking,
        Pirate,
    }

    public enum UpgradeTabType
    {
        Archer,
        Warrior,
        Barbarian,
        Wizard,
        King,
        Knight,
        Castle,
        Mana,
        GunFire,
        Max,
    }

    public enum UpgradePopupStaus
    {
        lv,
        hp,
        atk,
        def,
        crt,
        spd,
        mov,
        max,
    }


    public enum Shop_Tap
    {
        Gold,
        Gem,
        Buff,
        Max,
    }

    public enum Shop_PayType
    {
        Cash = 1,
        Gem,
        Gold,
        Ads,
    }

    public enum RewardDataType
    {
        Gem = 1,
        Gold,
        Hp_boost,
        Atk_boost,
        Def_boost,
        Gold_bosst,
    }

    public enum MessagePopupType
    {
        None,
        Ok,
        Ok_Cancel,
        Loading,
    }

    public enum BoostType
    {
        Atk,
        Hp,
        Def,
        Gold,
        Max,
    }

    public enum Bgm
    {
        BLUESKYBLUE_lobby,
        Ignition_battle,
        battle_boss,
        Max,
    }

    public enum Sfx
    {
        fail,
        ButtonClick,
        win,
        Hit,
        magic_hit,
        Max,
    }

    public enum PassiveType
    {
        Atk = 1,
        HP_Def,
        AtkSpd,
    }

    public enum PassiveTarget
    {
        One = 1,
        All,
    }

    public enum LobbyCharState
    {
        Idle,
        MoveR,
        MoveL,
        Taunt,
        Max,
    }
}
