﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleBg : MonoBehaviour
{
    public Camera cam;
    public Camera cam2;
    public UIPanel panel;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
    int start_x = 0;
    int now_x = 0;
   
    public void OnClick()
    {
        start_x = (int)Input.mousePosition.x;
        Debug.Log("OnClick");
    }

    public void OnDrag()
    {
        now_x = (int)Input.mousePosition.x;

        int move = -(now_x - start_x) / 4;

        Vector2 movepos = new Vector2(move + panel.clipOffset.x, 0);

        if(movepos.x < 0)
        {
            movepos.x = 0;
        } 

        if(movepos.x > 1365)
        {
            movepos.x = 1365;
        }

        panel.clipOffset = movepos;
        cam.transform.localPosition = new Vector3(movepos.x, cam.transform.localPosition.y, 0);
        cam2.transform.localPosition = new Vector3(movepos.x, cam2.transform.localPosition.y, 0);
        //IngameManager.Get.GoldDestPoint.localPosition = new Vector3(IngameManager.Get.GoldDestPoint.localPosition.x + movepos.x, IngameManager.Get.GoldDestPoint.localPosition.y, 0);
        //transform.localPosition = new Vector3(movepos.x, movepos.y, 0);
    }

   

    public void OffPress()
    {

    }
}
