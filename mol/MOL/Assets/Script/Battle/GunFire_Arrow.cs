﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunFire_Arrow : MonoBehaviour
{
    bool Startmove = false;

    Vector3 startpos = Vector3.zero;

    float vx;
    float vy;
    float g;

    float dat;
    float mh;
    float dh;
    float max_y = 50f;
    float mht = 1f;

    Vector2 lastpos = Vector2.zero;

    public Animation Ani;

    // Use this for initialization
    void Start()
    {

    }



    public void SetPos(Vector3 start, Vector3 dest, float hight, float h_time)
    {
        //Debug.Log("????");
        startpos = start;
        max_y = hight;
        mht = h_time;

        dh = dest.y - start.y;
        mh = max_y - start.y;

        g = 2 * mh / (mht * mht);

        vy = Mathf.Sqrt(2 * g * mh);

        float a = g;
        float b = -2 * vy;
        float c = 2 * dh;

        dat = (-b + Mathf.Sqrt(b * b - 4 * a * c)) / (2 * a);
        vx = -(start.x - dest.x) / dat;
        GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
        Startmove = true;
    }

    float delttime = 0f;
    void MovePos()
    {
        delttime += Time.deltaTime;

        if (delttime >= mht * 2)
            TweenStart();

        if (movestop)
            return;

        float x = startpos.x + vx * delttime;
        float y = startpos.y + vy * delttime - 0.5f * g * delttime * delttime;

        transform.rotation = Quaternion.Euler(0, 0, (float)getAngle(lastpos.x, lastpos.y, x, y));


        lastpos.x = x;
        lastpos.y = y;

        this.gameObject.transform.localPosition = new Vector3(x, y, 0);
    }

    void TweenStart()
    {
        movestop = true;
        //Debug.Log(this.gameObject.transform.localPosition.y);
        Ani.Play("GunFire_ArrowAlpha", PlayMode.StopAll);
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(transform.localPosition);
        if (Startmove)
            MovePos();
    }

    private static double getAngle(float before_x, float before_y, float After_x, float After_y)
    {
        float dx = After_x - before_x;
        float dy = After_y - before_y;

        double rad = Mathf.Atan2(dy, dx);
        double degree = (rad * 180) / Mathf.PI;

        return degree;
    }

    bool movestop = false;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Lend" || other.tag == "Enemy_Hit")
        {
            movestop = true;

            if (other.tag == "Enemy_Hit" && delttime >= mht)
            {
                TweenStart();
            }
        }


    }

    public void Reset()
    {
        delttime = 0f;
        Startmove = false;
        movestop = false;
        transform.localPosition = new Vector3(0f, 4000f, 0f);
        
    }
}
