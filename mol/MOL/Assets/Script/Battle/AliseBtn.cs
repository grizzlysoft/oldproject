﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class AliseBtn : MonoBehaviour
{
    AliseType BtnType;

    int Cost = 0;
    float CoolTime = 0;

    public UISprite BtnImg;
    public UISprite BtnBlock;

    public UILabel BtnCostText;

    public UIButton Btn;

    float deltatime = 0f;

    bool StartCoolTime = false;

	// Use this for initialization
	void Start ()
    {
        Btn.onClick.Add(new EventDelegate(OnClickedBtn));
	}

    public void SetData(Mercenary mer, AliseType type_)
    {
        if (mer == null || mer.level == 0)
        {
            this.gameObject.SetActive(false);
            return;
        }

        BtnType = type_;
        CoolTime = mer.cooltime;
        Cost = mer.costingame;

        BtnCostText.text = Cost.ToString();

        BtnImg.spriteName = mer.res + "_Head";
        Btn.normalSprite = mer.res + "_Head";
        Btn.pressedSprite = mer.res + "_Head";
        Btn.hoverSprite = mer.res + "_Head";
        if (StartCoolTime == false)
        {
            BtnBlock.gameObject.SetActive(false);
        }
    }

    void OnClickedBtn()
    {
        if (IngameManager.Get.SummonAlise(BtnType))
        {
            deltatime = 0f;
            BtnBlock.gameObject.SetActive(true);
            BtnBlock.fillAmount = 1f;
            StartCoolTime = true;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
		if(StartCoolTime)
        {
            deltatime += Time.deltaTime;

            BtnBlock.fillAmount = 1 - (deltatime / CoolTime);

            if(deltatime >= CoolTime)
            {
                StartCoolTime = false;
                BtnBlock.gameObject.SetActive(false);
                deltatime = 0f;
            }
        }
	}
}
