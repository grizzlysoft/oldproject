﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spell : MonoBehaviour
{
    public Animation Ani;

    int atk;

    float m_t = 1.0f; //이동시간
    float deltatime = 0f;
    float v = 0f;
    float start_x = 0f;
    float dest_x = 0f;
    bool movestart = false;

	// Use this for initialization
	void Start ()
    {
		
	}

    public void SetPermater(Vector3 start, Vector3 dest, int a)
    {
        float move_x = dest.x - start.x;
        start_x = start.x;
        dest_x = dest.x;
        v = move_x / m_t;
        atk = a;

        Ani.Play("MoveSpell");
        movestart = true;
    }

    void MoveSpell()
    {
        deltatime += Time.deltaTime;

        Vector3 p = transform.localPosition;
        p.x = start_x + v * deltatime;

        transform.localPosition = p;

        if(p.x >= 1690 || p.x >= dest_x)
        {
            movestart = false;
            Ani.Play("BoomSpell");
            SoundManager.Get.PlaySfx(Enum.Sfx.magic_hit);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Enemy_Hit")
        {
            other.GetComponent<CharacterObj>().Damage(atk);
            Ani.Play("BoomSpell");
            SoundManager.Get.PlaySfx(Enum.Sfx.magic_hit);
        }
    }

    public void BoomEnd()
    {
        GameObject.Destroy(this.gameObject);
    }

    // Update is called once per frame
    void Update ()
    {
        if (movestart)
            MoveSpell();	
	}
}
