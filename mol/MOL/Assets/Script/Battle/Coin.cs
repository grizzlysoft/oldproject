﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public Animator ani;

    Vector3 DestPoint;
    Vector3 StartPoint;
    Vector2 v = Vector2.zero;

    float MoveTime = 1f;
    float Deltatime = 0f;

    int GetCoin = 0;

    bool StartMove = false;

    bool AniEnd = false;
    
    public void SetData(Vector3 dest_, int coin_)
    {
        StartPoint = this.transform.position;
        DestPoint = dest_;

        GetCoin = coin_;

        float move_x = DestPoint.x - StartPoint.x;
        float move_y = DestPoint.y - StartPoint.y;
        v.x = move_x / MoveTime;
        v.y = move_y / MoveTime;

    }

    void MoveCoin()
    {
        Deltatime += Time.deltaTime;

        if(Deltatime >= MoveTime * 0.8f)
        {
            ani.Play("CoinMoveEnd");
        }

        Vector3 p = transform.localPosition;
        p.x = StartPoint.x + v.x * Deltatime;
        p.y = StartPoint.y + v.y * Deltatime;
        transform.position = p;
    }
    
    // Update is called once per frame
    void Update ()
    {
        if (StartMove)
            MoveCoin();
	}

    public void MoveStart()
    {
        //Debug.Log("coin move start!!!");
        StartMove = true;
    }

    public void DestroyCoin()
    {
        IngameManager.Get.AddGetGold(GetCoin);
        AniEnd = true;
        Destroy(this.gameObject);
    }

    private void OnDestroy()
    {
        if(!AniEnd)
        {
            Main.Get.GetUserDataManager().AddGold(GetCoin);
        }
    }
}
