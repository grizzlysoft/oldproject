﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class SkillBtn : MonoBehaviour
{
    int Cost = 0;
    float CoolTime = 2f;

    public UISprite BtnImg;
    public UISprite BtnBlock;

    public UILabel BtnCostText;

    public UIButton Btn;

    float deltatime = 0f;

    bool StartCoolTime = false;

	// Use this for initialization
	void Start ()
    {
        Btn.onClick.Add(new EventDelegate(OnClickedBtn));
	}

    public void SetData(int cost_, float cooltime_)
    {
        Cost = cost_;
        CoolTime = cooltime_;

        BtnCostText.text = Cost.ToString();

        //BtnImg.spriteName = mer.res + "_Head";

        if(StartCoolTime == false)
        {
            BtnBlock.gameObject.SetActive(false);
        }
    }

    void OnClickedBtn()
    {
        if (IngameManager.Get.UseGunFire())
        {
            deltatime = 0f;
            BtnBlock.gameObject.SetActive(true);
            BtnBlock.fillAmount = 1f;
            StartCoolTime = true;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
		if(StartCoolTime)
        {
            deltatime += Time.deltaTime;

            BtnBlock.fillAmount = 1 - (deltatime / CoolTime);

            if(deltatime >= CoolTime)
            {
                StartCoolTime = false;
                BtnBlock.gameObject.SetActive(false);
                deltatime = 0f;
            }
        }
	}
}
