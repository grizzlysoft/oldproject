﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillName : MonoBehaviour
{
    public TweenPosition tweenpos;
    public TweenScale tweenscal;
    public TweenAlpha tweenalpha;
    public TweenAlpha labelalpha;

    public UILabel SkillText;
    public UISprite bg;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void SetText(string text)
    {
        SkillText.text = text;
        bg.width = SkillText.width + 40;
        bg.height = SkillText.height + 40;

        tweenpos.PlayForward();
        tweenscal.PlayForward();
        tweenalpha.PlayForward();
        labelalpha.PlayForward();
    }

    public void OnAlphaFinshed()
    {
        Destroy(this.gameObject);
    }
}
