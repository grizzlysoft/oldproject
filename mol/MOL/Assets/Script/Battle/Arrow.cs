﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    //x = S.x + V.x* t;
    //y = S.y + (V.y* t) - ((1/2) * g* t * t);
    //z = S.z + V.z* t;

    //t = 시간
    //S = 시작위치
    //g = 중력 가속도
    //v = 속도

    //    특정 시간동안 포물선으로 원하는 최종위치까지 이동하고 싶다면,
    //특정 시간을 t에 대입, 최종 위치를 x, y, z에 대입한 후
    //속도 V를 구한다.
    //속도를 구했으면 시간에 따른 위치를 구하면 된다.


    bool Startmove = false;

    Vector3 startpos = Vector3.zero;

    float vx;
    float vy;
    float g;

    float dat;
    float mh;
    float dh;
    float max_y = 50f;
    float mht = 0.5f;

    Vector2 lastpos = Vector2.zero;

    public Animation Ani;

    int atk = 0;
    int target = 0;

    // Use this for initialization
    void Start()
    {

    }

    public void SetPos(Vector3 start, Vector3 dest, int atk_, int target_)
    {
        startpos = start;
        atk = atk_;
        target = target_;

        dh = dest.y - start.y;
        mh = max_y - start.y;

        g = 2 * mh / (mht * mht);

        vy = Mathf.Sqrt(2 * g * mh);

        float a = g;
        float b = -2 * vy;
        float c = 2 * dh;

        dat = (-b + Mathf.Sqrt(b * b - 4 * a * c)) / (2 * a);
        vx = -(start.x - dest.x) / dat;
        Startmove = true;
    }

    float delttime = 0f;
    void MovePos()
    {
        delttime += Time.deltaTime;

        if (delttime >= 2f)
            TweenStart();

        if (movestop)
            return;



        float x = startpos.x + vx * delttime;
        float y = startpos.y + vy * delttime - 0.5f * g * delttime * delttime;

        transform.rotation = Quaternion.Euler(0, 0, (float)getAngle(lastpos.x, lastpos.y, x, y));


        lastpos.x = x;
        lastpos.y = y;

        this.gameObject.transform.localPosition = new Vector3(x, y, 0);
    }

    void TweenStart()
    {
        //Debug.Log(this.gameObject.transform.localPosition.y);
        Ani.Play("ArrowAlpha");
    }

    public void EndTween()
    {
        DestroyObject(this.gameObject);
    }

    // Update is called once per frame
    void Update ()
    {
        //Debug.Log(transform.localPosition);
        if (Startmove)
            MovePos();
	}

    private static double getAngle(float before_x, float before_y, float After_x, float After_y)
    {
        float dx = After_x - before_x;
        float dy = After_y - before_y;

        double rad = Mathf.Atan2(dy, dx);
        double degree = (rad * 180) / Mathf.PI;

        return degree;
    }

    bool movestop = false;
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Lend" || other.tag == "Enemy_Hit")
        {
            movestop = true;

            if(other.tag == "Enemy_Hit" && delttime >= mht)
            {
                GetComponent<BoxCollider>().enabled = false;
                other.GetComponent<CharacterObj>().Damage(atk);
                TweenStart();
                SoundManager.Get.PlaySfx(Enum.Sfx.Hit);
            }
        }

        
    }
}
