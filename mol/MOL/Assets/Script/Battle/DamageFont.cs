﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageFont : MonoBehaviour
{
    public TweenPosition pos;
    public TweenAlpha alpha;
    public UILabel label;

    // Use this for initialization
    void Start ()
    {
        pos.PlayForward();
        alpha.PlayForward();
        alpha.onFinished.Add(new EventDelegate(AlpahFinshed));
	}

    public void SetDamage(int damage)
    {
        label.text = damage.ToString();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void AlpahFinshed()
    {
        Destroy(this.gameObject);
    }
}
