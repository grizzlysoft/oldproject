﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class Battle_UI : MonoBehaviour
{
    public AliseBtn[] AliesBtns = new AliseBtn[6];
    public SkillBtn SkillBtn;
    public UIButton UpGradeManaBtn;
    public UIButton PauseBtn;

    public UILabel ManaLabel;
    public UILabel ManaUpCostLabel;
    public UILabel SkillCostLabel;
    public UILabel GetGoldLabel;

    public UISprite AliseBg;
    public UIGrid AliseGrid;

    public UISprite CastleHpBar;

    public Transform GoldIcon;

    public UIGrid BoostGrid;
    public UISprite[] BoostIcon = new UISprite[(int)BoostType.Max];

    public UILabel WaveText;

	// Use this for initialization
	void Start()
    {
        AilesBtnSetting();
        BoostUseSetting();
        UpGradeManaBtn.onClick.Add(new EventDelegate(ManaUpGradeClicked));
        WaveText.text = "WAVE " + Main.Get.GetUserDataManager().PlayWave;
    }

    public void AilesBtnSetting()
    {
        int active = 0;
        for(int i = 0; i < AliesBtns.Length; ++i)
        {
            Mercenary mer = null;
           
            mer = DataManager.Get.GetMercenaryData((Enum.AliseType)i, Main.Get.GetUserDataManager().GetAliseLv((Enum.AliseType)i));
            AliesBtns[i].SetData(mer, (Enum.AliseType)i);
            if (mer != null && mer.level > 0) active++;
        }
        AliseGrid.Reposition();
        AliseBg.width = active * 90 + 40;
    }

    public void ManaUpBtnSetting(Mana m, int lv_)
    {
        if (lv_ < m.lvup_cost.Length - 1)
            ManaUpCostLabel.text = m.lvup_cost[lv_].ToString();
        else
            ManaUpCostLabel.text = "Max";
    }

    public void SkillBtnSetting(int cost_, float cooltime_)
    {
        SkillBtn.SetData(cost_, cooltime_);
    }

    public void BoostUseSetting()
    {
        for(int i = 0; i < (int)BoostType.Max; ++i)
        {
            if (Main.Get.GetUserDataManager().UseBoost[i])
                BoostIcon[i].gameObject.SetActive(true);
            else
                BoostIcon[i].gameObject.SetActive(false);
        }

        BoostGrid.Reposition();
    }

    void ManaUpGradeClicked()
    {
        IngameManager.Get.ManaUpGrade();
    }

    public void RefreshManaText(int mana_, int maxmana_)
    {
        ManaLabel.text = mana_ + "/" + maxmana_;
    }

    public void RefreshGoldText(int gold_)
    {
        GetGoldLabel.text = string.Format("{0:#,##0}",  gold_);
    }

    public void ReFreshCatleHpBar(float value_)
    {
        CastleHpBar.fillAmount = value_;
    }

    public Transform GetGoldIconTransform()
    {
        return GoldIcon;
    }

    public void OnClickedPauseBtn()
    {
        PopupMessage popup = new PopupMessage();
        popup.Type = MessagePopupType.Ok_Cancel;
        popup.Text = "일시정지\n로비로 이동하면\n보상을 획득할 수 없습니다.";
        popup.Ok_Text = "로비이동";
        popup.Cancel_Text = "취소";
        popup.Ok_Delegate = new EventDelegate(GoLobby);
        popup.Cancel_Delegate = new EventDelegate(PauseCancel);
        Time.timeScale = 0f;
        PopupManager.Get.OpenPopup("Popup_Message", popup);
    }

    void GoLobby()
    {
        Main.Get.ChangeScene(SceneType.Lobby);
    }

    void PauseCancel()
    {
        Time.timeScale = 1f;
        PopupManager.Get.ClosePopup("Popup_Message");
    }

    // Update is called once per frame
    void Update ()
    {
		
	}
}
