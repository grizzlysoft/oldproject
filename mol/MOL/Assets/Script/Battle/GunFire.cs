﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunFire : MonoBehaviour
{
    int Arrow_Cnt = 100; //화살 겟수
    float End_StartPoint = -270f; //떨어지는 범위 시작
    float End_EndPoint = 1700f; //떨어지는 범위 끝
    float MaxHight = 520f;
    float MinHight = 500f;

    int Atk = 0;

    List<GunFire_Arrow> Arrows = new List<GunFire_Arrow>();

	// Use this for initialization
	void Start ()
    {
        Object o = Resources.Load("Prefab/GunFire_Arrow");
		for(int i = 0; i < Arrow_Cnt; ++i)
        {
            GameObject arrow = GameObject.Instantiate(o) as GameObject;
            arrow.transform.parent = IngameManager.Get.BgRoot;
            arrow.transform.localPosition = new Vector3(0f, 4000f, 0f);
            arrow.transform.localScale = new Vector3(40f, 40f, 40f);
            Arrows.Add(arrow.GetComponent<GunFire_Arrow>());
        }
	}

    public void SetAtk(int atk_)
    {
        Atk = atk_;
    }

    public void Fire()
    {
        for(int i = 0; i < Arrow_Cnt; ++i)
        {
            Arrows[i].SetPos(new Vector3(-506, -109, 0f), new Vector3(Random.Range(End_StartPoint, End_EndPoint), -90f, 0f),
                Random.Range(MinHight, MaxHight), Random.Range(0.9f, 1.1f));
        }
        InvokeRepeating("GunFireDamage", 1.8f, 1.8f);
    }

    
	// Update is called once per frame
	void Update ()
    {
		
	}

    void GunFireDamage()
    {
        CancelInvoke("GunFireDamage");
        IngameManager.Get.AllEnemyAttack(Atk);
    }
}
