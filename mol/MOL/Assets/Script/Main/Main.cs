﻿using Enum;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public  class Main : Singleton<Main>
{
    public TweenAlpha Fade;
    public Camera FadeCam;
    public bool DataClear = false;
    UserDataManager UserManager = new UserDataManager();
    // Use this for initialization

    SceneType moveto = SceneType.Start;
    SceneType now = SceneType.Start;
    private void Awake()
    {
        Application.targetFrameRate = 30;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    void Start()
    {
#if UNITY_EDITOR
        if (DataClear)
            PlayerPrefs.DeleteAll();
#endif
        //Screen.orientation = ScreenOrientation.LandscapeLeft;
        UserManager.LoadPlayPrefab();
        DontDestroyOnLoad(GameObject.Find("FadeInOut"));
        DontDestroyOnLoad(this.gameObject);
        DontDestroyOnLoad(GameObject.Find("Managers"));
        DontDestroyOnLoad(GameObject.Find("SoundManager"));

        DataManager.Get.DataLoad();
        UserManager.CreateWaveClearCnt();

        Purchasing.Get.InitPurchasing();
        SceneManager.sceneLoaded += OnSceneLoad;
        SceneManager.sceneUnloaded += OnSceneUnLoad;

        PopupManager.Get.SetPopupParent(GameObject.Find("Popups").transform);

        GPGS_Manager.Get.InitializeGPGS();
        LogoEnd();
    }

    void LogoEnd()
    {
        ChangeScene(SceneType.Lobby);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if (PopupManager.Get.GetOpenPopupCnt() <= 0)
            {
                if (now == SceneType.Ingame)
                {
                    PopupMessage popup = new PopupMessage();
                    popup.Type = MessagePopupType.Ok_Cancel;
                    popup.Text = "일시정지\n로비로 이동하면\n보상을 획득할 수 없습니다.";
                    popup.Ok_Text = "로비이동";
                    popup.Cancel_Text = "취소";
                    popup.Ok_Delegate = new EventDelegate(GoLobby);
                    popup.Cancel_Delegate = new EventDelegate(PauseCancel);
                    Time.timeScale = 0f;
                    PopupManager.Get.OpenPopup("Popup_Message", popup);
                }
                else
                    OpenExitPopup();
            }
            else
            {
                if (now == SceneType.Ingame) Time.timeScale = 1f;

                PopupManager.Get.ClosePopup();
            }
        }
    }

    public void ChangeScene(SceneType type)
    {
        moveto = type;
        Fade.onFinished.Clear();
        Fade.onFinished.Add(new EventDelegate(FadeFinsh));
        FadeCam.gameObject.SetActive(true);
        PopupManager.Get.AllClosePopup();
        PopupManager.Get.ObjDestroy();
        //PopupManager.Get.ClearPopupParent();
        FadeIn();
    }

    void FadeFinsh()
    {
        SceneManager.LoadScene((int)moveto);
    }

    void OnSceneLoad(Scene scene, LoadSceneMode mode)
    {
        //Debug.Log("OnSceneLoaded: " + scene.name);
        //Debug.Log(mode);
        FadeOut();
        //PopupManager.Get.SetPopupParent(GameObject.Find("Popups").transform);
    }

    void OnSceneUnLoad(Scene scene)
    {
        //Debug.Log("OnSceneLoaded: " + scene.name);
        now = moveto;
    }

    void FadeIn()
    {
        Fade.PlayForward();
    }

    void FadeOut()
    {
        Time.timeScale = 1;
        Fade.onFinished.RemoveAt(0);
        Fade.onFinished.Add(new EventDelegate(Fade_PlayReverseEnd));
        Fade.PlayReverse();
        Resources.UnloadUnusedAssets();
        
    }

    void Fade_PlayReverseEnd()
    {
        
        FadeCam.gameObject.SetActive(false);
    }

    public UserDataManager GetUserDataManager()
    {
        return UserManager;
    }

    public SceneType GetNowScene()
    {
        return now;
    }

    void OpenExitPopup()
    {
        PopupMessage popup = new PopupMessage();
        popup.Text = "게임 을 종료하시겠습니까?";
        popup.Ok_Text = "종료";
        popup.Cancel_Text = "취소";
        popup.Ok_Delegate = new EventDelegate(ExitGame);
        popup.Cancel_Delegate = new EventDelegate(ExitCancel);
        popup.Type = MessagePopupType.Ok_Cancel;

        PopupManager.Get.OpenPopup("Popup_Message", popup);
    }

    void ExitGame()
    {
        Application.Quit();
    }

    void ExitCancel()
    {
        Time.timeScale = 1f;
        PopupManager.Get.ClosePopup("Popup_Message");
    }

    void GoLobby()
    {
        Main.Get.ChangeScene(SceneType.Lobby);
    }

    void PauseCancel()
    {
        Time.timeScale = 1f;
        PopupManager.Get.ClosePopup("Popup_Message");
    }

    private void OnApplicationPause(bool pause)
    {
        if(pause)
        {

        }
        else
        {

        }
    }
}
