﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Start : MonoBehaviour
{
    public void OnClicked()
    {

        Debug.Log("onclicked!");
        this.gameObject.SetActive(false);

        Main.Get.ChangeScene(Enum.SceneType.Lobby);
    }

}
