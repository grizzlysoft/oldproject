﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class PopupMessage
{
    public MessagePopupType Type = MessagePopupType.Ok_Cancel;
    public string Text = string.Empty;
    public string Ok_Text = "Ok";
    public string Cancel_Text = "Cancel";
    public EventDelegate Ok_Delegate = null;
    public EventDelegate Cancel_Delegate = null;
}

public class Popup_Message : Popup
{
    PopupMessage message;

    public UIButton OkBtn;
    public UIButton CancelBtn;

    public UILabel Text;
    public UILabel Ok_Text;
    public UILabel Cancel_Text;

    public UIButton Block;
    public UIButton Body;

    public override void SetData(object data_)
    {
        message = data_ as PopupMessage;
        base.SetData(data_);
    }

    public override void FirstLoad()
    {
        base.FirstLoad();
    }

    public override void Close()
    {
        base.Close();
    }

    public override void Open()
    {
        switch(message.Type)
        {
            case MessagePopupType.None:
                OkBtn.gameObject.SetActive(false);
                CancelBtn.gameObject.SetActive(false);
                Text.transform.localPosition = Vector3.zero;
                break;
            case MessagePopupType.Ok:
                OkBtn.gameObject.SetActive(true);
                OkBtn.transform.localPosition = new Vector3(0f, OkBtn.transform.localPosition.y, 0f);
                CancelBtn.gameObject.SetActive(false);
                Text.transform.localPosition = new Vector3(0f, 25f, 0f);
                break;
            case MessagePopupType.Ok_Cancel:
                CancelBtn.gameObject.SetActive(true);
                OkBtn.gameObject.SetActive(true);
                OkBtn.transform.localPosition = new Vector3(-165f, OkBtn.transform.localPosition.y, 0f);
                Text.transform.localPosition = new Vector3(0f, 25f, 0f);
                break;
            case MessagePopupType.Loading:
                OkBtn.gameObject.SetActive(false);
                CancelBtn.gameObject.SetActive(false);
                Text.transform.localPosition = Vector3.zero;
                break;
        }

        OkBtn.onClick.Clear();
        CancelBtn.onClick.Clear();
        Block.onClick.Clear();
        Body.onClick.Clear();

        if (null == message.Ok_Delegate && (message.Type == MessagePopupType.Ok || message.Type == MessagePopupType.Ok_Cancel))
            OkBtn.onClick.Add(new EventDelegate(ClosePopup));
        else if(null != message.Ok_Delegate && (message.Type == MessagePopupType.Ok || message.Type == MessagePopupType.Ok_Cancel))
            OkBtn.onClick.Add(message.Ok_Delegate);

        if (null == message.Cancel_Delegate && message.Type == MessagePopupType.Ok_Cancel)
            CancelBtn.onClick.Add(new EventDelegate(ClosePopup));
        else if(null != message.Cancel_Delegate && message.Type == MessagePopupType.Ok_Cancel)
            CancelBtn.onClick.Add(message.Cancel_Delegate);

        if(null == message.Ok_Delegate && message.Type == MessagePopupType.None)
        {
            Body.onClick.Add(new EventDelegate(ClosePopup));
            Block.onClick.Add(new EventDelegate(ClosePopup));
        }
        else if(null != message.Ok_Delegate && message.Type == MessagePopupType.None)
        {
            Body.onClick.Add(message.Ok_Delegate);
            Block.onClick.Add(message.Ok_Delegate);
        }

        if(message.Type == MessagePopupType.Loading)
        {
            Body.onClick.Clear();
            Block.onClick.Clear();
        }

        Text.text = message.Text;
        Ok_Text.text = message.Ok_Text;
        Cancel_Text.text = message.Cancel_Text;
        base.Open();
    }

    void ClosePopup()
    {
        PopupManager.Get.ClosePopup("Popup_Message");
    }

    public MessagePopupType GetMessageType()
    {
        return message.Type;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
