﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class Popup_Cheat : Popup
{
    public UIButton[] AilleType;
    public UIGrid TypeGrid;
    public UIInput AilleLvInput;
    public UIButton AilleOkBtn;

    public UIInput WaveInput;
    public UIButton WaveOkBtn;

    public UIInput GoldInput;
    public UIButton GoldOkBtn;

    public UIInput GemInput;
    public UIButton GemOkBtn;

    AliseType SelectType = AliseType.Archer;

    public override void FirstLoad()
    {
        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        base.SetData(data_);
    }

    public override void Open()
    {
        base.Open();
    }

    void TypeSelectSetting()
    {
        for(int i = 0; i < (int)AliseType.Max; ++i)
        {
            if ((AliseType)i != SelectType)
                AilleType[i].gameObject.SetActive(false);
            else
                AilleType[i].gameObject.SetActive(true);
        }

        TypeGrid.Reposition();
    }

    void TypeSelectOpen()
    {
        for (int i = 0; i < (int)AliseType.Max; ++i)
        {
            AilleType[i].gameObject.SetActive(true);
        }

        TypeGrid.Reposition();
    }

    public void AcherClicked()
    {
        if (SelectType == AliseType.Archer)
            TypeSelectOpen();
        else
        {
            SelectType = AliseType.Archer;
            TypeSelectSetting();
        }
    }

    public void WarriorClicked()
    {
        if (SelectType == AliseType.Warrior)
            TypeSelectOpen();
        else
        {
            SelectType = AliseType.Warrior;
            TypeSelectSetting();
        }
    }

    public void BarbarianClicked()
    {
        if (SelectType == AliseType.Barbarian)
            TypeSelectOpen();
        else
        {
            SelectType = AliseType.Barbarian;
            TypeSelectSetting();
        }
    }

    public void WizardClicked()
    {
        if (SelectType == AliseType.Wizard)
            TypeSelectOpen();
        else
        {
            SelectType = AliseType.Wizard;
            TypeSelectSetting();
        }
    }

    public void KingClicked()
    {
        if (SelectType == AliseType.King)
            TypeSelectOpen();
        else
        {
            SelectType = AliseType.King;
            TypeSelectSetting();
        }
    }

    public void KnightClicked()
    {
        if (SelectType == AliseType.Knight)
            TypeSelectOpen();
        else
        {
            SelectType = AliseType.Knight;
            TypeSelectSetting();
        }
    }

    public void AilleLvOkClicked()
    {
        if(AilleLvInput.value != string.Empty)
        {
            int lv = int.Parse(AilleLvInput.value);

            if (DataManager.Get.GetMercenaryData(SelectType, lv) == null)
                return;

            Main.Get.GetUserDataManager().SubstitutionAilleLv(SelectType, lv);
        }

        AilleLvInput.value = string.Empty;
    }

    public void WaveOkClicked()
    {
        if (WaveInput.value != string.Empty)
        {
            int wave = int.Parse(WaveInput.value);
            if (null == DataManager.Get.GetWaveData(wave))
                return;

            Main.Get.GetUserDataManager().SubstitutionWave(wave);
        }

        WaveInput.value = string.Empty;
    }

    public void GoldOkClicked()
    {
        if (GoldInput.value != string.Empty)
            Main.Get.GetUserDataManager().AddGold(int.Parse(GoldInput.value));

        GoldInput.value = string.Empty;
    }

    public void GemOkClicked()
    {
        if (GemInput.value != string.Empty)
            Main.Get.GetUserDataManager().AddGem(int.Parse(GemInput.value));

        GemInput.value = string.Empty;
    }

    public override void Close()
    {
        base.Close();
    }

}
