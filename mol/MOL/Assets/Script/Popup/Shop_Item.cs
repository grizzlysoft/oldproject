﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class Shop_Item : MonoBehaviour
{
    public UILabel ItemName;
    public UILabel ItemText;
    public UISprite ItemRes;

    public UILabel CostText;
    public UISprite CostIcon;

    public UIButton BuyBtn;

    ShopData item;

    public void SetData(int id_)
    {
        item = DataManager.Get.GetShopData(id_);

        ItemName.text = item.Name;
        ItemText.text = item.Text.Replace("\\n", "\n");

        ItemRes.spriteName = item.Res;

        CostText.text = string.Format("{0:#,##0}", item.Cost);
        CostIcon.gameObject.SetActive(true);
        if (item.PayType == Shop_PayType.Gold)
        {
            CostIcon.spriteName = "Cartoon RPG UI_Game Icon - Coin";
        }
        else if(item.PayType == Shop_PayType.Gem)
        {
            CostIcon.spriteName = "Cartoon RPG UI_Game Icon - Diamond";
        }
        else if(item.PayType == Shop_PayType.Cash)
        {
            CostIcon.gameObject.SetActive(false);
            CostText.text = string.Format("￦ {0:#,##0}", item.Cost);
        }
        else if(item.PayType == Shop_PayType.Ads)
        {
            CostIcon.spriteName = "movie_play";
            CostText.text = "PLAY";
        }
    }

    public void OnClickedByBtn()
    {
        PopupMessage popup = new PopupMessage();
        popup.Type = MessagePopupType.Ok_Cancel;
        popup.Text = item.Text.Replace("\\n", "\n") + "\n구입하시겠습니까?";
        popup.Ok_Text = "구입";
        popup.Cancel_Text = "취소";
        popup.Ok_Delegate = new EventDelegate(BuyDelegate);

        PopupManager.Get.OpenPopup("Popup_Message", popup);
    }

    void BuyDelegate()
    {
        PopupManager.Get.ClosePopup("Popup_Message");
        PaymentManager.Get.BuyPrice(item.PayType, item.Cost, item.RewardList, item.PURCHASE_ID);
    }

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
