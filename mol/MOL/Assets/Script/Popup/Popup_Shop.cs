﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class Popup_Shop : Popup
{
    public UILabel PopupTitle;

    public UIButton ExitBtn;

    public UIButton[] ShopTap = new UIButton[(int)Shop_Tap.Max];
    public UISprite[] ShopTapIcon = new UISprite[(int)Shop_Tap.Max];

    public UIScrollView[] ShopScroll = new UIScrollView[(int)Shop_Tap.Max];
    public UIGrid[] ShopScrollGrid = new UIGrid[3];

    List<int> Shop_GoldTapID = new List<int>();
    List<int> Shop_GemTapID = new List<int>();
    List<int> Shop_BuffTapID = new List<int>();

    Shop_Tap SelectTap = Shop_Tap.Gold;

    Shop_Item StartPackageObj = null;

    public override void FirstLoad()
    {
        Shop_GoldTapID = DataManager.Get.GetShopDataIDList(1);
        Shop_GemTapID = DataManager.Get.GetShopDataIDList(2);
        Shop_BuffTapID = DataManager.Get.GetShopDataIDList(3);

        for(int i = 0; i < Shop_GoldTapID.Count; ++i)
        {
            Shop_Item item = (GameObject.Instantiate(Resources.Load("Prefab/Popup/Shop_Item")) as GameObject).GetComponent<Shop_Item>();
            item.transform.parent = ShopScrollGrid[(int)Shop_Tap.Gold].transform;
            item.transform.localScale = Vector3.one;
            item.transform.localPosition = Vector3.zero;
            item.SetData(Shop_GoldTapID[i]);
        }

        for (int i = 0; i < Shop_GemTapID.Count; ++i)
        {
            Shop_Item item = (GameObject.Instantiate(Resources.Load("Prefab/Popup/Shop_Item")) as GameObject).GetComponent<Shop_Item>();
            item.transform.parent = ShopScrollGrid[(int)Shop_Tap.Gem].transform;
            item.transform.localScale = Vector3.one;
            item.transform.localPosition = Vector3.zero;
            item.SetData(Shop_GemTapID[i]);

            if (DataManager.Get.GetShopData(Shop_GemTapID[i]).PURCHASE_ID == "start_package") StartPackageObj = item;
        }

        for (int i = 0; i < Shop_BuffTapID.Count; ++i)
        {
            Shop_Item item = (GameObject.Instantiate(Resources.Load("Prefab/Popup/Shop_Item")) as GameObject).GetComponent<Shop_Item>();
            item.transform.parent = ShopScrollGrid[(int)Shop_Tap.Buff].transform;
            item.transform.localScale = Vector3.one;
            item.transform.localPosition = Vector3.zero;
            item.SetData(Shop_BuffTapID[i]);
        }

        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        SelectTap = (Shop_Tap)data_;

        base.SetData(data_);
    }

    public override void Open()
    {
        Debug.Log("Shop Open");
        SetTap();
        base.Open();
    }

    public override void Close()
    {
        base.Close();
    }

    void SetTap()
    {
        for(int i = 0; i < ShopScroll.Length; ++i)
        {
            if((int)SelectTap == i)
            {
                ShopScroll[i].gameObject.SetActive(true);
                ShopTapIcon[i].color = Color.white;
                ShopScrollGrid[i].Reposition();
                ShopScroll[i].GetComponent<UIPanel>().depth = this.GetComponent<UIPanel>().depth + 1;
            }
            else
            {
                ShopScroll[i].gameObject.SetActive(false);
                ShopTapIcon[i].color = Color.gray;
                ShopScroll[i].GetComponent<UIPanel>().depth = this.GetComponent<UIPanel>().depth + 1;
            }
        }
    }

    public void StartPackageHide()
    {
        if (StartPackageObj != null)
            StartPackageObj.gameObject.SetActive(false);

        ShopScrollGrid[(int)Shop_Tap.Gem].Reposition();
    }

    public void OnclickedGoldTab()
    {
        SelectTap = Shop_Tap.Gold;
        SetTap();
    }

    public void OnClickedGemTab()
    {
        SelectTap = Shop_Tap.Gem;
        SetTap();
    }

    public void OnClickedBuffTab()
    {
        SelectTap = Shop_Tap.Buff;
        SetTap();
    }

    public void OnClickedExit()
    {
        PopupManager.Get.ClosePopup("Popup_Shop");
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
