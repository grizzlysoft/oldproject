﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupReward
{
    /// <summary>
    /// 0 = gold, 1 = gem, 2 = atkboost, 3 = hpboost, 4 = defboost, 5 = goldboost, 6 = gold and gem
    /// </summary>
    public int type = 0;
    public string text = string.Empty;
    public string btn_text = string.Empty;
    public string CancelText = string.Empty;
    public int value = 0;
    public int value2 = 0;
    public EventDelegate Ok_Delegate = null;
    public EventDelegate Cancel_Delegate = null;

    /// <summary>
    /// 0 = ok_cancel, 1 = ok
    /// </summary>
    public int BtnType = 0; 
}

public class Popup_Reward : Popup
{
    public UILabel Text;
    public UILabel Value;
    public UISprite Value_Icon;
    public UIGrid Value_Grid;

    public UILabel Value_2;
    public UISprite Value_Icon_2;
    public UIGrid Value_Grid_2;

    public UIGrid ValueGrid_Par;

    public UIButton Ok_Btn;
    public UILabel Ok_BtnText;

    public UIButton Cancel_Btn;
    public UILabel Cancel_BtnText;

    public GameObject AdsBtn;

    PopupReward popupreward;

    public override void FirstLoad()
    {
        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        popupreward = data_ as PopupReward;
        base.SetData(data_);
    }

    public override void Open()
    {
        Text.text = popupreward.text;
        Ok_BtnText.text = popupreward.btn_text;
        Cancel_BtnText.text = popupreward.CancelText;

        switch(popupreward.type)
        {
            case 0:
                Value_Icon.spriteName = "Cartoon RPG UI_Game Icon - Coin";
                break;
            case 1:
                Value_Icon.spriteName = "Cartoon RPG UI_Game Icon - Diamond";
                break;
            case 2:
                Value_Icon.spriteName = "Cartoon RPG UI_Game Icon - Attack";
                break;
            case 3:
                Value_Icon.spriteName = "Cartoon RPG UI_Game Icon - Healt Potion";
                break;
            case 4:
                Value_Icon.spriteName = "Cartoon RPG UI_Game Icon - Defense";
                break;
            case 5:
                Value_Icon.spriteName = "Cartoon RPG UI_Game Icon - Gold Bag";
                break;
            case 6:
                Value_Icon.spriteName = "Cartoon RPG UI_Game Icon - Coin";
                Value_Icon_2.spriteName = "Cartoon RPG UI_Game Icon - Diamond";
                break;
        }

        if (popupreward.type != 6)
            Value_Grid_2.gameObject.SetActive(false);
        else
            Value_Grid_2.gameObject.SetActive(true);

        Value.text = string.Format("{0:#,##0}", popupreward.value);
        Value_Grid.Reposition();

        Value_2.text = string.Format("{0:#,##0}", popupreward.value2);
        Value_Grid_2.Reposition();

        if (Main.Get.GetNowScene() == Enum.SceneType.Ingame)
            AdsBtn.SetActive(true);
        else
            AdsBtn.SetActive(false);

        ValueGrid_Par.Reposition();

        if (null != popupreward.Ok_Delegate)
            Ok_Btn.onClick.Add(popupreward.Ok_Delegate);
        else
            Ok_Btn.onClick.Add(new EventDelegate(CloseDelegate));

        if (null != popupreward.Cancel_Delegate)
            Cancel_Btn.onClick.Add(popupreward.Cancel_Delegate);
        else
            Cancel_Btn.onClick.Add(new EventDelegate(CloseDelegate));

        if(popupreward.BtnType == 0)
        {
            Ok_Btn.transform.localPosition = new Vector3(-135f, Ok_Btn.transform.localPosition.y, 0f);
            Cancel_Btn.gameObject.SetActive(true);
        }
        else
        {
            Cancel_Btn.gameObject.SetActive(false);
            Ok_Btn.transform.localPosition = new Vector3(0f, Ok_Btn.transform.localPosition.y, 0f);
        }

        base.Open();
    }

    public override void Close()
    {
        Ok_Btn.onClick.Clear();
        base.Close();
    }

    void CloseDelegate()
    {
        PopupManager.Get.ClosePopup("Popup_Reward");
    }

    public void AdsDelegate()
    {
        AdsManager.Get.ShowRewardedAd_RewardPopup();
        AdsBtn.SetActive(false);
        ValueGrid_Par.Reposition();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
