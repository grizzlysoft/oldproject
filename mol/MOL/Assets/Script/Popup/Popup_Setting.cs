﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Setting : Popup
{
    public UIPanel ScrollPanel;

    public UIButton SoundBtn;
    public UISprite SoundBtn_Img;

    public UIInput Cupoon_Input;
    public UIButton CupoonSendBtn;

    public UIButton FaceBookBtn;
    public UIButton GoogleBtn;

    public UIButton CloseBtn;
    public UILabel CloseText;

    public GameObject Bonus;

    public UILabel VerInfo;

    public override void FirstLoad()
    {
        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        base.SetData(data_);
    }

    public override void Open()
    {
        if (SoundManager.Get.Sound_On)
            SoundBtn_Img.spriteName = "Cartoon RPG UI_Switch - ON";
        else
            SoundBtn_Img.spriteName = "Cartoon RPG UI_Switch - OFF";

        Cupoon_Input.value = string.Empty;

        ScrollPanel.depth = this.GetComponent<UIPanel>().depth + 1;

        if(Main.Get.GetUserDataManager().Facebook_GetBonus != 0)
            Bonus.gameObject.SetActive(false);

        VerInfo.text = "버전정보 : ver_" + Application.version;
        base.Open();
    }

    public void OnClickedSendBtn()
    {
        Debug.Log("쿠폰 번호로 입력한것 = " + Cupoon_Input.value);
    }

    public void OnClickedSoundOnOff()
    {
        if(SoundManager.Get.Sound_On)
        {
            SoundManager.Get.SoundOff();
            SoundBtn_Img.spriteName = "Cartoon RPG UI_Switch - OFF";
        }
        else
        {
            SoundManager.Get.SoundOn();
            SoundBtn_Img.spriteName = "Cartoon RPG UI_Switch - ON";
        }
    }

    public void OnClickedFaceBookBtn()
    {
        Application.OpenURL("https://www.facebook.com/GrizzlySoft/");
        if(Main.Get.GetUserDataManager().Facebook_GetBonus == 0)
        {
            Main.Get.GetUserDataManager().Facebook_GetBonus = 1;
            Bonus.gameObject.SetActive(false);
            PlayerPrefs.SetInt("Facebook_GetBonus", 1);

            PopupReward popup = new PopupReward();
            popup.BtnType = 1;
            popup.btn_text = "확인";
            popup.text = "보상을 획득하였습니다.";
            popup.type = 1;
            popup.value = 5;

            PopupManager.Get.OpenPopup("Popup_Reward", popup);

            Main.Get.GetUserDataManager().AddGem(5);
        }
    }

    public void OnClickedGoogleBtn()
    {
        Application.OpenURL("market://details?id=com.GrizzlySoft.MOL");
    }

    public void OnClickedCloseBtn()
    {
        PopupManager.Get.ClosePopup("Popup_Setting");
    }

    public override void Close()
    {
        Cupoon_Input.value = string.Empty;
        base.Close();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
