﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class Popup_Upgrade : Popup
{
    //public UILabel Title;
    public UIButton ExitBtn;

    public UIButton[] TabBtns = new UIButton[(int)UpgradeTabType.Max];
    public UISprite[] TabBtn_Imgs = new UISprite[(int)UpgradeTabType.Max];

    public UISprite SelectImg;
    public UIGrid StatusGrid;
    public UILabel[] Status = new UILabel[(int)UpgradePopupStaus.max];
    public UILabel[] Status_plus = new UILabel[(int)UpgradePopupStaus.max];

    public UIButton LevelupBtn;
    public UILabel Levelup_text;
    public UILabel Levelup_cost;
    public UISprite Levelup_coin;

    public UIButton RankUpBtn;
    public UILabel Rankup_text;
    public UILabel Rankup_cost;
    public UISprite Rankup_gem;

    public UIPanel TabScrollPanel;

    UpgradeTabType SelectTap = UpgradeTabType.Archer;

    int paytype = 0; //0 골드, 1 보석
    int pay = 0; //지불할 값

    public override void Open()
    {
        TabScrollPanel.depth = GetComponent<UIPanel>().depth + 1;
        SetTabImg();
        SetInfo();
        base.Open();
    }

    public override void SetData(object data_)
    {
        SelectTap = (UpgradeTabType)data_;
        base.SetData(data_);
    }

    void SetInfo()
    {
        switch(SelectTap)
        {
            case UpgradeTabType.Warrior:
            case UpgradeTabType.Archer:
            case UpgradeTabType.Barbarian:
            case UpgradeTabType.Knight:
            case UpgradeTabType.Wizard:
            case UpgradeTabType.King:
                {
                    Mercenary mer = DataManager.Get.GetMercenaryData((AliseType)SelectTap, Main.Get.GetUserDataManager().GetAliseLv((AliseType)SelectTap));
                    if(mer != null)
                    {
                        SelectImg.spriteName = mer.res;
                        if (mer.level == 0)
                            SelectImg.color = Color.gray;
                        else
                            SelectImg.color = Color.white;
                    }

                    Mercenary next = DataManager.Get.GetMercenaryData((AliseType)SelectTap, Main.Get.GetUserDataManager().GetAliseLv((AliseType)SelectTap) + 1);

                    SetInfoValue(mer, next);
                    SetUpgradeBtn(mer.costupgrade, mer.costgem);
                    break;
                }
            case UpgradeTabType.Castle:
                {
                    Castle castle = DataManager.Get.GetCastleData(Main.Get.GetUserDataManager().GetCastleLv());
                    if(castle != null)
                    {
                        SelectImg.spriteName = "Top-Down Forest Tileset_Building - Castle";

                        if (castle.lv == 0)
                            SelectImg.color = Color.gray;
                        else
                            SelectImg.color = Color.white;
                    }

                    Castle next = DataManager.Get.GetCastleData(Main.Get.GetUserDataManager().GetCastleLv() + 1);
                    SetInfoValue(castle, next);
                    SetUpgradeBtn(castle.upgrade_cost, castle.gem_cost);
                    break;
                }
            case UpgradeTabType.Mana:
                {
                    Mana mana = DataManager.Get.GetManaData(Main.Get.GetUserDataManager().GetManaLv());
                    SelectImg.spriteName = "Cartoon RPG UI_Game Icon - Magic";
                    if (mana != null)
                    {
                        if (mana.lv == 0)
                            SelectImg.color = Color.gray;
                        else
                            SelectImg.color = Color.white;
                    }

                    Mana next = DataManager.Get.GetManaData(Main.Get.GetUserDataManager().GetManaLv() + 1);
                    SetInfoValue(mana, next);
                    SetUpgradeBtn(mana.upgrade_cost, 0);
                    break;
                }
            case UpgradeTabType.GunFire:
                {
                    Skill_GunFire gunfire = DataManager.Get.GetGunFire(Main.Get.GetUserDataManager().GetGunFireLv());
                    SelectImg.spriteName = "arrow_skill";
                    if (gunfire != null)
                    {
                        if (gunfire.lv == 0)
                            SelectImg.color = Color.gray;
                        else
                            SelectImg.color = Color.white;
                    }

                    Skill_GunFire next = DataManager.Get.GetGunFire(Main.Get.GetUserDataManager().GetGunFireLv() + 1);
                    SetInfoValue(gunfire, next);
                    SetUpgradeBtn(gunfire.upgrade_cost, 0);
                    break;
                }
        }
    }

    void SetInfoValue(Mercenary now, Mercenary next)
    {
        for (int i = 0; i < Status.Length; ++i)
        {
            Status[i].gameObject.SetActive(true);
        }

        for (int i = 0; i < Status_plus.Length; ++i)
        {
            Status_plus[i].gameObject.SetActive(true);
        }

        if (now.level == 0)
        {
            for (int i = 0; i < Status.Length; ++i)
            {
                Status[i].gameObject.SetActive(false);
            }
        }

        Status[(int)UpgradePopupStaus.lv].text = string.Format("Lv : {0}", now.level);
        Status[(int)UpgradePopupStaus.hp].text = string.Format("Hp : {0}", now.hp);
        Status[(int)UpgradePopupStaus.atk].text = string.Format("Atk : {0}", now.atk);
        Status[(int)UpgradePopupStaus.def].text = string.Format("Def : {0}", now.def);
        Status[(int)UpgradePopupStaus.crt].text = string.Format("Crt : {0}", now.crt);
        Status[(int)UpgradePopupStaus.spd].text = string.Format("Spd : {0}", now.spd);
        Status[(int)UpgradePopupStaus.mov].text = string.Format("Mov : {0}", now.movespd);

        if (null != next)
        {
            Status_plus[(int)UpgradePopupStaus.lv].text = string.Empty;
            SetPlusSateus(Status_plus[(int)UpgradePopupStaus.hp], now.hp, next.hp);
            SetPlusSateus(Status_plus[(int)UpgradePopupStaus.atk], now.atk, next.atk);
            SetPlusSateus(Status_plus[(int)UpgradePopupStaus.def], now.def, next.def);
            SetPlusSateus(Status_plus[(int)UpgradePopupStaus.crt], now.crt, next.crt);
            Status_plus[(int)UpgradePopupStaus.spd].text = string.Empty;
            Status_plus[(int)UpgradePopupStaus.mov].text = string.Empty;
        }
        else
        {
            for (int i = 0; i < Status_plus.Length; ++i)
            {
                Status_plus[i].gameObject.SetActive(false);
            }
        }

        for (int i = 0; i < Status_plus.Length; ++i)
        {
            Status_plus[i].transform.localPosition = new Vector3(Status[i].width + 5f, 0f, 0f);
        }
        StatusGrid.Reposition();
    }

    void SetInfoValue(Castle now, Castle next)
    {
        for(int i = 0; i < Status.Length; ++i)
        {
            Status[i].gameObject.SetActive(true);
        }

        for (int i = 0; i < Status_plus.Length; ++i)
        {
            Status_plus[i].gameObject.SetActive(true);
        }

        if (now.lv == 0)
        {
            for (int i = 0; i < Status.Length; ++i)
            {
                Status[i].gameObject.SetActive(false);
            }
        }

        Status[(int)UpgradePopupStaus.lv].text = string.Format("Lv : {0}", now.lv);
        Status[(int)UpgradePopupStaus.hp].text = string.Format("Hp : {0}", now.hp);
        Status[(int)UpgradePopupStaus.atk].gameObject.SetActive(false);
        Status[(int)UpgradePopupStaus.def].gameObject.SetActive(false);
        Status[(int)UpgradePopupStaus.crt].gameObject.SetActive(false);
        Status[(int)UpgradePopupStaus.spd].gameObject.SetActive(false);
        Status[(int)UpgradePopupStaus.mov].gameObject.SetActive(false);

        if (null != next)
        {
            Status_plus[(int)UpgradePopupStaus.lv].text = string.Empty;
            SetPlusSateus(Status_plus[(int)UpgradePopupStaus.hp], now.hp, next.hp);
        }
        else
        {
            for (int i = 0; i < Status_plus.Length; ++i)
            {
                Status_plus[i].gameObject.SetActive(false);
            }
        }

        for (int i = 0; i < Status_plus.Length; ++i)
        {
            Status_plus[i].transform.localPosition = new Vector3(Status[i].width + 5, 0f, 0f);
        }

        StatusGrid.Reposition();
    }

    void SetInfoValue(Mana now, Mana next)
    {
        for (int i = 0; i < Status.Length; ++i)
        {
            Status[i].gameObject.SetActive(true);
        }

        for (int i = 0; i < Status_plus.Length; ++i)
        {
            Status_plus[i].gameObject.SetActive(true);
        }

        if (now.lv == 0)
        {
            for (int i = 0; i < Status.Length; ++i)
            {
                Status[i].gameObject.SetActive(false);
            }
        }

        Status[(int)UpgradePopupStaus.lv].text = string.Format("Lv : {0}", now.lv);
        Status[(int)UpgradePopupStaus.hp].text = string.Format("Max : {0} ~ {1}", now.maxvalue[0], now.maxvalue[now.maxvalue.Length - 1]);
        Status[(int)UpgradePopupStaus.atk].text = string.Empty;
        Status[(int)UpgradePopupStaus.def].text = string.Format("Sec per charge : {0}", now.charge_sce);
        Status[(int)UpgradePopupStaus.crt].gameObject.SetActive(false);
        Status[(int)UpgradePopupStaus.spd].gameObject.SetActive(false);
        Status[(int)UpgradePopupStaus.mov].gameObject.SetActive(false);

        if (null != next)
        {
            Status_plus[(int)UpgradePopupStaus.lv].text = string.Empty;
            Status_plus[(int)UpgradePopupStaus.hp].text = string.Format("[00FF00] + {0} ~ {1}", next.maxvalue[0] - now.maxvalue[0],
                next.maxvalue[next.maxvalue.Length - 1] - now.maxvalue[now.maxvalue.Length -1 ]);
            Status_plus[(int)UpgradePopupStaus.atk].text = string.Empty;
            SetPlusSateus(Status_plus[(int)UpgradePopupStaus.def], now.charge_sce, next.charge_sce);
            Status_plus[(int)UpgradePopupStaus.crt].text = string.Empty;
            Status_plus[(int)UpgradePopupStaus.spd].text = string.Empty;
            Status_plus[(int)UpgradePopupStaus.mov].text = string.Empty;
        }
        else
        {
            for (int i = 0; i < Status_plus.Length; ++i)
            {
                Status_plus[i].gameObject.SetActive(false);
            }
        }

        for (int i = 0; i < Status_plus.Length; ++i)
        {
            Status_plus[i].transform.localPosition = new Vector3(Status[i].width + 5, 0f, 0f);
        }

        StatusGrid.Reposition();
    }

    void SetInfoValue(Skill_GunFire now, Skill_GunFire next)
    {
        for (int i = 0; i < Status.Length; ++i)
        {
            Status[i].gameObject.SetActive(true);
        }

        for (int i = 0; i < Status_plus.Length; ++i)
        {
            Status_plus[i].gameObject.SetActive(true);
        }

        if (now.lv == 0)
        {
            for (int i = 0; i < Status.Length; ++i)
            {
                Status[i].gameObject.SetActive(false);
            }
        }

        Status[(int)UpgradePopupStaus.lv].text = string.Format("Lv : {0}", now.lv);
        Status[(int)UpgradePopupStaus.hp].text = string.Format("Atk : {0}", now.atk);
        Status[(int)UpgradePopupStaus.atk].gameObject.SetActive(false);
        Status[(int)UpgradePopupStaus.def].gameObject.SetActive(false);
        Status[(int)UpgradePopupStaus.crt].gameObject.SetActive(false);
        Status[(int)UpgradePopupStaus.spd].gameObject.SetActive(false);
        Status[(int)UpgradePopupStaus.mov].gameObject.SetActive(false);

        if (null != next)
        {
            Status_plus[(int)UpgradePopupStaus.lv].text = string.Empty;
            SetPlusSateus(Status_plus[(int)UpgradePopupStaus.hp], now.atk, next.atk);
            Status_plus[(int)UpgradePopupStaus.atk].text = string.Empty;
            Status_plus[(int)UpgradePopupStaus.def].text = string.Empty;
            Status_plus[(int)UpgradePopupStaus.crt].text = string.Empty;
            Status_plus[(int)UpgradePopupStaus.spd].text = string.Empty;
            Status_plus[(int)UpgradePopupStaus.mov].text = string.Empty;
        }
        else
        {
            for (int i = 0; i < Status_plus.Length; ++i)
            {
                Status_plus[i].gameObject.SetActive(false);
            }
        }

        for (int i = 0; i < Status_plus.Length; ++i)
        {
            Status_plus[i].transform.localPosition = new Vector3(Status[i].width + 5, 0f, 0f);
        }
        StatusGrid.Reposition();
    }

    void SetPlusSateus(UILabel label, int now, int next)
    {
        int plus = next - now;

        if(plus > 0)
        {
            label.text = string.Format("[00FF00] + {0}", plus);
        }
        else
        {
            label.text = string.Empty;
        }
    }

    void SetUpgradeBtn(int gold_, int gem_)
    {
        if(gold_ == 0 && gem_ == 0)
        {
            RankUpBtn.gameObject.SetActive(false);
            LevelupBtn.gameObject.SetActive(false);
        }
        else if(gem_ > 0)
        {
            RankUpBtn.gameObject.SetActive(true);
            LevelupBtn.gameObject.SetActive(false);

            Rankup_cost.text = string.Format("{0:#,##0}", gem_);

            paytype = 1;
            pay = gem_;
        }
        else
        {
            RankUpBtn.gameObject.SetActive(false);
            LevelupBtn.gameObject.SetActive(true);

            Levelup_cost.text = string.Format("{0:#,##0}", gold_);

            paytype = 0;
            pay = gold_;
        }
    }

    void SetTabImg()
    {
        for(int i = 0; i < TabBtn_Imgs.Length; ++i)
        {
            if(i == (int)SelectTap)
            {
                TabBtn_Imgs[i].color = Color.white;
            }
            else
            {
                TabBtn_Imgs[i].color = Color.gray;
            }

            switch ((UpgradeTabType)i)
            {
                case UpgradeTabType.Warrior:
                case UpgradeTabType.Archer:
                case UpgradeTabType.Barbarian:
                case UpgradeTabType.Knight:
                case UpgradeTabType.Wizard:
                case UpgradeTabType.King:
                    {
                        Mercenary mer = DataManager.Get.GetMercenaryData((AliseType)i, Main.Get.GetUserDataManager().GetAliseLv((AliseType)i));
                        if (mer != null)
                        {
                            TabBtn_Imgs[i].spriteName = mer.res;
                        }
                        break;
                    }
                case UpgradeTabType.Castle:
                    {
                        Castle castle = DataManager.Get.GetCastleData(Main.Get.GetUserDataManager().GetCastleLv());
                        if (castle != null)
                        {
                            TabBtn_Imgs[i].spriteName = "Top-Down Forest Tileset_Building - Castle";
                        }
                        break;
                    }
                case UpgradeTabType.Mana:
                    {
                        Mana mana = DataManager.Get.GetManaData(Main.Get.GetUserDataManager().GetManaLv());
                        TabBtn_Imgs[i].spriteName = "Cartoon RPG UI_Game Icon - Magic";
                        break;
                    }
                case UpgradeTabType.GunFire:
                    {
                        Skill_GunFire gunfire = DataManager.Get.GetGunFire(Main.Get.GetUserDataManager().GetGunFireLv());
                        TabBtn_Imgs[i].spriteName = "arrow_skill";
                        break;
                    }
            }
        }
    }

    public void OnClickSelectWarrior()
    {
        SelectTap = UpgradeTabType.Warrior;
        SetInfo();
        SetTabImg();
    }

    public void OnClickSelectArcher()
    {
        SelectTap = UpgradeTabType.Archer;
        SetInfo();
        SetTabImg();
    }

    public void OnClickSelectBarbarian()
    {
        SelectTap = UpgradeTabType.Barbarian;
        SetInfo();
        SetTabImg();
    }

    public void OnClickSelectCastle()
    {
        SelectTap = UpgradeTabType.Castle;
        SetInfo();
        SetTabImg();
    }

    public void OnClickSelectGunFire()
    {
        SelectTap = UpgradeTabType.GunFire;
        SetInfo();
        SetTabImg();

    }

    public void OnClickSelectKing()
    {
        SelectTap = UpgradeTabType.King;
        SetInfo();
        SetTabImg();

    }

    public void OnClickSelectKnight()
    {
        SelectTap = UpgradeTabType.Knight;
        SetInfo();
        SetTabImg();
    }

    public void OnClickSelectMana()
    {
        SelectTap = UpgradeTabType.Mana;
        SetInfo();
        SetTabImg();
    }

    public void OnClickSelectWizard()
    {
        SelectTap = UpgradeTabType.Wizard;
        SetInfo();
        SetTabImg();
    }

    public void OnClickedUpGradeBtn()
    {
        if(Main.Get.GetUserDataManager().GetWave() < 21 &&
            (SelectTap == UpgradeTabType.Wizard || SelectTap == UpgradeTabType.Knight || SelectTap == UpgradeTabType.King))
        {
            PopupMessage popup = new PopupMessage();
            popup.Type = Enum.MessagePopupType.Ok;
            popup.Text = "왕국군은 원정중 입니다.\n21웨이브 부터 사용이 가능합니다.";
            popup.Ok_Text = "확인";
            PopupManager.Get.OpenPopup("Popup_Message", popup);
        }
        else if(paytype == 0 && Main.Get.GetUserDataManager().UseGold(pay))
        {
            Main.Get.GetUserDataManager().AddAliesLv(SelectTap, 1);
        }
        else if(paytype == 1 && Main.Get.GetUserDataManager().UseGem(pay))
        {
            Main.Get.GetUserDataManager().AddAliesLv(SelectTap, 1);
        }
        SetInfo();
        SetTabImg();
    }

    public override void Close()
    {
        base.Close();
    }

    public void OnClickedExit()
    {
        PopupManager.Get.ClosePopup("Popup_Upgrade");
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
