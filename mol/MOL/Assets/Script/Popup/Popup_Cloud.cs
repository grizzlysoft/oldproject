﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup_Cloud : Popup
{
    public UILabel Text;

    public UIButton ExitBtn;
    public UIButton SaveBtn;
    public UIButton LoadBtn;

    public UILabel Save_Text;
    public UILabel Load_Text;

    public override void FirstLoad()
    {
        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        base.SetData(data_);
    }

    public override void Open()
    {
        Text.text = "저장/로드 하시겠습니까?";
        Save_Text.text = "저장";
        Load_Text.text = "로드";
        base.Open();
    }

    public override void Close()
    {
        base.Close();
    }

    public void OnClickedSaveBtn()
    {
        PopupManager.Get.ClosePopup("Popup_Cloud");
        GPGS_Manager.Get.SaveToCloud();
    }

    public void OnClickedLoadBtn()
    {
        PopupManager.Get.ClosePopup("Popup_Cloud");
        GPGS_Manager.Get.LoadFromCloud();
    }

    public void OnClickedExitBtn()
    {
        PopupManager.Get.ClosePopup("Popup_Cloud");
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
