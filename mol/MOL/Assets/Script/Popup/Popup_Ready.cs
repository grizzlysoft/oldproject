﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class Popup_Ready : Popup
{
    public UILabel WaveText;

    public UILabel MonsterInfoText;
    public UILabel WaveRewardText;
    public UILabel BuffSelectText;

    public GameObject Gold;
    public UISprite CoinIcon;
    public UILabel WaveRewardGold;

    public GameObject Gem;
    public UISprite GemIcon;
    public UILabel WaveReward_Gem;

    public UIGrid RewardGrid;

    public UIGrid MonsterImgGrid;
    public UIPanel MonsterImgScroll_Panel;
    List<UISprite> MonsterImg = new List<UISprite>();
    
    public UIButton[] BuffSelectBtn = new UIButton[(int)BoostType.Max];
    public UISprite[] BuffSelectImg = new UISprite[(int)BoostType.Max];

    public UIButton StartBtn;
    public UIButton ExitBtn;
    public UILabel StartText;
    public UILabel ExitText;

    public UIButton NextBtn;
    public UIButton PrevBtn;

    int Wave = 0;

    public override void FirstLoad()
    {
        for(int i = 0; i < MonsterImgGrid.transform.childCount; ++i)
        {
            MonsterImg.Add(MonsterImgGrid.transform.GetChild(i).GetComponent<UISprite>());
        }

        base.FirstLoad();
    }

    public override void SetData(object data_)
    {
        base.SetData(data_);
    }

    public override void Open()
    {
        Wave = Main.Get.GetUserDataManager().GetWave();
        WaveInfo wave = DataManager.Get.GetWaveData(Wave);
        WaveText.text = "Wave " + wave.wave;

        if (wave.reward > 0)
        {
            Gold.SetActive(true);
            WaveRewardGold.text = string.Format("{0:#,##0}", wave.reward);
            Gold.GetComponent<UIGrid>().Reposition();
        }
        else
            Gold.SetActive(false);


        if (wave.reward_gem > 0 && Main.Get.GetUserDataManager().GetWaveClearCnt(Wave) == 0)
        {
            Gem.SetActive(true);
            WaveReward_Gem.text = string.Format("{0:#,##0}", wave.reward_gem);
            Gem.GetComponent<UIGrid>().Reposition();
        }
        else
            Gem.SetActive(false);

        RewardGrid.Reposition();

        List<string> res = FindMonRes(wave.monsterinfo);
        
        for(int i = 0; i < MonsterImg.Count; ++i)
        {
            if(i <= res.Count -1)
            {
                MonsterImg[i].gameObject.SetActive(true);
                MonsterImg[i].spriteName = res[i];
            }
            else
            {
                MonsterImg[i].gameObject.SetActive(false);
            }
        }

        MonsterImgGrid.Reposition();
        SetBuffUse();

        if (Wave == Main.Get.GetUserDataManager().GetWave())
            NextBtn.gameObject.SetActive(false);
        else
            NextBtn.gameObject.SetActive(true);

        if (Wave == 1)
            PrevBtn.gameObject.SetActive(false);
        else
            PrevBtn.gameObject.SetActive(true);

        MonsterImgScroll_Panel.depth = this.GetComponent<UIPanel>().depth + 1;
        
        base.Open();
    }

    public override void Close()
    {
        base.Close();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void ReFreshInfo()
    {
        WaveInfo wave = DataManager.Get.GetWaveData(Wave);
        WaveText.text = "Wave " + wave.wave;

        if (wave.reward > 0)
        {
            Gold.SetActive(true);
            WaveRewardGold.text = string.Format("{0:#,##0}", wave.reward);
            Gold.GetComponent<UIGrid>().Reposition();
        }
        else
            Gold.SetActive(false);


        if (wave.reward_gem > 0 && Main.Get.GetUserDataManager().GetWaveClearCnt(Wave) == 0)
        {
            Gem.SetActive(true);
            WaveReward_Gem.text = string.Format("{0:#,##0}", wave.reward_gem);
            Gem.GetComponent<UIGrid>().Reposition();
        }
        else
            Gem.SetActive(false);

        RewardGrid.Reposition();

        List<string> res = FindMonRes(wave.monsterinfo);

        for (int i = 0; i < MonsterImg.Count; ++i)
        {
            if (i <= res.Count - 1)
            {
                MonsterImg[i].gameObject.SetActive(true);
                MonsterImg[i].spriteName = res[i];
            }
            else
            {
                MonsterImg[i].gameObject.SetActive(false);
            }
        }

        MonsterImgGrid.Reposition();
        SetBuffUse();

        if (Wave == Main.Get.GetUserDataManager().GetWave())
            NextBtn.gameObject.SetActive(false);
        else
            NextBtn.gameObject.SetActive(true);

        if (Wave == 1)
            PrevBtn.gameObject.SetActive(false);
        else
            PrevBtn.gameObject.SetActive(true);
    }

    public void OnClickedNextBtn()
    {
        Wave++;
        ReFreshInfo();
    }

    public void OnClickedPrevBtn()
    {
        Wave--;
        ReFreshInfo();
    }

    public void OnClickedAtkBuff()
    {
        if (false == Main.Get.GetUserDataManager().UseBoost[(int)BoostType.Atk])
        {
            if (Main.Get.GetUserDataManager().UseBooster(BoostType.Atk))
            {
                Main.Get.GetUserDataManager().UseBoost[(int)BoostType.Atk] = true;
            }
            else
            {
                Main.Get.GetUserDataManager().UseBoost[(int)BoostType.Atk] = false;
            }
        }
        else
        {
            Main.Get.GetUserDataManager().AddBooster(BoostType.Atk, 1);
            Main.Get.GetUserDataManager().UseBoost[(int)BoostType.Atk] = false;
        }
        SetBuffUse();
    }

    public void OnClickedHpBuff()
    {
        if (false == Main.Get.GetUserDataManager().UseBoost[(int)BoostType.Hp])
        {
            if (Main.Get.GetUserDataManager().UseBooster(BoostType.Hp))
            {
                Main.Get.GetUserDataManager().UseBoost[(int)BoostType.Hp] = true;
            }
            else
            {
                Main.Get.GetUserDataManager().UseBoost[(int)BoostType.Hp] = false;
            }
        }
        else
        {
            Main.Get.GetUserDataManager().AddBooster(BoostType.Hp, 1);
            Main.Get.GetUserDataManager().UseBoost[(int)BoostType.Hp] = false;
        }
        SetBuffUse();
    }

    public void OnClickedDefBuff()
    {
        if (false == Main.Get.GetUserDataManager().UseBoost[(int)BoostType.Def])
        {
            if (Main.Get.GetUserDataManager().UseBooster(BoostType.Def))
            {
                Main.Get.GetUserDataManager().UseBoost[(int)BoostType.Def] = true;
            }
            else
            {
                Main.Get.GetUserDataManager().UseBoost[(int)BoostType.Def] = false;
            }
        }
        else
        {
            Main.Get.GetUserDataManager().AddBooster(BoostType.Def, 1);
            Main.Get.GetUserDataManager().UseBoost[(int)BoostType.Def] = false;
        }
        SetBuffUse();
    }

    public void OnClickedGoldBuff()
    {
        if (false == Main.Get.GetUserDataManager().UseBoost[(int)BoostType.Gold])
        {
            if (Main.Get.GetUserDataManager().UseBooster(BoostType.Gold))
            {
                Main.Get.GetUserDataManager().UseBoost[(int)BoostType.Gold] = true;
            }
            else
            {
                Main.Get.GetUserDataManager().UseBoost[(int)BoostType.Gold] = false;
            }
        }
        else
        {
            Main.Get.GetUserDataManager().AddBooster(BoostType.Gold, 1);
            Main.Get.GetUserDataManager().UseBoost[(int)BoostType.Gold] = false;
        }
        SetBuffUse();
    }

    public void OnClickedExit()
    {
        for(int i = 0; i < (int)BoostType.Max; ++i)
        {
            if(Main.Get.GetUserDataManager().UseBoost[i])
            {
                Main.Get.GetUserDataManager().AddBooster((BoostType)i, 1);
            }
            Main.Get.GetUserDataManager().UseBoost[i] = false;
        }

        PopupManager.Get.ClosePopup("Popup_Ready");
    }

    public void OnClickedStart()
    {
        Main.Get.GetUserDataManager().PlayWave = Wave;
        Main.Get.ChangeScene(Enum.SceneType.Ingame);
    }

    public void SetBuffUse()
    {
        for(int i = 0; i < BuffSelectImg.Length; ++i)
        {
            if(Main.Get.GetUserDataManager().UseBoost[i])
            {
                BuffSelectImg[i].color = Color.white;
            }
            else
            {
                BuffSelectImg[i].color = Color.gray;
            }
        }
    }

    List<string> FindMonRes(List<WaveMonsterInfo> list)
    {
        List<string> Data = new List<string>();

        for(int i = 0; i < list.Count; ++i)
        {
            Monster m = DataManager.Get.GetMonsterData((Enum.MonsterType)list[i].type, list[i].lv);

            if(false == Data.Contains(m.res))
            {
                Data.Add(m.res);
            }
        }

        return Data;
    }
}
