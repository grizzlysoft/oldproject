﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeArea : MonoBehaviour
{
    public CharacterObj Obj;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("range area on tag = " + other.tag);
        if(tag == "Aille_Range" && other.tag == "Enemy_Hit")
        {
            //Debug.Log(Obj.name);
            Obj.AddAttackTarget(other.GetComponent<CharacterObj>().GetObjID());
        }

        if (tag == "Enemy_Range" && other.tag == "Aille_Hit")
        {
            Obj.AddAttackTarget(other.GetComponent<CharacterObj>().GetObjID());
        }

        if( tag == "Enemy_Range" && other.tag == "Castle")
        {
            Obj.AddAttackTarget(0); //0은 성 오브젝트 id 고유값
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (Obj.GetNuckBack() && Obj.GetNuckBackFinsh())
        {
            if (tag == "Aille_Range" && other.tag == "Enemy_Hit")
            {
                Obj.AddAttackTarget(other.GetComponent<CharacterObj>().GetObjID());
            }

            if (tag == "Enemy_Range" && other.tag == "Aille_Hit")
            {
                Obj.AddAttackTarget(other.GetComponent<CharacterObj>().GetObjID());
            }

            if (tag == "Enemy_Range" && other.tag == "Castle")
            {
                Obj.AddAttackTarget(0); //0은 성 오브젝트 id 고유값
            }
        }


    }

    private void OnTriggerExit(Collider other)
    {
        //Debug.Log("range area on tag = " + other.tag);
        if (tag == "Aille_Range" && other.tag == "Enemy_Hit")
        {
            //Debug.Log(Obj.name);
            other.GetComponent<CharacterObj>().RemoveAttackTarget(Obj.GetObjID());
        }

        if (tag == "Enemy_Range" && other.tag == "Aille_Hit")
        {
            other.GetComponent<CharacterObj>().RemoveAttackTarget(Obj.GetObjID());
        }
    }
}
