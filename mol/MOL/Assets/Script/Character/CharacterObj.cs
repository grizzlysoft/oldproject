﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterObj : MonoBehaviour
{
    public List<TweenAlpha> AlphaList = new List<TweenAlpha>();
    List<int> TargetList = new List<int>();
    List<int> AggroList = new List<int>();

    Animator ani;
    Enum.AniState anistate = Enum.AniState.Warlk;

    public ParticleSystem PassiveEffect;
    PassiveSkill passive;

    //캐릭터 파라미터(걍 따로따로 넣어준다) 전투에 필요한것만
    int hp;
    int atk;
    int def;
    float mov;
    int reward = 0;
    int Obj_Id;
    int MaxHp;

    float Ex_Atk = 0;
    float Ex_Def = 0;

    bool OnPassive = false;

    private void Awake()
    {
        //트윈알파 꺼줌
        for(int i = 0; i < AlphaList.Count; ++i)
        {
            AlphaList[i].enabled = false;
        }
    }

    // Use this for initialization
    void Start ()
    {
        ani = GetComponent<Animator>();

        //캐릭터가 죽었을때 오브젝트를 없에는 함수를 연결
        AlphaList[0].onFinished.Add(new EventDelegate(DestoryObj));
	}

    int layer_value = 0;

    /// <summary>
    /// 캐릭터의 위치가 겹치면 아래쪽에 있는 캐릭터를 맨 위로 보이기 하기 위해
    /// 이 캐릭터의 레이어값을 알아야한다
    /// </summary>
    /// <returns></returns>
    public int GetLavyerOder()
    {
        return layer_value;
    }

    /// <summary>
    /// 레이어 값을 조정에 보여지는 순서를 조정한다
    /// </summary>
    /// <param name="value_"></param>
    public void SetOrderLayer(int value_)
    {
        layer_value = AlphaList[0].GetComponent<SpriteRenderer>().sortingOrder + value_;
        for(int i = 0; i < AlphaList.Count; ++i)
        {
            AlphaList[i].GetComponent<SpriteRenderer>().sortingOrder += value_;
        }
    }

    public void SetOrderLayer2(int value_)
    {
        layer_value = value_;
        for (int i = 0; i < AlphaList.Count; ++i)
        {
            AlphaList[i].GetComponent<SpriteRenderer>().sortingOrder = value_;
        }
    }

    /// <summary>
    /// 스텟 세팅 함수
    /// </summary>
    /// <param name="h"> hp</param>
    /// <param name="a"> atk</param>
    /// <param name="d"> def</param>
    /// <param name="m"> move</param>
    /// <param name="id"> obj id</param>
    /// <param name="reward_"> reward gold(몹만 있다)</param>
    /// <param name="passiveid_">skill id (몹은 없으므로 기본값 0)</param>
    public void SetParameter(int h, int a, int d, float m, int id, int reward_, int passiveid_ = 0)
    {
        hp = h;
        MaxHp = h;
        atk = a;
        def = d;
        mov = m;
        Obj_Id = id;

        reward = reward_;

        if (passiveid_ != 0)
            passive = DataManager.Get.GetPassiveSkill(passiveid_);
    }
    
    bool NuckBack = false;
    bool NuckBackFinsh = false;

    public bool GetNuckBack()
    {
        return NuckBack;
    }

    public bool GetNuckBackFinsh()
    {
        return NuckBackFinsh;
    }
    // Update is called once per frame
    void Update ()
    {
        if (TargetList.Count > 0 && anistate != Enum.AniState.Die && anistate != Enum.AniState.NuckBackStart
            && anistate != Enum.AniState.NuckBack_Ing && anistate != Enum.AniState.NuckBackEnd && anistate != Enum.AniState.Battle)
        {
            anistate = Enum.AniState.Battle;
            ani.Play("Attacking");
            ani.SetInteger("state", 1);
        }

        if ((anistate == Enum.AniState.Warlk || anistate == Enum.AniState.Move) && Time.timeScale != 0)
        {
            if (tag == "Aille_Hit" && transform.localPosition.x < IngameManager.Get.AliesStopPoint.localPosition.x)
            {
                transform.localPosition = new Vector3(transform.localPosition.x + mov, transform.localPosition.y, 0);
            }
            else if(tag == "Enemy_Hit")
            {
                transform.localPosition = new Vector3(transform.localPosition.x - mov, transform.localPosition.y, 0);
            }
        }

        if(anistate == Enum.AniState.NuckBackStart && Time.timeScale != 0)
        {
            if (tag == "Aille_Hit" && transform.localPosition.x > IngameManager.Get.AliesPoint.localPosition.x)
            {
                transform.localPosition = new Vector3(transform.localPosition.x - 2f, transform.localPosition.y + 1.5f, 0);
            }
            else if (tag == "Enemy_Hit" && transform.localPosition.x < IngameManager.Get.EnemyPoint.localPosition.x)
            {
                transform.localPosition = new Vector3(transform.localPosition.x + 2f, transform.localPosition.y + 1.5f, 0);
            }
        }

        if (anistate == Enum.AniState.NuckBack_Ing && Time.timeScale != 0)
        {
            if (tag == "Aille_Hit" && transform.localPosition.x > IngameManager.Get.AliesPoint.localPosition.x)
            {
                transform.localPosition = new Vector3(transform.localPosition.x - 2f, transform.localPosition.y - 1.5f, 0);
            }
            else if (tag == "Enemy_Hit" && transform.localPosition.x < IngameManager.Get.EnemyPoint.localPosition.x)
            {
                transform.localPosition = new Vector3(transform.localPosition.x + 2f, transform.localPosition.y - 1.5f, 0);
            }
        }

        

        if (hp < MaxHp / 2 && NuckBack == false)
        {
            anistate = Enum.AniState.NuckBackStart;
            ani.Play("JumpLoop");
            Invoke("NuckBackIng", 0.3f);
            TargetList.Clear();
            NuckBack = true;
        }

        if (hp <= 0)
        {
            //if (tag == "Enemy_Hit") Debug.Log("죽어야됨");
            GetComponent<BoxCollider>().enabled = false;
            anistate = Enum.AniState.Die;
            ani.SetInteger("state", 3);
        }
    }

    void NuckBackIng()
    {
        CancelInvoke("NuckBackIng");
        Invoke("NuckBackEnd", 0.3f);
        anistate = Enum.AniState.NuckBack_Ing;
    }

    void NuckBackEnd()
    {
        CancelInvoke("NuckBackEnd");
        
        anistate = Enum.AniState.Move;
        ani.SetInteger("state", 2);
        NuckBackFinsh = true;
    }

    void PassiveOn()
    {
        if (anistate == Enum.AniState.Die)
            return;

        OnPassive = true;
        if(null != PassiveEffect)
            PassiveEffect.gameObject.SetActive(true);
        
        switch (passive.Parametertype)
        {
            case Enum.PassiveType.Atk:
                Ex_Atk = (float)atk * ((float)passive.Stat_Per * 0.01f);
                SetParticleColor(Color.red);
                break;
            case Enum.PassiveType.HP_Def:
                Ex_Def = (float)atk * ((float)passive.Stat_Per * 0.01f);
                SetParticleColor(Color.green);
                break;
            case Enum.PassiveType.AtkSpd:
                ani.SetFloat("AttackSpeed", 1.0f * ((float)passive.Stat_Per * 0.01f));
                SetParticleColor(Color.blue);
                break;
        }

        SkillName skillname = (GameObject.Instantiate(Resources.Load("Prefab/SkillName")) as GameObject).GetComponent<SkillName>();
        skillname.transform.parent = this.transform;
      
        skillname.SetText(passive.Text);

        Invoke("PassiveOff", passive.Duration);
    }

    public void ApplyPassive(PassiveSkill skill_)
    {
        if (OnPassive == true || anistate == Enum.AniState.Die) return;

        OnPassive = true;
        PassiveEffect.gameObject.SetActive(true);

        switch (skill_.Parametertype)
        {
            case Enum.PassiveType.Atk:
                Ex_Atk = (float)atk * ((float)skill_.Stat_Per * 0.01f);
                SetParticleColor(Color.red);
                break;
            case Enum.PassiveType.HP_Def:
                Ex_Def = (float)atk * ((float)skill_.Stat_Per * 0.01f);
                SetParticleColor(Color.green);
                break;
            case Enum.PassiveType.AtkSpd:
                ani.SetFloat("AttackSpeed", 1.0f * ((float)skill_.Stat_Per * 0.01f));
                SetParticleColor(Color.blue);
                break;
        }

        Invoke("PassiveOff", skill_.Duration);
    }

    void SetParticleColor(Color color_)
    {
        var col = PassiveEffect.colorOverLifetime;
        col.enabled = true;

        Gradient grad = new Gradient();
        grad.SetKeys(new GradientColorKey[] { new GradientColorKey(color_, 0.0f), new GradientColorKey(color_, 1.0f) }, new GradientAlphaKey[] { new GradientAlphaKey(1.0f, 0.0f), new GradientAlphaKey(0.0f, 1.0f) });

        col.color = grad;
    }

    void PassiveOff()
    {
        CancelInvoke("PassiveOff");
        OnPassive = false;
        PassiveEffect.gameObject.SetActive(false);
        ani.SetFloat("AttackSpeed", 1.0f);
        Ex_Atk = 0;
        Ex_Def = 0;
    }

    public void PlayTaunt()
    {
        anistate = Enum.AniState.Taunt;
        ani.Play("Taunt");
    }

    public void AddAggroList(int id_)
    {
        AggroList.Add(id_);
    }

    public void RemoveAggroList(int id_)
    {
        int removeindex = -1;
        for(int i = 0; i < TargetList.Count; ++i)
        {
            if(TargetList[i] == id_)
            {
                removeindex = i;
                break;
            }
        }

        if(removeindex != -1)
        {
            TargetList.RemoveAt(removeindex);
        }

        if(TargetList.Count <= 0)
        {
            anistate = Enum.AniState.Move;
            ani.SetInteger("state", 2);
        }
    }

    public int GetObjID()
    {
        return Obj_Id;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(tag == other.tag && other.transform.localPosition.y > transform.localPosition.y)
        {
            SetOrderLayer(other.GetComponent<CharacterObj>().GetLavyerOder() + 1);
        }
    }

    public void AddAttackTarget(int id_)
    {
        if (TargetList.Count > 0 && TargetList[0] == 0 && id_ != 0)
        {
            TargetList[0] = id_;
            TargetList.Add(0);
        }
        else
        {
            TargetList.Add(id_);
        }

        CharacterObj c = IngameManager.Get.FindCharacter(id_);
        if(c != null)
            c.AddAggroList(Obj_Id);
    }

    public void RemoveAttackTarget(int id)
    {
        int index = -1;

        for(int i = 0; i < TargetList.Count; ++i)
        {
            if(id == TargetList[i])
            {
                index = i;
                break;
            }
        }

        if (index != -1)
            TargetList.RemoveAt(index);
    }

    //근거리 공격
    public void Attack()
    {
        //Debug.Log("target id = " + TargetList[0]);
        if (TargetList.Count <= 0) return;

        CharacterObj c = IngameManager.Get.FindCharacter(TargetList[0]);
        if (c != null)
            c.Damage(atk + (int)Ex_Atk);
        else if (TargetList[0] != 0)
        {
            TargetList.Remove(0);
        }
        else // 이때가 성 공격이다
            IngameManager.Get.AttackCastle(atk + (int)Ex_Atk);

        SoundManager.Get.PlaySfx(Enum.Sfx.Hit);

        if (false == OnPassive && passive != null && Passive_Dice())
        {
            switch(passive.Target)
            {
                case Enum.PassiveTarget.One:
                    PassiveOn();
                    break;
                case Enum.PassiveTarget.All:
                    IngameManager.Get.PassiveAllAille(passive);
                    SkillName skillname = (GameObject.Instantiate(Resources.Load("Prefab/SkillName")) as GameObject).GetComponent<SkillName>();
                    skillname.transform.parent = this.transform;

                    skillname.SetText(passive.Text);
                    break;
            }
        }
    }

    //활공격
    public void Shot()
    {
        if (TargetList.Count > 0)
        {
            CharacterObj c = IngameManager.Get.FindCharacter(TargetList[0]);
            if (c != null)
            {
                GameObject arrow = GameObject.Instantiate(Resources.Load("Prefab/Arrow")) as GameObject;

                arrow.transform.parent = IngameManager.Get.BgRoot;
                arrow.transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, 0f);
                arrow.transform.localPosition = new Vector3(this.transform.localPosition.x, -96f, 0);
                arrow.GetComponent<Arrow>().SetPos(arrow.transform.localPosition, c.transform.localPosition, atk + (int)Ex_Atk, TargetList[0]);

                if (false == OnPassive && passive != null && Passive_Dice())
                {
                    switch (passive.Target)
                    {
                        case Enum.PassiveTarget.One:
                            PassiveOn();
                            break;
                        case Enum.PassiveTarget.All:
                            IngameManager.Get.PassiveAllAille(passive);
                            SkillName skillname = (GameObject.Instantiate(Resources.Load("Prefab/SkillName")) as GameObject).GetComponent<SkillName>();
                            skillname.transform.parent = this.transform;

                            skillname.SetText(passive.Text);
                            break;
                    }
                }
            }
        }
    }

    public void Spell()
    {
        if (TargetList.Count > 0)
        {
            CharacterObj c = IngameManager.Get.FindCharacter(TargetList[0]);
            if (c != null)
            {
                if (tag == "Aille_Hit")
                {
                    GameObject Spell = GameObject.Instantiate(Resources.Load("Prefab/Spell")) as GameObject;

                    Spell.transform.parent = IngameManager.Get.BgRoot;
                    Spell.transform.localScale = new Vector3(transform.localScale.x * 2, transform.localScale.y * 2, 0f);
                    Spell.transform.localPosition = new Vector3(this.transform.localPosition.x, -96f, 0);
                    Spell.GetComponent<Spell>().SetPermater(Spell.transform.localPosition, c.transform.localPosition, atk + (int)Ex_Atk);
                }
                else
                {
                    GameObject Spell = GameObject.Instantiate(Resources.Load("Prefab/E_Spell")) as GameObject;

                    Spell.transform.parent = IngameManager.Get.BgRoot;
                    Spell.transform.localScale = new Vector3(transform.localScale.x * 2, transform.localScale.y * 2, 0f);
                    Spell.transform.localPosition = new Vector3(this.transform.localPosition.x, -96f, 0);
                    Spell.GetComponent<E_Spell>().SetPermater(Spell.transform.localPosition, c.transform.localPosition, atk + (int)Ex_Atk);
                }
            }
            else if (TargetList[0] != 0)
            {
                TargetList.Remove(0);
            }
            else // 이때가 성 공격이다, 성공격은 몬스터만 한다
            {
                GameObject Spell = GameObject.Instantiate(Resources.Load("Prefab/E_Spell")) as GameObject;

                Spell.transform.parent = IngameManager.Get.BgRoot;
                Spell.transform.localScale = new Vector3(transform.localScale.x * 2, transform.localScale.y * 2, 0f);
                Spell.transform.localPosition = new Vector3(this.transform.localPosition.x, -96f, 0);
                Spell.GetComponent<E_Spell>().SetPermater(Spell.transform.localPosition, IngameManager.Get.Castle.localPosition, atk + (int)Ex_Atk);
            }

            //페시브 발동
            if (false == OnPassive && passive != null && Passive_Dice())
            {
                switch (passive.Target)
                {
                    case Enum.PassiveTarget.One:
                        PassiveOn();
                        break;
                    case Enum.PassiveTarget.All:
                        IngameManager.Get.PassiveAllAille(passive);
                        SkillName skillname = (GameObject.Instantiate(Resources.Load("Prefab/SkillName")) as GameObject).GetComponent<SkillName>();
                        skillname.transform.parent = this.transform;

                        skillname.SetText(passive.Text);
                        break;
                }
            }
        }
    }

    public void Damage(int atk)
    {
        int damage = atk - (def + (int)Ex_Def);

        if(damage <= 0)
        {
            damage = 1;
        }
#if UNITY_EDITOR
        DamageFont font = (GameObject.Instantiate(Resources.Load("Prefab/DamageFont")) as GameObject).GetComponent<DamageFont>();
        font.transform.parent = this.transform;

        if (tag == "Enemy_Hit")
            font.transform.localScale = new Vector3(-0.1f, 0.1f, 0.1f);
        else
            font.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);

        font.SetDamage(damage);
#endif
        hp -= damage;
    }

    public void Dying()
    {
        for (int i = 0; i < AlphaList.Count; ++i)
        {
            AlphaList[i].from = 1;
            AlphaList[i].to = 0;
            AlphaList[i].PlayForward();
        }

        if (reward > 0)
        {
            Coin coin = GameObject.Instantiate(Resources.Load("Prefab/Coin") as GameObject).GetComponent<Coin>();
            coin.transform.parent = IngameManager.Get.BgRoot;
            coin.transform.localScale = Vector3.one;
            coin.transform.localPosition = this.transform.localPosition;
            coin.SetData(IngameManager.Get.GoldDestPoint.position, reward);
        }

        //Debug.Log("Dying id = " + Obj_Id);
    }

    public void CallDying()
    {
        for(int i = 0; i < AggroList.Count; ++i)
        {
            CharacterObj c = IngameManager.Get.FindCharacter(AggroList[i]);
            if(c != null)
                c.RemoveAggroList(Obj_Id);
        }

        IngameManager.Get.RemoveCharList(Obj_Id);
        //Debug.Log("CallDying id = " + Obj_Id);
    }

    public Enum.AniState GetAnistate()
    {
        return anistate;
    }

    bool Passive_Dice()
    {
        int num = Random.Range(0, 100);

        if (num <= passive.Probability)
            return true;
        else
            return false;
    }

    void DestoryObj()
    {
        Destroy(this.gameObject);
    }
}
