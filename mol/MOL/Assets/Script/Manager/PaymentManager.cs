﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class PaymentManager : Singleton<PaymentManager>
{
    public void BuyPrice(Shop_PayType type_, int value_, List<int> reward_id_, string store_Id_)
    {
        switch(type_)
        {
            case Shop_PayType.Cash:
                if (store_Id_ == "start_package" && Main.Get.GetUserDataManager().BuyStartPackage != 0)
                {
                    PopupMessage popup = new PopupMessage();
                    popup.Type = MessagePopupType.Ok;
                    popup.Text = "해당 상품은 한번만\n구매하실수 있습니다.";
                    popup.Ok_Text = "확인";
                    PopupManager.Get.OpenPopup("Popup_Message", popup);
                }
                else
                {
                    if (store_Id_ == "start_package")
                    {
                        Main.Get.GetUserDataManager().BuyStartPackage = 1;
                        PlayerPrefs.SetInt("BuyStartPackage", 1);
                    }
                
                    Purchasing.Get.Buy(store_Id_);
                }
                break;
            case Shop_PayType.Ads:
                AdsManager.Get.ShowRewardedAd(reward_id_);
                break;
            case Shop_PayType.Gem:
                if(Main.Get.GetUserDataManager().UseGem(value_))
                {
                    for (int i = 0; i < reward_id_.Count; ++i)
                    {
                        PayPrice(reward_id_[i]);
                    }

                    PopupMessage popup = new PopupMessage();
                    popup.Type = MessagePopupType.Ok;
                    popup.Text = "상품이 성공적으로 구매 되었습니다";
                    popup.Ok_Text = "확인";
                    PopupManager.Get.OpenPopup("Popup_Message", popup);
                }
                break;
            case Shop_PayType.Gold:
                if(Main.Get.GetUserDataManager().UseGold(value_))
                {
                    for (int i = 0; i < reward_id_.Count; ++i)
                    {
                        PayPrice(reward_id_[i]);
                    }

                    PopupMessage popup = new PopupMessage();
                    popup.Type = MessagePopupType.Ok;
                    popup.Text = "상품이 성공적으로 구매 되었습니다";
                    popup.Ok_Text = "확인";
                    PopupManager.Get.OpenPopup("Popup_Message", popup);
                }
                break;
        }

        if(Main.Get.GetNowScene() == SceneType.Lobby)
        {
            LobbyUI.Get.RefreshMoney();
        }
    }

    /// <summary>
    /// 상품지급
    /// </summary>
    /// <param name="id_">리워드 시트 id</param>
    public string PayPrice(int id_)
    {
        ShopReward reward = DataManager.Get.GetShopReward(id_);
        string s = string.Empty;
        switch(reward.Type)
        {
            case RewardDataType.Gold:
                Main.Get.GetUserDataManager().AddGold(reward.Value);
                s += reward.Value + "골드";
                break;
            case RewardDataType.Gem:
                Main.Get.GetUserDataManager().AddGem(reward.Value);
                s += "잼 " + reward.Value + "개";
                break;
            case RewardDataType.Atk_boost:
                Main.Get.GetUserDataManager().AddBooster(BoostType.Atk, reward.Value);
                s += "공격강화 " + reward.Value + "개";
                break;
            case RewardDataType.Def_boost:
                Main.Get.GetUserDataManager().AddBooster(BoostType.Def, reward.Value);
                s += "방어강화 " + reward.Value + "개";
                break;
            case RewardDataType.Hp_boost:
                Main.Get.GetUserDataManager().AddBooster(BoostType.Hp, reward.Value);
                s += "체력강화 " + reward.Value + "개";
                break;
            case RewardDataType.Gold_bosst:
                Main.Get.GetUserDataManager().AddBooster(BoostType.Gold, reward.Value);
                s += "골드강화 " + reward.Value + "개";
                break;
        }

        return s;
    }

    public void PayPrice(string id_)
    {
        List<int> reward_id =  DataManager.Get.GetShopRewardList(id_);

        for(int i = 0; i < reward_id.Count; ++i)
        {
            PayPrice(reward_id[i]);
        }

        PopupMessage popup = new PopupMessage();
        popup.Type = MessagePopupType.Ok;
        popup.Text = "상품이 성공적으로 구매 되었습니다";
        popup.Ok_Text = "확인";

        PopupManager.Get.OpenPopup("Popup_Message", popup);

        PopupManager.Get.FindPopup("Popup_Shop").GetComponent<Popup_Shop>().StartPackageHide();
    }

}
