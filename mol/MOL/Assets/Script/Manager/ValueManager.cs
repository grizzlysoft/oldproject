﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValueManager : Singleton<ValueManager>
{
    private int Gold;
    private int Gem;

    public void SetGold(int gold)
    {
        Gold = gold;
    }

    public void AddGold(int add)
    {
        Gold += add;
    }

    public int GetGold()
    {
        return Gold;
    }

    /// <summary>
    /// 빼기
    /// </summary>
    /// <param name="sub">뺄 골드</param>
    public void SubGold(int sub)
    {
        Gold -= sub;
    }

  
    ////////////////////////////////////////////
    ////////////////////////////////////////////

    public void SetGem(int gem)
    {
        Gem = gem;
    }

    public void AddGem(int add)
    {
        Gem += add;
    }

    public int GetGem()
    {
        return Gem;
    }

    /// <summary>
    /// 빼기
    /// </summary>
    /// <param name="sub">뺄 골드</param>
    public void SubGem(int sub)
    {
        Gem -= sub;
    }
}


