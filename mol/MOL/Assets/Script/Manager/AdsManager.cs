﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using GoogleMobileAds.Api;

public class AdsManager : Singleton<AdsManager>
{
    private string
        androidGameId = "1475011",
        iosGameId = "1350598";

    [SerializeField]
    private bool testMode;

    List<int> Ad_Reward = null;

    void Start()
    {
        string gameId = null;

#if UNITY_ANDROID
        gameId = androidGameId;
#elif UNITY_IOS
        gameId = iosGameId;
#endif

        if (Advertisement.isSupported && !Advertisement.isInitialized)
        {
            Advertisement.Initialize(gameId, testMode);
        }

        //전면광고
        RequestBanner();
    }

    public void ShowAd()
    {
        if (Advertisement.IsReady())
        {
            //Debug.Log("광고 시청 로딩");
            Advertisement.Show();
        }
    }
    
    public void ShowRewardedAd(List<int>reward_id_)
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            Ad_Reward = reward_id_;
            //Debug.Log("광고 시청 로딩");
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
    }

    public void ShowRewardedAd_RewardPopup()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            //Debug.Log("광고 시청 로딩");
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                //Debug.Log("The ad was successfully shown.");
                if (Main.Get.GetNowScene() == Enum.SceneType.Ingame)
                {
                    IngameManager.Get.RewardDouble();
                }
                else
                {
                    string s = string.Empty;
                    for (int i = 0; i < Ad_Reward.Count; ++i)
                    {
                        s += PaymentManager.Get.PayPrice(Ad_Reward[i]);
                        if (i != Ad_Reward.Count - 1) s += "\n";
                    }

                    PopupMessage popup = new PopupMessage();
                    popup.Type = Enum.MessagePopupType.Ok;
                    popup.Text = s + " 획득";
                    popup.Ok_Text = "확인";
                    PopupManager.Get.OpenPopup("Popup_Message", popup);

                    Ad_Reward = null;
                }
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                //Popup_Manager.Get.ADFail.Show();
                break;
        }
    }

    //전면 광고
    BannerView bannerView;
    private void RequestBanner()
    {
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
        string adUnitId = "ca-app-pub-9910842226837850/8547898770";
#elif UNITY_IPHONE
        string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the banner with the request.
        bannerView.LoadAd(request);
    }

    private void OnDestroy()
    {
        bannerView.Destroy();
    }

}
