﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class UserData
{
    public int Gold = 0;
    public int Gem = 0;

    //기본 용병
    public int Archer_lv = 1;
    public int Warrior_lv = 1;
    public int Barbarian_lv = 1;
    
    //해금 용병
    public int Wizard_lv = 0;
    public int King_lv = 0;
    public int Knight_lv = 0;

    //스킬 및 시설
    public int Castle_lv = 1;
    public int GunFire_lv = 1;
    public int Tower_lv = 0;
    public int Mana_lv = 1;

    //현재 wave
    public int Wave = 1;

    //boseter
    public int[] Booster = new int[(int)BoostType.Max];
}

/// <summary>
/// userdata 관리 클레스 
/// 매니저 클레스이지만 메인에 선언해서 사용하도록 하자
/// 오브젝트 만들기 귀찮
/// </summary>
public class UserDataManager
{
    private UserData user = new UserData();

    public bool[] UseBoost = new bool[(int)BoostType.Max];

    public int loseCnt = 0;

    public int Facebook_GetBonus = 0;

    public int BuyStartPackage = 0;

    public List<int> WaveClearCnt = new List<int>();

    /// <summary>
    /// 인게임 들어갈때 불러와야할 웨이브
    /// </summary>
    public int PlayWave = 0;

    public void CreateWaveClearCnt()
    {
        int cnt = DataManager.Get.GetWaveDataCnt();
        for (int i = 0; i < cnt; ++i)
        {
            WaveClearCnt.Add(0);
        }

        LoadWaveClearCnt();
    }

    void LoadWaveClearCnt()
    {
        for(int i = 0; i < WaveClearCnt.Count; ++i)
        {
            if(PlayerPrefs.HasKey("WaveClear" + i))
            {
                WaveClearCnt[i] = PlayerPrefs.GetInt("WaveClear" + i);
            }
        }
    }

    public void PlusWaveClearCnt(int wave)
    {
        WaveClearCnt[wave - 1] = 1;

        PlayerPrefs.SetInt("WaveClear" + (wave - 1), WaveClearCnt[wave - 1]);
    }

    public int GetWaveClearCnt(int wave)
    {
        return WaveClearCnt[wave - 1];
    }

    public void LoadPlayPrefab()
    {
        if (PlayerPrefs.HasKey("Archer_lv"))
        {
            user.Archer_lv = PlayerPrefs.GetInt("Archer_lv");
        }

        if (PlayerPrefs.HasKey("Barbarian_lv"))
        {
            user.Barbarian_lv = PlayerPrefs.GetInt("Barbarian_lv");
        }

        if(PlayerPrefs.HasKey("Castle_lv"))
        {
            user.Castle_lv = PlayerPrefs.GetInt("Castle_lv");
        }

        if(PlayerPrefs.HasKey("GunFire_lv"))
        {
            user.GunFire_lv = PlayerPrefs.GetInt("GunFire_lv");
        }

        if(PlayerPrefs.HasKey("King_lv"))
        {
            user.King_lv = PlayerPrefs.GetInt("King_lv");
        }

        if(PlayerPrefs.HasKey("Knight_lv"))
        {
            user.Knight_lv = PlayerPrefs.GetInt("Knight_lv");
        }

        if(PlayerPrefs.HasKey("Mana_lv"))
        {
            user.Mana_lv = PlayerPrefs.GetInt("Mana_lv");
        }

        if(PlayerPrefs.HasKey("Tower_lv"))
        {
            user.Tower_lv = PlayerPrefs.GetInt("Tower_lv");
        }

        if(PlayerPrefs.HasKey("Warrior_lv"))
        {
            user.Warrior_lv = PlayerPrefs.GetInt("Warrior_lv");
        }

        if(PlayerPrefs.HasKey("Wizard_lv"))
        {
            user.Wizard_lv = PlayerPrefs.GetInt("Wizard_lv");
        }

        if(PlayerPrefs.HasKey("Wave"))
        {
            user.Wave = PlayerPrefs.GetInt("Wave");
        }

        if(PlayerPrefs.HasKey("Gold"))
        {
            user.Gold = PlayerPrefs.GetInt("Gold");
        }

        if(PlayerPrefs.HasKey("Gem"))
        {
            user.Gem = PlayerPrefs.GetInt("Gem");
        }

        if(PlayerPrefs.HasKey("AtkBoost"))
        {
            user.Booster[(int)BoostType.Atk] = PlayerPrefs.GetInt("AtkBoost");
        }

        if(PlayerPrefs.HasKey("HpBoost"))
        {
            user.Booster[(int)BoostType.Hp] = PlayerPrefs.GetInt("HpBoost");
        }

        if(PlayerPrefs.HasKey("DefBoost"))
        {
            user.Booster[(int)BoostType.Def] = PlayerPrefs.GetInt("DefBoost");
        }

        if(PlayerPrefs.HasKey("GoldBoost"))
        {
            user.Booster[(int)BoostType.Gold] = PlayerPrefs.GetInt("GoldBoost");
        }

        if(PlayerPrefs.HasKey("loseCnt"))
        {
            loseCnt = PlayerPrefs.GetInt("loseCnt");
        }

        if(PlayerPrefs.HasKey("Facebook_GetBonus"))
        {
            Facebook_GetBonus = PlayerPrefs.GetInt("Facebook_GetBonus");
        }

        if(PlayerPrefs.HasKey("BuyStartPackage"))
        {
            BuyStartPackage = PlayerPrefs.GetInt("BuyStartPackage");
        }
    }

    public void SavePlayerPrefab()
    {
        PlayerPrefs.SetInt("Wave", user.Wave);
        PlayerPrefs.SetInt("Warrior_lv", user.Warrior_lv);
        PlayerPrefs.SetInt("Archer_lv", user.Archer_lv);
        PlayerPrefs.SetInt("Barbarian_lv", user.Barbarian_lv);
        PlayerPrefs.SetInt("Wizard_lv", user.Wizard_lv);
        PlayerPrefs.SetInt("Knight_lv", user.Knight_lv);
        PlayerPrefs.SetInt("King_lv", user.King_lv);
        PlayerPrefs.SetInt("Castle_lv", user.Castle_lv);
        PlayerPrefs.SetInt("Mana_lv", user.Mana_lv);
        PlayerPrefs.SetInt("GunFire_lv", user.GunFire_lv);
        PlayerPrefs.SetInt("Gold", user.Gold);
        PlayerPrefs.SetInt("Gem", user.Gem);
        PlayerPrefs.SetInt("AtkBoost", user.Booster[(int)BoostType.Atk]);
        PlayerPrefs.SetInt("HpBoost", user.Booster[(int)BoostType.Hp]);
        PlayerPrefs.SetInt("DefBoost", user.Booster[(int)BoostType.Def]);
        PlayerPrefs.SetInt("GoldBoost", user.Booster[(int)BoostType.Gold]);
        PlayerPrefs.SetInt("loseCnt", loseCnt);

        for(int i = 0; i < WaveClearCnt.Count; ++i)
        {
            PlayerPrefs.SetInt("WaveClear" + i, WaveClearCnt[i]);
        }
    }

    public void SetUserDataToCloud(GPGS_Manager.CloudSaveData data_)
    {
        user.Wave = data_.Wave;
        user.Gold = data_.Gold;
        user.Gem = data_.Gem;

        user.Warrior_lv = data_.Warrior_lv;
        user.Barbarian_lv = data_.Barbarian_lv;
        user.Archer_lv = data_.Archer_lv;
        user.Wizard_lv = data_.Wizard_lv;
        user.Knight_lv = data_.Knight_lv;
        user.King_lv = data_.King_lv;

        user.Booster[(int)BoostType.Atk] = data_.AtkBoost;
        user.Booster[(int)BoostType.Hp] = data_.HpBoost;
        user.Booster[(int)BoostType.Def] = data_.DefBoost;
        user.Booster[(int)BoostType.Gold] = data_.GoldBoost;

        user.Castle_lv = data_.Castle_lv;
        user.Mana_lv = data_.Mana_lv;
        user.GunFire_lv = data_.GunFire_lv;
        user.Tower_lv = data_.Tower_lv;
        Facebook_GetBonus = data_.FaceBookBonus;
        BuyStartPackage = data_.BuyStartPackage;

        for(int i = 0; i < data_.ClearCnt.Length; ++i)
        {
            if(i < WaveClearCnt.Count)
            {
                WaveClearCnt[i] = int.Parse(data_.ClearCnt[i].ToString());
            }
        }

        SavePlayerPrefab();

        if (Main.Get.GetNowScene() == SceneType.Lobby)
        {
            LobbyUI.Get.RefreshMoney();
            LobbyUI.Get.AllRefreshBoostCnt();
        }
    }

    public int GetAliseLv(AliseType type)
    {
        switch(type)
        {
            case AliseType.Archer: return user.Archer_lv;
            case AliseType.Warrior: return user.Warrior_lv;
            case AliseType.Barbarian: return user.Barbarian_lv;
            case AliseType.Wizard: return user.Wizard_lv;
            case AliseType.King: return user.King_lv;
            case AliseType.Knight: return user.Knight_lv;
        }

        return 0;
    }

    public int GetTowerLv()
    {
        return user.Tower_lv;
    }

    public int GetCastleLv()
    {
        return user.Castle_lv;
    }

    public int GetGunFireLv()
    {
        return user.GunFire_lv;
    }

    public int GetManaLv()
    {
        return user.Mana_lv;
    }

    public int GetGold()
    {
        return user.Gold;
    }

    public int GetGem()
    {
        return user.Gem;
    }

    public int GetWave()
    {
        return user.Wave;
    }

    public void WaveStepUp()
    {
        if (user.Wave == DataManager.Get.GetLastWave()) return;

        user.Wave++;
        PlayerPrefs.SetInt("Wave", user.Wave);
        GPGS_Manager.Get.ReportScore(GPGSIds.leaderboard, user.Wave);
    }

    public void SubstitutionWave(int wave_)
    {
        user.Wave = wave_;
    }

    public void AddAliesLv(UpgradeTabType type_, int lv_)
    {
        switch(type_)
        {
            case UpgradeTabType.Warrior:
                user.Warrior_lv += lv_;
                PlayerPrefs.SetInt("Warrior_lv", user.Warrior_lv);
                break;
            case UpgradeTabType.Archer:
                user.Archer_lv += lv_;
                PlayerPrefs.SetInt("Archer_lv", user.Archer_lv);
                break;
            case UpgradeTabType.Barbarian:
                user.Barbarian_lv += lv_;
                PlayerPrefs.SetInt("Barbarian_lv", user.Barbarian_lv);
                break;
            case UpgradeTabType.Wizard:
                user.Wizard_lv += lv_;
                PlayerPrefs.SetInt("Wizard_lv", user.Wizard_lv);
                break;
            case UpgradeTabType.Knight:
                user.Knight_lv += lv_;
                PlayerPrefs.SetInt("Knight_lv", user.Knight_lv);
                break;
            case UpgradeTabType.King:
                user.King_lv += lv_;
                PlayerPrefs.SetInt("King_lv", user.King_lv);
                break;
            case UpgradeTabType.Castle:
                user.Castle_lv += lv_;
                PlayerPrefs.SetInt("Castle_lv", user.Castle_lv);
                break;
            case UpgradeTabType.Mana:
                user.Mana_lv += lv_;
                PlayerPrefs.SetInt("Mana_lv", user.Mana_lv);
                break;
            case UpgradeTabType.GunFire:
                user.GunFire_lv += lv_;
                PlayerPrefs.SetInt("GunFire_lv", user.GunFire_lv);
                break;
        }

        if(user.Wizard_lv > 0 && user.Knight_lv > 0 && user.King_lv > 0)
        {
            GPGS_Manager.Get.UnlockAchievement(GPGSIds.achievement_4);
        }
    }

    public void SubstitutionAilleLv(AliseType type_, int lv_)
    {
        switch (type_)
        {
            case AliseType.Warrior:
                user.Warrior_lv = lv_;
                PlayerPrefs.SetInt("Warrior_lv", user.Warrior_lv);
                break;
            case AliseType.Archer:
                user.Archer_lv = lv_;
                PlayerPrefs.SetInt("Archer_lv", user.Archer_lv);
                break;
            case AliseType.Barbarian:
                user.Barbarian_lv = lv_;
                PlayerPrefs.SetInt("Barbarian_lv", user.Barbarian_lv);
                break;
            case AliseType.Wizard:
                user.Wizard_lv = lv_;
                PlayerPrefs.SetInt("Wizard_lv", user.Wizard_lv);
                break;
            case AliseType.Knight:
                user.Knight_lv = lv_;
                PlayerPrefs.SetInt("Knight_lv", user.Knight_lv);
                break;
            case AliseType.King:
                user.King_lv = lv_;
                PlayerPrefs.SetInt("King_lv", user.King_lv);
                break;
        }
    }

    public void AddGold(int gold_)
    {
        if (user.Gold + gold_ <= 2100000000)
            user.Gold += gold_;
        else
            user.Gold = 2100000000;

        if (Main.Get.GetNowScene() == SceneType.Lobby)
        {
            LobbyUI.Get.RefreshMoney();
        }
        PlayerPrefs.SetInt("Gold", user.Gold);
    }

    public void AddGem(int gem_)
    {
        if (user.Gem + gem_ < 2100000000)
            user.Gem += gem_;
        else
            user.Gem = 2100000000;

        if (Main.Get.GetNowScene() == SceneType.Lobby)
        {
            LobbyUI.Get.RefreshMoney();
        }
        PlayerPrefs.SetInt("Gem", user.Gem);
    }

    public bool UseGold(int gold_)
    {
        if(gold_ > user.Gold)
        {
            //골드 부족 처리
            PopupMessage popup = new PopupMessage();
            popup.Text = "골드가 부족합니다.\n상점으로 이동하시겠습니까?";
            popup.Ok_Text = "확 인";
            popup.Cancel_Text = "취 소";
            popup.Type = MessagePopupType.Ok_Cancel;
            popup.Ok_Delegate = new EventDelegate(MoveShop_GoldTap);

            PopupManager.Get.OpenPopup("Popup_Message", popup);
            return false;
        }
        else
            user.Gold -= gold_;

        if (Main.Get.GetNowScene() == SceneType.Lobby)
        {
            LobbyUI.Get.RefreshMoney();
        }
        PlayerPrefs.SetInt("Gold", user.Gold);
        return true;
    }

    public bool UseGem(int gem_)
    {
        if (gem_ > user.Gem)
        {
            //잼 부족 처리
            PopupMessage popup = new PopupMessage();
            popup.Text = "잼이 부족합니다.\n상점으로 이동하시겠습니까?";
            popup.Ok_Text = "확 인";
            popup.Cancel_Text = "취 소";
            popup.Type = MessagePopupType.Ok_Cancel;
            popup.Ok_Delegate = new EventDelegate(MoveShop_GemTap);

            PopupManager.Get.OpenPopup("Popup_Message", popup);
            return false;
        }
        else
            user.Gem -= gem_;

        if (Main.Get.GetNowScene() == SceneType.Lobby)
        {
            LobbyUI.Get.RefreshMoney();
        }
        PlayerPrefs.SetInt("Gem", user.Gem);
        return true;
    }

    public void AddBooster(BoostType type, int value_)
    {
        user.Booster[(int)type] += value_;
        switch(type)
        {
            case BoostType.Atk:
                PlayerPrefs.SetInt("AtkBoost", user.Booster[(int)BoostType.Atk]);
                break;
            case BoostType.Hp:
                PlayerPrefs.SetInt("HpBoost", user.Booster[(int)BoostType.Hp]);
                break;
            case BoostType.Def:
                PlayerPrefs.SetInt("DefBoost", user.Booster[(int)BoostType.Def]);
                break;
            case BoostType.Gold:
                PlayerPrefs.SetInt("GoldBoost", user.Booster[(int)BoostType.Gold]);
                break;
        }

        if (Main.Get.GetNowScene() == SceneType.Lobby)
            LobbyUI.Get.ReFreshBoostCnt(type, user.Booster[(int)type]);
    }

    public bool UseBooster(BoostType type_)
    {
        user.Booster[(int)type_]--;

        if(user.Booster[(int)type_] >= 0)
        {
            switch (type_)
            {
                case BoostType.Atk:
                    PlayerPrefs.SetInt("AtkBoost", user.Booster[(int)BoostType.Atk]);
                    break;
                case BoostType.Hp:
                    PlayerPrefs.SetInt("HpBoost", user.Booster[(int)BoostType.Hp]);
                    break;
                case BoostType.Def:
                    PlayerPrefs.SetInt("DefBoost", user.Booster[(int)BoostType.Def]);
                    break;
                case BoostType.Gold:
                    PlayerPrefs.SetInt("GoldBoost", user.Booster[(int)BoostType.Gold]);
                    break;
            }
            if (Main.Get.GetNowScene() == SceneType.Lobby)
                LobbyUI.Get.ReFreshBoostCnt(type_, user.Booster[(int)type_]);
            return true;
        }
        else
        {
            user.Booster[(int)type_] = 0;
            PopupMessage popup = new PopupMessage();
            popup.Text = "버프아이탬이 부족합니다.\n상점으로 이동하시겠습니까?";
            popup.Ok_Text = "확 인";
            popup.Cancel_Text = "취 소";
            popup.Type = MessagePopupType.Ok_Cancel;
            popup.Ok_Delegate = new EventDelegate(MoveShop_BuffTap);

            PopupManager.Get.OpenPopup("Popup_Message", popup);
            return false;
        }
    }

    public void ReSetUseBoostFlag()
    {
        for(int i = 0; i < UseBoost.Length; ++i)
        {
            UseBoost[i] = false;
        }
    }

    public int GetBoosterCount(BoostType type)
    {
        return user.Booster[(int)type];
    }

    void MoveShop_GoldTap()
    {
        PopupManager.Get.AllClosePopup();
        PopupManager.Get.OpenPopup("Popup_Shop", Shop_Tap.Gold);
    }

    void MoveShop_GemTap()
    {
        PopupManager.Get.AllClosePopup();
        PopupManager.Get.OpenPopup("Popup_Shop", Shop_Tap.Gem);
    }

    void MoveShop_BuffTap()
    {
        PopupManager.Get.AllClosePopup();
        PopupManager.Get.OpenPopup("Popup_Shop", Shop_Tap.Buff);
    }
}
