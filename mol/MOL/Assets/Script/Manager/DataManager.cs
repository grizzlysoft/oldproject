﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : Singleton<DataManager>
{
    ParseUtile utlie = new ParseUtile();

    Dictionary<Enum.AliseType, List<Mercenary>> MercenaryData = new Dictionary<Enum.AliseType, List<Mercenary>>();

    Dictionary<Enum.MonsterType, List<Monster>> MonsterData = new Dictionary<Enum.MonsterType, List<Monster>>();

    List<Castle> CastleData = new List<Castle>();

    List<Tower> TowerData = new List<Tower>();
    List<Mana> ManaData = new List<Mana>();
    List<Skill_GunFire> GunFireData = new List<Skill_GunFire>();

    List<WaveInfo> WaveData = new List<WaveInfo>();

    List<ShopData> ShopItemData = new List<ShopData>();

    List<ShopReward> ShopRewardData = new List<ShopReward>();

    List<PassiveSkill> PassiveSkillData = new List<PassiveSkill>();

    public void DataLoad()
    {
        Load_ArcherData();
        Load_WarriorData();
        Load_BarbarianData();
        Load_WizardData();
        Load_KingData();
        Load_KnightData();

        Load_JakoData();
        Load_GoblinData();
        Load_OgreData();
        Load_CyclopsData();
        Load_GolemData();
        Load_SkullData();
        Load_VikingData();
        Load_PirateData();

        Load_CastleData();
        Load_TowerData();
        Load_ManaData();
        Load_GunFireData();

        Load_WaveData();

        Load_ShopData();
        Load_ShopRewardData();

        Load_PassiveSkillData();
    }

    public void Load_ArcherData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Archer", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');
            
            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                int cnt = 0;

                int level = int.Parse(stral[cnt]); cnt++;
                int hp = int.Parse(stral[cnt]); cnt++;
                int atk = int.Parse(stral[cnt]); cnt++;
                int def = int.Parse(stral[cnt]); cnt++;
                int crt = int.Parse(stral[cnt]); cnt++;
                int spd = int.Parse(stral[cnt]); cnt++;
                float movespd = float.Parse(stral[cnt]); cnt++;
                int cooltime = int.Parse(stral[cnt]); cnt++;
                int costingame = int.Parse(stral[cnt]); cnt++;
                int costupgrade = int.Parse(stral[cnt]); cnt++;
                int costgem = int.Parse(stral[cnt]); cnt++;
                string res = stral[cnt]; cnt++;
                int scale = int.Parse(stral[cnt]); cnt++;
                int passiveid = int.Parse(stral[cnt]); cnt++;
                
                Mercenary item = new Mercenary(level, hp, atk, def, crt, spd, movespd, res, scale, cooltime, costingame, costupgrade, costgem, passiveid);

                if (MercenaryData.ContainsKey(Enum.AliseType.Archer) == false)
                {
                    MercenaryData.Add(Enum.AliseType.Archer, new List<Mercenary>());
                }

                MercenaryData[Enum.AliseType.Archer].Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Archer");
        }

    }

    public void Load_WarriorData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Warrior", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');
            
            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                int cnt = 0;

                int level = int.Parse(stral[cnt]); cnt++;
                int hp = int.Parse(stral[cnt]); cnt++;
                int atk = int.Parse(stral[cnt]); cnt++;
                int def = int.Parse(stral[cnt]); cnt++;
                int crt = int.Parse(stral[cnt]); cnt++;
                int spd = int.Parse(stral[cnt]); cnt++;
                float movespd = float.Parse(stral[cnt]); cnt++;
                int cooltime = int.Parse(stral[cnt]); cnt++;
                int costingame = int.Parse(stral[cnt]); cnt++;
                int costupgrade = int.Parse(stral[cnt]); cnt++;
                int costgem = int.Parse(stral[cnt]); cnt++;
                string res = stral[cnt]; cnt++;
                int scale = int.Parse(stral[cnt]); cnt++;
                int passiveid = int.Parse(stral[cnt]); cnt++;

                Mercenary item = new Mercenary(level, hp, atk, def, crt, spd, movespd, res, scale, cooltime, costingame, costupgrade, costgem, passiveid);
                
                if (MercenaryData.ContainsKey(Enum.AliseType.Warrior) == false)
                {
                    MercenaryData.Add(Enum.AliseType.Warrior, new List<Mercenary>());
                }
               
                MercenaryData[Enum.AliseType.Warrior].Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Warrior");
        }

    }

    public void Load_BarbarianData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Barbarian", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');
            
            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                int cnt = 0;

                int level = int.Parse(stral[cnt]); cnt++;
                int hp = int.Parse(stral[cnt]); cnt++;
                int atk = int.Parse(stral[cnt]); cnt++;
                int def = int.Parse(stral[cnt]); cnt++;
                int crt = int.Parse(stral[cnt]); cnt++;
                int spd = int.Parse(stral[cnt]); cnt++;
                float movespd = float.Parse(stral[cnt]); cnt++;
                int cooltime = int.Parse(stral[cnt]); cnt++;
                int costingame = int.Parse(stral[cnt]); cnt++;
                int costupgrade = int.Parse(stral[cnt]); cnt++;
                int costgem = int.Parse(stral[cnt]); cnt++;
                string res = stral[cnt]; cnt++;
                int scale = int.Parse(stral[cnt]); cnt++;
                int passiveid = int.Parse(stral[cnt]); cnt++;

                Mercenary item = new Mercenary(level, hp, atk, def, crt, spd, movespd, res, scale, cooltime, costingame, costupgrade, costgem, passiveid);

                if (MercenaryData.ContainsKey(Enum.AliseType.Barbarian) == false)
                {
                    MercenaryData.Add(Enum.AliseType.Barbarian, new List<Mercenary>());
                }

                MercenaryData[Enum.AliseType.Barbarian].Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Barbarian");
        }

    }

    public void Load_WizardData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Wizard", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');
            
            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                int cnt = 0;

                int level = int.Parse(stral[cnt]); cnt++;
                int hp = int.Parse(stral[cnt]); cnt++;
                int atk = int.Parse(stral[cnt]); cnt++;
                int def = int.Parse(stral[cnt]); cnt++;
                int crt = int.Parse(stral[cnt]); cnt++;
                int spd = int.Parse(stral[cnt]); cnt++;
                float movespd = float.Parse(stral[cnt]); cnt++;
                int cooltime = int.Parse(stral[cnt]); cnt++;
                int costingame = int.Parse(stral[cnt]); cnt++;
                int costupgrade = int.Parse(stral[cnt]); cnt++;
                int costgem = int.Parse(stral[cnt]); cnt++;
                string res = stral[cnt]; cnt++;
                int scale = int.Parse(stral[cnt]); cnt++;
                int passiveid = int.Parse(stral[cnt]); cnt++;

                Mercenary item = new Mercenary(level, hp, atk, def, crt, spd, movespd, res, scale, cooltime, costingame, costupgrade, costgem, passiveid);

                if (MercenaryData.ContainsKey(Enum.AliseType.Wizard) == false)
                {
                    MercenaryData.Add(Enum.AliseType.Wizard, new List<Mercenary>());
                }

                MercenaryData[Enum.AliseType.Wizard].Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Wizard");
        }

    }

    public void Load_KingData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/King", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');
            
            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                int cnt = 0;

                int level = int.Parse(stral[cnt]); cnt++;
                int hp = int.Parse(stral[cnt]); cnt++;
                int atk = int.Parse(stral[cnt]); cnt++;
                int def = int.Parse(stral[cnt]); cnt++;
                int crt = int.Parse(stral[cnt]); cnt++;
                int spd = int.Parse(stral[cnt]); cnt++;
                float movespd = float.Parse(stral[cnt]); cnt++;
                int cooltime = int.Parse(stral[cnt]); cnt++;
                int costingame = int.Parse(stral[cnt]); cnt++;
                int costupgrade = int.Parse(stral[cnt]); cnt++;
                int costgem = int.Parse(stral[cnt]); cnt++;
                string res = stral[cnt]; cnt++;
                int scale = int.Parse(stral[cnt]); cnt++;
                int passiveid = int.Parse(stral[cnt]); cnt++;

                Mercenary item = new Mercenary(level, hp, atk, def, crt, spd, movespd, res, scale, cooltime, costingame, costupgrade, costgem, passiveid);

                if (MercenaryData.ContainsKey(Enum.AliseType.King) == false)
                {
                    MercenaryData.Add(Enum.AliseType.King, new List<Mercenary>());
                }

                MercenaryData[Enum.AliseType.King].Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  King");
        }

    }

    public void Load_KnightData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Knight", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');
            
            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                int cnt = 0;

                int level = int.Parse(stral[cnt]); cnt++;
                int hp = int.Parse(stral[cnt]); cnt++;
                int atk = int.Parse(stral[cnt]); cnt++;
                int def = int.Parse(stral[cnt]); cnt++;
                int crt = int.Parse(stral[cnt]); cnt++;
                int spd = int.Parse(stral[cnt]); cnt++;
                float movespd = float.Parse(stral[cnt]); cnt++;
                int cooltime = int.Parse(stral[cnt]); cnt++;
                int costingame = int.Parse(stral[cnt]); cnt++;
                int costupgrade = int.Parse(stral[cnt]); cnt++;
                int costgem = int.Parse(stral[cnt]); cnt++;
                string res = stral[cnt]; cnt++;
                int scale = int.Parse(stral[cnt]); cnt++;
                int passiveid = int.Parse(stral[cnt]); cnt++;

                Mercenary item = new Mercenary(level, hp, atk, def, crt, spd, movespd, res, scale, cooltime, costingame, costupgrade, costgem, passiveid);

                if (MercenaryData.ContainsKey(Enum.AliseType.Knight) == false)
                {
                    MercenaryData.Add(Enum.AliseType.Knight, new List<Mercenary>());
                }

                MercenaryData[Enum.AliseType.Knight].Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Knight");
        }

    }

    public void Load_CastleData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Castle", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');
            
            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                int cnt = 0;

                int lv = int.Parse(stral[cnt]); cnt++;
                int hp = int.Parse(stral[cnt]); cnt++;
                int upgrade_cost = int.Parse(stral[cnt]); cnt++;
                int gem_cost = int.Parse(stral[cnt]); cnt++;

                Castle item = new Castle(lv, hp, upgrade_cost, gem_cost);
                
                CastleData.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Castle");
        }
    }

    public void Load_TowerData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Tower", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');
            
            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                int cnt = 0;

                int lv = int.Parse(stral[cnt]); cnt++;
                int atk = int.Parse(stral[cnt]); cnt++;
                int spd = int.Parse(stral[cnt]); cnt++;
                int upgrade_cost = int.Parse(stral[cnt]); cnt++;
                int gem_cost = int.Parse(stral[cnt]); cnt++;

                Tower item = new Tower(lv, atk, spd, upgrade_cost, gem_cost);

                TowerData.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Tower");
        }
    }

    public void Load_ManaData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Mana", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');
            
            for (int i = 1; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                
                int cnt = 0;

                int lv = int.Parse(stral[cnt]); cnt++;
                int[] maxvalue = { 0, 0, 0, 0, 0 };
                int[] lvup_cost = { 0, 0, 0, 0, 0 };
                maxvalue[0] = int.Parse(stral[cnt]); cnt++;
                maxvalue[1] = int.Parse(stral[cnt]); cnt++;
                maxvalue[2] = int.Parse(stral[cnt]); cnt++;
                maxvalue[3] = int.Parse(stral[cnt]); cnt++;
                maxvalue[4] = int.Parse(stral[cnt]); cnt++;
                lvup_cost[0] = int.Parse(stral[cnt]); cnt++;
                lvup_cost[1] = int.Parse(stral[cnt]); cnt++;
                lvup_cost[2] = int.Parse(stral[cnt]); cnt++;
                lvup_cost[3] = int.Parse(stral[cnt]); cnt++;
                int charge_sce = int.Parse(stral[cnt]); cnt++;
                int upgrade_cost = int.Parse(stral[cnt]); cnt++;

                Mana item = new Mana(lv, maxvalue, lvup_cost, charge_sce, upgrade_cost);

                ManaData.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Mana");
        }
    }

    public void Load_GunFireData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/GunFire", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                int cnt = 0;

                int lv = int.Parse(stral[cnt]); cnt++;
                int atk = int.Parse(stral[cnt]); cnt++;
                int cooltime = int.Parse(stral[cnt]); cnt++;
                int ingame_cost = int.Parse(stral[cnt]); cnt++;
                int upgrade_cost = int.Parse(stral[cnt]); cnt++;

                Skill_GunFire item = new Skill_GunFire(lv, atk, cooltime, ingame_cost, upgrade_cost);

                GunFireData.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  GunFire");
        }
    }

    public void Load_JakoData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/JAKO", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                

                int cnt = 0;

                int level = int.Parse(stral[cnt]); cnt++;
                int hp = int.Parse(stral[cnt]); cnt++;
                int atk = int.Parse(stral[cnt]); cnt++;
                int def = int.Parse(stral[cnt]); cnt++;
                int crt = int.Parse(stral[cnt]); cnt++;
                int spd = int.Parse(stral[cnt]); cnt++;
                float movespd = float.Parse(stral[cnt]); cnt++;
                int reward_gold = int.Parse(stral[cnt]); cnt++;
                string res = stral[cnt]; cnt++;
                int scale = int.Parse(stral[cnt]); cnt++;

                Monster item = new Monster(level, hp, atk, def, crt, spd, movespd, res, scale, reward_gold);

                if (MonsterData.ContainsKey(Enum.MonsterType.Jako) == false)
                {
                    MonsterData.Add(Enum.MonsterType.Jako, new List<Monster>());
                }

                MonsterData[Enum.MonsterType.Jako].Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  JAKO");
        }
    }

    public void Load_GoblinData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Goblin", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');
                
                int cnt = 0;

                int level = int.Parse(stral[cnt]); cnt++;
                int hp = int.Parse(stral[cnt]); cnt++;
                int atk = int.Parse(stral[cnt]); cnt++;
                int def = int.Parse(stral[cnt]); cnt++;
                int crt = int.Parse(stral[cnt]); cnt++;
                int spd = int.Parse(stral[cnt]); cnt++;
                float movespd = float.Parse(stral[cnt]); cnt++;
                int reward_gold = int.Parse(stral[cnt]); cnt++;
                string res = stral[cnt]; cnt++;
                int scale = int.Parse(stral[cnt]); cnt++;

                Monster item = new Monster(level, hp, atk, def, crt, spd, movespd, res, scale, reward_gold);

                if (MonsterData.ContainsKey(Enum.MonsterType.Goblin) == false)
                {
                    MonsterData.Add(Enum.MonsterType.Goblin, new List<Monster>());
                }

                MonsterData[Enum.MonsterType.Goblin].Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Goblin");
        }
    }

    public void Load_OgreData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Ogre", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                int cnt = 0;

                int level = int.Parse(stral[cnt]); cnt++;
                int hp = int.Parse(stral[cnt]); cnt++;
                int atk = int.Parse(stral[cnt]); cnt++;
                int def = int.Parse(stral[cnt]); cnt++;
                int crt = int.Parse(stral[cnt]); cnt++;
                int spd = int.Parse(stral[cnt]); cnt++;
                float movespd = float.Parse(stral[cnt]); cnt++;
                int reward_gold = int.Parse(stral[cnt]); cnt++;
                string res = stral[cnt]; cnt++;
                int scale = int.Parse(stral[cnt]); cnt++;

                Monster item = new Monster(level, hp, atk, def, crt, spd, movespd, res, scale, reward_gold);

                if (MonsterData.ContainsKey(Enum.MonsterType.Ogre) == false)
                {
                    MonsterData.Add(Enum.MonsterType.Ogre, new List<Monster>());
                }

                MonsterData[Enum.MonsterType.Ogre].Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Ogre");
        }
    }

    public void Load_CyclopsData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Cyclops", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                int cnt = 0;

                int level = int.Parse(stral[cnt]); cnt++;
                int hp = int.Parse(stral[cnt]); cnt++;
                int atk = int.Parse(stral[cnt]); cnt++;
                int def = int.Parse(stral[cnt]); cnt++;
                int crt = int.Parse(stral[cnt]); cnt++;
                int spd = int.Parse(stral[cnt]); cnt++;
                float movespd = float.Parse(stral[cnt]); cnt++;
                int reward_gold = int.Parse(stral[cnt]); cnt++;
                string res = stral[cnt]; cnt++;
                int scale = int.Parse(stral[cnt]); cnt++;

                Monster item = new Monster(level, hp, atk, def, crt, spd, movespd, res, scale, reward_gold);

                if (MonsterData.ContainsKey(Enum.MonsterType.Cyclops) == false)
                {
                    MonsterData.Add(Enum.MonsterType.Cyclops, new List<Monster>());
                }

                MonsterData[Enum.MonsterType.Cyclops].Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Cyclops");
        }
    }

    public void Load_GolemData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Golem", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');
                
                int cnt = 0;

                int level = int.Parse(stral[cnt]); cnt++;
                int hp = int.Parse(stral[cnt]); cnt++;
                int atk = int.Parse(stral[cnt]); cnt++;
                int def = int.Parse(stral[cnt]); cnt++;
                int crt = int.Parse(stral[cnt]); cnt++;
                int spd = int.Parse(stral[cnt]); cnt++;
                float movespd = float.Parse(stral[cnt]); cnt++;
                int reward_gold = int.Parse(stral[cnt]); cnt++;
                string res = stral[cnt]; cnt++;
                int scale = int.Parse(stral[cnt]); cnt++;

                Monster item = new Monster(level, hp, atk, def, crt, spd, movespd, res, scale, reward_gold);

                if (MonsterData.ContainsKey(Enum.MonsterType.Golem) == false)
                {
                    MonsterData.Add(Enum.MonsterType.Golem, new List<Monster>());
                }

                MonsterData[Enum.MonsterType.Golem].Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Golem");
        }
    }

    public void Load_SkullData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Skull", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');
                
                int cnt = 0;

                int level = int.Parse(stral[cnt]); cnt++;
                int hp = int.Parse(stral[cnt]); cnt++;
                int atk = int.Parse(stral[cnt]); cnt++;
                int def = int.Parse(stral[cnt]); cnt++;
                int crt = int.Parse(stral[cnt]); cnt++;
                int spd = int.Parse(stral[cnt]); cnt++;
                float movespd = float.Parse(stral[cnt]); cnt++;
                int reward_gold = int.Parse(stral[cnt]); cnt++;
                string res = stral[cnt]; cnt++;
                int scale = int.Parse(stral[cnt]); cnt++;

                Monster item = new Monster(level, hp, atk, def, crt, spd, movespd, res, scale, reward_gold);

                if (MonsterData.ContainsKey(Enum.MonsterType.Skull) == false)
                {
                    MonsterData.Add(Enum.MonsterType.Skull, new List<Monster>());
                }

                MonsterData[Enum.MonsterType.Skull].Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Skull");
        }
    }

    public void Load_VikingData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Viking", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');
                
                int cnt = 0;

                int level = int.Parse(stral[cnt]); cnt++;
                int hp = int.Parse(stral[cnt]); cnt++;
                int atk = int.Parse(stral[cnt]); cnt++;
                int def = int.Parse(stral[cnt]); cnt++;
                int crt = int.Parse(stral[cnt]); cnt++;
                int spd = int.Parse(stral[cnt]); cnt++;
                float movespd = float.Parse(stral[cnt]); cnt++;
                int reward_gold = int.Parse(stral[cnt]); cnt++;
                string res = stral[cnt]; cnt++;
                int scale = int.Parse(stral[cnt]); cnt++;

                Monster item = new Monster(level, hp, atk, def, crt, spd, movespd, res, scale, reward_gold);

                if (MonsterData.ContainsKey(Enum.MonsterType.Viking) == false)
                {
                    MonsterData.Add(Enum.MonsterType.Viking, new List<Monster>());
                }

                MonsterData[Enum.MonsterType.Viking].Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Viking");
        }
    }

    public void Load_PirateData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Pirate", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');
                
                int cnt = 0;

                int level = int.Parse(stral[cnt]); cnt++;
                int hp = int.Parse(stral[cnt]); cnt++;
                int atk = int.Parse(stral[cnt]); cnt++;
                int def = int.Parse(stral[cnt]); cnt++;
                int crt = int.Parse(stral[cnt]); cnt++;
                int spd = int.Parse(stral[cnt]); cnt++;
                float movespd = float.Parse(stral[cnt]); cnt++;
                int reward_gold = int.Parse(stral[cnt]); cnt++;
                string res = stral[cnt]; cnt++;
                int scale = int.Parse(stral[cnt]); cnt++;

                Monster item = new Monster(level, hp, atk, def, crt, spd, movespd, res, scale, reward_gold);

                if (MonsterData.ContainsKey(Enum.MonsterType.Pirate) == false)
                {
                    MonsterData.Add(Enum.MonsterType.Pirate, new List<Monster>());
                }

                MonsterData[Enum.MonsterType.Pirate].Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Pirate");
        }
    }

    public void Load_WaveData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Wave", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                int cnt = 0;

                int wave = int.Parse(stral[cnt]); cnt++;
                

                List<int> type = utlie.StringPareser(stral[cnt]); cnt++;
                List<int> level = utlie.StringPareser(stral[cnt]); cnt++;
                List<int> cool = utlie.StringPareser(stral[cnt]); cnt++;
                int boss = int.Parse(stral[cnt]); cnt++;
                int reward = int.Parse(stral[cnt]); cnt++;
                int gem = int.Parse(stral[cnt]); cnt++;

                List<WaveMonsterInfo> m_infolist = new List<WaveMonsterInfo>();
                for(int z = 0; z < type.Count; ++z)
                {
                    int m_type = type[z];
                    int m_lv = level[z];
                    float m_summondelay = 0f;

                    if (cool.Count > z)
                        m_summondelay = cool[z];

                    WaveMonsterInfo m = new WaveMonsterInfo(m_type, m_lv, m_summondelay);
                    m_infolist.Add(m);
                }
               

                WaveInfo item = new WaveInfo(wave, m_infolist, reward, boss, gem);

                WaveData.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Wave");
        }
    }

    public void Load_ShopData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Shop", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                int cnt = 0;

                int id = int.Parse(stral[cnt]); cnt++;
                int type = int.Parse(stral[cnt]); cnt++;
                Enum.Shop_PayType paytype = (Enum.Shop_PayType)int.Parse(stral[cnt]); cnt++;
                int cost = int.Parse(stral[cnt]); cnt++;
                string name = stral[cnt]; cnt++;
                string text = stral[cnt]; cnt++;
                string res = stral[cnt]; cnt++;
                List<int> reward = utlie.StringPareser(stral[cnt]); cnt++;
                string store_id = stral[cnt]; cnt++;

                ShopData item = new ShopData(id, type, paytype, cost, name, text, res, reward, store_id);

                ShopItemData.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Shop");
        }
    }

    public void Load_ShopRewardData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/Reward", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                int cnt = 0;

                int id = int.Parse(stral[cnt]); cnt++;
                Enum.RewardDataType type = (Enum.RewardDataType)int.Parse(stral[cnt]); cnt++;
                string name = stral[cnt]; cnt++;
                int value = int.Parse(stral[cnt]); cnt++;

                ShopReward item = new ShopReward(id, type, name, value);

                ShopRewardData.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Wave");
        }
    }

    public void Load_PassiveSkillData()
    {
        int ErrorIndex = 0;
        try
        {
            TextAsset txObj = Resources.Load("Table/PassiveSkill", typeof(TextAsset)) as TextAsset;
            string[] strArray = txObj.text.Split('\n');

            for (int i = 0; i < strArray.Length; i++)
            {
                ErrorIndex = i;
                if (strArray[i] == "") break;
                string[] stral = strArray[i].Split('|');

                int cnt = 0;

                int id = int.Parse(stral[cnt]); cnt++;
                string name = stral[cnt]; cnt++;
                Enum.PassiveTarget target = (Enum.PassiveTarget)int.Parse(stral[cnt]); cnt++;
                Enum.PassiveType type = (Enum.PassiveType)int.Parse(stral[cnt]); cnt++;
                int stat = int.Parse(stral[cnt]); cnt++;
                int time = int.Parse(stral[cnt]); cnt++;
                int per = int.Parse(stral[cnt]); cnt++;
                string text = stral[cnt]; cnt++;

                PassiveSkill item = new PassiveSkill(id, name, target, type, stat, time, per, text);

                PassiveSkillData.Add(item);
            }
        }
        catch
        {
            Debug.LogError("Error :" + ErrorIndex + "  Wave");
        }
    }

    public Mercenary GetMercenaryData(Enum.AliseType type_, int lv)
    {
        List<Mercenary> mer = MercenaryData[type_];

        for(int i = 0; i < mer.Count; i++)
        {
            if(lv == mer[i].level)
            {
                return mer[i];
            }
        }

        return null;
    }

    public Monster GetMonsterData(Enum.MonsterType type_, int lv)
    {
        if (lv == 0)
        {
            return null;
        }

        List<Monster> mon = MonsterData[type_];
        
        for (int i = 0; i < mon.Count; i++)
        {
            
            if (lv == mon[i].level)
            {
                return mon[i];
            }
        }

        return null;
    }

    public Castle GetCastleData(int lv_)
    {
        for(int i = 0; i < CastleData.Count; ++i)
        {
            if(lv_ == CastleData[i].lv)
            {
                return CastleData[i];
            }
        }

        return null;
    }

    public WaveInfo GetWaveData(int wave_)
    {
        for(int i = 0; i < WaveData.Count; ++i)
        {
            if(WaveData[i].wave == wave_)
            {
                return WaveData[i];
            }
        }

        Debug.Log(wave_ + "데이터를 찾을 수 없습니다");
        return null;
    }
    
    public int GetWaveDataCnt()
    {
        return WaveData.Count;
    }

    public int GetLastWave()
    {
        int wave = 0;
        
        for (int i = 0; i < WaveData.Count; ++i)
        {
            if (WaveData[i].wave > wave)
            {
                wave = WaveData[i].wave;
            }
        }

        return wave;
    }

    public Mana GetManaData(int lv_)
    {
        for(int i = 0; i < ManaData.Count; ++i)
        {
            if(lv_ == ManaData[i].lv)
            {
                return ManaData[i];
            }
        }

        return null;
    }

    public Skill_GunFire GetGunFire(int lv_)
    {
        for(int i = 0; i < GunFireData.Count; ++i)
        {
            if(lv_ == GunFireData[i].lv)
            {
                return GunFireData[i];
            }
        }

        return null;
    }

    public ShopData GetShopData(int id_)
    {
        for(int i = 0; i < ShopItemData.Count; ++i)
        {
            if(id_ == ShopItemData[i].Id)
            {
                return ShopItemData[i];
            }
        }

        return null;
    }

    public List<int> GetShopDataIDList(int type_)
    {
        List<int> R_Data = new List<int>();

        for(int i = 0; i < ShopItemData.Count; ++i)
        {
            if (type_ == ShopItemData[i].Type)
            {
                if (ShopItemData[i].PURCHASE_ID == "start_package")
                {
                    if (Main.Get.GetUserDataManager().BuyStartPackage == 0)
                        R_Data.Add(ShopItemData[i].Id);
                }
                else
                {
                    R_Data.Add(ShopItemData[i].Id);
                }
            }
        }

        return R_Data;
    }

    public List<int> GetShopRewardList(string storeid_)
    {
        List<int> data = new List<int>();

        for(int i = 0; i < ShopItemData.Count; ++i)
        {
            if(ShopItemData[i].PURCHASE_ID == storeid_)
            {
                data = ShopItemData[i].RewardList;
            }
        }

        return data;
    }

    public ShopReward GetShopReward(int id_)
    {
        for(int i = 0; i < ShopRewardData.Count; ++i)
        {
            if(id_ == ShopRewardData[i].Id)
            {
                return ShopRewardData[i];
            }
        }

        return null;
    }

    public float GetMoveSpd(int move)
    {
        switch(move)
        {
            case 0:
                return 0;
            case 1:
                return 1f;
            case 2:
                return 1.5f;
            case 3:
                return 2f;
            default:
                return 1f;
        }
    }

    public PassiveSkill GetPassiveSkill(int id_)
    {
        for(int i = 0; i < PassiveSkillData.Count; ++i)
        {
            if(id_ == PassiveSkillData[i].Id)
            {
                return PassiveSkillData[i];
            }
        }

        Debug.LogError(id_ + "passive데이터 없음");
        return null;
    }
}
