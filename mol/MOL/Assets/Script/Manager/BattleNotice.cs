﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleNotice : MonoBehaviour
{
    UILabel Notice_text;
    // Use this for initialization

    private void Awake()
    {
        Notice_text = GetComponent<UILabel>();
    }

    void Start ()
    {
		
	}
	
    public void SetText(string s_)
    {
        Notice_text.text = s_;
    }

	// Update is called once per frame
	void Update () {
		
	}

    public void OnFinshTween()
    {
        Destroy(this.gameObject);
    }
}
