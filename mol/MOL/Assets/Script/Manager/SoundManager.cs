﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;


public class SoundManager : Singleton<SoundManager>
{
    public bool Sound_On = true;

    List<AudioClip> SoundList_Bgm = new List<AudioClip>();
    List<AudioSource> SoundList_Sfx = new List<AudioSource>();
    public AudioSource Bgm_Obj;
    public Transform Sfx_Obj;

	// Use this for initialization
	void Start ()
    {
        LoadBgm();
        SfxSetting();
    }

    void LoadBgm()
    {
        for(int i = 0; i < (int)Bgm.Max; ++i)
        {
            SoundList_Bgm.Add(Resources.Load("Sound/" + (Bgm)i) as AudioClip);
        }
    }

    public void SfxSetting()
    {
        for(int i = 0; i < (int)Sfx.Max; ++i)
        {
            GameObject o = GameObject.Instantiate(Resources.Load("Prefab/Sfx_Sound") as GameObject);
            o.transform.parent = Sfx_Obj;
            o.transform.localPosition = Vector3.zero;
            o.GetComponent<AudioSource>().clip = Resources.Load("Sound/" + (Sfx)i) as AudioClip;
            SoundList_Sfx.Add(o.GetComponent<AudioSource>());
        }
    }

    public void PlayBgm(Bgm bgm_)
    {
        if (!Sound_On) return;

        Bgm_Obj.clip = SoundList_Bgm[(int)bgm_];
        Bgm_Obj.loop = true;
        Bgm_Obj.Play();
    }

    public void StopBgm()
    {
        Bgm_Obj.Stop();
    }
    
    public void PlaySfx(Sfx sfx_)
    {
        if (!Sound_On) return;

        if(!SoundList_Sfx[(int)sfx_].isPlaying)
        {
            SoundList_Sfx[(int)sfx_].PlayOneShot(SoundList_Sfx[(int)sfx_].clip, 1f);
        }
    }

    public void SoundOff()
    {
        Sound_On = false;
        Bgm_Obj.Stop();
        for(int i = 0; i < (int)Sfx.Max; ++i)
        {
            SoundList_Sfx[i].Stop();
        }
    }

    public void SoundOn()
    {
        Sound_On = true;
        if(Main.Get.GetNowScene() == SceneType.Lobby)
        {
            PlayBgm(Bgm.BLUESKYBLUE_lobby);
        }
        else if(Main.Get.GetNowScene() == SceneType.Ingame)
        {
            PlayBgm(Bgm.Ignition_battle);
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
