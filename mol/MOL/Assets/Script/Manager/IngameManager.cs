﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class IngameManager : Singleton<IngameManager>
{
    public Transform AliesPoint;
    public Transform EnemyPoint;
    public Transform AliesStopPoint;
    public Transform BgRoot;
    public Transform Castle;
    public Transform GoldDestPoint;

    public Battle_UI BattleUI;
    public GunFire GunFireObj;
  
    int c_depth = 6;
    int last_id = 1;

    List<CharacterObj> CharacterList = new List<CharacterObj>();
    

    WaveInfo WaveData = null;
    Mana ManaData = null;

    //인겜 사용 변수들
    int Ingame_Mana = 0;
    int Ingame_ManaLv = 0;
    int castle_hp = 0;
    int castle_hpmax = 0;
    int monster_index = 0; //wave 몬스터 소환 인덱스
    int skillusecost = 0;
    int Skill_Atk = 0;
    int Ingame_GetGold = 0;

    int Enemy_Cnt = 0; //몹카운트
    bool FinshSummonMonster = false; //해당 웨이브에서 몹 소환 끝났는가

    //실패시 팁
    static string[] Tip = { "용병들을 업그레이드 해서 다시 싸워보세요!", "젬을 구매하시면 게임이 더 쉬워집니다.", "젬은 광고 시청으로도 얻을 수 있어요!" };

    // Use this for initialization
    void Start()
    {
        Castle get_caslte = DataManager.Get.GetCastleData(Main.Get.GetUserDataManager().GetCastleLv());
        //Debug.Log(Main.Get.GetUserDataManager().PlayWave);
        WaveInfo wave = DataManager.Get.GetWaveData(Main.Get.GetUserDataManager().PlayWave);
        ManaData = DataManager.Get.GetManaData(Main.Get.GetUserDataManager().GetManaLv());
        Skill_GunFire gun = DataManager.Get.GetGunFire(Main.Get.GetUserDataManager().GetGunFireLv());

        castle_hp = get_caslte.hp;
        castle_hpmax = get_caslte.hp;
        WaveData = wave;

        skillusecost = gun.ingame_cost;

        Skill_Atk = gun.atk;
        GunFireObj = GetComponent<GunFire>();
        GunFireObj.SetAtk(Skill_Atk);

        //ingame ui setting
        BattleUI.RefreshManaText(Ingame_Mana, ManaData.maxvalue[Ingame_ManaLv]);
        BattleUI.ManaUpBtnSetting(ManaData, Ingame_ManaLv);
        BattleUI.SkillBtnSetting(skillusecost, gun.cooltime);
        BattleUI.RefreshGoldText(Ingame_GetGold);


        Invoke("MonsterWaveStep", 1.0f);
        InvokeRepeating("ChargeMana", 0.1f, 0.1f);

        if (wave.boss == 0)
            SoundManager.Get.PlayBgm(Bgm.Ignition_battle);
        else
            SoundManager.Get.PlayBgm(Bgm.battle_boss);
    }

    public void ChargeMana()
    {
        if(Ingame_Mana < ManaData.maxvalue[Ingame_ManaLv])
            Ingame_Mana += (int)((float)ManaData.charge_sce * 0.1f);

        if (Ingame_Mana >= ManaData.maxvalue[Ingame_ManaLv])
            Ingame_Mana = ManaData.maxvalue[Ingame_ManaLv];

        BattleUI.RefreshManaText(Ingame_Mana, ManaData.maxvalue[Ingame_ManaLv]);
    }

    public void ManaUpGrade()
    {
        if (Ingame_ManaLv < ManaData.lvup_cost.Length - 1 && Ingame_Mana >= ManaData.lvup_cost[Ingame_ManaLv])
        {
            Ingame_Mana -= ManaData.lvup_cost[Ingame_ManaLv];
            Ingame_ManaLv++;
        }
        else if (Ingame_ManaLv >= ManaData.lvup_cost.Length - 1)
        {
            CreateNotice("[FF0000]더이상 올릴수 없습니다");
        }
        else
        {
            CreateNotice("[FF0000]자원이 부족합니다");
        }
        
        BattleUI.ManaUpBtnSetting(ManaData, Ingame_ManaLv);
        BattleUI.RefreshManaText(Ingame_Mana, ManaData.maxvalue[Ingame_ManaLv]);
    }
    
    public bool SummonAlise(AliseType type)
    {
        GameObject c = null;
        Mercenary mer = null;

        mer = DataManager.Get.GetMercenaryData(type, Main.Get.GetUserDataManager().GetAliseLv(type));

        if (Ingame_Mana >= mer.costingame)
        {
            //스텟
            int hp = mer.hp;
            int atk = mer.atk;
            int def = mer.def;
            //버프 사용시 스텟 적용시켜준다
            if (Main.Get.GetUserDataManager().UseBoost[(int)BoostType.Atk]) atk += (int)((float)atk * 0.1f);
            if (Main.Get.GetUserDataManager().UseBoost[(int)BoostType.Hp]) hp += (int)((float)hp * 0.1f);
            if (Main.Get.GetUserDataManager().UseBoost[(int)BoostType.Def]) def += (int)((float)def * 0.1f);

            c = GameObject.Instantiate(Resources.Load("Allies/" + mer.res + "/Prefab/" + mer.res)) as GameObject;
            
            c.transform.parent = BgRoot;
            c.transform.localPosition = new Vector3(AliesPoint.localPosition.x, AliesPoint.localPosition.y + Random.Range(-10, 31), 0f);
            c.transform.localScale = new Vector3(mer.scale, mer.scale, mer.scale);
            c.GetComponent<CharacterObj>().SetOrderLayer(c_depth);
            c.GetComponent<CharacterObj>().SetParameter(hp, atk, def, mer.movespd, last_id, 0, mer.passiveid); //스텟 및 스킬 세팅
            c_depth++; //캐릭터 뎁스 올려준다
            last_id++; //오브젝트 아이디도 올려준다
            CharacterList.Add(c.GetComponent<CharacterObj>()); //전체 캐릭터 리스트에 추가

            Ingame_Mana -= mer.costingame; //마나 소모적용
            BattleUI.RefreshManaText(Ingame_Mana, ManaData.maxvalue[Ingame_ManaLv]); //ui 갱신
            return true;
        }
        else
        {
            CreateNotice("[FF0000]자원이 부족합니다");
            //자원부족
            return false;
        }
    }

    public void SummonEnemy(MonsterType type, int lv)
    {
        GameObject m = null;
        Monster mon = null;
       
        mon = DataManager.Get.GetMonsterData(type, lv);
        
        m = GameObject.Instantiate(Resources.Load("Enemy/" + mon.res + "/Prefab/" + mon.res)) as GameObject;

        m.transform.parent = BgRoot;
        m.transform.localPosition = new Vector3(EnemyPoint.localPosition.x, EnemyPoint.localPosition.y + Random.Range(-10, 31), 0f);

        m.transform.localScale = new Vector3(-mon.scale, mon.scale, mon.scale);

        m.GetComponent<CharacterObj>().SetOrderLayer(c_depth);
        m.GetComponent<CharacterObj>().SetParameter(mon.hp, mon.atk, mon.def, mon.movespd, last_id, mon.reward_gold); //몬스터는 버프가 없어서 그냥 넣어줌
        c_depth++;
        last_id++;
        CharacterList.Add(m.GetComponent<CharacterObj>());

        Enemy_Cnt++;
    }

    public CharacterObj FindCharacter(int id_)
    {
        for(int i = 0; i < CharacterList.Count; ++i)
        {
            if(CharacterList[i].GetObjID() == id_)
            {
                return CharacterList[i];
            }
        }

        return null;
    }

    public void AttackCastle(int atk)
    {
        castle_hp -= atk;
        BattleUI.ReFreshCatleHpBar((float)castle_hp / (float)castle_hpmax);
        //Debug.Log("성이 " + castle_hp);
        if(castle_hp <= 0)
        {
            //Debug.Log("게임 끝!!!!!!!!");
            //CreateNotice("[FF0000]짐");
            Lose_Game();
        }
    }

    void MonsterWaveStep()
    {
        CancelInvoke("MonsterWaveStep");

        if (WaveData.monsterinfo.Count > monster_index)
        {
            WaveMonsterInfo monsterinfo = WaveData.monsterinfo[monster_index];

            SummonEnemy((MonsterType)monsterinfo.type, monsterinfo.lv);
            //Debug.Log("monster delay = " + monsterinfo.summondelay);
            Invoke("MonsterWaveStep", monsterinfo.summondelay);

            monster_index++;
        }
        else
        {
            Debug.Log("몬스터 소환 끝");
            FinshSummonMonster = true;
        }
    }

    public void RemoveCharList(int id_)
    {
        int index = -1;
        for (int i = 0; i < CharacterList.Count; ++i)
        {
            if (CharacterList[i].GetObjID() == id_)
            {
                index = i;
                break;
            }
        }

        if (-1 != index)
        {
            if (CharacterList[index].tag == "Enemy_Hit")
                Enemy_Cnt--;

            CharacterList.RemoveAt(index);
        }

        if(FinshSummonMonster && Enemy_Cnt == 0)
        {
            Win_Game();
        }
    }

    public void AllEnemyAttack(int atk_)
    {
        for(int i = 0; i < CharacterList.Count; ++i)
        {
            if(CharacterList[i].tag == "Enemy_Hit")
            {
                CharacterList[i].Damage(atk_);
            }
        }

        SoundManager.Get.PlaySfx(Sfx.Hit);
    }

    public void PassiveAllAille(PassiveSkill skill_)
    {
        for (int i = 0; i < CharacterList.Count; ++i)
        {
            if (CharacterList[i].tag == "Aille_Hit")
            {
                CharacterList[i].ApplyPassive(skill_);
            }
        }
    }

    public void Win_Game()
    {
        //승리 애니 플래이
        for (int i = 0; i < CharacterList.Count; ++i)
        {
            if (CharacterList[i].tag == "Aille_Hit")
            {
                CharacterList[i].PlayTaunt();
            }
        }

        //부스터 사용 플래그 초기화
        Main.Get.GetUserDataManager().ReSetUseBoostFlag();

        //자기가 진행한 웨이브가 이전에 클리어한게 아니면 웨이브 진행
        if (Main.Get.GetUserDataManager().GetWave() == WaveData.wave) Main.Get.GetUserDataManager().WaveStepUp();

      

        //보상 적용
        Main.Get.GetUserDataManager().AddGold(WaveData.reward);

        if (WaveData.reward_gem != 0 && Main.Get.GetUserDataManager().GetWaveClearCnt(WaveData.wave) == 0)
        {
            Main.Get.GetUserDataManager().AddGem(WaveData.reward_gem);
        }

        //구글 업적처리
        switch (Main.Get.GetUserDataManager().GetWave())
        {
            case 1:
                GPGS_Manager.Get.UnlockAchievement(GPGSIds.achievement);
                break;
            case 50:
                GPGS_Manager.Get.UnlockAchievement(GPGSIds.achievement_2);
                break;
            case 100:
                GPGS_Manager.Get.UnlockAchievement(GPGSIds.achievement_3);
                break;
        }

        //클리어 팝업
        PopupReward popup = new PopupReward();
        popup.text = "승  리\n보상을 획득하였습니다.";
        popup.btn_text = "다음 웨이브";
        popup.CancelText = "로비이동";
        popup.value = WaveData.reward;
        popup.value2 = WaveData.reward_gem;

        if (WaveData.reward_gem <= 0)
            popup.type = 0;
        else
        {
            if (Main.Get.GetUserDataManager().GetWaveClearCnt(WaveData.wave) == 0)
                popup.type = 6;
            else
                popup.type = 0;
        }
        popup.Ok_Delegate = new EventDelegate(NextWave);
        popup.Cancel_Delegate = new EventDelegate(MoveLobby);

        PopupManager.Get.OpenPopup("Popup_Reward", popup);

        //클리어 횟수 저장
        Main.Get.GetUserDataManager().PlusWaveClearCnt(WaveData.wave);

        //사운드
        SoundManager.Get.StopBgm();
        SoundManager.Get.PlaySfx(Sfx.win);
    }
    
    public void Lose_Game()
    {
        for (int i = 0; i < CharacterList.Count; ++i)
        {
            if (CharacterList[i].tag == "Enemy_Hit")
            {
                CharacterList[i].PlayTaunt();
            }
        }

        Main.Get.GetUserDataManager().ReSetUseBoostFlag();

        string tip = Tip[Random.Range(0, Tip.Length)];
       
        PopupMessage popup = new PopupMessage();
        popup.Type = MessagePopupType.Ok_Cancel;
        popup.Text = "실패 하였습니다." + "\n" + tip;
        popup.Ok_Text = "다시시작";
        popup.Cancel_Text = "로비이동";
        popup.Ok_Delegate = new EventDelegate(ReStart);
        popup.Cancel_Delegate = new EventDelegate(MoveLobby);
        Time.timeScale = 0f;
        PopupManager.Get.OpenPopup("Popup_Message", popup);

        SoundManager.Get.StopBgm();
        SoundManager.Get.PlaySfx(Sfx.fail);

        GPGS_Manager.Get.IncrementAchieve(GPGSIds.achievement_5, 1);
    }

    void MoveLobby()
    {
        Main.Get.ChangeScene(SceneType.Lobby);
        Main.Get.GetUserDataManager().AddGold(Ingame_GetGold);
    }

    void ReStart()
    {
        Main.Get.ChangeScene(SceneType.Ingame);
        Main.Get.GetUserDataManager().AddGold(Ingame_GetGold);
    }

    void NextWave()
    {
        if(DataManager.Get.GetWaveData(WaveData.wave + 1) != null)
            Main.Get.GetUserDataManager().PlayWave++;

        Main.Get.ChangeScene(SceneType.Ingame);
        Main.Get.GetUserDataManager().AddGold(Ingame_GetGold);
    }

    public bool UseGunFire()
    {
        if(Ingame_Mana >= skillusecost)
        {
            GunFireObj.Fire();
            Ingame_Mana -= skillusecost;
            return true;
        }
        else
        {
            CreateNotice("[FF0000]자원이 부족합니다");
            return false;
        }
    }

    public void AddGetGold(int gold_)
    {
        Ingame_GetGold += gold_;
        BattleUI.RefreshGoldText(Ingame_GetGold);
    }

    public void CreateNotice(string s_)
    {
        BattleNotice notice = GameObject.Instantiate(Resources.Load("Prefab/BattleNotice") as GameObject).GetComponent<BattleNotice>();
        notice.transform.parent = BattleUI.transform;
        notice.transform.localScale = Vector3.one;
        notice.transform.localPosition = new Vector3(0f, 150f, 0f);
        notice.SetText(s_);
    }

    public void RewardDouble()
    {
        Main.Get.GetUserDataManager().AddGold(WaveData.reward);

        PopupMessage popup = new PopupMessage();
        popup.Type = MessagePopupType.Ok;
        popup.Text = "보상을 획득하였습니다";
        popup.Ok_Text = "확인";

        PopupManager.Get.OpenPopup("Popup_Message", popup);
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }
}
