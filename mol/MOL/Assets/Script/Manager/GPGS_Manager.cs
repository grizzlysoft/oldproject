﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//GPGS를 사용 하기 위한 using 
using System;
using GooglePlayGames;
using GooglePlayGames.BasicApi.SavedGame;
using GooglePlayGames.BasicApi;


//xml 관련
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using UnityEngine.SocialPlatforms;

public class GPGS_Manager : Singleton<GPGS_Manager>
{
    /// <summary> 현재 로그인 중인지 체크 </summary>
    public bool bLogin
    {
        get;
        set;
    }

    /// <summary> GPGS를 초기화 합니다. </summary>
    public void InitializeGPGS()
    {
        bLogin = false;
        
        // 구글플레이를 초기화하고 로그인한다
        PlayGamesClientConfiguration _config = new PlayGamesClientConfiguration.Builder()
            .EnableSavedGames()
            .Build();

        PlayGamesPlatform.InitializeInstance(_config);
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();

        //암시적으로 로그인을 해줌.
        //PlayGamesPlatform.Instance.Authenticate((bool _success) =>
        //{
        //    bLogin = _success;

        //    if (_success)
        //    {
        //        Debug.LogWarning("Authentication true!");
        //    }
        //    else
        //    {
        //        Debug.LogWarning("Authentication failed!");
        //    }
        //});

        //Debug.Log("구글 플레이 게임 서비스 초기화!!");
        LoginGPGS();
    }

    /// <summary> GPGS를 로그인 합니다. </summary>
    public void LoginGPGS()
    {
        //Debug.Log("직접 구글 로그인");
        // 로그인이 안되어 있으면
        if (!Social.localUser.authenticated)
        {
            Social.localUser.Authenticate(LoginCallBackGPGS);
        }
    }

    /// <summary> GPGS Login Callback </summary>
    /// <param name="result"> 결과 </param>
    public void LoginCallBackGPGS(bool result)
    {
        bLogin = result;

        if (result == true)
        {
            //Debug.Log("로그인!");
        }
        else
        {
            //Debug.Log("로그인 실패!!!!");
            //재 로그인창 띄우기?
        }
    }

    /// <summary> GPGS를 로그아웃 합니다. </summary>
    public void LogoutGPGS()
    {
        // 로그인이 되어 있으면
        if (Social.localUser.authenticated)
        {
            ((GooglePlayGames.PlayGamesPlatform)Social.Active).SignOut();
            //Debug.Log("로그 아웃!!!");
            bLogin = false;
        }
    }

    /// <summary> GPGS에서 자신의 프로필 이미지를 가져옵니다.</summary>
    /// <returns> Texture 2D 이미지 </returns>
    public Texture2D GetImageGPGS()
    {
        if (Social.localUser.authenticated)
            return Social.localUser.image;
        else
            return null;
    }

    /// <summary> GPGS 에서 사용자 이름을 가져옵니다. </summary>
    /// <returns> 이름 </returns>
    public string GetNameGPGS()
    {
        if (Social.localUser.authenticated)
            return Social.localUser.userName;
        else
            return null;
    }

    /// <summary> 리더보드 UI show </summary>
    public void ShowLeaderBoard()
    {
        if (!CheckLogin()) //로그인되지 않았으면
        {
            //로그인루틴을 진행하던지 합니다.
            
            return;
        }
        Social.Active.ShowLeaderboardUI();
    }

    /// <summary> 리더보드 UI show - 특정 리더보드 만 보여줌 </summary>
    public void ShowLeaderBoard(string _id)
    {
        PlayGamesPlatform.Instance.ShowLeaderboardUI(_id);
    }

    /// <summary> 업적 UI show </summary>
    public void ShowAchievement()
    {
        if (!CheckLogin()) //로그인되지 않았으면
        {
            //로그인루틴을 진행하던지 합니다.
            
            return;
        }
        Social.ShowAchievementsUI();
    }

    /// <summary> 리더보드 점수 업로드 </summary>
    public void ReportScore(string _id, int _value)
    {
        Social.ReportScore(_value, _id, (bool success) =>
        {
            if (success) Debug.Log("ReportScore Success");

            else Debug.Log("ReportScore Fail");
        });
    }

    public void UnlockAchievement(string _id)
    {
        PlayGamesPlatform.Instance.UnlockAchievement(_id, (bool success) =>
        {
            if (success) Debug.Log("UnlockAchievement Success");

            else Debug.Log("UnlockAchievement Fail");
        });

    }

    /// <summary> 업적 점수 업로드  안씀</summary>
    //public void ReportProgress(string _id, float _value)
    //{
    //    Social.ReportProgress(_id, _value, (bool success) =>
    //    {
    //        if (success) Debug.Log("AchieveProgress Success");

    //        else Debug.Log("AchieveProgress Fail");
    //    });
    //}

    /// <summary> 업적 스텝을 증가 시킨다 - += 같은거 해당 업적을 _step 만큼 증가 (3이면 업적 수치 3증가)</summary>
    public void IncrementAchieve(string _id, int _step)
    {
        PlayGamesPlatform.Instance.IncrementAchievement(_id, _step, (bool success) =>
        {
            if (success) Debug.Log("IncrementAchieve Success");

            else Debug.Log("IncrementAchieve Fail");
        });
    }


    #region Save & Load
    /*
    //저장할 캐릭터 데이터 타입 - 기본자료형만 가능합니다 - string 은 안됨!
    [XmlInclude(typeof(CharacterData))]
    public class CharacterData
    {
        public int lv;
        public int exp;
        public int money;
    }


    //저장할 게임 캐릭터 정보
    public CharacterData SaveData = new CharacterData();

    //저장 인지 로드인지 여부
    bool isSaving;

    //게임 데이터 - 저장된 슬롯 데이터
    SaveSlot slot0;
    //게임 데이터 접근용 인터페이스
    public SaveSlot GetSlotData()
    {
        if (bLogin == false) return null;

        return slot0;
    }

    ////////////////////////////////////////////////////////////////////////
    /// 세이브 로드 부분
    ///////////////////////////////////////////////////
    /// 캐릭터 데이터와 게임 데이터 동기화 부분

    /// <summary> 오브젝트를 Xml 스트링으로 변환 </summary>
    public string objectToString(object _class)
    {
        XmlSerializer _xs = new XmlSerializer(typeof(CharacterData));
        using (StringWriter _textWriter = new StringWriter())
        {
            _xs.Serialize(_textWriter, _class);
            return _textWriter.ToString();
        }
    }
    /// <summary> Xml 스트링을  오브젝트로 변환 </summary>
    public CharacterData stringToObject(string _xmlString)
    {
        XmlSerializer _xs = new XmlSerializer(typeof(CharacterData));
        object _object = _xs.Deserialize(new StringReader(_xmlString));
        return (CharacterData)_object;
    }

    /// <summary> 캐릭터의 데이터를 구글 슬롯데이터에 저장 </summary>
    public void SaveGameDataFromCharacterData()
    {
        Debug.Log("캐릭터 데이터 => 구글 데이터 세이브");
        Debug.Log("lv: " + SaveData.lv + "    Exp: " + SaveData.exp + "   Money: " + SaveData.money);
        slot0.Data = objectToString(SaveData);
    }

    /// <summary> GPGS 매니저에 저장된 슬롯의 정보를 CharacterData 에 저장 </summary>
    public void LoadGameDataFromGPGS(string _xmlData)
    {
        Debug.Log("구글 데이터 => 캐릭터 데이터 로드");
        Debug.Log(_xmlData);
        SaveData = stringToObject(_xmlData);
    }

    /// <summary> 저장 UI Show </summary>
    public void ShowSaveUI()
    {
        isSaving = true;
        ((PlayGamesPlatform)Social.Active).SavedGame.ShowSelectSavedGameUI(
            "Select Slot to Save", 1, true, true, SavedGameSelected);
    }

    /// <summary> 로드 UI Show </summary>
    public void ShowLoadUI()
    {
        isSaving = false;
        ((PlayGamesPlatform)Social.Active).SavedGame.ShowSelectSavedGameUI(
            "Select Slot to Load", 1, false, false, SavedGameSelected);
    }

    /// <summary> 저장 , 혹은 로드 UI를 선택 했을시 호출될 함수 </summary>
    public void SavedGameSelected(SelectUIStatus _status, ISavedGameMetadata _game)
    {
        if (_status == SelectUIStatus.SavedGameSelected)
        {
            string _filename = _game.Filename;
            if (isSaving && (_filename == null || _filename.Length == 0))
            {
                // 새로 저장하기
                _filename = "save" + DateTime.Now.ToBinary();
            }
            if (isSaving)
            {
                // 저장하기
                slot0.State = "Saving to " + _filename;
            }
            else
            {
                // 불러오기
                slot0.State = "Loading from " + _filename;
            }
            // 파일을 읽고 쓰기 전에 열어야만 한다
            ((PlayGamesPlatform)Social.Active).SavedGame
                .OpenWithAutomaticConflictResolution(_filename,
                                                     DataSource.ReadCacheOrNetwork,
                                                     ConflictResolutionStrategy.UseLongestPlaytime,
                                                     SavedGameOpened);
        }
        else
        {
            Debug.LogWarning("Error selecting save game: " + _status);
        }

    }

    /// <summary> 저장된 슬롯을 열었을 경우 호출 될 함수 - 로드 , 세이브 둘다 일단 세이브 슬롯을 오픈 해야한다 </summary>
    public void SavedGameOpened(SavedGameRequestStatus _status, ISavedGameMetadata _game)
    {
        if (_status == SavedGameRequestStatus.Success)
        {
            //세이브 일때
            if (isSaving)
            {
                //캐릭터 정보를 GameData에 저장한다  -  여기서 하는게 좋을려나..?
                SaveGameDataFromCharacterData();
                Debug.Log("저장된 정보" + slot0.Data);
                // 스트링 데이터를 바이트로 바꿔서 메타 정보와 함꼐 저장한다
                slot0.State = "Opened, now writing";
                byte[] data = slot0.ToBytes();
                //TimeSpan playedTime = slot0.TotalPlayingTime;
                SavedGameMetadataUpdate.Builder builder =
                    new SavedGameMetadataUpdate.Builder()
                        //.WithUpdatedPlayedTime(playedTime)
                        .WithUpdatedDescription("Saved Game at " + DateTime.Now);
                SavedGameMetadataUpdate updatedMetadata = builder.Build();
                ((PlayGamesPlatform)Social.Active).SavedGame.CommitUpdate(
                    _game, updatedMetadata, data, SavedGameWritten);
            }
            //로드일때
            else
            {
                // 우선 파일을 읽어온다
                slot0.State = "Opened, reading...";
                ((PlayGamesPlatform)Social.Active).SavedGame
                    .ReadBinaryData(_game, SavedGameLoaded);
            }
        }
        else
        {
            Debug.LogWarning("Error opening game: " + _status);
        }
    }

    /// <summary> 로드 한 이후 호출 될 함수 </summary>
    public void SavedGameLoaded(SavedGameRequestStatus _status, byte[] _data)
    {
        if (_status == SavedGameRequestStatus.Success)
        {
            // 불러온 바이트 데이터를 게임데이터로 바꾼다
            slot0 = SaveSlot.FromBytes(_data);
            LoadGameDataFromGPGS(slot0.Data);
        }
        else
        {
            Debug.LogWarning("Error reading game: " + _status);
        }
    }

    /// <summary> 저장 한 이후 호출 될 함수 </summary>
    public void SavedGameWritten(SavedGameRequestStatus _status, ISavedGameMetadata _game)
    {
        if (_status == SavedGameRequestStatus.Success)
        {
            // 성공적으로 저장되었다
            slot0.State = "Saved!";
        }
        else
        {
            Debug.LogWarning("Error saving game: " + _status);
        }
    }

    // 게임 슬롯 클래스 - 스트링 데이터를 가지며 바이트로 변환 가능하다
    public class SaveSlot
    {
        string data;
        string state;

        public SaveSlot(string _initData)
        {
            data = _initData;
            state = "Initialized, modified";
        }

        public byte[] ToBytes()
        {
            return System.Text.ASCIIEncoding.Default.GetBytes(data);
        }

        public static SaveSlot FromBytes(byte[] bytes)
        {
            return FromString(System.Text.ASCIIEncoding.Default.GetString(bytes));
        }

        public static SaveSlot FromString(string _s)
        {
            SaveSlot _gd = new SaveSlot("TempData");
            _gd.data = _s;
            _gd.state = "Loaded successfully";
            return _gd;
        }

        public string Data
        {
            get { return data; }
            set { data = value; }
        }

        public string State
        {
            get { return state; }
            set { state = value; }
        }
    }//GameData 클래스

            private byte[] CombineByteArray(byte[] a, byte[] b)
    {
        byte[] c = new byte[a.Length + b.Length];
        System.Buffer.BlockCopy(a, 0, c, 0, a.Length);
        System.Buffer.BlockCopy(b, 0, c, a.Length, b.Length);
        return c;
    }

        */
    #endregion
    
    //저장 데이터 구조체
    [XmlInclude(typeof(CloudSaveData))]
    public class CloudSaveData
    {
        public int Wave;

        public int Archer_lv;  
        public int Warrior_lv;            
        public int Barbarian_lv;

        public int Wizard_lv;
        public int King_lv;
        public int Knight_lv;

        public int Castle_lv;
        public int GunFire_lv;
        public int Tower_lv;
        public int Mana_lv;

        public int Gold;
        public int Gem;

        public int AtkBoost;
        public int HpBoost;
        public int DefBoost;
        public int GoldBoost;

        public int FaceBookBonus;
        public int BuyStartPackage;

        public string ClearCnt;
    }
    //게임데이터를 세이브 데이터에 넣기
    void GameData_to_CloudSaveData()
    {
        SaveData.Archer_lv = Main.Get.GetUserDataManager().GetAliseLv(Enum.AliseType.Archer);
        SaveData.Warrior_lv = Main.Get.GetUserDataManager().GetAliseLv(Enum.AliseType.Warrior);
        SaveData.Barbarian_lv = Main.Get.GetUserDataManager().GetAliseLv(Enum.AliseType.Barbarian);
        SaveData.Wizard_lv = Main.Get.GetUserDataManager().GetAliseLv(Enum.AliseType.Wizard);
        SaveData.Knight_lv = Main.Get.GetUserDataManager().GetAliseLv(Enum.AliseType.Knight);
        SaveData.King_lv = Main.Get.GetUserDataManager().GetAliseLv(Enum.AliseType.King);

        SaveData.Tower_lv = Main.Get.GetUserDataManager().GetTowerLv();
        SaveData.Wave = Main.Get.GetUserDataManager().GetWave();
        SaveData.GunFire_lv = Main.Get.GetUserDataManager().GetGunFireLv();
        SaveData.Castle_lv = Main.Get.GetUserDataManager().GetCastleLv();
        SaveData.Mana_lv = Main.Get.GetUserDataManager().GetManaLv();

        SaveData.AtkBoost = Main.Get.GetUserDataManager().GetBoosterCount(Enum.BoostType.Atk);
        SaveData.HpBoost = Main.Get.GetUserDataManager().GetBoosterCount(Enum.BoostType.Hp);
        SaveData.DefBoost = Main.Get.GetUserDataManager().GetBoosterCount(Enum.BoostType.Def);
        SaveData.GoldBoost = Main.Get.GetUserDataManager().GetBoosterCount(Enum.BoostType.Gold);

        SaveData.Gold = Main.Get.GetUserDataManager().GetGold();
        SaveData.Gem = Main.Get.GetUserDataManager().GetGem();

        SaveData.FaceBookBonus = Main.Get.GetUserDataManager().Facebook_GetBonus;
        SaveData.BuyStartPackage = Main.Get.GetUserDataManager().BuyStartPackage;

        SaveData.ClearCnt = string.Empty;

        for(int i = 0; i < Main.Get.GetUserDataManager().WaveClearCnt.Count; ++i)
        {
            SaveData.ClearCnt += Main.Get.GetUserDataManager().WaveClearCnt[i].ToString();
        }
    }
    void CloudSaveData_to_GameData()
    {
        Main.Get.GetUserDataManager().SetUserDataToCloud(SaveData);
    }

    //오브젝트를 바이트로 던지기?
    public byte[] SaveDataToBytes()
    {
        //게임데이터 -> 세이브 데이터 이동
        GameData_to_CloudSaveData();
        //세이브 데이터 -> string -> byte[]로 변환
        return System.Text.ASCIIEncoding.Default.GetBytes(objectToString(SaveData));
    }
    /// <summary> 오브젝트를 Xml 스트링으로 변환 </summary>
    string objectToString(object _class)
    {
        XmlSerializer _xs = new XmlSerializer(typeof(CloudSaveData));
        using (StringWriter _textWriter = new StringWriter())
        {
            _xs.Serialize(_textWriter, _class);
            return _textWriter.ToString();
        }
    }

    //바이트 배열을 클래스로 넣기
    public void LoadDataFromBytes(byte[] bytes)
    {
        SaveData = stringToObject(System.Text.ASCIIEncoding.Default.GetString(bytes));
        CloudSaveData_to_GameData();
    }
    /// <summary> Xml 스트링을  오브젝트로 변환 </summary>
    public CloudSaveData stringToObject(string _xmlString)
    {
        XmlSerializer _xs = new XmlSerializer(typeof(CloudSaveData));
        object _object = _xs.Deserialize(new StringReader(_xmlString));
        return (CloudSaveData)_object;
    }


    //변수 선언
    public CloudSaveData SaveData = new CloudSaveData();
    string fileName = "Mol_saveData";

    //인증여부 확인
    public bool CheckLogin()
    {
        return Social.localUser.authenticated;
    }
    //--------------------------------------------------------------------
    //게임 저장은 다음과 같이 합니다.
    public void SaveToCloud()
    {
        Debug.Log("클라우드 세이브 시작");
        if (!CheckLogin()) //로그인되지 않았으면
        {
            //로그인루틴을 진행하던지 합니다.
            PopupMessage popup = new PopupMessage();
            popup.Type = Enum.MessagePopupType.Ok;
            popup.Ok_Text = "확인";
            popup.Text = "GooglePlay에\n로그인 되어있지 않습니다";
            PopupManager.Get.OpenPopup("Popup_Message", popup);
            return;
        }

        //파일이름에 적당히 사용하실 파일이름을 지정해줍니다.
        OpenSavedGame(fileName, true);
    }


    void OpenSavedGame(string filename, bool bSave)
    {
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        Invoke("ErrorNotConnent", 20f);
        if (bSave)
        {
            savedGameClient.OpenWithAutomaticConflictResolution(filename, DataSource.ReadCacheOrNetwork, 
                ConflictResolutionStrategy.UseLongestPlaytime, OnSavedGameOpenedToSave); //저장루틴진행

            //세이브중 로딩 인디케이트 설정
            PopupMessage popup = new PopupMessage();
            popup.Text = "잠시만 기다려 주세요";
            popup.Type = Enum.MessagePopupType.Loading;
            PopupManager.Get.OpenPopup("Popup_Message", popup);
        }
        else
        {
            savedGameClient.OpenWithAutomaticConflictResolution(filename, DataSource.ReadCacheOrNetwork,
                ConflictResolutionStrategy.UseLongestPlaytime, OnSavedGameOpenedToRead); //로딩루틴 진행

            //로딩 인디케이트 설정
            PopupMessage popup = new PopupMessage();
            popup.Text = "잠시만 기다려 주세요";
            popup.Type = Enum.MessagePopupType.Loading;
            PopupManager.Get.OpenPopup("Popup_Message", popup);
        }
    }

    //savedGameClient.OpenWithAutomaticConflictResolution호출시 아래 함수를 콜백으로 지정했습니다. 준비된경우 자동으로 호출될겁니다.
    void OnSavedGameOpenedToSave(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            // handle reading or writing of saved game.
            //파일이 준비되었습니다. 실제 게임 저장을 수행합니다.
            //저장할데이터바이트배열에 저장하실 데이터의 바이트 배열을 지정합니다.
            SaveGame(game, SaveDataToBytes(), DateTime.Now.TimeOfDay);
        }
        else
        {
            PopupManager.Get.AllClosePopup();
            CancelInvoke("ErrorNotConnent");
            //파일열기에 실패 했습니다. 오류메시지를 출력하든지 합니다.
            PopupMessage popup = new PopupMessage();
            popup.Type = Enum.MessagePopupType.Ok;
            popup.Text = "로드에 실패했습니다.";
            popup.Ok_Text = "확인";
            PopupManager.Get.OpenPopup("Popup_Message", popup);
        }

    }

    void SaveGame(ISavedGameMetadata game, byte[] savedData, TimeSpan totalPlaytime)
    {
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        SavedGameMetadataUpdate.Builder builder = new SavedGameMetadataUpdate.Builder();

        builder = builder
            .WithUpdatedPlayedTime(totalPlaytime)
            .WithUpdatedDescription("Saved game at " + DateTime.Now);

        /*
        if (savedImage != null)
        {
            // This assumes that savedImage is an instance of Texture2D
            // and that you have already called a function equivalent to
            // getScreenshot() to set savedImage
            // NOTE: see sample definition of getScreenshot() method below
            byte[] pngData = savedImage.EncodeToPNG();
            builder = builder.WithUpdatedPngCoverImage(pngData);
        }*/

        SavedGameMetadataUpdate updatedMetadata = builder.Build();
        savedGameClient.CommitUpdate(game, updatedMetadata, savedData, OnSavedGameWritten);
    }


    void OnSavedGameWritten(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        PopupManager.Get.AllClosePopup();
        CancelInvoke("ErrorNotConnent");
        if (status == SavedGameRequestStatus.Success)
        {
            //데이터 저장이 완료되었습니다.
            PopupMessage popup = new PopupMessage();
            popup.Type = Enum.MessagePopupType.Ok;
            popup.Text = "저장이 완료되었습니다.";
            popup.Ok_Text = "확인";
            PopupManager.Get.OpenPopup("Popup_Message", popup);
        }
        else
        {
            //데이터 저장에 실패 했습니다.
            PopupMessage popup = new PopupMessage();
            popup.Type = Enum.MessagePopupType.Ok;
            popup.Text = "저장 실패했습니다.";
            popup.Ok_Text = "확인";
            PopupManager.Get.OpenPopup("Popup_Message", popup);
        }

        
    }


    //----------------------------------------------------------------------------------------------------------------
    //클라우드로 부터 파일읽기
    public void LoadFromCloud()
    {
        Debug.Log("클라우드 로드 시작");

        if (!CheckLogin())
        {
            //로그인되지 않았으니 로그인 루틴을 진행하던지 합니다.
            PopupMessage popup = new PopupMessage();
            popup.Type = Enum.MessagePopupType.Ok;
            popup.Ok_Text = "확인";
            popup.Text = "GooglePlay에\n로그인 되어있지 않습니다";
            PopupManager.Get.OpenPopup("Popup_Message", popup);
            return;
        }

        //내가 사용할 파일이름을 지정해줍니다. 그냥 컴퓨터상의 파일과 똑같다 생각하시면됩니다.
        OpenSavedGame(fileName, false);
    }

    void OnSavedGameOpenedToRead(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            // handle reading or writing of saved game.
            LoadGameData(game);
        }
        else
        {
            PopupManager.Get.AllClosePopup();
            CancelInvoke("ErrorNotConnent");
            //파일열기에 실패 한경우, 오류메시지를 출력하던지 합니다.
            PopupMessage popup = new PopupMessage();
            popup.Type = Enum.MessagePopupType.Ok;
            popup.Text = "로드에 실패했습니다.";
            popup.Ok_Text = "확인";
            PopupManager.Get.OpenPopup("Popup_Message", popup);
        }
    }


    //데이터 읽기를 시도합니다.
    void LoadGameData(ISavedGameMetadata game)
    {
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        savedGameClient.ReadBinaryData(game, OnSavedGameDataRead);
    }

    void OnSavedGameDataRead(SavedGameRequestStatus status, byte[] data)
    {
        PopupManager.Get.AllClosePopup();
        CancelInvoke("ErrorNotConnent");
        if (status == SavedGameRequestStatus.Success)
        {
            // handle processing the byte array data
            //데이터 읽기에 성공했습니다.
            //data 배열을 복구해서 적절하게 사용하시면됩니다.
            LoadDataFromBytes(data);
            PopupMessage popup = new PopupMessage();
            popup.Type = Enum.MessagePopupType.Ok;
            popup.Text = "로드에 성공했습니다.";
            popup.Ok_Text = "확인";
            PopupManager.Get.OpenPopup("Popup_Message", popup);

        }
        else
        {
            //읽기에 실패 했습니다. 오류메시지를 출력하던지 합니다.
            PopupMessage popup = new PopupMessage();
            popup.Type = Enum.MessagePopupType.Ok;
            popup.Text = "로드에 실패했습니다.";
            popup.Ok_Text = "확인";
            PopupManager.Get.OpenPopup("Popup_Message", popup);
        }
    }

    void ErrorNotConnent()
    {
        CancelInvoke("ErrorNotConnent");

        PopupManager.Get.AllClosePopup();

        PopupMessage popup = new PopupMessage();
        popup.Type = Enum.MessagePopupType.Ok;
        popup.Text = "응답이 없습니다\n나중에 다시 시도해주세요.";
        popup.Ok_Text = "확인";
        PopupManager.Get.OpenPopup("Popup_Message", popup);
    }
}
