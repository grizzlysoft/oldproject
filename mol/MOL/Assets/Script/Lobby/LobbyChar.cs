﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;

public class LobbyChar : MonoBehaviour
{
    LobbyCharState state = LobbyCharState.Idle;

    float Mintime = 1f;
    float MaxTime = 5f;

    float mov;
    Animator ani;
    // Use this for initialization
    void Start ()
    {
        ani = GetComponent<Animator>();
        state = (LobbyCharState)Random.Range(0, (int)LobbyCharState.Max);
        float time = Random.Range(Mintime, MaxTime);
       
        PlayAni();

        Invoke("StateDice", time);
    }

    public void SetMov(float m_, int index_)
    {
        mov = m_;

        SpriteRenderer[] r = transform.GetComponentsInChildren<SpriteRenderer>();
        
        for(int i = 0; i < r.Length; ++i)
        {
            r[i].sortingOrder = index_ + 10;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
	    if(state == LobbyCharState.MoveR && transform.localPosition.x < 580)
        {
            //Debug.Log("전 = " + transform.localPosition);
            transform.localPosition = new Vector3(transform.localPosition.x + mov, transform.localPosition.y, 0);
            //Debug.Log("후 = " + transform.localPosition);
        }

        if(state == LobbyCharState.MoveL && transform.localPosition.x > -294)
        {
            transform.localPosition = new Vector3(transform.localPosition.x - mov, transform.localPosition.y, 0);
        }
	}

    void StateDice()
    {
        CancelInvoke("StateDice");
        state = (LobbyCharState)Random.Range(0, (int)LobbyCharState.Max);
        float time = Random.Range(Mintime, MaxTime);
        
        PlayAni();

        Invoke("StateDice", time);
    }

    void PlayAni()
    {
        switch (state)
        {
            case LobbyCharState.MoveL:
                if (transform.localScale.x > 0)
                    transform.localScale = new Vector3(transform.localScale.x * -1f, transform.localScale.y, transform.localScale.z);
                ani.Play("Walking");
                break;
            case LobbyCharState.MoveR:
                if (transform.localScale.x < 0)
                    transform.localScale = new Vector3(transform.localScale.x * -1f, transform.localScale.y, transform.localScale.z);
                ani.Play("Walking");
                break;
            case LobbyCharState.Idle:
                ani.Play("Idle");
                break;
            case LobbyCharState.Taunt:
                ani.Play("Taunt");
                break;
        }
    }
}
