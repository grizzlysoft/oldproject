﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;// Required when using Event data.

public class LobbyUI : Singleton<LobbyUI>
{
    public UILabel Gold;
    public UILabel Gem;

    public UILabel[] BoostCnt = new UILabel[4];

    public Transform CharPos;

    public GameObject tree;

	// Use this for initialization
	void Start ()
    {
        RefreshMoney();
        AllRefreshBoostCnt();
        Main.Get.GetUserDataManager().ReSetUseBoostFlag();
        SoundManager.Get.PlayBgm(Enum.Bgm.BLUESKYBLUE_lobby);

        for (int i = 0; i < (int)Enum.AliseType.Max; i++)
        {
            if(Main.Get.GetUserDataManager().GetAliseLv((Enum.AliseType)i) > 0)
                CreateLobbyChar((Enum.AliseType)i);
        }

        
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    void CreateLobbyChar(Enum.AliseType type_)
    {
        GameObject c = null;
        Mercenary mer = null;

        mer = DataManager.Get.GetMercenaryData(type_, Main.Get.GetUserDataManager().GetAliseLv(type_));

        c = GameObject.Instantiate(Resources.Load("LobbyChar/" + mer.res)) as GameObject;

        c.transform.parent = CharPos;
        c.transform.localPosition = new Vector3(Random.Range(-260, 500f), 0f, 0f);
        c.transform.localScale = new Vector3(mer.scale, mer.scale, mer.scale);
        c.GetComponent<LobbyChar>().SetMov(mer.movespd, (int)type_);
        
    }

    public void RefreshMoney()
    {
        Gold.text = string.Format("{0:#,##0}", Main.Get.GetUserDataManager().GetGold());
        Gem.text = string.Format("{0:#,##0}", Main.Get.GetUserDataManager().GetGem());
    }

    public void ReFreshBoostCnt(Enum.BoostType type_, int value_)
    {
        BoostCnt[(int)type_].text = value_.ToString();
    }

    public void AllRefreshBoostCnt()
    {
        for(int i = 0; i < (int)Enum.BoostType.Max; ++i)
        {
            BoostCnt[i].text = Main.Get.GetUserDataManager().GetBoosterCount((Enum.BoostType)i).ToString();
        }
    }

    public void OnClickedStart()
    {
        PopupManager.Get.OpenPopup("Popup_Ready", null);
        //SoundManager.Get.PlaySfx(Enum.Sfx.switch2_button);
    }

    public void OnClickedUpgrade()
    {
        PopupManager.Get.OpenPopup("Popup_Upgrade", null);
    }

    public void OnClickedShop()
    {
        PopupManager.Get.OpenPopup("Popup_Shop", null);
    }

    public void OnClickedGoldIcon()
    {
        PopupManager.Get.OpenPopup("Popup_Shop", Enum.Shop_Tap.Gold);
    }

    public void OnClikedBoostIcon()
    {
        PopupManager.Get.OpenPopup("Popup_Shop", Enum.Shop_Tap.Buff);
    }

    public void OnClickedGemIcon()
    {
        PopupManager.Get.OpenPopup("Popup_Shop", Enum.Shop_Tap.Gem);
        //SoundManager.Get.PlaySfx(Enum.Sfx.switch2_button);
    }

    public void OnClickedSettingBtn()
    {
        PopupManager.Get.OpenPopup("Popup_Setting", null);
    }

    public void OnclickedCloudMenu()
    {
        PopupManager.Get.OpenPopup("Popup_Cloud", null);       
    }

    public void OnClickedAchievement()
    {
        GPGS_Manager.Get.ShowAchievement();
    }

    public void OnClickedLeaderBoard()
    {
        GPGS_Manager.Get.ShowLeaderBoard();
    }

    public void OnClickedTree()
    {
#if UNITY_EDITOR
        PopupManager.Get.OpenPopup("Popup_Cheat", null);
#endif
    }
}
